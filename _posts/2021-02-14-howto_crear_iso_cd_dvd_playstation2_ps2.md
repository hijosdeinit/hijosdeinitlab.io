---
layout: post
title: "[HowTo] Respaldar juegos cd / dvd de PlayStation 2 (PS2) creando '.iso'"
date: 2021-02-14
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "juegos", "playstation 2", "ps2", "backup", "copia de seguridad", "respaldo", "dd", "k3b", "acetoneiso", "imgburn", "consolas"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Desde hace bastante tiempo la veterana consola de videojuegos 'Sony PlayStation 2' (PS2) puede reproducir copias de seguridad de juegos almacenadas en una unidad de almacenamiento usb sin necesidad de su lector dvd.  
Es de todo el mundo conocida la escasa esperanza de vida de los discos cd / dvd: fácil deterioro, posibilidad de extravío, etcétera...  
Por todo esto puede resultar interesante mantener una copia de seguridad de nuestros juegos de PS2. Y, para ello, la mejor opción es crear a partir de cada uno de éstos su correspondiente imagen '.iso'.  

<br>
Esto se puede hacer de varias formas:  

<br>
<br>
## Creación de .iso mediante el comando 'dd'
El comando 'dd' viene de serie en todas las distribuciones GNU Linux.
Si nuestra unidad de cd/dvd fuese /dev/sr0 el comando para crear su '.iso' mediante 'dd' sería:
~~~bash
dd if=/dev/sr0 of=nombredeljuego.iso bs=512 status=progress  
~~~

<br>
<br>
## Creación de .iso mediante programas específicos
- k3b
- [AcetoneIso](https://hijosdeinit.gitlab.io/howto_acetoneiso_linux_ubuntu/). <small>«Ripea los juegos de PS2 a *.bin para que funcionen en los emuladores epsxe/psx»</small>
- ImgBurn (Windows)
- DVD Decrypter (Windows)
- Alkohol 120 (Windows)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://forums.afterdawn.com/threads/ps2-dvd-backup-in-linux.428685/">supervin en forums.afterdawn.com</a>  
<a href="https://www.elotrolado.net/hilo_como-hacer-backup-s-de-playstation2-en-linux_298915">Ksh en ElOtroLado.net</a>  
<a href="https://ubuntuforums.org/showthread.php?t=889100&p=5593531#post5593531">cisforcojo en UbuntuForums.org</a>  
<a href="http://es.tldp.org/COMO-INSFLUG/COMOs/Grabadoras-Como/">es.tldp.org</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
