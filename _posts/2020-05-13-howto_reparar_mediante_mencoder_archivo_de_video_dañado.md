---
layout: post
title: "[HowTo] reparar mediante mencoder archivo de video dañado"
date: 2020-05-13
author: Termita
category: "multimedia"
tags: ["avi", "error", "problema", "indice", "mencoder", "ffmpeg", "audio", "video", "multimedia"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Es frecuente tener un video dañado. Esto puede deberse a una descarga parcial (no completada), a que el soporte donde lo almacenábamos se ha deteriorado, a que hay datos corruptos, etcétera.

[*MEncoder*](http://www.mplayerhq.hu/design7/news.html) es un codificador de vídeo libre liberado bajo licencia GPL que, entre otras muchas cosas, permite "reparar" archivos de video.
Dependiendo de lo corrupto que esté el archivo lo resultados serán más o menos satisfactorios.

El comando a ejecutar es el siguiente:
~~~
memcoder -idx archivo_de_video_a_reparar -ovc copy -oac copy -o archivo_de_video_reparado.avi
~~~



Otra manera de reparar el índice de un fichero de video es mediante *VLC*
