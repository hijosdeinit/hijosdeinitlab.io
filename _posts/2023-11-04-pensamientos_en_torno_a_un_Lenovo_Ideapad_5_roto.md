---
layout: post
title: "Elucubraciones en torno a una computadora moderna rota"
date: 2023-11-04
author: Termita
category: "hardware"
tags: ["derecho a reparar", "obsolescencia programada", "defectuoso por diseño", "ingenieria", "plutofagia", "componentes soldados", "recambios", "upgrade", "RAM", "CPU", "LCD", "BIOS", "UEFI", "ibm", "exIBM", "lenovo", "ideapad", "ideapad 5", "thinkpad", "t470", "boot menu", "bios", "uefi", "fast boot", "secure boot", "Windows", "Windows 10", "Microsoft Store", "tienda de software", "lenovo vantage", "lenovo commercial vantage", "bloatware", "sistemas operativos", "instalacion"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Un conocido me regaló una computadora portátil marca Lenovo y modelo 'ideapad 5' con la pantalla y bisagras rotas. Poco le duró. Siniestrada ya no le servía, no le era rentable su reparación completa, y no se le ocurría qué hacer con ella.  

<br>
Lo primero que se me ocurre cuando veo cosas así es que pareciera hecho a drede. Dicen que las casualidades no existen.  
**Diríase** que a las corporaciones del *hardware* -pese a que están por la calentología, el ecologismo, la sostenibilidad y la suscripción de los objetivos de la agenda *veintetreinta* que una diminuta, filantrópica y adinerada minoría ha diseñado para salvar de sí misma a una mayoría- paradójicamente no les interesa construir productos duraderos (ni reparables, <small>luego hablaremos de eso</small>).
Yo no creo que el mundo, que -como todos sabemos- es muy grande, se vaya a terminar a corto plazo por el consumismo desenfrenado -paradójicamente creado y fomentado por esas minorías que he mencionado antes-, o mejor dicho, creo que hay cosas como la estupidez que son más letales para la humanidad que los pedos del ganado. La estupidez es suicida. El estúpido se echa al cuello, con sus propias manos, la soga que le ofrece una entidad bancaria; como estúpido es también el que acepta rescatarlo luego con los recursos de todos. Y estúpido es aquel que se cree los sermones de un mentiroso "haz-lo-que-te-diga-no-lo-que-yo-haga", y aquel que es incapaz de echar un vistazo atrás en la historia de la humanidad para trazar similitudes entre esto y aquello.  
Creo que soy uno de tantos escépticos respecto a modas y supersticiones tan en boga hoy. Son la religión de la posmodernidad, absurda y crédula. Procedo de un entorno de labriegos, bastante desconfiados, que no tiraban nada, y que -sin ostentar un título de ingeniería- se las ingeniaban para no derrochar nada. Algo debió pegárseme y hoy, a parte de escribir cosas de tecnología cutre en un blog que no lee naiden, soy aún uno más de la muy honorable orden lonchafinista del puño cerrado. Podría sudarme la polla bastante lo que diga un tipo con título de científico que necesita el sueldo de una corporación o del bobierno para poder comer. Al fin y al cabo todos sabemos que la historia está llena de escusas: "no tenía opción", "me obligaron", "yo sólo obedecía órdenes", "yo no sabía", "yo estaba en el lavabo", etcétera.  

Pero volvamos a los fabricantes de hardware, concretamente de equipos portátiles...  
Esas y otras empresas hablan el neolenguaje de moda; su lengua pareciera es de serpiente. Porque a un ingeniero no le resulta difícil diseñar un sistema de apertura y/o un chasis duradero y barato de producir en masa, ergo si en tantísimos modelos de computadoras portátiles el chasis se flexiona que da grima cada vez que se abre la tapa recién sacada la máquina de la caja y las bisagras acaban rebentando es porque, por alguna razón, interesa o no importa que eso ocurra.  
Generalmente esa máquina, prematuramente dañada, acaba en un cajón o, lo que es peor, en el cubo de la basura. Desconozco si por ello la *Pachamama* llora, lo que sí que sé es que es un derroche. Y hay algo intrínsecamente malo en eso.  
A mí me lo han regalado, porque su dueño ya no lo quiere, ya no es "transportable", la única forma de utilizarlo es conectado a un televisor o pantalla externa. No todo el mundo está dispuesto a estropear la estética de un salón de esa manera ni a sacrificar movilidad, que es para lo que estos chismes en principio fueron diseñados.  
No me importa la estética demasiado mas sí me preocupa que Lenovo -y sospecho que otros fabricantes también- no posibilite acceder a la BIOS (de momento, todo se andará) cuando sus máquinas dependen de una pantalla externa por haberse roto la que trae pegada a la placa base del equipo.  

<br>
Y no sería justo pasar por alto el tema de la reparación y/o actualización. De esto se ha hablado ya bastante, mas quizás no lo suficiente dado que los fabricantes de equipos se están empleando a fondo en:
- incorporar componentes (memoria RAM, procesador, etc) SOLDADOS a la placa base y, por tanto, no fácilmente mejorables o sustituíbles.
- suministrar recambios de esas piezas que saben que se rompen a precios obscenamente elevados, haciendo que, de poderse, no sea rentable reparar. <small>Afortunadamente existe el mercado de piezas de segunda mano y/o no-oficial.</small>
- dificultar el acceso a los componentes reparables o actualizables.  

Y este ordenador portátil Lenovo Ideapad 5, que no tiene ni 2 años y ya ha sido deshauciado, es claro compendio y muestra de todos esos vicios. ¿De verdad estos cacharros descienden de IBM?  
Frágil, cortoplacista y... "sexualmente impracticable", que dirían 'Los Ilegales': porque este chisme carece de aperturas en la parte inferior del chasis, siquiera para acceder a una de las pocas cosas reparables / actualizables: su mediocre dispositivo de almacenamiento M2. Si uno desea echar un vistazo ha de desmontar la parte inferior entera.  
¿No parece que últimamente los fabricantes quieren que las computadoras portátiles se parezcan en su construcción a un *smartphone*?  

<br>
Ojeando la máquina en cuestión veo que es una computadora que no es sólida, que pareciera haber sido diseñada para romperse y para, de haber sobrevivido a las aperturas y cierres de la tapa, resultar en un breve tiempo inútil y/o obsoleta para la mayoria de sus usuarios. Y así se junta el hambre con las ganas de comer: 8gb de memoria RAM, compartida con la tarjeta gráfica integrada... y Windows viene de serie... ¿qué podría salir mal?. No existe todavía un producto actualizado llamado "Windows para equipos viejos", y GNU Linux -que sí se lleva bien con máquinas de pocos recursos y sí permite alargar mucho la vida útil de las computadoras- ni es una alternativa para el común de los mortales, que mayoritariamente desconoce incluso su existencia, ni -cada vez con mayor frecuencia- encuentra el camino allanado para ser instalado en según qué equipos en los que -debido a inventos como la BIOS UEFI, el *secure boot*, el *fast boot* y mil putadas activadas por defecto que cada vez se asemejan más a lo que antaño considerábamos directamente *malware* (software que, entre otras cosas, hace lo que le dá la gana dado que ha sido diseñado para no obedecer al que lo utiliza sino a la empresa que lo creó)- lo que habría de ser una simple tarea se convierte prácticamente en un ejercicio extenuante de prueba-error y consulta de tutoriales por internet que hacen que un simple usuario de *geneúlinux* se sienta casi como un *hacker* / *cracker*.  

<br>
<br>
<br>
Entradas relacionadas:  
- [ThinkPad T470](https://hijosdeinit.gitlab.io/thinkpad_t470/)
- [[HowTo] Arrancar ThinkPad Desde USB (Peleándome Con un ThinkPad, Vol.01)](https://hijosdeinit.gitlab.io/howto_thinkpad_arranque_usb/)
- [Teclados Mecánicos De Hoy. La Herencia Del '*Buckling Spring*'](https://hijosdeinit.gitlab.io/buckling_spring_teclados_mecanicos/)
- []()  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[]()  
[]()</small>  

<br>
<br>
<br>
<br>
