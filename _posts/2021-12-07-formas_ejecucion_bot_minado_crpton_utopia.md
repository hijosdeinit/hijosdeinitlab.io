---
layout: post
title: "[HowTo] Formas de ejecución del bot de minado 'crypton' (ecosistema 'Utopia')"
date: 2021-12-07
author: Termita
category: "redes"
tags: ["redes", "internet 4.0", "web", "gopher", "gemini", "utopia", "crypton", "bitcoin", "criptodivisas", "minado", "sistemas operativos", "memoria", "ram", "memoria ram", "seguridad", "maquina virtual", "sandbox", "desconfianza", "liveusb", "maquina virtual", "virtualizacion", "privacidad", "docker", "uam", "vmware", "virtualbox", "puente", "bridge", "upnp"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Existen varias formas de ejecutar ['uam'](), el bot de minado ['crypton'] del ecosistema [Utopia]():
1. De forma nativa  
	1.1. bajo GNU Linux  
		1.1.1. de forma nativa "pura", sin intermediarios: 1 sola instancia  
		1.1.2. mediante 'docker': varias instancias  
	1.2. bajo Windows  
		1.2.1. de forma nativa "pura", sin intermediarios: 1 sola instancia  
		1.2.2. mediante 'docker': varias instancias  
2. Mediante una **máquina virtual**  
	2.1. host Linux, máquina virtual Linux  
		2.1.1. sin intermediarios (a parte de la propia máquina virtual): 1 sola instancia  
		2.1.2. mediante 'docker': varias instancias  
	2.2. host Linux, máquina virtual Windows  
		2.2.1. sin intermediarios (a parte de la propia máquina virtual): 1 sola instancia  
		2.2.2. mediante 'docker': varias instancias  
	2.3. host Windows, máquina virtual Linux  
		2.3.1. sin intermediarios (a parte de la propia máquina virtual): 1 sola instancia  
		2.3.2. mediante 'docker': varias instancias  
	2.4. host Windows, máquina virtual Windows  
		2.4.1. sin intermediarios (a parte de la propia máquina virtual): 1 sola instancia  
		2.4.2. mediante 'docker': varias instancias  
 
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Obtener unos céntimos de 'CRP' ('crypton') gratis](https://hijosdeinit.gitlab.io/howto_obtener_crp_crypton_gratis/)
- [Protocolo 'GEMINI': Retornando A La Senda De 'Gopher' En Pro De Una Navegación Sin Aditivos](https://hijosdeinit.gitlab.io/protocolo_Gemini_retornando_al_camino_Gopher_solucionando_deficiencias_web/)
- [Instalación De 'Amfora', Navegador Gemini Para Línea De Comandos, En Debian Y Derivados](https://hijosdeinit.gitlab.io/howto_instalacion_amphora_gemini_web_browser/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
