---
layout: post
title: "Por qué Open Media Vault monta los discos por 'UUID' y no por 'label'"
date: 2021-01-28
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "servidor", "omv", "open media vault", "discos", "label", "uuid", "nube", "ext4", "ntfs", "ext3"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
No ha mucho me percaté que Open Media Vault ya no monta los discos por 'label', sino por 'UUID'.  
Esto se debe a que, [desde la versión 5.5.20 de OMV, los discos son montados por 'UUID' y no por 'label'](https://www.openmediavault.org/?p=2866).  
No obstante, es probable que editando manualmente las secciones 'mntent' en '/etc/openmediavault/config.xml' -que reescribirían 'fstab'- se consiguiese que OMV volviera a montar los discos por 'label'.  
<br>
<br>
El UUID es un identificador único de cada disco. Se trata de una cadena de caracteres bastante larga que evita "confusiones" (no es demasiado improbable encontrar discos diferentes con mismas etiquetas ('label')) pero cuyo uso es "menos cómodo" (por la longitud y complejidad del identificador, nada que ver con las etiquetas de los discos, más cortas e inteligibles).  

<br>
<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://forum.openmediavault.org/index.php?thread/37294-how-to-mount-hdd-with-omv-by-label-instead-of-uuid/&postID=260294#post260294">macom en forum.openmediavault.org</a></small>  
<br>
<br>
<br>
<br>
<br>
<br>
