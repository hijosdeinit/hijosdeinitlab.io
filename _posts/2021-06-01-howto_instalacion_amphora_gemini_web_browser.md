---
layout: post
title: "[HowTo] Instalación de 'amfora', navegador gemini para línea de comandos, en Debian y derivados"
date: 2021-06-01
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "internet", "navegacion", "web", "navegador", "navegador web", "browser", "protocolo", "proyecto", "http", "gopher", "gemini", "etica", "privacidad", "anonimato", "respeto", "texto plano", "browser", "internet", "internet 4.0", "icecat", "iceweasel", epiphany browser", "epiphany", "Firefox", "WaterFox", "LibreFox", "LibreWolf", "min browser", "min", "chrome", "chromium", "edge", "icecat", "iceweasel"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
['Amfora'](https://github.com/makeworld-the-better-one/amfora) es uno de los navegadores existentes para el protocolo de internet ['Gemini'](https://gemini.circumlunar.space/).  
> El protocolo 'Gemini' es una deriva actual del protocolo 'Gopher', una alternativa al protocolo 'web' (http). Cuando nació el protocolo Web ya existía el protocolo 'Gopher'. El primero se impuso sobre el segundo y, con el devenir del tiempo, dio lugar a lo que hoy conocemos: la web moderna, con todos sus claros y todos sus oscuros. 'Gemini' es una vuelta a lo que debió ser internet, un retorno a aquel punto desde el cual subsanar errores esenciales que han ido acontenciendo y se han ido imponiendo hasta el punto en que hoy todo el mundo pareciera los ve como algo normal y habitual. El protocolo 'Gemini' se aleja del concepto de internet como revista, plagado de artificios, con la información sepultada entre mil filigranas supuestamente estéticas y una pléyade de scripts que en nada potencian la adquisición de conocimiento, con el internauta convertido en mercancía, catalogado, cuantificado y con su privacidad siempre expuesta cuando no invadida.  

<br>
El navegador 'Amfora' funciona desde la línea de comandos.  

<br>
<br>
## Procedimiento de instalación de 'Amfora'
1. Descárguese la versión más estable existente en la [sección 'releases' del repositorio oficial de 'amfora'](https://github.com/makeworld-the-better-one/amfora/releases).  
<br>
<a href="/assets/img/2021-06-01-howto_instalacion_amphora_gemini_web_browser/20210528_amfora_releases.png" target="_blank"><img src="/assets/img/2021-06-01-howto_instalacion_amphora_gemini_web_browser/20210528_amfora_releases.png" alt="Última release en el repositorio oficial de 'Amfora'" width="800"/></a>  
<small>Última release en el repositorio oficial de 'Amfora'</small>  
<br>

2. Renómbrese el archivo descargado a un nombre más cómodo.  
En mi caso `amfora_1.8.0_linux_64-bit`, el archivo que descargué, lo renombro como 'amfora'.
~~~bash
mv amfora_1.8.0_linux_64-bit amfora
~~~

3. Cópiese el archivo a '/usr/local/bin/'
~~~bash
sudo cp amfora /usr/local/bin/
~~~

4. Hágase ejecutable el archivo
~~~bash
sudo chmod +x /usr/local/bin/amfora
~~~

<br>
A partir de ese instante podremos ejecutar el navegador 'Amfora' en cualquier momento desde la terminal. Bastará con ejecutar el comando <big><span style="background-color:#042206"><span style="color:lime">**`amfora`**</span></span></big>.  

<br>
<a href="/assets/img/2021-06-01-howto_instalacion_amphora_gemini_web_browser/20210528_amfora.png" target="_blank"><img src="/assets/img/2021-06-01-howto_instalacion_amphora_gemini_web_browser/20210528_amfora.png" alt="'Amfora'" width="800"/></a>  
<small>'Amfora'</small>  
<br>

<br>
## Atajos de teclado para controlar 'amfora'
<span style="background-color:#042206"><span style="color:lime">?</span></span>                    Bring up this help. You can scroll!  
<br>
<span style="background-color:#042206"><span style="color:lime">Esc</span></span>                  Leave the help  
<br>
<span style="background-color:#042206"><span style="color:lime">Arrow keys, h/j/k/l</span></span>  Scroll and move a page.  
<br>
<span style="background-color:#042206"><span style="color:lime">u, PgUp</span></span>              Go up a page in document  
<br>
<span style="background-color:#042206"><span style="color:lime">PgDn, d</span></span>              Go down a page in document  
<br>
<span style="background-color:#042206"><span style="color:lime">g</span></span>                    Go to top of document  
<br>
<span style="background-color:#042206"><span style="color:lime">G</span></span>                    Go to bottom of document  
<br>
<span style="background-color:#042206"><span style="color:lime">Tab</span></span>                  Navigate to the next item in a popup.  
<br>
<span style="background-color:#042206"><span style="color:lime">Shift-Tab</span></span>            Navigate to the previous item in a popup.  
<br>
<span style="background-color:#042206"><span style="color:lime">Alt-Left, b</span></span>          Go back in the history  
<br>
<span style="background-color:#042206"><span style="color:lime">Alt-Right, f</span></span>         Go forward in the history  
<br>
<span style="background-color:#042206"><span style="color:lime">Space</span></span>                Open bar at the bottom - type a URL, link number, search term. You can also type two dots (..) to go up a directory in the URL. Typing <span style="background-color:#042206"><span style="color:lime">new:N</span></span> will open link number N in a new tab instead of the current one.  
<br>
<span style="background-color:#042206"><span style="color:lime">1 to 0</span></span>               Go to links 1-10 respectively.  
<br>
<span style="background-color:#042206"><span style="color:lime">e</span></span>                    Edit current URL  
<br>
<span style="background-color:#042206"><span style="color:lime">Enter, Tab</span></span>           On a page this will start link highlighting. Press <span style="background-color:#042206"><span style="color:lime">Tab</span></span> and <span style="background-color:#042206"><span style="color:lime">Shift-Tab</span></span> to pick different links. Press <span style="background-color:#042206"><span style="color:lime">Enter</span></span> again to go to one, or <span style="background-color:#042206"><span style="color:lime">Esc</span></span> to stop.  
<br>
<span style="background-color:#042206"><span style="color:lime">! to (</span></span>               Go to a specific tab. (Default: Shift-NUMBER)  
<br>
<span style="background-color:#042206"><span style="color:lime">)</span></span>                    Go to the last tab.  
<br>
<span style="background-color:#042206"><span style="color:lime">F1</span></span>                   Previous tab  
<br>
<span style="background-color:#042206"><span style="color:lime">F2</span></span>                   Next tab  
<br>
<span style="background-color:#042206"><span style="color:lime">Backspace</span></span>            Go home  
<br>
<span style="background-color:#042206"><span style="color:lime">Ctrl-T</span></span>               New tab, or if a link is selected, this will open the link in a new tab.  
<br>
<span style="background-color:#042206"><span style="color:lime">Ctrl-W</span></span>               Close tab. For now, only the right-most tab can be closed.  
<br>
<span style="background-color:#042206"><span style="color:lime">R, Ctrl-R</span></span>            Reload a page, discarding the cached version. This can also be used if you resize your terminal.  
<br>
<span style="background-color:#042206"><span style="color:lime">Ctrl-B</span></span>               View bookmarks  
<br>
<span style="background-color:#042206"><span style="color:lime">Ctrl-D</span></span>               Add, change, or remove a bookmark for the current page.  
<br>
<span style="background-color:#042206"><span style="color:lime">Ctrl-S</span></span>               Save the current page to your downloads.  
<br>
<span style="background-color:#042206"><span style="color:lime">Ctrl-A</span></span>               View subscriptions  
<br>
<span style="background-color:#042206"><span style="color:lime">Ctrl-X</span></span>               Add or update a subscription  
<br>
<span style="background-color:#042206"><span style="color:lime">Ctrl-C, Ctrl-Q, q</span></span>    Quit  

<br>
<br>
Entradas relacionadas:  
- [Protocolo 'GEMINI': retornando a la senda de 'Gopher' en pro de una navegación sin aditivos](https://hijosdeinit.gitlab.io/protocolo_Gemini_retornando_al_camino_Gopher_solucionando_deficiencias_web/)
- [[HowTo] 'bombadillo', navegador gemini para línea de comandos, en Debian y derivados: instalación y primeros pasos](https://hijosdeinit.gitlab.io/howto_bombadillo_gemini_web_browser_debian_y_derivados/)
- ['Gopher' hoy](https://hijosdeinit.gitlab.io/Gopher_hoy/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://golangexample.com/a-fancy-terminal-browser-for-the-gemini-protocol/">GolangExample</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
