---
layout: post
title: '[HowTo] `nohup`: para que un trabajo iniciado en segundo plano no se cancele "si nos vamos"'
date: 2019-06-25
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "linea de comandos", "cli", "terminal", "productividad", "lotes", "nohup", "bash", "acceso remoto", "byobu", "screen", "sistemas operativos", "ssh", "tmux"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
¿Cómo hacer que si iniciamos un trabajo en segundo plano -remotamente o no- y "nos vamos" (hacemos un exit y cerramos la sesión del usuario con el que lo hemos iniciado, cerramos el terminal, estando en remoto apagamos el ordenador cliente, es decir el ordenador desde el que hemos ejecutado el comando, etcétera..) **NO se cancele el trabajo**?  

Con el comando [<big>**`nohup`**</big>](), tal como he leído que hace [Atareao](https://atareao.es/software/utilidades/nohup/).  

(*) Esto sólo lo he probado con scripts y comandos que NO abren la "pantalla" de un programa como, por ejemplo, hace ['Midnight Commander'](https://midnight-commander.org/). Desconozco si sería viable hacerlo con ese tipo de comandos / aplicaciones.  

<br>
**i**
A parte de 'nohup' existen **otras formas**, otros comandos, que -como 'nohup'- permiten controlar que el trabajo que iniciamos no se cancele si "nos vamos".
Los enumeraré ahora. Algún día los trataré en otras entradas de este blog, pues -entre otras cosas- aún nos los probé:
- ['tmux'](https://tmux.github.io/) <small>[artículo sobre 'tmux' en sromero.org](http://www.sromero.org/wiki/linux/aplicaciones/tmux)</small>
- ['GNU screen'](https://www.gnu.org/software/screen/) <small>[artículo sobre 'GNU screen' en ochobitshacenunbyte.com](https://www.ochobitshacenunbyte.com/2019/04/24/que-es-y-como-funciona-el-comando-screen-en-linux/)</small>
- ['byobu'](https://www.byobu.org/) <small>[artículo sobre 'byobu' en medium.com](https://medium.com/@aliartiza75/what-is-byobu-and-how-to-use-it-b09722008d65)</small>  

<br>
'nohup' permite "enviar" las tareas a **segundo plano** para que podamos continuar realizando otras.  Primero hay que definir el concepto "SEGUNDO PLANO". Para ello, y si se desea, basta con echar un vistazo a:  
https://www.atareao.es/como/procesos-en-segundo-plano-en-linux/
https://blog.carreralinux.com.ar/2016/09/procesos-en-segundo-plano-linux/

<br>
Podría ilustrarse el uso de 'nohup' de la siguiente forma:  
<br>
Estamos en remoto, conectados a otra máquina por ssh, y queremos calcular la suma de verificación de un archivo grande.
Como estamos muy atareados, no podemos permitirnos estar parados mirando cómo el trabajo se lleva a cabo y necesitamos apagar nuestro ordenador -que es el cliente desde el que lanzamos comandos en el ordenador remoto- para hacer otras cosas. Tampoco podemos permitirnos que haciendo esto el trabajo que iniciamos se cancele.  
Por eso ejecutaremos el trabajo NO de esta forma: `sha256sum nombredearchivogrande > nombredearchivogrande.sha256.txt`  
... SINO de esta otra forma: `nohup sha256sum nombredearchivogrande > nombredearchivogrande.sha256.txt &`  

Así, si "nos vamos" (apagamos nuestra máquina cliente, por ejemplo) el trabajo continuará en la máquina remota (servidor).

Esta imagen muestra lo que hemos hecho:
/2019-06-25-howto_nohup_nocancelacion_tareas_comandos_cli/nohup_comandos.png

1. Lanzo el comando
~~~bash
nohup comando &
~~~

2. El sistema responde informando que le ha asignado el número "1" a ese proceso en "segundo plano" (a causa de añadir "&" al final) que hemos iniciado.

3. [comprobación] La orden `jobs` nos muestra:
4. la lista de procesos que tenemos iniciados en segundo plano, el número que se le ha asignado a cada uno y el estado en que están. En nuestro caso el proceso que hemos lanzado es el nº 1 y está "Ejecutándose".

5. [comprobación] `fg 1`
Trae a primer plano el proceso nº 1, que no es otro que el que lanzamos en el punto 1.

6. púlsese la combinación de teclas Ctrl + Z
Pausa el proceso

7. `bg 1`
Manda al proceso nº 1 a segundo plano y lo reanuda.

8. [comprobación] `jobs`
9. vemos que el proceso nº 1 está efectivamente ejecutándose.

10. [nos vamos] salimos del usuario con el que, con sudo, estábamos trabajando.
Acto seguido cerramos la conexión ssh que teníamos con el servidor remoto donde aún se está ejecutando el comando que lanzamos hace apenas unos instantes.

11. Pasado un rato nos volvemos a loguear en el servidor remoto donde dejamos ejecutándose el proceso nº 1 aquel... ¿se acuerdan?

12. [comprobación] `ls -la`
Listamos el contenido de la carpeta.
13. [comprobación] Vemos que el archivo resultante del comando se ha creado y que su tamaño es mayor que cero. Esto es indicio de que el trabajo concluyó correctamente a pesar de haber cerrado la conexión.

14. [comprobación] `cat nombredelarchivoresultantedeltrabajo.txt`
Constatamos que el contenido es correcto y que el trabajo concluyó correctamente a pesar de haber cerrado la conexión.




---------------------------------------------

Fuentes:



https://www.atareao.es/software/utilidades/nohup/


https://maslinux.es/que-es-el-comando-nohup-y-como-usarlo/

























Una forma de ser productivo es vencer la tentación de quedarse mirando la terminal mientras el comando que acabamos de lanzar termine su trabajo, para luego lanzar otro comando.
Para evitar eso en GNU Linux existen los scripts, comandos como 'nohup' y parámetros como <span style="background-color:black">**` && \`**</span>
Y de eso último es de lo que tratará esta entrada.

<br>
Mediante <span style="background-color:black">**` && \`**</span> se pueden encadenar tareas en la línea de comandos de GNU Linux: <span style="color:lime">`trabajo1 && \trabajo2 && \trabajo3`</span>
Por ejemplo, si se desea crear un directorio y luego mover un archivo a dicha carpeta:
~~~bash
mkdir /home/usuario/directorio && \mv /home/usuario/archivo /home/usuario/directorio/
~~~
... y así se pueden ir encadenando tantos comandos como se deseen.
En [esta entrada de este blog se explica cómo hacer que el sistema avise con un sonido cuando termine de ejecutar un comando](https://hijosdeinit.gitlab.io/howto_sonido_de_aviso_cuando_termina_un_comando_cli/) utilizando precisamente el parámetro <span style="background-color:black">**` && \`**</span>

<br>
El límite está en el ingenio, en la propia creatividad.

<br>
Por ejemplo, si deseamos ejecutar una secuencia de trabajos en un servidor remoto y dejarlo trabajando sin que las tareas que iniciemos se paren al desconectar nuestra máquina cliente de dicho servidor o, incluso, al apagarla (nuestra máquina cliente), bastaría con:

<br>
1. Conectar al servidor por 'SSH'
~~~bash
ssh usuario@servidor
~~~

<br>
2. Crear un script
~~~bash
nano script_trabajoporlotes.sh
~~~
... escribir en su interior:
~~~bash
#!/bin/bash
mkdir /home/usuario/directorio && \mv /home/usuario/archivo /home/usuario/directorio/
~~~
...guardar (ctrl+s) y salir (ctrl+x)

<br>
3. Darle permisos de ejecución al script
~~~bash
chmod +x script_trabajoporlotes.sh
~~~

<br>
4. Ejecutar el script mediante 'nohup'
~~~bash
nohup script_trabajoporlotes.sh &
~~~

<br>
5. Desconectar y, si queremos, apagar nuestra máquina cliente.
~~~bash
logout
~~~

<br>
Cuando, más tarde, volvamos a conectar al servidor podemos revisar el archivo 'nohup.out', que crea automáticamente el comando 'nohup' para guardar los 'logs' del trabajo, para ver el registro de las tareas que ordenamos.

<br>
<br>
<br>
<br>
<br>
<br>
