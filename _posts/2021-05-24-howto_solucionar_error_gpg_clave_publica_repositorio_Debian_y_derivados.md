---
layout: post
title: "[HowTo] Solucionar error de GPG cuando la clave pública de un repositorio no está disponible. (Debian y derivados)"
date: 2021-05-24
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "ubuntu", "software", "apt", "ppa", "gpg", "key", "firma", "clave publica"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En algunas ocasiones agregamos repositorios para instalar el software que contienen.  
Es vital que todo aquel repositorio que agreguemos sea confiable.
Una de las formas de agregar repositorios consiste en añadir su "ppa" a nuestro sistema mediante el comando 'add-apt-repository', [cuya implementación en Debian se explica, por ejemplo, en esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_add-apt-repository_en_debian_y_derivados/).  

<br>
Sin embargo, si a nuestro sistema no le consta que un repositorio que hayamos agregado tenga una clave gpg pública válida, al actualizar nuestras fuentes de software mediante `sudo apt-get update` obtendremos un error como, por ejemplo, este:
> W: Error de GPG: http://ppa.launchpad.net/wereturtle/ppa/ubuntu impish InRelease: Las firmas siguientes no se pudieron verificar porque su clave pública no está disponible: NO_PUBKEY 653124679B3CCB19  

<br>
Para solucionar este error basta con ejecutar el siguiente comando:
~~~bash
sudo apt-key adv --keyserver <urldelkeyserver> --recv-keys númeroqueaparecíatrasnopubkey
~~~
Por ejemplo:
~~~bash
sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys 653124679B3CCB19
~~~
... y el sistema, en este ejemplo, responderá:  
> Executing: /tmp/apt-key-gpghome.DOFUsW9yla/gpg.1.sh --keyserver keyserver.ubuntu.com --recv-keys 653124679B3CCB19  
gpg: clave 653124679B3CCB19: clave pública "Launchpad PPA for wereturtle" importada  
gpg: Cantidad total procesada: 1  
gpg:               importadas: 1  

<br>
<br>
Entradas relaccionadas:  
- [[HowTo] '/etc/apt/sources.list': repositorios en Debian y derivados](https://hijosdeinit.gitlab.io/howto_repositorios_sources.list_debian_y_derivados/)
- [[HowTo] Agregar repositorio 'debian unstable' a Debian 10 y similares](https://hijosdeinit.gitlab.io/howto_agregar_repositorio_debian_unstable_debian_y_derivados/)
- [[HowTo] Instalación de una versión más actualizada de un paquete desde los repositorios oficiales de Debian: 'backports'](https://hijosdeinit.gitlab.io/howto_instalacion_version_mas_actualizada_desde_repositorios_Debian_backports/)
- [[HowTo] Instalar el comando 'add-apt-repository' en Debian](https://hijosdeinit.gitlab.io/howto_add-apt-repository_en_debian_y_derivados/)
- [[HowTo] Eliminar repositorio agregado a mano ('add-apt-repository') en Debian y derivados](https://hijosdeinit.gitlab.io/howto_eliminar_repositorios_agregados_manualmente_addaptrepository_Debian_derivados/)
- [[HowTo] Eliminar Clave 'Gpg' Cuyo 'Id' Desconocemos. (Debian Y Derivados)](https://hijosdeinit.gitlab.io/howto_eliminar_clave_gpg_id_desconocida_agregada_mediante_apt-key_add_/)
- [El repositorio de firmware / drivers para GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- ['RaspBian' / 'RaspBerry OS' incorpora furtivamente -sin aviso ni permiso- un repositorio de 'Microsoft'. Así no vamos bien](https://hijosdeinit.gitlab.io/repositorio_microsoft_instalado_furtivamente_en_Raspbian_RaspBerryOs/)
- [[HowTo] Extirpar todo rastro del repositorio de MicroSoft que Raspbian / RaspBerry Os instala furtivamente](https://hijosdeinit.gitlab.io/howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- []()  

<br>
<br>
<br>
<br>
<br>
<br>
