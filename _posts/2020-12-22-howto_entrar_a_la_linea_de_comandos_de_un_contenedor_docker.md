---
layout: post
title: "[HowTo] Acceder a la línea de comandos de un contenedor docker"
date: 2020-12-22
author: Termita
category: "sistemas operativos"
tags: ["docker", "contenedor", "CLI", "docker exec"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Para acceder a la línea de comandos -CLI- de un contenedor docker primero hemos de conocer su identificativo:
~~~bash
sudo docker ps
~~~
Aparecerá algo así:
><small>CONTAINER ID IMAGE                         COMMAND     CREATED    STATUS PORTS NAME</small>  
<small>4a414a7bcb70 ownyourbits/nextcloudpi-armhf "/sbin/tin  1 day ago  Up     80    nextcloudpi</small>  

Acto seguido bastará con ejecutar: `docker exec -i -t identificativodelcontenedor /bin/bash`. Por ejemplo:
~~~bash
docker exec -i -t 4a414a7bcb70 /bin/bash 
~~~
<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://borrowbits.com/2020/04/entrar-a-contenedor-docker-usando-la-consola/?cn-reloaded=1" target="_blank">borrowbits</a>  
<br>
<br>
<br>
<br>
