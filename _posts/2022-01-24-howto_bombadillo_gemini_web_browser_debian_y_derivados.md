---
layout: post
title: "[HowTo] 'bombadillo', navegador gemini para línea de comandos, en Debian y derivados: instalación y primeros pasos"
date: 2022-01-24
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "internet", "navegacion", "web", "navegador", "navegador web", "browser", "protocolo", "proyecto", "http", "gopher", "gemini", "etica", "privacidad", "anonimato", "respeto", "texto plano", "browser", "internet", "internet 4.0", "web 4.0", "icecat", "iceweasel", epiphany browser", "epiphany", "Firefox", "WaterFox", "LibreFox", "LibreWolf", "min browser", "min", "chrome", "chromium", "edge", "icecat", "iceweasel"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
['Bombadillo'](https://bombadillo.colorfield.space/) es uno de los navegadores existentes para el protocolo de internet ['Gemini'](https://gemini.circumlunar.space/).  
> El protocolo 'Gemini' es una deriva actual del protocolo ['Gopher'](https://en.wikipedia.org/wiki/Gopher_%28protocol%29), una alternativa al protocolo 'web' (http). Cuando nació el protocolo Web ya existía el protocolo 'Gopher'. El primero se impuso sobre el segundo y, con el devenir del tiempo, dio lugar a lo que hoy conocemos: la web moderna, con todos sus claros y todos sus oscuros. 'Gemini' es una vuelta a lo que debió ser internet, un retorno a aquel punto desde el cual subsanar errores esenciales que han ido acontenciendo y se han ido imponiendo hasta el punto en que hoy todo el mundo pareciera los ve como algo normal y habitual. El protocolo 'Gemini' se aleja del concepto de internet como revista, plagado de artificios, con la información sepultada entre mil filigranas supuestamente estéticas y una pléyade de scripts que en nada potencian la adquisición de conocimiento, con el internauta convertido en mercancía, catalogado, cuantificado y con su privacidad siempre expuesta cuando no invadida.  

<br>
<a href="/assets/img/2022-01-24-howto_bombadillo_gemini_web_browser_debian_y_derivados/bombadillo_debian.png" target="_blank"><img src="/assets/img/2022-01-24-howto_bombadillo_gemini_web_browser_debian_y_derivados/bombadillo_debian.png" alt="'Bombadillo'" width="500"/></a>  
<small>'Bombadillo'</small>  
<br>

<br>
'Bombadillo' es un navegador 'Gemini' de código abierto, desarrollado en 'GO', multiplataforma (GNU Linux, BSD y OSX) y multiarquitectura (x86-64bits, x86-32bits, y arm) que funciona desde la línea de comandos.  
'Bombadillo' toma su nombre de [Tom Bombadil](https://en.wikipedia.org/wiki/Tom_Bombadil), personaje de la obra 'El Señor de los anillos' de J.R.R. Tolkien.  
Su desarrollador principal había hecho tiempo atrás 2 clientes para 'Gopher': ['Stubb'](https://tildegit.org/sloum/stubb) y ['Burrow](https://tildegit.org/sloum/burrow).  
Otro sofware que ha desarrollado el autor es: ['chalk'](https://rawtext.club/~sloum/chalk.html) (<small>Line based text editor for the terminal. Coded in Python</small>), ['lid'](https://rawtext.club/~sloum/lid.html) (<small>Lo-fi image dithering for the lightweight web</small>), ['gfu'](https://rawtext.club/~sloum/gfu.html) (<small>*Gophermap* format utility. Coded in Go</small>), y ['filtress'](https://rawtext.club/~sloum/filtress.html) (<small>Image filtering language/interpreter. Coded in Go</small>). A alguno de ellos dedicaré una entrada en este cuaderno de bitácora.  

<br>
A diferencia de 'Amfora', otro navegador gemini del que hablamos en [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_instalacion_amphora_gemini_web_browser/), 'Bombadillo' no sólo soporta el protocolo 'Gemini', sino también 'Gopher', ['Finger'](https://en.wikipedia.org/wiki/Finger_protocol) y el propio sistema de archivos local (del propio sistema). Asimismo, a través de añadidos de terceros, puede acceder a recursos [telnet](https://en.wikipedia.org/wiki/Telnet) y [web (http)](https://en.wikipedia.org/wiki/World_Wide_Web).  
'Bombadillo' es un navegador enfocado a la privacidad. No guarda datos acerca del uso o la navegación; sólo almacenara los elementos que el propio usuario le ordene, y éstos pueden ser marcadores, certificados gemini, parámetros de configuración o archivos descargados.  
No envía / transmite nada a nadie si el propio usuario no se lo ordena.  
Utiliza atajos de teclado inspirados en 'Vi'/'Vim' y un sencillo sistema de comandos.  
El soporte del protocolo Gemini incluye comunicación segura a través de TLS y utiliza un sistema de certificados de tipo TOFU.  

<br>
<br>
## Procedimiento de instalación de 'Bombadillo'
En un sistema Debian 10 o similar, 'Bombadillo' se puede instalar de varias formas:
- Compilando su [código fuente](https://tildegit.org/sloum/bombadillo)
- Descargando (e incorporando al sistema) uno de sus [ejecutables precompilados](https://tildegit.org/sloum/bombadillo)
- Mediante el propio gestor de paquetes de las distribuciones GNU Linux, `apt` en el caso de Debian.  

### Instalación de 'Bombadillo' mediante `apt` en Debian 10 y similares
'Bombadillo' está disponible en el repositorio 'unstable' de Debian. Por consiguiente, es necesario incorporar al sistema el repositorio 'unstable' de Debian, cuyo [procedimiento se detalla en esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_agregar_repositorio_debian_unstable_debian_y_derivados/).  
Satisfecho este requisito, el procedimiento habitual:
~~~bash
sudo apt-get update
sudo apt install bombadillo
~~~

<br>
<br>
## Ejecución y funcionamiento de 'Bombadillo'
Para lanzar 'Bombadillo' basta con ejecutar:
~~~bash
bombadillo
~~~

<br>
Como 'Bombadillo' es un navegador para la línea de comandos, su funcionamiento se hace mediante atajos de teclado, muchos de ellos basados en Vi/Vim.  
Ejecutando <span style="background-color:#042206"><span style="color:lime">`man bombadillo`</span></span> se tiene acceso a las instrucciones / manual de 'Bombadillo'.  
Asimismo, ya con el programa en ejecución, si se desea acceder a la ayuda basta con <span style="background-color:#042206"><span style="color:lime">`:help`</span></span>.

<br>
<span style="background-color:#042206"><span style="color:lime">`q`</span></span> Quit Bombadillo  
<span style="background-color:#042206"><span style="color:lime">`b`</span></span> Go back a place in browsing history  
<span style="background-color:#042206"><span style="color:lime">`f`</span></span> Go forward a place in browsing history  
<span style="background-color:#042206"><span style="color:lime">`j`</span></span> Scroll down  
<span style="background-color:#042206"><span style="color:lime">`k`</span></span> Scroll up  
<span style="background-color:#042206"><span style="color:lime">`G`</span></span> Go to end  
<span style="background-color:#042206"><span style="color:lime">`g`</span></span> Go to beginning  
<span style="background-color:#042206"><span style="color:lime">`d`</span></span> Page down  
<span style="background-color:#042206"><span style="color:lime">`u`</span></span> Page up  
<span style="background-color:#042206"><span style="color:lime">vvv`B`</span></span> Toggle bookmarks sidebar into or out of view  
<span style="background-color:#042206"><span style="color:lime">`:`</span></span> Enter command mode. Once in command mode you may enter your command (see the [commands table])  
<span style="background-color:#042206"><span style="color:lime">`spc`</span></span> Alternate entry into command mode  
<span style="background-color:#042206"><span style="color:lime">`tab`</span></span> Toggle window selection between bookmark bar and main content window (only usable if bookmark bar is open). Lets a user scroll each window as needed.  



<br>
<br>
Entradas relacionadas:  
- [Protocolo 'GEMINI': retornando a la senda de 'Gopher' en pro de una navegación sin aditivos](https://hijosdeinit.gitlab.io/protocolo_Gemini_retornando_al_camino_Gopher_solucionando_deficiencias_web/)
- [[HowTo] Instalación de 'amfora', navegador gemini para línea de comandos, en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_amphora_gemini_web_browser/)
- ['Gopher' hoy](https://hijosdeinit.gitlab.io/Gopher_hoy/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://rawtext.club/~sloum/bombadillo.html">sloum en rawtext.club</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
