---
layout: post
title: "[HowTo] '/etc/apt/sources.list': repositorios en Debian y derivados"
date: 2021-09-30
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "debian", "debian 11", "bullseye", "repositorios", "sources.list", "apt", "software", "main", "contrib", "non-free", "backports", "security"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En el archivo <span style="background-color:#042206"><span style="color:lime">'/etc/apt/sources.list'</span></span> se encuentran listados los repositorios a los que se conectará nuestro sistema cuando, generalmente mediante el comando <span style="background-color:#042206"><span style="color:lime">`apt`</span></span>, instalamos o actualizamos software en un sistema operativo GNU Linux basado en Debian.  

<br> He aquí una muestra del contenido del archivo '/etc/apt/sources.list', con los repositorios 'main', 'contrib', 'non-free', 'security' y 'backports'.   <small>(i) Es de Debian 11 *Bullseye*, mas se puede adaptar a versiones de Debian anteriores.</small>
~~~
deb http://deb.debian.org/debian bullseye main contrib non-free
deb-src http://deb.debian.org/debian bullseye main contrib non-free
# debian-security
deb http://deb.debian.org/debian-security/ bullseye-security main contrib non-free
deb-src http://deb.debian.org/debian-security/ bullseye-security main contrib non-free
# bullseye-updates
deb http://deb.debian.org/debian bullseye-updates main contrib non-free
deb-src http://deb.debian.org/debian bullseye-updates main contrib non-free
# backports
deb http://deb.debian.org/debian bullseye-backports main contrib non-free
deb-src http://deb.debian.org/debian bullseye-backports main contrib non-free
~~~

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Agregar repositorio 'debian unstable' a Debian 10 y similares](https://hijosdeinit.gitlab.io/howto_agregar_repositorio_debian_unstable_debian_y_derivados/)
- [[HowTo] Instalación de una versión más actualizada de un paquete desde los repositorios oficiales de Debian: 'backports'](https://hijosdeinit.gitlab.io/howto_instalacion_version_mas_actualizada_desde_repositorios_Debian_backports/)
- [[HowTo] Instalar el comando 'add-apt-repository' en Debian](https://hijosdeinit.gitlab.io/howto_add-apt-repository_en_debian_y_derivados/)
- [[HowTo] Eliminar repositorio agregado a mano ('add-apt-repository') en Debian y derivados](https://hijosdeinit.gitlab.io/howto_eliminar_repositorios_agregados_manualmente_addaptrepository_Debian_derivados/)
- [[HowTo] Solucionar error de GPG cuando la clave pública de un repositorio no está disponible. (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_solucionar_error_gpg_clave_publica_repositorio_Debian_y_derivados/)
- [[HowTo] Eliminar Clave 'Gpg' Cuyo 'Id' Desconocemos. (Debian Y Derivados)](https://hijosdeinit.gitlab.io/howto_eliminar_clave_gpg_id_desconocida_agregada_mediante_apt-key_add_/)
- [El repositorio de firmware / drivers para GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- ['RaspBian' / 'RaspBerry OS' incorpora furtivamente -sin aviso ni permiso- un repositorio de 'Microsoft'. Así no vamos bien](https://hijosdeinit.gitlab.io/repositorio_microsoft_instalado_furtivamente_en_Raspbian_RaspBerryOs/)
- [[HowTo] Extirpar todo rastro del repositorio de MicroSoft que Raspbian / RaspBerry Os instala furtivamente](https://hijosdeinit.gitlab.io/howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://wiki.debian.org/SourcesList">wiki.debian.org</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
