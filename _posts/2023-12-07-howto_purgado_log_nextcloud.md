---
layout: post
title: "[HowTo] Purgado del enorme archivo '.log' de 'NextCloud'"
date: 2023-12-07
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "docker", "logs", "rotacion", "tamaño", "espacio"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno: cpu RaspBerry Pi 4b, 4gb RAM, Raspbian GNU/Linux 10 (buster) armv7l, kernel 5.10.103-v7l+</small>  

<br>
<br>
Otra vez sin espacio en el disco.  

<br>
El comando <span style="background-color:#042206"><span style="color:lime">`find / -type f -size +1G -exec ls -lh {} ; | sort -k 5 -r | head -n 40`</span></span>, que sirve para [buscar archivos de gran tamaño](https://hijosdeinit.gitlab.io/howto_busqueda_archivos_grandes_cli_gnu_linux/), da en el clavo y, entre los resultados, un archivo de casi 20gb y otro de 25gb.:
> 19286245416 /zzzz/ddblomv/appdata/ncdata/nextcloud/data/nextcloud.log.1  
25212624268 /zzzz/ddblomv/appdata/ncdata/nextcloud/data/nextcloud.log  

Son los archivos'.log' que registran los "acontecimientos" que van sucediendo en la instancia de 'NextCloud'. Son demasisados gigabytes, y siguen creciendo desmesuradamente, sin control, sin rotación de contenido.  
A día de hoy desconozco si se puede configurar NextCloud para que vaya **rotando los *logs***. Por consiguiente, hube de purgarlos a mano:
~~~bash
truncate -s 0 /zzzz/ddblomv/appdata/ncdata/nextcloud/data/nextcloud.log
truncate -s 0 /zzzz/ddblomv/appdata/ncdata/nextcloud/data/nextcloud.log.1
~~~

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Purgado y rotación de 'logs' de 'docker' para evitar su desmesurado aumento de tamaño](https://hijosdeinit.gitlab.io/howto_prevencion_crecimiento_desmesurado_logs_docker_rotacion/)
- []()
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://help.nextcloud.com/t/clear-logfile-not-logrotate-solved/89485">help.nextcloud.com - clear logfile not logrotate</a>  
<a href="https://help.nextcloud.com/t/huge-nextcloud-log-file-116-gb/111651">help.nextcloud.com logfile 116gb</a>  
<a href="https://help.nextcloud.com/t/log-rotates-to-nextcloud-log-1-but-nextcloud-log-2-etc-are-gone/106736">help.nextcloud.com logrotate</a>  
<a href="https://docs.nextcloud.com/server/latest/admin_manual/configuration_server/logging_configuration.html">docs.nextcloud.com - logging configuration latest</a>  
<a href="https://docs.nextcloud.com/server/19/admin_manual/configuration_server/logging_configuration.html">docs.nextcloud.com - logging configuratioon v.19</a>  
<a href="@@@">@@@</a></small>  

<br>
<br>
<br>
<br>
