---
layout: post
title: '[HowTo] Acceso desde Nautilus mediante webdav a cuenta NextCloud'
date: 2020-08-30
author: Termita
category: "redes"
tags: ["sistemas operativos", "redes", "nubes", "NextCloud", "servicios", "servidor", "OwnCloud", "Nautilus", "sincronización"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En [esta entrada del blog](https://hijosdeinit.gitlab.io/howto_Varios_metodos_acceso_Linux-cuenta_NextCloud/) se explica los diferentes métodos para acceder a una cuenta de NextCloud desde Ubuntu y derivados.  
Uno de los métodos señalados era Estableciendo un acceso **davs://** (webdav seguro) desde Nautilus (explorador de archivos) a una cuenta específica del servidor webdav de NextCloud: Otras Ubicaciones → Conectar al Servidor  

<br>

<a href="/assets/img/2020-08-30-howto_Acceso_desde_Nautilus_mediante-webdav_a_cuenta_NextCloud/nautilus_conectar_servidor_webdav_NextCloud.png" target="_blank"><img src="/assets/img/2020-08-30-howto_Acceso_desde_Nautilus_mediante-webdav_a_cuenta_NextCloud/nautilus_conectar_servidor_webdav_NextCloud.png" alt="Nautilus, conexión con servidor webdav NextCloud" width="800"/></a>  
<small>Nautilus, conexión con servidor webdav NextCloud</small>  

<br>
NextCloud -software de sincronización de ficheros cliente-servidor- emplea el protocolo **webdav**.  

En ese método la direccion de acceso al servidor webdav, será la siguiente:  

<br>
<big>davs://ipdelservidor/remote.php/dav/files/nombredeusuariocuenta/carpeta</big>  

<br>
Por ejemplo:  
~~~
davs://miservidor.selfip.com/remote.php/dav/files/usuario02/carpetafacturas
~~~


Se puede añadir como favorito y tendremos un acceso directo en la arborescencia de Nautilus (panel izquierdo).  

Los cambios que realicemos en los archivos seguirán siendo sometidos a control de versiones, tal como hace, por ejemplo, la sincronización a través de la aplicación oficial de NextCloud.  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Varios métodos de acceso desde GNU Linux a cuenta NextCloud](https://hijosdeinit.gitlab.io/howto_Varios_metodos_acceso_Linux-cuenta_NextCloud/)
- [Instalación nativa del cliente oficial NextCloud en Debian 10 y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_nativa_cliente_nextcloud_debian_y_derivados/)
- [[HowTo] Montar webdav desde línea de comandos en cliente GNU Linux](https://hijosdeinit.gitlab.io/howto_montar-webdav_desde_cli/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
