---
layout: post
title: "[HowTo] guardar una página web en formato .pdf mediante wkhtmltopdf"
date: 2021-01-17
author: Termita
category: "sistemas operativos"
tags: ["wkhtmltopdf", "pdf", "html", "web", "productividad", "offline", "sistemas operativos", "gnu linux", "linux", "pandoc", "latex", "CLI", "convertir", "exportar"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Existen varias formas de guardar una página web en .pdf. Una de ellas es mediante 'pandoc'. Otra es desde el menú de impresión de un navegador web. Y otra es mediante un pequeño y sencillo programa llamado <big>['wkhtmltopdf'](https://wkhtmltopdf.org/)</big>.  

<br>
## Instalación de 'wkhtmltopdf'
Existen versiones para arquitecturas x86 de 32 y 64 bits, para arm 64bits, ppc 64 bits y específicamente raspberry pi.  
Asimismo 'wkhtmltopdf' es multiplataforma: puede ser instalado en Windows, macOS y GNU Linux (Debian, Ubuntu, CentOS, Amazon Linux, openSUSE Leap y Arch Linux).  

La forma rápida de instalar 'wkhtmltopdf' en Debian o derivados:
~~~bash
sudo apt-get update
sudo apt install wkhtmltopdf
~~~

<br>
**<big>Aunque</big>** en Debian 10, por ejemplo, es preferible instalarlo así:  

1) Descargar el paquete .deb desde el [repositorio oficial de wkhtmltopdf](https://wkhtmltopdf.org/downloads.html)  
> <small>`wget https://github.com/wkhtmltopdf/packaging/releases/download/0.12.6-1/wkhtmltox_0.12.6-1.buster_amd64.deb`</small>  

2) Instalar el paquete .deb mediante apt  
> <small>`sudo apt install ./wkhtmltox_0.12.6-1.buster_amd64.deb`</small>  

<br>
## Funcionamiento de 'wkhtmltopdf'
wkhtmltopdf *url* *output.pdf*  
Por ejemplo:
~~~bash
wkhtmltopdf https://hijosdeinit.gitlab.io/juegos_de_submarinos/ /home/usuario/juegosdesubmarinos.pdf
~~~
... convertirá una entrada de este blog dedicada a los juegos de submarinos a un archivo .pdf.  

<br>
## Parámetros de 'wkhtmltopdf'
Los parámetros con que podemos acompañar al comando wkhtmltopdf se pueden consultar ejecutando `wkhtmltopdf --help` ô `man wkhtmltopdf`
~~~
Synopsis:
  wkhtmltopdf [GLOBAL OPTION]... [OBJECT]... <output file>
  
Document objects:
  wkhtmltopdf is able to put several objects into the output file, an object is
  either a single webpage, a cover webpage or a table of content.  The objects
  are put into the output document in the order they are specified on the
  command line, options can be specified on a per object basis or in the global
  options area. Options from the Global Options section can only be placed in
  the global options area

  A page objects puts the content of a single webpage into the output document.

  (page)? <input url/file name> [PAGE OPTION]...
  Options for the page object can be placed in the global options and the page
  options areas. The applicable options can be found in the Page Options and 
  Headers And Footer Options sections.

  A cover objects puts the content of a single webpage into the output document,
  the page does not appear in the table of content, and does not have headers
  and footers.

  cover <input url/file name> [PAGE OPTION]...
  All options that can be specified for a page object can also be specified for
  a cover.

  A table of content object inserts a table of content into the output document.

  toc [TOC OPTION]...
  All options that can be specified for a page object can also be specified for
  a toc, further more the options from the TOC Options section can also be
  applied. The table of content is generated via XSLT which means that it can be
  styled to look however you want it to look. To get an aide of how to do this
  you can dump the default xslt document by supplying the
  --dump-default-toc-xsl, and the outline it works on by supplying
  --dump-outline, see the Outline Options section.

Description:
  Converts one or more HTML pages into a PDF document, *not* using wkhtmltopdf
  patched qt.

Global Options:
      --collate                       Collate when printing multiple copies
                                      (default)
      --no-collate                    Do not collate when printing multiple
                                      copies
      --copies <number>               Number of copies to print into the pdf
                                      file (default 1)
  -H, --extended-help                 Display more extensive help, detailing
                                      less common command switches
  -g, --grayscale                     PDF will be generated in grayscale
  -h, --help                          Display help
      --license                       Output license information and exit
  -l, --lowquality                    Generates lower quality pdf/ps. Useful to
                                      shrink the result document space
  -O, --orientation <orientation>     Set orientation to Landscape or Portrait
                                      (default Portrait)
  -s, --page-size <Size>              Set paper size to: A4, Letter, etc.
                                      (default A4)
  -q, --quiet                         Be less verbose
      --read-args-from-stdin          Read command line arguments from stdin
      --title <text>                  The title of the generated pdf file (The
                                      title of the first document is used if not
                                      specified)
  -V, --version                       Output version information and exit

Reduced Functionality:
  This version of wkhtmltopdf has been compiled against a version of QT without
  the wkhtmltopdf patches. Therefore some features are missing, if you need
  these features please use the static version.

  Currently the list of features only supported with patch QT includes:

 * Printing more than one HTML document into a PDF file.
 * Running without an X11 server.
 * Adding a document outline to the PDF file.
 * Adding headers and footers to the PDF file.
 * Generating a table of contents.
 * Adding links in the generated PDF file.
 * Printing using the screen media-type.
 * Disabling the smart shrink feature of WebKit.

Contact:
  If you experience bugs or want to request new features please visit 
  <https://github.com/wkhtmltopdf/wkhtmltopdf/issues>
~~~

<br>
<br>
<br>
Otras entradas de este blog  que tratan la conversión de contenidos web a .pdf u otros formatos son:
- [[HowTo] guardar una página web en formato .pdf mediante pandoc](https://hijosdeinit.gitlab.io/howto_web_a_pdf_pandoc/)
- [[HowTo] Conversión básica de textos y/o web mediante pandoc](https://hijosdeinit.gitlab.io/howto_conversion_basica_textos_y_web_mediante_pandoc/)
- [[HowTo] Instalación de pandoc en Debian y/o derivados](https://hijosdeinit.gitlab.io/howto_instalar_pandoc_debian/)  

<br>
<br>
<br>
<br>
<small>Fuentes:  
<a href="https://computingforgeeks.com/install-wkhtmltopdf-on-ubuntu-debian-linux/">computingforgeeks</a></small>  

<br>
<br>
<br>
