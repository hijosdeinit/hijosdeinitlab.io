---
layout: post
title: TuxGuitar en Ubuntu
date: '2012-12-01T19:32:00.001+01:00'
author: Termita
comments: true
tags: ["tuxguitar", "ubuntu", "crunchbang", "linux", "guitar pro", "guitarra"]
category: Guitarra
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
modified_time: '2012-12-01T19:34:45.665+01:00'
blogger_id: tag:blogger.com,1999:blog-1446475121237311525.post-670718734264923627
blogger_orig_url: https://built4rocknroll.blogspot.com/2012/12/tuxguitar-en-ubuntu.html
---

[Tuxguitar](http://www.tuxguitar.com.ar/) es la alternativa libre a Guitar Pro. Es 100% compatible con los archivos de guitar pro.<br />Para hacerlo funcionar:<br /><br />

1. Instalar TuxGuitar  
1.a Desde synaptic  
1.b Desde terminal  
~~~
sudo apt-get install timidity
~~~
<br />
2. Ejecutar TuxGuitar y configurarlo<br />
> Menú Herramientas → Preferencias → Sonido<br />
Configurar el puerto MIDI como 'Timidity port 0 [128:0]' o similar.
<br /><br />
--- --- ---
<br /><br />
