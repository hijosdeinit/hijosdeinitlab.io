---
layout: post
title: "[HowTo] 'ntpdate': sincronización de fecha y hora sin demonio 'ntp' en Debian y derivados"
date: 2022-01-18
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "fecha", "hora", "time", "date", "sincronizacion", "sincronizar", "ntp", "ntpdate", "servicio", "demonio"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
La fecha y hora de muchos sistemas operativos -como es el caso de Debian- suele sincronizarse automáticamente conectando de forma remota a un 'servidor ntp' que le proporciona los datos precisos.  
En GNU Linux esto suele realizarse gracias al demonio 'ntp', mas si éste no está instalado en nuestro sistema y se desea sincronizar la hora existe el comando 'ntpdate' que, a demanda, lo lleva a cabo. Por ejemplo así:
~~~bash
ntpdate hora.roa.es
~~~

Si 'ntpdate' no está instalado, en Debian y derivados, se incorpora al sistema de la siguiente forma:
~~~bash
sudo apt-get update
sudo apt install ntpdate
~~~

Es importante que la fecha y la hora de nuestro sistema esté correctamente para evitar errores y problemas: Los archivos que creemos o modifiquemos deben, así, adquirir la fecha correcta, el comando 'apt' funcionará correctamente ([se dice que si la fecha es aberrante, por ejemplo `apt-get update` puede fallar](https://unix.stackexchange.com/a/470118)).  

<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.zeppelinux.es/sincronizar-la-fecha-y-la-hora-en-debian-con-ntp/">ZeppeLinux</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
