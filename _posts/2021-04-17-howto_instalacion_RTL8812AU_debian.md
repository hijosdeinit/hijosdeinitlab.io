---
layout: post
title: "[HowTo] Instalación de tarjetas de red inalámbricas con chip Realtek 'RTL8812AU' en Debian y derivados"
date: 2021-04-18
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "ubuntu", "compilar", "compilacion", "make", "install", "instalar", "instalacion", "controlador", "driver", "modulo", "kernel", "clean", "desinstalacion", "desinstalar", "RTL8812AU", "realtek", "wireless", "aircrack", "linux-headrs", "dkms", "firmware"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En el mercado hay multitud de tarjetas de red wireless con el chip Ralink RTL8812AU. Son tarjetas "wireless ac', más rápidas que las anteriores 'wireless n' o 'wireless g'.  
Sin embargo, su instalación y operación en entornos GNU Linux no está exenta de complicaciones en lo que a la instalación de sus controladores se refiere.  
Son tarjetas que generalmente, en GNU Linux, no son 'plug and play', es decir, no basta con conectarlas al puerto usb y el sistema automáticamente las reconoce, incorpora los drivers y comienza a trabajar con ellas: suele ser necesario ir al apartado de 'controladores restringidos' o 'controladores privativos' y seleccionar su controlador. Y esto, en mi experiencia, cada vez que se actualiza el sistema -una actualización del Kernel, por ejemplo- da problemas que obligan a volver a activar el controlador.  
Asimimismo he detectado que, en mi caso, con estos controladores privativos que ofrecen algunas distribuciones GNU Linux como Ubuntu, las tarjetas wireless con chips RTL8812AU no funcionan correctamente: velocidades inestables, desconexiones, tarjeta deja de ser detectada esporádicamente, etcétera. Desconozco si esto ocurre en otros casos.  

<br>
Existen en la red por lo menos un par de controladores que se pueden incorporar a mano, compilándolos, a un sistema operativo GNU Linux. De estos dos, [uno -el de *gnab*-](https://github.com/gnab/rtl8812au), por lo menos en mi caso, no funciona en plenitud, es decir, no está exento de los defectos que tienen los "controladores privativos" ya compilados o automáticamente compilados que, de serie, incorporan -previa selección- muchos sistemas operativos GNU Linux.  
El [otro controlador](https://github.com/aircrack-ng/rtl8812au) SÍ funciona plenamente. Es el controlador del "grupo" *aircrack*.

<br>
## El controlador RTL8812AU creado por *aircrack*
[Este es el repositorio oficial del proyecto](https://github.com/aircrack-ng/rtl8812au).  
El "grupo" *aircrack* está enfocado al hacking wireless y, como tal, con sus controladores y su software busca exprimir al máximo el potencial de cada modelo o chip de tarjeta wireless.  
El código de todo el software que crean estas personas está a la vista de todos y puede ser consultado y modificado a discreción.  
Con estos drivers una tarjeta de red inalámbrica, como aquellas basadas en el chip Ralink RT8812AU, es 100% funcional en sus operaciones normales (conectarse y trabajar en una red wireless) y, además, adquiere prestaciones extra cuando el usuario así lo desea para actividades de hacking: modo monitor, etcétera.  
El controlador también puede ser incorporado a máquinas con procesadores 'arm' como RaspBerry Pi.  

<br>
## Preparación de un sistema operativo Debian 10 (y derivados) para la instalación del controlador RTL8812AU del grupo *aircrack*
Primero hay que asegurarse de que el sistema GNU Linux que se está utilizando tenga las dependencias necesarias, que son:
- [Paquetes que siempre necesita un sistema GNU Linux para compilar](https://hijosdeinit.gitlab.io/howto_preparar_debian_para_compilar/): básicamente *'build-essential'* y *'linux-headers'*. De no tenerlos, en [este artículo de este blog](https://hijosdeinit.gitlab.io/howto_preparar_debian_para_compilar/) se señala cómo instalarlos.
- Paquetes específicos que requiere el código del controlador RTL8812AY de *aircrack* para ser compilado: *'dkms'*, *'libelf-dev'*.  
Y esas dependencias o paquetes **específicos** ('dkms' y 'libelf-dev') se instalan así:
~~~bash
sudo apt-get update
sudo apt install dkms
sudo apt install libelf-dev
~~~
- otros: 'git'
~~~bash
sudo apt install git
~~~

<br>
Anotación:
Es probable que se esté trabajando <big>*"offline"*</big> (sin conexión a internet) con la máquina en la que deseamos compilar e instalar los drivers; esto es cosa harto probable si dependemos de esa tarjeta wireless con chip RTL8812AU, que estamos tratando de hacer funcionar, para conectarnos a internet.  
En tal caso basta con descargar los paquetes desde otra máquina que sí tenga acceso a internet, copiarlos en algún dispositivo de almacenamiento e instalarlos "offline". Por ejemplo, estos son los enlaces de los paquetes específicos que requiere el controlador RTL8812AU de aircrack para poder ser compilado:
- [dkms](https://packages.debian.org/buster/dkms)
- [libelf-dev](https://packages.debian.org/buster/libelf-dev)  

Para instalar "offline" estas dependencias, si -por ejemplo- esos sus paquetes que manualmente descargamos desde otra máquina fueran '.deb', basta con ejecutar desde el terminal `dpkg -i nombredelpaquete.deb`  

<br>
## Compilación e Instalación del controlador RTL8812AU del grupo *aircrack*
Suelo compilar el software en un directorio dedicado *ex profeso* para ello. Sitúese desde la terminal en ese directorio:
~~~bash
cd misoftware
~~~
Clónese mediante *git* el repositorio oficial del proyecto:
~~~bash
git clone -b v5.6.4.2 https://github.com/aircrack-ng/rtl8812au.git
~~~
Sitúese en el subdirectorio que acaba de crearse al clonar el repositorio:
~~~bash
cd rtl8812au
~~~
**Compílese el software**.  
Esto puede hacerse de dos formas.  

<br>
La forma que he probado:
~~~bash
make
sudo dkms install dkms.conf
~~~

<br>
La forma que no he probado:
~~~bash
make && make install
~~~

<br>
Y con esto, una tarjeta wireless ac con chip Ralink RTL8812AU funcionará correctamente en nuestro sistema operativo Debian 10 (o derivado). Asimismo este procedimiento puede adaptarse fácilmente a otras familias o distribuciones GNU Linux.  

<br>
## Desinstalación del controlador RTL8812AU del grupo *aircrack*
Esto aún **no lo he probado**, porque no he necesitado desinstalarlo, <small>sí [reinstalarlo tras las actualizaciones del kernel](https://hijosdeinit.gitlab.io/howto_reactivacion_tarjetawirelessac_realtek_rtl8812au_tras_actualizacion_kernel_debian_derivados/)</small>.  
Sin embargo el procedimiento de desinstalación del controlador, según sus desarrolladores, es:  
Mediante la terminal y situados en el directorio donde clonamos anteriormente el código fuente (en mi caso '/misoftware/rtl8812au'):
~~~bash
sudo dkms remove dkms.conf
~~~

<br>
## Reactivación del controlador RTL8812AU del grupo *aircrack* tras actualización del kernel
Si tras actualizar el kernel la tarjeta wireless no funciona basta con hacer lo que [en esta entrada del blog se señala](https://hijosdeinit.gitlab.io/howto_reactivacion_tarjetawirelessac_realtek_rtl8812au_tras_actualizacion_kernel_debian_derivados/), que básicamente es instalar 'linux-headers' del nuevo kernel.

<br>
<br>
## Anexo: Prestaciones "extra" del controlador RTL8812AU del grupo *aircrack*

<br>
### Setting monitor mode
1. Fix problematic interference in monitor mode.
~~~bash
airmon-ng check kill
~~~
You may also uncheck the box "Automatically connect to this network when it is avaiable" in nm-connection-editor. This only works if you have a saved wifi connection.  

2. Set interface down
~~~
sudo ip link set wlan0 down
~~~

3. Set monitor mode
~~~
sudo iw dev wlan0 set type monitor
~~~

4. Set interface up
~~~
sudo ip link set wlan0 up
~~~

<br>
### Setting TX power
~~~bash
sudo iw wlan0 set txpower fixed 3000
~~~

<br>
### LED control
#### statically by module parameter in /etc/modprobe.d/8812au.conf or wherever, for example:
~~~bash
options 88XXau rtw_led_ctrl=0
~~~
value can be 0 or 1

#### or dynamically by writing to /proc/net/rtl8812au/$(your interface name)/led_ctrl, for example:
~~~bash
echo "0" > /proc/net/rtl8812au/$(your interface name)/led_ctrl
~~~
value can be 0 or 1

#### check current value:
~~~bash
cat /proc/net/rtl8812au/$(your interface name)/led_ctrl
~~~

<br>
### USB Mode Switch
0: doesn't switch, 1: switch from usb2.0 to usb 3.0 2: switch from usb3.0 to usb 2.0
~~~bash
rmmod 88XXau
modprobe 88XXau rtw_switch_usb_mode=int (0: no switch 1: switch from usb2 to usb3 2: switch from usb3 to usb2)
~~~

<br>
### NetworkManager
Newer versions of 'NetworkManager' switches to random MAC address. Some users would prefer to use a fixed address.  
Simply add these lines below at the end of file '/etc/NetworkManager/NetworkManager.conf'
~~~
[device]
wifi.scan-rand-mac-address=no
~~~
... and restart NetworkManager with the command:
~~~bash
sudo service NetworkManager restart
~~~

<br>
<br>
Entradas relaccionadas:  
- [[HowTo] Instalación de tarjetas de red inalámbricas con chip Realtek 'RTL8812AU' en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_RTL8812AU_debian/)
- [[HowTo] Instalación de tarjeta de red inalámbricas Realtek 'RTL8822BE' en Debian 11 y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_RTL8822BE_debian11/)
- [[HowTo] Instalación de los controladores de la tarjeta wireless Realtek RTL8822CE. (Debian 10 y derivados)](https://hijosdeinit.gitlab.io/howto_instalacion_controladores_tarjeta_wireless_Realtek_rtl8822ce/)
- [[HowTo] Reactivación de tarjeta wireless ac 'Realtek RTL8812AU' tras actualización del kernel en Debian y derivados](https://hijosdeinit.gitlab.io/howto_reactivacion_tarjetawirelessac_realtek_rtl8812au_tras_actualizacion_kernel_debian_derivados/)
- [El repositorio de firmare/drivers de GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- [[HowTo] Preparar Debian y derivados para compilar software](https://hijosdeinit.gitlab.io/howto_preparar_debian_para_compilar/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://github.com/aircrack-ng/rtl8812au/blob/v5.6.4.2/README.md">aircrack-ng rtl8812au - readme</a>  
<a href="https://www.addictivetips.com/ubuntu-linux-tips/fix-the-realtek-8812au-wifi-card-on-linux/">AddictiveTips - howto compilación e instalación del otro controlador, el de 'gnab'</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
