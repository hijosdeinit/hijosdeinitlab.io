---
layout: post
title: "[HowTo] Instalación de 'timeshift' en Debian y derivados"
date: 2021-04-04
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "backup", "respaldo", "copia de seguridad", "timeshift"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Una herramienta para realizar copias de seguridad es extremadamente útil, máxime si vamos a realizar pruebas en un sistema operativo.  
Conocí 'timeshift' probando 'Linux Mint'. Debian 10.9 *netinstall* evidentemente no trae por defecto esta herramienta, así que hay que instalársela a mano.  
Esto se puede hacer de varias formas:  

<br>
## Instalación de 'timeshift' desde los repositorios oficiales de Debian
Con este método no incorporamos al sistema la versión más actualizada de los desarrolladores de 'timeshift', sino la última versión que ha sido testeada -quiero creer que exhaustivamente- por el equipo de Debian.
~~~bash
sudo apt-get update
sudo apt install timeshift
~~~
**!** (20220712) Es posible que 'Timeshift' no esté en los repositorios oficiales de, por ejemplo, Ubuntu. En tal caso habrá que recurrir a instalarlo desde los repositorios de los propios desarrolladores de 'Timeshift'.  

<br>
## Instalación de 'timeshift' desde los repositorios de los desarrolladores de 'timeshift'
Será necesario agregar dicho repositorio a nuestra lista de repositorios.
~~~bash
sudo add-apt-repository -y ppa:teejee2008/ppa
sudo apt-get update
sudo apt-get install timeshift
~~~

<br>
## Instalación de 'timeshift' mediante paquete .deb
Hay que obtener el paquete .deb de la última versión de timeshift desde la [página de sus desarrolladores](https://github.com/teejee2008/Timeshift/releases).  
Y finalmente instalar ese paquete .deb mediante 'apt':
~~~bash
sudo apt install ./nombredelpaquetedeb
~~~

<br>
Cuando se ejecuta 'timeshift' por primera vez se inicia un asistente donde se configuran los parámetros que deseamos que rijan timeshift: método (rsync ô btrfs, periodicidad, ubicación del respaldo, usuarios que deseamos respaldar, etcétera...).  

<br>
<br>
<br>
[Página oficial de 'TimeShift'](https://teejeetech.com/timeshift/)  
[Repositorio oficial de 'TimeShift' en 'GitHub'](https://github.com/teejee2008/timeshift)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://wiki.debian.org/timeshift">Debian - wiki - timeshift</a>  
<a href="https://teejeetech.com/timeshift/">teejeetech - timeshift</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
