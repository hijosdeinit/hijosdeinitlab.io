---
layout: post
title: "[HowTo] Visualización de 'markdown' en la línea de comandos (CLI): 'MDLESS'"
date: 2021-06-25
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "ubuntu", "ruby", "gem", "mdless", "visor", "markdown", "lenguaje de marcas", "código", "texto", "texto plano", "editor", "editor de texto"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## 'mdless'
['mdless'](https://github.com/ttscoff/mdless) es una aplicación para la línea de comandos que permite visualizar los documentos escritos en 'markdown' con un formato similar a cómo se verían si se exportaran a .html o .pdf. Hay elementos, como las imagenes, que no se visualizan, como es fácil suponer.  
'mdless' está escrito en 'ruby' y sus atajos de teclado son similares a los del comando 'less', omnipresente en GNU Linux.  

<br>
<br>
## ¿Cómmo se instala 'mdless'?
'mdless' requiere tener 'ruby' instalado
~~~bash
sudo apt install ruby
sudo gem install mdless
~~~

<br>
<br>
## ¿Cómo funciona 'mdless'?
La forma más habitual de utilizar 'mdless' para previsualizar un documento escrito en 'markdown':  
Desde línea de comandos  
<span style="background-color:#042206"><span style="color:lime">`mdless `*nombredelarchivomarkdown*</span></span>  
<br>
<a href="/assets/img/2021-06-25-howto_mdless_visor_markdown_cli/mdless.png" target="_blank"><img src="/assets/img/2021-06-25-howto_mdless_visor_markdown_cli/mdless.png" alt="visualización de un documento en 'markdown' mediante 'mdless'" width="600"/></a>  
<small>Visualización de un documento en 'markdown' mediante 'mdless'</small>  
<br>

<br>
<br>
Para visualizar la ayuda:
~~~bash
mdless --help
~~~
mdless 1.0.20 by Brett Terpstra  

Usage: mdless [options] [path]  

<small><span style="background-color:#042206"><span style="color:lime">`-c`</span></span> <span style="background-color:#042206"><span style="color:lime">`--[no-]color`</span></span>         Colorize output (default on)  
<span style="background-color:#042206"><span style="color:lime">`-d`</span></span> <span style="background-color:#042206"><span style="color:lime">`--debug LEVEL`</span></span>        Level of debug messages to output  
<span style="background-color:#042206"><span style="color:lime">`-h`</span></span> <span style="background-color:#042206"><span style="color:lime">`--help`</span></span>               Display this screen  
<span style="background-color:#042206"><span style="color:lime">`-i`</span></span> <span style="background-color:#042206"><span style="color:lime">`--images=TYPE`</span></span>        Include [local|remote (both)] images in output (requires chafa or imgcat, default NONE). imgcat does not work with pagers, use with -P  
<span style="background-color:#042206"><span style="color:lime">`-I`</span></span> <span style="background-color:#042206"><span style="color:lime">`--all-images`</span></span>         Include local and remote images in output (requires imgcat or chafa)  
     <span style="background-color:#042206"><span style="color:lime">`--links=FORMAT`</span></span>       Link style ([inline, reference], default inline) [NOT CURRENTLY IMPLEMENTED]  
<span style="background-color:#042206"><span style="color:lime">`-l`</span></span> <span style="background-color:#042206"><span style="color:lime">`--list`</span></span>               List headers in document and exit  
<span style="background-color:#042206"><span style="color:lime">`-p`</span></span> <span style="background-color:#042206"><span style="color:lime">`--[no-]pager`</span></span>         Formatted output to pager (default on)  
<span style="background-color:#042206"><span style="color:lime">`-P`</span></span>                        Disable pager (same as --no-pager)  
<span style="background-color:#042206"><span style="color:lime">`-s`</span></span> <span style="background-color:#042206"><span style="color:lime">`--section=NUMBER`</span></span>     Output only a headline-based section of the input (numeric from --list)  
<span style="background-color:#042206"><span style="color:lime">`-t`</span></span> <span style="background-color:#042206"><span style="color:lime">`--theme=THEME_NAME`</span></span>   Specify an alternate color theme to load  
<span style="background-color:#042206"><span style="color:lime">`-v`</span></span> <span style="background-color:#042206"><span style="color:lime">`--version`</span></span>            Display version number  
<span style="background-color:#042206"><span style="color:lime">`-w`</span></span> <span style="background-color:#042206"><span style="color:lime">`--width=COLUMNS`</span></span>      Column width to format for (default terminal width)</small>  

<br>
<br>
Entradas relaccionadas:  
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href=""></a>  
<a href=""></a>  
<a href=""></a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

