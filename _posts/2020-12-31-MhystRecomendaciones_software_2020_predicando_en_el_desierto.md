---
layout: post
title: "Recomendaciones de software de Mhyst (2020): predicando en el desierto"
date: 2020-12-31
author: Termita
category: "productividad"
tags: ["Mhyst", "vim", "asciidoc", "git", "pass", "password store", "webtorrent", "peerflix", "kodi", "tacones", "multimedia", "autodesk sketchbook", "texto plano", "vi", "contraseñas", "pass", "linux", "gnu linux", "open source", "software libre"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
31 de diciembre de 2020, en un mentidero dedicado al consumo compulsivo de tecnología generalmente pagando, le piden a Mhyst -[Reality Cracking](http://feeds.feedburner.com/rcracking)- que envíe un audio con ideas, recomendaciones... lo que quiera para despedir el año.  

<br>
Aunque sospecho que predicó en el desierto, estas eran las recomendaciones de Mhyst, un estímulo para que la parroquia de aquel podcast dejara de pagarle los vicios a alguna de esas famosas empresas de servicios de software -y/o hardware-:  

- «El móvil sigue sin ser el dispositivo principal, por mucho que se empeñe Google. El dispositivo principal, aquel con el que se puede trabajar de verdad, es el ordenador personal».
- ['vim'](https://victorhck.gitlab.io/comandos_vim/)
- ['AsciiDoc'](https://asciidoc.org/), un lenguaje de marcas como lo son ['MarkDown'](https://joedicastro.com/pages/markdown.html) o ['reStructuredText'](https://www.sphinx-doc.org/es/master/usage/restructuredtext/index.html).
- ['git'](http://git-scm.com/)
- ['pass' (Password Store)](https://www.passwordstore.org/): gestor de contraseñas
- ['WebTorrent'](https://www.linuxadictos.com/webtorrent-desktop-una-excelente-aplicacion-para-el-streaming-de-archivos-torrent.html)
- ['PeerFlix'](https://blog.desdelinux.net/peerflix-o-como-ver-torrents-en-streaming-2/)
- plugin 'Tacones' para 'Kodi'.
- ['Autodesk Sketchbook'](https://sketchbook.com/)  

<br>
<br>
Entradas relacionadas:
- [Recomendaciones de Mhyst (noviembre de 2022): predicando en el desierto. vol. 02](https://hijosdeinit.gitlab.io/MhystRecomendaciones_software_noviembre_2022_predicando_en_el_desierto_vol02/)
- []()  

<br>
<br>
<br>
<br>
<br>
<br>
<br>
