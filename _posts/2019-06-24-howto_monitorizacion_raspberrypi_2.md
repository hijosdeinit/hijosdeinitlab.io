---
layout: post
title: '[HowTo] Monitorización de RaspBerry Pi [volumen 2]: Monitorix, Raspberry-Pi-Status y Monitoriza-Tu-Raspberry'
date: 2019-06-24
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "almacenamiento", "cliente", "memoria", "RAM", "Monitorix", "Monitoriza-Tu-Raspberry", "monitorización", "procesos", "Raspberry-Pi-Status", "rpi-monitor", "servicios", "servidor", "temperatura", "voltaje", "frecuencia", "raspberry", "servidor", "seguridad", "gnu linux", "linux", "debian", "cli"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
A parte de [rpi-monitor](https://rpi-experiences.blogspot.com/), del que ya se escribió en [otra entrada de este blog](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_1/), existen otras utilidades para monitorizar remotamente RaspberryPi:  

<br>
Utilidad <big>Monitoriza-Tu-Raspberry</big> (de Atareao.es)  
[https://github.com/atareao/monitoriza_tu_raspberry](https://github.com/atareao/monitoriza_tu_raspberry)  
[https://www.atareao.es/podcast/monitorizacion-o-que-pasa-en-mi-raspberry/](https://www.atareao.es/podcast/monitorizacion-o-que-pasa-en-mi-raspberry/)  

<br>
<big>Raspberry-Pi-Status</big>  
[https://geekytheory.com/panel-de-monitorizacion-para-raspberry-pi-con-node-js](https://geekytheory.com/panel-de-monitorizacion-para-raspberry-pi-con-node-js)  
[https://www.somosbinarios.es/monitor-raspberry-pi-navegador/](https://www.somosbinarios.es/monitor-raspberry-pi-navegador/)  
[https://github.com/GeekyTheory/Raspberry-Pi-Status](https://github.com/GeekyTheory/Raspberry-Pi-Status)  

<br>
<big>Monitorix</big>  
[https://www.monitorix.org/](https://www.monitorix.org/)  
[hijosdeinit](https://hijosdeinit.gitlab.io/howto_Monitorix_instalacion_configuracion_uso_basico/)  
[https://www.forocoches.com/foro/showthread.php?t=4705042](https://www.forocoches.com/foro/showthread.php?t=4705042)  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Monitorización de RaspBerry Pi [volumen 1]: rpi-monitor](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_1/)
- [[HowTo] Monitorización de RaspBerry Pi [volumen 3]: temperaturas de CPU y GPU, frecuencia y voltaje desde línea de comandos (CLI)](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_3_temperaturas/)
- [[HowTo] Monitorix: instalación, configuracion, uso básico](https://hijosdeinit.gitlab.io/howto_Monitorix_instalacion_configuracion_uso_basico/)
- [[HowTo] 5 comandos para monitorizar la memoria RAM (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_monitorizar_memoria_ram_debian_derivados/)
- [[HowTo] Incorporar monitor de sistema a CrunchBang++ y parientes similares](https://hijosdeinit.gitlab.io/howto_monitor_sistema_en_Crunchbang_Linux_derivados/)  

<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[hijosdeinit](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_1/)</small>  
<br>
<br>
<br>
<br>
