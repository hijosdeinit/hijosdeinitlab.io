---
layout: post
title: "[HowTo] Agregar idiomas al editor (IDE) atom. Ponerlo en español"
date: 2021-03-13
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "atom", "editor de texto", "ide", "programacion", "bash", "script", "bash scripting", "markdown", "texto", "texto plano", "código", "editor", "gnu linux", "debian", "vi", "vim", "nano", "emacs", "atom", "eclipse", "sublime text", "visual studio code", "vscodium", "codium", "vscode", "netbeans"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Para que el lenguaje del interfaz de 'atom' sea el 'Español (es)' -y no 'English (en)'- hay que agregar un paquete de idiomas: 'atom-i18n'.  

<br>
El procedimiento para hacer esto es el siguiente:  
Editar → Preferencias → Instalar  
... Y para localizar el paquete con rapidez escríbase en el cuadro de búsqueda 'atom-i18n'.  

<br>
<a href="/assets/img/2021-03-13-howto_poner_atom_en_español/atom_agregar_paquete_idiomas_i18n.png" target="_blank"><img src="/assets/img/2021-03-13-howto_poner_atom_en_español/atom_agregar_paquete_idiomas_i18n.png" alt="buscando el paquete 'atom-i18n' en el gestor de paquetes de 'atom'" width="800"/></a>  
<small>Buscando el paquete 'atom-i18n' en el gestor de paquetes de 'atom' para su posterior instalación.</small>  
<br>

El gestor de paquetes de 'atom' encontrará el paquete 'atom-i18n' y tan sólo bastará con hacer click en 'instalar'.  

<br>
A continuación hay que establecer el idioma español:  
Editar → Preferencias → Paquetes → atom-i18n → Ajustes → Language → 'Español (es)'.  

<br>
<br>
<br>
## Otros artículos en este blog sobre Atom
- [[HowTo] El editor (IDE) atom y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [[HowTo] Evitar que 'atom' elimine espacios en blanco al final de línea](https://hijosdeinit.gitlab.io/howto_atom_espacios_blanco_final_linea/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)  
  
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="http://somebooks.es/configurar-la-interfaz-atom-espanol/">SomeBooks</a>  
<a href="https://joseconejos.wordpress.com/2019/10/01/instala-un-minimapa-en-atom-para-previsualizar-el-codigo/">JoseConejos</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
