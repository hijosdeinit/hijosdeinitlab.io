---
layout: post
title: "[HowTo] borrar 'archivo inmutable' en GNU Linux"
date: 2021-10-31
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "ssh", "redes", "acceso remoto", "remote", "secure shell", "gnu linux", "debian", "debian 11", "bullseye", "error", "troubleshooting", "passwordautentication"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Manipulando un liveusb de ['Slax']() me he encontrado ante una figura hasta ahora desconocida: un **archivo inmutable**.  
Su nombre era '/slax/boot/ldlinux.sys'.
> 262177  64K -r--r--r-- 1 root   root    61K oct 31 19:08 ldlinux.sys  

Era de "sólo lectura" y ni siquiera como superusuario podía borrarse.  

<br>
## ¿Cómo borrar un 'archivo inmutable'?
mediante el comando 'chattr' le retiramos (u otorgamos) la "inmutabilidad"
> Usage: chattr [-pRVf] [-+=aAcCdDeijPsStTuFx] [-v version] files...  

~~~bash
sudo chattr -i archivoinmutable
~~~
... y ya se podrá borrar.  
... en mi caso:
~~~bash
sudo chattr -i /slax/boot/ldlinux.sys
sudo rm -f /slax/boot/ldlinux.sys
~~~

<br>
Anexo:  
Para otorgar la 'inmutabilidad' a un archivo bastaría con ejecutar `sudo chattr +i `*nombredelarchivo*  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
