---
layout: post
title: "[HowTo] Torrent *headless* desde terminal (CLI), vol. 01: manejando remotamente 'transmission-daemon' mediante 'transmission-remote-client' y derivados"
date: 2022-01-12
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "transmission", "transmission-remote-cli", "transmission-remote", "transmission-cli", "transmission-daemon", "Debian", "directorio", "archivo", "configuracion", "settings.json", "stats.json", "torrent", "torrent", "resume", "descargas", "blocklists", "headless"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Podría decirse que ['Transmission'](https://transmissionbt.com/) es el cliente bittorrent más conocido en GNU Linux. Suele venir "de serie" en la mayoría de distribuciones, [como por ejemplo Debian](https://packages.debian.org/search?searchon=names&keywords=transmission).</small>  
<small>Su "demonio" o servicio -['transmission-daemon'](https://packages.debian.org/buster/transmission-daemon)-, cuando es añadido al sistema, permite manejar 'Transmission' de forma remota vía navegador web o, también, línea de comandos. Es este el campo semántico de lo que suele denominarse ["***headless***"](https://techoverflow.net/2019/05/17/what-is-a-headless-program-or-application/): cuando un programa o sistema opera sin un interfaz gráfico.</small>  

<br>
Mas, **<big>¿y si deseamos manejar 'transmission-daemon' de forma remota mediante línea de comandos (CLI), sin emplear un entorno gráfico y/o un navegador web?</big>**  
Para este menester existe oficialmente su gestor de torrents remoto para la terminal: 'transmission-remote', que forma parte del "pack" 'transmission-cli'.  
Existen otros comandos, no oficiales, que permiten controlar el demonio 'transmission-daemon'. Uno de ellos es ['transmission-remote-cli'](https://packages.debian.org/buster/transmission-remote-cli).  
Su repositorio en GitHub: [https://github.com/fagga/transmission-remote-cli](https://github.com/fagga/transmission-remote-cli)  
Se define como «an *ncurses* interface for controlling the Transmission BitTorrent daemon. It is a full featured client, providing the ability to upload, queue and delete torrent files, monitor downloads and adjust speed limits»  
Actualmente, aunque es plenamente operativo -e interesante-, 'transmission-remote-cli' **está discontinuado**. Por lo que he podido comprobar [sólo está disponible en los repositorios oficiales de Debian hasta Debian 10 *Buster*](https://packages.debian.org/search?searchon=names&keywords=transmission-remote-cli).  

<br>
### Instalación de 'transmission-remote-cli'
Lo habitual es instalarlo en aquella o aquelas máquinas que vamos a emplear **para controlar el servicio 'transmission-daemon' que corre en una máquina remota**.  
~~~bash
sudo apt-get upddate
sudo apt install transmission-remote-cli
~~~

<br>
### Empleo de 'transmission-remote-cli'
Primero deberíase crear el archivo de configuracion '~/.config/transmission-remote-cli/settings.cfg'  
~~~bash
transmission-remote-cli -c 'usuariodetransmissiondaemon:contraseña'@ipdelservidortransmissiondaemon --create-config
~~~
<br>
Para ejecutarlo y conectarse a un demonio 'transmission-daemon' (remoto o no):
~~~bash
transmission-remote-cli -c 'usuario:contraseña'@ipdelservidor:puertodelservidor
~~~
Por ejemplo, para conectarse al demonio de transmission ('transmission-daemon') que está ejecutándose en la máquina 192.168.1.85 utilizando el puerto 5858, con el usuario 'miusuario' y la contraseña 'micontraseña': `transmission-remote-cli -c 'miusuario:micontraseña'@192.168.1.85:5858`  
<br>
**‼** Un inconveniente grave de este tipo de ejecución: la contraseña se introduce en texto plano. Eso no es bueno.  

<br>
Se abre así un interfaz [*ncurses*](https://wiki.debian.org/Ncurses) cuyos atajos de teclado son los siguientes:  

<span style="background-color:#042206"><span style="color:lime">F1</span></span> / <span style="background-color:#042206"><span style="color:lime">?</span></span>  Show this help (Close with <span style="background-color:#042206"><span style="color:lime">Esc</span></span>)  
<span style="background-color:#042206"><span style="color:lime">u</span></span> / <span style="background-color:#042206"><span style="color:lime">d</span></span>  Adjust maximum global up-/download rate  
<span style="background-color:#042206"><span style="color:lime">U</span></span> / <span style="background-color:#042206"><span style="color:lime">D</span></span>  Adjust maximum up-/download rate for focused torrent  
<span style="background-color:#042206"><span style="color:lime">L</span></span>  Set seed ratio limit for focused torrent  
<span style="background-color:#042206"><span style="color:lime">+</span></span> / <span style="background-color:#042206"><span style="color:lime">-</span></span>  Adjust bandwidth priority for focused torrent  
<span style="background-color:#042206"><span style="color:lime">p</span></span>  Pause/Unpause torrent  
<span style="background-color:#042206"><span style="color:lime">P</span></span>  Pause/Unpause all torrents  
<span style="background-color:#042206"><span style="color:lime">N</span></span>  Start torrent now  
<span style="background-color:#042206"><span style="color:lime">v</span></span> / <span style="background-color:#042206"><span style="color:lime">y</span></span>  Verify torrent  
<span style="background-color:#042206"><span style="color:lime">m</span></span>  Move torrent  
<span style="background-color:#042206"><span style="color:lime">n</span></span>  Reannounce torrent  
<span style="background-color:#042206"><span style="color:lime">a</span></span>  Add torrent  
<span style="background-color:#042206"><span style="color:lime">A</span></span>  Add torrent by hash  
<span style="background-color:#042206"><span style="color:lime">Del</span></span> / <span style="background-color:#042206"><span style="color:lime">r</span></span>  Remove torrent and keep content  
<span style="background-color:#042206"><span style="color:lime">Shift+Del</span></span> / <span style="background-color:#042206"><span style="color:lime">R</span></span>  Remove torrent and delete content  
<span style="background-color:#042206"><span style="color:lime">J</span></span> / <span style="background-color:#042206"><span style="color:lime">K</span></span>  Move focused torrent in queue up/down  
<span style="background-color:#042206"><span style="color:lime">Shift+Lft</span></span>/<span style="background-color:#042206"><span style="color:lime">Rght</span></span>  Move focused torrent in queue up/down by 10  
<span style="background-color:#042206"><span style="color:lime">Shift+Home</span></span>/<span style="background-color:#042206"><span style="color:lime">End</span></span>  Move focused torrent to top/bottom of queue  
<span style="background-color:#042206"><span style="color:lime">/</span></span>  Search in torrent list  
<span style="background-color:#042206"><span style="color:lime">f</span></span>  Filter torrent list  
<span style="background-color:#042206"><span style="color:lime">s</span></span>  Sort torrent list  
<span style="background-color:#042206"><span style="color:lime">Enter</span></span> / <span style="background-color:#042206"><span style="color:lime">Right</span></span>  View torrent's details  
<span style="background-color:#042206"><span style="color:lime">o</span></span>  Configuration options  
<span style="background-color:#042206"><span style="color:lime">t</span></span>  Toggle turtle mode  
<span style="background-color:#042206"><span style="color:lime">C</span></span>  Toggle compact list mode  
<span style="background-color:#042206"><span style="color:lime">Esc</span></span>  Unfocus  
<span style="background-color:#042206"><span style="color:lime">q</span></span>  Quit  

<br>
<br>
### *Forks*: 'stig' y 'tremc'
No obstante, aunque 'transmission-remote-client' sigue siendo funcional, a partir de éste existen 2 *forks* más actualizados que su "progenitor" (aunque no los he probado aún):
- [stig](https://github.com/rndusr/stig)
- [tremc](https://github.com/tremc/tremc)  

En [askubuntu](https://askubuntu.com/questions/1344460/want-a-cli-interface-to-transmission-transmission-remote-transmission-cli-tra), en junio de 2021, señalaban que 'stig' aún estaba en fase alfa y que 'tremc', cuyo desarrollo pareciera es más lento, podría ser más estable. Es cuestión de probar ambos para salir de dudas.  

<br>
### 'stig'

<br>
### 'tremc'






<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="https://askubuntu.com/a/1344465">Artur Meinild en AskUbuntu</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
