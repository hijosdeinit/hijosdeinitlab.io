---
layout: post
title: '[HowTo] Comandos de GNU Linux para identificar el hardware de RaspBerry Pi'
date: 2019-06-24
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "hardware", "raspberry pi", "almacenamiento", "CPU", "frecuencia", "linux", "RAM", "memoria", "procesador", "RaspBerryPi", "RaspBian", "recursos", "scaling governor"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Si bien con **htop** y **top** podemos ver los procesos que están activos en la máquina y la carga que suponen para el procesador, existen otras utilidades que permiten profundizar en el hardware y su rendimiento, conocer los detalles de cada uno de los componentes sobre los que operan los procesos.  

<br>
## hwinfo
~~~bash
sudo apt install hwinfo
~~~
~~~
hwinfo
~~~
<br>
<a href="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/hwinfo.png" target="_blank"><img src="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/hwinfo.png" alt="hwinfo" width="800"/></a>  
<small>hwinfo</small>  

<br>
## hardinfo
~~~bash
sudo apt install hardinfo
~~~
Hace un listado de los componentes físicos (también de software y de red) y lleva a cabo una serie de "benchmarks".
~~~
hardinfo
~~~
<br>
<a href="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/hardinfo.png" target="_blank"><img src="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/hardinfo.png" alt="hardinfo" width="800"/></a>  
<small>hardinfo</small>  

<br>
## inxi
Proporciona información detallada tanto del hardware como del software que tenemos en nuestro equipo
~~~bash
sudo apt install inxi
~~~
~~~bash
inxi -F
~~~
<br>
<a href="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/inxi.png" target="_blank"><img src="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/inxi.png" alt="inxi" width="800"/></a>  
<small>inxi</small>  

<br>
## lscpu
Está presente en Raspbian de serie, junto a sus comandos hermanos lsusb, lspci, etcétera.  
<br>
<a href="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/lscpu.png" target="_blank"><img src="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/lscpu.png" alt="lscpu" width="800"/></a>  
<small>lscpu</small>  

<br>
## neofetch
~~~bash
sudo apt install neofetch
~~~
~~~
neofetch
~~~
<br>
<a href="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/neofetch.png" target="_blank"><img src="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/neofetch.png" alt="neofetch" width="800"/></a>  
<small>neofetch</small>  

<br>
## cpufreq-info (cpufrequtils)
Forma parte del paquete cpufrequtils, utilidades para manejarse con la característica cpufreq del kernel de Linux.
~~~bash
sudo apt install cpufrequtils
~~~
~~~bash
cpufreq-info
~~~
<br>
<a href="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/cpufreq-info.png" target="_blank"><img src="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/cpufreq-info.png" alt="cpufreq-info" width="800"/></a>  
<small>cpufreq-info</small>  

<br>
## iostat (sysstat)
iostat forma parte del paquete sysstat
~~~bash
sudo apt install sysstat
~~~
~~~bash
iostat
~~~
<br>
<a href="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/iostat.png" target="_blank"><img src="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/iostat.png" alt="iostat" width="800"/></a>  
<small>iostat</small>  

<br>
## nproc
Nos muestra el número de procesadores
~~~bash
nproc
~~~
<br>
<a href="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/nproc.png" target="_blank"><img src="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/nproc.png" alt="nproc" width="800"/></a>  
<small>nproc</small>  

<br>
## ulimit
(*) Aunque este comando no va precisamente de hardware, sino de gestión de recursos, es interesante porque es usado para mostrar / modificar los límites de recursos para los procesos.
~~~bash
ulimit
~~~
<br>
<a href="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/ulimit.png" target="_blank"><img src="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/ulimit.png" alt="ulimit" width="800"/></a>  
<small>ulimit</small>  

<br>
## dmidecode
~~~bash
sudo dmidecode  | egrep "Max Speed|Current Speed"
~~~
~~~bash
sudo dmidecode
~~~
~~~bash
sudo dmidecode -t System
~~~
~~~bash
sudo dmidecode -t BIOS
~~~
~~~bash
sudo dmidecode -t Processor
~~~
Este comando, que ha de ser ejecutado como superusuario, en otras máquinas muestra información abundante sobre los componentes de una máquina.  
Sin embargo, en RaspberryPi no muestra nada [No SMBIOS nor DMI entry point found, sorry].  
<br>
<a href="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/dmidecode.png" target="_blank"><img src="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/dmidecode.png" alt="dmidecode" width="800"/></a>  
<small>dmidecode</small>  

<br>
## /proc/cpuinfo
~~~bash
cat /proc/cpuinfo
~~~
la información del fichero del directorio /proc donde el kernel almacena información de la CPU.  
<br>
<a href="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/cat_proc_cpuinfo.png" target="_blank"><img src="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/cat_proc_cpuinfo.png" alt="cat proc cpuinfo" width="800"/></a>  
<small>cat /proc/cpuinfo</small>  

<br>
## /sys/devices/system
(*) En las líneas que vienen a continuación, cpu0 puede ser sustituído por cpu1, cpu2, cpu3, por ejemplo.  
<br>
<a href="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/cats_scalingovernors.png" target="_blank"><img src="/assets/img/2019-06-24-howto_comandos_gnulinux_identificar_hardware_RaspberryPi/cats_scalingovernors.png" alt="cat scaling governors" width="800"/></a>  
<small>cat scaling governors</small>  

<br>
~~~bash
sudo cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq
~~~
La frecuencia máxima a la que puede escalar (frecuencia nominal, es decir, la que el fabricante afirma que funciona un determinado microprocesador)  

<br>
~~~bash
sudo cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_min_freq
~~~
La frecuencia mínima a la que se puede bajar un procesador, según el fabricante.  

<br>
~~~bash
sudo cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_cur_freq
~~~
El escalamiento actual de frecuencia que se está aplicando.  

<br>
~~~bash
sudo cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_governor
~~~
El gobernador actual que se está usando.  

<br>
~~~bash
sudo cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_available_governors
~~~
lista de gobernadores disponibles.  

<br>
~~~bash
sudo cat /sys/devices/system/cpu/cpu0/cpufreq/scaling_driver
~~~
lista del controlador o driver para estos gobernadores que, en función de si es Intel, VIA, AMD, etc., será uno u otro.  

<br>
~~~bash
sudo cat /sys/devices/system/cpu/cpu0/cpufreq/affected_cpus
~~~
El/los núcleos afectado/s por la política de energía.  

<br>
~~~bash
cat /etc/init.d/raspi-config
~~~
Se trata de un script relacionado con el "scaling governor" que aplicará Raspbian al iniciarse.  

<br>
## cpupower
[!?]  no está instalado ô no existe para RaspberryPi  

<br>
## thermald
[!?]  no está instalado ô no existe para RaspberryPi  

<br>
<br>
## Para entorno gráfico existen otras aplicaciones como son:
<big>CPU-G  
[CPU-X](https://www.linuxadictos.com/cpu-x-muestra-informacion-sobre-la-cpu-motherboard-y-mas.html)  
[I-Nex](https://github.com/i-nex/I-Nex)  
[Hardinfo](https://github.com/lpereira/hardinfo)  
LSHW-GTK  
SysInfo</big>  

<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[atareao.es](https://www.atareao.es/podcast/hardware-de-tu-ordenador/)  
[linuxcenter](https://linuxcenter.es/component/k2/item/37-conociendo-tu-cpu-desde-el-terminal)</small>  
<br>
<br>
<br>
<br>
