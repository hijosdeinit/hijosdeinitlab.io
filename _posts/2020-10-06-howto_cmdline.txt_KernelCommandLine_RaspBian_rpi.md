---
layout: post
title: '[HowTo] "cmdline.txt": la línea de comandos del kernel de RaspBian'
date: 2020-10-06
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "cmdline.txt", "CLI", "kernel", "RaspBian", "RaspBerry Pi"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
# Introducción
RaspBerry Pi funciona -salvo excepciones (Android, Windows 10 iot)- con un sistema operativo GNU Linux como, por ejemplo, RaspBian.  
El **kernel** de Linux acepta -**durante el arranque de la máquina**- una serie de parámetros: la línea de comandos del kernel (*Kernel Command Line*).  
En el caso de RaspBerry Pi y sus sistemas operativos GNU Linux, esta línea de comandos del kernel está definida en un archivo de texto plano llamado <big>**'cmdline.txt'**</big> ubicado en la partición 'boot'.  
El archivo 'cmdline.txt' puede ser editado -siempre como superusuario- con cualquier editor de texto plano como 'nano', 'vim', 'emacs', 'gedit', etcétera. Por ejemplo:
~~~
sudo nano /boot/cmdline.txt
~~~
(*) Todos los parámetros han de estar en la misma línea, sin retornos de carro.  

La línea de comandos que se le ha "pasado" al kernel durante el arranque puede ser consultada:
~~~
cat /proc/cmdline
~~~
(*) El resultado de este comando no será exactamente igual a lo que contiene 'cmdline.txt' dado que el firmware puede hacer cambios antes de lanzar el kernel.  

Este podría ser el contenido de un archivo 'cmdline.txt':  
><small>`dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 root=PARTUUID=1234567e4-01 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait rootdelay=5 quiet splash plymouth.ignore-serial-consoles`</small>  

<br>
# Parámetros y opciones

<br>
## Parámetros y opciones Estándar

### console
'console='
Define la consola serie. Usualmente hay 2 entradas:
> console=serial0,115200  
console=tty1  

<br>
### root
'root='  
Define la localización del sistema de archivos raíz (partición 'rootfs'). Por ejemplo 'root=/dev/mmcblk0p2' o 'root=PARTUUID=1234567e4-01'.  

<br>
### rootfstype
'rootfstype='  
Define qué tipo de sistema de archivos utiliza el sistema de archivos raíz (rootfs). Por ejemplo 'rootfstype=ext4'.  

<br>
### elevator
'elevator='  
Specifies the I/O scheduler to use.  
'elevator=deadline' means the kernel imposes a deadline on all I/O operations to prevent request starvation.  

<br>
### quiet
'quiet'  
Establece el nivel por defecto de registro (log) del kernel a 'KERN_WARNING', lo cual suprime todos los mensajes de registro -salvo los de seria gravedad- durante el arranque.  

<br>
<br>
## Parámetros y opciones de Pantalla (display) en los modos FKMS y KMS
><small>Explicación de lo que he averiguado acerca de los "modos" "FKMS" y "KMS"  
"Mode setting" (selección de modo) es una operación de software que activa un modo de visualización (resolución de pantalla, profundidad de color y tasa de refresco) para el controlador de pantalla de una computadora.  
KMS (*Kernel Mode Setting*): el modo de visualización es establecido por el kernel. KMS lo hace todo accediendo a los registro de hardware directamente desde el espacio ARM, pasando por alto completamente al firmware.  
FKMS (*Fake / Firmware Kernel Mode Setting*): en este modo el kernel selecciona el modo para la pantalla pero el firmware del videocore controla la salida de video. FKMS utiliza las APIs de dispmanx y mailbox para comunicarse con el firmware cuando se trata de cosas como la composición o la salida de video.  
Legacy Mode: el firmware del videocore controla todo.  
Full KMS: el kernel controla todo</small>  

El firmware automáticamente añade una resolución y un *overscan* preferidos vía una entrada como esta:  
<small>`video=HDMI-A-1:1920x1080M@60,margin_left=0,margin_right=0,margin_top=0,margin_bottom=0`</small>  
Esta entrada o configuración por defecto puede ser modificada copiándola y modificándola en '/boot/cmdline.txt'. Se pueden, por consiguiente, cambiar los parámetros de los márgenes, añadir rotación y parámetros de rotación, etcétera... tal como está documentado en [standard Linux Framebuffer documentation](https://github.com/raspberrypi/linux/blob/rpi-4.19.y/Documentation/fb/modedb.txt).  
Por defecto las opciones 'margin_*' son establecidas desde las entradas 'overscan' de '/boot/config.txt', si están presentes.  
Se puede prevenir al firmware para que no haga ningún cambio específico de KMS en la línea de comandos añadiendo al archivo '/boot/config.txt' el parámetro 'disable_fw_kms_setup=1'.  
Un ejemplo de entrada sería:  
<small>`video=HDMI-A-1:1920x1080M@60,margin_left=0,margin_right=0,margin_top=0,margin_bottom=0,rotate=90,reflect_x`</small>  

Algunas posibles opciones para el tipo de display, la primera parte de la entrada 'video=', son las siguientes:  

<br>
### HDMI-A-1
HDMI 1 (HDMI 0 on silkscreen of Pi4B, HDMI on single HDMI boards).  

<br>
### HDMI-A-2
HDMI 2 (HDMI 1 on silkscreen of Pi4B)  

<br>
### DSI-1
DSI o DPI  

<br>
### Composite-1
Composite  

<br>
<br>
## Otras entradas o parámetros

### splash
'splash'  
Hace que en el arranque se muestre una *splash screen* vía el módulo *Plymouth*.  

<br>
### plymouth.ignore-serial-consoles
'plymouth.ignore-serial-consoles'  
Normalmente, si el módulo *Plymouth* está activado, éste impedirá que aparezcan mensajes del arranque en cualquier consola serie que se halle presente.  
Este parámetro le señala a *Plymouth* que ignore todas las consolas serie, haciendo los mensajes del arranque visibles de nuevo, tal como ocurriría si *Plymouth* no estuviera activo.  

<br>
### dwc_otg.lpm_enable
'dwc_otg.lpm_enable='  
Apaga (0) / Enciende (1) el LPM (*Link Power Management*) en el driver 'dwc_otg'.  
> El driver 'dwc_otg' es el driver del controlador USB que está construído dentro de RaspBerry Pi.  

<br>
### dwc_otg.speed
'dwc_otg.speed='  
Establece la velocidad del controlador USB de la placa de RaspBerry Pi.  
Esta opción no debe ser configurada salvo para resolver problemas con dispositivos USB.  
> 'dwc_otg.speed=1' configura el controlador USB a "full speed" (USB 1.0, 12Mbps), lo cual es más lento que "high speed" (USB 2.0).  

<br>
### smsc95xx.turbo_mode
'smsc95xx.turbo_mode='  
Conecta / Desconecta el "modo turbo" del driver de red cableada.  
> 'smsc95xx.turbo_mode=N' desconecta el "modo turbo".  

<br>
### usbhid.mousepoll
'usbhid.mousepoll='  
Especifica el *polling interval* (¿latencia?) del ratón.  
> 'usbhid.mousepoll=0' ayudaría con un ratón wireless lento o errático.  

<br>
### fsck.repair
'fsck.repair='  
Responde 'sí' (=yes) o 'no' (=no) a todas las preguntas / interacción durante la comprobación del disco en el arranque.  

<br>
### rootdelay
'rootdelay='  
Retrasa x segundos el intento de montar el sistema de archivos raíz.

<br>
### rootwait
'rootwait'  
Especifica el número de segundos máximo que 'initramfs' esperará hasta que el sistema de archivos raíz (rootfs) esté disponible. Especialmente útil para arrancar desde dispositivos USB externos (hdd, pendrive, etc...).  

<br>
### kgdboc
'kgdboc=ttyAMA0,115200'  
[Utilización de kgdb / gdb](https://www.kernel.org/doc/htmldocs/kgdb/EnableKGDB.html)  

<br>
### smsc95xx.macaddr
'smsc95xx.macaddr=xx:xx:xx:xx:xx:xx'  
Ignora la dirección MAC real y la sustituye con la MAC especificada mediante este parámetro.  

<br>
### dwc_otg.microframe_schedule
'dwc_otg.microframe_schedule='  
1 (default now) This should fix the error when too many periodic endopoints are present.  

<br>
### dwc_otg.fiq_fix_enable
'dwc_otg.fiq_fix_enable='  
1 (default now) give about 10% extra performance to ARM when USB is not busy by lowering the number of interrupts USB does.  

<br>
### dwc_otg.nak_holdoff_enable
'dwc_otg.nak_holdoff_enable='  
1 (default now). NAK holdoff schame.  

<br>
### ip
'ip=xxx.xxx.xxx.xxx'. Ip local estática. [Más información](http://pihw.wordpress.com/guides/direct-network-connection/in-a-nut-shell-direct-network-connection/).  

<br>
<br>
## Command Line Options de XBian
XBian es una versión de Debian para RaspBerry Pi.  
XBian is a small, fast and lightweight media center distribution for the Raspberry Pi, CuBox-i, Hummingboard and many more devices to come. It is based on a minimal Debian and therefore offers much of the same freedom as Debian offers.  
Es posible que muchas de estas opciones sirvan para el 'cmdline.txt' de RaspBian.  

<br>
### noresizesd
noresizesd  
disables the automatic SD resize to full capacity.  

<br>
### partswap
partswap  
creates a separate swap partition instead of using a swap file.  

<br>
### noswap
noswap  
disables the creation of a swap file with standard system scripts / tools. Because swap is handled with zram-swap package on xbian, this option tells the system to skip all tasks around swaps (activating, waiting to appear, deactivating during reboot/shutdown etc.)  

<br>
### noconvertsd
noconvertsd  
won't convert the EXT4 filesystem to BTRFS. Convertion will run only if rootfstype=btrfs is specified and actual filesystem is EXT4. The actual location of the root partition is not relevant. XBian will convert the USB stick or external hard drives it is booting from (if the actual filesystem is still EXT4 and BTRFS is specified in cmdline.txt).  

<br>
### root=
root=  
this parameter can not only hold device path's but also partition labels root=LABEL=xbian-root-btrfs, UUID's root=UUID=12345-0123-10234 or PARTUUID's root=PARTUUID=abcdefab-02  

<br>
### rootflags=subvol=root/@,autodefrag,compress=lz4
rootflags=subvol=root/@,autodefrag,compress=lz4  
specifies additional mount options for the root partition.  

<br>
### rootwait=10
rootwait=10  
specifies the maximum number of seconds the initramfs should wait until a root device has become available. Especially handy for booting from external USB or HDD devices.  

<br>
### splash
splash  
hows the XBian splash.  

<br>
### debug
debug  
shows as much debug information as possible (overrides the loglevel, console, and quiet parameter).  

<br>
### rescue
rescue  
will drop you to a rescue shell just before mounting rootfs to /rootfs. It is possible to continue with booting after typing exit).  

<br>
### rescue_early
rescue_early  
will drop you to a rescue shell early in the initramfs process. It is possible to continue with booting after typing exit.  

<br>
### rescue_late
rescue_late  
will drop you to a rescue shell at the end in the initramfs process (before switching to root). It's possible to continue with booting after typing exit.  

<br>
### telnet
telnet  
will start a telnet session at boot time. This can be handy to fix your system before SSH has started. Telnet will be stopped when SSH has been started.  

<br>
### vnc
vnc  
will start VNC server at boot time if VNC stuff has been included in initramfs. For more information please read /etc/default/xbian-initramfs  

<br>
### modules-load
modules-load=module1=param=value,module2…  
this parameter allows loading kernel modules without the need of adding them to /etc/modules file or adding a conf file into one of the supported modules-load.d/ folders. Format is as follows (modules are separated by comma, params by colon): modules-load=moduleA[=paramA1=valueA1[:parmA2=valueA2][:…]]][,moduleB[=paramB1=valueB1], for example modules-load=bridge,em28xx=disable_ir=1:i2c_scan=100:core_debug=1:reg_debug=0  

<br>
<br>
<br>
---  
<small>Fuentes:  
[raspberrypi/linux](https://github.com/raspberrypi/linux/blob/rpi-4.0.y/Documentation/kernel-parameters.txt)  
[raspberrypi.org](https://www.raspberrypi.org/documentation/configuration/cmdline-txt.md)  
[linux uprising](https://www.linuxuprising.com/2019/05/how-to-force-fsck-filesystem.html)  
[manpages.debian.org](https://manpages.debian.org/stretch/systemd/systemd-fsck@.service.8.en.html)  
[wiki.xbian.org](http://wiki.xbian.org/doku.php/cmdline)  
[elinux](https://elinux.org/RPi_cmdline.txt)</small>  
<br>
<br>
<br>
<br>
