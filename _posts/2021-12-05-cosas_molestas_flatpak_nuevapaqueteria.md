---
layout: post
title: "Nueva paquetería, vol. 03: cosas MOLESTAS de 'flatpak'"
date: 2021-11-28
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "nueva paqueteria", "snap", "appimage", "electron", "flatpak", "deb", "dpkg", "apt", "software"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Para los, como yo, profanos en la nueva paquetería y acostumbrados a la instalación / desinstalación de software de forma nativa (mediante `apt`, por ejemplo) puede resultarnos muy molesto el hecho de que tras instalar desde 'FlatHub' un "paquete" 'flatpak' y sus correspondientes "dependencias", cuando se desea desinstalar dicho "paquete" la dependencias no se desinstalen / purguen también: quedan restos en el sistema.  

Por ejemplo:  
'Steam' se puede incorporar a Debian 10 de forma nativa -mediante `apt`- ô también mediante la nueva paquetería 'flatpak'. En ese segundo caso esto se hace así:
~~~bash
flatpak install flathub com.valvesoftware.Steam
~~~
... y así se instala 'Steam' -y los paquetes de los que depende, que son bastante grandes- desde 'FlatHub'.  

Para desinstalar el paquete 'flatpak' de 'Steam, por lo que sé, hay que ejecutar:
~~~bash
flatpak uninstall flathub Steam
~~~
... y el sistema responde:
~~~
Found installed ref ‘app/com.valvesoftware.Steam/x86_64/stable’ (system). Is this correct? [Y/n]: y
        ID                              Arch          Branch
 1. [-] com.valvesoftware.Steam         x86_64        stable
Uninstall complete.
~~~
... mas **SÓLO** se desinstala el 'flatpak' de 'Steam', mas no todos aquellos "paquetes" (dependencias) que se instalaron.  

<br>
Probablemente -al igual que sucede con 'apt' (<small>remove, purge, autoremove, clean, autoclean</small>)- existe algún parámetro para desinstalar / purgar esas "dependecias", mas -a diferencia de lo que ocurre con `apt`- me está costando encontrar esa información y, mientras tanto, la sensación es bastante desagradable.  
 
<br>
<br>
Entradas relacionadas:  
- [[HowTo] nueva paquetería, vol.01: 'snap' en Debian 10 Buster (y derivados)](https://hijosdeinit.gitlab.io/howto_snap_en_debian_10_buster_y_derivados/)
- [[HowTo] nueva paquetería vol.02: incorporación de soporte 'flatpak' a Debian 10 y derivados](https://hijosdeinit.gitlab.io/howto_soporte_flatpak_debian_10/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  
<br>
<br>
<br>
<br>
<br>
<br>
