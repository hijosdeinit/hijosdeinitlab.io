---
layout: post
title: "[HowTo] Extirpar todo rastro del repositorio de MicroSoft que Raspbian / RaspBerry Os instala furtivamente"
date: 2021-02-05
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "raspbian", "raspberry os", "repositorio", "microsoft", "vs code", "visual studio code", "desconfianza", "mala praxis", "seguridad", "privacidad", "microsoft.gpg", "hosts", "apt", "raspberry-sys-mods", "vscode.list"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## Antecedentes
<small>**‼** El [problema](https://hijosdeinit.gitlab.io/repositorio_microsoft_instalado_furtivamente_en_Raspbian_RaspBerryOs/) salió a la luz hace pocos días, el pasado 29 de enero de 2021 concretamente. Y lo descubrió involuntariamente un usuario llamado GarrickAndersLyng mientras solucionaba algunos incidentes que estaba experimentando con el paquete 'sudo'.  
Se percató, asombrado, de que su servidor se conectaba a un repositorio de Microsoft: `packages.microsoft.com`.</small>  

<br>
<br>
<br>
## Solución
La falta de confianza que genera la desafortunada acción de la 'Fundación RaspBerry Pi' bien mereciera el abandono de 'RaspBian' o 'RaspBerryPi OS'.  
No obstante, si -a pesar de todo- se decidiera continuar utilizando 'RaspBian' o 'RaspBerryPi OS', es menester bloquear "los cambios" que sin permiso se hicieron.  

<br>
**♠** Hágase copia de seguridad del archivo '/etc/hosts'
~~~bash
sudo cp /etc/hosts /etc/hosts.bkp01
~~~

Edítese el archivo '/etc/hosts'
~~~bash
sudo nano /etc/hosts
~~~
<br>
<a href="/assets/img/2021-02-05-howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/2021-02-07_nano_etc_hosts.png" target="_blank"><img src="/assets/img/2021-02-05-howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/2021-02-07_nano_etc_hosts.png" alt="WARNING: Do not edit this file, your changes will get lost" width="800"/></a>  

<br>
...y añádasele el dominio 'packages.microsoft.com' para que lo bloquee. Esto se hace agregando la línea
~~~
0.0.0.0 packages.microsoft.com
~~~
**ℹ** <small>En el archivo '/etc/hosts' hay un comentario que señala: '# WARNING: Do not edit this file, your changes will get lost'.</small>  
<small>Por consiguiente, habrá que ir comprobando si el cambio que se ha hecho en '/etc/hosts' soporta los reinicios y las actualizaciones venideras.</small>   
También podría añadirse ese dominio de Microsoft 'packages.microsoft.com' a [Pi-Hole](https://pi-hole.net/), si es que tenemos instalado este excelente agujero anti-vendedoresdeenciclopedias.  

Compruébese que el dominio 'packages.microsoft.com' ha sido efectivamente bloqueado ejecutando <span style="color:lime">`lynx packages.microsoft.com`</span>. <small>**ℹ** 'lynx' es un navegador para línea de comandos. Se puede instalar mediante <span style="color:lime">`sudo apt install lynx`</span></small>  
<br>
<a href="/assets/img/2021-02-05-howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/2021-02-07_packagesmicrosoft_efectivamente_bloqueado_etchosts.png" target="_blank"><img src="/assets/img/2021-02-05-howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/2021-02-07_packagesmicrosoft_efectivamente_bloqueado_etchosts.png" alt="packages.microsoft.com efectivamente bloqueado" width="800"/></a>  
<small>'packages.microsoft.com' efectivamente bloqueado vía '/etc/hosts'</small>

<br>
<br>
**♠** Fíjese como retenido (*on hold*) el paquete 'raspberrypi-sys-mods' para que no se instalen próximas actualizaciones y también para que no pueda ser removido:
~~~bashbash
sudo apt-mark hold raspberrypi-sys-mods
~~~
> raspberrypi-sys-mods set on hold.  

**ℹ**<small>¿Cómo se desharía esto ("on hold"), es decir hacer que el paquete 'raspberrypi-sys-mods' deje de estar retenido, llegado el caso?: <span style="color:lime">`sudo apt-mark unhold raspberrypi-sys-mods`</span></small>  

<br>
<br>
**♠** Hágase copia de seguridad de la clave gpg de Microsoft, que es el archivo '/etc/apt/trusted.gpg.d/microsoft.gpg':
~~~bash
sudo cp /etc/apt/trusted.gpg.d/microsoft.gpg /etc/apt/trusted.gpg.d/microsoft.gpg.bkp01
~~~

Bórrese la clave gpg de Microsoft ('/etc/apt/trusted.gpg.d/microsoft.gpg')
~~~bash
sudo rm -vf /etc/apt/trusted.gpg.d/microsoft.gpg
~~~
> removed '/etc/apt/trusted.gpg.d/microsoft.gpg'  

Créese un nuevo archivo '/etc/apt/trusted.gpg.d/microsoft.gpg'
~~~bash
sudo touch /etc/apt/trusted.gpg.d/microsoft.gpg
~~~

Asegúrese de que nuevas claves de Microsoft no puedan ser instaladas. Esto se hace cambiando atributos al archivo '/etc/apt/trusted.gpg.d/microsoft.gpg', haciéndolo de no-escritura:
~~~bash
sudo chattr +i /etc/apt/trusted.gpg.d/microsoft.gpg
~~~

... y compruébense a continuación los atributos de '/etc/apt/trusted.gpg.d/microsoft.gpg':
~~~bash
sudo lsattr /etc/apt/trusted.gpg.d/microsoft.gpg
~~~
> ----i---------e---- /etc/apt/trusted.gpg.d/microsoft.gpg  

**ℹ**<small> En el remoto caso que se desease devolver a un archivo -por ejemplo a '/etc/apt/trusted.gpg.d/microsoft.gpg'- su atributo de escritura, bastaría con ejecutar: <span style="color:lime">`sudo chattr -i /etc/apt/trusted.gpg.d/microsoft.gpg`</span></small>  

<br>
<br>
**♠** Hágase copia de seguridad de '/etc/apt/sources.list.d/vscode.list'
~~~bash
sudo cp /etc/apt/sources.list.d/vscode.list /etc/apt/sources.list.d/vscode.list.bkp01
~~~

Edítese '/etc/apt/sources.list.d/vscode.list'
~~~bash
sudo nano /etc/apt/sources.list.d/vscode.list
~~~
... y coméntese (añadiendo un '#' al principio) la línea <span style="color:lime">`#deb [arch=amd64,arm64,armhf] http://packages.microsoft.com/repos/code stable main`</span>  

Protéjase contra escritura el archivo '/etc/apt/sources.list.d/vscode.list'
~~~bash
sudo chattr +i /etc/apt/sources.list.d/vscode.list
~~~

Compruébense los atributos del contenido de '/etc/apt/sources.list.d/' (directorio donde se encuentra 'vscode.list'):
~~~bash
sudo lsattr /etc/apt/sources.list.d
~~~
> --------------e---- /etc/apt/sources.list.d/tenaciousd.list  
--------------e---- /etc/apt/sources.list.d/methodman.list  
--------------e---- /etc/apt/sources.list.d/straw.list  
--------------e---- /etc/apt/sources.list.d/openberry.list  
**----i---------e---- /etc/apt/sources.list.d/vscode.list**  
--------------e---- /etc/apt/sources.list.d/straw.list  
--------------e---- /etc/apt/sources.list.d/kangaroo.list  
--------------e---- /etc/apt/sources.list.d/vscode.list.bkp01  

<br>
Efectivamente 'vscode.list' ahora está protegido contra escritura, todo correcto.  

<br>
<br>
**♠** A partir de este momento el repositorio "furtivo" que la Fundación RaspBerryPi incorporó en enero de 2021 sin pedir permiso, habrá quedado anulado.  
Al hacer <span style="color:lime">`sudo apt-get update`</span> no hay rastro de él. Tan sólo aparecerá un mensaje ("Ignoring file 'vscode.list.bkp01' in directory '/etc/apt/sources.list.d/' as it has an invalid filename extension"), inofensivo y ocasionado por la presencia de la copia de seguridad -'vscode.list.bkp01'- que se guardó en '/etc/apt/sources.list.d/'.  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] '/etc/apt/sources.list': repositorios en Debian y derivados](https://hijosdeinit.gitlab.io/howto_repositorios_sources.list_debian_y_derivados/)
- [[HowTo] Agregar repositorio 'debian unstable' a Debian 10 y similares](https://hijosdeinit.gitlab.io/howto_agregar_repositorio_debian_unstable_debian_y_derivados/)
- [[HowTo] Instalación de una versión más actualizada de un paquete desde los repositorios oficiales de Debian: 'backports'](https://hijosdeinit.gitlab.io/howto_instalacion_version_mas_actualizada_desde_repositorios_Debian_backports/)
- [[HowTo] Instalar el comando 'add-apt-repository' en Debian](https://hijosdeinit.gitlab.io/howto_add-apt-repository_en_debian_y_derivados/)
- [[HowTo] Eliminar repositorio agregado a mano ('add-apt-repository') en Debian y derivados](https://hijosdeinit.gitlab.io/howto_eliminar_repositorios_agregados_manualmente_addaptrepository_Debian_derivados/)
- [[HowTo] Solucionar error de GPG cuando la clave pública de un repositorio no está disponible. (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_solucionar_error_gpg_clave_publica_repositorio_Debian_y_derivados/)
- [[HowTo] Eliminar Clave 'Gpg' Cuyo 'Id' Desconocemos. (Debian Y Derivados)](https://hijosdeinit.gitlab.io/howto_eliminar_clave_gpg_id_desconocida_agregada_mediante_apt-key_add_/)
- [El repositorio de firmware / drivers para GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- ['RaspBian' / 'RaspBerry OS' incorpora furtivamente -sin aviso ni permiso- un repositorio de 'Microsoft'. Así no vamos bien](https://hijosdeinit.gitlab.io/repositorio_microsoft_instalado_furtivamente_en_Raspbian_RaspBerryOs/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.cyberciti.biz/linux-news/heads-up-microsoft-repo-secretly-installed-on-all-raspberry-pis-linux-os/">nixCraft</a>  
<a href="https://raspberryparanovatos.com/noticias/repositorios-de-microsoft-raspberry-pi-os/">raspberryparanovatos</a>  
<a href="https://www.cyberciti.biz/faq/apt-get-hold-back-packages-command/">nixCraft - hold packages</a>  
<a href="https://www.cyberciti.biz/faq/linux-write-protecting-a-file/">nixCraft - linux write protecting a file</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
