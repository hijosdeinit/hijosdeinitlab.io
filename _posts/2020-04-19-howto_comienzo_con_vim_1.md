---
layout: post
title: '[HowTo] Comienzo con vim. I'
date: 2020-04-19
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "vi", "vim", "vino", "vinagre", "visudo", "emacs", "micro", "ed", "editor", "editor de textos", "texto", "texto plano", "latex", "pandoc"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Cuando dejé atrás la máquina de escribir Olivetti comencé a utilizar el fabuloso **WordPerfect 5.0** en aquel flamante 80286 Epson AX2E a 12 mhz. Imprimía los resultados con una impresora -matricial, por supuesto- también Epson... modelo LX, si mal no recuerdo.  

**Hoy** cuando he de editar texto utilizo 'nano' o 'gedit'. Estoy encantado con esos editores de texto, que colman sobradamente mis expectativas, de una forma eficaz y eficiente, con un consumo mínimo de recursos... y vienen de serie.  
De vez en cuando escribo con WordPerfect 5.0, emulándolo con DosBox. Pero ya no es lo mismo.  
No obstante, leyendo y escuchando a Atareao me ha picado el gusanillo por aprender a manejar 'Vim', un editor de texto que -junto a Emacs- dicen que tiene una potencia brutal. Escuchar el [podcast de Atareao](https://www.atareao.es/tutorial/vim/) ha contribuído a despertar el gusanillo.  
Cosas como 'vim', 'emacs' o 'latex', 'pandoc' son hoy lo desconocido y, como tal, son atrayentes.  

Sé que 'vi' -el patriarca de 'vim'- y 'visudo' están presentes en mi sistema desde tiempos ancestrales. Sospecho que forman parte de un paquete de nombre 'vino'.  
Mas antes de ponerme en serio con 'vimtutor' (recomendación de Mhyst) y los howtos de Atareao empiezaré por [ver qué es lo que hay "de serie" en mi sistema operativo GNU Linux](https://hijosdeinit.gitlab.io/howto_conocer_que_programas_tenemos_instalados_sistema_gnulinux/) de cabecera, es decir, si 'vim' está o no está.
~~~bash
which vim
~~~
> no hay respuesta: **NO ESTÁ**.  

Tampoco obtengo respuesta afirmativa de los comandos `dpkg -s vim` y `dpkg-query -l vim`.

<br>
Conclusión: Hay que instalar 'vim', que no es otra cosa que 'vi' "mejorado" (Vi iMproved):
~~~bash
whatis vim
~~~
>vim (1)              - Vi IMproved, a programmer's text editor  

<br>
## Instalación de 'vim'
Instalar 'vim' es fácil
~~~bash
sudo apt-get update
sudo apt install vim
~~~

Entradas relacionadas:
- [[HowTo] Comienzo con 'vim', II: descendientes / variantes de 'vi'](https://hijosdeinit.gitlab.io/howto_comienzo_con_vim_2_variantes_de_vim/)
- [Juegos para aprender 'vi': 'vimadventures', 'vimtutor', 'openvim tutorial'](https://hijosdeinit.gitlab.io/vimadventures-vimtutor_juegos_para_aprender_vi/)
- [[HowTo] El editor (IDE) atom y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [[HowTo] Evitar que 'atom' elimine espacios en blanco al final de línea](https://hijosdeinit.gitlab.io/howto_atom_espacios_blanco_final_linea/)
- [[HowTo] Agregar idiomas al editor (IDE) atom. Ponerlo en español](https://hijosdeinit.gitlab.io/howto_poner_atom_en_espa%C3%B1ol/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- [[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)
- [[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados](https://hijosdeinit.gitlab.io/howto_micro_editor_texto_debian/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [[HowTo] Añadir resaltado de sintaxis al editor de textos NANO](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [[HowTo] nano: parámetros de arranque útiles](https://hijosdeinit.gitlab.io/howto_parametros_utiles_arranque_nano/)
- [En nano no existe overtyping](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/)
- [[HowTo] Visualización de 'markdown' en la línea de comandos (CLI): 'MDLESS'](https://hijosdeinit.gitlab.io/howto_mdless_visor_markdown_cli/)
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[maslinux](https://maslinux.es/ver-si-un-paquete-esta-instalado-o-no-en-gnu-linux/)  
[atareao.es](https://www.atareao.es/tutorial/vim/)</small>  
<br>
<br>
<br>
<br>
