---
layout: post
title: "multiplexores de terminal GNU Linux"
date: 2023-06-11
author: Termita
category: "sistemas operativos"
tags: ["getty", "get tty", "tty", "sistemas operativos", "CLI", "linea de comandos", "linux", "GNU linux", "terminal", "gnome-terminal", "tmux", "gnu screen", "terminator", "byobu", "twin"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
zzzz
[tmux](https://github.com/tmux/tmux/wiki)  
[terminator]()  
[gnu screen]()  
[]()  
[]()  





zzzzzzzzzzzzzzzzzzzzzzzz  


Cuando hablamos de la línea de comandos a veces se confunde una ***terminal getty*** (tty1, por ejemplo) con un ***emulador de terminal*** (gnome-terminal, por ejemplo).  

<br>
<br>
«[<big>*getty*</big>](https://www.freebsd.org/cgi/man.cgi?query=getty&sektion=8), abreviatura de "get tty", es un programa Unix que se ejecuta en una computadora host que administra terminales físicos o virtuales (TTY). Cuando detecta una conexión, solicita un nombre de usuario y ejecuta el programa de inicio de sesión para autenticar al usuario.  

Originalmente, en los sistemas Unix tradicionales, getty manejaba conexiones a terminales seriales (a menudo máquinas teletipo) conectadas a una computadora host. La parte tty del nombre significa teletipo, pero ha llegado a significar cualquier tipo de terminal de texto. Un proceso getty provee el servicio a una terminal. En algunos sistemas, por ejemplo, Solaris, getty fue reemplazado por ttymon.

Las computadoras personales que ejecutan sistemas operativos Unix-like, incluso si no proporcionan ningún servicio de inicio de sesión remoto, todavía pueden usar getty como un medio de inicio de sesión en una consola virtual.

En lugar del programa de inicio de sesión, el Administrador de sistemas también puede configurar getty para ejecutar cualquier otro programa, por ejemplo pppd (el demonio de protocolo punto a punto) para proporcionar una conexión a Internet de acceso telefónico.»  

<br>
<br>
«Un <big>emulador de terminal</big> o emulador de consola -como por ejemplo 'gnome-terminal' o 'xterm'- es un programa informático que simula el funcionamiento de un terminal de computadora en cualquier dispositivo de visualización. Normalmente suele estar accesible pulsando CTRL + ALT + T simultáneamente. Una emulador de terminal dentro de una interfaz gráfica de usuario es comúnmente conocida como una ventana de terminal.

Los emuladores de terminal incorporan características tales como control de procesos, redirección de entrada/salida, listado y lectura de ficheros, protección, comunicaciones y un lenguaje de órdenes para escribir secuencias de instrucciones por lotes o (scripts o guiones). Uno de los lenguajes o intérpretes más conocidos, es el Bourne Shell, el cual fue el intérprete usado en las primeras versiones de Unix y se convirtió en un estándar de facto.1​

Al ingresar las órdenes en el emulador, un intérprete de comandos analiza la secuencia de caracteres ingresada y, si la sintaxis de la orden es correcta, la ejecuta, recurriendo para ello a las funciones que ofrece el sistema operativo o el programa que representa, bien sea un gestor de banco de datos, una sesión de FTP, etc. La respuesta al usuario se representa en el monitor, Es una forma de trabajo interactiva, es decir, usuario y máquina se comunican en forma sucesiva.»  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Seleccionar el tipo de arranque en Debian y derivados: alternando entre arranque en entorno gráfico y arranque en terminal pura (tty)](https://hijosdeinit.gitlab.io/howto_arranque_terminal_pura_debian_derivados/)
- ['terminal getty' vs. 'emulador de terminal'](https://hijosdeinit.gitlab.io/getty/)
- [[HowTo] nohup: para que un trabajo iniciado en segundo plano no se cancele "si nos vamos"](https://hijosdeinit.gitlab.io/howto_nohup_nocancelacion_tareas_comandos_cli/)  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/Getty_(Unix)" target="_blank">getty - wikipedia</a>  
<a href="https://es.wikipedia.org/wiki/Emulador_de_terminal" target="_blank">emulador de terminal - wikipedia</a>  
<br>
<br>
<br>
<br>
