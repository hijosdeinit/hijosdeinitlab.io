---
layout: post
title: "Algunos buenos juegos *open source* para el escritorio GNU Linux"
date: 2022-03-12
author: Termita
category: "juegos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "juegos", "software"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>En entradas anteriores de este blog se trató acerca de juegos como ['Angband'](), ['Zangband']() y derivados / similares *roguelike*, y del [listado de juegos para el terminal que Debian pone a disposición de todos](https://blends.debian.org/games/tasks/console).</small>  
<small>Esta entrada tratará de buenos juegos *opensource* para el escritorio GNU Linux.</small>  

<br>

- [Dwarf Fortress](https://www.bay12games.com/dwarves)
- [Oolite](http://www.oolite.org/)
- [Seven Kingdoms](https://www.7kfans.com/wiki/index.php/Download)
- [OpenRA](https://www.openra.net/)
- [Unknown Horizons](http://unknown-horizons.org/)
- [Widelands](https://www.widelands.org/)
- [Pioneer](https://pioneerspacesim.net/#slide0)
- [FreeCiv](http://www.freeciv.org/)
- [FreeCol](http://www.freecol.org/)
- [DCSS](https://crawl.develz.org/)
- [OpenTTD](https://www.openttd.org/)
- [Naev](https://store.steampowered.com/app/598530/Naev/)
- [Endless Sky](https://store.steampowered.com/app/404410/Endless_Sky/)
- [Battle for Wesnoth](https://www.wesnoth.org/)
- [0 A.D](https://play0ad.com/)
- [Micropolis](http://micropolis.mostka.com/)
- [FlightGear](https://www.flightgear.org/)
- [Ryzom](https://ryzom.com/)
- [Megaglest](https://megaglest.org/)
- [Glest]()
- [FreeOrion](https://freeorion.org/index.php/Main_Page)
- [Warzone 2100](https://wz2100.net/)
- [Zero-K]()
- [Bos Wars]()
- [Spring 1944]()
- [netpanzer](https://www.netpanzer.info)
- [lgeneral](https://lgames.sourceforge.io/LGeneral/)  

<br>
<br>
Entradas relacionadas:  
- [Listado oficial de juegos para línea de comandos de Debian](https://hijosdeinit.gitlab.io/listado_oficial_juegos_terminal_cli_debian/)
- [https://hijosdeinit.gitlab.io/tron_online_ssh_terminal/](https://hijosdeinit.gitlab.io/tron_online_ssh_terminal/)
- [zzz](zzz)zangband
- []()angband
- []()minetest
- []()warzone 2100  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
