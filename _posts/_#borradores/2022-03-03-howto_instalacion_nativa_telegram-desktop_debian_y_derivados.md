---
layout: post
title: "[HowTo] instalación NATIVA de 'telegram-desktop' en Debian y derivados"
date: 2022-03-03
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "telegram-desktop", "telegram-cli", "telegram", "mensajeria instantanea", "mensajeria", "mensajes", "nube", "nativo", "apt", "nueva paqueteria", "snap", "flatpak"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
['telegram-desktop'](https://desktop.telegram.org/) es el cliente oficial de 'Telegram Messenger' para el escritorio de GNU Linux.  
<br>
<small>['Telegram'](https://telegram.org/) es una aplicación de mensajería instantánea, generalmente para interfaz gráfico, multiplataforma (GNU Linux, macOs, Windows, IOS, Android) y multiarquitectura (x86/x64, arm)</small>.  

<br>
Los desarrolladores de 'Telegram" ofrecen la aplicación 'telegram-desktop' para GNU Linux de 4 formas:
- snap (nueva paquetería)
- flatpak (nueva paquetería)
- binario oficial (alternativa un tanto escondida)
- desde repositorios oficiales de Debian y derivados, es decir, instalación "nativa" (<small>no obstante, la aplicación instalable desde los repositorios está desactualizada, con todos los problemas que eso conlleva</small>)
















<br>
El proyecto 'telegram-cli' está un poco abandonado: la última actualización data de 2016.  

<br>
<br>
## Instalación de 'telegram-cli' en Debian y derivados.
'telegram-cli' puede ser instalado mediante [nueva paquetería (*snap*)](https://snapcraft.io/install/telegram-cli/debian) [<small>`sudo snap install telegram-cli`</small>] o de forma nativa (compilándolo).  
He preferido la segunda opción: compilarlo e incorporarlo al sistema.  

<br>
Para compilarlo, primero es conveniente asegurarse de que el sistema esté **preparado para compilar software a partir de su código fuente**. En [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_preparar_debian_para_compilar/) se señalan los componentes más habituales para que el sistema compile.  
'telegram-cli' requiere que 'make', entre otros paquetes, esté instalado (<small><span style="background-color:#042206"><span style="color:lime">`sudo apt install make`</span></span></small>).  

<br>
Lo segundo es instalar las **dependencias**, componentes de software imprescindibles para compilar 'telegram-desktop' y, posteriormente, hacerlo funcionar:
~~~bash
sudo apt get update
sudo apt install libreadline-dev libconfig-dev libssl-dev lua5.2
sudo apt install liblua5.2-dev libevent-dev libjansson-dev libpython-dev
~~~
<br>
Para **EVITAR ERRORES** en el momento de la compilación del código fuente de 'telegram-cli' [<small>por ejemplo este: <span style="background-color:#042206"><span style="color:lime">`-Werror=cast-function-type`</span></span></small>] al ejecutar <span style="background-color:#042206"><span style="color:lime">`./configure`</span></span> y/o <span style="background-color:#042206"><span style="color:lime">`make`</span></span> es conveniente instalar también `libgcrypt20-dev`
~~~bash
sudo apt install libgcrypt20-dev
~~~

<br>
Lo tercero: **clonar** en nuestro sistema el repositorio que contiene el código fuente de 'telegram-cli'
~~~bash
cd ~/Programas
git clone --recursive https://github.com/vysheng/tg.git
~~~

<br>
Finalmente, **compilación** del código fuente:  
<br>
"configure" (<small>El parámetro <span style="background-color:#042206"><span style="color:lime">`--disable-openssl`</span></span> evitará un error en la posterior compilación</small>)
~~~bash
cd tg
./configure --disable-openssl
~~~
<br>
... Luego hay que editar el archivo '~/Programas/tg/Makefile' eliminando la cadena <span style="background-color:#042206"><span style="color:lime">'Werror'</span></span>  
~~~bash
nano ~/Programas/tg/Makefile
~~~
Hay que dejar esta línea así:
> COMPILE_FLAGS=${CFLAGS} ${CPFLAGS} ${CPPFLAGS} ${DEFS} -Wall -Werror -Wextra -Wno-missing-field-initializers -Wno-deprecated-declarations -fno-strict-aliasing -fno-omit-frame-pointer -ggdb -Wno-unused-parameter -fPIC  

<br>
Y, para finalizar, "make":
~~~bash
make
~~~

<br>
<br>
## Ejecución de 'telegram-cli'
Desde el subdirectorio, en mi caso, '~/Programas/tg/bin'
~~~bash
./telegram-cli --enable-msg-id -k tg-server.pub
~~~
El parámetro <span style="background-color:#042206"><span style="color:lime">`--enable-msg-id`</span></span> sirve para visualizar en 'telegram-cli' el 'id' de los mensajes y adjuntos.  

<br>
<br>
## Recomendaciones de interés:  
<br>
* Recién ejecutemos 'telegran-cli' conviene cargar la lista de canales y de contactos mediante, respectivamente, los comandos:
~~~
contact_list
dialog_list
~~~

<br>
* El caracter <span style="background-color:#042206"><span style="color:lime">**`#`**</span></span> tiene un tratamiento **especial** en 'telegram-cli', se indica como <span style="background-color:#042206"><span style="color:lime">`_`</span></span>  
Por ejemplo:
Si el nombre de un canal es '#canal', para consultar sus detalles nos deberemos referir a él como '_canal'. Por consiguiente si, por ejemplo, deseamos consultar los detalles de dicho canal, el comando que deberemos ejecutar en 'telegram-cli' es:
~~~
channel_info _canal
~~~

<br>
* El carácter correspondiente a <span style="background-color:#042206"><span style="color:lime">**`espacio`**</span></span>, en la línea de "comandos" de 'telegram-cli' se especifica también como <span style="background-color:#042206"><span style="color:lime">`_`</span></span>. Por consiguiente, para -por ejemplo- visualizar los detalles de un canal llamado 'canal 2':
~~~
channel_info canal_2
~~~ 

<br>
<br>
## Comandos de 'telegram-cli'
[Command / Description](https://github.com/vysheng/tg/wiki/Telegram-CLI-Commands)
~~~
accept_secret_chat <secret chat> 	Accepts secret chat. Only useful with -E option
add_contact <phone> <first name> <last name> 	Tries to add user to contact list
block_user <user> 	Blocks user
broadcast <user>+ <text> 	Sends text to several users at once
channel_get_admins <channel> [limit=100] [offset=0] 	Gets channel admins
channel_get_members <channel> [limit=100] [offset=0] 	Gets channel members
channel_info <channel> 	Prints info about channel (id, members, admin, etc.)
channel_invite <channel> <user> 	Invites user to channel
channel_join <channel> 	Joins to channel
channel_kick <channel> <user> 	Kicks user from channel
channel_leave <channel> 	Leaves from channel
channel_list [limit=100] [offset=0] 	List of last channels
channel_set_about <channel> <about> 	Sets channel about info.
channel_set_admin <channel> <admin> <type> 	Sets channel admin. 0 - not admin, 1 - moderator, 2 - editor
channel_set_username <channel> <username> 	Sets channel username info.
channel_set_photo <channel> <filename> 	Sets channel photo. Photo will be cropped to square
chat_add_user <chat> <user> [msgs-to-forward] 	Adds user to chat. Sends him last msgs-to-forward message from this chat. Default 100
chat_del_user <chat> <user> 	Deletes user from chat
chat_info <chat> 	Prints info about chat (id, members, admin, etc.)
chat_set_photo <chat> <filename> 	Sets chat photo. Photo will be cropped to square
chat_upgrade <chat> 	Upgrades chat to megagroup
chat_with_peer <peer> 	Interface option. All input will be treated as messages to this peer. Type /quit to end this mode
clear 	Clears all data and exits. For debug.
contact_list 	Prints contact list
contact_search username 	Searches user by username
create_channel <name> <about> <user>+ 	Creates channel with users
create_group_chat <name> <user>+ 	Creates group chat with users
create_secret_chat <user> 	Starts creation of secret chat
del_contact <user> 	Deletes contact from contact list
delete_msg <msg-id> 	Deletes message
dialog_list [limit=100] [offset=0] 	List of last conversations
export_card 	Prints card that can be imported by another user with import_card method
export_channel_link 	Prints channel link that can be used to join to channel
export_chat_link 	Prints chat link that can be used to join to chat
fwd <peer> <msg-id>+ 	Forwards message to peer. Forward to secret chats is forbidden
fwd_media <peer> <msg-id> 	Forwards message media to peer. Forward to secret chats is forbidden. Result slightly differs from fwd
get_terms_of_service 	Prints telegram’s terms of service
get_message <msg-id> 	Get message by id
get_self 	Get our user info
help [command] 	Prints this help
history <peer> [limit] [offset] 	Prints messages with this peer (most recent message lower). Also marks messages as read
import_card <card> 	Gets user by card and prints it name. You can then send messages to him as usual
import_chat_link <hash> 	Joins to chat by link
import_channel_link <hash> 	Joins to channel by link
load_audio <msg-id> 	Downloads file to downloads dirs. Prints file name after download end
load_channel_photo <channel> 	Downloads file to downloads dirs. Prints file name after download end
load_chat_photo <chat> 	Downloads file to downloads dirs. Prints file name after download end
load_document <msg-id> 	Downloads file to downloads dirs. Prints file name after download end
load_document_thumb <msg-id> 	Downloads file to downloads dirs. Prints file name after download end
load_file <msg-id> 	Downloads file to downloads dirs. Prints file name after download end
load_file_thumb <msg-id> 	Downloads file to downloads dirs. Prints file name after download end
load_photo <msg-id> 	Downloads file to downloads dirs. Prints file name after download end
load_user_photo <user> 	Downloads file to downloads dirs. Prints file name after download end
load_video <msg-id> 	Downloads file to downloads dirs. Prints file name after download end
load_video_thumb <msg-id> 	Downloads file to downloads dirs. Prints file name after download end
main_session 	Sends updates to this connection (or terminal). Useful only with listening socket
mark_read <peer> 	Marks messages with peer as read
msg <peer> <text> 	Sends text message to peer
msg <peer> <kbd> <text> 	Sends text message to peer with custom kbd
post <peer> <text> 	Sends text message to peer as admin
post_audio <peer> <file> 	Posts audio to peer
post_document <peer> <file> 	Posts document to peer
post_file <peer> <file> 	Sends document to peer
post_location <peer> <latitude> <longitude> 	Sends geo location
post_photo <peer> <file> [caption] 	Sends photo to peer
post_text <peer> <file> 	Sends contents of text file as plain text message
post_video <peer> <file> [caption] 	Sends video to peer
quit 	Quits immediately
rename_channel <channel> <new name> 	Renames channel
rename_chat <chat> <new name> 	Renames chat
rename_contact <user> <first name> <last name> 	Renames contact
reply <msg-id> <text> 	Sends text reply to message
reply_audio <msg-id> <file> 	Sends audio to peer
reply_contact <msg-id> <phone> <first-name> <last-name> 	Sends contact (not necessary telegram user)
reply_document <msg-id> <file> 	Sends document to peer
reply_file <msg-id> <file> 	Sends document to peer
reply_location <msg-id> <latitude> <longitude> 	Sends geo location
reply_photo <msg-id> <file> [caption] 	Sends photo to peer
reply_video <msg-id> <file> 	Sends video to peer
resolve_username username 	Searches user by username
safe_quit 	Waits for all queries to end, then quits
search [peer] [limit] [from] [to] [offset] pattern 	Search for pattern in messages from date from to date to (unixtime) in messages with peer (if peer not present, in all messages)
send_audio <peer> <file> 	Sends audio to peer
send_contact <peer> <phone> <first-name> <last-name> 	Sends contact (not necessary telegram user)
send_document <peer> <file> 	Sends document to peer
send_file <peer> <file> 	Sends document to peer
send_location <peer> <latitude> <longitude> 	Sends geo location
send_photo <peer> <file> [caption] 	Sends photo to peer
send_text <peer> <file> 	Sends contents of text file as plain text message
send_typing <peer> [status] 	Sends typing notification. You can supply a custom status (range 0-10): none, typing, cancel, record video, upload video, record audio, upload audio, upload photo, upload document, geo, choose contact.
send_typing_abort <peer> 	Sends typing notification abort
send_video <peer> <file> [caption] 	Sends video to peer
set <param> <value> 	Sets value of param. Currently available: log_level, debug_verbosity, alarm, msg_num
set_password <hint> 	Sets password
set_profile_name <first-name> <last-name> 	Sets profile name.
set_profile_photo <filename> 	Sets profile photo. Photo will be cropped to square
set_ttl <secret chat> 	Sets secret chat ttl. Client itself ignores ttl
set_username <name> 	Sets username.
set_phone_number <phone> 	Changes the phone number of this account
show_license 	Prints contents of GPL license
start_bot <bot> <chat> <data> 	Adds bot to chat
stats 	For debug purpose
status_online 	Sets status as online
status_offline 	Sets status as offline
unblock_user <user> 	Unblocks user
user_info <user> 	Prints info about user (id, last online, phone)
version 	Prints client and library version
view_audio <msg-id> 	Downloads file to downloads dirs. Then tries to open it with system default action
view_channel_photo <channel> 	Downloads file to downloads dirs. Then tries to open it with system default action
view_chat_photo <chat> 	Downloads file to downloads dirs. Then tries to open it with system default action
view_document <msg-id> 	Downloads file to downloads dirs. Then tries to open it with system default action
view_document_thumb <msg-id> 	Downloads file to downloads dirs. Then tries to open it with system default action
view_file <msg-id> 	Downloads file to downloads dirs. Then tries to open it with system default action
view_file_thumb <msg-id> 	Downloads file to downloads dirs. Then tries to open it with system default action
view_photo <msg-id> 	Downloads file to downloads dirs. Then tries to open it with system default action
view_user_photo <user> 	Downloads file to downloads dirs. Then tries to open it with system default action
view_video <msg-id> 	Downloads file to downloads dirs. Then tries to open it with system default action
view_video_thumb <msg-id> 	Downloads file to downloads dirs. Then tries to open it with system default action
view <msg-id> 	Tries to view message contents
visualize_key <secret chat> 	Prints visualization of encryption key (first 16 bytes sha1 of it in fact)
~~~

<br>
<br>
Entradas relacionadas:
- [[HowTo] Preparar Debian y derivados para compilar software](https://hijosdeinit.gitlab.io/howto_preparar_debian_para_compilar/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://github.com/vysheng/tg/issues/1664">vysheng - solución errores de compilacion telegram-cli</a>  
<a href="https://github.com/vysheng/tg/issues/1256">vysheng - idem</a>  
<a href="https://github.com/freifunk-gluon/gluon/issues/973#issuecomment-265911151">freifunk-gluon - idem</a>  
<a href="https://medium.com/@weibeld/telegram-cli-cheatsheet-9944506eef0b">medium.com - telegram-cli cheatsheet</a>  
<a href="https://github.com/vysheng/tg/wiki/Telegram-CLI-Commands">telegram-cli - comandos</a>  
<a href="https://weibeld.net/misc/telegram-cli-commands.html">weibeld.net - comandos</a>  
<a href="https://snapcraft.io/install/telegram-cli/debian">snapcraft.io - telegram-cli *snap*</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
