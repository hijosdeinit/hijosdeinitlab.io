---
layout: post
title: "Peleándome con Chuwi HeroBook Pro, (vol.01)"
date: 2022-06-15
author: Termita
category: "hardware"
tags: ["hardware", "sonido", "typewriter", "cherry", "teclado", "IBM", "keyboard", "teclas", "pulsación", "bucklespring", "typewriter-sounds", "keypress", "buckling spring", "ibm model f", "ibm model m", "unicomp", "switch", "pulsadores"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---




## Introducción
En el fondo es una tontería prescindible... ciertamente; mas a mí siempre me gustaron las computadoras de IBM. Incluso he tenido la ocasión de tocar alguna -siempre ajena-; desde el primer PC de la historia con su encantadora y diminuta pantalla de fósforo naranja, pasando por un PS2 486, finalizando en un portátil IBM ThinkPad de los de antes, cuando todavía se llamaban IBM. Máquinas, todas ellas, muy robustas, con las que daba gusto trabajar.  
Tengo entendido que aquella división de portátiles de IBM acabó llamándose 'Lenovo' para, años más tarde, ser vendida a los chinos, que mantuvieron el nombre y el apelativo 'ThinkPad'. El hecho de que los nuevos dueños sean chinos no implica que la gama 'ThinkPad' haya perdido toda su esencia; ésta se mantiene, aunque con ciertos cambios.  
Me he dado el gustazo y -más vale tarde que nunca- me he hecho con un ThinkPad "de los de ahora": un [Lenovo **ThinkPad** T470](), más chino que Mao pero que aún transmite ciertas familiares sensaciones, recuerdos de un pasado mejor en el que las máquinas se hacían para durar, para poder ser reparadas. Lamentablemente muchos ThinkPad de hoy -entre ellos el mío- tienen el procesador soldado a la placa base, por poner un ejemplo.  
El teclado, no obstante, es -comparado con lo que hoy otras marcas y modelos venden- excelente, con buen tacto, buen recorrido y buen sonido.  El botoncito rojo, que muchos llaman 'clitoris' está presente, el color negro azabache, las formas cuadradas y la sensación de solidez se mantienen.  

<br>
<br>
Bien, basta de sentimentalismo y vayamos al grano...

<br>
<br>
## Arrancar desde USB, el primer paso para sacar a esta preciosidad de la jaula de Windows 10
El ordenador portátil Lenovo ThinkPad T470 viene con Windows 10 instalado de serie, mas ¿quien no quiere un sistema operativo de verdad en lugar de uno "de juguete"? Hay que instalarle GNU Linux. Y para ello es cuasi vital que el ordenador arranque desde un medio de almacenamiento externo (USB, por ejemplo), cosa que -recién extraído de la caja- no hará si no se deshabilita el parámetro <big>'***fast boot***'</big>, que trae activado por defecto.  
Para deshabilitar '*fast boot*' uno de los procedimientos -al menos el que he seguido yo- es el siguiente:
<br>
- Desde Windows 10: Opciones de energía → comportamiento del botón de encendido: deshabilitese '*fast boot*'.  
- Apáguese la máquina.  
- Conéctese a la máquina el dispositivo de almacenamiento externo que contenga la instalación de GNU Linux.  
- Enciéndase la máquina y -en el caso del ThinkPad T470 y otros muchos- púlsese repetidamente la tecla <big>**F12**</big> y finalmente aparecerá el '**boot menu**', es decir, el menú de arranque que nos permitirá elegir arrancar desde un medio externo -USB, por ejemplo-.  

<small>Si en lugar de 'F12' pulsáramos 'F1', en el ThinkPad T470 y otros se accederá a la configuración BIOS.</small>  

<br>
<br>
<br>
Entradas relacionadas:  
- [Teclados Mecánicos De Hoy. La Herencia Del '*Buckling Spring*'](https://hijosdeinit.gitlab.io/buckling_spring_teclados_mecanicos/)
- []()  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[]()  
[]()</small>  

<br>
<br>
<br>
<br>
