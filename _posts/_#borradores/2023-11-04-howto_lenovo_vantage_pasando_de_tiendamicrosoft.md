---
layout: post
title: "[HowTo] Instalación de Lenovo VANTAGE pasando de Microsoft y su tiendecita"
date: 2023-11-04
author: Termita
category: "hardware"
tags: ["ibm", "exIBM", "lenovo", "ideapad", "ideapad 5", "thinkpad", "t470", "boot menu", "bios", "uefi", "fast boot", "secure boot", "Windows", "Windows 10", "Microsoft Store", "tienda de software", "lenovo vantage", "lenovo commercial vantage", "bloatware", "sistemas operativos", "instalacion"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Un compañero me pasó una computadora portátil marca Lenovo y modelo 'ideapad 5' con la pantalla y bisagras rotas.  

<br>
Lo primero que se me ocurre cuando veo cosas así es que nada de esto es casual. A las corporaciones del hardware -pese a que hacen gala de ecologismo, sostenibilidad y suscribir de pé a pá los objetivos de la agenda veintetreinta- no les interesa hacer productos duraderos (ni reparables, luego hablaremos de eso). Esas y otras empresas hablan el neolenguaje de moda mas su lengua pareciera es de serpiente. A un ingeniero no le resulta difícil diseñar un sistema de apertura y/o un chasis duradero y barato de producir en masa, ergo si en tantísimos modelos de computadoras portátiles el chasis se flexiona que da grima cada vez que se abre la tapa recién sacada la máquina de la caja y las bisagras acaban rebentando es porque, por alguna razón, interesa que eso ocurra.  
Generalmente esa máquina acaba en un cajón o, lo que es peor, en el cubo de la basura.  
A mí me lo han regalado, su dueño ya no lo quiere, ya no es "transportable", la única forma de utilizarlo es conectado a un televisor o pantalla externa. No todo el mundo está dispuesto a estropear la estética de un salón de esa manera ni a sacrificar movilidad, que es para lo que estos chismes en principio son diseñados.  
A mí no me importa la estética demasiado mas sí me preocupa que Lenovo -y sospecho que otros fabricantes- no posibilite acceder a la BIOS (de momento, todo se andará) cuando sus máquinas dependen de una pantalla externa por haberse roto la que trae pegada a la placa del equipo.  

<br>
Y luego está el tema de la reparación y/o actualización. De esto se ha hablado ya bastante, mas no lo suficiente dado el empeño de los fabricantes de equipos en:
- incorporar componentes (memoria RAM, procesador, etc) SOLDADOS y, por tanto, no mejorables fácilmente.
- suministrar recambios de esas piezas que saben que se rompen a precios obscenos. <small>Afortunadamente existe el mercado de piezas de segunda mano y/o no-oficial.</small>  

<br»
Echándole un vistazo a la máquina en cuestión veo que es una computadora que no es sólida, que ha sido diseñada para romperse y para, de haber sobrevivido a las aperturas y cierres de la tapa, resultar en un breve tiempo inútil y/o obsoleta para la mayoria de sus usuarios: se junta el hambre con las ganas de comer (8gb de memoria RAM... y Windows viene de serie). No existe todavía un producto llamado "Windows para equipos viejos", y GNU Linux -que sí se lleva bien con máquinas de pocos recursos y sí permite alargar mucho la vida útil de las computadoras- ni es una alternativa para el común de los mortales ni -cada vez con mayor frecuencia- encuentra el camino allanado para ser instalado en según qué equipos en los que -debido a inventos como las BIOS UEFI, el *secure boot*, el *fast boot* y mil putadas activadas por defecto que cada vez se asemejan más a lo que antaño considerábamos directamente *malware* (software que, entre otras cosas, hace lo que le dá la gana dado que ha sido diseñado para no obedecer al que lo utiliza sino a la empresa que lo creó)- lo que habría de ser una simple tarea se convierte prácticamente en un ejercicio extenuante de prueba-error y consulta de tutoriales por internet que hacen que un simple usuario de *geneúlinux* se sienta casi como un *hacker* / *cracker*.  
 

zzzz
zzzzzzzzzzzzzzzzzzzzzzzz  

Arrancar desde un dispositivo interno -p.ej. un dispositivo USB- es el primer paso para sacar a un ThinkPad T470 de la jaula de Windows 10 en que es entregado preso.  
El ordenador portátil Lenovo ThinkPad T470 viene con Windows 10 instalado de serie, mas ¿quien no quiere un sistema operativo de verdad en lugar de uno "de juguete"? Hay que instalarle GNU Linux. Y para ello no conozco otra manera que aquella en la que el ordenador arranca e instala desde un medio de almacenamiento externo (USB, por ejemplo), cosa que -recién extraído de la caja- no hará si no se deshabilita el parámetro <big>'***fast boot***'</big>, que trae activado por defecto.  
Para deshabilitar '*fast boot*' uno de los procedimientos -al menos el que he seguido yo- es el siguiente:
<br>
- Desde Windows 10: Opciones de energía → comportamiento del botón de encendido: deshabilitese '*fast boot*'.  
- Apáguese la máquina.  
- Conéctese a la máquina el dispositivo de almacenamiento externo que contenga la instalación de GNU Linux.  
- Enciéndase la máquina y -en el caso del ThinkPad T470 y otros muchos- púlsese repetidamente la tecla <big>**F12**</big> y finalmente aparecerá el '**boot menu**', es decir, el menú de arranque que nos permitirá elegir arrancar desde un medio externo -USB, por ejemplo-.  

<small>Si en lugar de 'F12' pulsáramos 'F1', en el ThinkPad T470 y otros se accederá a la configuración BIOS.</small>  

<br>
<br>
<br>
Entradas relacionadas:  
- [ThinkPad T470](https://hijosdeinit.gitlab.io/thinkpad_t470/)
- [[HowTo] Arrancar ThinkPad Desde USB (Peleándome Con un ThinkPad, Vol.01)](https://hijosdeinit.gitlab.io/howto_thinkpad_arranque_usb/)
- [Teclados Mecánicos De Hoy. La Herencia Del '*Buckling Spring*'](https://hijosdeinit.gitlab.io/buckling_spring_teclados_mecanicos/)
- []()  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[]()  
[]()</small>  

<br>
<br>
<br>
<br>
