---
layout: post
title: 'Algunos juegos de simulación espacial'
date: 2022-08-16
author: Termita
category: "juegos"
tags: ["sistemas operativos", "juegos", "simulador", "simulacion", "simulacion espacial", "mods", "codigo abierto", "open source", "opensource", "mmo", "open world", "mundo abierto", "estrategia espacial", "4x", "freelancer", "discovery", "discovery freelancer", "freelancer discovery", "starlancer", "freespace", "privateer", "oolite" "elite", "eve", "eve online", "gnu linux", "linux", "windows"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## Freelancer
Discovery Freelancer  

<br>
## Starlancer

<br>
## FreeSpace

<br>
## Privateer

<br>
## oolite
[web oficial](http://oolite.org/)  
[repositorio oficial en GitHub](https://github.com/OoliteProject/oolite)  
[descripción en wikipedia](https://en.wikipedia.org/wiki/Oolite_(video_game))  

<br>
## Eve
[]()  
[]()  
[]()  
[video de *Eve Online* en linux, 2020](https://yewtu.be/watch?v=WYfP-x7Q358)

<br>
<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
<br>
<br>
---
<small>Fuentes:  
[]()  
[]()  
[]()</small>  
<br>
<br>
<br>
<br>
