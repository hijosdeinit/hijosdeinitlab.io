---
layout: post
title: "[HowTo] Cosas que hacer para mejorar PiHole tras instalarlo"
date: '2020-04-21T11:49:00.004+02:00'
author: Termita
category: "privacidad"
tags: ["bloqueador de publicidad", "regex", "pihole", "linux", "servidor", "sistemas operativos", "blocklist", "malware", "adblock", "ads", "adware", "DNS", "filtro"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
modified_time: '2020-04-21T11:50:14.717+02:00'
blogger_id: tag:blogger.com,1999:blog-1446475121237311525.post-3556690958532874947
blogger_orig_url: https://built4rocknroll.blogspot.com/2020/04/howto-cosas-que-hacer-en-pihole-tras.html
---
## 1. Actualizar PiHole (y otras operaciones desde línea de comandos)
~~~bash
sudo pihole -up
~~~  
<br>
<br>
Comprobar el estado de 'PiHole'
~~~bash
sudo pihole status
~~~
Deshabilitar PiHole
~~~bash
sudo pihole disable
~~~
Habilitar PiHole
~~~bash
sudo pihole enable
~~~

<br>
<br>
## 2. Añadir listas de bloqueo (BlockLists)
Settings → Blocklists  
(*)Agregar una por una aplicando "Save". Y al final pulsar en "Save and Update".  
> 
<small><b>https://raw.githubusercontent.com/StevenBlack/hosts/master/hosts
https://mirror1.malwaredomains.com/files/justdomains
http://sysctl.org/cameleon/hosts
https://s3.amazonaws.com/lists.disconnect.me/simple_tracking.txt
https://s3.amazonaws.com/lists.disconnect.me/simple_ad.txt
~~#https://hosts-file.net/ad_servers.txt~~
~~#https://zeustracker.abuse.ch/blocklist.php?download=domainblocklist~~
~~#https://hosts-file.net/psh.txt~~
https://v.firebog.net/hosts/Airelle-hrsk.txt
https://v.firebog.net/hosts/Shalla-mal.txt
~~#https://ransomwaretracker.abuse.ch/downloads/RW_DOMBL.txt~~
~~#https://ransomwaretracker.abuse.ch/downloads/LY_C2_DOMBL.txt~~
~~#https://ransomwaretracker.abuse.ch/downloads/CW_C2_DOMBL.txt~~
~~#https://ransomwaretracker.abuse.ch/downloads/TC_C2_DOMBL.txt~~
~~#https://ransomwaretracker.abuse.ch/downloads/TL_C2_DOMBL.txt~~
~~#http://www.networksec.org/grabbho/block.txt
https://isc.sans.edu/feeds/suspiciousdomains_Medium.txt
~~#https://raw.githubusercontent.com/crazy-max/WindowsSpyBlocker/master/data/hosts/win10/spy.txt~~
https://v.firebog.net/hosts/static/SamsungSmart.txt
https://v.firebog.net/hosts/Easyprivacy.txt
https://gist.githubusercontent.com/anudeepND/adac7982307fec6ee23605e281a57f1a/raw/5b8582b906a9497624c3f3187a49ebc23a9cf2fb/Test.txt
https://v.firebog.net/hosts/Easylist.txt
https://v.firebog.net/hosts/AdguardDNS.txt
https://raw.githubusercontent.com/mmotti/pihole-regex/master/regex.list
https://raw.githubusercontent.com/Marfjeh/coinhive-block/master/domains
https://github.com/x0uid/SpotifyAdBlock/blob/master/hosts</small></b>  

(*) Las listas que están tachadas están caídas, desatendidas.  
(**) Si accedemos vía navegador a algunas de esas URL -como por ejemplo v.firebog.net- encontraremos más listas cuya URL podemos añadir.  
<br>

## 3. Agregar nuevos **"regex"** a PiHole
Con el objeto fundamentalmente de que no aparezca publicidad en Youtube.  
BlackList → “” → Add (regex)  
> 
<small><b>(^r[[:digit:]]+(.|-+)[[:alnum:]]+-+[[:alnum:]]+-+[[:alnum:]]+.)(googlevideo|gvt1).com$  
^(.+[-.])??adse?rv(er?|ice)?s?[0-9][-.]  
^(.+[-.])??m?ad[sxv]?[0-9][-.]  
^adim(age|g)s?[0-9][-.]  
^adtrack(er|ing)?[0-9][-.]  
^advert(s|is(ing|ements?))?[0-9][-_.]  
^aff(iliat(es?|ion))?[-.]  
^analytics?[-.]  
^banners?[-.]  
^beacons?[0-9][-.]  
^count(ers?)?[0-9][-.] ^pixels?[-.]  
^stat(s|istics)?[0-9][-.]  
^telemetry[-.]  
^track(ers?|ing)?[0-9]*[-.]  
^traff(ic)?[-.]</small></b>  




