--- ---  
Sistemas de respaldo / backup en GNU Linux  
--- ---  
# Consideraciones
**cold* backup* = con el sistema operativo que se va a respaldar **"apagado"**  
**hot* backup* =  con el sistema operativo que se va a respaldar **en marcha**  
<br>
No es recomendable realizar respaldos del sistema operativo en caliente porque se corre el riesgo de que se produzca corrupción en algunos archivos del respaldo. Si aún así desea hacerse es importante elegir el software de respaldo adecuado, el método adecuado y cerrar bases de datos, aplicaciones, archivos, etcétera.  
Un documento que explica muy bien esto es este: []()  

<br>
# Lista de software para realizar respaldos
- [TimeShift - originario, cuasi descontinuado]()
- [TimeShift - *fork* "mint", continuado]()
- [cronopete](https://www.rastersoft.com/programas/cronopete.html) ·[<small>repositorio en github</small](https://gitlab.com/rastersoft/cronopete)
- [restic](https://restic.net/) ·[<small>repositorio en github</small](https://github.com/restic/restic)
- [borg backup](https://www.borgbackup.org/) ·[<small>repositorio en github</small]() | **extra** [+ [vorta (frontend)](https://vorta.borgbase.com)] | **extra** [+ [borg web](https://borgweb.readthedocs.io/en/stable/)] [+ [pika backup (frontend)]()]
- rsync
- [rsnapshot](https://rsnapshot.org/) ·[<small>repositorio en github</small]() [[info](https://unix.stackexchange.com/questions/681311/finding-an-appropriate-backup-strategy-borgbackup-restic-etc/682736#682736)]
- [luckybackup]()
- [fsarchiver](https://www.fsarchiver.org/live-backup/)
- [kopia](https://kopia.io)
- [bacula](https://www.bacula.org/)
- dejadup
- duplicati
- clonezilla
- [foxclone](https://www.foxclone.com/)
- acronis TrueImage
- [AOMEI Backupper](https://www.aomeitech.com/ab/comparison.html)  
- **Otros**  
