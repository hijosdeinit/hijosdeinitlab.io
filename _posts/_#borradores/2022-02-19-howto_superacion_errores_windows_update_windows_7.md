---
layout: post
title: "[HowTo] Extirpación manual de componentes nativos de Windows 10"
date: 2019-06-19
author: Termita
category: "sistemas operativos"
tags: ["hack", "crack", "purgar", "bloatware", "powershell", "windows", "windows 10", "aligerar", "sistemas operativos"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Cuando terminamos de instalar Windows es posible que deseemos aligerarlo desinstalando algunos componentes -de Microsoft y/o de sus asociados- que vienen de serie.  
<br>
<br>
(*) Si, tras eliminarlos, deseáramos **reinstalarlos**:
~~~
Get-AppXPackage | Foreach {Add-AppxPackage -DisableDevelopmentMode -Register "$($_.InstallLocation)\AppXManifest.xml"
~~~

<br>
<br>
En Windows 10 eliminar esos componentes se puede hacer de 3 formas:  
- Mediante aplicaciones específicas.
- Mediante la aplicación de Microsoft <a href="https://www.microsoft.com/es-es/software-download/windows10startfresh?irgwc=1&OCID=AID681541_aff_7791_1246483&tduid=(ir__u000olwubokfrlspkk0sohz30n2xjw3fdkqxpazj00)(7791)(1246483)(%2834ed028759b398bce095fea9b164578c%29%28190947%29%281344120%29%28%29%28%29)(34ed028759b398bce095fea9b164578c)&irclickid=_u000olwubokfrlspkk0sohz30n2xjw3fdkqxpazj00">Refresh Windows Tool</a>, que sirve para borrar todas las aplicaciones del sistema operativo.
- **A mano**  

<br>
<br>
## Desinstalar componentes de Windows 10 a mano
Es necesario abrir 'PowerShell' como administrador.  

<br>
<big>**Listar**</big> componentes:
~~~
Get-AppxPackage
~~~
ô
~~~
Get-AppxPackage | Select Name, PackageFullName
~~~
ô
~~~
Get-AppxPackage | Select Name, PackageFullName >"$env:userprofile\Desktop\Aplicaciones.txt"
~~~

<br>
<big>**Eliminar**</big> componentes:  
`Get-AppxPackage `*nombredelcomponente*` | Remove-AppxPackage`  

<br>
<br>
<br>
## Anexo
<br>
<big>Listado de componentes nativos de Windows 10 "pro":</big>  
<small>
Microsoft.AAD.BrokerPlugin  
Microsoft.Windows.CloudExperienceHost  
Microsoft.Windows.ShellExperienceHost  
windows.immersivecontrolpanel  
Microsoft.MicrosoftEdge  
Microsoft.Windows.Cortana  
Microsoft.Windows.ContentDeliveryManager  
Microsoft.NET.Native.Runtime.1.7  
Microsoft.Windows.SecureAssessmentBrowser  
Microsoft.Windows.XGpuEjectDialog  
Windows.PrintDialog  
Windows.CBSPreview  
Microsoft.XboxGameCallableUI  
Microsoft.Windows.PinningConfirmationDialog  
Microsoft.Windows.PeopleExperienceHost  
Microsoft.Windows.SecHealthUI  
Microsoft.Windows.ParentalControls  
Microsoft.Windows.OOBENetworkConnectionFlow  
Microsoft.Windows.OOBENetworkCaptivePortal  
Microsoft.Windows.NarratorQuickStart  
Microsoft.Windows.CapturePicker  
Microsoft.Windows.AssignedAccessLockApp  
Microsoft.Windows.Apprep.ChxApp  
Microsoft.Win32WebViewHost  
Microsoft.PPIProjection  
Microsoft.AsyncTextService  
Microsoft.AccountsControl  
1527c705-839a-4832-9118-54d4Bd6a0c89  
c5e2524a-ea46-4f67-841f-6a9465d9d515  
E2A4F912-2574-4A75-9BB0-0D023378592B  
F46D4000-FD22-4DB4-AC8E-4E1DDDE828FE  
InputApp  
Microsoft.BioEnrollment  
Microsoft.CredDialogHost  
Microsoft.ECApp  
Microsoft.LockApp  
Microsoft.MicrosoftEdgeDevToolsClient  
Microsoft.VCLibs.140.00  
Microsoft.Wallet  
Microsoft.NET.Native.Runtime.2.2  
Microsoft.NET.Native.Framework.2.2  
Microsoft.XboxGameOverlay  
Microsoft.UI.Xaml.2.0  
Microsoft.VCLibs.140.00.UWPDesktop  
Microsoft.NET.Native.Framework.1.7  
Microsoft.WindowsSoundRecorder  
Microsoft.ScreenSketch  
Microsoft.UI.Xaml.2.1  
Microsoft.People  
Microsoft.WindowsStore  
Microsoft.WebMediaExtensions  
microsoft.windowscommunicationsapps  
Microsoft.VP9VideoExtensions  
Microsoft.Services.Store.Engagement  
Microsoft.StorePurchaseApp  
Microsoft.DesktopAppInstaller  
Microsoft.HEIFImageExtension  
Microsoft.OneConnect  
Microsoft.GetHelp  
Microsoft.WebpImageExtension  
Microsoft.Advertising.Xaml  
</small>  
<br>
*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*
<br>
<small>
Name                                        PackageFullName  
<br>
Microsoft.Windows.CloudExperienceHost  
Microsoft.Windows.CloudExperienceHost_10.0.17763.1_neutral_neutral_cw5n1...  
<br>
Microsoft.AAD.BrokerPlugin  
Microsoft.AAD.BrokerPlugin_1000.17763.1.0_neutral_neutral_cw5n1h2txyewy  
<br>
Microsoft.Windows.ShellExperienceHost  
Microsoft.Windows.ShellExperienceHost_10.0.17763.1_neutral_neutral_cw5n1...  
<br>
windows.immersivecontrolpanel  
windows.immersivecontrolpanel_10.0.2.1000_neutral_neutral_cw5n1h2txyewy  
<br>
Microsoft.Windows.Cortana  
Microsoft.Windows.Cortana_1.11.5.17763_neutral_neutral_cw5n1h2txyewy  
<br>
Microsoft.MicrosoftEdge  
Microsoft.MicrosoftEdge_44.17763.1.0_neutral__8wekyb3d8bbwe  
<br>
Microsoft.Windows.ContentDeliveryManager  
Microsoft.Windows.ContentDeliveryManager_10.0.17763.1_neutral_neutral_cw...  
<br>
Microsoft.NET.Native.Runtime.1.7  
Microsoft.NET.Native.Runtime.1.7_1.7.25531.0_x86__8wekyb3d8bbwe  
<br>
Windows.PrintDialog  
Windows.PrintDialog_6.2.1.0_neutral_neutral_cw5n1h2txyewy  
<br>
Microsoft.XboxGameCallableUI  
Microsoft.XboxGameCallableUI_1000.17763.1.0_neutral_neutral_cw5n1h2txyewy  
<br>
Windows.CBSPreview  
Windows.CBSPreview_10.0.17763.1_neutral_neutral_cw5n1h2txyewy  
<br>
Microsoft.Windows.XGpuEjectDialog  
Microsoft.Windows.XGpuEjectDialog_10.0.17763.1_neutral_neutral_cw5n1h2tx...  
<br>
Microsoft.Windows.SecureAssessmentBrowser  
Microsoft.Windows.SecureAssessmentBrowser_10.0.17763.1_neutral_neutral_c...  
<br>
Microsoft.Windows.SecHealthUI  
Microsoft.Windows.SecHealthUI_10.0.17763.1_neutral__cw5n1h2txyewy  
<br>
Microsoft.Windows.PinningConfirmationDialog  
Microsoft.Windows.PinningConfirmationDialog_1000.17763.1.0_neutral__cw5n...  
<br>
Microsoft.Windows.PeopleExperienceHost  
Microsoft.Windows.PeopleExperienceHost_10.0.17763.1_neutral_neutral_cw5n...  
<br>
Microsoft.Windows.ParentalControls  
Microsoft.Windows.ParentalControls_1000.17763.1.0_neutral_neutral_cw5n1h...  
<br>
Microsoft.Windows.OOBENetworkConnectionFlow  
Microsoft.Windows.OOBENetworkConnectionFlow_10.0.17763.1_neutral__cw5n1h...  
<br>
Microsoft.Win32WebViewHost  
Microsoft.Win32WebViewHost_10.0.17763.1_neutral_neutral_cw5n1h2txyewy  
<br>
Microsoft.Windows.AssignedAccessLockApp  
Microsoft.Windows.AssignedAccessLockApp_1000.17763.1.0_neutral_neutral_c...  
<br>
Microsoft.Windows.Apprep.ChxApp  
Microsoft.Windows.Apprep.ChxApp_1000.17763.1.0_neutral_neutral_cw5n1h2tx...  
<br>
Microsoft.Windows.NarratorQuickStart  
Microsoft.Windows.NarratorQuickStart_10.0.17763.1_neutral_neutral_8wekyb...  
<br>
Microsoft.Windows.OOBENetworkCaptivePortal  
Microsoft.Windows.OOBENetworkCaptivePortal_10.0.17763.1_neutral__cw5n1h2...  
<br>
1527c705-839a-4832-9118-54d4Bd6a0c89  
1527c705-839a-4832-9118-54d4Bd6a0c89_10.0.17763.1_neutral_neutral_cw5n1h...  
<br>
Microsoft.PPIProjection  
Microsoft.PPIProjection_10.0.17763.1_neutral_neutral_cw5n1h2txyewy  
<br>
Microsoft.MicrosoftEdgeDevToolsClient  
Microsoft.MicrosoftEdgeDevToolsClient_1000.17763.1.0_neutral_neutral_8we...  
<br>
Microsoft.AsyncTextService  
Microsoft.AsyncTextService_10.0.17763.1_neutral__8wekyb3d8bbwe  
<br>
Microsoft.LockApp  
Microsoft.LockApp_10.0.17763.1_neutral__cw5n1h2txyewy  
<br>
c5e2524a-ea46-4f67-841f-6a9465d9d515  
c5e2524a-ea46-4f67-841f-6a9465d9d515_10.0.17763.1_neutral_neutral_cw5n1h...  
<br>
E2A4F912-2574-4A75-9BB0-0D023378592B  
E2A4F912-2574-4A75-9BB0-0D023378592B_10.0.17763.1_neutral_neutral_cw5n1h...  
<br>
F46D4000-FD22-4DB4-AC8E-4E1DDDE828FE  
F46D4000-FD22-4DB4-AC8E-4E1DDDE828FE_10.0.17763.1_neutral_neutral_cw5n1h...  
<br>
InputApp  
InputApp_1000.17763.1.0_neutral_neutral_cw5n1h2txyewy  
<br>
Microsoft.AccountsControl  
Microsoft.AccountsControl_10.0.17763.1_neutral__cw5n1h2txyewy  
<br>
Microsoft.Windows.CapturePicker  
Microsoft.Windows.CapturePicker_10.0.17763.1_neutral__cw5n1h2txyewy  
<br>
Microsoft.ECApp  
Microsoft.ECApp_10.0.17763.1_neutral__8wekyb3d8bbwe  
<br>
Microsoft.CredDialogHost  
Microsoft.CredDialogHost_10.0.17763.1_neutral__cw5n1h2txyewy  
<br>
Microsoft.BioEnrollment  
Microsoft.BioEnrollment_10.0.17763.1_neutral__cw5n1h2txyewy  
<br>
Microsoft.Wallet  
Microsoft.Wallet_2.2.18179.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.WebpImageExtension  
Microsoft.WebpImageExtension_1.0.12821.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.VCLibs.140.00  
Microsoft.VCLibs.140.00_14.0.27323.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.NET.Native.Framework.1.7  
Microsoft.NET.Native.Framework.1.7_1.7.27413.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.NET.Native.Runtime.2.2  
Microsoft.NET.Native.Runtime.2.2_2.2.27328.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.NET.Native.Framework.2.2  
Microsoft.NET.Native.Framework.2.2_2.2.27405.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.UI.Xaml.2.0  
Microsoft.UI.Xaml.2.0_2.1810.18004.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.StorePurchaseApp  
Microsoft.StorePurchaseApp_11811.1001.18.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.VCLibs.140.00.UWPDesktop  
Microsoft.VCLibs.140.00.UWPDesktop_14.0.27629.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.GetHelp  
Microsoft.GetHelp_10.1706.20381.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.MSPaint  
Microsoft.MSPaint_5.1904.8017.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.WebMediaExtensions  
Microsoft.WebMediaExtensions_1.0.20875.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.VP9VideoExtensions  
Microsoft.VP9VideoExtensions_1.0.21371.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.WindowsStore  
Microsoft.WindowsStore_11905.1001.4.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.HEIFImageExtension  
Microsoft.HEIFImageExtension_1.0.20982.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.ScreenSketch  
Microsoft.ScreenSketch_10.1901.10521.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.Windows.Photos  
Microsoft.Windows.Photos_2019.19041.16510.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.UI.Xaml.2.1  
Microsoft.UI.Xaml.2.1_2.11906.6001.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.YourPhone  
Microsoft.YourPhone_1.19053.5.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.MicrosoftOfficeHub  
Microsoft.MicrosoftOfficeHub_18.1903.1152.0_x86__8wekyb3d8bbwe  
<br>
Microsoft.Services.Store.Engagement  
Microsoft.Services.Store.Engagement_10.0.19011.0_x86__8wekyb3d8bbwe  
</small>  
<br>
<br>
<br>
<small>
See [Remove default Apps from Windows 10](https://thomas.vanhoutte.be/miniblog/delete-windows-10-apps/)  
See [Debloat Windows 10](https://github.com/W4RH4WK/Debloat-Windows-10)  
Command line to list all packages: `Get-AppxPackage -AllUsers | Select Name, PackageFullName`  
<br>
Get-AppxPackage Microsoft.Windows.ParentalControls | Remove-AppxPackage  
Get-AppxPackage Windows.ContactSupport | Remove-AppxPackage  
Get-AppxPackage Microsoft.Xbox* | Remove-AppxPackage  
Get-AppxPackage microsoft.windowscommunicationsapps | Remove-AppxPackage # Mail and Calendar  
Get-AppxPackage Microsoft.Windows.Photos | Remove-AppxPackage  
Get-AppxPackage Microsoft.WindowsCamera | Remove-AppxPackage  
Get-AppxPackage Microsoft.SkypeApp | Remove-AppxPackage  
Get-AppxPackage Microsoft.Zune* | Remove-AppxPackage  
Get-AppxPackage Microsoft.WindowsPhone | Remove-AppxPackage # Phone Companion  
Get-AppxPackage Microsoft.WindowsMaps | Remove-AppxPackage  
Get-AppxPackage Microsoft.Office.OneNote | Remove-AppxPackage  
Get-AppxPackage Microsoft.Office.Sway | Remove-AppxPackage  
Get-AppxPackage Microsoft.Appconnector | Remove-AppxPackage  
Get-AppxPackage Microsoft.WindowsFeedback* | Remove-AppxPackage  
Get-AppxPackage Microsoft.Windows.FeatureOnDemand.InsiderHub | Remove-AppxPackage  
Get-AppxPackage Microsoft.Windows.Cortana | Remove-AppxPackage  
Get-AppxPackage Microsoft.People | Remove-AppxPackage  
Get-AppxPackage Microsoft.Bing* | Remove-AppxPackage # Money, Sports, News, Finance and Weather  
Get-AppxPackage Microsoft.Getstarted | Remove-AppxPackage  
Get-AppxPackage Microsoft.MicrosoftOfficeHub | Remove-AppxPackage  
Get-AppxPackage Microsoft.MicrosoftSolitaireCollection | Remove-AppxPackage  
Get-AppxPackage Microsoft.WindowsSoundRecorder | Remove-AppxPackage  
Get-AppxPackage Microsoft.3DBuilder | Remove-AppxPackage  
Get-AppxPackage Microsoft.Messaging | Remove-AppxPackage  
Get-AppxPackage Microsoft.CommsPhone | Remove-AppxPackage  
Get-AppxPackage Microsoft.Advertising.Xaml | Remove-AppxPackage  
Get-AppxPackage Microsoft.Windows.SecondaryTileExperience | Remove-AppxPackage  
Get-AppxPackage Microsoft.Windows.ParentalControls | Remove-AppxPackage  
Get-AppxPackage Microsoft.Windows.ContentDeliveryManager | Remove-AppxPackage  
Get-AppxPackage Microsoft.Windows.CloudExperienceHost | Remove-AppxPackage  
Get-AppxPackage Microsoft.Windows.ShellExperienceHost | Remove-AppxPackage  
Get-AppxPackage Microsoft.BioEnrollment | Remove-AppxPackage  
Get-AppxPackage Microsoft.OneConnect | Remove-AppxPackage  
Get-AppxPackage *Twitter* | Remove-AppxPackage  
Get-AppxPackage king.com.CandyCrushSodaSaga | Remove-AppxPackage  
Get-AppxPackage flaregamesGmbH.RoyalRevolt2 | Remove-AppxPackage  
Get-AppxPackage *Netflix | Remove-AppxPackage  
Get-AppxPackage Facebook.Facebook | Remove-AppxPackage  
Get-AppxPackage Microsoft.MinecraftUWP | Remove-AppxPackage  
Get-AppxPackage *MarchofEmpires | Remove-AppxPackage  
</small>
<br>
<br>
<small>
See [How to Completely Uninstall OneDrive in Windows 10](http://lifehacker.com/how-to-completely-uninstall-onedrive-in-windows-10-1725363532)  
`taskkill /f /im OneDrive.exe`  
`C:\Windows\SysWOW64\OneDriveSetup.exe /uninstall`  
</small>

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.frikisdeatar.com/limpiar-windows-10-de-software-innecesario/#Centro-de-Opiniones">FrikisDeAtar</a>  
<a href="https://gist.github.com/tkrotoff/830231489af5c5818b15">tkrotoff en github</a>  
<a href="https://www.genbeta.com/paso-a-paso/como-desinstalar-el-bloatware-y-las-aplicaciones-nativas-no-desinstalables-de-windows-10">GenBeta</a>  
<a href="https://answers.microsoft.com/es-es/windows/forum/apps_windows_10-winapps/windows-10-desinstalar-aplicaciones-nativas/6742f5d2-196e-4dfd-ba9a-e9d7cf46efd5">answers.microsoft.com</a></small>  
<br>
<br>
<br>
<br>
<br>
<br>
