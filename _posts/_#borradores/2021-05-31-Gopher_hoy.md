---
layout: post
title: "'Gopher' HOY"
date: 2021-05-30
author: Termita
category: "redes"
tags: ["sistemas operativos", "linux", "gnu linux", "internet", "navegacion", "web", "protocolo", "http", "gopher", "gemini", "etica", "privacidad", "texto plano"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
EN CONSTRUCCIÓN  

<br>
<br>
Entradas relacionadas:  
- [Protocolo 'GEMINI': retornando a la senda de 'Gopher' en pro de una navegación sin aditivos](https://hijosdeinit.gitlab.io/protocolo_Gemini_retornando_al_camino_Gopher_solucionando_deficiencias_web/)
- [Instalación de 'amfora', navegador gemini para línea de comandos, en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_amphora_gemini_web_browser/)
- [[HowTo] 'bombadillo', navegador gemini para línea de comandos, en Debian y derivados: instalación y primeros pasos](https://hijosdeinit.gitlab.io/howto_bombadillo_gemini_web_browser_debian_y_derivados/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="@@@">@@@</a>  
<a href="@@@">@@@</a>  
<a href="@@@">@@@</a>  
<a href="@@@">@@@</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
