---
layout: post
title: "[HowTo] Instalación de 'transmission-daemon' (torrent *headless*) en RaspBerry Pi (RaspBian y similares)"
date: 2021-02-27
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "transmission", "transmission-cli", "transmission-daemon", "headless", "raspbian", "raspberry pi os", "raspios", "raspi os", "Debian", "directorio", "archivo", "configuracion", "settings.json", "stats.json", "torrent", "resume", "descargas", "blocklists", "p2p", "ed2k", "kad", "amule", "amule-daemon", "demonio", "servicio", "amuleweb", "amulegui", "amulecmd", "open media vault", "omv"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno: RaspBerry Pi 3b, RaspBian Buster, Open Media Vault 5</small>  
¡Ojo! omv 6 vs omv 5  

<br>
<br>
<br>
~~~bash
sudo apt-get update
sudo apt install transmission-daemon
~~~
Compruébese el estado del servicio 'transmission-daemon' que acaba de incorporarse al sistema
~~~bash
sudo systemctl status transmission-daemon.service
~~~
Si todo está más o menos correcto, el sistema podría responder algo similar a esto:
~~~
● transmission-daemon.service - Transmission BitTorrent Daemon
     Loaded: loaded (/lib/systemd/system/transmission-daemon.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2022-05-30 16:01:41 CEST; 3min 54s ago
   Main PID: 813 (transmission-da)
     Status: "Idle."
      Tasks: 4 (limit: 780)
        CPU: 330ms
     CGroup: /system.slice/transmission-daemon.service
             └─813 /usr/bin/transmission-daemon -f --log-error

May 30 16:01:40 rp3b systemd[1]: Starting Transmission BitTorrent Daemon...
May 30 16:01:41 rp3b systemd[1]: Started Transmission BitTorrent Daemon.
May 30 16:01:42 rp3b transmission-daemon[813]: [2022-05-30 16:01:42.586] UDP Failed to set receive buffer: requested 4194304, got 425>
May 30 16:01:42 rp3b transmission-daemon[813]: [2022-05-30 16:01:42.586] UDP Failed to set send buffer: requested 1048576, got 425984>
~~~
> Está funcionando correctamente: está cargado y activo, a la espera.  
> <small>las 2 últimas líneas hacen referencia a los puertos que emplea transmission. Es opcional "adecentar" eso mediante la configuración</small>.  






~~~bash
mkdir -p /srv/dev-disk-by-uuid-5eab39b2-7cf4-48a9-92fa-1be0bcbbe21d/Descargas/completados_torrent
mkdir -p /srv/dev-disk-by-uuid-5eab39b2-7cf4-48a9-92fa-1be0bcbbe21d/Descargas/encurso_torrent
~~~


























En muchas ocasiones para descargar mediante el protocolo 'torrent' basta con instalar el demonio de transmission -'transmission-daemon'- en una máquina para, a continuación, ir añadiéndole las descargas de forma remota (desde otras máquinas). De esta forma transmision trabaja de forma "headless" [<small>[headless](https://techoverflow.net/2019/05/17/what-is-a-headless-program-or-application/): cuando un programa o sistema opera sin un interfaz gráfico</small>] y centralizamos las descargas.  
Esto es muy habitual, por ejemplo, en RaspBerry Pi que actúan como servidor.  

'transmission-daemon', una vez instalado, posee los siguientes archivos y directorios:  
- '/etc/init.d/transmission-daemon' = el script init de 'transmission-daemon'  
- '/var/lib/transmsision-daemon/info/' = directorio vinculado a '~/.config/transmission-daemon/'  
- '/var/lib/transmsision-daemon/info/settings.json' = archivo vinculado a '/etc/transmission-daemon/settings.json'. Es el [archivo de configuración](https://trac.transmissionbt.com/wiki/MovedToGitHub/EditConfigFiles).  
- '/var/lib/transmission-daemon/info/stats.json' =  archivo json que contiene las estadísticas de la sesión.  
- '/var/lib/transmission-daemon/info/torrents/' = directorio que contiene los archivos '.torrent' que han sido añadidos a Transmission.  
- '/var/lib/transmission-daemon/info/resume/' = Este dierctorio contiene los archivos '.resume' que mantienen la información de cada uno de los archivos torrent, tal como qué partes han sido descargadas, el directorio donde se almacenaron los datas descargados, etcétera.  
- '/var/lib/transmission-daemon/info/blocklists/' = Este directorio contiene las [listas de bloqueo](https://trac.transmissionbt.com/wiki/MovedToGitHub/Blocklists).  
- '/var/lib/transmission-daemon/info/dht.dat'  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Incorporar un tema visual más atractivo al interfaz web de 'amule-daemon'](https://hijosdeinit.gitlab.io/howto_incorporar_tema_visual_atractivo_al_interfaz_web_amule/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://trac.transmissionbt.com/wiki/MovedToGitHub/ConfigFiles">trac.transmissionbt.com</a>  
<a href="https://trac.transmissionbt.com/wiki/UnixServer/Debian">trac.transmissionbt.com - debian</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
