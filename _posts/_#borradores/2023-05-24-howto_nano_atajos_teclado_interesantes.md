editor nano: atajos de teclados útiles para mostrar números de línea, espacios en blanco, resaltado de sintaxis, desplazamiento suave, ajuste de linea  

esc s habilita/deshabilita el desplazamiento suave (<small>este atajo está obsoleto en las versiones nuevas del editor nano</small>)  
esc $ habilita/deshabilita·el·ajuste·suave·de·líneas·largas
esc y habilita/dehabilita el resaltado de sintaxis  
esc p muestra/ oculta espacios en blanco  
esc # muestra/ocultalos números de línea  
explorador de ficheros: ctrl + r y, a continuación, ctrl + T  



<br>
<br>
