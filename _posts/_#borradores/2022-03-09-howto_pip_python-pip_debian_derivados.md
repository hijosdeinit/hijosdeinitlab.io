---
layout: post
title: "[HowTo] 'pip' (python-pip) en Debian y derivados"
date: 2022-03-09
author: Termita
category: "programacion"
tags: ["pip", "python-pip", "python-pip3", "python", "programacion", "software", "gestor de paquetes", "sistemas operativos", "gnu linux", "linux", "windows", "macos"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
['pip'](https://pip.pypa.io/) es un sistema de gestión de paquetes escrito en [python](https://www.python.org/) que sirve para instalar y manejar software. Conecta a un repositorio *online* de paquetes públicos llamado [*'Python Package Index'*](https://pypi.org/). No obstante, 'pip' puede ser también configurado para conectar con otros repositorios de paquetes -remotos o locales- siempre que éstos cumplan con el estándar *Python Enhanced Proposal*.  
'pip' fue creado en 2008 bajo el nombre '*pyinstall*' por Ian Bicking como una alternativa para instalar fácilmente paquetes.  

<br>
<br>
## Instalación de 'pip' en Debian 10 *Buster* y derivados
<br>
<small><small>Entorno:
cpu Core2Duo, 6gb RAM, gpu gforce 7600gt, GNU Linux Debian 10 Buster (*netinstall*) stable, kernel 4.19.0-18-amd64, gnome, i3wm</small></small>

<br>
Dependencias:  
Es requisito indispensable que **'python'** esté instalado en la máquina.  
'python 2' suele coexistir con 'python 3' en la misma máquina. Para comprobar sus versiones y, de paso, si están efectivamente instaldos en el sistema:
~~~bash
python --version
python3 --version
~~~

<br>
Existe una versión de 'pip' para cada una de las versiones de 'python' existentes en un sistema. Es decir, si en un sistema coexisten 'python 2' y 'python 3', podrá (y será conveniente) incorporar 'pip 2' ('pip') y 'pip 3' (pip3).
<br>
Existen, al menos 2 formas de instalar 'pip' (y 'pip3') en Debian y derivados  
- a) desde los repositorios oficiales de Debian
- b) mediante el script 'get-pip.py'
- c) mediante el módulo 'ensurepip' de 'python'

<br>
###.A. Instalación desde los repositorios oficiales de debian
~~~bash
sudo apt-get update
~~~
Para instalar 'pip 2'('pip')
~~~bash
sudo apt install python-pip
~~~
Para instalar 'pip3'
~~~bash
sudo apt install python3-pip
~~~

<br>
###.B. Instalación mediante el script 'get-pip.py'
1. Descargar la última versión del script 'get-pip.py' desde la [página oficial](https://bootstrap.pypa.io/).
2. Ejecutar:
Para instalar 'pip' (pip2)
~~~bash
sudo python get-pip.py
~~~
Para instalar 'pip3'
~~~bash
sudo python3 get-pip.py
~~~

<br>
###.C. Instalación mediante el módulo 'ensurepip' de 'python'
Para instalar 'pip' ('pip 2')
~~~bash
python -m ensurepip --upgrade
~~~
Para instalar 'pip3'
~~~bash
python3 -m ensurepip --upgrade
~~~

<br>
<br>
## Utilización de 'pip'
Instalación de un paquete mediante 'pip'
~~~
pip install nombredepaquete
~~~
Desinstalación de un paquete mediante 'pip'
~~~bash

~~~

Let’s say you want to install a package named urllib3, you can do that by issuing the following command:

pip install urllib3

urllib3 is a powerful HTTP client for Python.

Uninstalling a package:

pip uninstall package_name

Searching packages from PyPI:

pip search "search_query"

Listing installed packages:

pip list

Listing outdated packages:

pip list --outdated


<br>
<br>
## Actualización de 'pip'
Para actualizar pipxxxxx
~~~bash
pip install --upgrade pip
~~~
Para actualizar 'pip3'
~~~bash
pip3 install --upgrade pip
~~~

## Downgrade de 'pip'
Puede "desactualizarse" (*downgrade*) a una versión previa de 'pip' o pip3 en el caso de que estemos experimentando errores de compatibilidad inesperados.  
Por ejemplo, para "downgradear" 'pip3' a pip3 v19.0:
~~~bash
python -m pip3 install pip==19.0
~~~






<br>
<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://en.wikipedia.org/wiki/Pip_(package_manager)">wikipedia - pip</a>  
<a href="https://en.wikipedia.org/wiki/Python_Package_Index">wikipedia - *Python Package Index*</a>  
<a href="https://pip.pypa.io/en/stable/installation/">pip.pypa.io - instalación de 'pip'</a>  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
