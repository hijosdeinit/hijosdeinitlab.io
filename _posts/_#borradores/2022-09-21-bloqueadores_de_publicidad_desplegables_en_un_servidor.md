---
layout: post
title: "algunos bloqueadores de publicidad desplegables en tu servidor"
date: 2022-09-21
author: Termita
category: "privacidad"
tags: ["bloqueador de publicidad", "adblock", "regex", "pihole", "adguard", "adguard home", "blocky", "gnu linux", "linux", "servidor", "sistemas operativos", "blocklist", "malware", "adblock", "ads", "adware", "DNS", "filtro"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## PiHole
multiplataforma y multiarquitectura.  
No es necesario instalarlo de forma nativa, también se puede desplegar en el servidor mediante *docker*.  
Podría decirse que es el bloqueador de publicidad "para servidor" más famoso en la comunidad de RaspBerry Pi.  
[Página web oficial de PiHole](https://pi-hole.net/)  
[Repositorio de PiHole en *GitHub*](https://github.com/pi-hole/pi-hole)  

<br>
<a href="/assets/img/2022-09-21-bloqueadores_de_publicidad_desplegables_en_un_servidor/zzzz" target="_blank"><img src="/assets/img/2022-09-21-bloqueadores_de_publicidad_desplegables_en_un_servidor/zzz" width="600"/></a>  
<small>zzzz</small>  

<br>
<br>
<br>
## Blocky
Lo escuché mencionar en [el podcast de UGeek](https://ugeek.github.io/list.html), pendiente de probar en mi servidor.  
[Página web oficial de Blocky](https://0xerr0r.github.io/blocky/)  
[Repositorio de Blocky en *GitHub*](https://github.com/0xERR0R/blocky)  


<br>
<a href="/assets/img/2022-09-21-bloqueadores_de_publicidad_desplegables_en_un_servidor/zzzz" target="_blank"><img src="/assets/img/2022-09-21-bloqueadores_de_publicidad_desplegables_en_un_servidor/zzz" width="600"/></a>  
<small>zzzz</small>  

<br>
<br>
<br>
## AdGuard Home
Pendiente de probar en mi servidor.   
[Página oficial de AdGuard Home](https://adguard.com/es/adguard-home/overview.html)  
[Repositorio de AdGuard Home en *GitHub*](https://github.com/AdguardTeam/AdguardHome)  

<br>
<a href="/assets/img/2022-09-21-bloqueadores_de_publicidad_desplegables_en_un_servidor/zzzz" target="_blank"><img src="/assets/img/2022-09-21-bloqueadores_de_publicidad_desplegables_en_un_servidor/zzz" width="600"/></a>  
<small>zzzz</small>  



<br>
<br>
Entradas relacionadas:  
- []()
- []()
- []()  

<br>
<br>
<br>

--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/Pi-hole">PiHole - Wikipedia</a>  
<a href=""></a>  
<a href=""></a>  
<a href=""></a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
