---
layout: post
title: "[HowTo] persistencia en 'Ubuntu Web Remix" *liveusb*
date: 2021-06-15
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "Debian", "liveusb", "livecd", "persistente", "dd", "iso", "boot"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---

https://ubuntu-web.org/download/

https://discourse.ubuntu.com/t/ubuntu-web-remix/19394


http://linux.darkpenguin.net/distros/ubuntu-unity/ubuntu-web/20.04.4/firefox/
http://linux.darkpenguin.net/distros/ubuntu-unity/ubuntu-web/20.04.4/brave/








EN CONSTRUCCION

¿Para qué sirve un liveusb?  
los sistemas operativos GNU Linux de tipo</small> ***LIVE***<small>: *LiveCD*, *LiveDVD*, *LiveUSB*.</small>  
<small>Este tipo de sistemas operativos son prácticamente "universales" (funcionan en casi cualquier máquina de una arquitectura compatible), no requieren ser instalados y se ejecutan sin dejar huella en la máquina que los ejecute. Suelen ser muy útiles para probar un sistema operativo GNU Linux antes de instalarlo, como medio de instalación, y también como herramienta de emergencia o forense. No son sistemas ideados para trabajar de forma constante y habitual.  

Un sistema operativo "Live persistente" está desplegado generalmente en un dispositivo de almacenamiento USB y, al igual que sus "padres" -livecd, livedvd o liveusb no persistentes-, pueden ser ejecutados en cualquier ordenador siempre que la arquitectura de éste le corresponda. Sin embargo, a diferencia de aquéllos, un sistema operativo GNU Linux "Live" persistente -dado que se halla desplegado en un dispositivo de almacenamiento "escribible" y "reescribible"- conserva el trabajo realizado por el usuario: sus archivos nuevos, sus configuraciones y reconfiguraciones, sus nuevos programas instalados, etcétera. Generalmente hay en el dispositivo una partición donde se "guarda" la persistencia.  
Un sistema operativo GNU Linux Live persistente -a diferencia de livecd, livedvd y liveusb no persistente- podría servir, no como sistema de emergencia, de prueba o de instalación, sino como sistema para trabajar habitualmente.  



Hay quien emplea 'rufus'  
pero a mi no me funciona con una de las máquinas  
Otros emplean mkusb  

~~~bash
wget http://debian-cd.debian.net/debian-cd/10.9.0-live/amd64/iso-hybrid/debian-live-10.9.0-amd64-mate.iso
~~~

~~~bash
sha256sum debian-live-10.9.0-amd64-mate.iso > debian-live-10.9.0-amd64-mate.iso.sha256.txt
~~~

`LANG=C sed 's/splash quiet/persistence /;s/quiet splash/persistence /' </home/nervic/Descargas/construccion_liveusb_debian10.9.0.mate/debian-live-10.9.0-amd64-mate.iso >/home/nervic/Descargas/construccion_liveusb_debian10.9.0.mate/debian-live-10.9.0-amd64-mate-persist.iso`  

ô lo que es igual, orden por orden:  
`LANG=C sed 's/splash quiet/persistence /;s/quiet splash/persistence /' \`  
`</home/nervic/Descargas/construccion_liveusb_debian10.9.0.mate/debian-live-10.9.0-amd64-mate.iso \`  
`>/home/nervic/Descargas/construccion_liveusb_debian10.9.0.mate/debian-live-10.9.0-amd64-mate-persist.iso`  

<br>
`sudo dd if=/home/nervic/Descargas/construccion_liveusb_debian10.9.0.mate/debian-live-10.9.0-amd64-mate-persist.iso of=/dev/sdx conv=fsync status=progress`  

<br>
~~~bash
sudo fdisk -l /dev/sdx
~~~
~~~
Disco /dev/sdf: 7,5 GiB, 8019509248 bytes, 15663104 sectores
Modelo de disco: USB DISK 2.0    
Unidades: sectores de 1 * 512 = 512 bytes
Tamaño de sector (lógico/físico): 512 bytes / 512 bytes
Tamaño de E/S (mínimo/óptimo): 512 bytes / 512 bytes
Tipo de etiqueta de disco: dos
Identificador del disco: 0x23f26e04

Disposit.  Inicio Comienzo   Final Sectores Tamaño Id Tipo
/dev/sdx1  *             0 5054239  5054240   2,4G  0 Vacía
/dev/sdx2             1604    7555     5952   2,9M ef EFI (FAT-12/16/32)
~~~

~~~bash
sudo fdisk /dev/sdx
~~~
~~~
n           # new partition
p           # primary
<Return>    # default: 3
<Return>    # default: next free sector
<Return>    # default: last addressable sector
w           # write and quit
~~~

~~~bash
sudo fdisk -l /dev/sdx
~~~
~~~
Disco /dev/sdf: 7,5 GiB, 8019509248 bytes, 15663104 sectores
Modelo de disco: USB DISK 2.0    
Unidades: sectores de 1 * 512 = 512 bytes
Tamaño de sector (lógico/físico): 512 bytes / 512 bytes
Tamaño de E/S (mínimo/óptimo): 512 bytes / 512 bytes
Tipo de etiqueta de disco: dos
Identificador del disco: 0x23f26e04

Disposit.  Inicio Comienzo    Final Sectores Tamaño Id Tipo
/dev/sdf1  *             0  5054239  5054240   2,4G  0 Vacía
/dev/sdf2             1604     7555     5952   2,9M ef EFI (FAT-12/16/32)
/dev/sdf3          5054464 15663103 10608640   5,1G 83 Linux
~~~

~~~bash
sudo mkfs.ext4 -L persistence /dev/sdx3
~~~
~~~
mke2fs 1.44.5 (15-Dec-2018)
Creating filesystem with 1326080 4k blocks and 331936 inodes
Filesystem UUID: af4c0735-ff95-413f-aa83-6e9aa7d43add
Superblock backups stored on blocks: 
	32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocating group tables: done                            
Writing inode tables: done                            
Creating journal (16384 blocks): done
Writing superblocks and filesystem accounting information: done
~~~

~~~bash
sudo mount /dev/sdx3 /mnt
~~~

~~~bash
sudo su
echo '/ union' >/mnt/persistence.conf
exit
~~~

~~~bash
sync
sudo sync
~~~

~~~bash
sudo umount /mnt
~~~








<br>
<br>

Entradas relacionadas:  
- []()  

<br>
<br>
<br>

--- --- ---  
<small>Fuentes:  
<a href=""></a>  
<a href=""></a>  
<a href=""></a>  
<a href=""></a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
