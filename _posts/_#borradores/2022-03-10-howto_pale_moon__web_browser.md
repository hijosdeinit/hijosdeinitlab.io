---
layout: post
title: "[HowTo] Incorporar el navegador web 'Pale Moon' en Debian y derivados"
date: 2022-03-10
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "ubuntu", "web browser", "navegador", "internet", "internet 4.0", "elza", "elza browser", "palemoon", "palemoon browser", "min browser", "min", "min browser v.1.20", "chrome", "chromium", "edge", "firefox", "waterfox", "librefox", "librewolf", "icecat", "iceweasel", "basilisk", "borealis", "privacidad", "anonimato", "respeto"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## ¿Qué es 'Pale Moon' browser?








zzz zzz zzz zzz zzz

## ¿Qué es 'Basilisk'?
Es un navegador de código abierto alternativo a Mozilla Firefox.  
En los últimos tiempos -dado que Mozilla Firefox subsiste económicamente gracias a Google ("dime quién te paga y te diré a qué y a quién beneficias") y a [cierta mala praxis]() (configuraciones por defecto poco acordes con la privacidad)- cada vez más usuarios buscan alternativas más respetuososas.  
'Basilisk' está desarrollado por el equipo ['Pale Moon'](), propulsores asimismo de otro navegador, del mismo nombre: [Pale Moon browser](). El motor de ambos navegadores -'Basilisk' y 'Pale Moon'- se llama ['Goanna']() y es un clon de ['Gecko'](), el motor de Mozilla Firefox.
@@@@@@@@@@


El navegador 'basilisk' sólo está desarrollado para 64bits, no hay versiones para 32bits.  

## Incorporación de 'basilisk' al sistema
No he descubierto la forma nativa (`apt`) de instalar 'basilisk' en Debian y derivados.  
Básicamente la forma de incorporar 'basilisk' a este mi sistema GNU Linux -Ubuntu 18 *bionic*- es descargando el binario en el subdirectorio '~/_#Programas' y ejecutándolo desde allí.  
~~~bash
cd ~/Descargas
wget 'https://eu.basilisk-browser.org/release/basilisk-latest.linux64.tar.xz'
~~~
A continuación hay que Descomprimir 'basilisk-latest.linux64.tar.xz' en '~/_#Programas'  

<br>
## Ejecución del binario de 'basilisk'
Para ejecutarlo basta con hacer doble click en '~/_#Programas/basilisk/basilisk'  

<br>
<a href="/assets/img/2022-01-04-howto_basilisk_ubuntu_18_bionic/basilisk.png" target="_blank"><img src="/assets/img/2022-01-04-howto_basilisk_ubuntu_18_bionic/basilisk.png" alt="el navegador web 'Basilisk'" width="300"/></a>  
<small>El navegador web 'Basilisk'</small>  
<br>

<br>
## Postinstalación
- revisar ajustes (privacidad, etcétera...)
- (opcional) crear lanzador, añadir extensiones, etc.  

<br>
EN CONSTRUCCIÓN  
<br>


 ## ¿Qué es 'Basilisk'?                                                                                               21 Es un navegador de código abierto alternativo a Mozilla Firefox.                                                     22 En los últimos tiempos -dado que Mozilla Firefox subsiste económicamente gracias a Google ("dime quién te paga y te     diré a qué y a quién beneficias") y a [cierta mala praxis]() (configuraciones por defecto poco acordes con la privac    idad)- cada vez más usuarios buscan alternativas más respetuososas.                                                  23 'Basilisk' está desarrollado por el equipo ['Pale Moon'](), propulsores asimismo de otro navegador, del mismoConnection to 192.168.0.11 closed by remote host.mbos navegadores -'Basilisk' y 'Pale Moon' se llama ['Goanna']() y es un clon
Connection to 192.168.0.11 closed.zilla Firefox.
PS C:\Users\vicza>                                                                                                       25                                                                                                                      26                                                                                                                      27 El navegador 'basilisk' sólo está desarrollado para 64bits, no hay versiones para 32bits.


http://www.moonchildproductions.info/goanna.shtml

Goanna is an open-source browser engine that is a fork of Mozilla 's Gecko. It is used in the Pale Moon browser, the Basilisk browser, and other UXP -based applications. Other uses include a fork of the K-Meleon browser and the Mypal browser for Windows XP. Goanna as an independent fork of Gecko was first released in January 2016.


https://www.softzone.es/programas/navegadores/basilisk/

















'LibreWolf' [1](https://librewolf-community.gitlab.io/) [2](https://librewolf.net) es un fork del navegador web Firefox.  
Los creadores de 'LibreWolf' lo describen como «enfocado en la privacidad, la seguridad y la libertad», en contraposición al [en ocasiones polémico 'Mozilla Firefox'](https://www.scss.tcd.ie/Doug.Leith/pubs/browser_privacy.pdf) cuyo código modifican.  
'LibreWolf' asegura ofrecer:
- Ausencia de Telemetría: «sin "experimentos", sin *adware*, sin molestias o distracciones innecesarias».
- Sin "sincronización en la nube (*cloud sync*) mediante cuenta en Firefox".
- Incorpora proveedores privados de búsquedas: 'Searx', 'Qwant', 'DuckDuckGo' (configurado por defecto), etc...
- Incorpora 'ublock origin', el bloqueador de scripts y anuncios.
- Sin 'Pocket' (sin botón "añadir a Pocket").
- Sin contenido esponsorizado / recomendado en la página de inicio.
- Los *snippets* de Firefox para añadir noticias / tips en una nueva pestaña han sido eliminados de la configuración.
- Sin accesos directos esponsorizados.
- Protección frente a *tracking* establecida por defecto en modo 'estricto'.
- Configurado por defecto para que *Cookies* e historial sean eliminados al cerrar el navegador.
- Configurado por defecto en modo 'sólo https'.  

<br>
<br>
## Instalación de 'librewolf' en Debian 10 *buster*
Los métodos de instalación nativa que [se describen en la página oficial de 'librewolf'](https://librewolf.net/installation/debian/) no me han servido en Ubuntu 18 *bionic*; quizás se debe a que están descrito para versiones más modernas de Debian o Ubuntu.  
Por consiguiente he recurrido a la "nueva paquetería", concretamente a 'appimage'.  

Tengo por costumbre ubicar las 'appimage' en '/home/miusuario/_#Programas/'  
~~~bash
mkdir ~/_#Programas/LibreWolf
cd ~/_#Programas/LibreWolf
wget https://gitlab.com/api/v4/projects/24386000/packages/generic/librewolf/95.0.2-1/LibreWolf.x86_64.AppImage
sudo chmod +x LibreWolf.x86_64.AppImage
~~~

<br>
## Ejecución de la *appimage* de 'librewolf'
Para ejecutar 'librewolf':
Desde el subdirectorio donde se ha descargado la 'appimage' de 'librewolf' (`cd /home/miusuario/_#Programas/`)
~~~bash
./LibreWolf-89.0.2-1.x86_64.AppImage
~~~
... o bien, desde el "explorador de archivos" (nautilus, etc...) bastará con hacer 'doble click' sobre el archivo 'LibreWolf-89.0.2-1.x86_64.AppImage'.  

## Postinstalación
Aunque de serie 'librewolf' está configurado óptimamente para la privacidad, reviso la configuración de 'librewolf'.  
Activo 'historial' en los ajustes de 'librewolf', condición necesaria para que "recuerde" la sesión en curso.  

<br>
<br>
Entradas relacionadas:  
- [¿Harto de Firefox? Alternativas: WaterFox, LibreFox, LibreWolf](https://hijosdeinit.gitlab.io/WaterFox_LibreFox_LibreWolf_BadWolf/)
- [[HowTo] Instalación NATIVA de 'epiphany' ('gnome-web') en Ubuntu y familia](https://hijosdeinit.gitlab.io/howto_instalacion_nativa_epiphany_browser_ubuntu/)
- [[HowTo] Instalación de MicroSoft Edge (dev) en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalar_ms_edge_chromium_dev_en_debian_derivados/)
- ["Instalación de 'amfora', navegador gemini para línea de comandos, en Debian y derivados"](https://hijosdeinit.gitlab.io/howto_instalacion_amphora_gemini_web_browser/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
