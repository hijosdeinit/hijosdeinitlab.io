---
layout: post
title: "servidores web, vol.01: un pequeño listado"
date: 2022-02-13
author: Termita
category: "redes"
tags: ["redes", "sistemas operativos", "gnu linux", "linux", "windows", "macos", "Debian", "web server", "web", "gemini", "gopher", "apache", "cherokee", "nginx", "lighttpd"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
¿Qué es la web (*world wide web*)?

¿Qué es un servidor web?

<br>
<a href="/assets/img/" target="_blank"><img src="/assets/img/" alt="zzz" width="600"/></a>  
<small>zzz</small>  
<br>


<br>
<br>
<br>
Estos son los que conozco:  
Apache HTTP Server
Nginx
Microsoft IIS
Caddy Server
lighttpd
cherokee

<br>
<br>
<br>
Estos son otros que aún no he probado





<small><small>entorno: RaspBerry Pi 4b 4gb RAM, SD y HDD usb. RaspBian Buster 32bits.</small></small>  

<br>
<br>
<small>[Esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_amule_como_demonio_servicio_vol01/) explicaba la forma de instalar, configurar y ejecutar ['amule'](http://www.amule.org/) como servicio / demonio.</small>  

<br>
Generalmente suelen compartirse los subdirectorios <span style="background-color:#042206"><span style="color:lime">'/home/.aMule/Incoming'</span></span> y <span style="background-color:#042206"><span style="color:lime">'/home/amule/.aMule/Temp'</span></span> y así suele aparecer especificado en el archivo de configuración de 'amule' ('/home/amule/.aMule/amule.conf'). No obstante, como es mi caso, establecí otros directorios diferentes como destino de las descargas completadas y las descargas en curso.  
Si hubiese instalado 'amule' de una forma convencional, con su interfaz gráfico, mediante éste podrían especificarse los subdirectorios extra para compartir. Sin embargo, cuando 'amule' es instalado tan sólo como servicio o demonio (<small>*headless* creo que se dice</small>), el interfaz gráfico de las máquinas cliente -web y/o 'amulegui'- que a él se conectan para controlarlo no permite agregar gráfica y recursivamente directorios adicionales a compartir.  

<br>
<br>
## ¿Como añadir directorios extra para compartir mediante el demonio de 'amule'?
Los directorios a compartir se especifican en 2 archivos de configuración, propiedad -en mi caso- del usuario 'amule':
- '/home/amule/.aMule/amule.conf' : En ese archivo de configuración se especifica la ubicación de los directorios 'Incoming' (donde se almacena lo descargado) y 'Temp' (donde se almacena lo que se está descargando). El contenido de esos dos directorios -lo descargado y lo que se está descargando- es lo que se comparte por defecto.
- <big><span style="background-color:#042206"><span style="color:lime">**'/home/amule/.aMule/shareddir.dat'**</span></span></big> : En ese archivo de configuración se especifica la ruta de los directorios "extra" cuyo contenido será compartido por toda la red 'ed2k' y/o 'kad'.  
Para agregar un 'directorio a compartir' hay que:
- PARAR primero el demonio de 'amule' (<span style="background-color:#042206"><span style="color:lime">`sudo systemctl stop amule-daemon.service`</span></span>).  
- A continuación edítese ese archivo -'shareddir.dat'- y agréguese la ruta de dicho directorio.  
- Una vez guardados los cambios, bastará con volver a iniciar el demonio de 'amule' ejecutando <span style="background-color:#042206"><span style="color:lime">`sudo systemctl start amule-daemon.service`</span></span>  

<br>
**!** **Atención**, los directorios así agregados NO son **recursivos**, es decir sus subdirectorios no se compartirán a no ser que a su vez éstos se especifiquen -uno a uno- en el fichero 'shareddir.dat'.  

<br>
Para hacer esta labor menos tediosa se puede aprovechar la potencia de la línea de comandos de GNU Linux y hacer que la ruta de cada uno de los directorios y subdirectorios que deseamos compartir se agregue de forma automática, cuasi mágica.  
Por ejemplo, si deseamos compartir el directorio '/home/tuusuario/Descargas' y todos los archivos y subdirectorios que contiene, con esta orden
~~~bash
find /home/tuusuario/Descargas -type d -print >> /home/amule/.aMule/shareddir.dat
~~~
... se añadirán automáticamente la ruta de ese directorio y la de cada uno de los subdirectorios que contuviese.

<br>
<br>
Entradas relacionadas:  
- [[HowTo] 'amule' como servicio / demonio. vol.01](https://hijosdeinit.gitlab.io/howto_amule_como_demonio_servicio_vol01/)
- [[HowTo] directorios y archivos de transmission-daemon en Debian y derivados](https://hijosdeinit.gitlab.io/howto_directorios_de_transmission_cli_en_debian_y_derivados/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://forum.amule.org/index.php?topic=16702.0">forum.amule.org</a>  
<a href="https://wiki.amule.org/wiki/Shareddir.dat_file">wiki.amule.org - shareddir.dat</a>  
<a href="https://wiki.amule.org/wiki/Shared_directory">wiki.amule.org - shared directory</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
