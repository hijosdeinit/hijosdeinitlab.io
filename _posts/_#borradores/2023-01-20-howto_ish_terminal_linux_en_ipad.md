---
layout: post
title: "[HowTo] terminal linux en ipad mediante 'ish'"
date: 2023-01-20
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "apple", "ipad", "iphone", "ipod", "itunes", "apps", "obsolescencia", "derecho a reparar", "apple store", "ios", "macOS", "windows", "hack", "crack", "retromatica", "lonchafinismo", "ish", "tty", "terminal", "gnu linux", "linux", "ssh"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>ipad zzz</small>  

<br>
<br>
[ish]() es zzzz  






<small>En este mundo posmoderno cuando compramos una máquina no somos conscientes de que en realidad lo que estamos haciendo es pagar un dinero a cambio de usar una máquina durante un periodo de tiempo muy pequeño: apenas 5 años en muchas ocasiones.  
Esto es más grave aún en el caso de máquinas caras, como son los productos de Apple.  
La obsolescencia programada en máquinas que han sido diseñadas para durar -no estropearse- pero para quedarse obsoletas al cabo de unos años, es la práctica común y exagerada hoy día. Antaño existía, mas no a los niveles que vemos hoy, cuando además éstas impiden el "derecho a reparar" sus máquinas.  
Nos enfrentamos, pues, a una obsolescencia de las máquinas:
... a nivel ***software***: pasados unos años el sistema operativo, las aplicaciones, el soporte, etc... dejan de funcionar y/o de tener soporte.
... a nivel ***hardware***: dificultad, por no decir, imposibilidad de reparar, actualizar, mejorar, acceder a recambios, etcétera.
Quizás si todos fuéramos conscientes de todo esto en todo momento no compraríamos tan a la ligera esas bonitas máquinas que, al cabo de menos de una década -aún funcionando correctamente- nos van a *dejar tirados* sobre todo a nivel software.</small>  

<br>
<a href="/assets/img/2019-06-23-howto_mantener_vivo_ipad_antiguo_vol01__instalar_aplicaciones_no_tenidas_antes/ipad_cronologia.png" target="_blank"><img src="/assets/img/2019-06-23-howto_mantener_vivo_ipad_antiguo_vol01__instalar_aplicaciones_no_tenidas_antes/ipad_cronologia.png" alt="cronología de los diferentes modelos de 'iPad'" width="600"/></a>  
<small>cronología de los diferentes modelos de 'iPad'</small>  
<br>

<br>
<br>
Los iPad viejos ya no pueden ser actualizados a las últimas versiones de IOS. Esto hace que:
1. Desde la App Store del propio Ipad haya actualizaciones de aplicaciones que tenemos instaladas que no puedan llevarse a cabo por ser incompatibles con nuestra versión de IOS
2. Desde la App Store del propio Ipad no se puedan instalar aplicaciones que no hayamos instalado antes, cuando nuestro ipad aún no estaba lo suficientemente "anticuado".

Por otra parte, las últimas versiones de iTunes para Windows o macOS -a partir de iTunes 12.7, incluído- no tienen acceso a la App Store, los tonos de llamada y otras funciones. Esto implica que con esas últimas versiones -12.7 en adelante- no se pueden instalar aplicaciones en un iPad.  

SIN EMBARGO, Apple mantiene una versión de iTunes -la 12.6.5.3- que SÍ tiene acceso a la App Store y otras funciones. Esta versión, que existe para Windows y para macOS (no es compatible con Mojave), se puede descargar desde la [página oficial de soporte de Apple](https://support.apple.com/es-es/HT208079):
- itunes 12.6.5.3 para Windows 32 bits    [descarga](https://secure-appldnld.apple.com/itunes12/091-87820-20180912-69177170-B085-11E8-B6AB-C1D03409AD2A5/iTunesSetup.exe)    mirror
- iTunes 12.6.5.3 para Windows 64 bits    [descarga](https://secure-appldnld.apple.com/itunes12/091-87819-20180912-69177170-B085-11E8-B6AB-C1D03409AD2A6/iTunes64Setup.exe)    mirror
- iTunes 12.6.5.3 para macOS    [descarga](https://secure-appldnld.apple.com/itunes12/091-87821-20180912-69177170-B085-11E8-B6AB-C1D03409AD2A/iTunes12.6.5.dmg)    mirror  

<br>
<br>
## PROCEDIMIENTO PARA INSTALAR APLICACIONES QUE NO HAYAMOS TENIDO ANTES DESDE ITUNES 12.6.5.3
<small>Quede claro que, a día de hoy, hay aplicaciones -como 'YouTube' o 'Google Drive'- que, aunque mediante este procedimiento las instalemos, no funcionarán. Esto es debido, entre otras cosas a incompatibilidad de la última versión apta para nuestro IOS con los "servidores", cuyo código ha cambiado sin que se haya buscado, intencionadamente o no, la retrocompatibilidad.  </small>
<br>
El procedimiento (<small>materiales: computadora, cable de datos del iPad y conexión a internet</small>), no obstante, es el siguiente:  

<big>***1.**</big> Arránquese iTunes 12.6.5.3

<big>***2.**</big> Conéctese el iPad y **autorícese** al ordenador si fuere necesario

<big>***3.**</big> En iTunes del ordenador, click en "App Store" y buscamos la app que deseamos instalar. Click en "Descargar".  

<small>**(*)** Como ejemplo pongo la app de Netflix, no porque recomiende su uso o suscribirse a ese servicio y llenarle los bolsillos a sus amos a costa de nuestra vagancia, sino porque es la típica 'app' que actualmente no podemos instalar **por primera vez** en dispositivos Apple antiguos (iOS 9, p.ej.)</small>  

<br>
<a href="/assets/img/2019-06-23-howto_mantener_vivo_ipad_antiguo_vol01__instalar_aplicaciones_no_tenidas_antes/itunes_01.png" target="_blank"><img src="/assets/img/2019-06-23-howto_mantener_vivo_ipad_antiguo_vol01__instalar_aplicaciones_no_tenidas_antes/itunes_01__green.png" alt="buscar y descargar 'app' desde 'iTunes'" width="600"/></a>  
<small>buscar y descargar 'app' desde 'iTunes'</small>  
<br>

<big>***4.**</big> En 'iTunes' del ordenador, ábrase la sección correspondiente al iPad y hacemos click en el apartado "Aplicaciones". Veremos que aparece la app que acabamos de descargar. Click en instalar. Dirá que es incompatible; ni caso.

<br>
<a href="/assets/img/2019-06-23-howto_mantener_vivo_ipad_antiguo_vol01__instalar_aplicaciones_no_tenidas_antes/itunes_02.png" target="_blank"><img src="/assets/img/2019-06-23-howto_mantener_vivo_ipad_antiguo_vol01__instalar_aplicaciones_no_tenidas_antes/itunes_02__green.png" alt="instalación de 'app' desde 'iTunes'" width="600"/></a>  
<small>instalación de 'app' desde 'iTunes'</small>  
<br>

<big>***5.**</big> En el iPad, ábrase 'App Store'. Veremos esa App, lista para instalar. Click en el icono con forma de nube que aparece al lado. El iPad nos preguntará si deseamos instalar la última versión compatible. Aceptamos. Trabajo hecho.

<br>
<a href="/assets/img/2019-06-23-howto_mantener_vivo_ipad_antiguo_vol01__instalar_aplicaciones_no_tenidas_antes/appstore_ipad.jpg" target="_blank"><img src="/assets/img/2019-06-23-howto_mantener_vivo_ipad_antiguo_vol01__instalar_aplicaciones_no_tenidas_antes/appstore_ipad__green.jpg" alt="desde el iPad -'AppStore'- instalación de la última versión compatible de la 'app'" width="500"/></a>  
<small>desde el iPad -'AppStore'- instalación de la última versión compatible de la 'app'</small>  
<br>

<br>
<br>
<br>
Entradas relacionadas:  
- [https://hijosdeinit.gitlab.io/Germen_rendicion_gnulinux_a_obsolescencia/](https://hijosdeinit.gitlab.io/Germen_rendicion_gnulinux_a_obsolescencia/)
- [[HowTo] Instalación netinstall de Debian en netbook Compaq Mini](https://hijosdeinit.gitlab.io/howto_instalacion_netinstall_Debian_netbook_compaqmini/)
- [https://hijosdeinit.gitlab.io/Germen_rendicion_gnulinux_a_obsolescencia/](https://hijosdeinit.gitlab.io/Germen_rendicion_gnulinux_a_obsolescencia/)
- [[HowTo] Instalación netinstall de Debian en netbook Compaq Mini](https://hijosdeinit.gitlab.io/howto_instalacion_netinstall_Debian_netbook_compaqmini/)
- [[HowTo] búsqueda de páginas web ‘desaparecidas’, directamente en la caché de Google](https://hijosdeinit.gitlab.io/howto_busqueda_en_cache_de_google_paginas_desaparecidas/)
- [Saque todo lo que pueda de los soportes ópticos (cd/dvd), si no lo ha hecho ya](https://hijosdeinit.gitlab.io/saque-todo-lo-que-pueda-de-los-soportes/)
- [Otra faceta de la vida útil de las memorias usb](https://hijosdeinit.gitlab.io/otra-faceta-vida-util-memoria-usb/)
- [Qué pedirle a un ‘pendrive’ USB](https://hijosdeinit.gitlab.io/requisitos_de_un_pendrive/)
- [Mirando cara a cara al abismo de la incoherencia posmoderna: un vistazo a la Tecnofilia creditofágica](https://hijosdeinit.gitlab.io/tenofilia_creditofagica/)
- ["Gratis"](https://hijosdeinit.gitlab.io/lo_gratis/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.applesfera.com/itunes/apple-sigue-ofreciendo-una-version-de-itunes-con-app-store-asi-puedes-descargarla">AppleSfera</a>  
<a href="https://omicrono.elespanol.com/2016/07/instalar-aplicaciones-versiones-antiguas-ios/">Omicrono</a>  
<a href="https://support.apple.com/es-es/HT208079">support.apple.com</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
