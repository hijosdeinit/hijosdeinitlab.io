---
layout: post
title: "[HowTo] 'Tribler', cliente torrent pro-anonimato"
date: 2022-01-30
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "redes", "tor", "onion", "tribler", "seguridad", "privacidad", "anonimato", "transmission", "transmission-cli", "transmission-daemon", "Debian", "directorio", "archivo", "configuracion", "settings.json", "stats.json", "torrent", "resume", "descargas", "blocklists", "p2p", "ed2k", "kad", "amule", "amule-daemon", "demonio", "servicio", "amuleweb", "amulegui", "amulecmd"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
[Tribler](https://www.tribler.org/) es un cliente p2p torrent enfocado a la privacidad y a esquivar la censura (<small>hay países donde las descargas torrent son monitorizadas y, dependiendo los contenidos, sancionadas</small>).  
Para ello utiliza un 'enrutado cebolla' -['onion routing'](https://en.wikipedia.org/wiki/Onion_routing) -"por capas"- **inspirado** en [*The Onion Router* (Tor)](https://www.torproject.org/).  
> <small>El *enrutado cebolla* es una «technique for anonymous communication over a computer network. In an onion network, messages are encapsulated in layers of encryption, analogous to layers of an onion. The encrypted data is transmitted through a series of network nodes called onion routers, each of which "peels" away from a single layer, uncovering the data's next destination. When the final layer is decrypted, the message arrives at its destination. The sender remains anonymous because each intermediary knows only the location of the immediately preceding and following nodes. While onion routing provides a high level of security and anonymity, there are methods to break the anonymity of this technique, such as timing analysis.»</small>  

zzz zzz vvv vvv

ojo! ... como protonmail  




Privacy using our Tor-inspired onion routing
Search and download torrents with less worries or censorship

Disclaimer

Do not put yourself in danger. Our anonymity is not yet mature.

Tribler does not protect you against spooks and government agencies. We are a torrent client and aim to protect you against lawyer-based attacks and censorship. With help from many volunteers we are continuously evolving and improving.


· [Página oficial]()  
· [Repositorio en GitHub]()  
· []()  


<br>
<br>
Entradas relacionadas:  
- [[HowTo] Incorporar un tema visual más atractivo al interfaz web de 'amule-daemon'](https://hijosdeinit.gitlab.io/howto_incorporar_tema_visual_atractivo_al_interfaz_web_amule/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://trac.transmissionbt.com/wiki/MovedToGitHub/ConfigFiles">trac.transmissionbt.com</a>  
<a href="https://trac.transmissionbt.com/wiki/UnixServer/Debian">trac.transmissionbt.com - debian</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
