---
layout: post
title: "[HowTo] Algunas herramientas de monitorización para la terminal de Debian y derivados más allá de 'top' y 'htop'"
date: 2022-01-20
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "ram", "memoria", "memoria ram", "RAM", "seguridad", "gnu linux", "linux", "debian", "cli", "Monitorix", "Monitoriza-Tu-Raspberry", "monitorización", "procesos", "Raspberry-Pi-Status", "rpi-monitor", "servicios", "servidor", "temperatura", "voltaje", "frecuencia", "servidor", "top", "htop", "ps", "vmstat", "free"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<br>
<big>EN CONSTRUCCION</big>
<br>
<br>

Son tan conocidos como útiles los comandos 'top' -que suele traer de serie cualquier distribución GNU Linux- y 'htop'.  
Proporcionan información suficiente como para conocer la carga de un sistema.  
Estos comandos, junto a 'ps' y 'pkill', son -a mi entender- indispensables: <small>Últimamente el viejo ordenador 'core2duo' del 2008 que -por nostalgia- suelo emplear con más frecuencia de lo que debiera, tiene por costumbre colapsar inoportunamente. Sospecho que, en ocasiones, tiene algo que ver con los drivers 'nouveau' de la tarjeta gráfica. Otras veces se debe al abuso del navegador web, más allá de lo razonable; demasiadas ventanas y pestañas abiertas acaban devorándolo todo. Cuando, a pesar del colapso, la máquina aún se deja, evito el apagado en caliente entrando por 'ssh' y, mediante 'htop' echo un vistazo a lo que están consumiendo los procesos y termino con los más tragones. En otras ocasiones no queda otra que echar mano de `sudo reboot`</small>.  

<br>
No obstante, sin desmerecer a 'top' y a 'htop', existen otros comandos o herramientas similares, que más o menos hacen lo mismo aunque suministrando más información... con una estética más atractiva aún y probablemente un poco más pesados:
- ['btop'](https://github.com/aristocratos/btop)
- ['bashtop'](https://github.com/aristocratos/bashtop)
- ['bpytop'](https://github.com/aristocratos/bpytop)
- ['nmon'](http://nmon.sourceforge.net/pmwiki.php)
- ['ytop'](https://github.com/cjbassi/ytop)
- ['gtop'](https://github.com/aksakalli/gtop)
- ['vtop'](https://parall.ax/blog/view/3071/introducing-vtop-a-terminal-activity-monitor-in-node-js), y ['atop'](http://www.atoptool.nl/).
- ['gotop'](https://github.com/cjbassi/gotop)
- ['bottom'](https://github.com/ClementTsang/bottom)
- ['psutil'](https://github.com/giampaolo/psutil)
- ['px'](https://github.com/walles/px)
- ['libstatgrab'](https://libstatgrab.org/)
- ['lsof'](https://people.freebsd.org/~abe/)
- ['glances'](https://nicolargo.github.io/glances/)
- ['monitorix'](https://www.monitorix.org/)
- ['grafana'](https://grafana.com/)  


<br>
<br>
## ['btop']()

<br>
## ['bashtop']()

<br>
## ['bpytop']()



<br>
<br>
Entradas relacionadas:  
- [[HowTo] 5 comandos para monitorizar la memoria RAM (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_monitorizar_memoria_ram_debian_derivados/)
- [[HowTo] Monitorización de RaspBerry Pi [volumen 1]: rpi-monitor](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_1/)
- [[HowTo] Monitorización de RaspBerry Pi [volumen 2]: Monitorix, Raspberry-Pi-Status y Monitoriza-Tu-Raspberry](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_2/)
- [[HowTo] Monitorización de RaspBerry Pi [volumen 3]: temperaturas de CPU y GPU, frecuencia y voltaje desde línea de comandos (CLI)](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_3_temperaturas/)
- [[HowTo] Monitorix: instalación, configuracion, uso básico](https://hijosdeinit.gitlab.io/howto_Monitorix_instalacion_configuracion_uso_basico/)
- [[HowTo] Incorporar monitor de sistema a CrunchBang++ y parientes similares](https://hijosdeinit.gitlab.io/howto_monitor_sistema_en_Crunchbang_Linux_derivados/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://haydenjames.io/btop-the-htop-alternative/">btop</a>
<a href="zzz">zzz</a>
<a href="zzz">zzz</a>
<a href="zzz">zzz</a>
<a href="zzz">zzz</a>
<a href="zzz">zzz</a>
<a href="zzz">zzz</a>
<a href="https://parall.ax/blog/view/3071/introducing-vtop-a-terminal-activity-monitor-in-node-js">zzz</a>
<a href="https://alternativeto.net/software/htop/">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
