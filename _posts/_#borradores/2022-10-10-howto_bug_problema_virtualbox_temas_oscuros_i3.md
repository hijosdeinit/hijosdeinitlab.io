---
layout: post
title: "[HowTo] Solución al problema del texto ilegible en el interfaz de 'Oracle VirtualBox' 6.1"
date: 2022-10-10
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "maquinas virtuales", "virtual", "vm", "disco duro virtual", "almacenamiento virtual", "virtualbox", "vmware workstation", "vmware player", "qemu", "gnome boxes", "vmware esxxi", "vmware esx", "proxmox ve", "parallels desktop", "xen", "docker", "kubernetes", "podman", "wine", "dosbox", "scummvm", "emulacion", "emulador", "gnu linux", "linux", "debian", "buster", "windows", "windows 10", "virtualizacion", "problema", "bug", "fallo", "error", "errata", "tema oscuro", "dark", "ilegible", "texto", "color", "contraste", "fondo"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno: CPU Intel Celeron N4000C (2) @ 2.600GHz, GPU Intel GeminiLake [UHD Graphics 600], 8gb RAM, GNU Linux Mint Debian Edition 5 (*elsie*) x86_64, kernel 5.10.0-19-amd64, cinnamon, lxde, i3, tema visual *Midnight-Green* [GTK2/3]</small>  

<br>
Problema:


<br>
<br>
Entradas relacionadas:
- []()
- []()
- [Software de virtualización (máquinas virtuales)](https://hijosdeinit.gitlab.io/utilidades_maquinas_virtuales/)
- [[HowTo] Aumentar el tamaño del disco duro virtual de una máquina virtual de Virtual Box](https://hijosdeinit.gitlab.io/howto_aumentar_tama%C3%B1o_discodurovirtualdinamico_maquinavirtual_virtualbox/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzzzzz</a>  
<a href="zzz">zzzzzz</a>  
<a href="zzz">zzzzzz</a>  
<a href="zzz">zzzzzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
