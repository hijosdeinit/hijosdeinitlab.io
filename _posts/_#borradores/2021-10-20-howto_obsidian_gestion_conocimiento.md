---
layout: post
title: "[HowTo] @@@@@@@@@@ 'Obsidian'"
date: 2021-10-20
author: Termita
category: "productividad"
tags: ["productividad", "sistemas operativos", "obsidian", "gnu linux", "linux", "debian", "windows", "macos", "wiki", "gestion del conocimiento"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
EN CONSTRUCCION  
<br>
<br>
<br>
<br>
Obsidian is a powerful knowledge base that works on top of a local folder of plain text Markdown files. In Obsidian, making and following connections is frictionless, and with the connections in place, you can explore all of your knowledge in the interactive graph view. Obsidian supports CommonMark and GitHub Flavored Markdown (GFM), along with other useful notetaking features such as tags, LaTeX mathematical expressions, mermaid diagrams, footnotes, internal links and embedding Obsidian notes or external files. Obsidian also has a plugin system with 20+ supported plugins to expand its capabilities. The plugin system is currently a work-in-progress, there will be public APIs available to developers in the v1.0 release.

The documentation can be accessed by opening the demo vault that is shipped in Obsidian.

Obsidian will always remain free for personal use, no account or license is required for qualifying use cases described in the End User License Agreement. However, commercial users should purchase a license on Obsidian's website. Users also have the option to purchase a Catalyst license to get early access to insider builds and support the development. Additionally, Obsidian offers an optional paid hosting service for your notes. Details about this optional hosting service can be found on the Obsidian website.

This distribution is currently in beta and not officially supported by the Obsidian team

There is now experimental Wayland support that can be enabled with OBSIDIAN_USE_WAYLAND=1 in Flatseal

Note: Regular users should disable "Automatic Updates" within Obsidian as public release updates are handled by Flatpak itself. Insiders should keep this enabled to receive insider builds, as those will not be made available on Flathub.

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
