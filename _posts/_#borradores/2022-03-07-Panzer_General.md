---
layout: post
title: "'Panzer General' y su saga"
date: 2022-03-08
author: Termita
category: "juegos"
tags: ["sistemas operativos", "software", "juegos", "gnu linux", "linux", "windows", "macos", "panzer general"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## Panzer General
'Panzer General I' es un 'wargame' para computadora, ambientado en la 2ª Guerra Mundial (1939-1945), desarrollado y publicado en 1994 por Strategic Simulations Inc. (SSI), que dio lugar a toda una saga "oficial" y a multitud de "forks" y juegos inspirados en él, muchos de ellos *open source*.  
Es multiplataforma: fue desarrollado para 	MS-DOS, Windows, 3DO, MacOS 7 y PlayStation.  
Es un juego de estrategia por turnos sobre un mapa divididido en multitud de sectores hexagonales. El jugador puede elegir el bando -Eje ô Aliados- y acometer desde un simple escenario hasta una campaña entera.  
'Panzer General' tenía, asimismo, un modo multijugador a través de correo electrónico (*Play by Email (PBEM)*).  
<br>
En 'Panzer General I' hay 38 **escenarios**. Están basados en batallas reales y también en ficticias.  
El modo **"campaña"** de 'Panzer General I' consiste en una serie de batallas que conducirán hacia la victoria o la derrota. Hay una larga campaña alemana, con 5 localizaciones:
- Polonia (1939): desde Polonia, a través de Noruega, hasta el Oeste con posible invasión anfibia en Gran Bretaña.
- Norte de África (1941): desde el Norte de África hasta el Oriente Medio.
- Barbarroja (1941): desde la invasión de la frontera soviética hasta Moscú.
- Italia (1943): desde el aterrizaje aliado en Sicilia hasta el final de la guerra.
- Kharkov (1943): desde la ofensiva alemana de primavera hasta el final de la guerra.  

Las unidades son batallones (aproximadamente) y tanto el nombre de éstas como sus características son histórica y razonablemente precisas. No ocurre lo mismo con el tamaño de éstas y la características -dimensiones, escala, etc...- de los mapas, que se asemejan a la realidad histórica de un modo elástico y aproximado.  
Las unidades pueden ganar experencia e irse haciendo cada vez más potentes. El éxito en los enfrentamientos aumenta el "prestigio" del jugador / general, lo cual le permitirá mejorar unidades, adquirir nuevas unidades o seleccionar un escenario mejor para la siguiente batalla.  
Hay 3 formas de finalizar de una misión:
- "victoria mayor": cuando se finaliza una misión en menos turnos de los exigidos
- "victoria táctica": cuando se finaliza una misión exitosamente, pero tardando más turnos de los exigidos.
- "derrota"  

<br>
Las unidades combatientes son, entre otras: infantería, carros de combate, anticarro, artillería, artillería antiaérea, bombarderos, cazas, etcétera...  
El juego requiere que el jugador use tácticas en las que se **combinan** diferentes "armas": en los enfrentamientos cada unidad tiene ventajas y vulnerabilidades respecto a otras unidades: Las unidades enemigas atrincheradas pueden ser "ablandadas" a distancia a base de fuego de artillería, y ésta -a su vez- es vulnerable a los asaltos directos, por lo que ha de ser protegida.  
Antes de atacar a la infantería y a las unidades anticarro es muy recomendable lanzar primero los carros de combate a destruir la artillería enemiga que, distante, las protege. Si éstos -nuestros carros de combate- no tienen a la infantería a tiro, el trabajo puede ser realizado mediante bombarderos, para lo cual primero es necesario destruir la artillería antiaérea enemiga.  
Los cazas tienen un rol doble: ataque aire-aire (protegiendo, por ejemplo, a nuestros bombarderos) y ataque aire-tierra.  
El jugador debe analizar cuidadosamente la red de carreteras y puentes para aprovecharloss y así ganar ventaja haciendo que el avance terrestre sea más rápido. El juego recompensa una estrategia basada en la '*blitzkrieg*': penetración profunda en las líneas enemigas, posponiendo la destrucción de alguna de las unidades enemigas que van siendo detectadas para más tarde.  
El desempeño de las unidades es incrementado por los puntos de experiencia que van ganando en combate. Especialmente en el modo "Campaña", es muy recomendable proteger la integridad de las unidades experimentadas, que son las más valiosas.  
<br>
El modo "Campaña" de 'Panzer General I'
Las tareas en la mayoría de escenarios consisten en tomar todas las ciudades "objetivo" en un número determinado de turnos (o antes). Tomarlas 5 turnos antes es considerado una "victoria mayor". En los escenarios de las últimas etapas de la guerra lo alemanes intentarán mantener posiciones ante un enemigo mucho más fuerte; una tarea típica es "mantener al menos 2 (5 para una "victoria mayor") de las ciudades objetivo durante 20 turnos".
«All campaigns branch out and end either by the general being sacked for incompetence or end of the war. In Campaign Mode, a major victory could possibly change known historical events. For example, after a major victory over France, the player invades Britain. Later in the game, after a major victory in Barbarossa, the player can convince the German High Command to attack Moscow immediately (which costs him or her much prestige) rather than diverting to Kiev before Moscow.»  
«If the player achieves a major victory both in Britain and in Moscow, he or she is allowed to carry out an invasion of the United States and reach Washington. In any other case, he/she must fight well in many battles to get another chance to attack them. If either Britain or USSR survive this attack, they drive the Germans all the way back to Berlin. The best the player can do is to fight well in each battle to have enough prestige for the next one - and to achieve a major victory in the final defense of Berlin.»  



<br>
## La saga oficial de 'Panzer General'
Panzer General was the first game in the Panzer General series, which grew in the years after its release.  
It was followed by Allied General (Panzer General II in Germany), which allows play from the Allied point of view and features four new campaigns.  
Other sequels include Fantasy General, Pacific General and Star General.  
Its mainline sequel, Panzer General II (titled Panzer General 3D in Germany), upgraded the interface to use an overlay of photorealistic terrain, and to display different unit facings, resulting in an improved appearance. In the April 2000 issue of PC Gamer, it was voted the 44th best computer game of all time.  
The series continued with People's General, an upgraded version of Panzer General II based on a World War III scenario between China and the UN.  
Panzer General 3D Assault arrived in 1999, with fully 3D graphics.  
Panzer General III: Scorched Earth was released in 2000, with better graphics and a redesigned interface.  




Allied General Pacific General Fantasy General Star General People's General  

Panzer General Panzer General II Panzer General 3D Assault Panzer General III: Scorched Earth Panzer General: Allied Assault Panzer General: Russian Assault  

Panzer General IV: Western Assault = Panzer General 3D: assault???????  




<br>
## Juegos inspirados en 'Panzer General'
Tratado [en esta entrada de este blog]().  

<br>
<br>
<br>
## Descarga de 'Panzer General I'
'Panzer General I' es un juego muy antiguo. **La versión para PC se puede descargar desde archive.org**: [1](https://archive.org/download/msdos_Panzer_General_1994/Panzer_General_1994.zip), [2]()  

<br>
<br>
Entradas relacionadas:  
- []()saga panzer General
- []()estrategia 4x
- [Listado oficial de juegos para línea de comandos de Debian](https://hijosdeinit.gitlab.io/listado_oficial_juegos_terminal_cli_debian/)
- [https://hijosdeinit.gitlab.io/tron_online_ssh_terminal/](https://hijosdeinit.gitlab.io/tron_online_ssh_terminal/)
- [zzz](zzz)zangband
- []()angband
- []()minetest
- []()warzone 2100  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://en.wikipedia.org/wiki/Panzer_General">Wikipedia - 'Panzer General'</a>  
<a href="http://hartmann.valka.cz/panzergeneral/eindex.htm">Pepa's Panzer General center</a>  
<a href="https://tabernadegrog.blogspot.com/2013/01/panzer-general.html">La taberna de Grog</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
