---
layout: post
title: "[HowTo] 'dosemu2' en Debian y derivados"
date: 2021-08-26
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "windows", "windows 10", "msdos", "dos", virtualizacion", "maquinas virtuales", "disco duro virtual", "almacenamiento virtual", "virtual box", "vmware workstation", "vmware player", "qemu", "gnome boxes", "vmware esxxi", "vmware esx", "proxmox ve", "parallels desktop", "xen", "docker", "kubernetes", "podman", "wine", "dosbox", "dosemu", "dosemu2", "scummvm", "emulacion", "emulador"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<br>
<big>**EN CONSTRUCCIÓN**<big>
<br>
<br>
<br>


Hace tiempo me planteé la posibilidad de utilizar mi procesador de texto favorito -*wordperfect 5.0*, un programa para 'MS DOS' de finales de los 80 y principios de los 90- en máquinas actuales y/o bajo GNU Linux. Una de las formas de llevar a cabo esto es mediante el empleo de emuladores de 'msdos' como 'DosEmu2' o 'DosBox'.  
[Sobre 'DosBox' existe un artículo en este blog](https://hijosdeinit.gitlab.io/howto_dosbox_debian/).  
<small>(Otras opciones podrían consistir en crear máquinas virtuales de 'msdos' o 'Windows antiguos' mediante 'VMware', 'VirtualBox', 'Qemu', 'Gnome Boxes' y/o similares).</small>  

<br>
['DosEmu2'](https://dosemu2.github.io/dosemu2/) es una *máquina virtual* que permite ejecutar programas 'msdos' bajo GNU Linux. A diferencia de 'DosBox', 'DosEmu2' no es multiplataforma. El proyecto 'DosEmu2' se emprendió como un intento de reencarnar el famoso proyecto ['DosEmu'](http://www.dosemu.org/) bajo licencia *GPLv2+* (<small>la licencia de 'dosemu1' es controvertida</small>), aunque ha crecido y se ha desarrollado como un completamente nuevo proyecto con diferentes ideas, metas y código. Asimismo tiene una mayor amplitud de miras que las que tuvo 'dosemu1': en lugar de simplemente ser una máquina virtual, 'DosEmu2' también desarrolla su propio DOS de 64 bits (<small>véase la lista completa de los proyectos del equipo 'DosEmu2'</small>.  
'DosEmu2' proclama ser un emulador seguro, rápido y rico en prestaciones.  

<br>
<a href="/assets/img/@@@@@@@@@@@@@@@@@@" target="_blank"><img src="/assets/img/@@@@@@@@@@@@@@@@@@" alt="@@@@@@@@@@@@@@@@@@@@@@@@@" width="800"/></a>  
<small>'@@@@@@@@</small>  
<br>

<br>
'DosEmu2' puede ser instalado en GNU Linux de diversas formas:
- Compilando el código fuente, que se obtiene clonando el repositorio de 'DosEmu2' en *GitHub' [<small><span style="background-color:#042206"><span style="color:lime">`git clone https://github.com/dosemu2/dosemu2.git`</span></span></small>]
- Desde el propio repositorio del proyecto para las diferentes distribuciones GNU Linux [Ubuntu](https://code.launchpad.net/~dosemu2/+archive/ubuntu/ppa), [Fedora](https://copr.fedorainfracloud.org/coprs/stsp/dosemu2), [OpenSUSE](https://download.opensuse.org/repositories/home:/stsp2/openSUSE_Tumbleweed/home:stsp2.repo.mirrorlist)]  

Esa segunda opción es la que escogí para incorporar 'DosEmu2' a Debian 10 *Buster*.  


<br>
## Compilación de 'DosEmu2' en Debian 10 *Buster*
Instálense las dependencias
~~~bash
sudo apt-get update
sudo apt install flex
sudo apt install bison
sudo apt install gawk
sudo apt install libbsd-dev
~~~
Instálese / compílese otra de las dependencias: 'fdpp'
~~~
???desconocido / no logro compilar en Debian 10 *Buster* x32
'fdpp' está relacionado con 'FreeDos' de 64 bits
~~~
~~~
Clónese el repositorio de 'DosEmu2' (*GitHub*)
~~~bash
git clone https://github.com/dosemu2/dosemu2.git
~~~
Compílese
~~~bash
cd dosemu2
./autogen.sh
./default-configure
make
~~~

<br>
## Instalación de 'DosEmu2' en Debian 10 *Buster* desde el repositorio del proyecto 'DosEmu2'
Agréguese el repositorio 'ppa:dosemu2/ppa' mediante el comando `add-apt-repository'.
~~~bash
sudo add-apt-repository ppa:dosemu2/ppa
~~~
Si el comando 'add-apt-repository' no estuviera instalado en el sistema Debian, [en este blog hay una entrada que explica cómo hacerlo](https://hijosdeinit.gitlab.io/howto_add-apt-repository_en_debian_y_derivados/): <span style="background-color:#042206"><span style="color:lime">`sudo apt install software-properties-common`</span></span>.  

<br>
Procédase a instalar. Desde línea de comandos:
~~~bash
sudo apt update
sudo apt install dosemu2
~~~

<br>
<br>
Entradas relacionadas:  
- [[HowTo] 'Dosbox' En Debian Y Derivados](https://hijosdeinit.gitlab.io/howto_dosbox_debian/)
- [Software de virtualización (máquinas virtuales)](https://hijosdeinit.gitlab.io/utilidades_maquinas_virtuales/)
- [[HowTo] cygwin: GNU Linux en Windows](https://hijosdeinit.gitlab.io/howto_cygwin_GNU_Linux_en_Windows/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="@@@@@@@@@@@@@@@@@@@@@@@@@@@">@@@@@@@@@@@@@@@@@@@@@@@@@@@</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
