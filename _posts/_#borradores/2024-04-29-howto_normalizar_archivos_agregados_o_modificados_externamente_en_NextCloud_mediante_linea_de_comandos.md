---
layout: post
title: "[HowTo] normalizar en 'NextCloud' archivos modificados o creados externamente (procedimiento desde línea de comandos)"
date: 2024-04-29
author: Termita
category: "sistemas operativos"
tags: ["nextcloud", "nube", "web 4.0", "occ", "config.php", "nc-maintenance", "modo mantenimiento", "nextcloudpi", "panel", "nc-scan", "nc-fix-permissions", "www-data"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Generalmente la forma de mantener sincronizados zzzzz es:
- cliente oficial de sincronización NextCloud
- desde el navegador, en la aplicación web de NextCloud
- 'webdav'  

webdav en NextCloud a veces va malzzz  

Existe otra forma: directamente en disco, local o remotamente
















<small>Entiendo que el servidor de NextCloud puede desplegarse de 2 formas, 2 "sabores":</small>  
- <small>NextCloud (sin panel de administración :4443)</small>
- <small>NextCloudPi (con panel de administración :4443)</small>  
<br>

El modo mantenimiento en NextCloud sirve para apagar el servidor NextCloud y llevar a cabo tareas de mantenimiento sin que haya interferencias.  
<br>
<br>

## ♠ Activación del 'modo mantenimiento' en NextCloud
El modo mantenimiento en NextCloud se puede **activar** de 3 formas. Todas ellas son válidas para NextCloudPi, mas sólo las 2 primeras se aplican en NextCloud puro y duro.  
<br>

### Por línea de comandos
(Sirve en NextCloudPi y NextCloud)  
<small>(*) Si NextCloud -o NextCloudPi- está desplegado mediante docker, primero averiguamos el identificador del docker mediante <span style="color:lime">`sudo docker ps`</span> y luego ejecutamos <span style="color:lime">`sudo docker exec -i -t identificadordeldocker /bin/bash`</span>, tal como se explica en [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_entrar_a_la_linea_de_comandos_de_un_contenedor_docker/).</small>  
~~~
sudo -u www-data php /var/www/nextcloud/occ maintenance:mode --on
~~~
El sistema responderá:
> Maintenance mode enabled  

<br>
### Editando '/var/www/nextcloud/config/config.php'
(Sirve en NextCloudPi y NextCloud)
~~~
sudo nano /var/www/nextcloud/config/config.php
~~~
Búsquese `'maintenance' => false,` y cambiese por `'maintenance' => true,`  

<br>
### Interfaz web de control (panel :4443)
(Sirve sólo en NextCloudPi)  
Al interfaz web de control de NextCloudPi -lo que suele llamarse "el panel", que suele escuchar en el puerto :4443- se accede vía navegador web en la url 'https://ipdelservidor:4443'  
Tools → nc-maintenance  
Márquese 'Enabled' y púlsese en el botón 'Apply'.  
<br>
<a href="/assets/img/2020-12-23-howto_activar_modo_mantenimiento_en_NextCloud/20201223_maintenancemode_panel_nextcloudpi.png" target="_blank"><img src="/assets/img/2020-12-23-howto_activar_modo_mantenimiento_en_NextCloud/20201223_maintenancemode_panel_nextcloudpi.png" alt="nc-maintenance en el panel de NextCloudPi" width="800"/></a>  
<small>'nc-maintenance' en el panel de NextCloudPi</small>  

<br>
### Utilidad 'ncp-config'
~~~
sudo ncp-config
~~~
Tools → nc-maintenance
<br>
<a href="/assets/img/2020-12-23-howto_activar_modo_mantenimiento_en_NextCloud/20201223_maintenancemode_ncp-config_nc-maintenance.png" target="_blank"><img src="/assets/img/2020-12-23-howto_activar_modo_mantenimiento_en_NextCloud/20201223_maintenancemode_ncp-config_nc-maintenance.png" alt="nc-maintenance en el panel de NextCloudPi" width="800"/></a>  
<small>'nc-maintenance' en el 'ncp-config</small>  

<br>
<br>
<br>
<br>
## ♠ Desactivación del 'modo mantenimiento' en Nextcloud
El modo mantenimiento en NextCloud se puede **desactivar** de 3 formas. Todas ellas son válidas para NextCloudPi, mas sólo algunas de ellas se aplican en NextCloud puro y duro  
<br>

### Por línea de comandos
(Sirve en NextCloudPi y NextCloud)  
<small>(*) Si NextCloud -o NextCloudPi- está desplegado mediante docker, primero averiguamos el identificador del docker mediante <span style="color:lime">`sudo docker ps`</span> y luego ejecutamos <span style="color:lime">`sudo docker exec -i -t identificadordeldocker /bin/bash`</span>, tal como se explica en [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_entrar_a_la_linea_de_comandos_de_un_contenedor_docker/).</small>  
~~~
sudo -u www-data php /var/www/nextcloud/occ maintenance:mode --off
~~~
El sistema responderá:
> Maintenance mode disabled  

<br>
### Editando '/var/www/nextcloud/config/config.php'
(Sirve en NextCloudPi y NextCloud)  
~~~
sudo nano /var/www/nextcloud/config/config.php
~~~
Búsquese `'maintenance' => true,` y cambiese por `'maintenance' => false,`  

<br>
### Interfaz web de control (panel :4443)
(Sirve sólo en NextCloudPi)  
Al interfaz web de control de NextCloudPi -lo que suele llamarse "el panel", que suele escuchar en el puerto :4443- se accede vía navegador web en la url 'https://ipdelservidor:4443'  
Tools → nc-maintenance  
Márquese 'Disabled' y púlsese en el botón 'Apply'.  

<br>
### Utilidad 'ncp-config'
~~~
sudo ncp-config
~~~
Tools → nc-maintenance

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://help.nextcloud.com/t/update-to-13-nextcloud-is-in-maintenance-mode/27951/3" target="_blank">Schmu en help.nextcloud.com</a>  
<br>
<br>
<br>
<br>
<br>
<br>
