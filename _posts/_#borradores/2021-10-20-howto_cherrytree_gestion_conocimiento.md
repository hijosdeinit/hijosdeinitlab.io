---
layout: post
title: "[HowTo] Gestión del conocimiento con 'CherryTree' en Debian 10 y derivados"
date: 2021-10-20
author: Termita
category: "productividad"
tags: ["productividad", "sistemas operativos", "cherrytree", "obsidian", "gnu linux", "linux", "debian", "windows", "macos", "wiki", "gestion del conocimiento"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
EN CONSTRUCCION  
<br>
<br>
<br>
<br>
<small>Conocí 'CherryTree' hace unos años probando ['Kali Linux'](https://www.kali.org/), la distribución GNU Linux enfocada al *pentesting* y auditorías de seguridad diversas heredera -desde 2013- de ['BackTrack Linux'](https://www.backtrack-linux.org/). Venía [instalado "de serie"](https://www.kali.org/tools/cherrytree/). Era una especie de 'KeePass' enfocado, no en la gestión de contraseñas, sino en las notas. La filosofía del programa me pareció interesantísima, no ya en la toma de anotaciones, sino en la gestión de todo ese conocimiento con el que vamos teniendo contacto y que, sospechamos, de no anotarlo, lo perderíamos. No obstante, por costumbre, seguí tomando mis notas en texto plano.</small>  

<br>
[CherryTree](https://www.giuspen.com/cherrytree/) es una aplicación para tomar notas de forma jerárquica. Pese a su aparentemente simple interfaz, este programa tiene montones de prestaciones. Permite, por ejemplo, incoporar texto plano, texto enriquecido, resaltado de sintaxis, manejo de imágenes, hiperenlaces, importación/exportación a múltiples formatos, multilenguaje, etcétera. Los datos se almacenan en un sólo archivo 'xml' o 'sqlite'.  
CherryTree está desarrollada en 'python' y es multiplataforma -GNU Linux, Windows y [macOS](http://giuspen.com/cherrytreemanual/#_macos_not_tested)-. Hay versiones para arquitecturas de 64bits y 'arm64'.  

<br>
## Instalación de 'CherryTree' en Debian 10 (y derivados)
En el sitio web oficial se pueden descargar paquetes para Ubuntu, instaladores y una versión *portable* para Windows, y el código fuente, que se puede descargar también desde el [repositorio oficial de CherryTree en GitHub](https://github.com/giuspen/cherrytree).  
Para Ubuntu hay un repositorio *ex profeso*.  
También hay una versión FlatPak.  
Asimismo, 'CherryTree' se puede encontrar en los repositorios de muchas distribuciones GNU Linux, aunque generalmente no en su última versión, listo para ser fácilmente instalado con sus correspondientes gestores de paquetes.  
<br>
Decidí instalar 'CherryTree' en mi sistema Debian 10 64bits aprovechándome del [repositorio de 'LaunchPad' (Canonical/Ubuntu)](https://launchpad.net/~giuspen/+archive/ubuntu/ppa/+packages):
~~~bash
sudo apt-get update
wget https://launchpad.net/~giuspen/+archive/ubuntu/ppa/+files/cherrytree_0.99.42-4_amd64.deb
sudo apt-get install ./cherrytree_0.99.42-4_amd64.deb
~~~

<br>
## Funcionamiento de 'CherryTree'
Existe un [manual online exhaustivo de 'CherryTree'](http://giuspen.com/cherrytreemanual/) del cual se puede, además, disponer en modo offline [descargándolo](http://giuspen.com/cherrytreemanual/cherrytree_manual.ctb) en formato '.ctb', que es el formato de las anotaciones de 'CherryTree'.  

<br>
La organización de las anotaciones jerárquicamente es muy adecuada para proyectos complejos y guías porque ayuda a estructurar la información de forma significativa. Todos los elementos creados en CherryTree -carpetas, subcarpetas y notas- son llamados **"nodos"**, y éstos pueden ser de uno de estos 3 tipos:
- texto plano
- texto enriquecido
- resaltado de sintaxis automático (especialmente indicado para notas que contienen código).  
<br>
Todas las notas se almacenan en un sólo archivo que puede ser de formato **.xml** o bien **sqlite**, y éste puede protegerse mediante contraseña.  

<br>
Cuando se crea un nodo, se le pueden asignar **etiquetas** para facilitar las búsquedas.  
Los nodos pueden contener **subnodos**, que son el equivalente de los "bloc de notas" (*notebooks*) en 'EverNote' o 'MyNotex'.  
Los nodos pueden configurarse como de "sólo lectura", pueden ordenarse, moverse, expandirse, colapsarse, enlazarse o agregárseles un marcador para una navegación más rápida.  
Enlazar unos nodos con otros es una característica muy útil. Cualquier texto seleccionado puede ser convertido -mediante la orden 'Insert Anchor'- en un enlace, lo cual permite crear un repositorio de información, una especie de 'Wikipedia' personal.  

<br>
El interfaz principal de 'CherryTree' es una ventana formada por:
- menús
- barra de herramientas
- árbol de nodos del archivo que tenemos abierto (<small>en el lateral izquierdo</small>)
- área de edición  
Todos los elementos -salvo menús y área de edición- pueden ocultarse, y el árbol de nodos puede ser movido al lateral derecho.  
<br>
La barra de herramientas tiene botones con funciones de "dar formato" que sirven para los habituales cambios de color, negrita, cursiva, superíndice, conversión del texto en listas (listas con casillas de chequeo, por ejemplo), alineación del texto, o creación de subtítulos. La barra de herramientas es personalizable.  

<br>
'CherryTree' permite 

CherryTree allows you to insert different kinds of content into your notes. You can insert entire files, paste items from clipboard, insert, resize and rotate images (PNG, JPG, TIFF…), add current date and time, generate a table of contents, and insert regular text tables, which can also be imported from CSV files. If you want to add pieces of code to your notes, insert a

If you want to add pieces of code to your notes, insert a codebox – it supports syntax highlighting for more than 70 programming and scripting languages. Items can be inserted from the right-click menu in the editing area and tree view, or from the “Edit” menu.









<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
