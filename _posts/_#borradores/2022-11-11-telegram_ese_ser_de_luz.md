---
layout: post
title: '"Telegram", ese ser de luz'
date: 2022-11-11
author: Termita
category: "internet 4.0"
tags: ["gratis", "servicios", "Ivoox", "Alphabet", "Google", "Facebook", "mendicidad 4.0", "patreon", "mercaderes", "mercantil", "suscripción", "incerteza", "dependencia", "web 4.0"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Muchos de nosotros tenemos cuenta de ['Telegram']() ese servicio de mensajería instantánea, competencia directa de ['Whatsapp'](), al que supera en prestaciones y en discreción a la hora de mostrar sus verdaderas intenciones.  
En Telegram se han refugiado muchos usuarios -aunque quizás debería decir "usados"- huyendo del lodazal manifiesto que es Whatsapp -propiedad de 'Facebook', hoy 'Meta'-, de sus abusos... de su descaro y prepotencia.  

zzzzzzzzzzzzzzzzz









Voy a recordar lo que realmente significa "gratis" en este mundo mercantil:  

1. Hoy es gratis, mañana quién sabe. Y es intencionado.
Si se genera una dependencia mientras la cosa es gratuíta, cuando ya no lo sea tendremos un consumidor.
Veamos un ejemplo: Ivoox albergaba un producto gratuíto. Por amor al arte se producía y, supuestamente gratis, la empresa albergaba los contenidos. Cuando el producto se hubo popularizado, Ivoox y los que allí alojaban contenidos, borrachos de éxito, comenzaron primero a poner límites y luego a **mendigar** lo de siempre... dinero. Surgió así un apartado "premium", de pago: la famosa suscripción. Era previsible desde el primer día.  

2. Las cosas gratuítas lo son en apariencia, generalmente siempre hay que aportar algo, aunque el usuario no sea consciente.  
Cuando un producto es gratuíto en realidad el producto es el consumidor.  
Véase el ecosistema de esa web 4.0 que tenemos ahora, de la que Alphabet (Google) y Facebook son los máximos exponentes: el producto  en realidad son vds.  
Véase la telefonía móvil, hoy a precio irrisorio. Cada usuario geolocalizado, espiado, marcado como ganado... para la corporación y última instancia para los estados. Nunca fue el hombre tan SIERVO.  

3. Cuando se ofrece algo gratuíto, el usuario ¿qué va a a reclamar si se produjera algún fallo o si cambiaran las condiciones, o incluso, si el producto desapareciera?  
Involucrarse en estos productos es mal asunto. Incerteza y Dependencia combinan mal.  

Vds ya lo saben, porque son aventajados: DESCONFIAMOS de lo gratuíto. CONFIAMOS en lo que construímos con nuestras manos y nuestro intelecto.  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Eliminar todas las imagenes de 'google photos'](https://hijosdeinit.gitlab.io/howto_eliminar_todas_imagenes_googlefotos/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
