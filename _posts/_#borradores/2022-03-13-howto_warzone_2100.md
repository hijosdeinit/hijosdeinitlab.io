---
layout: post
title: "[HowTo] 'Warzone 2100' en Debian 10 y derivados"
date: 2022-03-13
author: Termita
category: "juegos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "juegos", "warzone 2100", "juegos", "software"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
[Warzone 2100](https://wz2100.net/) es un juego para computador híbrido de estrategia y tácticas en tiempo real, desarrollado por Pumpkin Studios y publicado por Eidos Interactive. El juego destaca por varios aspectos dentro de su género: incluye varias tecnologías de radar, se enfoca mucho en la artillería y tecnología anti-baterías, y diseño de vehículos personalizados. Inicialmente fue desarrollado en 1999 para Microsoft Windows y PlayStation. El 6 de diciembre de 2004 el **código fuente y la mayoría de sus archivos de datos fueron liberados bajo la GNU General Public License**, así pues haciéndolo un juego libre. El 10 de junio de 2008 la licencia del juego se liberó del todo y se permitió la distribución de películas y bandas sonoras.  
Es multiplataforma -GNU Linux, Windows, macos, freeBSD- y multiarquitectura -x32/x64, arm-.  
Repositorio de 'Warzone 2100' en 'GitHub': [https://github.com/Warzone2100/warzone2100](https://github.com/Warzone2100/warzone2100)  

<br>
<a href="/assets/img/2022-03-13-howto_warzone_2100/warzone2100.png" target="_blank"><img src="/assets/img/2022-03-13-howto_warzone_2100/warzone2100.png" alt="'Warzone 2100'" width="500"/></a>  
<small>'Warzone 2100'</small>  
<br>

## Instalación de 'Warzone 2100' en Debian 10 y derivados
El código fuente de 'Warzone 2100' está disponible en su página oficial y en [su repositorio de GitHub](https://github.com/Warzone2100/warzone2100) y puede [compilarse](https://github.com/Warzone2100/warzone2100#linux-from-source), mas como este juego está [presente en los repositorios oficiales de Debian](https://packages.debian.org/buster/warzone2100) opté por instalarlo desde éstos, sin necesidad de compilar nada.
~~~bash
sudo apt-get update
sudo apt install warzone2100
~~~

## Configuración de 'Warzone 2100' en Debian
'Warzone 2100' puede ser configurado de 2 formas:
- desde el propio juego
- desde el archivo de configuración, sito en '~/.warzone2100-???/config'  

<br>
<br>
Entradas relacionadas:  
- [Listado oficial de juegos para línea de comandos de Debian](https://hijosdeinit.gitlab.io/listado_oficial_juegos_terminal_cli_debian/)
- [https://hijosdeinit.gitlab.io/tron_online_ssh_terminal/](https://hijosdeinit.gitlab.io/tron_online_ssh_terminal/)
- []()4x
- []()panzer General
- []()panzergeneral saga  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/Warzone_2100">Wikipedia - Warzone 2100</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
