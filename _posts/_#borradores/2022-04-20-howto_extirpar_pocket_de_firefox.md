---
layout: post
title: "[HowTo] Extirpar 'pocket' del navegador web 'Mozilla Firefox'"
date: 2022-04-20
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "mozilla firefox", "firefox", "pocket", "bloatware", "adware", "spyware", "respeto", "privacidad", "maltrato"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small><small>Entorno: máquina virtual ('Virtual Box') Debian 10 *netinstall*, Gnome, i3wm.</small></small>  

<br>
<br>
El navegador web 'Mozilla Firefox', en su configuración por defecto incorpora un componente llamado 'pocket' que, quiera o no quiera el usuario, estará activo hasta que éste no lo desinstale. Puro *bloatware*, a mi entender.  
Es este asunto, junto a la pésima configuración de privacidad por defecto, una de las deficiencias de Firefox hoy.  

<br>
'Pocket' es
