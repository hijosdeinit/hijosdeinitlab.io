---
layout: post
title: "[HowTo] navegadores web para la terminal GNU Linux"
date: 2022-03-20
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "internet", "navegacion", "web", "navegador", "navegador web", "terminal", "cli", "linea de comandos", "browser", "protocolo", "http", "gopher", "gemini", "etica", "privacidad", "anonimato", "respeto", "texto plano", "internet", "internet 4.0", "web 4.0", "icecat", "iceweasel", epiphany browser", "epiphany", "gnome web", "Firefox", "WaterFox", "LibreFox", "LibreWolf", "min browser", "min", "chrome", "chromium", "edge", "icecat", "iceweasel", "basilisk", "palemoon", "pale moon", "amfora", "bombadillo", "lynx"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
- ['lynx'](http://lynx.browser.org/)
- ['links2'](http://links.twibright.com/)
- ['links'](http://www.jikos.cz/~mikulas/links/)
- ['w3m'](http://w3m.sourceforge.net/)
- ['elinks'](http://www.elinks.cz/)
- ['browsh'](https://www.brow.sh/)  

<br>
<br>
Entradas relacionadas:  
- [Protocolo 'GEMINI': retornando a la senda de 'Gopher' en pro de una navegación sin aditivos](https://hijosdeinit.gitlab.io/protocolo_Gemini_retornando_al_camino_Gopher_solucionando_deficiencias_web/)
- [[HowTo] Instalación de 'amfora', navegador gemini para línea de comandos, en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_amphora_gemini_web_browser/)
- ['Gopher' hoy](https://hijosdeinit.gitlab.io/Gopher_hoy/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://en.wikipedia.org/wiki/Lynx_(web_browser)">Lynx web browser - wikipedia</a>  
<a href="https://en.wikipedia.org/wiki/Links_(web_browser)">Links web browser - wikipedia</a>  
<a href="https://en.wikipedia.org/wiki/ELinks">ELinks web browser - wikipedia</a>  
<a href="https://en.wikipedia.org/wiki/W3m">w3m web browser - wikipedia</a>  
<a href="https://en.wikipedia.org/wiki/Browsh">browsh web browser - wikipedia</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
