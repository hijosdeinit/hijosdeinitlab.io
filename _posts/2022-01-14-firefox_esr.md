---
layout: post
title: "Firefox ESR"
date: 2022-01-14
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "ubuntu", "web browser", "navegador", "internet", "internet 4.0", "firefox", "firefox ESR", "firefox beta", "firefoxt nightly", "firefox developer edition", "esr", "beta", "nightly", "developer edition", "min browser", "min", "min browser v.1.20", "chrome", "chromium", "edge", "waterfox", "librefox", "librewolf", "icecat", "iceweasel", "basilisk", "privacidad", "anonimato", "respeto"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
['Firefox ESR (*Extended Support Release*)'](https://www.mozilla.org/en-US/firefox/enterprise/) es -a parte de los *forks* o derivados- una de las [ediciones o "sabores" oficiales](https://www.mozilla.org/en-US/firefox/channel/desktop/) del conocido navegador web ['Mozilla Firefox'](https://www.mozilla.org/en-US/firefox/new/). Otras ediciones son: ['Firefox beta'](https://www.mozilla.org/en-US/firefox/beta/notes/), ['Firefox nightly'](https://www.mozilla.org/en-US/firefox/nightly/notes/), ['Firefox Developer Edition'](https://www.mozilla.org/en-US/firefox/developer/) y ['Firefox Enterprise'](https://www.mozilla.org/en-US/firefox/enterprise/).  

<br>
'Firefox ESR' y 'Firefox' convencional son versiones estables, por consiguiente, aptas para un uso cotidiano. Por el contrario, 'Beta', 'Nightly', y 'Developer Edition' NO son versiones estables; son aptas para desarrollo y pruebas varias.  
'Firefox ESR' está varias versiones por detrás de 'Firefox' convencional.  
'Firefox Beta' y 'Firefox Developer Edition' van 1 versión por delante respecto a la versión de 'Firefox' convencional. 'Firefox Nightly' 2 versiones por delante.  

<br>
'Firefox ESR' es, en definitiva, una edicion estable de soporte a largo plazo que no requiere actualizarse con frecuencia. Las nuevas versiones aparecen aproximadamente 1 vez al año, no cada pocas semanas, lo que equivaldría a 1 versión de 'Firefox ESR' cada 10 versiones de 'Firefox' convencional.  
No obstante, al igual que 'Firefox' convencional, 'Firefox ESR' recibe **parches de seguridad de emergencia**; la aplicación de éstos resultan en una subversión (p.ej. Firefox 68.5.***2***).  
<br>
'Firefox ESR' cuenta de serie con una **configuración de privacidad más severa y restrictiva** respecto a 'Firefox' convencional; el sistema de bloqueo de rastreo está mejorado y el soporte para *DNS sobre HTTPS* está activado por defecto.  
'Firefox ESR' trae por defecto deshabilitadas una serie de características -p.ej. *webrender* o *MITM Detection*- que sí están activadas en 'Firefox' convencional. No obstante pueden activarse si se desea.  
'Firefox ESR' permite incorporarle **extensiones NO firmadas**, cosa que actualmente no permite 'Firefox' convencional.  

<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://victorhckinthefreeworld.com/2019/12/27/para-que-son-las-distintas-versiones-del-navegador-firefox/">VictorHckInTheFreeWorld</a>  
<a href="https://www.softzone.es/programas-top/firefox/firefox-vs-firefox-esr/">SoftZone</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
