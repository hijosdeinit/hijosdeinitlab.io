---
layout: post
title: "[HowTo] youtube-dl: solución 'ERROR: unable to download video data: HTTP Error 403: Forbidden'"
date: 2020-05-01
author: Termita
category: "multimedia"
tags: ["youtube-dl", "youtube", "error", "problema", "web 4.0", "descargar", "ffmpeg", "audio", "video", "multimedia", "degoogled", "privacidad", "publicidad", "web 4.0", "tracking"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---

Ayer fui a descargar mediante **youtube-dl** el audio de un video alojado en Youtube, esa conocida plataforma multimedia de Google que más que eso es red social y publicitaria.  
**Obtuve un error 403**. Concretamente este:  
<br>
![youtube-dl_error403]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-05-01-youtube-dl-solucion-error-403/youtube-dl_error403.png)  
<small>'ERROR: unable to download video data: HTTP Error 403: Forbidden'</small>  
<br>
Inicialmente sospeché que se debía a los <a href="https://hijosdeinit.gitlab.io/howto-cosas-que-hacer-en-pihole-tras/" target="_blank">nuevos filtros antipublicidad de Youtube que incorporé recientemente a PiHole</a>, el agujero negro que se encarga de mandar al guano banners y anuncios publicitarios.
Desconecté **PiHole** unos minutos mas el error 403 de youtube-dl cuando intentaba descargar persistía.  
<br>
Especulé que quizás se debía a la **DNS** que estaba empleando, que por alguna razón Youtube no aceptara descargas resueltas desde éstas. Por especular que no quede, mas antes busqué en la web.
Y hallé la solución.  
<br>
El problema no era debido a interferencias de PiHole, y ni mucho menos al DNS que estaba empleando, sino a la **caché de youtube-dl** que, por alguna razón, estaba interfiriendo en la capacidad de descarga.  
Y esto se soluciona fácilmente ejecutando desde la terminal (CLI):
~~~
youtube-dl --rm-cache-dir
~~~  
![youtube-dl_error403_solución]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-05-01-youtube-dl-solucion-error-403/youtube-dl_error403_solucion.png)  
<br>
Ejecutar youtube-dl con el <a href="https://www.mankier.com/1/youtube-dl#--rm-cache-dir" target="_blank">parámetro '--rm-cache-dir'</a> le insta a borrar todos los archivos que tiene almacenados en su caché.
