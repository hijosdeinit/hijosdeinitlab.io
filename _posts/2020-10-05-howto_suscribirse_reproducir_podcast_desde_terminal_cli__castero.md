---
layout: post
title: '[HowTo] Suscribirse y reproducir podcasts desde la terminal (CLI): castero'
date: 2020-10-05
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "podcast", "audio", "radio", "multimedia", "castero", "CLI", "TUI", "Terminal", "podcatcher"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
[Castero](https://github.com/xgi/castero) es un cliente / gestor de *podcast* TUI (*'Text User Interface'*), es decir, para línea de comandos / Terminal.  
Este *podcatcher* es ligero, intuitivo y -a mi juicio- agradable a la vista. Entre otras funcionalidades, permite suscribirse, sincronizar los capítulos, descargarlos y/o reproducirlos controlando la velocidad, el volumen, etcétera.  

<br>
## Requisitos / Dependencias
Para que 'Castero' funcione requiere las siguientes dependencias:
- Python >= 3.5 (ejecutar `python --version` permite que conozcamos qué versión de 'python' tenemos)
- sqlite3
- Alguno de estos reproductores multimedia: vlc >= 2.2.3 / (mpv y libmpv) >= 0.14.0
- (si el método de instalación que escogemos es mediante 'pip3') 'python3-pip'

<br>
## Instalación de 'castero'
Existen 2 formas de instalar 'castero':
- a) mediante 'pip3'
- b) manual, a partir de script de instalación (python). <small>No he probado este método aún</small>  

<br>
### a) instalación mediante 'pip3'
~~~bash
pip3 install castero
~~~
(*)<small>Hay quien lo instala ejecutando `sudo -H pip3 install castero`</small>  
(**) Para instalar 'castero' mediante 'pip3' es necesario tener instalado el paquete 'python3-pip'.
~~~bash
sudo apt install python3-pip
~~~

<br>
### b) instalación manual a partir de script de instalación (python)
Clonamos el repositorio oficial de 'castero':
~~~bash
git clone https://github.com/xgi/castero
~~~
Nos situamos en la carpeta donde se ha ubicado el repositorio que acabamos de clonar:
~~~bash
cd castero
~~~
Ejecutamos el script de instalación, que está escrito en 'python':
~~~bash
sudo python setup.py install
~~~


<br>
## Funcionamiento de 'castero'
Para arrancarlo hay que ejecutar desde linea de comandos:
~~~bash
castero
~~~
<br>
<a href="/assets/img/2020-10-05-howto_suscribirse_reproducir_podcast_desde_terminal_cli/castero.png" target="_blank"><img src="/assets/img/2020-10-05-howto_suscribirse_reproducir_podcast_desde_terminal_cli/castero.png" alt="interfaz de castero" width="800"/></a>  
<small>interfaz de 'castero', el podcatcher para línea de comandos</small>  

<br>
### **‼** Resolución de problemas:  

1. Si ejecutar el comando `castero` no funcionara puede probarse ejecutar 'castero' de la siguiente forma:
~~~bash
python3 -m castero
~~~
ó
~~~bash
python -m castero
~~~

2. Si al ejecutar el comando `castero` obtuvieramos el error `bash: /usr/local/bin/castero: No existe el archivo o el directorio` debemos copiar el archivo '.local/bin/castero' a '/usr/local/bin/'
~~~bash
sudo cp .local/bin/castero /usr/local/bin/
~~~

3. Si tuviéramos una terminal cuyo fondo no fuera el negro estándar y los textos aparecieran sombreados de un color diferente a de la propia terminal podemos modificar el archivo `{HOME}/.config/castero/castero.conf` para activar la 'transparencia' (sólo en terminales que permitan *compositing*) o establecer un color de fondo (black, blue, cyan, green, magenta, red, white, yellow) más apropiado:
~~~bash
nano ~/.config/castero/castero.conf
~~~
Y, por ejemplo, si deseamos activar la transparencia del fondo editamos la línea 'color_background':
~~~
# The background color of the main interface.
# default: black
# color_background = black
color_background = transparent
~~~

<br>
### Atajos de teclado
    h           - mostrar la pantalla de ayuda
    q           - cerrar el programa
    a           - añadir un feed
    d           - borrar el feed seleccionado
    r           - recargar / refrescar feeds
    s           - guardar episodio para reproducción offline
    UP/DOWN     - navegación arriba/abajo en los menús
    RIGHT/LEFT  - navegación derecha/izquierda en los menús
    PPAGE/NPAGE - scroll arriba/abajo en los menús
    ENTER       - reproducción del feed/episodio seleccionado
    SPACE       - añadir el feed/episodio seleccionado a la cola de reproducción
    c           - limpiar la cola de reproducción
    n           - ir al siguiente episodio de la cola de reproducción
    i           - invertir el orden del menú
    /           - filtrar los contenidos del menú
    m           - marcar episodio como reproducido/no-reproducido
    p or k      - pausar/continuar reproducciendo el episodio actual
    f or l      - buscar hacia delante
    b or j      - buscar hacia atrás
    =/-         - incrementar/decrementar el volumen
    ]/[         - incrementar/decrementar la velocidad de reproducción
    u           - mostrar la URL del episodio
    1-4         - cambiar entre layouts del cliente

<br>
### Configuración de 'castero':

#### ¶ Fichero de configuración
El archivo de configuración se genera automáticamente en `{HOME}/.config/castero/castero.conf` tras la primera ejecución de 'castero'.  

#### ¶ Importar / Exportar feeds
'Castero' permite importar y exportar archivos OPML de las suscripciones de/hacia otros podcatchers.  
La importación / exportación se realiza por línea de comandos. Para conocer todos los parámetros de importación / exportación basta con ejecutar
~~~bash
castero --help
~~~

<br>
### Almacenamiento
Los datos del usuario, incluídos los episodios descargados, y una base de datos con información de los feeds a los que se ha suscrito el usuario, se ubican en `{HOME}/.local/share/castero/`.  
(**‼**) Esos archivos no deben ser modificados manualmente. Borrar la base de datos, por ejemplo, implicaría simplemente que 'castero' genere otra -vacía- la próxima vez que se inicie.  


<br>
## Actualización de 'castero' (si lo instalamos mediante 'pip3')
Si castero fue instalado mediante 'pip3' la actualización se realizará ejecutando:
~~~bash
pip3 install castero --upgrade
~~~
<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[ugeek](https://ugeek.github.io/blog/post/2019-01-26-castero-podcatcher-ligero-para-terminal.html)  
[ochobitshacenunbyte](https://www.ochobitshacenunbyte.com/2018/10/25/podcasts-desde-la-consola-de-comandos-con-castero/)  
[linuxlinks](https://www.linuxlinks.com/castero-command-line-podcast-player/)  
[castero issue 59](https://github.com/xgi/castero/issues/59)</small>  
<br>
<br>
<br>
<br>
