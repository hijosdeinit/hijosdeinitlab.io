---
layout: post
title: "Extirparle el DRM a un ebook .acsm"
date: 2020-04-25
author: Termita
category: "crack"
tags: ["crack", "DRM", "Calibre", "epub", ".pdf", ".acsm", "libro electrónico", "Adobe Digital Editions"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---

Desconocía el tema de primera mano. Jamás pagué por un libro electrónico ni lo tomé en préstamo en una biblioteca pública, que ahora también ofrecen ese servicio.  
Por lo visto el asunto ya tiene años.  
  
Cuando uno obtiene un libro electrónico éste puede venir con DRM, una especie de protección anticopia. Dicen que ese "mecanismo" es totalmente nefasto porque supone que, aún habiendo pagado por el material, no puedes imprimir partes que te puedan interesar ni copiarlo o moverlo a otro dispositivo (aunque sea tuyo). Muchas personas lo ven como una limitación intolerable.  
<br>
<br>
## .acsm ¿qué es?
Hay un formato de libro electrónico, a parte del famoso .epub o .pdf, que tiene la extensión .acsm.
Este formato está relacionado con el software **Adobe Digital Editions**. El archivo .acsm es tan sólo un pequeño fichero XML que contiene un link de descarga para el libro electrónico. Es este libro electrónico, el que -una vez descargado- contendrá el DRM que comprueba si el que trata de leerlo fue el que lo adquirió o no.  
<br>
<br>
**¿Cómo quitarle el DRM a un libro electrónico y convertirlo en un material "limpio", fácil de compartir?**  
<br>
<br>
## Ingredientes
- Máquina -virtual o real- con sistema operativo Windows.
- El software *Adobe Digital Editions* instalado. (**ℹ**) Desconozco otra forma de descargar, a partir del archivo .acsm, libros electrónicos que no sea mediante Adobe Digital Editions y Windows.
- El software **Calibre**, que es *open source*.
- El plugin anti-DRM *DeDRM* para Calibre.

<br>
<br>
El procedimiento es el siguiente:  
<br>
<br>
## 1. Descarga e Instalación de *Adobe Digital Editions*
*Adobe Digital Editions* se [descarga desde su página oficial](https://www.adobe.com/es/solutions/ebook/digital-editions/download.html).  
Tras instalarlo hay que autorizar un usuario en el programa introduciendo *ID de Adobe* y su contraseña. La *Id de Adobe* es el correo electrónico con el que se registra el usuario en la web de Adobe. Si no se dispone de una, hay que registrarse.  
<br>
![autorización de usuario en Adobe Digital Editions]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-25-quitar-DRM-de_ebook_acsm/ade_autorización.png)  
<br>
<br>
## 2. Descarga, Instalación de *Calibre* y adición de plugin *DeDrm_tools*
*Calibre* se [descarga desde su página oficial](https://calibre-ebook.com/download).  
Una vez instalado Calibre, hay que añadirle 2 plugins que previamente han de ser descargados desde el [repositorio de github de DeDRM_tools](https://github.com/apprenticeharper/DeDRM_tools/releases/)  
El archivo de plugins antiDRM descargado ha de descomprimirse y contiene 2 plugins, que habrán de ser añadidos a Calibre (Preferencias → Complementos → Cargar complemento desde un archivo:  
- 'DeDRM_Plugin.zip'
- 'Obook_plugin.zip'

<br>
![Adición del plugin DeDRM_tools]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-25-quitar-DRM-de_ebook_acsm/calibre_añadirplugin.png)  
<br>
<br>
## 3. Descarga del ebook
Cuando adquirimos un libro electrónico se nos solicitará lo típico: si abrir el archivo (.acsm) o si guardarlo.  
<br>
![Elección "abrir con"]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-25-quitar-DRM-de_ebook_acsm/compra2.png)  

Lo preferible es elegir "Abrirlo". Y se abrirá mediante *Adobe Digital Editions*. Automáticamente el libro electrónico se descargará entonces en la carpeta por defecto, que es /Documentos/My Digital Editions  
<br>
![Descarga ebook protegido DRM]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-25-quitar-DRM-de_ebook_acsm/ade_descarga_ebook.png)  
<br>
![ebook protegido con DRM recién descargado por ADE]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-25-quitar-DRM-de_ebook_acsm/adeyadescargado.png)  
<br>
![ebook protegido con DRM recién descargado por ADE en carpeta "My Digital Editions"]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-25-quitar-DRM-de_ebook_acsm/exploradorwindows_ade_mydigitaleditiones.png)  
<br>
<br>
## 4. Extirpación del DRM mediante Calibre y su plugin DeDRM
Ábrase Calibre.  
Añadir libros.  
<br>
![Añadir libros a Calibre]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-25-quitar-DRM-de_ebook_acsm/calibre_importar.png)  
<br>
![Añadir libros a Calibre]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-25-quitar-DRM-de_ebook_acsm/calibre_importar_02.png)  
<br>
![Libro ya importado]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-25-quitar-DRM-de_ebook_acsm/calibre_importado.png)  
<br>
Los libros con DRM que se importan mediante Calibre, por la acción automática de su plugin DeDRM, automáticamente quedan almacenados en la Biblioteca de Calibre **SIN DRM**, listos para disponer de ellos con libertad.  
<br>
<br>
![]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-25-quitar-DRM-de_ebook_acsm/comparación_bibliotecas_exploradordearchivos.png)  
<small>Biblioteca de *Adobe Digital Editions* (libros con DRM) y Biblioteca de *Calibre* (libros sin DRM)</small>
