---
layout: post
title: '¿Harto de Firefox? Alternativas: WaterFox, LibreFox, LibreWolf, min browser, Basilisk, epiphany, etc'
date: 2020-09-26
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "ubuntu", "web browser", "navegador", "navegador web", "internet", "internet 4.0", "Firefox", "WaterFox", "LibreFox", "LibreWolf", "basilisk", "borealis", "min browser", "min", "chrome", "chromium", "edge", "icecat", "iceweasel", "epiphany", "epiphany browser", "privacidad", "anonimato", "respeto"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---

<big>**EN CONSTRUCCIÓN**</big>  

- LibreWolf
- LibreFox
- WaterFox
- min browser
- [Basilisk](https://www.basilisk-browser.org/)
- [Pale Moon](https://www.palemoon.org/)
- [Borealis](https://binaryoutcast.com/projects/borealis/)
- Epiphany / Gnome Web
- etc  

##

<br>
<br>
Entradas relacionadas:  
- []()
- []()
- []()
- []()
- []()
- []()  

<br>
<br>
<br>
---  
<small>Fuentes:  
[]()  
[]()</small>  
<br>
<br>
<br>
<br>
