---
layout: post
title: '[HowTo] Varios métodos de acceso desde GNU Linux a cuenta NextCloud'
date: 2020-08-30
author: Termita
category: "redes"
tags: ["sistemas operativos", "redes", "nubes", "NextCloud", "servicios", "servidor", "OwnCloud", "Nautilus", "sincronización", "Ubuntu"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En Ubuntu y derivados el acceso a las cuentas de NextCloud puede realizarse de varias formas:  
- Mediante la [aplicación cliente específica para GNU Linux que proporciona NextCloud u OwnCloud](https://hijosdeinit.gitlab.io/howto_instalacion_nativa_cliente_nextcloud_debian_y_derivados/). (<small>puede ser instalada de forma nativa (mediante 'apt' o compilando el código fuente), que es -a mi juicio- lo recomendable, o mediante la nueva paquetería ['appimage'](https://nextcloud.com/install/)</small>).  

<a href="/assets/img/2020-08-30-howto_Varios_metodos_acceso_Linux-cuenta_NextCloud/aplicación_cliente_NextCloud.png" target="_blank"><img src="/assets/img/2020-08-30-howto_Varios_metodos_acceso_Linux-cuenta_NextCloud/aplicación_cliente_NextCloud.png" alt="cliente NextCloud" width="800"/></a>  
<small>cliente NextCloud</small>  

<br>
- Configurando una "cuenta online" desde el interfaz de configuración de Ubuntu: Configuración → Cuentas en línea → NextCloud  
<br>

- [Desde la terminal, configurar acceso webdav](https://hijosdeinit.gitlab.io/howto_montar-webdav_desde_cli/)  

<br>
- [Estableciendo un acceso **davs://** (webdav seguro) desde Nautilus (explorador de archivos) a una cuenta específica del servidor webdav de NextCloud](https://hijosdeinit.gitlab.io/howto_Acceso_desde_Nautilus_mediante-webdav_a_cuenta_NextCloud/): Otras Ubicaciones → Conectar al Servidor  

<br>
- Mediante el [cliente oficial de NextCloud para GNU Linux]()   

<a href="/assets/img/2020-08-30-howto_Varios_metodos_acceso_Linux-cuenta_NextCloud/nautilus_conectar_servidor_webdav_NextCloud.png" target="_blank"><img src="/assets/img/2020-08-30-howto_Varios_metodos_acceso_Linux-cuenta_NextCloud/nautilus_conectar_servidor_webdav_NextCloud.png" alt="Nautilus, conexión con servidor webdav NextCloud" width="800"/></a>  
<small>Nautilus, conexión con servidor webdav NextCloud</small>  

<br>
<br>
NextCloud -software de sincronización de ficheros cliente-servidor- emplea el protocolo **webdav**.  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Acceso desde Nautilus mediante webdav a cuenta NextCloud](https://hijosdeinit.gitlab.io/howto_Acceso_desde_Nautilus_mediante-webdav_a_cuenta_NextCloud/)
- [Instalación nativa del cliente oficial NextCloud en Debian 10 y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_nativa_cliente_nextcloud_debian_y_derivados/)
- [[HowTo] Montar webdav desde línea de comandos en cliente GNU Linux](https://hijosdeinit.gitlab.io/howto_montar-webdav_desde_cli/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
