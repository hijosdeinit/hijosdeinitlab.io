---
layout: post
title: "Atajos de teclado del frontend 'smplayer' para el reproductor multimedia 'mplayer'"
date: 2021-09-20
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "i3wm", "configuracion", "config", "bindings", "keybindings", "atajos de teclado", "teclado", "atajo", "shortcut", "servidor x", "x11", "gestor de ventanas", "windows manager"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
EN CONSTRUCCION
<br>
<br>
<br>
['SMPlayer'](https://smplayer.info) es un *frontend* gráfico de código abierto y multiplataforma (<small>GNU Linux, Windows y MacOS</small>) para ['Mplayer'](http://www.mplayerhq.hu/). Entre algunas de sus características, puede recordar la posición de los archivos reproducidos, bregar con videos 4K a 60 fps, soporte de 'Chromecast' (plugin), reproducción de videos de 'Youtube', etc.  
> <small>MPlayer is a free and open-source media player software application. It is available for Linux, OS X and Microsoft Windows. Versions for OS/2, Syllable, AmigaOS, MorphOS and AROS Research Operating System are also available. A port for DOS using DJGPP is also available.[4] Versions for the Wii Homebrew Channel[5] and Amazon Kindle[6] have also been developed.</small>  

<br>
<a href="/assets/img/2022-02-03-atajos_teclado_mpv/smplayer.jpg" target="_blank"><img src="/assets/img/2022-02-03-atajos_teclado_mpv/smplayer.jpg" alt="smplayer" width="300"/></a>  
<small>'smplayer'</small>  
<br>

<br>
Los atajos de teclado por defecto de 'SMPlayer'son:  

<br>
## Reproducción
`espacio` : Play / Pause  
<small>Clicking stop button twice will reset current time to zero.</small>  
`>` : Reproduce el siguiente  
`<` : Reproduce el anterior  
`N` : siguiente  
`P` : previo  
`@` : próximo capítulo  
`!` : previo capítulo  
`Ctrl + A` : Añade nuevo marcador  
`Ctrl + B` : previo marcador  
`Ctrl + N` : siguiente marcador  
`Alt + ↑` : move arriba  
`Alt + ↓` : move abajo  
`Alt + ←` : move izquierda  
`Alt + →` : move derecha  
`Shift + L` : previa radio  
`Shift + H` : siguiente radio  

<br>
## Zoom
`E` : Zoom increase`  
`W` : Zoom decrease`  
`Ctrl + 1` : Zoom 100%  
`Ctrl + 2` : Zoom 200%  
`Shift + E` : Reset  
`Shift + W` : Auto zoom  
`Shift + A` : Zoom for 16:9  
`Shift + S` : Zoom for 2.35:1  

<br>
## Display & OSD
`O` : OSD - Next level  
`Shift + U` : Increase OSD size  
`Shift + Y` : Decrease OSD size  
`1` : Decrease contrast  
`2` : Increase contrast  
`3` : Decrease brightness  
`4` : Increase brightness  
`5` : Decrease hue  
`6` : Increase hue  
`7` : Decrease saturation  
`8` : Increase saturation  
`Ctrl + D` : Toggle double size  
`A` : Next aspect ratio  
`Shift + O` : Show filename on OSD  
`Shift + I` : Show info on OSD  
`I` : Show playback time on OSD  
`D` : Toggle deinterlacing  

<br>
## Subtitles
`J` : Next subtitle  
`Shift + J` : Previous subtitle  
`V` : Subtitle visibility  
`Z` : Subtitle delay decrease  
`X` : Subtitle delay increase  
`R` : Move subtitle up  
`T` : Move subtitle down  
`Shift + R` : Decrease subtitle size  
`Shift + T` : Increase subtitle size  
`G` : Previous line in subtitles  
`Y` : Next line in subtlites  
`Ctrl + →` : Seek to next subtitle  
`Ctrl + ←` : Seek to previous subtitle  

<br>
## Audio
`M` : Mute  
`9` : Volume up  
`0` : Volume down  
`Shift + K` : Previous audio  
`K` : Next audio  
`+` or `=` : Audio delay increase  
`-` or `_` : Audio delay decrease  

<br>
## Seek
`←` or `Ctrl + Shift + B` : 10 seconds backward  
`→` or `Ctrl + Shift + F` : 10 seconds forward  
`↓` : 1 minute backward  
`↑` : 1 minute forward  
`Page Down` : 10 minutes backward  
`Page Up` : 10 minutes forward  
`Ctrl + J` : Jump to...  

<br>
## Playback speed
`Backspace` : Normal speed  
`}` or `Shift + ]` : Half speed of video  
`}` or `Shift + [` : Double speed of video  

<br>
## Screenshot (6 shortcuts)
`S` : Take a snapshot  
`Ctrl + Shift + S` : Screenshot with subtitles  
`Ctrl + Alt + S` : Screenshot without subtitles  
`Shift + D` : Start / stop taking multiple screenshots  
`F` or `Ctrl + T` : Full screen  

<br>
## Misceláeno
`F5` : Show main toolbar  
`Ctrl + P` : Preferences  
`Ctrl + L` : Playlist  
`Ctrl + I` : Information and properties  
`F11` : YouTube browser  
`Ctrl + M` : Mplayer / mpv log  
`Ctrl + S` : SMplayer log  
`F` or `Ctrl + T` : Full screen  
`Esc` : Leave fullscreen  
`Ctrl + C` : Kompakt mod  
`Ctrl + E` : Equalizer  
`Ctrl + F` : Open file  
`Ctrl + U` : Open URL  
`Ctrl + X` : Close  
`Ctrl + Q` : Quit program  

<br>
<br>
<br>
Entradas relacionadas:  
- []())  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://defkey.com/smplayer-shortcuts">DefKey - 'smplayer'</a>  
<a href="">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
