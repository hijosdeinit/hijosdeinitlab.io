---
layout: post
title: "[HowTo] Formas diversas de creación de SD ô dispositivo de almacenamiento USB de arranque para RaspBerry Pi"
date: 2021-09-09
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "iso", "img", "imagen", "instalación", "desplegar", "deploy", "quemar", "burn", "raspberry pi imager", "rpi-imager", "pibakery", "snap", "apt", "balena etcher", "rufus", "win 32 disk imager", "raspberry pi", "raspbian", "raspberry pi os", "gnu linux", "windows", "macos", "dd"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Existen diversas formas de preparar el dispositivo de almacenamiento -microSD ô USB- desde el que arrancará RaspBerry Pi, esa fabulosa máquina.  

<br>
<br>
## <big>¬1.</big> desde GNU LINUX

<br>
### 1.1 comando 'dd' (ô, gráficamente, 'gnome-disks')
~~~bash
sudo dd if=nombredelaimagendelsistemaoperativo of=/dev/sdX bs=4M conv=fsync status=progress
~~~
<small>El parámetro <span style="background-color:#042206"><span style="color:lime">'bs'</span></span> es el **tamaño de los bloques** que se leerán y escribirán. Un tamaño de bloque establecido en '4M' funcionará en prácticamente todas las ocasiones; si no, basta con establecerlo en '1M', aunque el proceso tomará más tiempo.</small>  
<small><span style="background-color:#042206"><span style="color:lime">'conv=fsync'</span></span>: Algunos dispositivos utilizan búferes y cachés para mejorar su rendimiento y latencia. Este parámetro hace que el dispositivo vacíe sus búferes y cachés para que, si se quita el dispositivo, los datos se escriban en él antes de que la operación se marque como completa y el control se devuelva al indicador de la terminal. Este parámetro fuerza una escritura de los datos antes de terminar el proceso.</small>  

<br>
### 1.2 ['Balena Etcher'](https://www.balena.io/etcher/)
Existe una versión 'appimage' y una versión 'electron'.  
Aún desconozco si hay una forma **nativa** (mediante <span style="background-color:#042206"><span style="color:lime">'apt'</span></span> o <span style="background-color:#042206"><span style="color:lime">'dpkg'</span></span>, por ejemplo) de incorporar 'Balena Etcher' a un sistema GNU Linux.  

<br>
<a href="/assets/img/2021-09-09-howto_formas_creacion_de_sd_o_dispositivo_almacenamiento_usb_arranque_raspberrypi/balena_etcher.gif" target="_blank"><img src="/assets/img/2021-09-09-howto_formas_creacion_de_sd_o_dispositivo_almacenamiento_usb_arranque_raspberrypi/balena_etcher.gif" alt="funcionamiento de 'Balena Etcher'" width="400"/></a>  
<small>funcionamiento de 'Balena Etcher'</small>  

<br>
### 1.3 ['RaspBerry Pi Imager'](https://www.raspberrypi.org/software/)

<br>
<a href="/assets/img/2021-09-09-howto_formas_creacion_de_sd_o_dispositivo_almacenamiento_usb_arranque_raspberrypi/raspberry_pi_imager.png" target="_blank"><img src="/assets/img/2021-09-09-howto_formas_creacion_de_sd_o_dispositivo_almacenamiento_usb_arranque_raspberrypi/raspberry_pi_imager.png" alt="'RaspBerry Pi Imager'" width="400"/></a>  
<small>'RaspBerry Pi Imager'</small>  

<br>
### 1.4 ['PiBakery'](https://pibakery.org/)
[repositorio código fuente en GitHub](https://github.com/davidferguson/pibakery/), a partir del cual podríase compilar 'PiBakery' en GNU Linux.  

<br>
<br>
<br>
## <big>¬2.</big> desde WINDOWS

<br>
### 2.1 ['Balena Etcher'](https://www.balena.io/etcher/)

<br>
### 2.2 ['RaspBerry Pi Imager'](https://www.raspberrypi.org/software/)

<br>
### 2.3 ['PiBakery'](https://pibakery.org/)

<br>
### 2.4 ['Win 32 Disk Imager'](https://sourceforge.net/projects/win32diskimager/)

<br>
<a href="/assets/img/2021-09-09-howto_formas_creacion_de_sd_o_dispositivo_almacenamiento_usb_arranque_raspberrypi/win32diskimager-1.0.0.jpg" target="_blank"><img src="/assets/img/2021-09-09-howto_formas_creacion_de_sd_o_dispositivo_almacenamiento_usb_arranque_raspberrypi/win32diskimager-1.0.0.jpg" alt="'Rufus'" width="400"/></a>  
<small>'Win 32 Disk Imager'</small>  

<br>
### 2.5 ['Rufus'](https://rufus.ie/es/)

<br>
<a href="/assets/img/2021-09-09-howto_formas_creacion_de_sd_o_dispositivo_almacenamiento_usb_arranque_raspberrypi/Rufus_3.13.1730.png" target="_blank"><img src="/assets/img/2021-09-09-howto_formas_creacion_de_sd_o_dispositivo_almacenamiento_usb_arranque_raspberrypi/Rufus_3.13.1730.png" alt="'Rufus'" width="400"/></a>  
<small>'Rufus'</small>  

<br>
<br>
<br>
## <big>¬3.</big> desde MAC-OS

<br>
### 2.1 ['Balena Etcher'](https://www.balena.io/etcher/)

<br>
### 2.2 comando ['dd'](https://www.cyberciti.biz/faq/how-to-create-disk-image-on-mac-os-x-with-dd-command/)
~~~
diskutil list
sudo dd if=nombredelaimagendelsistemaoperativo of=/dev/sdX bs=4M
~~~

<br>
### 2.3 ['RaspBerry Pi Imager'](https://www.raspberrypi.org/software/)

<br>
### 2.4 ['PiBakery'](https://pibakery.org/)

<br>
<a href="/assets/img/2021-09-09-howto_formas_creacion_de_sd_o_dispositivo_almacenamiento_usb_arranque_raspberrypi/pibakery.png" target="_blank"><img src="/assets/img/2021-09-09-howto_formas_creacion_de_sd_o_dispositivo_almacenamiento_usb_arranque_raspberrypi/pibakery.png" alt="'PiBakery'" width="400"/></a>  
<small>'PiBakery'</small>  
<br>

<br>
<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] creación de SD o dispositivo de almacenamiento USB de arranque para RaspBerry Pi mediante 'Raspberry Pi Imager'](https://hijosdeinit.gitlab.io/howto_raspberrypi_imager_creacion_de_sd_o_dispositivo_almacenamiento_usb_arranque/)
- [[HowTo] Mostrar el progreso de un backup con 'dd'](https://hijosdeinit.gitlab.io/howto_dd_progreso_verbose/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.raspberrypi.org/blog/raspberry-pi-imager-imaging-utility/">RaspBerryPi.org - 'RaspBerry Pi Imager' info</a>  
<a href="https://www.cyberciti.biz/faq/how-to-create-disk-image-on-mac-os-x-with-dd-command/">CyberCiti.biz - 'dd' MacOS</a>  
<a href="https://www.raspberrypi.org/documentation/computers/getting-started.html">RaspBerryPi.org - dd</a>  
<a href="https://atareao.es/como/creando-un-usb-de-arranque-desde-el-terminal-en-ubuntu/">Atareao.es</a>  
<a href="https://abbbi.github.io/dd/">abbi.github.io - 'dd, bs= and why you should use conv=fsync'</a>  
<a href="https://superuser.com/questions/622541/what-does-dd-conv-sync-noerror-do">SuperUser.com - 'conv=sync'</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
