---
layout: post
title: "[HowTo] Activación del área de notificaciones de las aplicaciones ('system tray') en Debian 10"
date: 2021-04-03
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "netinstall", "system tray", "notificaciones", "bandeja de notificaciones", "retoques", "gnome", "tweaks", "gnome-tweak-tool", "gnome shell", "gnome-shell-extension-appindicator", "kstatusnotifier"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Cuando la instalación de Debian 10 ha sido de tipo "netinstall" hay elementos que no vienen por defecto.  
Uno de estos elementos es la bandeja de notificaciones de los programas. Y cuando trabajamos en Debian 10 con el escritorio 'Gnome' se echa de menos: Cuando minimizamos una aplicación como, por ejemplo, Telegram su icono no aparece en el área de notificaciones, que es la zona que suele estar en la barra de sistema, al lado de la hora o los indicadores de red y volumen.  
Para incorporar ese área de notificaciones basta con hacer lo siguiente:

<br>
Instalar la extensión 'appindicator' de 'gnome-shell':
~~~bash
sudo apt-get update
sudo apt install gnome-shell-extension-appindicator
~~~

<br>
Reiniciar el sistema (quizás baste con desloguearse)
~~~bash
sudo reboot
~~~

<br>
Abrir la aplicación 'Retoques', también conocida como 'gnome-tweak-tool', y en el apartado 'Extensiones" activar 'Kstatusnotifieritem/appindicator support'.  

<br>
<a href="/assets/img/2021-04-03-howto_bandeja_notificaciones_system_tray_debian_netinstall/activacion_system_tray_debian_10.png" target="_blank"><img src="/assets/img/2021-04-03-howto_bandeja_notificaciones_system_tray_debian_netinstall/activacion_system_tray_debian_10.png" alt="'Retoques' → Extensiones → 'Kstatusnotifieritem/appindicator support'" width="800"/></a>  
<small>'Retoques' → Extensiones → 'Kstatusnotifieritem/appindicator support'</small>  
<br>

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://ep.gnt.md/index.php/how-to-install-and-enable-gnome-system-tray-icons-in-debian-10/">ep.gnt.md</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
