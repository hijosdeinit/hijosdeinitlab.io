---
layout: post
title: 'Saque todo lo que pueda de los soportes ópticos (cd/dvd), si no lo ha hecho ya'
date: 2020-04-16
author: Termita
category: "almacenamiento"
tags: ["cdrom", "luz", "backup", "respaldo", "almacenamiento", "princo", "verbatim", "problemas", "humedad", "óxido", "disk rot", "dvdrom", "obsolescencia"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Antaño los cds grabables fueron el sistema de copia de seguridad favorito, causaban furor. Luego lo fueron los dvds grabables.  
Eternas discusiones sobre qué marcas, utilidades de quemado -los famosos, Nero, Alkohol 120 (que, por cierto, hoy trae regalo en forma de malware), BlindRead/Write, ImgBurn-, formas y velocidades de quemado, etcétera, etcétera...  
Y eterna era también la aseveración de que aquellos soportes ópticos -fabricados en su mayoría en China- **no eran en absoluto confiables para almacenar copias de seguridad**. Ya no era una cuestión de calidad del soporte sino de que el medio en sí tenía una vida finita a corto plazo.  
<br>
No sólo los soportes ópticos tienen tendencia a "morir", los [dispositivos de almacenamiento "flash" -como las memorias usb o pendrives- también fenecen, bien por agotamiento de su capacidad de escritura o bien por deterioro físico (oxidación, efectos del calor, etc...)](https://hijosdeinit.gitlab.io/otra-faceta-vida-util-memoria-usb/).
<br>
Si el cd o dvd grabable es de mala calidad, ha sido quemado "a lo loco" y/o almacenado expuesto a la luz solar y/o la humedad y/o a "ataques físicos", su contenido posiblemente jamás podrá ser rescatado cuando se precise.  
<br>
Bien, las probabilidades de que un dvd o cd grabable, quemado correctamente y almacenado en lugar seco y oscuro aguante un tiempo razonable son altas. Mas ¿cuánto tiempo? **NO el suficiente** si lo que contienen es importante.  
<br>
No hace demasido se "descubrió" que los soportes ópticos grabables -cd, dvd- padecían una enfermedad llamada "[disk rot](https://en.wikipedia.org/wiki/Disc_rot)" que consistía en el deterioro de la superficie donde los datos son grabados. Literalmente ésta se pudre, se "oxida", cosa visible a simple vista en los casos más extremos. La información jamás podrá ser extraída en su totalidad. Esta enfermedad era favorecida sobre todo por la acción de la humedad.  
<br>
![disk rot]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-16-saque-todo-lo-que-pueda-de-los-soporte/Disc_rot_close_up.png)  
<small>*disk rot*, superficie química de un soporte óptico pudriéndose</small>  
<br>
Las capas protectoras de material plástico -una especie de policarbonato- también se degradan con el tiempo y se produce una especie de "empañamiento".  
A esto cabe añadirle que cada vez será más difícil acceder a lectoras de soportes ópticos. En el remoto caso de que el dvd o cd esté en buenas condiciones... ¿cómo lo leeremos? ¿Se acuerdan de los dikettes de "cincoycuarto" y de "tresymedio", de las cintas de cassette, de las cintas vhs?  
<br>
Por todo esto, si no se ha hecho ya, ha de extraerse la información de todos esos dispositivos. Cuanto antes.  
<br>
En el caso de los dvd y cd grabables, en mi caso, he optado por hacer imagenes .iso o .bin/.cue o .mdf de todo. Sobra decir qué me he encontrado, pese a que fueron grabados a baja velocidad con una buena máquina estaban almacenados en sitio oscuro y seco: un 20% del material tenía fallos. Ese porcentaje, en gran medida, corresponde a soportes marca "Princo" y "Data Safe", aunque también es notable la cantidad de "Traxdata" y "Verbatim" en los que hay información que se ha quedado para no salir nunca más.  
<br>
Si lo que deseamos respaldar son juegos -entre los cuales abundan las protecciones- es preferible usar programas y formatos de imagen que copien también los "sectores" que albergan las protecciones. Para ello he optado por una versión antigua de Alcohol120 y el formato .mdf/.mds  
<br>
Para los datos personales: una simple .iso realizada directamente desde la terminal según el [método tradicional mencionado en otra entrada de este blog](https://hijosdeinit.gitlab.io/howto-crear-desde-el-terminal-la-iso-de/):  
<br>
Si queremos almacenar en nuestro escritorio una .ISO de un dvd que está en /dev/dvd
~~~
dd if=/dev/dvd of=/home/usuario/Escritorio/dvdrom.iso status=progress
~~~  
<br>
ô, tratándose de un una unidad lectora de dvd por usb (/dev/sr0):
~~~
dd if=/dev/sr0 of=/home/usuario/Escritorio/dvdrom.iso status=progress
~~~  
<br>
Si queremos almacenar en nuestro escritorio una .ISO de un cd que está en /dev/cdrom
~~~
dd if=/dev/cdrom of=/home/usuario/Escritorio/cdrom.iso status=progress
~~~  
<br>
<br>
