---
layout: post
title: "[HowTo] split: Trocear archivos en linux"
date: 2020-03-22
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "trocear", "part", "split", "archivos", "cat"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Generalmente las distribuciones GNU Linux traen de serie el comando **_split_**.
Mediante split podemos trocear archivos en pedazos más pequeños.

Por ejemplo, tenemos un archivo llamado 'backup.iso' que ocupa 5gb y lo queremos trocear en pedazos de 1gb:
~~~
split backup.iso -b 1000MB -d backup
~~~
De esta forma el nombre de los 5 trozos de 1gb (1000mb) creados comenzará por 'backup' y, además, la secuencia de los pedazos creados será numérica:  
> backup00  
> backup02  
> backup03  
> backup04  
> backup05  

<br>
<br>

Para unirlos basta con ejecutar el comando cat:
~~~
cat backup* > backup.iso
~~~


