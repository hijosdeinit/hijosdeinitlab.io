---
layout: post
title: "[HowTo] Purgado y rotación de 'logs' de 'docker' para evitar su desmesurado aumento de tamaño"
date: 2021-06-05
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "docker", "logs", "rotacion", "tamaño", "espacio"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<span style="color:orange">**EN CONSTRUCCIÓN**</span>  
Hará un tiempo que me percaté de que me estaba quedando sin espacio en el disco que contenía el sistema operativo.  
> <small>La máquina es RaspBerry Pi 4b 4GB. El sistema operativo: RaspBian Buster. Aunque siempre preferí que las cosas funcionaran de forma nativa (instalándolas mediante <span style="background-color:#042206"><span style="color:lime">`sudo apt install`</span></span>), empleo 'docker' para que 'NextCloud' y 'KanBoard' -ambos en contenedores- coexistan con 'Open Media Vault' (instalado de forma nativa) por la vía fácil. Reconozco que no es la forma en que me gusta hacer las cosas, mas pereza y prisa son malas compañeras.</small>  

La **causa del problema**: los 'logs' de 'docker' habían ido creciendo desmesuradamente, sin rotación de contenido, sin control.  

<br>
Bastaba con hacer una [búsqueda de archivos grandes](https://hijosdeinit.gitlab.io/howto_busqueda_archivos_grandes_cli_gnu_linux/) en raíz ('/') y todos sus subdirectorios para constatar la descomunal talla de los logs de 'docker':
~~~bash
sudo find / -type f -printf '%s %p\n'| sort -nr | head -10
~~~
> `/var/lib/docker/containers/***-json.log` [66gb]  
66 gigabytes son muchos gigabytes, demasiados para un simple log que, por lo visto, acumula innecesariamente el registro de todos los acontecimientos desde que se creó su docker.  

<br>
Como puede verse, los 'logs' de docker son en formato 'json' y se ubican siguiendo el siguiente patrón:  
`/var/lib/docker/containers/{id_del_contenedor}/{id_del_contenedor}-json.log`  

<br>
## Purgar manualmente un 'log' de 'docker'
Está claro que si se les deja desatendidos los 'logs' de 'docker' pueden llegar a ocupar todo el espacio libre existente en el disco. Si el archivo 'json' que contiene el 'log' de un 'docker' ha crecido hasta ocupar una significativa porción del disco, <big>**púrguese**</big> ejecutando desde línea de comandos:  
<span style="background-color:#042206"><span style="color:lime">`sudo truncate -s 0 /var/lib/docker/containers/`*id_del_contenedor*`/`*id_del_contenedor*`-json.log`</span></span>  

<br>
Podríase, asimismo, establecer una tarea de cron (*cronjob*) para purgar regular y automáticamente los registros '.json' de 'docker'. No obstante, a largo plazo, es mejor configurar la rotación de logs (*log rotation*).

<br>
## Configurar la rotación de 'logs' (*log rotation*) de 'docker'




<br>
<br>
Entradas relacionadas:  
- [Purgado del enorme archivo '.log' de 'NextCloud'](https://hijosdeinit.gitlab.io/howto_purgado_log_nextcloud/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://medium.com/free-code-camp/how-to-setup-log-rotation-for-a-docker-container-a508093912b2">medium.com</a>  
<a href="https://stackoverflow.com/questions/31829587/docker-container-logs-taking-all-my-disk-space">StackOverflow</a>  
<a href="https://docs.docker.com/config/containers/logging/configure/#configure-the-logging-driver-for-a-container">docs.docker.com</a>  
<a href="@@@">@@@</a></small>  

https://duckduckgo.com/?t=ffab&q=docker+json.log+big&ia=web

https://duckduckgo.com/?t=ffab&q=consultar+log+json+docker&ia=web

<br>
<br>
<br>
<br>
