---
layout: post
title: "[HowTo] Generación automática de sitemap.xml para blog Jekyll en GitLab"
date: 2020-05-03
author: Termita
category: "programación"
tags: ["blog", "web 4.0", "Jekyll", "Ruby", "GitLab", "GitHub", "programación", "markdown", "texto plano", "html", "estático", "sitemap", "buscador", "araña", "Google"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
en construcción  



<br>
<br>
Entradas relacionadas:
- [[HowTo] Corregir error en Jekyll (Gitlab): versión listen vs versión ruby](https://hijosdeinit.gitlab.io/howto_corregir_error_gitlab_jekyll_listen_vs_ruby/)
- [[HowTo] Corregir error en Jekyll (Gitlab): mis incompatibilidades con la útima versión de ruby disponible](https://hijosdeinit.gitlab.io/howto_corregir_error_gitlab_jekyll_ruby3.4.0/)
- [[HowTo] Creación de un blog Jekyll en GitLab](https://hijosdeinit.gitlab.io/creaci%C3%B3n-blog-jekyll-en-gitlab/)  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://zzzz" target="_blank">zzz</a>  
<br>
<br>
<br>
<br>
