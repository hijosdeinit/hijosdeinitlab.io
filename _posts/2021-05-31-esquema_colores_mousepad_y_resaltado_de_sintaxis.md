---
layout: post
title: "Hay que establecer un 'esquema de colores' activo en 'mousepad' para visualizar el 'resaltado de sintaxis'"
date: 2021-05-30
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "mousepad", "editor de texto", "texto plano", "resaltado de sintaxis", "syntax highlightning", "markdown", "lenguaje de marcas", "exquema de colores"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<big>**ℹ‼**</big> Aunque el **resaltado de sintaxis** esté activo en el editor de texto *'mousepad'* y éste sea el correcto -cosa que hace automáticamente bien-, el **esquema de colores** por defecto -que es ninguno- no lo mostrará (el resaltado de sintaxis).  
Para que 'mousepad' haga patente efectivamente el resaltado de sintaxis es necesario que se establezca un esquema de colores.  

<br>
## Establecimiento del 'esquema de color'
'Ver' → 'Esquema de color'  
ô  
'Editar' → 'Preferencias' → 'Vista' → 'Esquema de colores'  

<br>
## Comprobar / establecer el 'resaltado de sintaxis' del documento actual
'Documento' → 'Tipo de Archivo'  

<br>
Anexo:  
Los esquemas de colores disponibles por defecto en 'mousepad' generalmente son:
- 'nada' (este es el que no muestra resaltado de sintaxis)
- 'clásico'
- 'cobalto' (el que suelo escoger porque a mi entender muestra mejor el resaltado *markdown*)
- 'kate'
- 'oblivion'
- 'solar claro'
- 'solar oscuro'
- 'tango'  

<br>
<br>
Entradas relacionadas:  
- [Añadir resaltado de sintaxis al editor de texto 'nano'](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [Repositorio con todos los resaltados de sintaxis para 'nano'](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [Instalacion de 'atom' en debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [Evitar que 'atom' elimine espacios en blanco al final de linea](https://hijosdeinit.gitlab.io/howto_atom_espacios_blanco_final_linea/)
- [Listado de editores de texto / código / IDEs interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/]
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)  

<br>
<br>
<br>
--- --- ---  

<br>
<br>
<br>
<br>
<br>
<br>
