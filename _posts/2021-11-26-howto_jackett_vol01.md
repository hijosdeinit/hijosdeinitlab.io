---
layout: post
title: "[HowTo] 'jackett', el buscador de torrents, vol.01"
date: 2021-11-26
author: Termita
category: "descargas"
tags: ["descargas", "sistemas operativos", "gnu linux", "linux", "windows", "jackett"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
EN CONSTRUCCION  
<br>
<br>
<br>
['jackett'](https://github.com/Jackett/Jackett) es un buscador de torrents que se instala en nuestro sistema como un programa convencional o, incluso, como un servicio. Funciona como un servidor al que se le agregan 'trackers' de tal forma que se puede buscar contenido en ellos. Integrado con otras herramientas como @@@@ o @@@@ facilita, por ejemplo, la descarga de "colecciones" (series).  
@@@@@@  

<br>
## Instalación convencional (no como servicio) de 'jackett'
Lo primero, descargar la última versión desde la [sección *'releases'* del repositorio de 'jackett' en GitHub](https://github.com/Jackett/Jackett/releases).
~~~bash
wget 'https://github.com/Jackett/Jackett/releases/download/v0.20.53/Jackett.Binaries.LinuxAMDx64.tar.gz'
tar xzvf 'Jackett.Binaries.LinuxAMDx64.tar.gz'
~~~
Para iniciar 'jackett':
~~~bash
cd Jackett
./jackett
~~~
Para parar 'jackett' basta con pulsar <span style="background-color:#042206"><span style="color:lime">'ctrl'</span> + <span style="background-color:#042206"><span style="color:lime">'c'</span></span>  
Para acceder a 'jackett': <span style="background-color:#042206"><span style="color:lime">`http://localhost:9117`</span></span> ô <span style="background-color:#042206"><span style="color:lime">`http://ipdelamaquinadondeseejecutajackett:9117`</span></span>.  

<br>
<br>
## Agregar 'indexadores' a 'jackett'
Tal cual, recién desplegado, 'jackett' no tiene ningún "sitio" donde buscar torrents. Es necesario agregarle 'indexadores'.  
 
<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  
<br>
<br>
<br>
<br>
<br>
<br>
