---
layout: post
title: "[HowTo] Extracción de imagenes de un video mediante ffmpeg (GNU Linux)"
date: 2021-07-11
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "ffmpeg", "screencast", "captura", "pantalla", "multimedia", "audio", "video", "transcodificacion", "compresion", "imagen", "thumbnail"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Bien porque se requiera una imagen o *thumbnail* que defina un video, bien porque se desea manipular un fotograma, etcétera... puede quererse extraer una imagen de un archivo de video.  
Con el comando 'ffmpeg' es tarea fácil:  
<br>
<span style="background-color:#042206"><span style="color:lime">`ffmpeg -i` *archivo_de_video* `-ss` hh:mm:ss `-t` *duración_en_segundos* `-r` número_de_imagenes_por_segundo* `-f image2 image%03d.jpg`</span></span>  

<br>
por ejemplo:
~~~bash
ffmpeg -i 2021-07-11-howto_modificacion_prompt_cli.mp4 -ss 00:00:01 -t 5 -r 30 -f image2 image%03d.jpg
~~~
... extraerá 30 imágenes por segundo desde el primer segundo hasta el segundo número 6 -es decir, durante 5 segundos- y las guardará siguiendo el patrón definido al final de la línea del comando. Un total de 150 imagenes '.jpg'  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Extraer mediante ffmpeg el audio de un archivo de video](https://hijosdeinit.gitlab.io/howto-ffmpeg-Extraer-audio-de-archivo-de-video/)
- [[HowTo] Convertir vídeo ‘.ogv’ a otros formatos](https://hijosdeinit.gitlab.io/howto_convertir_ogv_a_otros_formatos/)
- [[HowTo] Incorporar pista de audio a una pista de video](https://hijosdeinit.gitlab.io/howto_incorporar_pista_audio_a_video/)
- [[HowTo] Trocear un video](https://hijosdeinit.gitlab.io/howto_trocear_video/)
- [[HowTo] Cálculo del bitrate de un video para su recompresión en 2 pasadas](https://hijosdeinit.gitlab.io/howto_calculo_bitrate_video_recompresion_2_pasadas_ffmpeg_handbrake/)
- [[HowTo] instalacion de HandBrake en Ubuntu y derivados](https://hijosdeinit.gitlab.io/howto_instalacion-handbrake-ubuntu-apt/)
- [[HowTo] Capturar la pantalla mediante 'ffmpeg'](https://hijosdeinit.gitlab.io/howto_capturar_pantalla_mediante_ffmpeg/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://stackoverflow.com/a/29511463">Scott Stensland en StackOverflow</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
