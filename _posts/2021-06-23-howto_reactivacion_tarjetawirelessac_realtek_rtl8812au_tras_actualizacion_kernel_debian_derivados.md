---
layout: post
title: "[HowTo] Reactivación de tarjeta wireless ac 'Realtek RTL8812AU' tras actualización del kernel en Debian y derivados"
date: 2021-06-23
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "ubuntu", "compilar", "compilacion", "make", "install", "instalar", "instalacion", "controlador", "driver", "modulo", "kernel", "clean", "desinstalacion", "desinstalar", "RTL8812AU", "realtek", "wireless", "aircrack", "linux-headrs", "dkms", "firmware"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Ayer actualicé el sistema Debian 10 Buster de mi computadora. La actualización era "potente", se actualizó el 'kernel' desde la versión '4.19.0.16-amd64' a la versión '4.19.0.17-amd64'.  
Las actualizaciones del 'kernel' pueden ser "delicadas": hay que comprobar después que todo funcione correctamente y, de no ser así, se plantean básicamente 2 opciones:
- "retroceder" al kernel anterior vía 'grub', es decir, seleccionando en el 'grub' arrancar con el kernel anterior.
- solucionar los problemas acontecidos  

En este caso los problemas no revestían gran importancia: aparentemente todo funcionaba bien, salvo la tarjeta wireless ac con chip 'Realtek RTL8812AU' [cuyos controladores había instalado tiempo atrás a mano, compilando, etc](https://hijosdeinit.gitlab.io/howto_instalacion_RTL8812AU_debian/), que no estaba operando correctamente: el gestor de redes no estaba reconociéndola, no había conectividad.  
Opté por solucionar el problema para que la máquina pudiera trabajar con el nuevo kernel y todas sus prestaciones a pleno rendimiento.  

<br>
## Solución del problema
1. Trato de activar el driver de la tarjeta en el nuevo kernel  
Desde la carpeta donde compilé el driver:
~~~bash
sudo dkms install dkms.conf
sudo dkms install realtek-rtl88xxau/5.6.4.2~20201019
~~~
**‼** El sistema informa que **no tiene instalados los 'linux-headers' del kernel actual**: 'linux-headers-4.19.0.17'.  
Como habiendo arrancado con ese nuevo kernel no dispongo de conectividad wireless y no tengo ganas de conectar un cable 'rj45' a la tarjeta de red cableada, decido arrancar con el kernel antiguo, que sí tiene conectividad wireless.
~~~bash
sudo reboot
~~~

2. Arranco con el kernel antiguo (4.19.0.16-amd64)  
En el 'grub' elijo arrancar con el kernel antiguo (4.19.0.16-amd64).  
Abro una terminal e instalo 'linux-headers' del nuevo kernel: 'linux-headers-4.19.0.17-amd64'
~~~bash
sudo apt-get update
sudo apt install linux-headers-4.19.0.17-amd64
~~~

3. Arranco con el kernel nuevo ('4.19.0.16-amd64')  
~~~bash
sudo reboot
~~~
En el 'grub' escojo el kernel nuevo ('4.19.0.16-amd64') y constato que la tarjeta wireless con chip 'Realtek RTL8812AU' funciona, ahora sí, correctamente.  

<br>
<br>
Entradas relaccionadas:  
- [[HowTo] Instalación de tarjetas de red inalámbricas con chip Realtek 'RTL8812AU' en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_RTL8812AU_debian/)
- [[HowTo] Instalación de tarjeta de red inalámbricas Realtek 'RTL8822BE' en Debian 11 y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_RTL8822BE_debian11/)
- [[HowTo] Instalación de los controladores de la tarjeta wireless Realtek RTL8822CE. (Debian 10 y derivados)](https://hijosdeinit.gitlab.io/howto_instalacion_controladores_tarjeta_wireless_Realtek_rtl8822ce/)
- [[HowTo] Reactivación de tarjeta wireless ac 'Realtek RTL8812AU' tras actualización del kernel en Debian y derivados](https://hijosdeinit.gitlab.io/howto_reactivacion_tarjetawirelessac_realtek_rtl8812au_tras_actualizacion_kernel_debian_derivados/)
- [El repositorio de firmare/drivers de GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- [[HowTo] Preparar Debian y derivados para compilar software](https://hijosdeinit.gitlab.io/howto_preparar_debian_para_compilar/)
- []()  

<br>
<br>
<br>
<br>
<br>
<br>

