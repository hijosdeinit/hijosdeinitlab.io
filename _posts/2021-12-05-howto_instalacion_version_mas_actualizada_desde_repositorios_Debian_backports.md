---
layout: post
title: "[HowTo] Instalación de una versión más actualizada de un paquete desde los repositorios oficiales de Debian: 'backports'"
date: 2021-12-05
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "apt", "Debian", "backports", "sources", "sources.list", "software", "instalacion"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En 'Debian' existen unos repositorios oficiales llamados <big>'**backports**'</big> que contienen software más actualizado que en el resto de repositorios oficiales.  
Esto puede ser extremadamente útil. Por ejemplo:  
La versión de 'minetest' que se instala desde los repositorios oficiales convencionales es inferior a la que se instala desde los 'backports' de Debian.  

<br>
### Procedimiento
1. Aseguremonos de que los 'backports' están activados en el archivo '/etc/apt/sources.list'
~~~bash
sudo nano '/etc/apt/sources.list'
~~~
Debe existir la siguiente línea -por ejemplo, **en el caso de 'Debian 10 Buster'-**: <span style="background-color:#042206"><span style="color:lime">`deb http://deb.debian.org/debian `*buster*`-backports main`</span></span>  

2. Instalación del paquete desde el repositorio de 'backports'
Por ejemplo, **en el caso de 'Debian 10 Buster'**
~~~bash
sudo apt-get update
sudo apt install nombredelpaquete/buster-backports
~~~

<br>
<br>
Entradas relacionadas:  
- [[HowTo] '/etc/apt/sources.list': repositorios en Debian y derivados](https://hijosdeinit.gitlab.io/howto_repositorios_sources.list_debian_y_derivados/)
- [[HowTo] Agregar repositorio 'debian unstable' a Debian 10 y similares](https://hijosdeinit.gitlab.io/howto_agregar_repositorio_debian_unstable_debian_y_derivados/)
- [[HowTo] Instalar el comando 'add-apt-repository' en Debian](https://hijosdeinit.gitlab.io/howto_add-apt-repository_en_debian_y_derivados/)
- [[HowTo] Eliminar repositorio agregado a mano ('add-apt-repository') en Debian y derivados](https://hijosdeinit.gitlab.io/howto_eliminar_repositorios_agregados_manualmente_addaptrepository_Debian_derivados/)
- [[HowTo] Solucionar error de GPG cuando la clave pública de un repositorio no está disponible. (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_solucionar_error_gpg_clave_publica_repositorio_Debian_y_derivados/)
- [[HowTo] Eliminar Clave 'Gpg' Cuyo 'Id' Desconocemos. (Debian Y Derivados)](https://hijosdeinit.gitlab.io/howto_eliminar_clave_gpg_id_desconocida_agregada_mediante_apt-key_add_/)
- [El repositorio de firmware / drivers para GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- ['RaspBian' / 'RaspBerry OS' incorpora furtivamente -sin aviso ni permiso- un repositorio de 'Microsoft'. Así no vamos bien](https://hijosdeinit.gitlab.io/repositorio_microsoft_instalado_furtivamente_en_Raspbian_RaspBerryOs/)
- [[HowTo] Extirpar todo rastro del repositorio de MicroSoft que Raspbian / RaspBerry Os instala furtivamente](https://hijosdeinit.gitlab.io/howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://backports.debian.org/Instructions/">backports.debian.org</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
