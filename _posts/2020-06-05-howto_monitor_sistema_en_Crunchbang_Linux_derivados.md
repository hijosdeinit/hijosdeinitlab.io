---
layout: post
title: '[HowTo] Incorporar monitor de sistema a CrunchBang++ y parientes similares'
date: 2020-06-05
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "cpu", "core", "hardware", "procesador", "nproc", "lscpu", "cpuinfo", "CLI", "gnome", "gnome-system-monitor", "xfce4-taskmanager", "lxtask", "htop", "top", "conky", "monitorix", "monitor de sistema", "monitorización", "bashtop", "neofetch"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Pareciera que CrunchBang++ no trae ninguna aplicación de escritorio configurada "de serie" para monitorizar la carga del sistema de una forma **completa**.  
<br>
## Lo que tenemos: Htop y Conky
<big>**[Htop](http://hisham.hm/htop/)**</big> muestra, en su configuración inicial, el porcentaje de cpu, de memoria y los procesos mas no la red ni un esquema gráfico.  
<br>
<big>**[Conky](https://github.com/brndnmtthws/conky)**</big>, tal como viene por defecto, muestra porcentajes de uso de cpu y memoria mas no de la red y tampoco una gráfica.  
<br>
Htop y [Conky](https://wiki.archlinux.org/index.php/Conky_(Espa%C3%B1ol)) se pueden configurar, no obstante, para que sí proporcione esas informaciones y/o características.  
<br>
Hay otras diversas opciones para mostrar los procesos, la carga que éstos suponen al sistema -memoria y cpu- e, incluso (y recomentado), el "consumo" de la red.  
<br>
Antes de todo es conveniente actualizar los repositorios.
~~~
sudo apt-get update
~~~
<br>
<br>
## Lo que podemos incorporar
<br>
### gnome-system-monitor
No se caracteriza por su ligereza.
~~~
sudo apt install gnome-system-monitor
~~~
<br>
### xfce4-taskmanager
Muy ligero.
~~~
sudo apt install xfce4-taskmanager
~~~
<br>
### lxtask
Muy ligero. No muestra información sobre el uso de la red.
~~~
sudo apt install lxtask
~~~
<br>
### Monitorix
Muy completo.
Utiliza un servidor web "incorporado" (*built-in*). Esto puede suponer una ventaja o un inconveniente, dependiendo de las circunstancias.
~~~
sudo apt install monitorix
~~~
Para utilizarlo hay que apuntar un navegador gráfico (lynx no sirve) a:
~~~
http://localhost:8080/monitorix/
~~~
ô
~~~
http://ipdelamáquina:8080/monitorix/
~~~  
<br>
En esta entrada de blog se señalan [más detalles sobre la instalación y configuración de Monitorix](https://hijosdeinit.gitlab.io/HowTo-_Monitorix_instalacion_configuracion_uso_basico/).  
<br>
### BashTop
[BashTop](https://github.com/aristocratos/bashtop/blob/master/bashtop) está escrito en bash. Licencia de código abierto Apache 2.0  
Requiere bash 4.4 o superior.  
Se puede instalar o [adaptar a un script propio](https://github.com/aristocratos/bashtop/blob/master/bashtop). La instalación en Ubuntu y derivados es así:  
~~~
apt-cache policy bash
sudo add-apt-repository ppa:bashtop-monitor/bashtop
sudo apt-get update
sudo apt install bashtop
~~~
<br>
### NeoFetch
[NeoFetch](https://github.com/dylanaraps/neofetch) es un programa escrito en lenguaje bash, que permite ver en la terminal la información básica del hardware y del software instalado.Funciona con Linux (es soportada por más de 50 distribuciones), Windows (de XP a 10), iOS, OS X y BSD (FreeBSD, openBSD, NetBSD).  
~~~
sudo apt install neofetch
~~~

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Monitorización de RaspBerry Pi [volumen 1]: rpi-monitor](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_1/)
- [[HowTo] Monitorización de RaspBerry Pi [volumen 2]: Monitorix, Raspberry-Pi-Status y Monitoriza-Tu-Raspberry](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_2/)
- [[HowTo] Monitorización de RaspBerry Pi [volumen 3]: temperaturas de CPU y GPU, frecuencia y voltaje desde línea de comandos (CLI)](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_3_temperaturas/)
- [[HowTo] Monitorix: instalación, configuracion, uso básico](https://hijosdeinit.gitlab.io/howto_Monitorix_instalacion_configuracion_uso_basico/)
- [[HowTo] 5 comandos para monitorizar la memoria RAM (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_monitorizar_memoria_ram_debian_derivados/)  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[it-swarm.dev](https://www.it-swarm.dev/es/gnome/una-alternativa-ligera-gnome-system-monitor/959757807/)  
[SoloLinux](https://www.sololinux.es/bashtop-el-monitor-de-linux-escrito-en-bash/)</small>  
<br>
<br>
<br>
<br>
