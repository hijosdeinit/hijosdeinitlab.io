---
layout: post
title: "[HowTo] creación de SD ô dispositivo de almacenamiento USB de arranque para RaspBerry Pi mediante 'Raspberry Pi Imager'"
date: 2021-09-10
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "iso", "img", "imagen", "instalación", "desplegar", "deploy", "quemar", "burn", "raspberry pi imager", "rpi-imager", "pibakery", "snap", "apt", "balena etcher", "rufus", "win 32 disk imager", "raspberry pi", "raspbian", "raspberry pi os", "gnu linux", "windows", "macos", "dd"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
['RaspBerry Pi Imager' ('rpi-imager')](https://www.raspberrypi.org/software/) es una aplicación desarrollada por 'RaspBerry Pi Foundation' para facilitar el "quemado" de las imágenes de los sistemas operativos de RaspBerry Pi, valgan las redundancias.  
'RaspBerry Pi Imager' es una modificación de una aplicación anterior llamada [PiBakery](https://www.pibakery.org/), es multiplataforma -tiene versiones para GNU Linux, MS Windows y MacOs- y los dispositivos de arranque creados sirven para cualquier versión de Raspberry Pi.  
<small>No obstante, en lo que a dispositivos de arranque USB refiere, cabe recordar que no toda RaspBerry Pi es capaz de bootear desde USB: las versiones más antiguas no pueden hacerlo, y las nuevas generalmente hay que llevar a cabo un sencillo procedimiento previo -de carácter permanente- para habilitar el arranque desde USB.</small>

<br>
Antes de que apareciera 'RaspBerry Pi Imager' ya existían diversas formas de preparar los dispositivos de arranque -microSD o USB- desde diversos sistemas operativos (GNU Linux, MS Windows, MacOS); véase [esta entrada de este blog]().  

<br>
<br>
## Instalación de 'RaspBerry Pi Imager'

<br>
### <big>¬A</big>. Instalación de 'RaspBerry Pi Imager' en Debian 10 *Buster* mediante *SNAP*
~~~bash
sudo snap install rpi-imager
~~~
Este método es también válido para muchos derivados de Debian, como por ejemplo Ubuntu.  

<br>
<br>
### <big>¬B</big>. Instalación de 'RaspBerry Pi Imager' en Debian 10 *Buster* de forma nativa <span style="color:red">(!)</span>
**NO** he logrado una instalación nativa de 'RaspBerry Pi' imager en Debian 10 *Buster* 64bits porque **la dependencia 'libqt5core5a' en mi sistema es una versión inferior a '5.12.2'**, que es la requerida, y no he encontrado forma de actualizarla.  
El proceso habría de ser el siguiente:
#### *B.1. Instalación de las dependencias
~~~bash
wget http://ftp.br.debian.org/debian/pool/main/g/gcc-11/gcc-11-base_11.2.0-5_amd64.deb
sudo dpkg -i gcc-11-base_11.2.0-5_amd64.deb
wget http://ftp.br.debian.org/debian/pool/main/g/gcc-11/libgcc-s1_11.2.0-5_amd64.deb
sudo dpkg -i libgcc-s1_11.2.0-5_amd64.deb
sudo apt install libqt5core5a
sudo apt install qml-module-qtquick2
sudo apt install qml-module-qtquick-controls2
sudo apt install qml-module-qtquick-layouts
sudo apt install qml-module-qtquick-templates2
sudo apt install qml-module-qtquick-window2
sudo apt install qml-module-qtgraphicaleffects
~~~

<br>
#### *B.2. Descarga e instalación del paquete .deb ('imager_latest_amd64.deb') de 'RaspBerry Pi Imager'
~~~bash
wget 'https://downloads.raspberrypi.org/imager/imager_latest_amd64.deb'
sudo dpkg -i imager_latest_amd64.deb
~~~
Y es entonces cuando se produce el error de instalación:
> **‼** Error rpi-imager depende de libqt5core5a (>= 5.12.2); sin embargo La versión de `libqt5core5a:amd64' en el sistema es 5.11.3+dfsg1-1+deb10u4.  

<br>
<br>
### <big>¬C</big>. Instalación de 'RaspBerry Pi Imager' en Ubuntu
~~~bash
wget 'https://downloads.raspberrypi.org/imager/imager_latest_amd64.deb'
sudo dpkg -i imager_latest_amd64.deb
~~~

<br>
<br>
### <big>¬D</big>. Instalación de 'RaspBerry Pi Imager' en RaspBerry Pi OS
~~~bash
sudo apt-get update
sudo apt install rpi-imager
~~~

<br>
<br>
### <big>¬E</big>. Instalación de 'RaspBerry Pi Imager' en Windows
[Descargar el correspondiente instalador de 'RaspBerry Pi Imager' desde la página oficial](https://downloads.raspberrypi.org/imager/imager_latest.exe) y ejecutarlo.  

<br>
<b>
### <big>¬F</big>. Instalación de 'RaspBerry Pi Imager' en MacOS
[Descargar el correspondiente instalador de 'RaspBerry Pi Imager' desde la página oficial](https://downloads.raspberrypi.org/imager/imager_latest.dmg) y ejecutarlo.  

<br>
<br>
<br>
## Funcionamiento de 'RaspBerry Pi Imager
<br>
<a href="/assets/img/2021-09-10-howto_raspberrypi_imager_creacion_de_sd_o_dispositivo_almacenamiento_usb/How_to_use_Raspberry_Pi_Imager_Install_Raspberry_Pi_OS_to_your_Raspberry_Pi_Raspbian-ntaXWS8Lk34.gif" target="_blank"><img src="/assets/img/2021-09-10-howto_raspberrypi_imager_creacion_de_sd_o_dispositivo_almacenamiento_usb/How_to_use_Raspberry_Pi_Imager_Install_Raspberry_Pi_OS_to_your_Raspberry_Pi_Raspbian-ntaXWS8Lk34.gif" alt="funcionamiento de 'RaspBerry Pi Imager'" width="500"/></a>  
<small>funcionamiento de 'RaspBerry Pi Imager'</small>  
<br>

Básicamente se trata de insertar la tarjeta microSD o el dispositivo de almacenamiento USB que deseamos que albergue el sistema operativo a quemar, ejecutar 'Raspberry Pi Imager', y seleccionar origen -la imagen que se desea quemar- y destino -tarjeta microSD o dispositivo de almacenamiento USB-.  

<br>
<br>
<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.raspberrypi.org/blog/raspberry-pi-imager-imaging-utility/">RaspBerryPi.org</a>  
<a href="https://opensource.com/article/20/4/raspberry-pi-imager-mac">OpenSourve.com</a>  
<a href="https://snapcraft.io/install/rpi-imager/debian">SnapCraft.io</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
