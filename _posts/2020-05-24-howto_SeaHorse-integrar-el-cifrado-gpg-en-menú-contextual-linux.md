---
layout: post
title: "[HowTo] SeaHorse: integrar el cifrado gpg en el menú contextual del gestor de archivos en Gnome (nautilus, nemo, caja, etc)"
date: 2020-05-24
author: Termita
category: "cifrado"
tags: ["sistemas operativos", "Linux", "encriptación", "cifrado", "criptografía", "gpg", "GnuPG", "OpenPGP", "archivos", "Windows", "kleopatra", "kde", "Ubuntu", "Debian", "clave privada", "clave pública", "SeaHorse"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Hace un par de días instalé [Kleopatra en Ubuntu (Gnome) sin conseguir hacerlo funcionar](https://hijosdeinit.gitlab.io/HowTo-instalacion-kleopatra-ubuntu-derivados/), asunto que continuaré intentando.  
Como he seguido buscando y aprendiendo formas de cifrar desde GNU Linux y desde Windows, la entrada de hoy tratará de cómo integrar el cifrado gpg en el menú contextual del gestor de archivos de linux: nautilus, caja, etcétera.  
Esto **sólo funciona para escritorios GNU Linux basados en Gnome**.
<br>
Para el gestor de archivos Nautilus, por ejemplo, el procedimiento sería:
~~~
sudo apt-get update
sudo apt install seahorse-nautilus
nautilus -q
~~~  
<br>
A partir de este momento, en Nautilus, haciendo *click derecho* sobre cualquier archivo aparecerá una nueva operación en el menú contextual: "*cifrar*". Si hacemos *click derecho* sobre un archivo ya cifrado aparecerá en el menú contextual la opción de "*descifrar*".  
<br>
![]()
<small></small>  
<br>






el archivo cifrado resultante tiene la extensión .pgp (mediante Kleopatra la extensión es .gpg)


<br>
---  
<small>Fuentes:  
[TechRepublic](https://www.techrepublic.com/article/how-to-enable-encryptdecrypt-menu-option-in-the-ubuntu-file-manager/)</small>
