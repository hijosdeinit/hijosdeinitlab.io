---
layout: post
title: "[HowTo] Comprobación de que un directorio y sus subdirectorios se han transferido correctamente a su destino"
date: 2019-04-18
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "windows", "backup", "respaldo", "comparacion", "catalogacion", "comparar", "comparar contenidos", "contenido", "almacenamiento", "disco", "directorio", "archivo", "fichero", "calculo", "diff", "diffmerge", "total commander", "winmerge", "tar", "md5", "md5sum", "suma de verificacion", "sha256", "sha256sum", "transferir", "copiar", "sincronizar", "sort", "cli"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
El título bien podría ser otro -el procedimiento podría servir para otra cosa-, sin embargo lo que explicaré a continuación lo he empleado justo para eso.  
El proceso básicamente es:
- copiar ô transferir
- crear suma de verificación de original y de copia
- ordenar las sumas de verificación contenidas en ambos archivos de texto resultantes
- comparar ambos archivos de texto  

<br>
<br>
<br>
## Procedimiento
¬<big>1</big>. Cópiese (ô sincronícese, etc) la carpeta y sus subcarpetas en otro destino. Lo haremos de nuestra manera favorita, como nos dé la gana.  

<br>
¬<big>2</big>. Ábranse 2 terminales, y en cada una de ellas nos dirigimos a la carpeta en cuestión (la original y la copiada).  
Vamos a crear dos ficheros, cada uno de ellos con las sumas de verificación de las carpetas (y sus subcarpetas) original y copia respectivamente.  
<br>
2.1. En la terminal en la que nos hemos ubicado en la carpeta original ejecútese:  
<span style="background-color:#042206"><span style="color:lime">`find . -type f -exec sha256sum "{}" + > `*/rutaquequeramos/sha256sum_delORIGINAL.txt*</span></span>  
<br>
2.2. En la terminal en la que nos hemos ubicado en la carpeta copia ejecútese:  
<span style="background-color:#042206"><span style="color:lime">`find . -type f -exec sha256sum "{}" + > `*/rutaquequeramos/sha256sum_delaCOPIA.txt*</span></span>  
Habremos creado así, en la ubicación que nos haya dado la gana, **2** archivos de texto con las sumas de verificación de todos los archivos de las carpetas (y subcarpetas) original y copiada, respectivamente.  

<br>
¬<big>3</big>. Puede ocurrir que ambos ficheros contengan las mismas sumas de verificación, mas no en el mismo orden. El comando ['diff'](https://es.wikipedia.org/wiki/Diff) percibiría que los contenidos no son iguales (cuando, a pesar de no estar en el mismo orden, sí lo son)  
Por eso, desde cualquiera de las terminales, es conveniente ordenarlos:  
<br>
<span style="background-color:#042206"><span style="color:lime">`cat /rutaquequeramos/sha256sum_delORIGINAL.txt | sort > `*/rutaquequeramos/sha256sum_delORIGINAL_ordenado.txt*</span></span>  
<br>
<span style="background-color:#042206"><span style="color:lime">`cat /rutaquequeramos/sha256sum_delaCOPIA.txt | sort > `*/rutaquequeramos/sha256sum_delaCOPIA_ordenado.txt*</span></span>  

<br>
¬<big>4</big>. Y, para finalizar, desde cualquiera de las terminales, comparamos con 'diff' ambos archivos de texto con las sumas de verificación de ambas carpetas -original y copia- ordenadas:  
<span style="background-color:#042206"><span style="color:lime">`diff /rutaquequeramos/sha256sum_delORIGINAL_ordenado.txt `*/rutaquequeramos/sha256sum_delaCOPIA_ordenado.txt*</span></span>  
Si todo ha ido bien no nos dirá nada. Si hay errores, 'diff' los listará.

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Averiguar el tamaño de un directorio desde línea de comandos (CLI) mediante 'du'](https://hijosdeinit.gitlab.io/howto_averiguar_tama%C3%B1o_directorio__cli_find_du/)
- [[HowTo] Cálculo del número de archivos y subdirectorios que contiene un directorio mediante 'find'](https://hijosdeinit.gitlab.io/howto_calcular_numero_archivos_y_o_subdirectorios__find/)
- [[HowTo] Comparación de la suma de verificación md5 de varios directorios mediante 'tar'](https://hijosdeinit.gitlab.io/howto_comparacion_md5sum_varios_directorios_mediante_tar/)
- [[HowTo] Comparación de discos, directorios y archivos](https://hijosdeinit.gitlab.io/howto_comparacion_discos_directorios_archivos/)
- [[HowTo] 'diff'. Comparar directorios ô archivos](https://hijosdeinit.gitlab.io/howto_comparar_directorios_o_archivos/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.svennd.be/recursively-md5sha1sha256sha512-a-directory-with-files/">svennd.be</a>  
<a href="https://blog.desdelinux.net/con-la-terminal-ordenar-alfabeticamente-el-contenido-lineas-de-un-archivo/">DesdeLinux.net</a>  
<a href="https://es.wikipedia.org/wiki/Diff">wikipedia - 'diff'</a>  
<a href="https://gist.github.com/clarkli86/301440e3092fd7d52692">ckarkli86 en gist.github.com</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
