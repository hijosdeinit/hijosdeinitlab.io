---
layout: post
title: "[HowTo] 5 comandos para monitorizar la memoria RAM (Debian y derivados)"
date: 2021-09-09
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "ram", "memoria", "memoria ram", "RAM", "seguridad", "gnu linux", "linux", "debian", "cli", "Monitorix", "Monitoriza-Tu-Raspberry", "monitorización", "procesos", "Raspberry-Pi-Status", "rpi-monitor", "servicios", "servidor", "temperatura", "voltaje", "frecuencia", "servidor"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---

<br>
<big>EN CONSTRUCCION</big>
<br>
<br>

Tan importante como la potencia del procesador es la cantidad y calidad de la memoria RAM. Una máquina con la memoria RAM saturada no funcionará correctamente, llegando incluso al colapso.  
En Debian y derivados -y también la mayoría de familias de GNU Linux- vienen "de serie" varios comandos que, ejecutados desde línea de comandos (CLI), permiten conocer en cada momento "cómo está" la memoria RAM.  

<br>
~~~bash
free
~~~

<br>
~~~bash
cat /proc/meminfo
~~~

<br>
~~~bash
vmstat
~~~

<br>
~~~bash
top
~~~
ô
~~~bash
htop
~~~

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Monitorización de RaspBerry Pi [volumen 1]: rpi-monitor](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_1/)
- [[HowTo] Monitorización de RaspBerry Pi [volumen 2]: Monitorix, Raspberry-Pi-Status y Monitoriza-Tu-Raspberry](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_2/)
- [[HowTo] Monitorización de RaspBerry Pi [volumen 3]: temperaturas de CPU y GPU, frecuencia y voltaje desde línea de comandos (CLI)](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_3_temperaturas/)
- [[HowTo] Monitorix: instalación, configuracion, uso básico](https://hijosdeinit.gitlab.io/howto_Monitorix_instalacion_configuracion_uso_basico/)
- [[HowTo] Incorporar monitor de sistema a CrunchBang++ y parientes similares](https://hijosdeinit.gitlab.io/howto_monitor_sistema_en_Crunchbang_Linux_derivados/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.blog.binaria.uno/2020/04/02/como-verificar-el-uso-de-memoria-desde-la-terminal/">blog.binaria.uno</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
