---
layout: post
title: "[HowTo] Incorporar pista de audio a una pista de video"
date: 2021-01-05
author: Termita
category: "multimedia"
tags: ["multimedia", "ffmpeg", "video", "unir", "partir", "gnu linux", "linux", "sistemas operativos", "pista", "audio"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Suponiendo que a una pista de video se le desea añadir una pista de audio, ambas de igual duración.  
> 'input_pistadevideo.mp4' es la pista de video a la que se desea añadir audio.
> 'input_pistadeaudio.mp3' es la pista de audio que se quiere añadir al video.
> 'output_video_con_audio.mp4' es el resultado, video + audio  

<br>
`ffmpeg -i 'input_pistadevideo.mp4'-i 'input_pistadeaudio.mp3' -c:v copy -c:a copy 'output_video_con_audio.mp4'`  

<br>
Explicación de los parámetros:  
> `-i 'input_pistadevideo.mp4'` = parámetro que define la entrada de video  
> `-i 'input_pistadeaudio.mp3'` = parámetro que define la entrada de audio  
> `-c:v copy` = parámetro que indica que en el proceso el audio sólo hay que copiarlo, sin recodificar  
> `-c:a copy` = parámetro que indica que en el proceso el video sólo hay que copiarlo, sin recodificar  
> `'output_video_con_audio.mp4'` = nombre del archivo salida, que poseerá audio + video  

<br>
<br>
Si se deseara averiguar las características del video original, o del video resultante:
~~~bash
ffmpeg -i *nombredelvideo*
~~~
<br>
<br>
<br>
Otras entradas de este blog al respecto que podrían interesar:  
- [[HowTo] Extraer mediante ffmpeg el audio de un archivo de video](https://hijosdeinit.gitlab.io/howto-ffmpeg-Extraer-audio-de-archivo-de-video/)
- [[HowTo] Trocear un video](https://hijosdeinit.gitlab.io/howto_trocear_video/)
- [[HowTo] Cálculo del bitrate de un video para su recompresión en 2 pasadas](https://hijosdeinit.gitlab.io/howto_calculo_bitrate_video_recompresion_2_pasadas_ffmpeg_handbrake/)
- [[HowTo] reparar mediante mencoder archivo de video dañado](https://hijosdeinit.gitlab.io/howto_reparar_mediante_mencoder_archivo_de_video_da%C3%B1ado/)  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://linuxcenter.es/component/k2/item/97-anadir-una-pista-de-audio-a-un-video-con-ffmpeg">Linux Center Valencia</a>  
<br>
<br>
<br>
<br>
<br>
<br>
