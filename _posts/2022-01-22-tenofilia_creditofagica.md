---
layout: post
title: "Mirando cara a cara al abismo de la incoherencia posmoderna: un vistazo a la Tecnofilia creditofágica"
date: 2022-01-22
author: Termita
category: "internet 4.0"
tags: ["gratis", "servicios", "Ivoox", "Alphabet", "Google", "Facebook", "mendicidad 4.0", "patreon", "mercaderes", "mercantil", "suscripción", "incerteza", "dependencia", "web 4.0", "tecnofilia", "creditofagia", "tecnofilos", "creditofagos", "posmodernidad", "absurdo", "coherencia", "incoherencia", "progreso", "hardware", "software", "sostenible", "verde", "obsolescencia programada", "obsolescencia", "embudo", "retro", "arqueotecnologia", "retromatica", "vim", "i3wm"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Una de las incómodas reflexiones que me han venido a la cabeza mientras preparaba una antigualla para mi día a día es acerca del <big>por qué lo que hoy es puntero y objeto de deseo, mañana no vale. Ese supuesto "progreso" ¿es por nuestro bien o es por el bien de **mercaderes** e **instituciones**?</big>  
<br>
Todas esas corporaciones de *hardware* y de *software* -hoy todas ellas tan sorprendentes como ostentosas sacerdotisas de la **calentología**, la nueva religión "verde"- que se suman a ese rollito de repercutir sobre la humanidad de a pie "lo sostenible", ¿cómo pueden ser tan desapercibidamente cínicas?  
No creo que haga falta que lo diga. Es la ley del embudo: «lo ancho para mí y mis coleguis y lo estrecho para todos vosotros, que sois mayoría».  
<br>
La *Big Tech*, los fabricantes y las instituciones están en el ajo.  
Usted a desguazar su aún plenamente funcional automóvil diesel porque esa chupipandi dice que es poco menos que Satanás y hace que los gatitos tosan y lloren.  
Ellos a fabricar en esa modélica democracia, paraíso proletario, llamada China -sin control alguno de "impahto medioambientáh"- todo tipo de chismes compinchados con los grandes del software, que actualizan sus productos para que -para hacer lo mismo (casi siempre dejarse maltratar con polladas de redes sociales y vejaciones varias)- vd. tenga que comprar esos chismes nuevos y tirar los repentinamente "viejos" -aunque aún funcionen perfectamente, a pesar de que hace cuatro días cuando vd. los compró le hubieran costado un riñón, y costara riñón y medio fabricarlos explotando naturaleza y esclavos en minas y fábricas de ensamblaje... cuando no, fomentando guerras-.

<br>
-- Ej que no me va el whatsapp en este móvil, voy a comprarme otro  
<br>
-·- Querido, usted mismo... ¿cuánto le cuesta a vd. un móvil nuevo?... coste-beneficio... ¿Cuál es el valor de sus contactos de whatsapp? ¿ha barajado vd. dejar de usar whatsapp?
<br>
<br>
Todo el mundo sabe que, entre la gente de a pie, el 90% de las notificaciones de Whatsapp son por tonterías, puro incordio, y que para lo importante -si cabe- está la tradicional llamada telefónica. Whastapp no inventó nada -ya existía IRC, messenger, talk, email, etc...- y hay alternativas menos exigentes y más respetuosas. Pero no. Mejor someterse. Todo muy sostenible y tal y tal.  

<br>
<br>
Dicen que el que escribió 'Juego de Tronos' usa (o usaba hasta no hace demasiado) una computadora PC compatible de los 80 y 'Wordstar' como procesador de textos. Desconozco su situación financiera -pobre es probable que no sea-, mas la cabeza, al menos en lo que a tecnología y porqueyolovalguismo refiere, la tiene bien amueblada, pareciera.  

<br>
<a href="/assets/img/2022-01-22-tenofilia_creditofagica/wordstar_georgerrmartin.png" target="_blank"><img src="/assets/img/2022-01-22-tenofilia_creditofagica/wordstar_georgerrmartin.png" alt="George R.R. Martin haciéndose polvo la columna con Wordstar y una flamante pantalla plana. La leyenda no es tan leyenda :p" width="500"/></a>  
<small>George R.R. Martin haciéndose polvo la columna, con Wordstar y una flamante pantalla plana. La leyenda no es tan leyenda :p</small>  
<br>

Todo este tinglado tecnófilo-creditófago puede llegar a ser hilarante, desconcertante... inquietante. A ver a cuento de qué necesito una máquina nueva con 8 gigas de memoria RAM para NAVEGAR por internet, si antes con un Pentium III y 128 mb. lo hacía perfecta y sobradamente (y ahora no puedo).  
¿Acaso en el 2003 aquellos solitarios adictos al porno vivían cual monjas ursulinas porque sus ordenadores no daban de sí?  
Dicen que ahora van todas rasuradas, ¿tendrá alguna relación con el excesivo consumo de recursos de CPU, RAM y GPU? Habrá que pasarse por algún podcast para *geeks* gordos nuncafollistas de *'Ivoox Orinals'* y preguntar a los expertos :)  

<br>
Y lo llamativo de los *geeks* que, pese al consumismo ciego y porqueyolovalguista que les hace adquirir chismes *últimomodelo-últimaversión* que no necesitan -para impresionar a desconocidos a los que importan una puta mierda- pagan fortunas por un teclado mecánico de los 90... "porque es mejor".  
Sí, hijo, claro que es mejor (quizás si en los 90 no hubieras tirado el que ya tenías...). Y, por hacer alegoría, mejores también eran aquellos aromáticos tomates a granel, sin esas bandejicas de plástico que pagamos hoy, de semilla natural y criados en el campo, que no en invernadero. Sí, *broh*,esa verdura que comprábamos en los ochenta en la frutería y por el que -los mismos que luego te dijeron que, "por higiene", mejor TODO envasado- hoy te hacen pagar una fortuna porque, claro, "es ¡ecológico!". Son esos los mismos que llevan una temporada con el tan cínico como cansino cuento de que "hay que reciclar" su derroche (en y para la empresa del hay-untamiento).  
Y así todo.  
Y los que tienen la sartén por el mango -diríase son psicópatas-: "donde dije digo digo diego, rompo la baraja cuando me salga de los huevos y tú a pagar más por lo mismo". Y yo lo entiendo, es lícito, porque el deber de todo delincuente es escapar, mentir, engañar, delinquir... es su trabajo-, mas no veo que la sociedad occidental moderna sea consciente de la tomadura de pelo.  

<br>
<br>
Absurda posmodernidad.  
<br>
Otra:  
-- "¡No sin mi i7!"  
<br>
Es decir:
("M'hecomprao un maquinote cuasi cuántico -os lo suplico, *laik*, *suscribe* y darme argo en el *patreón* que mañana vienen los del banco a deshauciarme-", pero "para picar código uso 'neoVim' porque es más ligero" (¿o *cool*?) "y babeo con el gestor de ventanas i3wm o *bdsm*")
<br>
<br>
-·- Muchacho, y el resto de recursos... ¿a qué los dedica usted? ¿a hacerse pajas?  

<br>
<br>
Pues sí, todo podría y debería coherentemente ser como el inmortal 'vim': más LIGERO. La hoy, de lo mercantilizada tan pesada, world wide web, por ejemplo.  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Mantener vivo un iPad antiguo, vol.01: instalar aplicaciones 'no tenidas antes'](https://hijosdeinit.gitlab.io/howto_mantener_vivo_ipad_antiguo_vol01__instalar_aplicaciones_no_tenidas_antes/)
- [https://hijosdeinit.gitlab.io/Germen_rendicion_gnulinux_a_obsolescencia/](https://hijosdeinit.gitlab.io/Germen_rendicion_gnulinux_a_obsolescencia/)
- [[HowTo] Instalación netinstall de Debian en netbook Compaq Mini](https://hijosdeinit.gitlab.io/howto_instalacion_netinstall_Debian_netbook_compaqmini/)
- [[HowTo] búsqueda de páginas web ‘desaparecidas’, directamente en la caché de Google](https://hijosdeinit.gitlab.io/howto_busqueda_en_cache_de_google_paginas_desaparecidas/)
- [Saque todo lo que pueda de los soportes ópticos (cd/dvd), si no lo ha hecho ya](https://hijosdeinit.gitlab.io/saque-todo-lo-que-pueda-de-los-soportes/)
- [Otra faceta de la vida útil de las memorias usb](https://hijosdeinit.gitlab.io/otra-faceta-vida-util-memoria-usb/)
- [Qué pedirle a un ‘pendrive’ USB](https://hijosdeinit.gitlab.io/requisitos_de_un_pendrive/)
- ["Gratis"](https://hijosdeinit.gitlab.io/lo_gratis/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
