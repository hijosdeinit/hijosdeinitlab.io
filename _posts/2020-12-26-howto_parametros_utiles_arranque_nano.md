---
layout: post
title: "[HowTo] editor 'nano': parámetros de arranque útiles"
date: 2020-12-26
author: Termita
category: "productividad"
tags: ["sistemas operativos", "gnu linux", "pico", "nano", "editor de texto", "vim", "vi", "emacs", "texto", "programación", "texto plano", "latex", "herramientas", "código", "markdown", "org mode", "pandoc", "keybindings"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Podemos arrancar 'nano' con unos parámetros concretos para que la edición de un documento directamente se lleve a cabo de la manera que se desea específicamente en ese momento, saltándose las opciones definidas en la propia configuración de nano, que en ese momento pudiera interesarnos contradecir.
Alguno de estos parámetros son:  

<br>
<span style="background-color:#042206"><span style="color:lime">`-B`</span></span> ô <span style="background-color:#042206"><span style="color:lime">`--backup`</span></span> : respaldará el archivo que se va a editar.  
<span style="background-color:#042206"><span style="color:lime">`-G`</span></span> ô <span style="background-color:#042206"><span style="color:lime">`--locking`</span></span> : bloquea el archivo que se va a editar para que, mientras, nadie ni nada -salvo nosotros- pueda editarlo.  
<span style="background-color:#042206"><span style="color:lime">`-S`</span></span> ô <span style="background-color:#042206"><span style="color:lime">`--smooth`</span></span> : activará el desplazamiento suave, línea a línea en lugar de media pantalla.  
<span style="background-color:#042206"><span style="color:lime">`-l`</span></span> ô <span style="background-color:#042206"><span style="color:lime">`--linenumbers`</span></span> : mostrará el número de las líneas.  
<span style="background-color:#042206"><span style="color:lime">`-m`</span></span> ô <span style="background-color:#042206"><span style="color:lime">`--mouse`</span></span> : habilita el uso del ratón para posicionar el cursor (implicará no poder usar el ratón para seleccionar texto).  
<span style="background-color:#042206"><span style="color:lime">`-$`</span></span> ô <span style="background-color:#042206"><span style="color:lime">`--softwrap`</span></span> : habilitará el ajuste de línea suave, es decir, las líneas largas no sobrepasarán los bordes.  

<br>
Se puede visualizar una lista completa de parámetros con los que iniciar nano:
~~~
nano -h
~~~
ô
~~~
man nano
~~~

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Atajos de teclado del editor 'nano'](https://hijosdeinit.gitlab.io/howto_atajos_teclado_nano/)
- [[HowTo] Mostrar el número de línea en el editor 'nano' cuando Alt+Shift+3 no funciona](https://hijosdeinit.gitlab.io/nano__mostrar_numeros_linea_cuando_altshift3_nofunciona/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Añadir resaltado de sintaxis al editor de textos NANO](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [En nano no existe overtyping](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/)
- [Expresiones regulares no aceptadas por el editor de texto 'nano'](https://hijosdeinit.gitlab.io/regexp_no_aceptadas_por_nano/)
- [[HowTo] Comienzo con vim. I](https://hijosdeinit.gitlab.io/howto_comienzo_con_vim_1/)
- [[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados](https://hijosdeinit.gitlab.io/howto_micro_editor_texto_debian/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- [[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)
- [[HowTo] El editor (IDE) atom y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzzzzzzzzz">zzzzzzz</a>  
<a href="zzzzzzzzzz">zzzzzzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

