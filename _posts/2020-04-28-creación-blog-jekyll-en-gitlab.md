---
layout: post
title: "[HowTo] Creación de un blog Jekyll en GitLab"
date: 2020-04-28
author: Termita
category: "programación"
tags: ["blog", "web 4.0", "Jekyll", "Ruby", GitLab", "GitHub", "programación", "markdown", "texto plano", "html", "estático", "Hugo", "asciidoc", "gema", "gem", "ci"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
# Introducción
<br>
<br>
## ¿Por qué me resulta interesante un blog estático como [Jekyll](https://jekyllrb.com/)?
- Porque es simple de desplegar y mantener, a la vez que potente.
- Porque hace lo mismo que un blog dinámico sin tanta parafernalia. Ni siquiera requiere base de datos.
- Porque es productivo y prima el contenido. Se redacta en texto plano incorporando lenguaje de marcas -markdown- sin necesidad de editores de texto y maquetadores que pareciera que hacen lo que les da la gana. No requiere CMS.
- Porque es rápido y ligero.
- Porque es "seguro". Su código está disponible para cualquiera que quiera auditarlo.
- Porque me recuerda a la internet 1.0 que conocí.  

<br>
## ¿Por qué despliego un blog Jekyll en [GitLab](https://gitlab.com/)?
- Porque es gratis.
- Porque se respeta la privacidad.
- Porque no existe publicidad. Nadie utilizará el contenido que aportas para insertar publicidad.
- Porque es productivo. Un "hosting" fácil de configurar, mantener y producir.
- Porque se comparte, no sólo los contenidos, sino el propio código.
- Porque permite el uso de git para subir los contenidos: permite trabajar en local y subir lo producido de una forma simple, efectiva y eficiente.
- Porque es "seguro". GitLab se ocupa de ello.
- Porque, llegado el caso, permite enlazar un dominio personal, propio.
- 10 Gb por repositorio. **¿**sin límite de tamaño de archivo (<small>siempre que no supere esos 10 Gb</small>)**?**  

<br>
## ¿Qué es GitLab?
GitLab es fundamentalmente un sistema de alojamiento de repositorios Git, es decir, un hosting colaborativo para proyectos de software gestionados por el sistema de versiones [Git](https://es.wikipedia.org/wiki/Git).  
Asimismo GitLab ofrece otras prestaciones, todas ellas bajo una licencia de código abierto, como son: alojamiento de wikis, un sistema de seguimiento de errores y un sistema de alojamiento web estático. Esto último recibe el nombre de [**GitLab Pages**](https://gitlab.com/pages).  
<br>
![GitLab Pages]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gilab_pages.png)  
<small>GitLab Pages: cosas que permite desplegar</small>  
<br>
Los competidores de GitLab son: [GitHub](https://github.com/), un gigante recientemente comprado por Microsoft, y [BitBucket](https://bitbucket.org/).  
La principal diferencia entre GitLab y sus competidores es que éste se ofrece como un software libre que, si no se desea alojar un projecto en GitLab.com, se puede descargar e instalar el servicio con toda su funcionalidad en cualquier servidor.  
La estrategia de GitLab difiere de la de GitHub (hoy vinculada a Microsoft) entre otros aspectos en que para GitLab es importante que el núcleo de sus productos permanezca siendo *open source*. En GitHub, sin embargo, no existe la certeza de que esto sea así en un futuro.  
Un repositorio de GitHub puede vincularse (sincronizarse) con un repositorio de GitLab.  
Tanto GitLab como GitHub tienen "planes" gratuítos. En ese sentido GitLab ofrece:
- Tenencia de repositorios privados ilimitados, y para tantos colaboradores como se desee, gratuitamente.
- 10 Gb por repositorio. **¿**sin límite de tamaño de archivo (<small>siempre que no supere esos 10 Gb</small>)**?**  

<br>
## ¿Qué es Jekyll?
Jekyll es un generador simple para sitios web estáticos con capacidades de blog; adecuado para sitios web personales, de proyecto o de organizaciones. Fue escrito en lenguaje de programación Ruby por Tom Preston-Werner, el cofundador de GitHub, y se distribuye bajo la licencia de Código abierto MIT.  

<br>
--- --- ---  

El **procedimiento** para construir un blog Jekyll en Gitlab básicamente es este:
- Registro en Gitlab
- Despliegue del software para blogs estáticos Jekyll
- Despliegue del tema visual que más se acomoda a nuestro gusto y propósito
- Preparar Git en directorio local para gestionar y empujar (subir, sincronizar) contenidos al blog
- Retoques en la configuración de Jekyll y el **tema visual** (por ejemplo: incorporación de datos de contacto, incorporación de sistema de búsqueda, incorporación de un sistema de comentarios, etcétera).
- (<small>si se está reubicando (trasladando) un blog ya existente:</small>) Importación del blog antiguo.

<br>
--- --- ---  


# Procedimiento

## Registro en GitLab
[Página de registro de GitLab](https://gitlab.com/users/sign_up).  
Una vez registrados hay que hacer login.  
<br>
<br>
## Despliegue de Jekyll
<br>
1. Realizar "Fork" del proyecto Jekyll de GitLab Pages
Para desplegar Jekyll hay que dirigirse a [GitLab Pages](https://gitlab.com/groups/pages) y realizar un **fork** del proyecto Jekyll.  
<br>
![GitLab Pages - Jekyll]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gilab_pages_jekyll.png)  
<small>proyecto Jekyll en GitLab Pages</small>  
<br>
![GitLab Pages - Jekyll FORK]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gilab_pages_jekyll_fork.png)  
<small>Haciendo "fork" del proyecto Jekyll en GitLab Pages</small>  
<br>
![GitLab Pages - Jekyll FORK.]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gilab_pages_jekyll_fork_02.png)  
<small>Eligiendo el espacio -usuario (o grupo)- destino del fork  
(*) Si existiera algún "grupo" previamente creado debería elegirse si el fork se va ha realizar en el usuario o bien en uno de los grupos creados.</small>  
<br>
![GitLab Pages - Jekyll FORK..]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gilab_pages_jekyll_fork_03.png)  
<small>Proyecto Jekyll en GitLab Pages: fork realizado</small>  
<br>
<br>
2. Romper relación con el proyecto original  
Projects → Your projects → *usuario*/*nombredelproyecto* ...  
<br>
![Selección del proyecto romper relación fork]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gilab_pages_jekyll_romper_relacion_fork.png)  
<small>Selección del proyecto cuya relación fork se desea romper</small>  
<br>
...→ Settings → General → Advanced Settings → Remove fork relationship  
<br>
![Selección del proyecto romper relación fork.]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gilab_pages_jekyll_romper_relacion_fork_02.png)  
<small>Romper relación fork</small>  
<br>
<br>
3. Renombrar el proyecto y establecer los detalles  
Projects → Your projects → *usuario*/*nombredelproyecto* ...  
...→ Settings → General → Naming, topics, avatar  
<br>
![Establecer nombre y detalles del proyecto]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gitlab_jekyll_nombre_y_detalles_proyecto.png)  
<small>Establecer nombre y detalles del proyecto</small>  
<br>
<br>
4. Activación de los *Runners*, que crearán el *Docker*  
Settings → CI/CD → Runners → Shared Runners  
Hay que asegurarse de que 'Shared Runners' estén activados. De no estarlo hay que activarlos.  
<br>
![Activación de Shared Runners]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gitlab_jekyll_shared_runners.png)  
<small>Activación de 'Shared Runners'</small>  
<br>
<br>
5. Ajustar la URL (ruta o *path*) del blog jekyll  
Es probable que se desee **personalizar la url** en que se muestren los resultados -es decir, el blog que estamos construyendo propiamente dicho- de forma que, por ejemplo sea 'https://nuestrousuario.gitlab.io' o 'https://nombregrupo.gitlab.io' o 'https://nombredenuestroproyecto.gitlab.io', etcétera...  
<br>
5.1. Projects → Your projects → *usuario*/*nombredelproyecto* ...  
... → Settings → Advanced Settings → Change path  
<br>
![Establecer *path* del blog]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gitlab_jekyll_url_path_proyecto.png)  
<small>Establecer *path* del blog</small>  
<br>
5.2. Modificar la variable 'baseurl' en el archivo '_config.yml' eliminando el valor 'jekyll' que por defecto trae asignado. Así el blog se "verá" correctamente.  
Projects → Your projects → *usuario*/*nombredelproyecto* ...  
... → Details → _config.yml ...  
... → Edit  
<br>
![Establecer *path* del blog.]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gitlab_jekyll_url_path_configyml.png)  
<small>_config.yml</small>  
<br>
![Establecer *path* del blog..]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gitlab_jekyll_url_path_configyml_02.png)  
<small>Selección del modo edición para el archivo _config.yml</small>  
<br>
![Establecer *path* del blog...]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gitlab_jekyll_url_path_configyml_03.png)  
<small>Modificación variable 'baseurl' en _config.yml</small>  
<br>
**El blog en este momento ya está construído**  
<big>**i**</big> Estando como están configurados "Shared Runners", apenas se confirma la modificación de un fichero -en este caso '_config.yml'-, se puede constatar cómo se pone en marcha un trabajo de construcción del sitio (blog).  
Esto se puede constatar así: CI/CD → Jobs ó CI/CD → Pipelines  
<br>
![CI/CD Trabajos]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gitlab_jekyll_shared_cicd_jobs.png)  
<small>Estado de los trabajos</small>  
<br>
![CI/CD Trabajos.]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gitlab_jekyll_shared_cicd_jobs_02.png)  
<small>Detalle del estado de un trabajo</small>  
<br>
<br>
6. Forzar https (si así se deseara)  
Projects → Your projects → *usuario*/*nombredelproyecto* ...  
...→ Settings → Pages → [x] Force HTTPS (requires valid certificates)  
<br>
![Forzar https]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gitlab_jekyll_forzar_https.png)  
<small>Forzar *https* en el blog</small>  
<br>
Se puede observar, un poco más abajo, la URL del blog.
Como he mencionado antes, a estas alturas el blog ya está construído y su url sería si, por ejemplo se ha cambiado el *path*: 'https://usuario.gitlab.io'
<br>
<br>

## Preparar Git en directorio local para gestionar y empujar (subir, sincronizar) contenidos al blog
Si git no está instalado en nuestro sistema:
~~~bash
sudo apt install git
~~~
Desde la terminal hay que ir al directorio donde se ubicará el blog "en local", por ejemplo '/home/usuario/blogs/'. <small>(*) No hace falta crear ningún subdirectorio "extra" porque esto se hará automáticamente cuando se ejecute el comando git de clonación del repositorio remoto en el repositorio local.</small>
~~~bash
cd /home/usuario/blogs/
~~~
Es necesario conocer **la url del repositorio remoto** de nuestro blog (que está en gitlab.com). Esto se hace así:  
Projects → Your Projects →*usuario*/*nombredelproyecto* ...  
... → "Clone" → Clone with https  
<br>
![URL del repositorio git remoto]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gitlab_jekyll_url_repositorio_git.png)  
<small>URL del repositorio git remoto</small>  
<br>
Conocida la URL del repositorio remoto hay que clonarlo en nuestro directorio local para que se pueda ir trabajando "en local" y subir cuando interese los cambios al remoto. La clonación se realiza así:
~~~bash
git clone urldelrepositorioremoto
~~~
Por ejemplo: git clone https://gitlab.com/*usuariogitlab*/*usuariogitlab*.gitlab.io.git  
Se observa entonces que aparece un subdirectorio nuevo ('/usuariogitlab.gitlab.io').  
Hay que ubicarse, desde la terminal, en ese subdirectorio que acaba de aparecer y que es un espejo (clon) del repositorio remoto que tenemos en GitLab.
~~~bash
cd /home/usuario/blogs/usuariogitlab.gitlab.io
~~~
A continuación hay que inicializar el repositorio git en esa ubicación  
~~~bash
git init
~~~
El sistema habrá de responder:  
> Inicializado repositorio Git vacío en /home/usuario/blogs/usuariodegitlab.gitlab.io/.git/  

Hay que configurar entonces el acceso, la "identidad" para ese repositorio local que es espejo del remoto:  
~~~bash
git config user.name "usuariodegitlab"
git config user.email "dirección_de_email"
git config --list
~~~
Cuando se realizan modificaciones "en repositorio **local**" y se desea "empujarlas" al "repositorio **remoto**" para que ambos estén sincronizados basta con ejecutar:
~~~bash
git add .
git commit -m "texto explicativo de los cambios que empujamos"
git push -u origin master
git status
~~~
Y cuando se realizan modificaciones "en repositorio **remoto**" y se desea traerlas también al "repositorio **local**" para que ambos estén sincronizados:
~~~bash
git pull origin master
~~~
<br>
<br>

## Despliegue del tema visual
Un blog Jekyll tiene, recién desplegado, un aspecto muy espartano. Si se desea dotarlo de un aspecto más sofisticado y/o añadirle prestaciones hay que modificar el "tema".  
<small>(*) Conviene hacer copia de seguridad antes de hacer cambios. Cuando están los repositorios local y remoto sincronizados bastaría, por ejemplo, con crear un fichero comprimido que contenga la carpeta del repositorio local.</small>
Existen temas realizados por la comunidad listos para incorporar a nuestro blog. Muchos de ellos pueden previsualizarse [aquí - themes](http://jekyllthemes.org/)  
Para incorporar un tema basta con:
- Descargarlo
- Descomprimirlo en la carpeta local que contiene nuestro repositorio, sobreescribiendo lo que sea menester (repito, es interesante respaldarlo todo previamente).
- Efectuar los retoques necesarios para adaptar el nuevo tema, que es genérico, a nuestro propio blog.
<br>
<br>

## Retoques
Básicamente consiste en personalizar el blog y añadir elementos nuevos.  
<br>
Muchas de esas acciones se fundamentan en la edición de uno de los archivos más importantes -quizás el más importante- de nuestro blog Jekyll: **'_config.yml'**.
<br>
Personalizar el blog hace referencia esencialmente a hacer que muestre la dirección de correo electrónico (contacto) correcta, un "acerca de" que explique el propósito de nuestro blog, los enlaces a nuestros repositorios de GitHub o GitLab, una *homepage* a nuestro gusto, modificar las plantillas de página o post, etcétera...  
<br>
Añadir elementos nuevos es, por poner un ejemplo, incorporar un sistema de *feedback* (comentarios de los visitantes), un sistema de analíticas (en mi opinión eso es futil pues el arte ha de ser por el arte; si uno escribe para agradar o -peor aún- por avaricia, mal vamos), un sistema de búsqueda, un sistema de etiquetas y categorías (<small>aún no lo he conseguido</small>), etcétera...  
Has
<br>
<br>
## Importación / Exportación (del blog antiguo)
Los contenidos en un blog Jekyll se redactan texto plano y se pueden adornar rápida y eficazmente mediante lenguaje de marcas, lo que comúnmente se denomina [**MarkDown**](https://docs.gitlab.com/ee/user/markdown.html).  
MarkDown tiene variantes como, por ejemplo, KramDown [<small>kramdown</small>_1](https://kramdown.gettalong.org/), [<small>kramdown</small>_2](https://soniaalvarezbrasa.github.io/blog/sobre-markdown-y-kramdown.html)  
Es posible importar un blog preexistente ubicado en otro sitio como puede ser Blogger, la plataforma de blogs de Google. Para ello hay que convertir todos los contenidos a texto plano markdown. El procedimiento es sencillo y está [detallado en la página oficial de Jekyll](https://import.jekyllrb.com).  
Por ejemplo, para portar un blog de Blogger a Jekyll:  
1. Exportar, desde el interfaz de Blogger, los contenidos a un fichero .xml (blog-MM-DD-YYYY.xml). Así:  
<br>
![Exportar todo el contenido desde Blogger a un archivo .xml]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-28-creación-blog-jekyll-en-gitlab/gitlab_jekyll_blogger_exportar.png)  
<small>Exportar todo el contenido desde Blogger a un archivo .xml</small>  
<br>
Obtendremos el archivo 'blog-MM-DD-YYYY.xml'  
<br>
2. Convertir todo el contenido del archivo 'blog-MM-DD-YYYY.xml' a contenido markdown asimilable por Jekyll.  
Para hacer esto en un sistema GNU Linux son necesarios, a parte de 'ruby':
- el paquete 'ruby-dev
~~~bash
sudo apt install ruby-dev
~~~
- la gema jekyll-import
~~~bash
gem install jekyll-import
~~~  

Con todas las dependencias cumplidas, sólo hay que ejecutar línea por línea lo siguiente, tal como se explica en la [página oficial de Jekyll](https://import.jekyllrb.com/docs/blogger/). Tan solo hay que modificar la ruta (*path*) y el nombre del archivo 'blog-MM-DD-YYYY.xml' que se obtuvo de Blogger:
~~~ruby
ruby -r rubygems -e 'require "jekyll-import";
    JekyllImport::Importers::Blogger.run({
      "source"                => "/path/to/blog-MM-DD-YYYY.xml",
      "no-blogger-info"       => false, # not to leave blogger-URL info (id and old URL) in the front matter
      "replace-internal-link" => false, # replace internal links using the post_url liquid tag.
    })'
~~~
Como resultado aparecerá una carpeta que contendrá 2 ó más subcarpetas; una para los borradores, otra para los posts, etcétera. Todo el contenido del antiguo blog estará ahí, en esas subcarpetas, convertido a fomato markdown inteligible para Jekyll.  
Sólo hace falta trasladar los archivos de los posts a la carpeta _posts de nuestro blog Jekyll y, por supuesto, sincronizar local con remoto mediante git (el procedimiento explicado más arriba).  
<small>(*) Las etiquetas ("labels") estarán incluídas en la exportación como etiquetas ("tags") dentro de cada post.</small>

<br>
<br>
<br>
Entradas relacionadas:
- [[HowTo] Corregir error en Jekyll (Gitlab): versión listen vs versión ruby](https://hijosdeinit.gitlab.io/howto_corregir_error_gitlab_jekyll_listen_vs_ruby/)
- [[HowTo] Corregir error en Jekyll (Gitlab): mis incompatibilidades con la útima versión de ruby disponible](https://hijosdeinit.gitlab.io/howto_corregir_error_gitlab_jekyll_ruby3.4.0/)
- [[HowTo] Generación automática de sitemap.xml para blog Jekyll en GitLab](https://hijosdeinit.gitlab.io/howto_generar-autom%C3%A1ticamente-sitemap.xml_jekyll_gitlab/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://elblogdelazaro.gitlab.io/posts/2017-09-23-jekyll-y-gitlab/" target="_blank">El blog de Lázaro</a>  
<a href="https://chenhuijing.com/blog/hosting-static-site-gitlab-pages/" target="_blank">chenhuijing.com</a>  
<a href="https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html" target="_blank">gitlab.com - start using git</a>  
<a href="https://gitlab.com/jekyll-themes/default-bundler" target="_blank">gitlab.com - construir Jekyll con Bundler</a>  
<a href="https://colaboratorio.net/ondiz/developer/2017/como-personalizar-nuestro-sitio-jekyll-con-gemas/" target="_blank">colaboratorio.net - Personalizar Jekyll con gemas</a>  
<a href="http://www.independent-software.com/building-and-publising-a-jekyll-site-from-gitlab-using-ci-ci.html" target="_blank">independent-software.com - Building and publishing a Jekyll site from GitLab usig CI/CD</a>  
<a href="https://soniaalvarezbrasa.github.io/blog/sobre-markdown-y-kramdown.html" target="_blank">soniaalvarezbrasa - MarkDown y KramDown</a>  
<a href="https://anandjoshi.me/articles/2016-10/Blogging-with-github-and-jekyll" target="_blank">anandjoshi.me - Blogging with github and Jekyll</a>
</small>
<a href="" target="_blank"></a>
