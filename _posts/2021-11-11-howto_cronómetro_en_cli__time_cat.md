---
layout: post
title: "[HowTo] Cronómetro en la terminal mediante 'time' y 'cat'"
date: 2021-11-11
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "time", "cat", "crononometro", "terminal", "linea de comandos", "cli"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En más ocasiones de las que pienso no es necesario recurrir a herramientas externas cuando trabajamos con la terminal de  GNU Linux.  
Andaba yo buscando la forma de hacer un script para crear un cronómetro cuando descubrí que, conjugando dos comandos que vienen de serie con todas las distribuciones GNU Linux -'time' y 'cat'-, se podía disponer de algo parecido de una forma más directa aún.  
Basta con ejecutar:
~~~bash
time cat
~~~
... y cuando deseemos pararlo, con pulsar 'ctrl'+'C' es suficiente.  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
