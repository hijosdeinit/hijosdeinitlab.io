---
layout: post
title: "[HowTo] Extracción de los subtitulos de videos 'matroska' ('.mkv') mediante 'mkvextract'"
date: 2023-02-28
author: Termita
category: "multimedia"
tags: ["sistemas operativos", "multimedia", "subtitulos", "srt", "matroska", "mkv", "mkvextract", "2 pass", "2 pasadas", "HandBrake", "bitrate", "ffmpeg", "gnu linux", "linux", "debian"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En ocasiones un archivo de video contiene varias pistas de audio e, incluso, subtítulos en varios idiomas.  
Para extraer los subtítulos de un archivo de video ['matroska' (.mkv)](https://www.matroska.org/index.html) existe el comando ['mkvextract'](https://mkvtoolnix.download/doc/mkvextract.html), que se incorpora al sistema instalando el paquete ['mkvtoolnix-gui'](https://mkvtoolnix.download/index.html):
~~~bash
sudo apt-get update
sudo apt install mkvtoolnix-gui
~~~

<br>
'mkvtoolnix-gui' es una utilidad multiplataforma -GNU Linux, MacOS y Windows- y multiarquitectura -32bits y 64bits-.

<br>
Primero es necesario averiguar el 'id' del subtítulo que nos interesa. Hay quien recurre al programa ['mediainfo'](https://mediaarea.net/es/MediaInfo), aunque basta con emplear el potente y versátil comando [**'ffmpeg'**](https://ffmpeg.org/), que puede mostrarnos toda la información sin recurrir a otras herramientas:
~~~bash
ffmpeg -i archivodevideo.mkv
~~~
... y, de lo que al ejecutarse el comando se muestra en pantalla en referencia a los subtítulos que contiene el video, he aquí un ejemplo:
~~~
Stream #0:4(spa): Subtitle: subrip
Metadata:
	BPS-eng         : 50
	DURATION-eng    : 01:51:34.979000000
	NUMBER_OF_FRAMES-eng: 1447
	NUMBER_OF_BYTES-eng: 41964
	_STATISTICS_WRITING_APP-eng: mkvmerge v41.0.0 ('Smarra') 64-bit
	_STATISTICS_WRITING_DATE_UTC-eng: 2019-12-25 13:24:32
	_STATISTICS_TAGS-eng: BPS DURATION NUMBER_OF_FRAMES NUMBER_OF_BYTES
Stream #0:5(eng): Subtitle: subrip
Metadata:
	BPS-eng         : 57
	DURATION-eng    : 01:51:06.408000000
	NUMBER_OF_FRAMES-eng: 1457
	NUMBER_OF_BYTES-eng: 47750
	_STATISTICS_WRITING_APP-eng: mkvmerge v41.0.0 ('Smarra') 64-bit
	_STATISTICS_WRITING_DATE_UTC-eng: 2019-12-25 13:24:32
	_STATISTICS_TAGS-eng: BPS DURATION NUMBER_OF_FRAMES NUMBER_OF_BYTES
Stream #0:6(spa): Subtitle: subrip (default) (forced)
Metadata:
	BPS-eng         : 34
	DURATION-eng    : 00:00:16.169000000
	NUMBER_OF_FRAMES-eng: 1
	NUMBER_OF_BYTES-eng: 70
	_STATISTICS_WRITING_APP-eng: mkvmerge v41.0.0 ('Smarra') 64-bit
	_STATISTICS_WRITING_DATE_UTC-eng: 2019-12-25 13:24:32
	_STATISTICS_TAGS-eng: BPS DURATION NUMBER_OF_FRAMES NUMBER_OF_BYTES
~~~
> Esto nos hace saber que el "stream" con identificador 4 contiene los subtítulos en español, el 5 contiene los subtítulos en inglés, y el 6 los subtítulos en español, forzados y por defecto.  

<br>
Una vez claro el identificador del subtítulo que deseamos extraer, procedemos con el comando **'mkvextract'**:
~~~bash
mkvextract tracks archivodevideo.mkv 4:subtitulos_español.srt
~~~
~~~
Extrayendo pista 4 con ID del códec 'S_TEXT/UTF8' al archivo 'subtitulo_eng.srt'. Formato del contenedor: SRT text subtitles
Progreso: 100%
~~~
... para extraer, por ejemplo, los subtítulos en español, cuyo id en este caso -repito- es el 4.

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Extraer mediante ffmpeg el audio de un archivo de video](https://hijosdeinit.gitlab.io/howto-ffmpeg-Extraer-audio-de-archivo-de-video/)
- [[HowTo] Convertir vídeo ‘.ogv’ a otros formatos](https://hijosdeinit.gitlab.io/howto_convertir_ogv_a_otros_formatos/)
- [[HowTo] Incorporar pista de audio a una pista de video](https://hijosdeinit.gitlab.io/howto_incorporar_pista_audio_a_video/)
- [[HowTo] Trocear un video](https://hijosdeinit.gitlab.io/howto_trocear_video/)
- [[HowTo] instalacion de HandBrake en Ubuntu y derivados](https://hijosdeinit.gitlab.io/howto_instalacion-handbrake-ubuntu-apt/)
- [[HowTo] Extracción de imagenes de un video mediante ffmpeg (GNU Linux)](https://hijosdeinit.gitlab.io/howto_ffmpeg_extraer_fotogramas_de_archivo_video/)  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[widmanyblog](https://widmanyblog.wordpress.com/2019/02/10/extraer-subtitulos-de-un-archivo-mkv-o-matrosca-en-linux/)  
[superuser.com](https://superuser.com/questions/1527829/extracting-subtitles-from-mkv-file)  
[]()</small>  
<br>
<br>
<br>
<br>
