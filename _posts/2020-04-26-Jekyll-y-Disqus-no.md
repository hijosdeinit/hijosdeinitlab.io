---
layout: post
title: '¿De Blogger a Gitlab/Jekyll + Disqus?: salir del fuego para meterse en las brasas'
date: 2020-04-26
author: Termita
category: "programación"
tags: ["web", "programación", "html", "diseño", "jekyll", "blog", "web", "web 4.0"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Desde el primer día <a href="https://built4rocknroll.blogspot.com/" target="_blank">mi espacio en **Blogger**</a> iba a ser algo temporal. Empecé a escribir en Blogger anotaciones de lo que iba haciendo, mi bitácora sobre mi relación cotidiana con la tecnología, porque era sencillo y porque no tenía tiempo para montármelo de una manera coherente e inteligente en un sitio y de una forma más acorde con mi manera de entender las cosas.  
<br>
<small>Bien, hace unos días monté la bitácora en **GitLab** mediante **Jekyll**. Hay un post aquí dedicado al procedimiento que, básicamente, se podría resumir en:</small>  
<small>1. Registro en GitLab</small>  
<small>2. Despliege del software para blogs estáticos Jekyll</small>  
<small>3. Despliegue del tema visual que más me agradó</small>  
<small>4. Modificaciones en la configuración de Jekyll y el tema visual para adecuarlos a mis gustos y necesidades:</small>  
<small>4.1. Incorporación de sistema de búsqueda (escogí <a href="https://github.com/jekylltools/jekyll-tipue-search" target="_blank">**Tipue**</a>).</small>  
<small>4.2. Incorporación de un **sistema de comentarios honesto**... (¡ahí me estanqué!)</small>  

<br>
Todo el mundo sabe que Jekyll **no trae sistema de *feedback* de los visitantes**, es decir, sistema de comentarios.
En esa última fase -incorporación de sistema de comentarios- lo más fácil es, tras buscar un poco en la web, incorporar Disqus registrándose e insertando en la plantilla 'posts' un código que proporcionan. Mas, al igual que ocurre cuando se elige Blogger, lo cómodo no es lo adecuado.  

<br>
Todos sabemos que Alphabet (Google) vive de la publicidad y que extrae su enorme fuerza de los usuarios -que muchas veces dependen de sus productos "gratuítos", desde su buscador hasta su "nube"- a los que traquea y espía, de los que saca estadísticas y perfiles publicitarios.  

<br>
Disqus es lo mismo, pero peor. No sólo extrae todo tipo de información del visitante del blog, sino que además inserta publicidad en la web. En Blogger no hay publicidad explícita. En Disqus SÍ, y es de la peorcita... publicidad de estafas y crecepelo.  

<br>
<big>**NO**</big> usaré Disqus como sistema de comentarios. Trasladé el blog a GitLab/Jekyll porque deseaba privacidad y respeto. Desplegué Jekyll en GitLab porque no me interesa que me perfilen a mí o a los visitantes, ni me interesan las estadísticas de visitantes; sería absurdo implementar un mecanismo como Disqus. Es más, puedo prescindir de los comentarios convencionales y establecer el correo electrónico de toda la vida como mecanismo de *feedback*, o bien buscar un sistema más respetuoso con la privacidad del creador de contenidos y del visitante.
