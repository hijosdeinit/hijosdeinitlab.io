---
layout: post
title: "[HowTo] Incorporación de temas visuales a i3wm en Debian 10 y derivados"
date: 2022-02-12
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "ubuntu", "temas", "tema visual", "themes", "apariencia", "dark", "oscuro", "negro", "yaru", "yaru-dark", "midnight", "midnight-green", "visual", "aspecto", "i3wm", "i3", "lxde", "lxappearance", "unity", "gnome", "unity-tweak-tool", "gnome-tweak-tool", "retoques", "ubuntu tweak", "tweak tool"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small><small>Entorno:
cpu Core2Duo, 6gb RAM, gpu gforce 7600gt, GNU Linux Debian 10 Buster (*netinstall*) stable, kernel 4.19.0-18-amd64, gnome, i3wm</small></small>  

<br>
El tema visual con el que suele presentarse el gestor de ventanas 'i3' recién instalado no es -a mi juicio- demasiado "atractivo": es un tema visual claro, blanco brillante, deslumbrante incluso.  
Afortunadamente esto -el tema visual- puede cambiarse.  
En mi caso he optado por un tema visual oscuro. Para mi gusto el mejor es [<big>***'midnight-green'***</big>](https://github.com/i-mint/midnight), del cual [trata esta otra entrada de este blog]().  

<br>
<br>
## Obtención de temas visuales para nuestro gestor de ventanas (*i3*, por ejemplo)
Un sitio interesante para buscar y revisar temas visuales para GNU Linux es [Gnome-Look](https://www.gnome-look.org).  
Una vez encontrado el tema visual que nos guste, por ejemplo 'midnight-green', hay que descargarlo. <small>El archivo descargado suele ser un archivo comprimido; en el caso de 'midnight-green' el archivo es **''**.</small>  
**Generalmente** el procedimiento para incorporar el tema visual a nuestro gestor de ventanas 'i3' es el siguiente:

<br>
<br>
## Procedimiento de incorporación de temas visuales a i3
Descomprímase el archivo comprimido correspondiente al tema visual que descargamos.  
Como superusuario, muévase el subdirectorio resultante tras la descompresión al subdirectorio donde nuestro sistema operativo almacena los temas visuales; generalmente éste suele ser '/usr/share/themes/', <small>aunque también -en otras distribuciones GNU Linux- podría ser '/.local/share/themes' ô '/.themes'</small>.  
Por ejemplo, al descomprimir el archivo 'Midnight-Green.tar.gz' que contiene el tema visual 'midnight-green', se obtiene el subdirectorio 'Midnight-Green'; moverlo al subdirectorio '/usr/share/themes' se haría así:
~~~bash
sudo mv Midnight-Green/ /usr/share/themes
~~~
Ahora sólo queda "activar" ese tema visual.  

<br>
<br>
## Activación de un tema visual en el gestor de ventanas 'i3' y similares
De serie, 'i3' y -creo- 'lxde' incorporan una utilidad llamada <big>***lxappearance***</big> que sirve para modificar el aspecto visual del gestor de ventanas. Es una herramienta similar a 'Ubuntu Tweak Tool'.  
Ejecútese, por consiguiente, 'lxappearance'. Mediante el lanzador de aplicaciones de 'i3' (<small>en mi caso 'meta + d'</small>) o bien directamente desded línea de comandos; así, por ejemplo:
~~~bash
lxappearance
~~~
Acto seguido basta con seleccionar el tema visual que incorporamos previamente, 'midnight-green' en mi caso.  

<br>
<a href="/assets/img/2022-02-12-howto_incorporacion_temas_visuales_i3wm_debian10/lxappearance.png" target="_blank"><img src="/assets/img/2022-02-12-howto_incorporacion_temas_visuales_i3wm_debian10/lxappearance.png" alt="seleccionando tema visual 'midnight-green' en 'lxappearance'" width="600"/></a>  
<small>seleccionando tema visual 'midnight-green' en 'lxappearance'</small>  
<br>

Hecho.  
Y así se ve ahora, por ejemplo, el monitor de sistema:

<br>
<a href="/assets/img/2022-02-12-howto_incorporacion_temas_visuales_i3wm_debian10/midnight-green__monitor_sistema.png" target="_blank"><img src="/assets/img/2022-02-12-howto_incorporacion_temas_visuales_i3wm_debian10/midnight-green__monitor_sistema.png" alt="tema visual 'midnight-green'" width="500"/></a>  
<small>tema visual 'midnight-green'</small>  
<br>

<br>
<br>
### Otros temas oscuros interesantes
<small>Se instalan y configuran siguiendo procedimientos similares.  
- [Equilux](https://www.gnome-look.org/p/1182169/)
- [Ambiance DS BlueSB12](https://www.gnome-look.org/p/1013664/)
- [Deepen Dark](https://www.gnome-look.org/p/1190867/)  

<br>
<br>
<br>
Entradas relacionadas:  
- ['Midnight-Green', mi tema visual favorito para i3wm en Debian y derivados](https://hijosdeinit.gitlab.io/midnight-green_mi_tema_visual_favorito_para_i3wm_debian_10/)
- [[HowTo] Incorporar temas oscuros a Ubuntu 16.04.7 LTS](https://hijosdeinit.gitlab.io/howto_tema_oscuro_yaru_dark_ubuntu_1604/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
