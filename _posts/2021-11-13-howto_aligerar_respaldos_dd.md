---
layout: post
title: "[HowTo] aligerar respaldos hechos con 'dd'"
date: 2021-11-13
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "ubuntu", "windows", "macos", "respaldo", "backup", "img", "imagen de disco", "dd", "acronis trueimage", "clonacion", "clon", "clonar", "comprimir", "reducir", "aligerar", "7zip"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno: cpu Intel i7-8750H (12) @ 4.100GHz, 16gb RAM, gpu nvidia GeForce GTX 1050 Mobile, Ubuntu 20.04.3 LTS x86_64, kernel 5.11.0-40-generic</small>  

<br>
<br>
[videotutorial](https://odysee.com/@hijosdeinit:4/aligerar_imagen_respaldo_backup_dd:8)  

<br>
<br>
Este procedimiento es válido, no sólo para los respaldos realizados con 'dd' (o 'gnome-disks'), sino para aquellos respaldos -valga la redundancia- totales realizados con herramientas como 'Acronis Trueimage' y otros que permiten realizar 'imágenes completas' de un dispositivo de almacenamiento.  

<br>
Una de las formas más habituales de realizar respaldos es crear una 'imagen completa' de un dispositivo de almacenamiento. Esto crea un archivo que contiene TODO lo que hay en dicho dispositivo:
- estructura **visible**: archivos y directorios, sean cuales sean sus permisos
- estructura **invisible**: archivos y directorios eliminados, sectores, arranque, etcétera... cosas que no se ven a simple vista pero están ahí y, por consiguiente, ocupan un espacio.  

Es la estructura invisible la que, precisamente, puede hacer que la 'imagen completa' resultante tenga un tamaño enorme, aún comprimida:
Es normal que el **tamaño de la 'imagen completa'** sea similar al **tamaño total del dispositivo de almacenamiento**: un pendrive de 8gb suele dar lugar a una 'imagen completa' de 8gb.  
**Sin embargo**, si luego comprimimos -mediante '7zip', por ejemplo- dicha imagen, el tamaño **debería ser muy inferior**.  

<br>
## ¿Cuándo el tamaño comprimido de la 'imagen completa' de un dispositivo es demasiado similar al tamaño de éste?
a) Cuando el dispositivo del que hicimos la 'imagen completa' estaba casi lleno
b) Cuando en el dispositivo del que hicimos la 'imagen completa', aún estando casi vacío, existe una estructura **invisible** (directorios y archivos eliminados, por ejemplo) que ocupa -aunque no lo veamos- un gran espacio.  

<br>
El caso 'a' es lógico y las herramientas de compresión no hacen milagros.  
<br>
Sin embargo, el caso <big>**'b'**</big> puede enmendarse.  

<br>
<br>
## ¿Cómo hacer que el tamaño comprimido de la 'imagen completa' de un dispositivo sea muy inferior al tamaño de éste?
El procedimiento es sencillo.  
<big>**Antes**</big> de hacer la imagen del dispositivo:
1. Anótese el espacio libre existente en el dispositivo del que querremos hacer una imagen completa.
2. [Créese en el dispositivo un archivo "de ceros"](https://hijosdeinit.gitlab.io/howto_crear_fichero_relleno_ceros/) de un tamaño igual al espacio libre. Por ejemplo, para crear un archivo "de ceros" que ocupe 1gb: <span style="background-color:#042206"><span style="color:lime">`sudo dd if=/dev/zero of=/media/tuusuario/tudispositivo/archivozeros.foo bs=1024 count=1000k conv=fsync status=progress`</span></span>. <small>Eso sobreescribirá todos esos restos de archivos y subdirectorios tiempo atrás borrados</small>.
3. Bórrese luego ese "archivo de ceros": <span style="background-color:#042206"><span style="color:lime">`rm /media/tuusuario/tudispositivo/archivozeros.foo`</span></span>
4. Desmóntese el dispositivo. Por ejemplo, si el dispositivo es '/dev/sdh1': <span style="background-color:#042206"><span style="color:lime">`sudo umount /dev/sdh1`</span></span>  

Y ya, <big>**a continuación**</big>:
4. Créese la imagen completa del dispositivo -mediante 'dd', por ejemplo- (<small>ésta tendrá un tamaño similar al del dispositivo</small>): <span style="background-color:#042206"><span style="color:lime">`sudo dd if=/dev/sdh of=imagendetudispositivo.img bs=4M conv=fsync status=progress`</span></span>
5. Comprímase (<small>El archivo resultante de comprimir el archivo de la imagen será bastante inferior al tamaño del dispositivo origen</small>). El comando requerido podría ser, por ejemplo: <span style="background-color:#042206"><span style="color:lime">`7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on -v1490m -p imagendetudispositivo.img.7z imagendetudispositivo.img`</span></span>  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Crear Fichero De Relleno A Base De Ceros](https://hijosdeinit.gitlab.io/howto_crear_fichero_relleno_ceros/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
