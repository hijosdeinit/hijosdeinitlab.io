---
layout: post
title: "[HowTo] El editor (IDE) 'geany' en Debian y derivados"
date: 2022-02-08
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "geany", "atom", "editor de texto", "ide", "programacion", "bash", "script", "bash scripting", "markdown", "texto", "texto plano", "código", "editor", "gnu linux", "debian", "vi", "vim", "nano", "emacs", "atom", "eclipse", "sublime text", "visual studio code", "vscodium", "codium", "vscode", "netbeans"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Hace un tiempo hice un [listado de los editores de texto / IDE que me parecían interesantes en un artículo de este blog](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/).</small>  
<small>Un IDE (*Integrated Developement Environment*) consta, entre otras cosas, de un editor de texto con muchos extras como, por ejemplo, resaltado de sintaxis, autocompletado.</small>  
<small>Uno de los editores de aquella lista era ['Geany'](https://www.geany.org/).</small>  

<br>
<a href="/assets/img/2022-02-08-howto_editor_ide_geany_en_debian_y_derivados/arc-dark-geany.png" target="_blank"><img src="/assets/img/2022-02-08-howto_editor_ide_geany_en_debian_y_derivados/arc-dark-geany.png" alt="'geany'" width="600"/></a>  
<small>'geany'</small>  
<br>

<br>
<br>
## Geany
'Geany' es un «poderoso, estable, flexible y ligero editor de código que provee toneladas de características útiles sin empatizar el flujo de trabajo». Es de código abierto (<small>licencia GPL v2</small>), multiplataforma (GNU Linux, Windows y macOS), funciona bajo interfaz gráfico (no para línea de comandos (*CLI*) como 'vi'), está traducido a 40 idiomas e incorpora soporte para más de 50 lenguajes de programación.  
Según sus desarrolladores, «una de las principales razones de la existencia de 'geany' es el hecho de que la mayoría de IDE/editores destacan en algunos aspectos -interfaz gráfico, ligereza, versatilidad, potencia, multiplataforma- y se quedan cortos en otros. Geany intenta cubrirlos todos».  
Entorno a 'geany' existe una gran [comunidad](https://www.geany.org/contribute/) que, ateniéndose a su licéncia de código abierto, asegura que cualquiera pueda modificarlo, adaptarlo, "hackearlo", y compartir con todos los demás los cambios y mejoras.  
A Geany se le pueden añadir multitud de [temas visuales (*colorschemes*)](https://www.geany.org/download/themes/), [tipos de archivo](https://www.geany.org/about/filetypes/) de los diferentes lenguajes de programación (p.ej. C, Java, PHP, HTML, JavaScript, Python, Perl, etc), ajustes, [*plugins*](https://www.geany.org/support/plugins/), etcétera.  

<br>
[Lista de correo (*mailing list*) oficial de 'geany'](https://www.geany.org/support/mailing-lists/)  
[Repositorio de 'geany' en 'GitHub](https://github.com/geany)  
['geany' en la red descentralizada 'Matrix'](https://matrix.to/#/#geany:matrix.org)  
['geany' en 'Twitter'](https://twitter.com/GeanyIDE/)  

<br>
<br>
## Instalación de 'geany' en Debian 10 (y derivados)
~~~bash
sudo apt-get update
sudo apt install geany
~~~

<small><small>Entorno:
cpu Core2Duo, 6gb RAM, gpu gforce 7600gt, GNU Linux Debian 10 Buster (*netinstall*) stable, kernel 4.19.0-18-amd64, gnome</small></small>  

<br>
<br>
## Postinstalación
'geany' es un IDE personalizable con multitud de plugins, esquemas de colores (temas), etc...  
Por ejemplo, para instalar un **TEMA VISUAL** (esquema de colores (*colorscheme*)) oscuro:
1. Descargar el tema visual, por ejemplo, desde el [apartado de temas visuales de la página oficial](https://www.geany.org/download/themes/). <small>También esisten otros temas visuales interesantes en otros "sitios", como por ejemplo ['arc dark '](https://silvercircle.subspace.cc/software/arc-dark-colorscheme-for-geany/). Para llegar a ellos basta una búsqueda en duckduckgo o fuckoffgoogle.</small>
2. Copiarlo en el subdirectorio '~/.config/geany/colorschemes'
3. Activarlo desde el interfaz del propio 'geany': ver → cambiar esquema de color  

[Repositorio de temas visuales oficiales de 'geany' en 'GitHub'](https://github.com/geany/geany-themes).  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] El editor (IDE) 'atom' y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [[HowTo] Agregar idiomas al editor (IDE) atom. Ponerlo en español](https://hijosdeinit.gitlab.io/howto_poner_atom_en_espa%C3%B1ol/)
- [[HowTo] Evitar que 'atom' elimine espacios en blanco al final de línea](https://hijosdeinit.gitlab.io/howto_atom_espacios_blanco_final_linea/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)
- [[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
