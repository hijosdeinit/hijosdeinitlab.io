---
layout: post
title: "[HowTo] 'searX', un metabuscador *hackable* que busca la privacidad"
date: 2021-09-03
author: Termita
category: "internet 4.0"
tags: ["internet 4.0", "buscador", "metabuscador", "altavista", "searx", "frontend", "fuckoffgoogle", "privacidad", "ads", "publicidad", "no ads", "tracking", "traqueo", "rastreo", "seguimiento", "cookies", "neutralidad", "perfil", "monetizacion", "google", "servidor", "descentralizacion", "open source", "libre", "libertad", "seguridad", "web", "tor", "proxy", "invidious"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
##### <small>**ANTECEDENTES y CONTEXTO**</small>  
<small>Hace muchos años 'Google' ('Alphabet') y/o su **"buscador"** ni siquiera existían. A principios y mediados de los años 90 del siglo pasado la web estaba compuesta sobre todo por servidores de instituciones educativas, científicas, y algunas páginas personales ubicadas en servidores "de pago" o sitios "gratuítos" como ['Geocities'](https://es.wikipedia.org/wiki/GeoCities) o 'Lycos'.  
Por entonces, disponer de una cuenta de correo electrónico era un "privilegio" disponible tan sólo para unos pocos internautas afortunados que, generalmente, formaban parte de alguna institución o, que -de su bolsillo- pagaban por disponer de ello. Luego -1996- llegó [Hotmail](https://es.wikipedia.org/wiki/Outlook_(correo_web)#Lanzamiento_de_Hotmail) y el panorama al respecto cambió: una cuenta de correo electrónico "gratuíta" a cambio de tus datos, de tu privacidad, de ser comprado y vendido como un objeto. Y así fue, años más tarde -1997-, Microsoft adquirió Hotmail, con toda su cartera de "clientes".  
La publicidad en aquellos años era escasísima, limitada a algunos *banners*, y generalmente poco intrusiva. La cuantificación, catalogación, rastreo y monetización del internauta brillaba por su ausencia. [Amazon](https://en.wikipedia.org/wiki/Amazon_(company) (1994) era una simple librería pionera en la venta de su producto -libros- por internet, y lo que hoy es [Netflix](https://en.wikipedia.org/wiki/Netflix), por entonces no era ni siquiera el videoclub que fue (se fundó en 1997).  
El **buscador de referencia** era [Altavista](https://digital.com/altavista/), cuya URL era 'http://altavista.digital.com' y fue lanzado por [Digital Equipment Corporation](https://es.wikipedia.org/wiki/Digital_Equipment_Corporation) -una empresa desarrolladora de megacomputadoras- en 1995 para poner a prueba las capacidades de uno de sus superordenadores. A su sombra estaba el buscador de [Yahoo](https://es.wikipedia.org/wiki/Yahoo!), nacido en 1994, cuyo objeto era (y es) el lucro, comprar y vender.  
En [este blog se le dedicó un artículo a 'Altavista, su historia y su "legado"'](https://hijosdeinit.gitlab.io/internet1.0_la_era_Altavista/), un buscador -el mejor de su era- que no fue creado para tomar internet por asalto o aprovecharse de una oportunidad comercial en auge.  
<small>'Alphabet'-'Google', 'Amazon', 'Yahoo', 'Microsoft', 'Hotmail' / 'MSN', 'Netflix'..., hay que ver cómo, escribiendo sobre buscadores, han ido saliendo retratados todos los enemigos del internauta de a pie, que -constituídos en *'Big Tech'*- también lo son -tal como están las cosas hoy- de la humanidad, a la cual maltratan.</small></small>  
<br>
<a href="/assets/img/2021-09-03-howto_searx_metabuscador/altavista1995.jpg" target="_blank"><img src="/assets/img/2021-09-03-howto_searx_metabuscador/altavista1995.jpg" alt="el buscador 'Altavista' en 1995" width="300"/></a>  
<small><small>el buscador 'Altavista' en 1995</small></small>  
<br>

<br>
# 'searX'
[SearX](https://searx.me) es un metabuscador de internet que incorpora resultados de más de 70 servicios de búsqueda. Es libre y sus usuarios no son rastreados ni analizados.  
> [Sitio web oficial](http://searx.me/)  
[blog oficial](https://searx.github.io/searx/blog/index.html)  
[repositorio de código en GitHub](https://github.com/searx/searx)  
[seguimiento de errores](https://github.com/searx/searx/issues)  

<br>
Al igual que 'invidious' (el *frontend* para 'Youtube' [al que hay dedicada en este blog una entrada](https://hijosdeinit.gitlab.io/howto_invidious_youtube_desde_navegador_sin_publicidad/)), 'SearX' puede ser utilizado de 2 formas:
- a) desde el navegador, mediante **una de las muchas instancias públicas de SearX <big>existentes**</big>
- b) desde el navegador, mediante una <big>**instancia propia**</big> (instalándo su software en un servidor propio)  

Hay muchas instancias públicas de Searx ejecutadas por los mismos usuarios, algunas de las cuales están disponibles como servicios ocultos de Tor. Los sitios "Meta-searx" consultan una instancia aleatoria diferente con cada búsqueda.  
Por otro lado, cualquier usuario puede ejecutar su propia instancia de 'searX'.  

<br>
<a href="/assets/img/2021-09-03-howto_searx_metabuscador/Searx_logo_agosto2015.png" target="_blank"><img src="/assets/img/2021-09-03-howto_searx_metabuscador/Searx_logo_agosto2015.png" alt="logo de searX en agosto 2015" width="300"/></a>  
<br>

'searX' es el padre de otro buscador, prácticamente idéntico (utiliza su software): [Fuck Off Google](https://search.fuckoffgoogle.net/), del que hablamos en [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_fuckofgoogle_metabuscador/).  

<br>
## Prestaciones de 'searX'
- no rastrea al usuario
- no analiza al usuario
- los resultados son neutrales (<small>NO resultados personalizados de nuestros perfiles dentro de Google y otros motores de búsqueda</small>)
- sin publicidad
- soporta más de 70 motores de búsqueda
- fácil integración con cualquier motor de búsqueda
- cookies desactivadas por defecto
- conexiones seguras y cifradas (HTTPS/SSL)
- su idiosincracia, sus rasgos esenciales, favorecen la seguridad, la intimidad y la libertad
- puede funcionar tanto en la **web** convencional como en el anonimato de la red **Tor** (Onion)
- dispone de una API pública
- dispone de extensiones para Mozilla Firefox.
- descentralizado
- autohospedado (<small>si no se desea utilizar una de las instancias públicas</small>)  

<br>
El software que hace funcionar 'searX' está escrito en 'Python', es libre y está distribuído en su [**repositorio** de GitHub](https://github.com/searx/searx) bajo la licencia [GNU Affero General Public License](https://es.wikipedia.org/wiki/GNU_Affero_General_Public_License).  
'searx' es alojado y promovido, entre otros, por organizaciones -como *La quadrature du net* o *FuckOffGoogle*- que promueven los derechos digitales.  

<br>
Con el fin de proteger la privacidad de sus usuarios, 'searX' no comparte sus direcciones ip ni su historial de búsqueda con los motores de búsqueda a los que accede para recopilar los resultados de las búsquedas, valgan las redundancias.  
'SearX' bloquea las *cookies* de seguimiento de los motores de búsqueda -que son los tradicionales- de los que se sirve. Esto evita la adulteración de los resultados basada en el perfil del usuario.  
SearX emplea por defecto el método de petición 'POST' cuando envía las consultas a los buscadores; de esta forma se evita que las palabras clave de las consultas de los usuarios aparezcan en los registros del servidor web de los buscadores.

Cada resultado de búsqueda se proporciona como un enlace directo al sitio respectivo, en lugar de un enlace de redireccionamiento rastreado como utiliza 'Google'. Además, cuando están disponibles, estos enlaces directos van acompañados de enlaces "almacenados en caché" y/o disponibles a través de un *proxy* lo cual permite ver las páginas de resultados sin tener que visitar los sitios en cuestión directamente.  
Los **enlaces en *caché*** apuntan a versiones archivadas de una página en ['WayBack Machine (archive.org)](https://archive.org/), mientras que los **enlaces *proxy*** permiten ver la página actual a través de un *proxy* web basado en búsquedas. Además de la búsqueda general, el motor también presenta pestañas para buscar dentro de dominios específicos: archivos, imágenes, TI, mapas, música, noticias, ciencia, redes sociales y videos.  

<br>
## Los motores de búsqueda que usa 'searX' y cómo configurarlos
«En todas las categorías, Searx puede obtener resultados de búsqueda de aproximadamente 82 buscadores diferentes. Esto incluye los principales motores de búsqueda de sitios como 'Bing', 'Google', 'Reddit', 'Wikipedia', 'Yahoo' o 'Yandex'.  
Los motores utilizados para cada categoría de búsqueda se pueden configurar a través de una **interfaz de 'preferencias'**. Estas configuraciones se guardarán en una *cookie* en el navegador del usuario, en lugar de en el servidor, ya que por razones de privacidad, 'searX' no implementa un inicio de sesión de usuario.  

<br>
<a href="/assets/img/2021-09-03-howto_searx_metabuscador/motores-de-busqueda.png" target="_blank"><img src="/assets/img/2021-09-03-howto_searx_metabuscador/motores-de-busqueda.png" alt="configuración de los motores de búsqueda en 'searX'" width="500"/></a>  
<small>configuración de los motores de búsqueda en 'searX'</small>  
<br>

Otras configuraciones, como el idioma de la interfaz de búsqueda y el idioma de los resultados (hay más de 20 idiomas disponibles) se pueden configurar de igual manera.  
​Además de la *cookie* de preferencias, en cada consulta es posible modificar los motores utilizados, las categorías de búsqueda seleccionadas y/o los idiomas para buscar especificando uno o más de los siguientes operadores de búsqueda textual antes de las palabras clave de búsqueda»:​
- <span style="background-color:#042206"><span style="color:lime">`!categoría`</span></span> – busca la categoría especificada en lugar de las predeterminadas.
- <span style="background-color:#042206"><span style="color:lime">`?categoría`</span></span> – busca la categoría especificada además de las predeterminadas.
- <span style="background-color:#042206"><span style="color:lime">`!motor`</span></span> – usa el motor especificado en lugar de los predeterminados.
- <span style="background-color:#042206"><span style="color:lime">`?motor`</span></span> – usa el motor especificado además de los predeterminados.
- <span style="background-color:#042206"><span style="color:lime">`:idioma`</span></span> – busca resultados en el idioma especificado en lugar del predeterminado.  

<small>Los operadores <span style="background-color:#042206"><span style="color:lime">`!`</span></span> y <span style="background-color:#042206"><span style="color:lime">`?`</span></span> se pueden especificar más de una vez para seleccionar varias categorías o motores, por ejemplo: <span style="background-color:#042206"><span style="color:lime">`!google`</span></span> <span style="background-color:#042206"><span style="color:lime">`!deviantart`</span></span> <span style="background-color:#042206"><span style="color:lime">`?images`</span></span> <span style="background-color:#042206"><span style="color:lime">`:japanese cow`</span></span></small>  

<br>
<br>
# Utilizar 'searX'
## <big>**A**</big>. Desde el navegador, mediante una de las muchas instancias públicas de SearX existentes
### A.1 instancias públicas
Accediendo mediante el navegador a cualquier instancia de 'searX' se pueden hacer las búsquedas.  
La instancia "oficial" es [searx.org](https://searx.org/).  
Otra de mis favoritas es [searx.tux.land](https://searx.tux.land/search).  
Si se desea cualquier otra instancia de 'searX', [he aquí el listado oficial de instancias de 'searX'](https://searx.space/), escójase la que más guste.  
<br>
### A.2 extensión para Mozilla Firefox
Para que, mediante Mozilla Firefox, cualquier texto que se inserte en la barra de direcciones URL sea buscado mediante 'searX', basta con agregar el buscador 'searX' al navegador:  
Preferencias → Buscar → Encontrar más buscadores  
<br>
<a href="/assets/img/2021-09-03-howto_searx_metabuscador/motores-de-busqueda_Mozilla_Firefox_001.png" target="_blank"><img src="/assets/img/2021-09-03-howto_searx_metabuscador/motores-de-busqueda_Mozilla_Firefox_001.png" alt="configuración de los motores de búsqueda en 'Mozilla Firefox'" width="500"/></a>  
<small>configuración de los motores de búsqueda en 'Mozilla Firefox'</small>  
<br>
<br>
## <big>**B**</big>. Desde el navegador, mediante tu propia instancia de 'searX' instalada en tu propio servidor
«Cualquier usuario puede ejecutar su propia instancia de Searx, consiguiendo de esta forma maximizar la privacidad, evitar la congestión en instancias públicas, preservar la configuración personalizada incluso si se borran las cookies del navegador, permitir la auditoría de código fuente en ejecución, etc. Los usuarios pueden incluir sus propias instancias de Searx en la lista editable de todas las instancias públicas o mantenerlas privadas. También el usuario puede agregar motores de búsqueda personalizados a su propia instancia que no están disponibles en las instancias públicas.  
Otra razón para usar diferentes instancias, y/o ejecutar una propia, es que a partir de 2019, 'Google' ha comenzado a bloquear algunas de ellas, incluídas algunas de las direcciones IP utilizadas por 'searx.me' (instancia operada anteriormente por el desarrollador), lo que genera el siguiente error 'Google (bloqueo inesperado: se requiere CAPTCHA)'. En respuesta, algunas instancias se han modificado para omitir silenciosamente la opción de buscar con 'Google', incluso cuando es el único motor especificado».  

<br>
### Instalación de 'searX' en tu propio servidor
El procedimiento está detallado en la [documentación oficial](https://searx.github.io/searx/admin/installation.html#installation).  
Los métodos para disponer de 'searX' en tu propio servidor son:
- **b1.** [mediante 'docker'](https://searx.github.io/searx/admin/installation-docker.html#installation-docker).
- **b2.** [automáticamente mediante 'script'](https://searx.github.io/searx/admin/installation.html#installation-scripts).
- **b3.** [manualmente -paso a paso- de forma nativa](https://searx.github.io/searx/admin/installation-searx.html#installation-basic).  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] el metabuscador 'FuckOffGoogle y su proyecto de libertad](https://hijosdeinit.gitlab.io/howto_fuckofgoogle_metabuscador/)
- [[HowTo] 'QWant': otro buscador respetuoso con la privacidad](https://hijosdeinit.gitlab.io/howto_qwant_buscador_web_respetuoso_privacidad/)
- ['DuckDuckGo' NO es de fiar](https://hijosdeinit.gitlab.io/googlizacion_de_duckduckgo_censurando_contenidos_adulterando_busquedas/)
- [[HowTo] Buscadores web respetuosos con el internauta](https://hijosdeinit.gitlab.io/buscadores_web_respetuosos/)
- ['Altavista' y su era. Internet 1.0](https://hijosdeinit.gitlab.io/internet1.0_la_era_Altavista/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/Searx">Wikipedia - searX</a>  
<a href="https://ubunlog.com/searx-instala-este-metabuscador-en-ubuntu/#Sintaxis_de_busqueda">UbunLog</a>  
<a href="https://blog.desdelinux.net/fuck-off-google-searx-2-interesantes-proyectos-conocer-utilizar/">DesdeLinux</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
