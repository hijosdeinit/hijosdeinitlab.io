---
layout: post
title: "[HowTo] Corregir la manía de Windows 10 por borrar archivos y/o directorios creados/modificados desde GNU Linux en particiones ntfs"
date: 2021-11-14
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "windows", "windows 10", "windows 10 home", "problema", "ntfs", "gnu linux", "linux", "arranque dual", "perdida de información", "perdida", "desastre", "inicio rapido", "fast boot", "secure boot"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En GNU Linux nunca fue recomendable manejarse con archivos relevantes en particiones 'ntfs'. Aunque desde GNU Linux <big>**se puede**</big> leer y escribir en particiones 'ntfs' desde hace décadas, y el soporte 'ntfs' en estos sistemas se ha refinado muchísimo, la posibilidad de perder información cuando se juega con particiones 'ntfs', en mayor o menor medida, siempre ha existido.  
<br>
Y, cuando esto ocurre, muchas veces no es culpa de GNU Linux y su antaño tosco tratamiento del formato 'ntfs'.  
Y de eso trata esta entrada, de lo que pasa cuando, en un sistema "dual" -<small>aquel en el que coexisten sistemas operativos GNU Linux con sistemas operativos Windows y puede arrancar con cualquiera de ellos</small>-, **desde GNU Linux escribimos información en una partición 'ntfs' y ésta (información) <big>"DESAPARECE"</big> apenas, al arrancar Windows, éste accede a dicha partición**.  
Sin embargo, gracias a Dios, generalmente tras comprobar la integridad de la partición desde la herramienta que Windows tiene al efecto -'chkdsk'- la información "desaparecida" suele aparecer, presuntamente completa (<small>aunque también aparece en la raíz de la partición ese inquietante subdirectorio que suele emerger cuando se recuperar "sectores defectuosos"</small>). Un asunto feo.  

<br>
## ¿Por qué Windows 10 hace desaparecer lo que GNU Linux escribió en una partición 'ntfs'?
Dicen los entendidos que se debe al <big>**'inicio rápido' (*'fast boot'*)**</big>, una "prestación" novedosa que trae activada de serie Windows 10 para que los impacientes del mundo no se desesperen cuando el sistema operativo de Microsoft arranca, <small>no vayan a herniarse por esperar medio minuto más de la cuenta</small>.  
El "inicio rápido" de Windows 10 hace uso del componente de "hibernación" del sistema y, para evitar males, es decir, evitar esas pérdidas de información (amén de -sospecho- deterioro de discos sólidos (SSD), dada la escritura intensiva que conlleva todo aquello relacionado con la hibernación), hay que DESHABILITARLO.  

<br>
## Procedimiento para deshabilitar el 'inicio rápido' (*'fast boot'*) de Windows 10
Panel de control → hardware y sonido → opciones de energía → configuración del sistema → Definir los botones de inicio/apagado y activar la protección con contraseña → configuración de apagado:  
> inicio rápido: NO  
suspender: NO  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.solvetic.com/topic/8857-archivos-desaparecen-windows-10/">Solvetic - archivos desaparecen en Windows 10</a>  
<a href="https://www.solvetic.com/tutoriales/article/2815-como-activar-o-desactivar-inicio-rapido-en-windows-10/">Solvetic - desactivar "fast boot" en Windows 10</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
