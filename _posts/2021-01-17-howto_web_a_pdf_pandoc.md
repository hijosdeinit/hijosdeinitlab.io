---
layout: post
title: "[HowTo] guardar una página web en formato .pdf mediante pandoc"
date: 2021-01-17
author: Termita
category: "sistemas operativos"
tags: ["pandoc", "pdf", "html", "web", "productividad", "offline", "sistemas operativos", "gnu linux", "linux", "wkhtmltopdf", "latex", "convertir", "exportar"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Existen varias formas de guardar una página web en .pdf. Una de ellas es mediante 'wkhtmltopdf'. Otra es desde el menú de impresión de un navegador web. Y otra es mediante <big>'pandoc'</big>.  

<br>
[Pandoc](https://pandoc.org/) es un programa que permite convertir textos, incluso webs, a otros formatos (.pdf, .html, LaTeX, etc...)  

[En esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_instalar_pandoc_debian/) se explica cómo instalar 'pandoc' en Debian y/o derivados.  

Para **convertir una url (una página web) a .pdf**:
~~~bash
pandoc -s urlquequeremosconvertir -o nombrearchivo.pdf
~~~

<br>
<br>
Otras entradas de este blog  que tratan la conversión de contenidos web a .pdf u otros formatos son:
- [[HowTo] guardar una página web en formato .pdf mediante wkhtmltopdf](https://hijosdeinit.gitlab.io/howto_web_a_pdf_wkhmltopdf/)
- [[HowTo] Conversión básica de textos y/o web mediante pandoc](https://hijosdeinit.gitlab.io/howto_conversion_basica_textos_y_web_mediante_pandoc/)
- [[HowTo] Instalación de pandoc en Debian y/o derivados](https://hijosdeinit.gitlab.io/howto_instalar_pandoc_debian/)  

<br>
<br>
<br>
<br>
<br>
<br>
