---
layout: post
title: "[HowTo] Incorporar barra de ventanas en Gnome. (Debian 10 y derivados)"
date: 2021-05-20
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "ubuntu", "wm", "ventanas", "gestor de ventanas", "gnome", "botones", "windows manager", "display manager", "retoques", "gnome-tweak-tool", "lista de ventanas"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
El entorno de escritorio Gnome que incorpora Debian 10 *Buster* por defecto tras una instalación *netinstall*, entre otras peculiaridades, posee la de no mostrar un panel inferior donde aparezcan las ventanas abiertas.  
No obstante, uno puede ver lo que está abierto pulsando la tecla 'Windows' o bien con 'Alt'+'Tab' sin necesidad de un panel.  

<br>
Hacer que en Gnome aparezca una barra que muestre en la parte inferior de la pantalla la lista de ventanas abiertas es sencillo.  
Desde la herramienta 'Retoques' ('gnome-tweak-tool'):  
Retoques → Extensiones → Windows list: activado  

<br>
<a href="/assets/img/2021-05-20-howto_incorporar_barra_ventanas_activas_Gnome_Debian_10_y_derivados/20210521_retoques_gnome_lista_ventanas.png" target="_blank"><img src="/assets/img/2021-05-20-howto_incorporar_barra_ventanas_activas_Gnome_Debian_10_y_derivados/20210521_retoques_gnome_lista_ventanas.png" alt="Habilitando el panel de ventanas en Debian 10 desde 'gnome-tweak-tool' ('Retoques')" width="800"/></a>  
<small>Habilitando el panel de ventanas en Debian 10 desde 'gnome-tweak-tool' ('Retoques')</small>  
<br>

Y el resultado es este:  

<br>
<a href="/assets/img/2021-05-20-howto_incorporar_barra_ventanas_activas_Gnome_Debian_10_y_derivados/debian_10_barra_inferior_ventanas.png" target="_blank"><img src="/assets/img/2021-05-20-howto_incorporar_barra_ventanas_activas_Gnome_Debian_10_y_derivados/debian_10_barra_inferior_ventanas.png" alt="Recién activado panel de ventanas en Debian 10 (Gnome)" width="800"/></a>  
<small>Recién activado panel de ventanas en Debian 10 (Gnome)</small>  
<br>

<br>
<br>
Entradas relaccionadas:  
- [[HowTo] Activación del área de notificaciones de las aplicaciones ('system tray') en Debian 10](https://hijosdeinit.gitlab.io/howto_bandeja_notificaciones_system_tray_debian_netinstall/)
- [[HowTo] Incorporar los botones de 'maximizar'/'minimizar' a las ventanas en Debian 10 (Gnome)](https://hijosdeinit.gitlab.io/howto_botones_maximizar_minimizar_Debian_10_buster/)  

<br>
<br>
<br>
<br>
<br>
<br>
