---
layout: post
title: '[HowTo] Diablo 1 en GNU Linux mediante devilutionX'
date: 2020-09-21
author: Termita
category: "juegos"
tags: ["sistemas operativos", "juegos", "rpg", "Diablo", "devilutionX", "linux"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## Diablo 1
Diablo 1 es un juego rpg mítico de finales de los años 90. Supuso el inicio de toda una saga que consta de 3 entregas, 2 expansiones -que yo sepa- y multitud de [mods](https://mod.diablo.noktis.pl/).

Probablemente se puede jugar a Diablo 1 en Linux mediante 'wine', cosa que no he probado.

Sin embargo sí he probado a jugarlo, en GNU Linux, mediante <big>[**devilutionX**](https://github.com/diasurgical/devilutionX/)</big>, un sencillo programa de código abierto que, juraría, lo simplifica todo.


## Procedimiento

1. Descarga de la última versión de **devilutionX** adecuada para nuestro sistema.
En mi caso descargo la versión para x86, aunque existen versiones para otros sistemas y arquitecturas como armhf (p.ej. raspberry pi), Windows, Mac, Amiga, etcétera).  
Creamos, eso sí, una carpeta donde ubicarlo todo:
~~~
mkdir devilutionX
cd devilutionX
wget https://github.com/diasurgical/devilutionX/releases/download/1.0.1/devilutionx-linux-x86_64.7z
~~~

2. Descomprimimos el software devilutionX que acabamos de descargar
Se puede hacer desde el interface gráfico mediante, por ejemplo, el 'gestor de archivadores', o bien directamente desde terminal con el comando 7z (si no lo tenemos se instala sencillamente con un `sudo apt install p7zip-full`).
Desde la carpeta que creamos anteriormente, en la que habíamos descargado el archivo:
~~~
7z e devilutionx-linux-x86_64.7z
~~~

3. Ubicamos el archivo 'diabdat.mpq' en la carpeta aquella que habíamos creado.
El archivo 'diabdat.mpq' está en el cd original de Diablo 1, mas -si nos da pereza buscar en el trastero- también se puede descargar:
~~~
wget http://cdn.pvpgn.pro/diablo1/DIABDAT.MPQ
~~~
El nombre ha de estar en minúsculas, así que nos aseguramos de ello:
~~~
mv DIABDAT.MPQ diabdat.mpq
~~~

4. Ejecutamos el juego
~~~
./devilutionx
~~~

5. (Si faltaran dependencias) Instalar las librerías / dependencias que nos faltaran.
Que podrían ser:
~~~
sudo apt install libsdl2-ttf-2.0-0
sudo apt install libsdl2-mixer-2.0-0
sudo apt install libsdl2-image-2.0-0
sudo apt install liblua5.3-0
~~~


6. **!** Si en nuestro **PC**, aún habiendo bajado la versión para x86 (x32/x64( y realizado correctamente el procedimiento mencionado, no funcionara el script ejecutable ('./devilutionx') una solución es compilar el código fuente, [tal como explico en esta entrada del blog](https://hijosdeinit.gitlab.io/howto-compilar_devilutionX_Atom_x86_DebianBuster/).
Detecté este problema cuando traté de ejecutar la versión x86 (x32/x64) que, aparentemente, había de ser adecuada para mi netbook con procesador Atom (que, en principio, es x86).  


<br>
<br>
Las configuraciones y savegames se guardan en:
> ~/.local/share/diasurgical/devilution  

<br>
MULTIJUGADOR  
- Para poder jugar en modo multijugador el ordenador anfitrión ha de tener el puerto **6112 TCP** abierto en su cortafuegos.  
Si el anfitrión está corriendo GNU Linux Debian o derivados esto se puede hacer así:  
~~~
sudo ufw allow from ipdelordenadorcliente to any port 6112
~~~

- Las instrucciones del juego también señalan que los clientes han de tener el puerto **6112 UDP** expuesto. En mi caso no hizo falta que lo abriese, funcionó aún con el puerto cerrado.

- Una partida multijugador se guarda automáticamente. En principio al reemprender la sesión los personajes tendrán intactos el equipamiento y todos los avances realizados.

<br>
<br>
Entradas relacionadas:  
- [[HowTo] 'eduke32': un renovado Duke Nukem 3D para GNU Linux](https://hijosdeinit.gitlab.io/howto_eduke32_renovado_duke_nukem_3d_para_gnu_linux/)
- []  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[diasurgical/devilutionX](https://github.com/diasurgical/devilutionX)  
[PlayonLinew](http://www.playonlinew.com/jugar-a-diablo-en-raspberry-pi)  
[Sir Gamba](https://www.youtube.com/watch?v=PDZ3f655LMk)  
[Diablo Wiki (gamepedia)](https://diablo.gamepedia.com/Save_files_(The_Hell))</small>  
<br>
<br>
<br>
<br>
