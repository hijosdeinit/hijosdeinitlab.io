---
layout: post
title: "[HowTo] Instalación de los controladores de la tarjeta wireless Realtek RTL8822CE. (Debian 10 y derivados)"
date: 2021-06-25
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "ubuntu", "realtek", "controladores privativos", "non-free", "rtl8822ce", "c8822", "10ec:c822", "rtlwifi", "firmware", "drivers", "controladores", "hardware", "asus tuf gaming fx505"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno:  
Asus 'Tuf Gaming FX505', liveusb persistente GNU Linux Debian 10.9.0 x386 lxde</small>  

<br>
Observo que la tarjeta de red inalámbrica / bluetooth 'Realtek Semiconductor RTL8822CE 802.11ac PCIe Wireless Network Adapter' no está funcionando, pese a que el paquete ['firmware-realtek'](https://packages.debian.org/buster/firmware-realtek) -<small><span style="background-color:#042206"><span style="color:lime">`sudo apt install firmware-realtek`</span></span></small>-, que contiene todos los firmware / drivers de Realtek está ya instalado.  
Debian suele venir bastante "pelado", así que no es la primera vez que me encuentro que hay piezas de hardware que, siendo detectadas, no están trabajando por no contener o tener agregados *por defecto* el sistema base los correspondientes firmware / drivers. De hecho, en las instalaciones *net-install* de Debian existe un momento en el que el sistema solicita se le aporten -generalmente mediante un medio de almacenamiento externo- firmwares / drivers ([véase el ejemplo en este artículo del blog](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)).  

<br>
## Cómo detecta Debian 10 la tarjeta de red inalámbrica / bluetooth 'Realtek Semiconductor RTL8822CE 802.11ac PCIe Wireless Network Adapter'
~~~bash
sudo dmesg |grep c822
~~~
> [    0.619875] pci 0000:04:00.0: [10ec:c822] type 00 class 0x028000  

~~~bash
lspci
~~~
> 04:00.0 Network controller: Realtek Semiconductor Co., Ltd. Device c822  

<br>
<br>
## Instalando en Debian 10 el firmware / driver de la tarjeta de red inalámbrica / bluetooth 'Realtek Semiconductor RTL8822CE 802.11ac PCIe Wireless Network Adapter'
El procedimiento que se describirá a continuación se ha realizado -como señalo al principio de esta entrada ("entorno")- desde un liveusb persistente de Debian 10.9.0 32bits lxde.  

1. Habrá que compilar, así que es necesario [tener instalados en el sistema los paquetes necesarios para llevar a cabo la compilación](https://hijosdeinit.gitlab.io/howto_preparar_debian_para_compilar/). En concreto, para compilar este firmware/drivers son necesarios: 'make', 'gcc', 'kernel headers', 'kernel build essentials', y 'git'.  
Por otro lado, nunca está de más ejecutar <span style="background-color:#042206"><span style="color:lime">`sudo apt-get update`</span></span>.  

2. Clónese el repositorio [https://github.com/lwfinger/rtw88](https://github.com/lwfinger/rtw88)
~~~bash
git clone https://github.com/lwfinger/rtw88.git
~~~

3. Créese el directorio '/lib/modules/4.19.0-16-686/kernel/drivers/net/wireless/realtek/rtw88', dado que al compilar -en mi caso- obtuve un error inicial por incapacidad del proceso de compilación/instalación para crear dicho directorio.
~~~bash
sudo mkdir /lib/modules/'versiondekernel'/kernel/drivers/net/wireless/realtek/rtw88
~~~

4. Procédase a compilar, desde el directorio clonado en el paso nº 2
~~~bash
cd rtw88
make
sudo make install
~~~

5. Reiníciese el sistema
~~~bash
sudo reboot
~~~

<br>
<br>
<br>
## Otras formas de instalar en Debian 10 el firmware / driver de la tarjeta de red inalámbrica / bluetooth 'Realtek Semiconductor RTL8822CE 802.11ac PCIe Wireless Network Adapter'
Seguramente existirán otras formas de hacerla funcionar, otros firmwares/drivers compatibles y/o mejores, incluso es posible que el problema ante el que me encontré sea una particularidad de mi sistema liveusb persistente Debian 10.9.0 x32 lxde.  
Una de estas formas, [esta](https://wiki.debian.org/InstallingDebianOn/Lenovo/Yoga%20S730/buster), no fui capaz de adaptarla a mi caso particular (un Debian 10 ya instalado) y llevarla a la práctica.  

<br>
<br>
Entradas relaccionadas:  
- [[HowTo] Reactivación de tarjeta wireless ac 'Realtek RTL8812AU' tras actualización del kernel en Debian y derivados](https://hijosdeinit.gitlab.io/howto_reactivacion_tarjetawirelessac_realtek_rtl8812au_tras_actualizacion_kernel_debian_derivados/)
- [[HowTo] Instalación de tarjetas de red inalámbricas con chip Realtek 'RTL8812AU' en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_RTL8812AU_debian/)
- [[HowTo] Instalación de tarjeta de red inalámbricas Realtek 'RTL8822BE' en Debian 11 y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_RTL8822BE_debian11/)
- [El repositorio de firmare/drivers de GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- [[HowTo] Preparar Debian y derivados para compilar software](https://hijosdeinit.gitlab.io/howto_preparar_debian_para_compilar/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://github.com/lwfinger/rtw88">github.com - lwfinter/rtw88</a>  
<a href="https://github.com/torvalds/linux/tree/master/drivers/net/wireless/realtek/rtw88">github.com - torvalds rtw88</a>  
<a href="https://wiki.debian.org/InstallingDebianOn/Lenovo/Yoga%20S730/buster">Wiki.Debian.org - DebianOn - installing Debian Buster on</a>  
<a href="https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git">git.kernel.org - index : kernel/git/firmware/linux-firmware.git</a>  
<a href="https://linux-hardware.org/?id=pci:10ec-c822-1a3b-3750&page=2">Linux-Hardware - subsystem AzureWave (Asustek Tuf Gaming FX505 etc...</a>  
<a href="https://linux-hardware.org/?id=pci:10ec-c822-17aa-c123&page=4#status">Linux-Hardware - subsystem Lenovo</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

