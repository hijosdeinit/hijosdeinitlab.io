---
layout: post
title: "[HowTo] Trocear un video"
date: 2020-12-29
author: Termita
category: "multimedia"
tags: ["multimedia", "ffmpeg", "video", "trocear", "partir", "gnu linux", "linux", "sistemas operativos"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Lo primero es averiguar las características del video:
~~~bash
ffmpeg -i nombredelvideo
~~~

<br>
Trocearlo. Por ejemplo, para partir en 2 trozos un video que dure 02:15:08:
~~~bash
ffmpeg -i 'nombredelvideoimput' -ss 00:00:00 -t 01:08:00 -c copy nombredelvideooutput_1de2.mkv
ffmpeg -i 'nombredelvideoimput' -ss 01:08:00 -t 02:15:08 -c copy nombredelvideooutput_2de2.mkv
~~~

<br>
<br>
<br>
<br>
<br>
<br>
