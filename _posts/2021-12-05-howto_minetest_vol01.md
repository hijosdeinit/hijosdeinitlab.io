---
layout: post
title: "[HowTo] 'Minetest', el clon libre de 'Minecraft'. vol. 01"
date: 2021-12-05
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "juegos", "minetest", "minecraft", "voxel", "lua"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
[Minetest](https://www.minetest.net/) es un motor de juego de tipo voxel escrito desde cero en C++ y licenciado bajo la LPGL (versión 2.1 o anterior). Pone a disposición del usuario una fácil creación de juegos y mods usando el lenguaje de programación [Lua](https://www.lua.org/), y viene junto con un "Juego de Minetest" que es similar a Minecraft. Minetest soporta los modos creativo y supervivencia, ambos con soporte multijugador, iluminación dinámica y un generador de mapas "infinitos".  

<br>
![Minetest](/assets/img/2021-12-05-howto_minetest_vol01/minetest.jpg)  
<small>'MineTest'</small>  
<br>

'Minetest' está en los repositorios de muchas distribuciones GNU Linux como, por ejemplo, 'Debian'.  
Asimismo, 'Minetest' también dispone de un [repositorio en 'GitHub'](https://github.com/minetest).  
En los repositorios de Debian existen varios paquetes relacionados con Minetest, listos para instalar según lo que se necesite.
- `minetest`: el juego y sus datos (desconozco aún si trae el servidor por defecto)
- `minetest-server`: el servidor de Minetest; se puede instalar independientemente del juego en sí
- `minetestmapper`: utilidad para generar un mapa desde un mundo de Minetest
- `minetest-mod-*`: *'third-party'* mods (opcionales)  

<br>
## Configuración
Los archivos de configuración de 'Minetest' están en el subdirectorio `~/.minetest`, que se crea apenas se lanza por primera vez el juego.  
Dentro de ese subdirectorio, el archivo `minetest.conf` contiene las preferencias del jugador, que pueden ser modificadas directamente en ese archivo o bien a través del juego.  
En la [*wiki*](https://wiki.minetest.net) del juego se tratan en profundidad estos y otros detalles.  

<br>
## Instalación de 'Minetest' en Debian y derivados
~~~bash
sudo apt-get update
sudo apt install minetest
~~~
Si se desea instalar una versión más actualizada, en la [instalación se puede "invocar" a los 'backports' de Debian](https://hijosdeinit.gitlab.io/howto_minetest_vol01/).  
Multijugador: Es importante que la versión del cliente de 'minetest' que ejecutemos sea compatible con la versión del servidor al que queramos conectarnos.  
<br>
Para lanzar 'Minetest' basta con ejecutar <span style="background-color:#042206"><span style="color:lime">`minetest`</span></span> desde la terminal, o bien echar mano del menú correspondiente en nuestra distribución GNU Linux.  

<br>
## Comandos de la consola de 'Minetest'
Dentro del juego se pueden ejecutar comandos abriendo la "consola". Las teclas predefinidas para tal efecto -abrir la consola de Minetest- es <span style="background-color:#042206"><span style="color:lime">`/`</span></span> o <span style="background-color:#042206"><span style="color:lime">`F10`</span></span>  

Algunos comandos importantes que se pueden ejecutar en la consola del juego son:
~~~
<span style="background-color:#042206"><span style="color:lime">`/status`</span></span> - Get the status of the server: roster, message of the day.
<span style="background-color:#042206"><span style="color:lime">`/privs`</span></span> - View privileges.
<span style="background-color:#042206"><span style="color:lime">`/privs <player>`</span></span> - See privileges 'player'. Requires privilege 'privs'.
<span style="background-color:#042206"><span style="color:lime">`/grant <player> <priv>`</span></span> - Leave a privilege to 'player'. Requires privilege 'privs'.
<span style="background-color:#042206"><span style="color:lime">`/revoke <player> <priv>`</span></span> - Remove a privilege to 'player'. Requires privilege 'privs'.
<span style="background-color:#042206"><span style="color:lime">`/time <time>`</span></span> - Set the time of day. 0 and 24000 correspond to midnight, 12000 and 5000 at noon at dawn. (time * 1000) . Requires privilege 'time'.
<span style="background-color:#042206"><span style="color:lime">`/shutdown`</span></span> - Turn off the server.
<span style="background-color:#042206"><span style="color:lime">`/setting <name> = <value>`</span></span> - Adds or replaces a parameter in the configuration file. The parameter can not be applied properly before restarting the server.
<span style="background-color:#042206"><span style="color:lime">`/teleport <x>,<y>,<z>`</span></span> - Teleport to the indicated position. Requires privilege 'teleport'.
<span style="background-color:#042206"><span style="color:lime">`/grantme <priv>`</span></span> - Grant yourself privileges. To easily grant yourself all privileges in singleplayer, you might consider running /grantme all
~~~

Por ejemplo:  
Para volar: <span style="background-color:#042206"><span style="color:lime">`grantme fly`</span></span> (la letra <span style="background-color:#042206"><span style="color:lime">'k'</span></span> activa el vuelo)  
Para ir deprisa: <span style="background-color:#042206"><span style="color:lime">`grantme fast`</span></span> (la letra <span style="background-color:#042206"><span style="color:lime">'j'</span></span> activa el ir deprisa)  
Para garantizarme todos los privilegios: `grantme all`</span></span>

<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://wiki.debian.org/Games/Minetest">Debian *wiki*</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>