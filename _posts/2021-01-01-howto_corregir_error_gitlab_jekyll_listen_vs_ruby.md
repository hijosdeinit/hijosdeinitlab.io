---
layout: post
title: "[HowTo] Corregir error en Jekyll (Gitlab): versión listen vs versión ruby"
date: 2021-01-01
author: Termita
category: "programación"
tags: ["listen", "error", "ci", "blog", "web 4.0", "Jekyll", "Ruby", "GitLab", "GitHub", "programación", "markdown", "texto plano", "html", "estático"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Mi último push a este blog, que está alojado en Gitlab, fue hace 5 días (2020-12-27).  
Hoy (2021-01-01) iba a *pushear* contenido. He obtenido un ERROR:
~~~
Executing "step_script" stage of the job script
00:04
$ gem install bundler
Successfully installed bundler-2.2.4
1 gem installed
$ bundle install
Fetching gem metadata from https://rubygems.org/..........
Fetching gem metadata from https://rubygems.org/.
Resolving dependencies...
listen-3.2.1 requires ruby version >= 2.2.7, ~> 2.2, which is incompatible with
the current version, ruby 3.0.0p0
Cleaning up file based variables
00:01
ERROR: Job failed: exit code 1
~~~

<br>
Resumiendo: Pareciera que [GitLab ha actualizado *Ruby* tal como se rumoreaba que haría estas navidades](https://gitlab.com/groups/gitlab-org/-/epics/4408) y esto ha repercutido en una incompatibilidad entre la **versión de** ***listen*** (listen-3.2.1) y la **versión de** ***Ruby*** (ruby 3.0.0p0) y sus dependencias.  

<br>
<big>SOLUCIÓN</big>  
Hay que editar el archivo '.gitlab-ci-yml' (es conveniente hacer una copia de seguridad primero) y añadir en el apartado 'before_script', justo antes de `-bundle install` lo siguiente: <big>`- bundle update`</big> de tal forma que el apartado quedará así:
~~~yaml
before_script:
  - gem install bundler
#inicio_añadido por mí 2021-01-01
  - bundle update
#fin_añadido por mí 2021-01-01
  - bundle install
~~~

<br>
<br>
Entradas relacionadas:
- [Corregir error en Jekyll (Gitlab): mis incompatibilidades con la útima versión de ruby disponible](https://hijosdeinit.gitlab.io/howto_corregir_error_gitlab_jekyll_ruby3.4.0/)
- [[HowTo] Generación automática de sitemap.xml para blog Jekyll en GitLab](https://hijosdeinit.gitlab.io/howto_generar-autom%C3%A1ticamente-sitemap.xml_jekyll_gitlab/)
- [[HowTo] Creación de un blog Jekyll en GitLab](https://hijosdeinit.gitlab.io/creaci%C3%B3n-blog-jekyll-en-gitlab/)  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://github.com/guard/listen/issues/382#issuecomment-216433297" target="_blank">GitLab</a>  
<br>
<br>
<br>
<br>
