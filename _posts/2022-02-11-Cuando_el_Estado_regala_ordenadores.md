---
layout: post
title: 'Cuando el ESTADO "regala" ordenadores'
date: 2022-02-11
author: Termita
category: "liberticidio"
tags: ["gratis", "servicios", "Ivoox", "Alphabet", "Google", "Facebook", "mendicidad 4.0", "patreon", "mercaderes", "mercantil", "suscripción", "incerteza", "dependencia", "web 4.0", "tecnofilia", "creditofagia", "tecnofilos", "creditofagos", "posmodernidad", "absurdo", "coherencia", "incoherencia", "progreso", "hardware", "software", "sostenible", "verde", "obsolescencia programada", "obsolescencia", "embudo", "retro", "arqueotecnologia", "retromatica", "vim", "i3wm"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Esta noche al volver de trabajar me encuentro con que a mi hijo unos obedientes funcionarios del Estado que hieden a gel hidroalcohólico (no a coñac) y pasean carpetas por esos edificios llamados "colegios" (maestros se hacen llamar) le han dado un ordenador.  
A sus otros compañeros también.  
La horda de calzonazos y meonas -o sea, los papis- no tiene dinero para ordenadores pero sí para smartphones, para mascarillas de diseño, o para desembuchar 200 pavos para que sus críos se vayan a las "convivencias" del cole a descubrir lo que es una gallina o abrazar un manzano.  
Aquí, en este mi hogar -que es el único Estado al que sirvo y rindo pleitesía, soberano y tan humilde como poco dado a aceptar limosnas-, ya tenemos ordenadores, pagados con el sudor de la frente. La mejor computadora de esta santa casa es la de mis hijos: un portátil de la serie *Tuff Gaming* y un Lenovo *Legion*.  
No hizo falta dejar de comer postre para pagarlos. El último me lo malvendió un tal Converso. Uno de sus caprichos, fruto -diríase- de decisiones de cadáver financiero.  
El ordenador ese con el que el estado pareciera pretende comprar voluntades no valdrá más de 300 €. El precio es futil, no obstante. Lo importante es "lo otro" :), lo que subyace.  
Procesador celeron, 8gigas de ram. Una marca puntera: Acer :p Yo me parto.  
Eso sí, bien cargadito con su *Windowsdiez* y una máquina virtual con el Ubuntu "trucado" por el Estado, o sea, la Generalidad de Cataluña. Un linux de virtualbox, cuya comunidad es tan "potente" que el último mensaje de su foro es del 2019. Bugs a mansalva, según se ve en algunos mensajes. A saber cuántos miles de euros de nuestros bolsillos pagaron tamaña mierda.  
Por supuesto, todo el ecosistema Google preinstalado.  
No vayan a ocurrírseles meter GNU Linux nativo en un arranque dual, no vayan a marearse.  

<br>
Se supone que debo conectar ese trasto a la red de mi casa.  
Antes me clavo un hacha en la cara.  

<br>
Imagínense qué gran idea hubiera sido si, en aquellos años del plomo, el Estado hubiera regalado en Vascongadas un ordenador a cada niño vasco y una conexión a internet. Menudo filón. Los calabozos de Intxaurrondo no hubieran dado abasto.  

Regalicos del Estado...  
Y yo no puedo evitar acordarme de aquel episodio de la guerra de Troya... y también de 1984 y del cuento de la casita de chocolate, todo en tropel.  

Estoy por echar con esa joyita un partido de fútbol en la puerta del colegio.  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Mantener vivo un iPad antiguo, vol.01: instalar aplicaciones 'no tenidas antes'](https://hijosdeinit.gitlab.io/howto_mantener_vivo_ipad_antiguo_vol01__instalar_aplicaciones_no_tenidas_antes/)
- [https://hijosdeinit.gitlab.io/Germen_rendicion_gnulinux_a_obsolescencia/](https://hijosdeinit.gitlab.io/Germen_rendicion_gnulinux_a_obsolescencia/)
- [[HowTo] Instalación netinstall de Debian en netbook Compaq Mini](https://hijosdeinit.gitlab.io/howto_instalacion_netinstall_Debian_netbook_compaqmini/)
- [[HowTo] búsqueda de páginas web ‘desaparecidas’, directamente en la caché de Google](https://hijosdeinit.gitlab.io/howto_busqueda_en_cache_de_google_paginas_desaparecidas/)
- [Saque todo lo que pueda de los soportes ópticos (cd/dvd), si no lo ha hecho ya](https://hijosdeinit.gitlab.io/saque-todo-lo-que-pueda-de-los-soportes/)
- [Otra faceta de la vida útil de las memorias usb](https://hijosdeinit.gitlab.io/otra-faceta-vida-util-memoria-usb/)
- [Qué pedirle a un ‘pendrive’ USB](https://hijosdeinit.gitlab.io/requisitos_de_un_pendrive/)
- ["Gratis"](https://hijosdeinit.gitlab.io/lo_gratis/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
