---
layout: post
title: "[HowTo] Averiguación desde línea de comandos (CLI) de la versión del sistema operativo GNU Linux"
date: 2024-03-14
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "cli", "kernel", "version", "codename", "os-release", "lsb_release", "hostnamectl", "tzdata"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Es frecuente que, sobre todo cuando ha pasado bastante tiempo desde la instalación del sistema operativo GNU Linux, no recordemos su versión.  
Ésta puede averiguarse desde línea de comandos (CLI) de varias formas:

<br>
En sistemas GNU Linux que no incorporan '/etc/os-release' como Debian *Squeeze*, puede explotarse 'tzdata' para averiguar el *codename* y/o la versión del sistema. En sistemas basados en Debian como LMDE (*Linux Mint Debian Edition*) obtendremos el codename de la versión de Debian **en que están basados**:  
<span style="background-color:#042206"><span style="color:lime">`dpkg --status tzdata|grep Provides|cut -f2 -d'-'`</span></span>  

ô también, empleando menos "tuberías":  
<span style="background-color:#042206"><span style="color:lime">`dpkg --status tzdata|awk -F'[:-]' '$1=="Provides"{print $NF}'`</span></span>  

<br>
<br>
<span style="background-color:#042206"><span style="color:lime">`cat /etc/os-release`</span></span> nos dará una información parecida a esta:
~~~
PRETTY_NAME="Debian GNU/Linux 11 (bullseye)"
NAME="Debian GNU/Linux"
VERSION_ID="11"
VERSION="11 (bullseye)"
VERSION_CODENAME=bullseye
ID=debian
HOME_URL="https://www.debian.org/"
SUPPORT_URL="https://www.debian.org/support"
BUG_REPORT_URL="https://bugs.debian.org/"
~~~

<br>
<br>
<span style="background-color:#042206"><span style="color:lime">`lsb_release -c`</span></span> informa acerca del *codename* -una especie de alias- de la versión de nuestro sistema operativo GNU Linux
> Codename:       bullseye  

<br>
<br>
<span style="background-color:#042206"><span style="color:lime">`hostnamectl | grep "Operating System"`</span></span> menciona tanto el *codename* como el número de versión
> Operating System: Debian GNU/Linux 11 (bullseye)  


<br>
<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://unix.stackexchange.com/questions/180776/how-to-get-the-debian-codename-without-lsb-release">unix.stackexchange.com</a>  

<br>
<br>
<br>
<br>
<br>
<br>
