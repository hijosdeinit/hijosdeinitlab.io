---
layout: post
title: '[HowTo] Conocer si un programa está instalado en nuestro sistema GNU Linux'
date: 2020-10-05
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "programas", "dpkg", "which", "whatis"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Hay veces en que uno desea conocer qué programas tiene instalados en su sistema operativo GNU Linux. Esto suele acontecer cuando recién acabamos de instalar el sistema, cuando buscamos un programa o una alternativa, etcétera...  

<br>
Conociendo qué programas tenemos en nuestro sistema GNU Linux  
Por ejemplo, quiero saber si tengo el comando 'tree' instalado.  

<br>
### mediante 'which'
~~~bash
which tree
~~~
> /usr/bin/tree  

La respuesta indica que, afirmativo, 'tree' sí está instalado.

<br>
### mediante 'dpkg'
~~~bash
dpkg -s tree
~~~
><small>Package: tree  
Status: install ok installed  
Priority: optional  
Section: utils  
Installed-Size: 103  
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>  
Architecture: amd64  
Version: 1.7.0-5  
Depends: libc6 (>= 2.14)  
Description: displays an indented directory tree, in color  
 Tree is a recursive directory listing command that produces a depth indented listing of files, which is colorized ala dircolors if the LS_COLORS environment variable is set and output is to tty.  
Original-Maintainer: Florian Ernst <florian@debian.org>  
Homepage: http://mama.indstate.edu/users/ice/tree/</small>  

~~~bash
dpkg-query -l tree
~~~
><small>Deseado=desconocido(U)/Instalar/eliminaR/Purgar/retener(H)  
| Estado=No/Inst/ficheros-Conf/desempaqUetado/medio-conF/medio-inst(H)/espera-disparo(W)/pendienTe-disparo  
|/ Err?=(ninguno)/requiere-Reinst (Estado,Err: mayúsc.=malo)  
||/ Nombre                      Versión            Arquitectura       Descripción  
+++-===========================-==================-==================-===========================================================  
ii  tree                        1.7.0-5            amd64              displays an indented directory tree, in color</small>  

~~~bash
dpkg --list | grep -i tree
~~~
><small>ii  tree  1.7.0-5    amd64   displays an indented directory tree, in color</small>  

Nuevamente confirmamos que 'tree' está instalado.  

<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[maslinux](https://maslinux.es/ver-si-un-paquete-esta-instalado-o-no-en-gnu-linux/)</small>  
<br>
<br>
<br>
<br>
