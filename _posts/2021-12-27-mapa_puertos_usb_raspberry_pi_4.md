---
layout: post
title: "Mapa de los puertos usb de RaspBerry Pi 4b"
date: 2021-12-27
author: Termita
category: "hardware"
tags: ["hardware", "raspberry", "raspberry pi", "raspberry pi 4b", "hardware", "usb", "usb 2.0", "usb 3.0"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
RaspBerry Pi 4b tiene 2 puertos USB 3.0 y 2 puertos USB 2.0  
Los puertos usb azules son usb 3.0, mientras que los puertos usb negros son usb 2.0.  
El mapa de puertos USB es el siguiente:
- Bus 1, port 1 - USB1/2 device in upper USB3 (blue) port
- Bus 1, port 2 - USB1/2 device in lower USB3 (blue) port
- Bus 1, port 3 - USB1/2 device in upper USB2 (black) port
- Bus 1, port 4 - USB1/2 device in lower USB2 (black) port
- Bus 2, port 1 - USB3 device in upper USB3 (blue) port
- Bus 2, port 2 - USB3 device in lower USB3 (blue) port  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://forums.raspberrypi.com/viewtopic.php?t=283039">forums.raspberrypi.com</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
