---
layout: post
title: "[HowTo] instalacion de HandBrake en Ubuntu y derivados"
date: 2020-05-14
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "repositorio", "software", "edicion de video", "handbrake", "convertir", "transcodificar", "codec", "mencoder", "software", "ffmpeg", "audio", "video", "multimedia"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---

## Básicamente existen 2 opciones
- Instalar desde la "tienda" (Centro de Software) de Ubuntu o Synaptic
- Instalar desde el repositorio oficial de HandBrake  

<br>
<br>
## Instalación desde repositorio oficial de HandBrake
Yo he preferido instalar HandBrake desde su repositorio oficial por 2 razones:
- Es la versión más actualizada
- Se instala de forma "nativa", es decir, no es un paquete "de nueva paquetería" sino ad hoc.  

### Procedimiento
El procedimiento es el siguiente
~~~
sudo add-apt-repository ppa:stebbins/handbrake-releases
sudo apt-get update
sudo apt install handbrake-gtk
~~~  
<br>
![HandBrake en Ubuntu 18]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-05-14-[HowTo]-instalacion-handbrake-ubuntu-apt/handbrake_first_run.png)  
<br>
<br>
<br>
--- --- ---
<br>
Fuentes:  
[DriveMeca](https://www.drivemeca.com/handbrake-tutorial-para-linux/)
