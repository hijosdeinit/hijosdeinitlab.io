---
layout: post
title: "Software de virtualización (máquinas virtuales)"
date: 2021-02-10
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "windows", "windows 10", "virtualizacion", "maquinas virtuales", "disco duro virtual", "almacenamiento virtual", "virtual box", "vmware workstation", "vmware player", "qemu", "gnome boxes", "vmware esxxi", "vmware esx", "proxmox ve", "parallels desktop", "xen", "docker", "kubernetes", "podman", "wine", "dosbox", "scummvm", "emulacion", "emulador"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## Un listado de software de virtualización
<small>De todos estos sólo he utilizado los 3 primeros.</small>
- [VMware Workstation, VMware player, etc...](https://www.vmware.com/es/products/workstation-pro.html)
- [VirtualBox](https://www.virtualbox.org/)
- [Qemu](https://wiki.qemu.org/Index.html)
- [KVM](https://linux-kvm.org/page/Main_Page)
- [Gnome Boxes](https://wiki.gnome.org/Apps/Boxes)
- [VMware ESXXI (antiguamente VMware ESX)](https://www.vmware.com/products/esxi-and-esx.html)
- [Proxmox VE](https://www.proxmox.com/en/proxmox-ve)
- [Parallels Desktop](https://www.parallels.com/products/desktop/)
- [XEN](https://xenproject.org/)
- [OpenVZ](https://es.wikipedia.org/wiki/OpenVZ)
- [Virtuozzo](https://es.wikipedia.org/wiki/Virtuozzo)
- [Vagrant](https://www.vagrantup.com/)  

<br>
Otras herramientas que pertenecen al campo semántico de la virtualización:
<br>
<br>
## Virtualización a nivel de sistema operativo (contenedorización)
[Virtualización a nivel de sistema operativo](https://es.wikipedia.org/wiki/Virtualizaci%C3%B3n_a_nivel_de_sistema_operativo), también llamada virtualización basada en contenedores, contenerización​ o contenedorización es un método de virtualización en el que, sobre el núcleo del sistema operativo se ejecuta una capa de virtualización que permite que existan múltiples instancias aisladas de espacios de usuario, en lugar de solo uno. Tales instancias, las cuales son llamadas contenedores, contenedores de software, jaulas o prisiones, pueden verse y sentirse como un servidor real desde el punto de vista de sus dueños y usuarios. Al software que permite el alojamiento de distintos contenedores se le llama motor de contenedores. Además de mecanismos de aislamiento, el kernel a menudo proporciona mecanismos de administración de recursos para limitar el impacto de las actividades de un contenedor sobre otros contenedores.  
[Docker](https://www.docker.com/)  
<small>Docker es un proyecto de código abierto que automatiza el despliegue de aplicaciones dentro de contenedores de software, proporcionando una capa adicional de abstracción y automatización de virtualización de aplicaciones en múltiples sistemas operativos. Docker utiliza características de aislamiento de recursos del kernel Linux, tales como cgroups y espacios de nombres (namespaces) para permitir que "contenedores" independientes se ejecuten dentro de una sola instancia de Linux, evitando la sobrecarga de iniciar y mantener máquinas virtuales.  
El soporte del kernel Linux para los espacios de nombres aísla la vista que tiene una aplicación de su entorno operativo,​ incluyendo árboles de proceso, red, ID de usuario y sistemas de archivos montados, mientras que los cgroups del kernel proporcionan aislamiento de recursos, incluyendo la CPU, la memoria, el bloque de E/S y de la red. Desde la versión 0.9, Docker incluye la biblioteca libcontainer como su propia manera de utilizar directamente las facilidades de virtualización que ofrece el kernel Linux, además de utilizar las interfaces abstraídas de virtualización mediante libvirt, LXC (Linux Containers) y systemd-nspawn.  
De acuerdo con la firma analista de la industria 451 Research, "Docker es una herramienta que puede empaquetar una aplicación y sus dependencias en un contenedor virtual que se puede ejecutar en cualquier servidor Linux. Esto ayuda a permitir la flexibilidad y portabilidad en donde la aplicación se puede ejecutar, ya sea en las instalaciones físicas, la nube pública, nube privada, etc."</small>  

[Kubernetes](https://kubernetes.io/)  
<small>Kubernetes (referido en inglés comúnmente como “K8s”) es un sistema de código libre para la automatización del despliegue, ajuste de escala y manejo de aplicaciones en contenedores1​ que fue originalmente diseñado por Google y donado a la Cloud Native Computing Foundation (parte de la Linux Foundation). Soporta diferentes entornos para la ejecución de contenedores, incluido Docker.</small>  

[Podman](https://podman.io/)  
<small>«Podman is a daemonless container engine for developing, managing, and running OCI Containers on your Linux System. Containers can either be run as root or in rootless mode». Podman es un motor de contenedores (container engine) desarrollado por RedHat. [Podman](https://www.atareao.es/tutorial/podman/) quiere ser la alternativa al archiconocido motor Docker para contenedores.</small>  

<br>
## software que, no siendo de virtualización, permite, por ejemplo, ejecutar aplicaciones de un sistema en otro:
[wine](https://www.winehq.org/)  
<small>Wine (acrónimo recursivo en inglés para Wine Is Not an Emulator, que significa «Wine no es un emulador») es una reimplementación de la interfaz de programación de aplicaciones de Win16 y Win32 para sistemas operativos basados en Unix. Permite la ejecución de programas diseñados para MS-DOS, y las versiones de Microsoft Windows 3.11, 95, 98, Me, NT, 2000, XP, Vista, 7, 8 y 10.  
El nombre Wine inicialmente fue un acrónimo para WINdows Emulator. Este significado fue cambiado posteriormente al acrónimo recursivo actual. El doble significado deriva del hecho de que WINE es un emulador de API de SOs, en contraposición a un entorno de máquina o un emulador de CPU. Algunas personas no aplicarían el término emulador a lo que algunos autores​ llaman un emulador de API porque la herramienta es realmente una reimplementación de una API existente, no una forma de emular un entorno de máquina completo.  
Wine provee de:  
Un conjunto de herramientas de desarrollo para portar código fuente de aplicaciones Windows a Unix.  
Un cargador de programas, el cual permite que muchas aplicaciones para Windows 2.0/3.x/9X/ME/NT/2000/XP/Vista/7 y 8 se ejecuten sin modificarse en varios sistemas operativos Unix como macOS, BSD y Unix-like como GNU/Linux o Solaris.</small>  

[Codeweavers Crossover](https://www.codeweavers.com/)  
<small>«CrossOver is a Microsoft Windows compatibility layer available for Linux, macOS, and Chrome OS. This compatibility layer enables many Windows-based applications to run on Linux operating systems, macOS, or Chrome OS.  
CrossOver is developed by CodeWeavers and based on Wine, an open-source Windows compatibility layer. CodeWeavers modifies the Wine source code, applies compatibility patches, adds configuration tools that are more user-friendly, automated installation scripts, and provides technical support. All changes made to the Wine source code are covered by the LGPL and publicly available. CodeWeavers maintains an online database listing how well various Windows applications perform under CrossOver».</small>  

[cygwin](http://www.cygwin.com/)  
<small>Cygwin es una colección de herramientas desarrollada por Cygnus Solutions para proporcionar un comportamiento similar a los sistemas Unix en Microsoft Windows. Su objetivo es portar software que ejecuta en sistemas POSIX a Windows con una recompilación a partir de sus fuentes. Aunque los programas portados funcionan en todas las versiones de Windows, su comportamiento es mejor en Windows NT, Windows XP y Windows Server 2003.
[Esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_cygwin_GNU_Linux_en_Windows/) trata de cygwin</small>  

[dosbox](https://www.dosbox.com/news.php?show_news=1)  
<small>DOSBox es un emulador que recrea un entorno similar al sistema DOS con el objetivo de poder ejecutar programas y videojuegos originalmente escritos para el sistema operativo MS-DOS de Microsoft en computadoras más modernas o en diferentes arquitecturas (como Power PC). También permite que estos juegos funcionen en otros sistemas operativos como GNU/Linux. Fue hecho porque Windows XP ya no se basa en MS DOS y pasó a basarse a Windows NT.</small>

[emuladores]()  
<small>Hay emuladores de consolas, de ordenadores antiguos, etc...</small>  

<br>
## un pequeño glosario
["virtualización"](https://es.wikipedia.org/wiki/Virtualizaci%C3%B3n)  
<small>La virtualización es la creación a través de software de una representación (versión virtual) de algún recurso tecnológico.1​ Por tanto este software tiene la función de simular la existencia del recurso tecnológico que se quiere virtualizar. En los ámbitos de habla inglesa, este término se suele conocer por el numerónimo "v12n".</small>  

["paravirtualización"](https://es.wikipedia.org/wiki/Paravirtualizaci%C3%B3n)  
<small>La paravirtualización es una técnica de programación informática que permite virtualizar por software sistemas operativos. El programa paravirtualizador presenta una interfaz de manejo de máquinas virtuales. Cada máquina virtual se comporta como un computador independiente, por lo que permite usar un sistema operativo o varios por computador emulado.  
La intención de la interfaz modificada es reducir la porción del tiempo de ejecución del usuario, empleado en operaciones que son sustancialmente más difíciles de ejecutar en un entorno virtual, comparado con un entorno no virtualizado. La paravirtualización provee filtros especialmente definidos para permitir a los invitados y al anfitrión hacer peticiones y conocer estas tareas, que de otro modo serían ejecutadas en el dominio virtual (donde el rendimiento de la ejecución es peor.) Por lo tanto, una plataforma de paravirtualización exitosa puede permitir que el monitor de la máquina virtual (VMM) sea más simple (por traslado de la ejecución de tareas críticas desde el dominio virtual al anfitrión de dominio), y/o que reduzca la degradación del rendimiento global de la ejecución de la máquina dentro del anfitrión virtual.  
La paravirtualización requiere que el sistema operativo invitado sea portado de manera explícita para la API. Una distribución de un sistema operativo convencional que no soporte paravirtualización no puede ser ejecutada ni visualizada en un monitor de máquina virtual VMM.1​  
La paravirtualización es un término nuevo para una vieja idea. el sistema operativo VM de IBM ha ofrecido tales características desde 1972 (y antes como CP-67). En el mundo de máquinas virtuales, esto se conoce como "código de DIAGNÓSTICO", debido a que usa un código de instrucción usado normalmente solo por software para mantenimiento de hardware y por lo tanto indefinido.</small>  

["hipervisor"](https://es.wikipedia.org/wiki/Hipervisor)  
<small>Un hipervisor (en inglés *hypervisor*) o monitor de máquina virtual (virtual machine monitor)1​ es una capa de software para realizar una virtualización de hardware que permite utilizar, al mismo tiempo, diferentes sistemas operativos (sin modificar o modificados, en el caso de paravirtualización) en una misma computadora.2​3​4​ Es una extensión de un término anterior, «supervisor», que se aplicaba a los kernels de los sistemas operativos de computadora.</small>  

["cooperative linux"](https://es.wikipedia.org/wiki/Cooperative_Linux)  
<small>Cooperative Linux, abreviado como coLinux, es un software que permite a Microsoft Windows y a Linux ejecutarse en forma paralela en la misma computadora.  
coLinux utiliza el concepto de una máquina virtual cooperativa (CVM). En contraste a máquinas virtuales tradicionales, la CVM comparte los recursos que ya existen en el sistema operativo principal. En las máquinas virtuales, los recursos son virtualizados para cada sistema operativo huésped. La CVM da a ambos sistemas operativos el poder para usar los recursos de forma paralela. La máquina virtual tradicional pone a cada sistema operativo huésped en una "caja de arena"(*sandbox*).</small>  

["Kernel-based Virtual Machine (KVM)"](https://es.wikipedia.org/wiki/Kernel-based_Virtual_Machine)  
<small>Máquina virtual basada en el núcleo. Es una solución para implementar virtualización completa con Linux. Está formada por un módulo del núcleo (con el nombre *kvm.ko*) y herramientas en el espacio de usuario, siendo en su totalidad software libre. El componente KVM para el núcleo está incluido en Linux desde la versión 2.6.20.  
KVM permite ejecutar máquinas virtuales utilizando imágenes de disco que contienen sistemas operativos sin modificar. Cada máquina virtual tiene su propio hardware virtualizado: una tarjeta de red, discos duros, tarjeta gráfica, etc.</small>  

[OpenVZ](https://es.wikipedia.org/wiki/OpenVZ)  
<small>*OpenVZ* es una tecnología de virtualización en el nivel de sistema operativo para Linux. OpenVZ permite que un servidor físico ejecute múltiples instancias de sistemas operativos aislados, conocidos como Servidores Privados Virtuales (SPV o VPS en inglés) o Entornos Virtuales (EV).  
Si se lo compara a máquinas virtuales tales como VMware, VirtualBox y las tecnologías de virtualización tales como Xen, OpenVZ ofrece menor flexibilidad en la elección del sistema operativo: tanto los huéspedes como los anfitriones deben ser Linux (aunque las distribuciones de GNU/Linux pueden ser diferentes en diferentes EVs). Sin embargo, la virtualización en el nivel de sistema operativo de OpenVZ proporciona mejor rendimiento, escalabilidad, densidad, administración de recursos dinámicos, y facilidad de administración que las alternativas.  
OpenVZ es una base de Virtuozzo que es un software comercial desarrollado por SWsoft, Inc., OpenVZ es un producto de software libre y licenciado bajo los términos de la licencia GNU GPL versión 2.  
OpenVZ consiste del núcleo y de herramientas en el nivel de usuario.</small>  

[Proxmox Virtual Environment](https://es.wikipedia.org/wiki/Proxmox_Virtual_Environment)  
<small>*Proxmox Virtual Environment*, o *Proxmox VE*, entorno de virtualización de servidores de código abierto. Está en distribuciones GNU/Linux basadas en Debian con una versión modificada del Kernel RHEL y permite el despliegue y la gestión de máquinas virtuales y contenedores.1​ 2​ Proxmox VE incluye una consola Web y herramientas de línea de comandos, y proporciona una API REST para herramientas de terceros. Dos tipos de virtualización son compatibles: los contenedores basados con LXC (a partir de la versión 4.0 reemplaza OpenVZ, utilizado en la versión 3.4, incluido3​), y la virtualización con KVM.4​ Viene con un instalador e incluye un sitio Web basado en la interfaz de administración.  
Proxmox VE está bajo la licencia GNU Affero General Public License, versión 3.</small>  

[Virtual 8086 mode](https://en.wikipedia.org/wiki/Virtual_8086_mode)  
<small>«Based on painful experiences with the 80286 protected mode, which by itself was not suitable enough to run concurrent DOS applications well, Intel introduced the virtual 8086 mode in their 80386 chip, which offered virtualized 8086 processors on the 386 and later chips. Hardware support for virtualizing the protected mode itself, however, became available 20 years later».</small>  

[AMD virtualization (AMD-V)](https://en.wikipedia.org/wiki/X86_virtualization#AMD_virtualization_(AMD-V))  
<small>«AMD developed its first generation virtualization extensions under the code name "Pacifica", and initially published them as AMD Secure Virtual Machine (SVM), but later marketed them under the trademark AMD Virtualization, abbreviated AMD-V.  
On May 23, 2006, AMD released the Athlon 64 ("Orleans"), the Athlon 64 X2 ("Windsor") and the Athlon 64 FX ("Windsor") as the first AMD processors to support this technology.  
AMD-V capability also features on the Athlon 64 and Athlon 64 X2 family of processors with revisions "F" or "G" on socket AM2, Turion 64 X2, and Opteron 2nd generation and third-generation, Phenom and Phenom II processors. The APU Fusion processors support AMD-V. AMD-V is not supported by any Socket 939 processors. The only Sempron processors which support it are APUs and Huron, Regor, Sargas desktop CPUs.  
AMD Opteron CPUs beginning with the Family 0x10 Barcelona line, and Phenom II CPUs, support a second generation hardware virtualization technology called Rapid Virtualization Indexing (formerly known as Nested Page Tables during its development), later adopted by Intel as Extended Page Tables (EPT).  
As of 2019, all Zen-based AMD processors support AMD-V.  
The CPU flag for AMD-V is "svm". This may be checked in BSD derivatives via dmesg or sysctl and in Linux via '/proc/cpuinfo'. Instructions in AMD-V include VMRUN, VMLOAD, VMSAVE, CLGI, VMMCALL, INVLPGA, SKINIT, and STGI.  
With some motherboards, users must enable AMD SVM feature in the BIOS setup before applications can make use of it».</small>  

[Intel virtualization (VT-x/VT-d)](https://en.wikipedia.org/wiki/X86_virtualization#Intel-VT-x)  
<small>«Previously codenamed "Vanderpool", VT-x represents Intel's technology for virtualization on the x86 platform. On November 13, 2005, Intel released two models of Pentium 4 (Model 662 and 672) as the first Intel processors to support VT-x. The CPU flag for VT-x capability is "vmx"; in Linux, this can be checked via /proc/cpuinfo, or in macOS via sysctl machdep.cpu.features.  
"VMX" stands for Virtual Machine Extensions, which adds 13 new instructions: VMPTRLD, VMPTRST, VMCLEAR, VMREAD, VMWRITE, VMCALL, VMLAUNCH, VMRESUME, VMXOFF, VMXON, INVEPT, INVVPID, and VMFUNC. These instructions permit entering and exiting a virtual execution mode where the guest OS perceives itself as running with full privilege (ring 0), but the host OS remains protected.  
As of 2015, almost all newer server, desktop and mobile Intel processors support VT-x, with some of the Intel Atom processors as the primary exception. With some motherboards, users must enable Intel's VT-x feature in the BIOS setup before applications can make use of it.  
Intel started to include Extended Page Tables (EPT), a technology for page-table virtualization, since the Nehalem architecture, released in 2008. In 2010, Westmere added support for launching the logical processor directly in real mode – a feature called "unrestricted guest", which requires EPT to work.  
Since the Haswell microarchitecture (announced in 2013), Intel started to include VMCS shadowing as a technology that accelerates nested virtualization of VMMs. The virtual machine control structure (VMCS) is a data structure in memory that exists exactly once per VM, while it is managed by the VMM. With every change of the execution context between different VMs, the VMCS is restored for the current VM, defining the state of the VM's virtual processor. As soon as more than one VMM or nested VMMs are used, a problem appears in a way similar to what required shadow page table management to be invented, as described above. In such cases, VMCS needs to be shadowed multiple times (in case of nesting) and partially implemented in software in case there is no hardware support by the processor. To make shadow VMCS handling more efficient, Intel implemented hardware support for VMCS shadowing».</small>  

[Intel VT-i](https://en.wikipedia.org/wiki/IA-64#Architectural_changes)  
<small>«Itanium processors released prior to 2006 had hardware support for the IA-32 architecture to permit support for legacy server applications, but performance for IA-32 code was much worse than for native code and also worse than the performance of contemporaneous x86 processors. In 2005, Intel developed the IA-32 Execution Layer (IA-32 EL), a software emulator that provides better performance. With Montecito, Intel therefore eliminated hardware support for IA-32 code».</small>  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Instalación de 'Oracle VirtualBox' 6.1 64bits en Debian 10 y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_virtualbox_debian10_y_derivados/)
- [[HowTo] Aumentar el tamaño del disco duro virtual de una máquina virtual de Virtual Box](https://hijosdeinit.gitlab.io/howto_aumentar_tama%C3%B1o_discodurovirtualdinamico_maquinavirtual_virtualbox/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://pandorafms.com/blog/es/virtualizacion/">PandoraFms - qué es la virtualización</a>  
<a href="https://www.atareao.es/tutorial/podman/">atareao.es - tutorial podman</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
