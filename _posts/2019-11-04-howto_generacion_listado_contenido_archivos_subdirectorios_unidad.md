---
layout: post
title: "[HowTo] Generación de un listado de todos los ficheros y directorios de un dispositivo de almacenamiento"
date: 2019-11-04
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "productividad", "archivos", "ficheros", "directorios", "subdirectorios", "texto plano", "texto", "listado", "busqueda", "contenido", "find", "tree", "indice", "indexar", "indexacion"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Muchas veces nos interesa tener un listado de todos los archivos, carpetas y subcarpetas de un dispositivo de almacenamiento, y poderlo consultar sin necesidad de tener que insertar dicho dispositivo, trasladarnos al lugar en que está o conectarnos por red.</small>  
<br>
Se puede hacer seguro de más formas, mas estas dos -mediante el comando <span style="background-color:#042206"><span style="color:lime">'find'</span></span> y mediante el comando <span style="background-color:#042206"><span style="color:lime">'tree'</span></span>- son las que conozco:  

<br>
## 1. listado de todo el contenido mediante 'FIND'
Desde el directorio a partir del cual queremos saber todo lo que contiene, ejecutar en la terminal:
~~~bash
sudo find . -type f > /ruta/listadoarchivossubdirectorios.txt
~~~
<br>
(*) Si quisiéramos que además muestre la **suma de verificación** sha256 de cada uno de los archivos y/o carpetas:
~~~bash
sudo find . -type f -exec sha256sum "{}" + > /ruta/listadoconsha256archivossubdirectorios.txt
~~~

<br>
## 2. listado de todo el contenido mediante 'TREE'
Desde el directorio a partir del cual queremos saber todo lo que contiene, ejecutar en la terminal:
~~~bash
sudo tree > /ruta/listadoarchivossubdirectorios.txt
~~~
<br>
Si no tuviéramos instalado el comando <span style="background-color:#042206"><span style="color:lime">'tree'</span></span> bastará con, previamente, instalarlo. Así:
~~~bash
sudo apt install tree
~~~

<br>
<br>
<br>
**(!)** El inconveniente de estos dos métodos es que **no listan el contenido de los archivos comprimidos**, que también contienen cosas que podrían interesarnos. Sobre este asunto, seguro, indagaré en lo sucesivo.  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
