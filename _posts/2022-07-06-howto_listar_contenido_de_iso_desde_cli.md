---
layout: post
title: "[HowTo] Listar el contenido de un archivo '.iso' desde la línea de comandos de GNU Linux"
date: 2022-07-06
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "iso", "imagen", "mount", "p7zip", "p7zip-full", "CLI", "linea de comandos", "terminal", "gnu linux", "linux", "tty", "contenidos", "listar", "ls", "dir"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## Intro / contexto
(pueden saltársela)  
<small>En internet no todo es perenne. Más cosas de las que imaginaba son caducas, desaparecen. Es posible, por ejemplo, que uno piense que cualquier versión de cualquier distribución GNU Linux estará siempre disponible en la red. Pese al trabajo de 'archive.org' y de 'distrowatch', esto no es así: las páginas de los desarrolladores desaparecen, y también los torrent, mirros de descarga, etcétera...</small>  
<small>Andaba yo inventariando las 'iso' que tenía almacenadas desde hace años, para lo cual conocer la fecha en que cada una de sus diversas versiones fueron desarrolladas me parece importante hacer constar. Me percaté entonces de dos obviedades: a) la fecha en que las descargué, en la mayoría de los casos, no se corresponde con la fecha en que se desarrollaron, y b) no siempre existe información precisa sobre las diversas distribuciones GNU Linux que en el mundo ha habido.</small>  
<small>¿Cómo averiguar la fecha en que se desarrolló una versión concreta de una Distribución GNU Linux de la que en la red no existe información lo suficientemente precisa o concreta? Mirando en el interior de su correspondiente archivo '.iso', fijándose en las fechas de los archivos que ésta contiene (o, incluso, leyendo sus 'readme' y 'changelogs').</small>  

<br>
<br>
Para listar / consultar los contenidos de un archivo '.iso' desde la línea de comandos, sin necesidad de quemarlo en un soporte de almacenamiento (cd, dvd, pendrive usb, etc) se me ocurren **2 opciones**:
- a) utilizar el compresor 'p7zip' (7-zip) para listar ô extraer los archivos que contiene la 'iso'
- b) montar la 'iso'  

<br>
<br>
## Listar ô extraer los archivos que contiene la 'iso' mediante el compresor 'p7zip'
<small>Si 'p7zip' no estuviera instalado, se puede incorporar al sistema de manera sencilla. En Debian y derivados bastaría con ejecutar desde la línea de comandos: <span style="background-color:#042206"><span style="color:lime">`sudo apt-get update && \sudo apt install p7zip p7zip-full`</span></span></small>  
Listar:
~~~bash
7z l archivo.iso
~~~
Extraer:
~~~bash
7z x archivo.iso
~~~

<br>
<br>
## Montar el archivo '.iso' para revisar los archivos que contiene
Créese, si se desea, el directorio donde se montará el archivo 'iso'
~~~bash
sudo mkdir /mnt/puntomontaje
~~~
Móntese el archivo '.iso' en dicho directorio
~~~bash
sudo mount -o loop -t iso9660 archivo.iso /mnt/puntomontaje
~~~
Para revisar los contenidos bastará con, por ejemplo:
~~~bash
ls -lisah /mnt/puntomontaje
~~~
<br>
Cuando se desee se puede desmontar el archivo '.iso' ejecutando
~~~bash
sudo umount /mnt/puntomontaje
~~~

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Montar webdav desde línea de comandos en cliente GNU Linux](https://hijosdeinit.gitlab.io/howto_montar-webdav_desde_cli/)
- [[HowTo] Una forma ortodoxa de crear '.iso' a partir de un DVD o CD mediante 'dd'](https://hijosdeinit.gitlab.io/howto_forma_ortodoxa_realizar_iso_apartir_dvd_mediante_dd/)
- [[HowTo] Respaldar juegos cd / dvd de PlayStation 2 (PS2) creando '.iso'](https://hijosdeinit.gitlab.io/howto_crear_iso_cd_dvd_playstation2_ps2/)
- [[HowTo] Acetone ISO en GNU Linux Ubuntu](https://hijosdeinit.gitlab.io/howto_acetoneiso_linux_ubuntu/)  

<br>
<br>
<br>
---  
<small>Fuentes:  
[LinuxHispano](https://www.linuxhispano.net/2016/02/01/extraer-descomprimir-imagen-iso-desde-terminal/)  
[UbunLog](https://ubunlog.com/montar-imagenes-iso-ubuntu/#Usar_la_linea_de_comandos_para_montar_imagenes_ISO)</small>  

<br>
<br>
<br>
<br>
