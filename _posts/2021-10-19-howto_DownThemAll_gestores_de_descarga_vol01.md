---
layout: post
title: "[HowTo] Gestores de descarga, vol.01: 'DownThemAll'"
date: 2021-10-19
author: Termita
category: "descargas"
tags: ["descargas", "internet 4.0", "sistemas operativos", "gnu linux", "linux", "Windows", "macos", "firefox", "chrome", "chromium", "opera", "basilisk", "pale moon", "borealis", "librewolf", "waterfox", "librefox", "web browser", "navegador web", "productividad", "bulk", "masivo", "en masa", "downthemall", "downwemall", "xpi", "extensiones", "plugins", "getemall"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>«Un gestor (o administrador o acelerador) de descargas es un programa diseñado para descargar archivos de Internet, ayudado de distintos medios como algoritmos, para ir pausando y reanudando las descargas de algún servidor FTP o página de Internet. Es muy recomendable su utilización cuando se trata de archivos grandes, tales como imágenes ISO, programas, vídeos, música, por mencionar algunos.  
A diferencia de un navegador (que permite navegar por las páginas de la web, siendo la función de descarga de menos importancia), un gestor de descargas permite automatizar la descarga de varios archivos de acuerdo a parámetros de configuración. En particular, un gestor de descargas se puede utilizar para hacer una réplica de un sitio web completo.»</small>  

<br>
Existen varios gestores de descarga interesantes, como son ['aria2'](https://aria2.github.io/), ['uGet'](https://ugetdm.com/), [JDownloader](https://jdownloader.org/), 'DownThemAll' o 'Get'emAll'. Tarde o temprano este blog irá tratando acerca de cada uno de ellos. Se inicia, por tanto, la saga con DownThemAll.  

<br>
[DownThemAll](https://www.downthemall.org/) fue durante muchos años mi gestor de descarga favorito; al ser una extensión para navegadores web como Mozilla Firefox y ser éstos multiplataforma, lo utilizaba tanto en GNU Linux como en MS Windows.  
En los últimos años dejé de utilizar DownThemAll. No recuerdo bien la causa; creo recordar que durante una temporada fue discontinuado... La cuestión es que hace un tiempo ví que DownThemAll seguía vivo, así que he aquí una mención a este fabuloso gestor de descargas.  

<br>
Los desarrolladores de 'DownThemAll' lo describen como «una poderosa y fácil de usar extensión que añade nuevas capacidades de descarga al navegador web. DownThemAll permite descargar todos los contenidos -enlaces, imagenes, etc...- de un sitio web.  
Además pueden aplicarse filtros personalizados para refinar las descargas y descargar así únicamente lo que se desea.  
Permite poner en cola, pausar y reemprender descargas en cualquier momento, y está totalmente integrado en el navegador web».  

<br>
Existe una modificación de 'DownThemAll' -para los navegadores ['Borealis'](https://binaryoutcast.com/projects/borealis/), ['Pale Moon'](https://www.palemoon.org/) y/o sus derivados como ['Basilisk'](https://hijosdeinit.gitlab.io/howto_basilisk_ubuntu_18_bionic/)- llamada [**'Get'emAll'**](https://deusrex.neocities.org/xul/getemall/getemall.html), de la que trata [esta entrada de este blog]().  

<br>
<a href="/assets/img/2021-10-19-howto_DownThemAll_gestores_de_descarga_vol01/donwthemall.png" target="_blank"><img src="/assets/img/2021-10-19-howto_DownThemAll_gestores_de_descarga_vol01/donwthemall.png" alt="descargando en masa mediante 'DownThemAll'" width="700"/></a>  
<small>descargando en masa mediante 'DownThemAll'</small>  
<br>

<br>
## ¿Cómo "instalar" 'DownThemAll'?
Accédase desde el navegador web a la versión apropiada de DownThemAll; dependiendo de si el navegador es Firefox, Chrome (o derivados) u Opera:
- [extensión para Mozilla Firefox](https://addons.mozilla.org/firefox/addon/downthemall/)
- [extensión para Opera](https://addons.opera.com/en/extensions/details/downthemall/)
- [extensión para Google Chrome (y derivados)](https://chrome.google.com/webstore/detail/downthemall/nljkibfhlpcnanjgbnlnbjecgicbjkge)  

Instálese, pulsando en el "botón" correspondiente, la extensión de 'DownThemAll'.  

<br>
<br>
## ¿Cómo utilizar el gestor de descargas 'DownThemAll'?
'DownThemAll' permite realizar descargas sencillas (de un sólo archivo) pero también -y eso es lo que lo hace interesante- descargas en masa.  

<br>
### a. Descargar un sólo archivo (descarga simple)
Click derecho sobre un enlace, sobre una imagen, etc... > DownThemAll! > 'Añadir descarga'  

<br>
### b. Descarga masiva de archivos de una página web
Click derecho en cualquier espacio "libre" de la página web > DownThemAll! > 'DownThemAll!'  
Se abrirá la ventana del gestor de descargas 'DownThemAll'. En ésta pueden establecerse filtros para que descargue de forma masiva sólo los tipos de archivo que nos interesen.  

<br>
<a href="/assets/img/2021-10-19-howto_DownThemAll_gestores_de_descarga_vol01/downthemall_aplicando_filtros_masivos.png" target="_blank"><img src="/assets/img/2021-10-19-howto_DownThemAll_gestores_de_descarga_vol01/downthemall_aplicando_filtros_masivos.png" alt="Aplicando filtros para descargar en masa mediante 'DownThemAll'" width="700"/></a>  
<small>Aplicando filtros para descargar en masa sólo .mp3 mediante 'DownThemAll'</small>  
<br>

Una vez hecho esto, basta con pulsar en "Descargar" y en la ventana del gestor de descargas 'DownThemAll' aparecera la cola de trabajos realizándose.  

<br>
<a href="/assets/img/2021-10-19-howto_DownThemAll_gestores_de_descarga_vol01/gestor_donwthemall_masivo.png" target="_blank"><img src="/assets/img/2021-10-19-howto_DownThemAll_gestores_de_descarga_vol01/gestor_donwthemall_masivo.png" alt="El Gestor de descargas 'DownThemAll' descargando en masa sólo los archivos .mp3 de una página" width="500"/></a>  
<small>El Gestor de descargas 'DownThemAll' descargando en masa  
sólo los archivos .mp3 (*podcasts*) de la web ['msdos.club'](https://msdos.club/)</small>  
<br>

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Gestores de descarga, vol.02: 'Get'emAll'](https://hijosdeinit.gitlab.io/howto_getemall_gestores_de_descarga_vol02/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/Gestor_de_descargas">Wikipedia - gestor de descargas</a>  
<a href="https://es.wikipedia.org/wiki/Anexo:Comparaci%C3%B3n_de_gestores_de_descargas">wikipedia - comparativa de gestores de descarga</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
