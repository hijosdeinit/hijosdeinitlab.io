---
layout: post
title: "[HowTo] Cifrar sin interrupciones mediante gpg desde línea de comandos (CLI)"
date: 2020-08-23
author: Termita
category: "cifrado"
tags: ["sistemas operativos", "linux", "gnu linux", "cifrar", "seguridad", "gpg", "pgp", "kleopatra", "cli", "terminal", "linea de comandos", "productividad", "lotes", "nohup", "bash"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Cuando encriptamos mediante gpg desde línea de comandos y no deseamos interrupciones requiriéndonos si confiamos en el certificado con el que vamos a cifrar cada archivo bastará con añadir el parámetro **`--always-trust`**  
<br>
> <small>`gpg --verbose --always-trust archivo_01.ext.gpg --encrypt --recipient nombredelaclavedecifrado archivo_01.ext`</small>  

<br>
Si se desea se puede cifrar ficheros en masa, sin interrupciones:  
<br>
> <small>`gpg --verbose --always-trust archivo_01.ext.gpg --encrypt --recipient nombredelaclavedecifrado archivo_01.ext && \gpg --verbose --always-trust archivo_02.ext.gpg --encrypt --recipient nombredelaclavedecifrado archivo_02.ext`</small>  

<br>
Por ejemplo, esto quedaría así:  
<br>
> <small>`gpg --verbose --always-trust anotaciones_casa.md.gpg --encrypt --recipient piedra@cluster.eu anotaciones_casa.md && \gpg --verbose --always-trust lista_compra.md.gpg --encrypt --recipient piedra@cluster.eu lista_compra.md`</small>  

<br>
<br>
El "añadido" **` && \`** sirve para "encadenar" órdenes, una tras otra, secuencialmente. Y éstas pueden ser, no sólo cifrar, sino también borrar archivos ya cifrados, calcular sumas de verificación, etcétera...  
<br>
<br>
Si estuviéramos trabajando remotamente y deseáramos que el trabajo no se interrumpiera cuando, dejando la máquina remota encendida, desearamos cerrar la shell remota bastaría, por ejemplo, con crear un script de bash con los comandos y ejecutarlo mediante 'nohup' y su nomenclatura. Así:  
<br>
> <small>`nohup sh script.sh &`</small>  

<br>
<br>
<br>
<br>
<br>
