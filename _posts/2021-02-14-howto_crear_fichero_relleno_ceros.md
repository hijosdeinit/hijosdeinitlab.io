---
layout: post
title: "[HowTo] Crear fichero de relleno a base de ceros"
date: 2021-02-14
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "ceros", "zero", "dd"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## Procedimiento
~~~bash
sudo dd if=/dev/zero of=/archivo_relleno_ceros_borrable.foo bs=1024 count=1000k
~~~
... donde:  

<br>
<span style="color:lime">`if=/dev/zero`</span> <small>establece como *input* a '/dev/zero', un archivo especial en sistemas operativos tipo Unix que provee tantos caracteres null (ASCII NUL, 0x00; no el carácter ASCII "0", 0x30) como se lean desde él. Uno de los usos típicos es proveer un flujo de caracteres para sobreescribir información. Otro uso puede ser para generar un archivo "limpio" de un determinado tamaño. Usando mmap para mapear /dev/zero al espacio de memoria virtual, es la manera en que BSD implementa memoria compartida.</small>  

<br>
<span style="color:lime">`of=/archivo_relleno_ceros_borrable.foo`</span> <small>es el nombre del *output*, el archivo relleno de ceros resultante.</small>  

<br>
<span style="color:lime">`bs=1024`</span> <small>es el "tamaño de bloque" o *block size*. Desconozco de qué va exactamente eso, aunque intuyo que tiene que ver con el tamaño de los sectores de los discos o con optimizar el trabajo del comando 'dd'. Es probable que pueda prescindirse de ese parámetro. El comando `isoinfo -d -i /dev/sr0` puede indicar el tamaño de bloque apropiado.</small>  

<br>
<span style="color:lime">`count=100k`</span> <small>es el parámetro que indica el tamaño que se desea tenga el archivo resultante, en este caso 1000 Mb.</small>  

<br>
## ¿Para qué sirve un 'archivo de ceros'?
Por ejemplo, para sobreescribir todo aquel material borrado que permanece aún -aunque oculto- en un disco, o para [facilitar / potenciar la compresión de una 'imagen de disco' y, por consiguiente, aligerarla notablemente](https://hijosdeinit.gitlab.io/howto_aligerar_respaldos_dd/).  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Aligerar Respaldos Hechos Con 'Dd'](https://hijosdeinit.gitlab.io/howto_aligerar_respaldos_dd/)
- []()
- []()
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
