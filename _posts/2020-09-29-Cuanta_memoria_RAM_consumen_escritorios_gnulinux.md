---
layout: post
title: '¿Cuánta memoria RAM consumen los escritorios de GNU Linux'
date: 2020-09-29
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "memoria", "RAM", "ligero", "escritorio", "gestor de ventanas", "window manager", "wm", "tema visual", "consumo", "tinywm", "9wm", "dwm", "ratpoison", "olwm", "twm", "xmonad", "jwm", "i3", "blackbox", "sawfish", "icewm", "pekwm", "openbox", "window maker", "awesome", "fwm", "fluxbox", "mutter", "e17", "lxde", "kwin", "mate", "trinity", "xfce", "cinnamon", "razor-qt", "gnome3", "unity", "kde"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
¿Cuánto pesan en memoria RAM los diferentes escritorios de GNU LInux?  
He aquí una comparativa de los siguientes:  
tinywm, 9WM, dwm, ratpoison, olwm, twm, xmonad, jwm, i3, blackbox, sawfish, icewm, pekwm, openbox, window maker, awesome, fwm, fluxbox, mutter, e17, lxde, kwin, mate, trinity, xfce, cinnamon, razor-qt, gnome3, unity, kde  

<br>

<a href="/assets/img/2020-09-29-Cuanta_memoria_RAM_consumen_escritorios_gnulinux/memoria-RAM-escritorios-linux.png" target="_blank"><img src="/assets/img/2020-09-29-Cuanta_memoria_RAM_consumen_escritorios_gnulinux/memoria-RAM-escritorios-linux.png" width="800"/></a>  
<small>memoria RAM que suelen consumir los diferentes escritorios para GNU Linux</small>  

<br>
<br>
<br>
<br>
<br>
