---
layout: post
title: '[HowTo] BuckleSpring (y otros): incorporación de sonido a las pulsaciones del teclado en GNU Linux'
date: 2020-06-06
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "sonido", "typewriter", "cherry", "teclado", "IBM", "keyboard", "teclas", "pulsación", "editor de texto", "buckle", "bucklespring", "typewriter-sounds", "keypress", "tickeys", "focus writer", "writemonkey"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
![teclado mecánico]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-06-04-[HowTo]_BuckleSpring_sonido_pulsaciones_teclado/model-m_teclado.png)  

<br>
En el fondo es una tontería prescindible... mas a mí siempre me agradó el sonido de las teclas de los teclados de antes. Con la llegada de los teclados modernos se extendió la "era del silencio" y aquellos entrañables, sólidos y ruidosos teclados mecánicos quedaron relegados. Con qué inconsciencia a finales de los 90 tiré a la basura aquellos teclados, víctimas de la modernidad.  
Echo de menos los teclados Cherry, aquellos teclados de IBM... su sonido y la experiencia de pulsar aquellas teclas.  

<br>
![tecla mecánica]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-06-04-[HowTo]_BuckleSpring_sonido_pulsaciones_teclado/buckle.gif)

<br>
Esto se puede justificar, ya no sólo en lo subjetivo, sino también en lo práctico.  
Es curioso lo que acontece demasiadas veces cuando uno ha de introducir una contraseña: fallo, suele colarse algún caracter por error... porque uno no vé lo que escribe... uno escribe a ciegas, sin referencias.  
Una referencia es lo visual (ver lo que se escribe); otra referencia es el sonido característico de la tecla al ser pulsada. Pulsar una tecla "extra" por error en un teclado silencioso suele pasar inadvertido.  

Por todas estas razones he hecho que mi sistema emita un sonido cada vez que pulso una tecla.  
Los inconvenientes son que se produce un consumo extra -irrisorio, eso sí- de recursos, y que si los altavoces están apagados es invento resulta inútil.  

<br>
Para incorporar sonido a cada una de las pulsaciones del teclado en GNU Linux tenemos 2 opciones:  
- A. Instalación de un paquete que proporcione esa prestación a todo el sistema:  
-- A.1. BuckleSpring  
-- A.2. TypeWriter-Sounds (python)  
-- A.3. Linux TypeWriter (keypress) (python)  
-- A.4. Chino tickeys  
- B. Instalar y utilizar un editor de texto que tenga esa opción o característica (sólo sonarán las teclas en ese editor no en el sistema completo). Editores como [FocusWriter](https://gottcode.org/focuswriter/) o WriteMonkey (wine) permiten activar el sonido del teclado.  

<br>
<br>
## ♠ BuckleSpring
[Nostalgia BuckleSpring Keyboard Sound](https://github.com/zevv/bucklespring)  
ℹ En sistemas cuyos escritorios utilizan [*'Wayland'*](https://wayland.freedesktop.org/) 'bucklespring' no tiene toda su funcionalidad: por ejemplo, es frecuente que las pulsaciones al trabajar en un emulador de terminal ('gnome-terminal', 'lxterminal', etc...) no emitan ningún sonido.  
Cuando, por ejemplo, 'Gnome' funciona bajo las 'X' y no bajo Wayland no hay problemas / disfunciones de ese tipo.  

<br>
**Instalación**:
~~~bash
sudo apt-get install libopenal-dev libalure-dev libxtst-dev
sudo apt install bucklespring
~~~

**Ejecución**:
~~~bash
buckle &
~~~
**Terminación**:
Si se ejecutó añadiendo **'&'** al final hay que buscar y matar el proceso.  
Si se ejecutó sin añadir '&' al final bastará con pulsar ctrl+c o bien cerrar la terminal donde se ejecutó.  

<br>
Bucklespring emula el sonido de un IBM Model-M. Se ejecuta en segundo plano y reproduce el sonido de cada tecla cuando es pulsada y liberada en el teclado. El sonido de las teclas ha sido cuidadosamente registrado y se reproduce simulando la distancia y dirección exactas.  
Para deshabilitar temporalmente bucklespring -por ejemplo, para introducir contraseñas- basta con pulsar 2 veces "ScrollBlock". Para volverlo a habilitar se hace lo mismo.  

<br>
Los sonidos se alojan en '/usr/share/buckle/wav/' y pueden ser cambiados, sustituídos... añadidos. Esto último es especialmente útil porque, en mi caso, los sonidos correspondientes al teclado numérico no existen y habría que añadírselos.  
>[Aquí (FreeSound.org)](https://freesound.org/search/?q=mg+42) hay más sonidos de teclado.  
[Aquí también hay sonidos de teclado](https://github.com/netpipe/videoDriverUpdater/tree/master/OtherScripts/keysounds)  
[Y aquí (Soundjay)](https://www.soundjay.com/)  
[Y aquí (SoundBible)](https://soundbible.com)  

<br>
![listado de sonidos de BuckleSpring (carpeta '/usr/share/buckle/wav/']({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-06-04-[HowTo]_BuckleSpring_sonido_pulsaciones_teclado/bucklespring_ficheros_audio.png)  
<small>archivos de audio de cada una de las teclas ('/usr/share/buckle/wav/')</small>  

<br>
BuckleSpring, si se desea, puede ser <big>**COMPILADO**</big> a partir de su código fuente (`make`), aunque está disponible (sin necesidad de compilar) en los repositorios de Ubuntu, Debian, ArchLinux y Fedora. Se rumorea que BuckleSpring también funciona en MacOs y en Windows.  
Los detalles del procedimiento de instalación y/o compilación en todos estos sistemas operativos están [en el 'readme' del repositorio de BuckleSpring en Gitlab](https://github.com/zevv/bucklespring) y también en [**ESTA ENTRADA DE ESTE BLOG, detallada y específica para Debian y Derivados**](https://hijosdeinit.gitlab.io/howto_compilar_bucklespring/).  

<br>
<br>
## ♠ Otros métodos que incorporan sonido a las teclas en todo el sistema
#### typewriter-sounds
[Typewriter-sounds](https://github.com/aizquier/typewriter-sounds) es un programa que reproduce los sonidos de una antigua máquina de escribir.  
El código está inspirado en la demo de *keylogger* que viene en [*The Python X Library*](https://github.com/python-xlib/python-xlib). La lógica es simple: captura el *keycode* de toda tecla pulsada y, en lugar de guardarlo (como haría un keylogger) sólo reproduce un sonido. Dado que la detección de teclas está realizada utilizando *Xlib*, este programa funcionará en aquellas plataformas que soportan X11.  
Los sonidos provienen de [FreeSound](https://www.freesound.org/), aunque algunos fueron modificados ex profeso para este proyecto.  
Requiere:
- Python 2.7 (aunque debería funcionar también con la versión 3.5)
- X11 and Xlib bindings for Python
- PyGame (for sound)
- [aplay command line player](https://linux.die.net/man/1/aplay) (casi todas las distribuciones GNU Linux lo traen de serie)  

**Instalación**:
~~~bash
sudo apt-get update
sudo apt install python-pygame
sudo apt install python-xlib
sudo apt install git
git clone https://github.com/aizquier/typewriter-sounds.git
~~~
**Ejecución**, desde el directorio donde clonamos el repositorio:
~~~
python typewriter_sounds.py &
~~~
**‼** BUG: Si no ejecutamos el script de python 'typewriter_sounds.py' desde el directorio donde clonamos el repositorio, es decir, si lo ejecutamos desde otro directorio proporcionando la ruta completa y correcta **el sonido estará distorsionado**. Desconozco a qué se debe y si es cosa de mi sistema.  
**ℹ** <small>La adición de **'&'** permite cerrar la terminal sin matar el programa. Para **terminarlo**, cuando proceda, bastaría con buscar el proceso y matarlo. Si no se añade '&' el programa se termina, bien pulsando Ctrl+C o bien cerrando la terminal donde se lanzó.</small>  

<br>
#### keypress
No lo he probado

<br>
#### Tickeys
No me inspira confianza. No hay que perder de vista que, como bien señalan los desarrolladores de 'typewriter-sounds', estos programas son muy similares a un keylogger. Tickeys pareciera que es de origen chino.  
**‼** No logré hacerlo funcionar; error en la instalación.

<br>
<br>
## ♠ Editores con sonido de teclas incorporado

- [FocusWriter](https://gottcode.org/focuswriter/)
>~~~
sudo apt install focuswriter
~~~

- []



- [WriteMonkey (Windows)](https://writemonkey.com/)

- [Q10 (Windows)](http://www.baara.com/q10/)

- [Dark Room (Windows)](http://jjafuller.com/dark-room)

- [CodeRoom (Windows)](https://code.google.com/archive/p/coderoom/)

- []

- []

- []

- []

- []






## ℹ Anexo: Otros editores interesantes (muchos de ellos muy conocidos), sin sonido de teclas

- [PyRoom](https://pyroom.org/)
>~~~
sudo apt install pyroom
~~~

- [GhostWriter](https://wereturtle.github.io/ghostwriter/)
>~~~
sudo apt install ghostwriter
~~~

- [Typora](https://typora.io/)
Excelente editor de markdown.
>~~~
wget -qO - https://typora.io/linux/public-key.asc | sudo apt-key add -
sudo add-apt-repository 'deb https://typora.io/linux ./'
sudo apt-get update
sudo apt install typora
~~~

<br>
<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[FreedomPenguin](https://web.archive.org/web/20191206223025/https://freedompenguin.com/articles/how-to/sound-effects-linux-application-launchers/)  
[Herramientas para escritores](https://blog.desdelinux.net/herramientas-para-escritores-y-guionistas/)</small>  

<br>
<br>
<br>
<br>
