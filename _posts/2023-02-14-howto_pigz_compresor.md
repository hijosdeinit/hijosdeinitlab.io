---
layout: post
title: "[HowTo] 'pigz', un compresor multiprocesador / multinúcleo"
date: 2023-02-14
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "GNU linux", "linux", "Debian", "pigz", "gzip", "tar", "tar.gz", ".gz", "7zip", "p7zip", "rar", "arj", "ace", "zip", "compresion", "multinucleo", "multiprocesador", "multiprocessor", "multicore", "zlib", "pthread", "apt", "cli", "linea de comandos", "terminal", "instalacion", "nextcloud"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
['pigz'](https://zlib.net/pigz/) -acrónimo de *Parallel Implementation of GZip*, pronunciado *“pig-zee”*- es una mejora del compresor ['gzip'](https://www.gnu.org/software/gzip/) que permite aprovechar las ventajas multiprocesador y multinúcleo de las máquinas modernas para conseguir compresiones más rápidas.  
Escrito por el ingeniero de software [Mark Adler](https://en.wikipedia.org/wiki/Mark_Adler), utiliza las librerías ['zlib'](http://zlib.net/) y ['pthread'](http://en.wikipedia.org/wiki/POSIX_Threads).
<br>
Supe de su existencia cuando, mientras hacía un respaldo de una instancia 'NextCloud' y sus datos, me percaté de que 'htop' mostraba un proceso llamado 'pigz'. Una búsqueda en 'searx.org' me aclaró qué era "aquello".  

<br>
<br>
## Instalación de 'pigz' en Debian y derivados
~~~bash
sudo apt-get update
sudo apt install pigz
~~~

<br>
<br>
## Utilización de 'pigz'
Extraído de `pigz --help`:
~~~
Usage: pigz [options] [files ...]
  will compress files in place, adding the suffix '.gz'. If no files are
  specified, stdin will be compressed to stdout. pigz does what gzip does,
  but spreads the work over multiple processors and cores when compressing.

Options:
  -0 to -9, -11        Compression level (level 11, zopfli, is much slower)
  --fast, --best       Compression levels 1 and 9 respectively
  -b, --blocksize mmm  Set compression block size to mmmK (default 128K)
  -c, --stdout         Write all processed output to stdout (won't delete)
  -d, --decompress     Decompress the compressed input
  -f, --force          Force overwrite, compress .gz, links, and to terminal
  -F  --first          Do iterations first, before block split for -11
  -h, --help           Display a help screen and quit
  -i, --independent    Compress blocks independently for damage recovery
  -I, --iterations n   Number of iterations for -11 optimization
  -J, --maxsplits n    Maximum number of split blocks for -11
  -k, --keep           Do not delete original file after processing
  -K, --zip            Compress to PKWare zip (.zip) single entry format
  -l, --list           List the contents of the compressed input
  -L, --license        Display the pigz license and quit
  -m, --no-time        Do not store or restore mod time
  -M, --time           Store or restore mod time
  -n, --no-name        Do not store or restore file name or mod time
  -N, --name           Store or restore file name and mod time
  -O  --oneblock       Do not split into smaller blocks for -11
  -p, --processes n    Allow up to n compression threads (default is the
                       number of online processors, or 8 if unknown)
  -q, --quiet          Print no messages, even on error
  -r, --recursive      Process the contents of all subdirectories
  -R, --rsyncable      Input-determined block locations for rsync
  -S, --suffix .sss    Use suffix .sss instead of .gz (for compression)
  -t, --test           Test the integrity of the compressed input
  -v, --verbose        Provide more verbose output
  -V  --version        Show the version of pigz
  -Y  --synchronous    Force output file write to permanent storage
  -z, --zlib           Compress to zlib (.zz) instead of gzip format
  --                   All arguments after "--" are treated as files
  ~~~

<br>
Ejemplos de uso (TL;DR):
~~~bash
pigz -k nombredearchivoacomprimir
~~~
<small>El parámetro `-k` indica que se desea conservar el archivo original -es decir, el archivo que comprimiremos- tras finalizar la compresión</small>.  

<br>
Comprimir un archivo utilizando el mejor método de compresión:
~~~bash
pigz -k -9 archivoacomprimir
~~~

Comprimir un archivo sin efectuar compresión utilizando 4 procesadores:
~~~bash
pigz -0 -p4 archivoacomprimir
~~~

Comprimir un directorio utilizando 'tar':
~~~bash
tar cf - ruta/al/directorio | pigz > archivocomprimido.tar.gz
~~~

Descomprimir un archivo:
~~~bash
pigz -d archive.gz
~~~

Listar los contenidos de un archivo:
~~~bash
pigz -l archive.tar.gz
~~~


<br>
<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[TecMint](https://www.tecmint.com/compress-files-faster-in-linux/)  
[manual oficial de 'pigz'](https://zlib.net/pigz/pigz.pdf)  
[mankier - 'pigz'](https://www.mankier.com/1/pigz)</small>  

<br>
<br>
<br>
<br>
