---
layout: post
title: "[HowTo] 'Ryzen Controller' en Debian y derivados"
date: 2021-11-10
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "Debian", "ryzen controller", "amd ryzen", "ryzen", "microcode", "cpu", "gpu", "procesador", "intel", "amd", "ati", "nvidia", "drivers", "controladores", "firmware", "synaptic", "apt", "temperatura", "frecuencia", "bus", "codigo abierto", "open source"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Los desarrolladores de la herramienta ['Ryzen Controller](https://ryzencontroller.com) la definen así:  
«Debido a los muchos diseños diferentes (y propietarios) de las placas base de computadoras portátiles, no existe una herramienta oficial para ajustar el rendimiento de los procesadores móviles 'AMD Ryzen'. Debido a esto, muchos de estos chips se ven frenados por los límites térmicos y de potencia de los fabricantes.  
'Ryzen Controller' es una **utilidad de código abierto compatible con la mayoría de procesadores AMD Ryzen 2XXX, 3XXX, 4XXX y 5XXX que libera el poder de las computadoras portátiles 'AMD Ryzen' con una GUI fácil de usar. Los ajustes se pueden guardar como ajustes preestablecidos personalizados que se pueden habilitar siempre que necesite energía adicional, o deshabilitar cuando desee mantener las cosas frescas**.»  

<br>
[repositorio de 'Ryzen Controller' en 'GitLab'](https://gitlab.com/ryzen-controller-team/ryzen-controller)  

<br>
<br>
https://youtu.be/ANG0fa_6pUA  
https://yewtu.be/watch?v=ANG0fa_6pUA  
yoyo salmorejo geek  




El 'microcode' es un firmware que controla la forma de funcionar de los procesadores -CPU (~~¿y GPU?~~)-. Es importante incorporar el último 'microcode' al sistema operativo GNU Linux en pro de una mayor estabilidad y seguridad del sistema.  
Instalando el correspondiente 'microcode' del procesador (~~y ¿la tarjeta gráfica?~~) uno se asegura de recibir sus actualizaciones.  

<br>
## ¿Cómo incorporar el 'microcode' del procesador (CPU) a un sistema operativo GNU Linux Debian (o derivado)
Mediante el gestor de paquetes <big>**'Synaptic'**<big>, por ejemplo:  

1. Botón de actualizar repositorios  
(desde la terminal ejecutar `sudo apt-get update` también serviría)  

2. búsquese en el nombre y la descripción de los paquetes la cadena de caracteres 'microcode'.  

3. Selecciónese e Instálese -entre los resultados de la búsqueda- el paquete correspondiente a la plataforma -Intel o AMD- de nuestro procesador.  

<br>
<br>

Entradas relacionadas:  
microcode
firmwares
- []()  

<br>
<br>
<br>

--- --- ---  
<small>Fuentes:  
<a href="https://averagelinuxuser.com/debian-10-after-install/">AverageLinuxUser</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
