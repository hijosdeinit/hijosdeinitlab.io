---
layout: post
title: "[HowTo] 'git': 'progit2', el mejor manual probablemente"
date: 2022-02-17
author: Termita
category: "programación"
tags: ["git", "blog", "web 4.0", "Jekyll", "Ruby", GitLab", "GitHub", "programación", "markdown", "texto plano", "html", "estatico", "Hugo", "asciidoc"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
['git'] es «un software de control de versiones diseñado por Linus Torvalds, pensando en la eficiencia, la confiabilidad y compatibilidad del mantenimiento de versiones de aplicaciones cuando éstas tienen un gran número de archivos de código fuente. Su propósito es llevar registro de los cambios en archivos de computadora incluyendo coordinar el trabajo que varias personas realizan sobre archivos compartidos en un repositorio de código.
Al principio, 'git' se pensó como un motor de bajo nivel sobre el cual otros pudieran escribir la interfaz de usuario o front end como 'Cogito' o 'StGIT'. Sin embargo, 'git' se ha convertido desde entonces en un sistema de control de versiones con funcionalidad plena. Hay algunos proyectos de mucha relevancia que ya usan 'git', en particular, el grupo de programación del núcleo Linux.  
'git' es un software libre distribuible bajo los términos de la versión 2 de la Licencia Pública General de GNU».  

<br>
<br>
Pues bien, junto con `man git`, he aquí el que es probablemente el mejor manual de 'git': ['progit2'](https://github.com/progit/progit2).  
Puede descargarse en [.pdf](https://github.com/progit/progit2/releases/download/2.1.337/progit.pdf) / [.epub](https://github.com/progit/progit2/releases/download/2.1.337/progit.epub) o consultarse [online vía web](https://git-scm.com/book/en/v2).  

<br>
<br>
<br>
Entradas relacionadas:
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/Git" target="_blank">Wikipedia - 'git'</a>  
<a href="https://elblogdelazaro.gitlab.io/posts/2017-09-23-jekyll-y-gitlab/" target="_blank">El blog de Lázaro</a>  
<a href="https://chenhuijing.com/blog/hosting-static-site-gitlab-pages/" target="_blank">chenhuijing.com</a>  
<a href="https://docs.gitlab.com/ee/gitlab-basics/start-using-git.html" target="_blank">gitlab.com - start using git</a>  
<a href="https://gitlab.com/jekyll-themes/default-bundler" target="_blank">gitlab.com - construir Jekyll con Bundler</a>  
<a href="https://colaboratorio.net/ondiz/developer/2017/como-personalizar-nuestro-sitio-jekyll-con-gemas/" target="_blank">colaboratorio.net - Personalizar Jekyll con gemas</a>  
<a href="http://www.independent-software.com/building-and-publising-a-jekyll-site-from-gitlab-using-ci-ci.html" target="_blank">independent-software.com - Building and publishing a Jekyll site from GitLab usig CI/CD</a>  
<a href="https://soniaalvarezbrasa.github.io/blog/sobre-markdown-y-kramdown.html" target="_blank">soniaalvarezbrasa - MarkDown y KramDown</a>  
<a href="https://anandjoshi.me/articles/2016-10/Blogging-with-github-and-jekyll" target="_blank">anandjoshi.me - Blogging with github and Jekyll</a>
</small>  
