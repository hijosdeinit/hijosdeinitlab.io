---
layout: post
title: "[HowTo] Metrónomos en GNU Linux"
date: 2022-01-05
author: Termita
comments: true
tags: ["metronomo", "guitarra", "tuxguitar", "debian", "bash", "cli", "terminal", "gui", "ubuntu", "gnu", "linux", "guitar pro", "sistemas operativos", "software", "scripts", "programacion"]
category: Guitarra
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
modified_time: '2012-12-01T19:34:45.665+01:00'
blogger_id: tag:blogger.com,1999:blog-1446475121237311525.post-670718734264923627
blogger_orig_url: https://built4rocknroll.blogspot.com/2012/12/tuxguitar-en-ubuntu.html
---
## 1. Metrónomos en la terminal
/usr/share/sounds/  
### 1.1. script python
https://askubuntu.com/questions/814759/where-can-i-find-a-metronome-for-music-practice  

<br>
### 1.2. script bash
https://askubuntu.com/questions/814759/where-can-i-find-a-metronome-for-music-practice  

<br>
<br>
## 2. Metrónomos con interfaz gráfico
### 2.1. Gtick
[Página oficial de Gtick](http://antcom.de/gtick/)
~~~bash
sudo apt-get update
sudo apt install gtick
~~~

<br>
### 2.2. Orange Metronome
[Orange Metronome](https://launchpad.net/~vlijm/+archive/ubuntu/orangemetronome/+packages)
~~~bash
wget 'https://launchpad.net/~vlijm/+archive/ubuntu/orangemetronome/+files/orangemetronome_0.5.5-1_amd64.deb'
sudo dpkg -i orangemetronome_0.5.5-1_amd64.deb
~~~

<br>
### 2.3. Metrónomos web
Por ejemplo: [online-stopwatch](https://www.online-stopwatch.com/metronome/)  


