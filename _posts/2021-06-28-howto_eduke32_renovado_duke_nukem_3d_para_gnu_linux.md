---
layout: post
title: "[HowTo] 'EDuke32': un renovado 'Duke Nukem 3D' para GNU Linux"
date: 2021-06-28
author: Termita
category: "juegos"
tags: ["sistemas operativos", "juegos", "duke nukem 3d", "duke nukem", "duke3d", "3d realms", "gnu linux", "linux", "compilar", "make", "build-essential", "first person shooter", "shooter", "cmon get some"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small> En este blog se escribe acerca de cosas que, experimentandose personalmente, han concluído satisfactoriamente.  
Esta entrada tratará de algo cuya preparación y ejecución han sido ejecutados en persona, mas el resultado ha sido insatisfactorio: no he logrado hacer que funcione, tan solo una pantalla negra. El sonido, eso sí, funcionaba.  
Se puede deber a diversos factores: el hecho de que probablemente en el sistema en que lo probé la aceleración 3d *opengl* no está funcionando correctamente (tarjeta gráfica nvidia muy antigua, driver *nouveau*, firmware probablemente incompleto...), la posibilidad de que el código que probé -el más actual en la fecha- tenga un bug, etcétera.  
Seguiré intentándolo. Sé que a otros le ha funcionado. Mientras tanto...</small>  

<br>
<br>
## ¿Qué es 'EDuke32'?
['EDuke32'](https://www.eduke32.com/) es un *port* libre del motor del clásico 'Duke Nukem 3D', mejorado -texturas hd, *add-ons*, etc- y [multiplataforma](https://wiki.eduke32.com/wiki/Packages). Está construído para casi todos los sistemas operativos actuales -GNU/Linux, FreeBSD, Windows, MacOS- [e incluso para la veterana consola de videojuegos Nintendo Wii](https://wiki.eduke32.com/wiki/EDuke32_Wii) [<small>[2]</small>](http://wiibrew.org/wiki/EDuke32_Wii#Download).  
'EDuke32' permite [multijugador online](https://wiki.eduke32.com/wiki/Multiplayer).  
<br>
<a href="/assets/img/2021-06-28-howto_eduke32_renovado_duke_nukem_3d_para_gnu_linux/duke_nukem_3d_coverart.png" target="_blank"><img src="/assets/img/2021-06-28-howto_eduke32_renovado_duke_nukem_3d_para_gnu_linux/duke_nukem_3d_coverart.png" alt="Duke Nukem 3D" width="300"/></a>  
<small>El veterano 'Duke Nukem 3D'</small>  
<br>

<br>
No vayamos a pensar que es un *remaster* pirata de 'Duke Nukem 3d': Para jugar 'EDuke32' es necesario poseer el juego original, desde el cual se extraerá un archivo -'duke3d.grp'- que contiene "el juego en sí".  
El archivo 'duke3d.grp', no obstante, puede ser descargado desde [aquí](https://github.com/zear/eduke32/blob/master/polymer/eduke32/duke3d.grp) (42'3MB.). Es el correspondiente a la edición 'Plutonium Pack'.  
Por otra parte, también sirve el archivo 'duke3d.grp' procedente de la versión *shareware* o de prueba.  

<br>
<br>
## Instalar 'EDuke32' en GNU Linux
No he logrado encontrar 'EDuke32' en los repositorios de Debian; el [procedimiento de instalación mediante `apt install`](https://wiki.eduke32.com/wiki/APT_repository) que se describe en la página web oficial del juego está obsoleto: no funciona.  
Probablemente la única forma de instalar 'EDuke32' en un sistema GNU Linux sea **compilándolo**.  

1. Preparación del sistema para la compilación del código  
En [este artículo de este blog]() se trata de las herramientas necesarias para compilar software. Instálense.  
Es importante que una versión de 'gcc' mayor o igual a la 4.8 esté instalada:
~~~bash
sudo apt-get update
sudo apt install build-essential
~~~

2. Instalación de las dependencias  
<span style="background-color:#042206"><span style="color:lime">`sudo apt-get update`</span></span>  
<span style="background-color:#042206"><span style="color:lime">`sudo apt install nasm libgl1-mesa-dev libglu1-mesa-dev libsdl1.2-dev libsdl-mixer1.2-dev libsdl2-dev libsdl2-mixer-dev flac libflac-dev libvorbis-dev libvpx-dev libgtk2.0-dev freepats`</span></span>  

3. Clonación del proyecto  
~~~bash
git clone https://voidpoint.io/terminx/eduke32.git
~~~

4. [Compilación del código](https://wiki.eduke32.com/wiki/Building_EDuke32_on_Linux)  
~~~bash
cd eduke32
make
~~~
Cuando termine la compilación, si ésta ha sido satisfactoria, en el directorio 'eduke32' habrán aparecido 2 archivos:
- 'eduke32': el binario que ejecutará el juego
- 'mapster32': el binario que ejecutará el editor de mapas.  

5. Copia del archivo 'duke3d.grp' anteriormente mencionado al directorio '/eduke32/'  
Básicamente sería algo así: <span style="background-color:#042206"><span style="color:lime">`cp` */rutadondelogengamos/duke3d.grp* *rutadondeclonamoselproyecto/eduke32/*</span></span>  

<br>
<br>
## Ejecución de 'EDuke32'
Desde /eduke32/:
~~~bash
./eduke32
~~~
<br>
<a href="/assets/img/2021-06-28-howto_eduke32_renovado_duke_nukem_3d_para_gnu_linux/eduke32.png" target="_blank"><img src="/assets/img/2021-06-28-howto_eduke32_renovado_duke_nukem_3d_para_gnu_linux/eduke32.png" alt="El lanzador de EDuke32" width="800"/></a>  
<small>El lanzador de EDuke32</small>  
<br>

<br>
<br>
## Mejorar 'EDuke32'
Todos los packs de Alta Resolución (HRP) se pueden encontrar en [hrp.duke4.net](http://hrp.duke4.net/download.php).  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Compilar "DevilutionX" Para Jugar Diablo 1 En X86 Atom (Debian Buster)](https://hijosdeinit.gitlab.io/howto-compilar_devilutionX_Atom_x86_DebianBuster/)
- [[HowTo] Diablo 1 En GNU Linux Mediante DevilutionX](https://hijosdeinit.gitlab.io/howto-Diablo1-linux/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://wiki.eduke32.com/wiki/Main_Page">wiki EDuke32</a>  
<a href="http://wiibrew.org/wiki/EDuke32_Wii#Download">wiki WiiBrew - port de EDuke32 para Nintendo Wii</a>  
<a href="https://linuxize.com/post/how-to-install-gcc-compiler-on-ubuntu-18-04/">linuxize.com</a>  
<a href="https://odysee.com/@gnusenquirer:c/Videojuegos-libres-para-lobos-solitarios-II:2">gnusenquire en odysee.com - videojuegos libres para lobos solitarios II</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

