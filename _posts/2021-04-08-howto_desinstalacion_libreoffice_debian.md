---
layout: post
title: "[HowTo] Desinstalación completa de 'Libreoffice' en Debian y derivados"
date: 2021-04-04
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "libreoffice", "software", "apt", "remove", "purge", "autoclean", "autoremove", "clean", "desinstalacion"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
~~~
sudo apt remove --purge libreoffice*
sudo apt clean
sudo apt autoclean
sudo apt autoremove
~~~

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://qastack.mx/ubuntu/180403/how-to-uninstall-libreoffice">qastack</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
