---
layout: post
title: '[HowTo] Añadir resaltado de sintaxis al editor de textos NANO'
date: 2020-05-25
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "editores de texto", "nano", "resaltado de sintaxis", "nanorc", "markdown", "bash", "programación", "yaml", "python", "CLI", "syntax highlightning", "lenguaje de marcas", "convertir", "emacs", "LaTeX", "orgmode", "pandoc", "texto", "txt", "texto plano", "ubuntu", "debian", "cygwin", "vi", "vim"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>**revisión 20220630**: Acabo de percatarme de que en Debian 11.3.0 'nano' ya proporciona resaltado de sintaxis para 'markdown'. La versión de 'nano' es 5.4</small>  

<br>
<br>
En '/usr/share/nano/' están todos los ficheros de resaltado de sintaxis que posee nano en tu sistema operativo gnu linux.  
Ejecutando 'ls /usr/share/nano/` veremos algo similar a esto:  
~~~
asm.nanorc     fortran.nanorc   man.nanorc     ocaml.nanorc   ruby.nanorc
awk.nanorc     gentoo.nanorc    mgp.nanorc     patch.nanorc   sh.nanorc
c.nanorc       groff.nanorc     mutt.nanorc    perl.nanorc    tcl.nanorc
cmake.nanorc   html.nanorc      nano-menu.xpm  php.nanorc     tex.nanorc
css.nanorc     java.nanorc      nanorc.nanorc  pov.nanorc     xml.nanorc
debian.nanorc  makefile.nanorc  objc.nanorc    python.nanorc
~~~

<br>
Para que nano resalte la sintaxis de markdown en un documento de texto debe existir el archivo **'markdown.nanorc'**.  
Si el archivo 'markdown.nanorc' no existe en '/usr/share/nano/'tenemos 2 alternativas para disponer de él:  

<br>
<br>
#### a. Crearlo nosotros mismos:
~~~
sudo nano /usr/share/nano/markdown.nanorc
~~~

Y pegar en su interior lo siguiente:  
~~~
syntax "markdown" "\.md$" "\.markdown$"
## Quotations
color cyan "^>.*"
## Emphasis
color green "_[^_]*_"
color green "\*[^\*]*\*"
## Strong emphasis
color brightgreen "\*\*[^\*]*\*\*"
color brightgreen "__[\_]*__"
## Underline headers
color brightblue "^====(=*)"
color brightblue "^----(-*)"
## Hash headers
color brightblue "^#.*"
## Linkified URLs (and inline html tags)
color brightmagenta start="<" end=">"
## Links
color brightmagenta "\[.*\](\([^\)]*\))?"
## Link id's:
color brightmagenta "^\[.*\]:( )+.*"
## Code spans
color brightyellow "`[^`]*`"
## Links and inline images
color brightmagenta start="!\[" end="\]"
color brightmagenta start="\[" end="\]"
## Lists
color yellow "^( )*(\*|\+|\-|[0-9]+\.) "
~~~

<br>
Guardamos el archivo, salimos y trabajo hecho.  

<br>
<br>
#### b. Descargar **'markdown.nanorc'** desde el repositorio oficial de resaltado de sintaxis de nano, donde está el "addon" para resaltado de sintaxis de markdown y otros "lenguajes".
Ese archivo, obviamente habremos de copiarlo (y otorgarle los permisos adecuados) en '/usr/share/nano/'  

<br>
<br>
<br>
Entradas relacionadas:  
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Atajos de teclado del editor 'nano'](https://hijosdeinit.gitlab.io/howto_atajos_teclado_nano/)
- [[HowTo] Mostrar el número de línea en el editor 'nano' cuando Alt+Shift+3 no funciona](https://hijosdeinit.gitlab.io/nano__mostrar_numeros_linea_cuando_altshift3_nofunciona/)
- [[HowTo] nano: parámetros de arranque útiles](https://hijosdeinit.gitlab.io/howto_parametros_utiles_arranque_nano/)
- [En nano no existe overtyping](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/)
- [Expresiones regulares no aceptadas por el editor de texto 'nano'](https://hijosdeinit.gitlab.io/regexp_no_aceptadas_por_nano/)
- [[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados](https://hijosdeinit.gitlab.io/howto_micro_editor_texto_debian/)
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)
- [[HowTo] El editor (IDE) atom y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[ourcodeworld](https://ourcodeworld.com/articles/read/807/how-to-enable-syntax-highlighting-for-markdown-files-in-gnu-nano)  
[victorhckinthefreeworld](https://victorhckinthefreeworld.com/2018/02/13/flipando-en-colores-con-el-editor-nano/)  
[markdownguide](https://www.markdownguide.org/basic-syntax/)  
[markdown.es](https://markdown.es/)  
[nano-highlight](https://github.com/serialhex/nano-highlight)</small>  

<br>
<br>
<br>
<br>
