---
layout: post
title: "[HowTo] Conversión a granel desde línea de comandos .flac a .mp3 manteniendo metadatos"
date: 2021-02-07
author: Termita
category: "multimedia"
tags: ["sistemas operativos", "gnu linux", "linux", "flac", "mp3", "ffmpeg", "bulk", "granel", "audio", "multimedia", "cli", "terminal", "linea de comandos", "file", "mplayer"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Pudiera ser que tuviéramos un directorio con unos archivos en formato .flac. Pudiera tratarse, por ejemplo, de un disco de música que hemos convertido a ese formato de audio digital de alta fidelidad -.flac- con compresión mas sin pérdida, lo cual redunda en un reducido tamaño de archivo.  
<br>
Pudiera ser, asimismo, que deseáramos convertir esos archivos .flac en .mp3, formato que sí comprime con pérdida de calidad, de menor tamaño y más compatible con los reproductores.  
Si los archivos *input* .flac poseen metainfomación -tags id3, etcétera... que dotan a un archivo de audio de información acerca del título, álbum, autor, pista, fecha, etc...- el procedimiento es el siguiente:  

<br>
<br>
Desde línea de comandos:  

<br>
Nos situamos en el directorio donde se almacenan los archivos de audio .flac
~~~bash
cd /rutadirectorioflac
~~~

**♠**  
Ejecutamos el siguiente comando:  
<span style="background-color:black"><span style="color:lime"><small>`for F in *.flac; do ffmpeg -i "$F" -ab 256k -map_metadata 0 -id3v2_version 3 "${F%flac}mp3"; done`</small></span></span>  

El parámetro <span style="color:lime">**'-ab'**</span> es el que indica la calidad de la compresión, el *bitrate*. Cuanto mayor es el número, mayor calidad. Por consiguiente, el parámetro '-ab' pueden establecerse *bitrate*, no sólo de **256k** como en el ejemplo, sino de **320k**, **480k**, **520k** o más.  

<br>
<a href="/assets/img/2021-02-07-howto_conversion_flac_a_mp3_manteniendo_metadatos/conversion_flac-mp3_algunasadvertencias.png" target="_blank"><img src="/assets/img/2021-02-07-howto_conversion_flac_a_mp3_manteniendo_metadatos/conversion_flac-mp3_algunasadvertencias.png" alt="Conversión por lotes .fla a .mp3 muestra algunas advertencias" width="800"/></a>  
<small>Conversión por lotes .flac a .mp3 muestra algunas advertencias</small>  

<br>
En mi caso las **advertencias** que muestra el proceso de conversión de .flac a .mp3 no son alarmantes y no afectan a la conversión. A mi entender, pueden ignorarse:
- 'Discarding ID3 tags because more suitable tags were found'
- 'deprecated pixel format used, make sure you did set range correctly'
- 'Frame rate very high for a muxer not efficiently supporting it. Please consider specifying a lower framerate, a different muxer or -vsync 2'  

<br>
<br>
<br>
Se puede comprobar las características de los archivos de audio .mp3 obtenidos y también de los .flac originales (*input*).  
Esto se puede hacer de muchas formas; por ejemplo mediante los comandos **'file'** y **'mplayer'**.  
El comando 'file', sin embargo no proporciona apenas datos de los archivos .mp3. El comando 'mplayer' sí muestra información exhaustiva tanto de .mp3 como de .flac.  

<br>
**♠** Ejecútese desde línea de comandos:  
<span style="background-color:black"><span style="color:lime"><small>`file 01_Bach,JS-Prelude_in_C_Minor_arr_for_guitar_by_Narciso_Yepes.mp3`</small></span></span>  

<br>
<a href="/assets/img/2021-02-07-howto_conversion_flac_a_mp3_manteniendo_metadatos/propiedades_archivo_audio_mp3_mediante_file.png" target="_blank"><img src="/assets/img/2021-02-07-howto_conversion_flac_a_mp3_manteniendo_metadatos/propiedades_archivo_audio_mp3_mediante_file.png" alt="Obteniendo información del archivo .mp3 resultante mediante el comando 'file'. Como puede verse, no muestra apenas detalles." width="800"/></a>  
<small>Obteniendo información de un archivo .mp3 mediante el comando 'file'. Como puede verse, no muestra apenas detalles</small>  

<br>
**♠** Ejecútese desde línea de comandos:  
<span style="background-color:black"><span style="color:lime"><small>`file 01_Bach,JS-Prelude_in_C_Minor_arr_for_guitar_by_Narciso_Yepes.flac`</small></span></span>  

<br>
<a href="/assets/img/2021-02-07-howto_conversion_flac_a_mp3_manteniendo_metadatos/propiedades_archivo_audio_flac_mediante_file.png" target="_blank"><img src="/assets/img/2021-02-07-howto_conversion_flac_a_mp3_manteniendo_metadatos/propiedades_archivo_audio_flac_mediante_file.png" alt="Obteniendo información del archivo .flac *input* mediante el comando 'file'" width="800"/></a>  
<small>Obteniendo información del archivo .flac *input* mediante el comando 'file'</small>  

<br>
**♠** Ejecútese desde línea de comandos:  
<span style="background-color:black"><span style="color:lime"><small>`mplayer -vo null -ao null -identify -frames 0 01_Bach,JS-Prelude_in_C_Minor_arr_for_guitar_by_Narciso_Yepes.mp3`</small></span></span>  

<br>
<a href="/assets/img/2021-02-07-howto_conversion_flac_a_mp3_manteniendo_metadatos/propiedades_archivo_audio_mp3_mediante_mplayer.png" target="_blank"><img src="/assets/img/2021-02-07-howto_conversion_flac_a_mp3_manteniendo_metadatos/propiedades_archivo_audio_mp3_mediante_mplayer.png" alt="Obteniendo información del archivo .mp3 resultante mediante el comando 'mplayer'" width="800"/></a>  
<small>Obteniendo información del archivo .mp3 resultante mediante el comando 'mplayer'</small>  

<br>
**♠** Ejecútese desde línea de comandos:  
<span style="background-color:black"><span style="color:lime"><small>`mplayer -vo null -ao null -identify -frames 0 01_Bach,JS-Prelude_in_C_Minor_arr_for_guitar_by_Narciso_Yepes.flac`</small></span></span>  

<br>
<a href="/assets/img/2021-02-07-howto_conversion_flac_a_mp3_manteniendo_metadatos/propiedades_archivo_audio_flac_mediante_mplayer.png" target="_blank"><img src="/assets/img/2021-02-07-howto_conversion_flac_a_mp3_manteniendo_metadatos/propiedades_archivo_audio_flac_mediante_mplayer.png" alt="Obteniendo información del archivo .flac *input* mediante el comando 'mplayer'" width="800"/></a>  
<small>Obteniendo información del archivo .flac *input* mediante el comando 'mplayer'</small>  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.linuxito.com/gnu-linux/nivel-basico/1186-convertir-flac-a-mp3-con-ffmpeg-preservando-metadatos-id3">Linuxito</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
