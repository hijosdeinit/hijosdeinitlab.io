---
layout: post
title: '[HowTo] Compilar "devilutionX" para jugar Diablo 1 en x86 Atom (Debian Buster)'
date: 2020-09-26
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "juegos", "Diablo", "devilutionX", "linux"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## Antecendentes
En [esta entrada del blog](https://hijosdeinit.gitlab.io/howto-Diablo1-linux/) expliqué cómo jugar a Diablo 1 desde GNU Linux mediante un sencillo programa llamado 'devilutionX'. Generalmente bastará con descargar la versión de este programa adecuada para la arquitectura que estemos utilizando (x86, arm, etc...).  
Sin embargo, en mi caso, he detectado que la versión para x86 -que debería ser apta para arquitecturas de 32 y de 64 bits- no funciona en mi netbook con procesador Atom y Debian Buster (CrunchBang++).  
Solución: compilar el programa.  

<br>
## Procedimiento

1. Descarga del **código fuente** de la última versión de **devilutionX**.
Primero creo una carpeta donde ubicarlo todo. Luego descargo el archivo comprimido que contiene el código fuente desde la sección de descargas de [página oficial](https://github.com/diasurgical/devilutionX/releases):
~~~
mkdir devilutionX
cd devilutionX
wget https://github.com/diasurgical/devilutionX/archive/1.0.1.zip
~~~

2. Descomprimo el archivo
~~~
unzip 1.0.1.zip
~~~
Se descomprimirá en una carpeta llamada 'devilutionX-1.0.1'

3. Instalo las dependencias que se requieren para compilar este programa.
(*) Si alguno de los paquetes no estuvieran en los repositorios que tuviéramos configurados en nuestro sistema deberíamos añadir los repositorios necesarios o buscar en [packages.debian.org](https://packages.debian.org).  
En mi caso:
~~~
sudo apt-get update
sudo apt-get install cmake
sudo apt install g++
sudo apt install libsodium-dev
sudo apt install libsdl2-dev
sudo apt install libsdl2-mixer-dev
sudo apt install libsdl2-ttf-dev
sudo apt install libsdl2-ttf-2.0-0
sudo apt install libsdl2-mixer-2.0-0
sudo apt install libsdl2-image-2.0-0
sudo apt install liblua5.3-0
~~~

4. Compilación
~~~
cd devilutionX/devilutionX-1.0.1/build/
cmake ..
make -j$(nproc)
~~~

<br>
Fin del procedimiento.  

<br>
Para jugar necesitaremos el archivo diabdat.mpq  

5. Descarga de diabdat.mpq
El archivo 'diabdat.mpq' está en el cd original de Diablo 1, mas -si nos da pereza buscar en el trastero- también se puede descargar:
~~~
wget http://cdn.pvpgn.pro/diablo1/DIABDAT.MPQ
~~~
El nombre ha de estar en minúsculas, así que nos aseguramos de ello:
~~~
mv DIABDAT.MPQ diabdat.mpq
~~~

6. Ejecutamos el juego
(Desde la carpeta '/build' en que compilamos y luego ubicamos el archivo diabdat.mpq
~~~
./devilutionx
~~~




## Otras consideraciones

Las configuraciones y savegames se guardan en:
> ~/.local/share/diasurgical/devilution  


MULTIJUGADOR  
- Para poder jugar en modo multijugador el ordenador anfitrión ha de tener el puerto **6112 TCP** abierto en su cortafuegos.  
Si el anfitrión está corriendo GNU Linux Debian o derivados esto se puede hacer así:  
~~~
sudo ufw allow from ipdelordenadorcliente to any port 6112
~~~

- Las instrucciones del juego también señalan que los clientes han de tener el puerto **6112 UDP** expuesto. En mi caso no hizo falta que lo abriese, funcionó aún con el puerto cerrado.

- Una partida multijugador se guarda automáticamente. En principio al reemprender la sesión los personajes tendrán intactos el equipamiento y todos los avances realizados.

<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[diasurgical/devilutionX](https://github.com/diasurgical/devilutionX)  
[zeppelinux](https://www.zeppelinux.es/que-version-de-debian-tengo-instalada/)  
[Medium.com](https://medium.com/@klvst3r/c%C3%B3mo-configurar-ufw-en-ubuntu-18-o-debian-67dab8c72170)</small>  
<br>
<br>
<br>
<br>
