---
layout: post
title: "[HowTo] bot de minado 'crypton' (ecosistema 'Utopia') SIN activar 'upnp'"
date: 2021-11-01
author: Termita
category: "redes"
tags: ["redes", "internet 4.0", "web", "Solid", "gopher", "gemini", "utopia", "crypton", "bitcoin", "criptodivisas","minado", "sistemas operativos", "memoria", "ram", "memoria ram", "seguridad", "maquina virtual", "sandbox", "desconfianza", "liveusb", "maquina virtual", "virtualizacion", "privacidad"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Existen varias redes alternativas a la hoy tan hinchada e irrespetuosa con la privacidad *world wide web*.  
Hace un tiempo [en este blog se habló de *'Gopher'* (aparecido un poco antes que la web) y del *'Proyecto Gemini'*](https://hijosdeinit.gitlab.io/protocolo_Gemini_retornando_al_camino_Gopher_solucionando_deficiencias_web/).  
<br>
Otra de estas redes alternativas es el recién nacido ecosistema [*'Utopia'*](https://u.is/en/), tras 6 años de desarrollo.  
La red 'Utopia', que dice estar centrada en la privacidad y los derechos fundamentales del hombre, está basada *blockchain*, tan en boga hoy, y tiene una criptomoneda llamada <big>***'crypton'***</big> que se mina -según sus desarrolladores- mediante la memoria RAM, de manera sostenible, no invasiva, si gastar apenas recursos de la máquina.  
Actualmente para minar *'crypton'* es necesario:
- procesador de 4 núcleos
- 4gb o más de memoria RAM
- conexión a internet de banda ancha
- ***'upnp'* activado en el router**.  

El problema que plantea ese último requisito se añade al que conlleva otra cuestión más, inspiradora de **desconfianza**:
El software del ecosistema 'Utopia' **NO es de código abierto** (<small>por consiguiente no puede ser auditado en profundidad por la comunidad</small>).  
<br>
Por ello NO es recomendable ejecutar nada que provenga de 'Utopia' "a pelo" en nuestras máquinas, sin aislarlo en un entorno restringido o *'sandbox'*. Estoy probando ese ecosistema en un 'liveusb' y/o en una máquina virtual, sin otras máquinas de la red local encendidas.  

<br>
En lo que respecta a 'Utopia' y su requisito de activación del 'upnp':  
Activar 'upnp' -*Universal Plug and Play*- en el router supone un riesgo importante de seguridad, entre otras cosas porque cualquier aplicación instalada en cualquier máquina de la red local puede abrir puertos de forma automática, sin intervención de su dueño.  

<br>
## ¿Cómo minar 'crypton' sin activar 'upnp'?
1. Arránquese el bot de minado
2. Anótese el puerto que está utilizando
3. Ábrase tan sólo **ese puerto** para la ip de nuestra máquina en la red local.  

En poco más de 1 minuto el bot comenzará a minar.  
  
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Obtener unos céntimos de 'CRP' ('crypton') gratis](https://hijosdeinit.gitlab.io/howto_obtener_crp_crypton_gratis/)
- [Protocolo 'GEMINI': Retornando A La Senda De 'Gopher' En Pro De Una Navegación Sin Aditivos](https://hijosdeinit.gitlab.io/protocolo_Gemini_retornando_al_camino_Gopher_solucionando_deficiencias_web/)
- [Instalación De 'Amfora', Navegador Gemini Para Línea De Comandos, En Debian Y Derivados](https://hijosdeinit.gitlab.io/howto_instalacion_amphora_gemini_web_browser/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
