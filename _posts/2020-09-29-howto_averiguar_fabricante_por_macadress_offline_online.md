---
layout: post
title: '[HowTo] Averiguar el fabricante de un dispositivo de red mediante su dirección MAC (online y offline)'
date: 2020-09-29
author: Termita
category: "redes"
tags: ["sistemas operativos", "redes", "MAC address", "fabricante", "seguridad", "hardware"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Cada dispositivo de red tiene una dirección MAC única. En función de esta dirección -fundamentalmente los 3 primeros pares de caracteres- se puede averiguar el fabricante del dispositivo.  
Existen 2 formas de conocer quién fabricó un dispositivo de red a partir de su dirección MAC.  

<br>
## Offline
Descargando y consultando un documento de texto que contiene un registro de a quién pertenecen los 3 primeros pares de caracteres de cada dirección MAC.
~~~bash
wget http://standards-oui.ieee.org/oui/oui.txt
~~~
La lista es extensísima. Si no se desea abrir el documento en un editor de texto y buscar mediante -por ejemplo- ctrl+f o ctrol+w, se puede construir -en el mismo directorio donde esté ubicado 'oui.txt' un script que, dada la dirección MAC, la busque en la lista:
~~~bash
#!/bin/bash
   
MAC="$(echo $1 | sed 's/ //g' | sed 's/-//g' | sed 's/://g' | cut -c1-6)";
  
result="$(grep -i -A 4 ^$MAC ./oui.txt)";
  
if [ "$result" ]; then
    echo "For the MAC $1 the following information is found:"
    echo "$result"
else
    echo "MAC $1 is not found in the database."
fi
~~~
Para realizar búsquedas basta con darle permisos de ejecución...
~~~bash
sudo chmod +x oui.sh
~~~
... y ejecutar:
~~~bash
bash oui.sh MACquequeremosbuscar
~~~

<br>
## Online
Existen servicios online -unos más actualizados que otros- que permiten consultar la base de datos de fabricantes dada una dirección MAC. Por ejemplo: [suip.biz](https://suip.biz/?act=mac).  

<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[miloserdov](https://miloserdov.org/?p=298)</small>  

<br>
<br>
<br>
<br>
