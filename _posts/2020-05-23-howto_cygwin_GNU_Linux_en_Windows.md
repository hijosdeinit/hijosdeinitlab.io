---
layout: post
title: "[HowTo] cygwin: GNU Linux en Windows"
date: 2020-05-23
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "Linux", "cygwin", "cygwin/X", "emulación", "Windows", "virtualización", "máquinas virtuales", "wine", "dosbox", "scummvm", "emulacion", "emulador"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---

[Cygwin](https://cygwin.com) es una colección de herramientas desarrollada por Cygnus Solutions para proporcionar un comportamiento similar a los sistemas Unix en Microsoft Windows. Su objetivo es portar software que ejecuta en sistemas POSIX a Windows con una recompilación a partir de sus fuentes. Aunque los programas portados funcionan en todas las versiones de Windows, su comportamiento es mejor en Windows NT, Windows XP y Windows Server 2003.  
En la actualidad, el paquete está mantenido principalmente por trabajadores de Red Hat. Se distribuye habitualmente bajo los términos de la GPL con la excepción de que permite ser enlazada con cualquier tipo de software libre cuya licencia esté de acuerdo con la definición de software libre. También es posible adquirir una licencia con costo para distribuirla bajo otros tipos de licencia.<small>  
fuente: [wikipedia](https://es.wikipedia.org/wiki/Cygwin)</small>  

En resumen, Cygwin permite que en menos de 5 minutos (que es lo que dura la instalación) podamos ejecutar linux -con escritorio gráfico incluso- sin necesidad de ejecutar una máquina virtual. A diferencia de una máquina virtual, cygwin se instala como una aplicación de Windows normal, ocupa muy poco espacio en disco y su consumo de recursos es bajísimo. La instalación por defecto es totalmente funcional y se le pueden añadir comandos y aplicaciones extra.  
Cygwin es, en definitiva, "una colección de herramientas que se ejecutan en Windows, y que se comportan como si estuviesen siendo ejecutadas en sistemas Unix. Gracias a estas herramientas vamos a poder utilizar programas tan emblemáticos como la terminal en Windows".  
<br>
El procedimiento para comenzar a trabajar con cygwin es el siguiente:  
<br>
<br>
### 1. Descarga del instalador de cygwin
Desde la página oficial: [setup-x86_64.exe](https://cygwin.com/setup-x86_64.exe)  
**<big>NO</big> DEBE BORRARSE EL INSTALADOR** UNA VEZ TERMINADA LA INSTALACIÓN. MÁS ADELANTE SERÁ NECESARIO EJECUTARLO NUEVAMENTE CADA VEZ QUE DESEEMOS INCORPORAR NUEVOS PAQUETES / PROGRAMAS / HERRAMIENTAS A CYGWIN. No se generará una instalación nueva porque detectará que cygwin ya está instalado, mas ese es el procedimiento que los creadores determinaron para que el usuario fuera añadiendo tanto sofware extra como deseara.
<br>
<br>
### 2. Instalación de cygwin
Doble click en el instalador y empieza el proceso de instalación.  
Preguntará qué tipo de origen se desea para la instalación; yo escogí "install from internet". Asimismo se debe especificar:  
- directorio de instalación.  
- directorio donde se almacenarán los paquetes extras que querramos instalarle.  
- tipo de conexión a internet que estamos utilizando, que es la que empleará para descargar los paquetes (en mi caso: "Direct Connection").  
- *mirror* desde el que que se desea descargar los paquetes (en mi caso elegí el primero de la lista).
- paquetes (aplicaciones, comandos, herramientas) extra a instalar (Escogí nano. Más adelante se pueden añadir los que se deseen).  
- si se desea un acceso directo en el escritorio de Windows.  

<br>
### 3. Ejecución de cygwin
Si al final de la instalación se especificó crear un enlace directo en el escritorio de windows bastará con hacerle doble click.  
Si no, buscamos el lanzador en el menú inicio de Windows y lo ejecutamos desde ahí.  
Aparecerá la típica línea de comandos (CLI) de GNU Linux. De serie, sin añadirle paquetes extras, trae los comandos típicos: ls, mkdir, cd, rm, cat, split, sha256sum, etcétera. Me ha sorprendido que por defecto trae el comando rename.
Para conocer el **listado de comandos** de que disponemos basta con ejecutar:
~~~
ls /bin/*.exe
~~~  
El **directorio raíz** en cygwin -aquel al que accedemos cuando ejecutamos 'cd /' es la carpeta donde lo instalamos, y es accesible tanto desde cygwin como desde Windows; es más, todos los dispositivos de almacenamiento de que tenga el sistema Windows serán accesibles desde cygwin, concretamente en la carpeta **'/cygdrive'** que se encuentra en el directorio raíz.  
El **directorio /home** de nuestro usuario, por supuesto, existe.  
<br>
[Manual de cygwin](x.cygwin.com/docs/ug/cygwin-x-ug.pdf)  

<br>
<br>
### 4. Instalación / Desinstalación de paquetes extra
Ejecutando nuevamente el instalador de cygwin (**manteniendo siempre la misma ruta de destino que se especificó anteriormente en la instalación**) se pueden seleccionar o deseleccionar nuevos paquetes para añadir o eliminar respectivamente.  
En ese interfaz hay un sistema de búsqueda para acceder rápidamente -en caso de que exista- al paquete que se desea instalar.  
El [listado de paquetes, asimismo, se puede consultar en la página oficial de cygwin](https://cygwin.com/packages/package_list.html). Hay cientos de ellos: 'git', 'vi', etc... y también aquellos necesarios para dotar a cygwin de **entorno gráfico** (paquete 'xinit' dentro de la categoría 'X11'), convirtiéndolo así en lo que se denomina [**'cygwin/X'**](x.cygwin.com/docs/ug/cygwin-x-ug.pdf).  
<br>
Más adelante, con más tiempo en otra entrada, escribiré mis impresiones de usuario aprendiz sobre cygwin/X.  
<br>
![cygwin/X]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-05-23_[HowTo]_cygwin_GNU_Linux_en_Windows/cygx-multiwindow-20150201.png)  
<small>cygwin/X, escritorio GNU Linux en Windows mediante cygwin</small>
<br>
<br>
<br>
### Otros proyectos similares y/o relacionados
- [Swan](www.starlig.ht/). Los creadores lo definen como "GNU/Cygwin Xfce Desktop". Básicamente es una implementación de cygwin que permite ejecutar una réplica del entorno gráfico XFCE en Windows de 64 bits, y por supuesto la terminal de GNU Linux.
- [CyGnome](cygnome.sourceforge.net/). Cygwin + Gnome. Permite portar el escritorio Gnome al sistema operativo Windows usando Cygwin como herramienta y entorno de usuario.  

<br>
Como curiosidad, señalar que, aunque no he buscado mucho, aún no he encontrado un equivalente inverso a cygwin; es decir: no he encontrado ningún programa (no una máquina virtual) que ejecute Windows -su powershell y/o su entorno gráfico- en una ventana de GNU Linux. Seguiré buscando.
<br>
<br>
---  
<small>Fuentes:  
[(fuubar)Cygwin: creyéndome un chico Linux en Windows - Parte I](https://fuubar.wordpress.com/2014/01/14/cygwin-creyendome-un-chico-linux-en-windows-parte-i/)  
[(fuubar)Cygwin: creyéndome un chico Linux en Windows - Parte II](https://fuubar.wordpress.com/2014/02/24/cygwin-creyendome-un-chico-linux-en-windows-parte-ii/)  
[UbuntuLife](https://ubuntulife.wordpress.com/2008/10/22/cygwin-un-entorno-de-linux-para-tu-windows/)  
[Microsiervos](https://www.microsiervos.com/archivo/ordenadores/cygwin-entorno-unixlinux-sobre-windows-en-dos-patadas.html)  
[cygwin and UNIX tutorial for beginners](www2.imm.dtu.dk/courses/02333/cygwin_tutorial/index.html)  
[cygwin user's guide (html)](https://www.cygwin.com/cygwin-ug-net/cygwin-ug-net.html)
[cygwin/X user's guide(html)](x.cygwin.com/docs/ug/cygwin-x-ug.html)  
[install cygwin](https://cygwin.com/install.html)
[(freedesktop)How to startup cygwin/X](https://www.freedesktop.org/wiki/CygwinX/HowTo/)</small>  

<br>
<br>
<br>
