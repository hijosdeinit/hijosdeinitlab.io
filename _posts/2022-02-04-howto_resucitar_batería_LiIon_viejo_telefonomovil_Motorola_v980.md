---
layout: post
title: "[HowTo] Resucitando la batería de un viejo teléfono móvil Motorola v980 (El poder del lonchafinismo con un teléfono móvil viejo, vol02)"
date: 2022-02-04
author: Termita
category: "hardware"
tags: ["hardware", "firmware", "sistemas operativos", "software", "flash", "rom", "custom rom", "lineage os", "degoogle", "root", "kazam trooper 450", "kazam", "trooper", "apple", "motorola", "ipad", "iphone", "ipod", "itunes", "apps", "obsolescencia", "derecho a reparar", "apple store", "ios", "macOS", "windows", "hack", "crack", "retromatica", "lonchafinismo", "libertad", "recursos", "bloatware", "motorola v980", "bateria", "resucitar bateria", "litio ion", "liion", "lipo", "imax b6", "nimh", "mah", "voltios", "electronica"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>En este mundo posmoderno cuando compramos una máquina no somos conscientes de que en realidad lo que estamos haciendo es pagar un dinero a cambio de usar una máquina durante un periodo de tiempo muy pequeño: apenas 5 años en muchas ocasiones.  
La obsolescencia programada en máquinas que han sido diseñadas para durar -no estropearse- pero para quedarse obsoletas al cabo de unos años, es la práctica común y exagerada hoy día. Antaño existía, mas no a los niveles que vemos hoy, cuando además éstas impiden el "derecho a reparar" sus máquinas.  
Nos enfrentamos, pues, a una obsolescencia de las máquinas:
... a nivel ***software***: pasados unos años el sistema operativo, las aplicaciones, el soporte, etc... dejan de funcionar y/o de tener soporte.
... a nivel ***hardware***: dificultad, por no decir, imposibilidad de reparar, actualizar, mejorar, acceder a recambios, etcétera.
Quizás si todos fuéramos conscientes de todo esto en todo momento no compraríamos tan a la ligera esas bonitas máquinas que, al cabo de menos de una década -aún funcionando correctamente- nos van a *dejar tirados* sobre todo a nivel software.</small>  

<br>
<br>
Teléfono móvil del año 2005 marca <big>'Motorola', modelo 'v980'</big>
- Network Technology: GSM / UMTS | 2G bands  GSM 900 / 1800 / 1900 | 3G bands  UMTS 2100 | Speed: Yes, 384 kbps
- Launch Announced  2004, Q3
- Status: Discontinued
- Body Dimensions: 94 x 49 x 27 mm, 105 cc (3.70 x 1.93 x 1.06 in)
- Weight: 137 g (4.83 oz)
- SIM  Mini-SIM
- Display  Type  TFT, 65K colors
- Size  1.9 inches, 30 x 37 mm, 11.4 cm2 (~24.7% screen-to-body ratio)
- Resolution  176 x 220 pixels (~148 ppi density)
- Second external CSTN display (96 x 80 pixels), 4K colors
- Screensaver, wallpapers: Downloadable screensavers, wallpapers
- Memory  Card slot  microSD (dedicated slot)
- Phonebook  Photo call
- Call records  10 dialed, 10 received, 10 missed calls
- Internal  2MB
- Main Camera  Single  VGA
- Features  LED flash
- Video  Yes
- Selfie camera  Single  Yes
- Video  
- Sound  Loudspeaker  Yes
- Alert types  Vibration; Downloadable polyphonic, monophonic, MP3 ringtones
- 3.5mm jack  No
- Comms  WLAN  No
- Bluetooth  No
- GPS  No
- Radio  No
- USB  Proprietary
- Features  Sensors  
- Messaging  SMS, EMS, MMS, EMail, Instant Messaging
- Browser  WAP 2.0/xHTML
- Games  Yes + Java downloadable
- Java  Yes, MIDP 2.0
- MP3/MP4 player
- Organizer
- Predictive text input
- Voice dial
- Battery  Type  Removable Li-Ion 820 mAh battery (BA685)
- Stand-by  Up to 145 h
- Talk time  Up to 2 h
- Misc  Colors  Black

<br>
<a href="/assets/img/2022-02-04-howto_resucitar_batería_LiIon_viejo_telefonomovil_Motorola_v980/motorola_v980.jpg" target="_blank"><img src="/assets/img/2022-02-04-howto_resucitar_batería_LiIon_viejo_telefonomovil_Motorola_v980/motorola_v980.jpg" alt="Motorola v980" width="300"/></a>  
<small>Motorola v980</small>  
<br>

Pues bien, ese es un teléfono movil perfecto para el macho alfa en el 2022. Bromas aparte, podría decirse que este teléfono es apropiado para llevar una vida en libertad. Ahí queda eso.

<br>
El teléfono funciona perfectamente, mas la batería -de tipo Li-ion, 3'7v 810mAh- estaba totalmente descargada. No es buena idea dejar que una batería de litio se vacíe completamente, puede ser hasta peligroso: las baterías LiPo tieden a inflarse e incluso incendiarse o explotar.
Este tipo de baterías cuando se drenan totalmente quedan aparentemente inútiles. Mas hay una forma de **devolverlas a la vida, resucitarlas**.  

<br>
<a href="/assets/img/2022-02-04-howto_resucitar_batería_LiIon_viejo_telefonomovil_Motorola_v980/motorola_v980_battery_3,7v_810mah.jpg" target="_blank"><img src="/assets/img/2022-02-04-howto_resucitar_batería_LiIon_viejo_telefonomovil_Motorola_v980/motorola_v980_battery_3,7v_810mah.jpg" alt="batería de Motorola v980: 3'7v. 810mah" width="300"/></a>  
<small>batería de Motorola v980: 3'7v. 810mah</small>  
<br>

<br>
## Procedimiento
Se necesita un cargador inteligente como, por ejemplo, ['SkyRc Imax B6'](https://www.skyrc.com/iMAX_B6_Charger). Son baratos, menos de 30€. Disponer de uno es una inversión a largo plazo, pues son muy versátiles y se le puede sacar mucho partido en lo sucesivo en múltiples "escenarios". Pueden cargar hasta baterías de plomo (Pb), revivir pilas AA / AAA NiMh, etc...  

<br>
<a href="/assets/img/2022-02-04-howto_resucitar_batería_LiIon_viejo_telefonomovil_Motorola_v980/imaxb6_cargando_bateria_motorolav980.jpg" target="_blank"><img src="/assets/img/2022-02-04-howto_resucitar_batería_LiIon_viejo_telefonomovil_Motorola_v980/imaxb6_cargando_bateria_motorolav980.jpg" alt="Imax B6 cargando batería de Motorola v980" width="300"/></a>  
<small>Imax B6 cargando batería de Motorola v980</small>  
<br>

Si intentamos cargar la batería siguiendo el método tradicional -programa 'LiPo'- el cargador nos dirá que está "muerta": 'Low voltage'. Que no cunda el pánico.  
Para hacer que eso cambie basta con cargarla un poco como si fuera una batería NiMh, es decir, mediante el programa 'NiMh' que trae definido este y otros cargadores inteligentes. Hay, eso sí, que especificarle al preset el amperaje de la batería antes, introduciéndolo en el apartado existente al efecto. En mi caso: 0'8.  
Tras tenerla cargando un par de minutos así, como si fuera una batería NiMh, hay que parar la carga.  
Acto seguido se puede proceder a cargar la batería mediante el programa 'LiPo', especificando -como siempre- los parámetros de la batería. En el caso de esta batería son:
- USER SET PROGRAM: LiPo
- V.Type: 3.7V
- Safety Timer: OFF
- Capacity Cut-Off: OFF
- USB/Temp Select : Temp Cut-Off 40C
- LiPo CHARGE: 0.4A    3.7V(1S)  

Cuando termine con la carga el aparato avisará. Desconéctese la batería, insértese en el teléfono móvil y enciéndase. Trabajo hecho, disfrute de su vida.  

<br>
<br>
<br>
Entradas relacionadas:  
- [El Poder Del Lonchafinismo Con Un Teléfono Móvil Viejo, Vol01: Kazam Trooper 450 Del Año 2015)](https://hijosdeinit.gitlab.io/el_poder_de_un_telefono_movil_viejo_vol01_kazam_trooper_450/)
- [Mirando cara a cara al abismo de la incoherencia posmoderna: un vistazo a la Tecnofilia creditofágica](https://hijosdeinit.gitlab.io/tenofilia_creditofagica/)
- [https://hijosdeinit.gitlab.io/Germen_rendicion_gnulinux_a_obsolescencia/](https://hijosdeinit.gitlab.io/Germen_rendicion_gnulinux_a_obsolescencia/)
- [[HowTo] Instalación netinstall de Debian en netbook Compaq Mini](https://hijosdeinit.gitlab.io/howto_instalacion_netinstall_Debian_netbook_compaqmini/)
- [[HowTo] Mantener vivo un iPad antiguo, vol.01: instalar aplicaciones 'no tenidas antes'](https://hijosdeinit.gitlab.io/howto_mantener_vivo_ipad_antiguo_vol01__instalar_aplicaciones_no_tenidas_antes/)
- [[HowTo] búsqueda de páginas web ‘desaparecidas’, directamente en la caché de Google](https://hijosdeinit.gitlab.io/howto_busqueda_en_cache_de_google_paginas_desaparecidas/)
- [Saque todo lo que pueda de los soportes ópticos (cd/dvd), si no lo ha hecho ya](https://hijosdeinit.gitlab.io/saque-todo-lo-que-pueda-de-los-soportes/)
- [Otra faceta de la vida útil de las memorias usb](https://hijosdeinit.gitlab.io/otra-faceta-vida-util-memoria-usb/)
- [Qué pedirle a un ‘pendrive’ USB](https://hijosdeinit.gitlab.io/requisitos_de_un_pendrive/)
- ["Gratis"](https://hijosdeinit.gitlab.io/lo_gratis/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="http://aeromodelismo.epiel.com/scripts/carga.cgi?Lang=es&Tipo=LiPo&Voltaje=3%2C7&Capacidad=820&CargaMaxima=&Modo=Lento&Cargador=SkyRC+iMAX+B6&.submit=Calcular&.cgifields=Cargador&.cgifields=Tipo&.cgifields=Modo&.cgifields=Lang">El pitufo volador - calculador de cargas de baterías</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
