---
layout: post
title: "[HowTo] Convertir vídeo '.ogv' a otros formatos"
date: 2021-02-15
author: Termita
category: "multimedia"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "linea de comandos", "cli", "terminal", "multimedia", "ogv", "ogm", "ogg", "avi", "mkv", "mp4", "video", "convertir", "ffmpeg", "mencoder", "mplayer"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
El formato de vídeo <big>**'.ogv'**</big> está relacionado con el formato contenedor estandarizado OGG Vorbis (**'.ogg'**), diseñado específicamente para la manipulación y transmisión.  
<small>«El formato de vídeo '.ogv' es un formato sin restricciones de patentes de software, desarrollado por Xiph.org, que ofrece una compresión de vídeo con pérdida. El formato OGV puede multiplexar numerosos códecs de código abierto independientes para texto, vídeo y audio, como Theora. Sus códecs han sido incorporados en diversos reproductores multimedia gratuitos y comerciales, así como en varios reproductores portables, por parte de distintos fabricantes. En las páginas web los archivos OGV se utilizan habitualmente para reproducir vídeos mediante la etiqueta `HTML5 <video>`. En general, en el código HTML se hace referencia a estos vídeos como si fuesen archivos OGG, a pesar de que su formato base es el tipo OGV».</small>  

<br>
Si se necesitara convertir un vídeo '.ogv' a otro formato se puede recurrir a herramientas como **'[mencoder](https://www.mankier.com/1/mplayer#General_Encoding_Options_(Mencoder_Only))'** o **'[ffmpeg](https://www.mankier.com/1/ffmpeg)'**.  

<br>
<br>
## Conversión de '.ogv' a otro formato mediante 'MENCODER'
Desde línea de comandos:
~~~bash
mencoder input.ogv -ovc lavc -oac mp3lame -o output.avi
~~~
... donde:
>`input.ogv` es el archivo de video .ogv que se desea convertir (input)
`-ovc lavc` codificar el video con el códec 'libavcodec'
`-oac mp3lame` codificar el audio con el códec 'lamp mp3 audio codec'
`-o output.avi` es el archivo de video resultante, convertido (output)

No está de más verificar los videos, origen y destino (resultante):
~~~bash
file input.ogv
file output.avi
~~~

<br>
<br>
## Conversión de '.ogv' a otro formato mediante 'FFMPEG'
~~~bash
ffmpeg -i input.ogv -vcodec mpeg4 -sameq -acodec libmp3lame output.avi
~~~

<br>
<br>
## Conversión "a granel"
Se puede automatizar la conversión de múltiples archivos de video '.ogv' mediante un script similar a este creado por Vivek Gite:  

<br>
### Conversión de 1 sólo fichero.
~~~bash
#!/bin/bash
# ovg2avi - Conversión ovg a avi
# Author: Vivek Gite <www.cyberciti.biz> Under GPL 2.0+
# -------------------------------------------------------
input="$1"
output="${input%%.ogv}.avi}"
 
die(){
   echo -e "$@"
   exit 1
}
 
[ $# -eq 0 ] && die "Uso: $0 input.ovg\n\tI convertirá un archivo .ogv a formato .avi"
[ ! -f "$input" ] && die "Error $input file not found."
 
if [ -f "$output" ] 
then
	read -p "Peligro: output file $output exists. Sobrescribir (y/n)? " ans
	case $ans in 
		y|Y|YES|Yes) 	mencoder "${input}" -ovc lavc -oac mp3lame -o "${output}";;
	esac
fi
~~~

<br>
A partir de este script se pueden hacer conversiones a granel, en masa:  

<br>
### Conversión en lote, a granel:
Para convertir muchos archivos basta con utilizar bash para crear un bucle. Así:
~~~bash
for o in *.ogv
do
   /path/to/ovg2avi "$o"
done
~~~

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.cyberciti.biz/faq/linux-unix-bsd-appleosx-convert-ogv-to-avi-video-audio/">nixCraft</a>  
<a href="https://es.wikipedia.org/wiki/Ogg">WikiPedia - ogg</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
