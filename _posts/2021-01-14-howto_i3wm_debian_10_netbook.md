---
layout: post
title: "[HowTo] Instalación de i3wm en Debian 10 vía repositorio oficial"
date: 2021-01-14
author: Termita
category: "sistemas operativos"
tags: ["debian", "i3wm", "i3status", "i3lock", "dmenu", "window manager", "sistemas operativos", "gnu linux", "linux", "escritorio", "interfaz grafico", "desktop", "gui"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Hardware: netbook con procesador Atom, 1gb de RAM y muchos años de antiguedad.  
Software: Debian 10. Instalación básica. LXDE.  

<br>
Conozco 2 formas de instalar 'i3' en un sistema operativo GNU Linux:
- Desde repositorio oficial
- [Descargando el código fuente y compilándolo](https://ugeek.github.io/blog/post/2020-08-16-compilar-i3wm-para-debian-ubuntu-y-otras-derivadas.html).  


<br>
## Instalar 'i3' desde repositorio oficial de Debian
Desde línea de comandos basta con ejecutar:
~~~bash
sudo apt-get update
sudo apt install i3
~~~
><small>(ℹ) El paquete **'i3'** -que es el que he instalado- es un [metapaquete](https://www.reddit.com/r/i3wm/comments/9utk7n/ubuntu_i3_vs_i3wm/e96wexd?utm_source=share&utm_medium=web2x&context=3) que contiene, a su vez, los paquetes 'i3status' e 'i3lock' y otros como 'i3wm' y 'dmenu'. No ocurre así con el paquete 'i3-wm' que, de haber sido instalado en lugar de 'i3', hubiera requerido que también se instalaran manualmente los paquetes 'i3status' e 'i3lock'.</small>  

<br>
En el primer login en el recién instalado 'i3wm' el sistema preguntará:
- Qué tecla se desea como "comodín" ("meta"). [Elegí la tecla "Windows"].
- Si se desea crear un archivo de configuración nuevo. [Respondí SÍ y se creó el archivo `/home/miusuario/.config/i3/config`]  

<br>
<br>
Una vez instalado i3wm hay quienes, a parte de configurarlo, le añaden -y configuran- complementos como, por ejemplo:
- i3-gaps (unsure what repo this is available in for Ubuntu, though)
- Polybar
- i3lock-fancy (or similar)  
- [py3status](https://www.youtube.com/watch?v=1oJJq4ji56k)

Aún no he probado nada de eso.  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Configuración de 'i3wm', vol.02: los nombres de las teclas](https://hijosdeinit.gitlab.io/howto_configuracion_i3wm_vol02__nombres_teclas/)
- [[HowTo] Configuración de 'i3wm', vol.01: Cambiar el fondo de pantalla (wallpaper) en OpenBox, i3wm, etc... sin software extra, bajo Debian y derivados](https://hijosdeinit.gitlab.io/howto_cambiar_fondopantalla_Debian_openbox_i3wm/)  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://ugeek.github.io/blog/post/2020-08-16-compilar-i3wm-para-debian-ubuntu-y-otras-derivadas.html">UGeek</a>  
<a href="https://h4ckseed.wordpress.com/2020/08/12/i3wm-en-mi-vida-i/">h4ckseed</a>  
<a href="https://www.ochobitshacenunbyte.com/2020/11/16/conociendo-el-gestor-de-ventanas-i3wm-en-linux/">OchoBitsHacenUnByte</a>  
<a href="https://www.reddit.com/r/i3wm/comments/9utk7n/ubuntu_i3_vs_i3wm/e96wexd?utm_source=share&utm_medium=web2x&context=3">nraygun, en Reddit</a>  
<a href="https://medium.com/hacker-toolbelt/i3wm-on-debian-10-buster-c302420853b1">Miguel Sampaio da Vega, en Medium</a>  
<a href="https://i3wm.org/docs/userguide.html">i3wm.org</a>  
<a href="https://elblogdelazaro.gitlab.io/2019-01-10-mi-configuracion-i3/">El blog de Lázaro</a></small>  
<br>
<br>
<br>
<br>
<br>
<br>
