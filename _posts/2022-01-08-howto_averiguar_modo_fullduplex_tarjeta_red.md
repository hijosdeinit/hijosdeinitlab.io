---
layout: post
title: "[HowTo] Averiguar si una tarjeta de red está trabajando en modo 'full duplex' en Debian y derivados"
date: 2022-01-08
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "redes", "linux", "gnu linux", "debian", "ubuntu", "ethtool", "miitool", "full duplex", "half duplex", "ethernet", "gigabit", "iperf", "dmesg", "test", "auditoria", "velocidad", "transferencia", "ip", "ifconfig", "net-tools"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## ¿Para qué sirve conocer el "modo" en que está trabajando nuestra tarjeta de red?
Porque para obtener el máximo rendimiento es conveniente que esté establecido en el mejor modo admitido por nuestro hardware. Generalmente GNU Linux establece automáticamente esto de la forma correcta. Mas si notamos que las transferencias de datos se realizan de forma sospechosamente lenta es uno de los parámetros que conviene comprobar.  

<br>
<br>
<br>
## Comprobación del "modo" en que está trabajando nuestra tarjeta de red
Algunos de los modos en que suele trabajar una tarjeta de red ethernet son:
- 1000baseTx-FD: 1000Mbps full duplex (FD)
- 1000baseTx-HD: 1000Mbps half duplex (HD)
- 100baseTx-FD: 100Mbps full duplex (FD)
- 100baseTx-HD: 100Mbps half duplex (HD)
- 10baseT-FD: 10Mbps full duplex (FD)
- 10baseT-HD: 10Mbps half duplex (HD)  

El "nombre" de nuestra tarjeta de red se averigua mediante el comando:
~~~bash
ip a
~~~
ô, si el paquete 'net-tools' está instalado, mediante 'ifconfig'
~~~bash
sudo ifconfig
~~~

<small>Supongamos que el nombre de la tarjeta de red cuyo modo de trabajo queremos comprobar es 'eth0'</small>

<br>
Existen varios procedimientos para comprobar el modo y/o la velocidad a la que está configurada que trabaje nuestra tarjeta de red. Algunos son:
- 'dmesg'
- 'ethtool'
- 'miitool'
- 'iperf'  

<br>
<br>
#### a. mediante 'dmesg'
~~~bash
dmesg | grep -i eth0
~~~
> eth0: Link is Up - 1Gbps/Full - flow control rx/tx

<br>
<br>
#### b. mediante 'ethtool'
~~~bash
ethtool eth0
~~~
... y la información que se obtendrá será algo parecido a esto:
~~~
Settings for eth0:
	Supported ports: [ TP MII ]
	Supported link modes:   10baseT/Half 10baseT/Full 
	                        100baseT/Half 100baseT/Full 
	                        1000baseT/Full 
	Supported pause frame use: Symmetric Receive-only
	Supports auto-negotiation: Yes
	Supported FEC modes: Not reported
	Advertised link modes:  10baseT/Half 10baseT/Full 
	                        100baseT/Half 100baseT/Full 
	                        1000baseT/Full 
	Advertised pause frame use: Symmetric Receive-only
	Advertised auto-negotiation: Yes
	Advertised FEC modes: Not reported
	Link partner advertised link modes:  10baseT/Half 10baseT/Full 
	                                     100baseT/Half 100baseT/Full 
	                                     1000baseT/Half 1000baseT/Full 
	Link partner advertised pause frame use: Symmetric
	Link partner advertised auto-negotiation: Yes
	Link partner advertised FEC modes: Not reported
	Speed: 1000Mb/s
	Duplex: Full
	Port: MII
	PHYAD: 0
	Transceiver: internal
	Auto-negotiation: on
Cannot get wake-on-lan settings: Operation not permitted
	Current message level: 0x00000033 (51)
			       drv probe ifdown ifup
	Link detected: yes
~~~
En este caso está todo correcto: `Speed: 1000Mb/s, Duplex: Full`  

<br>
Si deseáramos cambiar ese parámetro a, por ejemplo, 100Mbps 'full duplex' y autonegociación activada:  
<small>(no lo he probado aún)</small>
~~~bash
sudo ethtool –s eth0 speed 100 duplex full autoneg on
~~~
Esto modificaría el modo sólo durante la sesión, es decir, retornaría a su estado normal tras el siguiente reinicio.  
Para que el cambio sea permanente hay que, además, modificar la variable **'ETHTOOL_OPTS'** en el archivo '/etc/sysconfig/network-scripts/ifconfig-eth0' [<small><span style="background-color:#042206"><span style="color:lime">`sudo nano /etc/sysconfig/network-scripts/ifconfig-eth0`</span></span></small>] de la siguiente forma:
~~~
DEVICE=eth0
IPADDR=192.168.100.11
NETMASK=255.255.255.0
BOOTPROTO=static
ONBOOT=yes
ETHTOOL_OPTS="speed 100 duplex full autoneg off"
~~~

<br>
<br>
#### c. mediante 'miitool'
<small>(Este método no lo he probado aún)</small>
~~~bash
sudo mii-tool -v eth0
~~~
... a lo que el sistema, si -por ejemplo- nuestra tarjeta de red es 'full duplex' y está configurada como tal, respondería:
~~~
eth0: negotiated 1000baseT-FD flow-control, link ok
  product info: vendor 00:07:32, model 0 rev 0
  basic mode:   autonegotiation enabled
  basic status: autonegotiation complete, link ok
  capabilities: 1000baseT-FD 100baseTx-FD 100baseTx-HD 10baseT-FD 10baseT-HD
  advertising:  1000baseT-FD 100baseTx-FD 100baseTx-HD 10baseT-FD 10baseT-HD flow-control
  link partner: 1000baseT-HD 1000baseT-FD 100baseT4 100baseTx-FD 100baseTx-HD 10baseT-FD 10baseT-HD flow-control
~~~
Si deseáramos cambiar ese parámetro a, por ejemplo, 100Mbps 'full duplex':  
<small>(no lo he probado aún)</small>
~~~bash
sudo mii-tool -F 100baseTx-FD eth0
~~~

<br>
<br>
#### d. 'iperf'
['iperf'](https://iperf.fr/) es un pequeño programa cliente-servidor que permite auditar el rendimiento real de transferencia midiendo la velocidad máxima que alcanzan dos máquinas conectadas en una red local.  
En [este artículo de este blog](https://hijosdeinit.gitlab.io/howto_iperf_auditar_velocidad_real_red_ethernet_gnu_linux/) se detalla su sencillo modo de empleo.  

<br>
<br>
Entradas relacionadas:
- [[HowTo] Auditar la velocidad máxima de una red ethernet mediante 'iperf' en GNU Linux](https://hijosdeinit.gitlab.io/howto_iperf_auditar_velocidad_real_red_ethernet_gnu_linux/)
- [[HowTo] Instalación de tarjetas de red inalámbricas con chip Realtek 'RTL8812AU' en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_RTL8812AU_debian/)
- [[HowTo] Reactivación de tarjeta wireless ac 'Realtek RTL8812AU' tras actualización del kernel en Debian y derivados](https://hijosdeinit.gitlab.io/howto_reactivacion_tarjetawirelessac_realtek_rtl8812au_tras_actualizacion_kernel_debian_derivados/)
- [[HowTo] Instalación de los controladores de la tarjeta wireless Realtek RTL8822CE. (Debian 10 y derivados)](https://hijosdeinit.gitlab.io/howto_instalacion_controladores_tarjeta_wireless_Realtek_rtl8822ce/)
- [El repositorio de firmare/drivers de GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- [[HowTo] Preparar Debian y derivados para compilar software](https://hijosdeinit.gitlab.io/howto_preparar_debian_para_compilar/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://linoxide.com/how-to-check-ethernet-duplex-mode/">LinoXide</a>  
<a href="https://www.cyberciti.biz/faq/howto-setup-linux-lan-card-find-out-full-duplex-half-speed-or-mode/">CiberCiti.biz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
