---
layout: post
title: "[HowTo] Comienzo con 'vim', II: descendientes / variantes de 'vi'"
date: 2020-04-19
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "vi", "vim", "vino", "vinagre", "visudo", "neovim", "spacevim", "lunarvim", "elvis", "lemmy", "cream", "kakoune", "levee", "nvi", "emacs", "micro", "ed", "editor", "editor de textos", "texto", "texto plano", "latex", "pandoc"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<a href="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/esto_es_sobre_vi-editor.png" target="_blank"><img src="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/esto_es_sobre_vi-editor.png" alt="esta entrada es sobre el veterano editor 'vi'" width="300"/></a>  
<br>

'vim' procede de 'vi', el veterano editor de texto creado por [Bill Joy](https://en.wikipedia.org/wiki/Bill_Joy) en 1976.  
El original, 'vi', que es el padre de toda una saga de editores de texto, tiene multitud de variantes y subvariantes, aunque básicamente el árbol genealógico podría resumirse en:
<big>**'vi'** → **'vim'** → **'neovim'**</big>  

<br>
Los entendidos en la materia -muchos de ellos programadores, gente que pasa aporreando un teclado la mayor parte de su vida- coinciden en que es bueno no perder nunca de vista 'vi', a pesar de los sabores más sofisticados ('vim' o 'neovim', por ejemplo), para no mantener una excesiva dependencia de 'plugins' y "ayudas" de los que 'vi' -más espartano y rudo- carece.  
No hay que olvidar que 'vi' se encuentra presente 'de serie' en la mayoría -por no decir todas- las distribuciones GNU Linux.  

<br>
<br>
## 'vi'
[página oficial de 'vi'](http://ex-vi.sourceforge.net/)  
[artículo en 'Wikipedia' sobre 'vi'](https://en.wikipedia.org/wiki/Vi)  
[repositorio cvs](http://ex-vi.cvs.sourceforge.net/ex-vi/ex-vi/)  

<br>
<a href="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/Vi_source_code_join_line_logic.png" target="_blank"><img src="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/Vi_source_code_join_line_logic.png" alt="'vi' editor" width="400"/></a>  
<br>

<br>
<a href="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/OpenBSD_vi_Editor_Ruby_Hello_World.png" target="_blank"><img src="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/OpenBSD_vi_Editor_Ruby_Hello_World.png" alt="El editor 'vi' en OpenBSD" width="500"/></a>  
<small>El editor 'vi' en OpenBSD. <small>[By Huihermit - Own work, CC0](https://commons.wikimedia.org/w/index.php?curid=30558746)</small></small>  
<br>

<br>
<br>
## 'vim'
[página oficial de 'vim'](https://www.vim.org/)  
[artículo en 'Wikipedia' sobre 'vim'](https://en.wikipedia.org/wiki/Vim_(text_editor))  

<br>
<a href="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/vim.png" target="_blank"><img src="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/vim.png" alt="'neovim'" width="500"/></a>  
<small>'neovim'. <small>[GPL](https://commons.wikimedia.org/w/index.php?curid=365954)</small></small>  
<br>

<br>
#### Una modificación destacable de 'vim': 'SpaceVim'
[página oficial de 'SpaceVim](https://spacevim.org/)  

<br>
<a href="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/spacevim.png" target="_blank"><img src="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/spacevim.png" alt="'SpaceVim'" width="500"/></a>  
<small>'SpaceVim'</small>  
<br>

<br>
<br>
## 'neovim'
[página oficial de 'neovim'](https://neovim.io/)  
[artículo en 'Wikipedia' sobre 'neovim'](https://en.wikipedia.org/wiki/Vim_(text_editor)#Neovim)  
[repositorio de 'neovim' en 'GitHub'](https://github.com/neovim/neovim)  

<br>
<a href="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/neovim.png" target="_blank"><img src="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/neovim.png" alt="'neovim'" width="500"/></a>  
<small>'neovim'</small>  
<br>

<br>
#### Una modificación destacable de 'neovim': 'LunarVim'
[página oficial de 'LunarVim'](https://www.lunarvim.org/)  
[repositorio de 'LunarVim' en 'GitHub'](https://github.com/LunarVim/LunarVim)  

<br>
<a href="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/lunarvim.png" target="_blank"><img src="/assets/img/2020-04-19-howto_comienzo_con_vim_2_variantes_de_vim/lunarvim.png" alt="'LunarVim" width="500"/></a>  
<small>'LunarVim'</small>  
<br>


<br>
<br>
## Otros no menos importantes descendientes de 'vi'
Basándose en 'vi', existen otras variantes / forks / "interpretaciones". Son legion, muchas de ellas importantísimas.  
- AmigaVIM - A port of VIM 5.0 to the amiga
- bvi - A binary editor that follows the ViFamily KeyboardLayout convention.
- calvin - A limited vi for DOS. (No R replace, and limited to 640KB memory.)
- Cream - Vim repackaged to be CUA compliant
- Elvis - Began on the Atari 520 ST computer.
- jsvi - Open source web-based editor in JavaScript, modeled on vi
- Kakoune - Open source VIM inspired editor for Linux and OS/X
- Lemmy - A windows version with syntax highlighting.
- levee - A tiny vi clone for Linux
- MacVim - Open source port of the Vim editor to the Macintosh
- nvi - new vi. Free BSD-Unixes (FreeBSD, NetBSD and OpenBSD) use this as vi.
- OpenWatcom Vi - A Vi-like editor included with the OpenWatcom C++/F77 development environment.
- PVIC - A portable Vi clone based on Stevie
- S Miller - Tiny, multi-platform, Vi-like editor. (31k)
- Stevie - Small vi clone originally written for the Atari ST and ported to other platforms
- svicc - An nvi clone for Commodore C64
- tvx - Open source editor emulating vi - predecessor of Vide
- VI65 - Open source vi implementation for devices using the 6502 processor (Commodore, Atari 8bit, Apple II)
- VI Distributed - Open source distributed ncurses based editor
- Vigor - "Clippy" plugin for vi under Linux and BSD
- vile - VI and Emacs combination. Implemented using MicroEmacs
- VIM - A great (and portable) VI implementation. Clearly the best version available today. It is based on Stevie.
- VimCE - A port of Vim to the PocketPC
- VimOutliner - An outlining editor based on VIM
- VIrus - Open source minimalist vi implementation taken from Busybox
- vis - Open source Vim-like text editor
- WinVI - Freeware Windows GUI vi clone
- xvi - A portable multi-file text editor based on vi  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Comienzo con vim. I](https://hijosdeinit.gitlab.io/howto_comienzo_con_vim_1/)
- [Juegos para aprender 'vi': 'vimadventures', 'vimtutor', 'openvim tutorial'](https://hijosdeinit.gitlab.io/vimadventures-vimtutor_juegos_para_aprender_vi/)  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[texteditors.org](https://www.texteditors.org/cgi-bin/wiki.pl?ViFamily)</small>  

<br>
<br>
<br>
<br>
