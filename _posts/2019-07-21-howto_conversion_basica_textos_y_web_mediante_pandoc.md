---
layout: post
title: "[HowTo] Conversión básica de textos y/o web mediante pandoc"
date: 2019-07-21
author: Termita
category: "sistemas operativos"
tags: ["productividad", "gnu linux", "linux", "pandoc", "latex", "tex", "txt", "pdf", "html", "web", "CLI", "convertir", "exportar"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<big>[Pandoc](https://pandoc.org/)</big> es un conversor de texto a diversos formatos (pdf, odt, tex, etc...)
<br>

Ejemplos de conversión básica son:  

<br>
Convertir una url (una página web) a .pdf
~~~bash
pandoc -s urlquequeremosconvertir -o nombrearchivo.pdf
~~~

<br>
Convertir un texto plano con sintaxis markdown a .pdf
~~~bash
pandoc -s nombrearchivotextomarkdown -o nombrearchivo.pdf
~~~

<br>
Convertir un texto plano con sintaxis markdown a .html
~~~bash
pandoc -s nombrearchivotextomarkdown -o nombrearchivo.html
~~~

<br>
Convertir un texto plano con sintaxis markdown a .odt (libreoffice)
~~~bash
pandoc -s nombrearchivotextomarkdown -o nombrearchivo.odt
~~~

<br>
<br>
Otras entradas de este blog  que tratan la conversión de contenidos web a .pdf u otros formatos son:
- [[HowTo] guardar una página web en formato .pdf mediante wkhtmltopdf](https://hijosdeinit.gitlab.io/howto_web_a_pdf_wkhmltopdf/)
- [[HowTo] guardar una página web en formato .pdf mediante pandoc](https://hijosdeinit.gitlab.io/howto_web_a_pdf_pandoc/)
- [[HowTo] Instalación de pandoc en Debian y/o derivados](https://hijosdeinit.gitlab.io/howto_instalar_pandoc_debian/)  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikibooks.org/wiki/Manual_de_LaTeX/Otros/Exportar_a_otros_formatos">WikiBooks</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
