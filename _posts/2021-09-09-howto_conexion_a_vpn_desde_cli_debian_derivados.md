---
layout: post
title: "[HowTo] Conexión a vpn desde CLI (línea de comandos) en Debian y derivados"
date: 2021-09-09
author: Termita
category: "redes"
tags: ["redes", "sistemas operativos", "vpn", "seguridad", "gnu linux", "linux", "debian", "pivpn", "openvpn"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Muchos estamos acostumbrados a conectarnos a nuestra 'vpn' desde el entorno gráfico, mediante aplicaciones como 'network-manager'.  
Mas, ¿cómo conectar a una 'vpn' desde línea de comandos, sin necesidad de un interfaz gráfico?</small>  

<br>
Obviamente -al igual que cuando nos conectamos mediante el interfaz gráfico- es necesario disponer de:
- las credenciales que nos autentifican en el servidor 'vpn', generamente y para mayor facilidad contenidas en un archivo '.ovpn'
- la dirección ip -o nombre- del servidor 'vpn'  

<br>
<br>
Con esos requisitos, basta con ejecutar desde la terminal:
~~~bash
sudo openvpn --config fichero_configuración.ovpn
~~~

<br>
Para interrumpir la coneción bastará con pulsar Ctrl + C  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://geekland.eu/conectarse-servidor-openvpn-linux-terminal-networkmanager/">geekland.eu</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
