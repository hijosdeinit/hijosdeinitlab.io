---
layout: post
title: "[HowTo] Comparación de discos, directorios y archivos"
date: 2014-05-06
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "windows", "backup", "comparacion", "catalogacion", "comparar", "comparar contenidos", "contenido", "almacenamiento", "disco", "directorio", "archivo", "diff", "diffmerge", "total commander", "winmerge", "backup", "respaldo", "copiar", "sincronizar", "transferir", "cli"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## 1. 'diff'
Para lo más sencillo -que es comparar dos directorios para ver qué archivos diferentes tienen- en GNU Linux se puede utilizar el comando 'diff', que es incorporado por defecto por prácticamente todas las distribuciones.
~~~bash
diff -rq subdirectorio1 subdirectorio2
~~~
El parámetro <span style="background-color:#042206"><span style="color:lime">`-rq`</span></span> indica que comparemos recursivamente ('r') y sólo los archivos diferentes ('q'). Por ejemplo, si dados 2 directorios y uno de ellos tiene un archivo más que el otro, este parámetro nos lo indicará, diciéndonos qué archivo es.  

<br>
Para comparar el contenido de 2 archivos para ver sus diferencias:
~~~bash
diff archivo1 archivo2
~~~
Por ejemplo:
El comando 'diff' nos va a indicar qué pasos hay que seguir para que ambos archivos sean iguales (una manera diferente de decir que son diferentes), teniendo en cuenta que para cada diferencia encontrada nos mostrará primero el número de línea afectado de cada archivo separado por una letra indicando la acción a llevar a cabo en la línea ('**a**': añadir, '**c**': cambiar, '**d**': borrar), y después las líneas afectadas, indicando con '**<**' si la diferencia está en el primer archivo y con '**>**' si está en el segundo archivo.  

<br>
El comando 'diff' es perfecto para ejecutarlo en servidores remotos. Podemos consultar todas sus prestaciones ejecutando <span style="background-color:#042206"><span style="color:lime">`man diff`</span></span> o consultando su [documentación online](http://linux.die.net/man/1/diff).  


<br>
<br>
## 2. 'WinMerge'
['WinMerge'](http://winmerge.org/?lang=es) es una herramienta de diferenciación y combinación de código abierto para Windows que puede comparar tanto carpetas como archivos, presentando las diferencias en un formato de texto visual fácil de entender y controlar.  

<br>
<a href="/assets/img/2014-05-06-howto_comparacion_discos_directorios_archivos/winmerge_foldercmp_treeview.png" target="_blank"><img src="/assets/img/2014-05-06-howto_comparacion_discos_directorios_archivos/winmerge_foldercmp_treeview.png" alt="interfaz de 'WinMerge'" width="500"/></a>  
<small>interfaz de 'WinMerge'</small>  
<br>

<br>
<br>
## 3. 'DiffMerge'
['DiffMerge'](http://www.sourcegear.com/diffmerge/) es un programa que permite comparar más de 2 archivos. También permite comparar directorios y muestra qué ficheros están en una y no en la otra, y la diferencia de los archivos que comparten ambas.  
También permite juntar dos o tres archivos. Además, seleccionando dos o tres archivos en el explorador (o Finder, o Nautilus…), permite abrir el programa para compararlos inmediatamente.  
Está disponible para Windows, MacOS y Linux.  

<br>
<br>
## 4. Total Commander
['Total Commander'](http://www.ghisler.com/amazons3.php) es un *shell* para sustituir al explorador de windows, también basado en el concepto de dos ventanas para poder copiar o mover facilmente de una carpeta a otra o de unas a otras, porque en cada ventana puedes tener múltiples pestañas. Entre las decenas de cosas para las que sirve este programa, está el sincronizar directorios. Un par de clics y señala qué archivos están en uno y no en otro, cuáles son iguales, cuáles son el mismo archivo, pero diferentes.. y, si se desea, mueve los de un lado para el otro para dejar los dos directorios iguales.... y sus subdirectorios, si así se quisiera.  

<br>
<br>
## 5. Otros
Las siguientes herramientas no las he probado aún, mas viene bien conocer que existen.  
- ['Duplicate Cleaner'](http://duplicate-cleaner.uptodown.com/descargar): sencillo programa que  se encarga de localizar en la ubicación que especifiquemos cualquier tipo de coincidencia que pueda existir entre dos o más ficheros, ya sea ésta de nombre, tamaño, fecha o incluso contenido de los archivos.
- ['DoubleKiller'](http://doublekiller.uptodown.com/descargar): este  programa analiza el disco duro o el directorio seleccionado por el usuario para encontrar todos los archivos que coincidan en tamaño y características. Elimina los ficheros duplicados para ahorrar espacio en disco.
- ['Duplicate File Finder'](http://duplicate-file-finder.uptodown.com/descargar): su funcionamiento es simple: compara los ficheros byte a byte o mediante sus CRC (Códigos de Redundancia Cíclica). El porcentaje de acierto en sus búsquedas es del 99.99%.
- ['Anti-Twin'](http://anti-twin.uptodown.com/) puede realizar su análisis en una ruta específica, escaneando toda la estructura de carpetas de la misma, o bien realizar una comparación entre dos directorios cualquiera.
- ['EasyDuplicateFinder'](http://www.easyduplicatefinder.com/files/easy_duplicate_setup.exe) es una práctica y muy útil aplicación que detecta archivos duplicados (100% duplicados; mismo nombre, mismo contenido, en comparación binaria byte por byte).
- ['NoClone'](http://noclone.net/downloadp.aspx?idle=300): esta herramienta se encargará de buscar todos los archivos duplicados que tengas en tu pc y los eliminará (jpg, gif, videos olvidados, canciones repetidas…), con la posibilidad de restaurar los archivos en caso de error.
- ['DeadRinger'](http://deadringer.codeplex.com/releases/view/27900) permite realizar la búsqueda con varios algoritmos diferentes, además, permite la previsualización de los archivos de imágenes encontrados, permitiendo comprobar fácilmente si se trata realmente de un archivo duplicado o no.
- ['CloneSpy'](http://www.clonespy.de/?Download) es una herramienta que ayuda a detectar y eliminar archivos duplicados, vacíos o idénticos pero de diferentes versiones, recuperando un montón de espacio libre en el disco duro.  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Averiguar el tamaño de un directorio desde línea de comandos (CLI) mediante 'du'](https://hijosdeinit.gitlab.io/howto_averiguar_tama%C3%B1o_directorio__cli_find_du/)
- [[HowTo] Cálculo del número de archivos y subdirectorios que contiene un directorio mediante 'find'](https://hijosdeinit.gitlab.io/howto_calcular_numero_archivos_y_o_subdirectorios__find/)
- [[HowTo] Comprobación de que un directorio y sus subdirectorios se han transferido correctamente a su destino](https://hijosdeinit.gitlab.io/howto_comprobacion_copiado_correcto_directorios_archivos/)
- [[HowTo] Comparación de la suma de verificación md5 de varios directorios mediante 'tar'](https://hijosdeinit.gitlab.io/howto_comparacion_md5sum_varios_directorios_mediante_tar/)
- [[HowTo] 'diff'. Comparar directorios ô archivos](https://hijosdeinit.gitlab.io/howto_comparar_directorios_o_archivos/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="http://www.dreamingbytes.com/como-comparar-archivos-y-carpetas-con-el-comando-diff-de-unix-mac-linux/">DreamingBytes</a>  
<a href="http://entreclick.com/7-programas-gratuitos-para-encontrar-archivos-duplicados-en-tu-pc/">EntreClick</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
