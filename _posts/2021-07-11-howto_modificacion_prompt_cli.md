---
layout: post
title: "[HowTo] Modificación del 'prompt' de la línea de comandos (CLI) 'bash' en GNU Linux"
date: 2021-07-11
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "bash", "cli", "línea de comandos", "terminal", "prompt"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>[link al **videotutorial** en Odysee](https://odysee.com/@hijosdeinit:4/2021-07-11-howto_modificacion_prompt_cli:9)</small>  

<br>
## ¿Por qué podría quererse cambiar el *prompt* de la línea de comandos?  
Se me ocurren 2 razones a bote pronto (seguro que hay más):
- Personalización, mera estética
- Controlar la información que se muestra de cara a, por ejemplo, la privacidad.  
Esta 2ª razón suele darse cuando se elaboran y publican tutoriales: vídeos, capturas de pantalla, etcétera.

<br>
<br>
<br>
## ¿Cómo se personaliza el *prompt* de la línea de comandos de un usuario?
En el archivo **'/home/usuario/.bashrc'** (usuarios convencionales) y **'/root/.bashrc'** (root) está la configuración de la línea de comandos bash.  
Editando ese archivo (.bashrc) y agregando al final una línea con una serie de parámetros se consigue controlar lo que el *prompt* muestra.  
<span style="background-color:#042206"><span style="color:lime">`nano /home/`*usuario*`/.bashrc`</span></span> para el *prompt* bash de usuario convencional.  
<span style="background-color:#042206"><span style="color:lime">`sudo nano /root/.bashrc`</span></span> para el *prompt* bash de root.  
Cuando se desee retornar al prompt original -por defecto- bastará con comentar esa línea.  

<br>
Esos parámetros se pueden establecer a mano o bien -como señalaré al final de esta entrada del blog- con ayuda de algún programa que facilite la tarea.  
Básicamente los parámetros que constituyan esa línea a agregar al final del archivo '.bashrc' podrían ser:  
<big><span style="background-color:#042206"><span style="color:lime">`export PS1="\\$\u  \W :> "`</span></span></big>  

<br>
Donde:  
- <span style="background-color:#042206"><span style="color:lime">`\$`</span></span> es un caracter que indica en el *prompt* si estamos *logueados* como un usuario convencional o, por el contrario, como root. Cuando nos *logueemos* como root -por ejemplo ejecutando <small><span style="background-color:#042206"><span style="color:lime">`sudo su`</span></span></small>- el caracter <span style="background-color:#042206"><span style="color:lime">`$`</span></span> cambia a <span style="background-color:#042206"><span style="color:lime">`#`</span></span>. No obstante, si se desea controlar el *prompt* del usuario root debe también modificarse el archivo '/root/.bashrc' tal como estamos haciendo aquí.
- <span style="background-color:#042206"><span style="color:lime">`\u`</span></span> indica que deseamos que se muestre el usuario. Si, por la razón que sea, no deseamos que se muestre el nombre real del usuario podemos sustituirlo por <span style="background-color:#042206"><span style="color:lime">`\@usuarioinventado`</span></span>
- Los dos <span style="background-color:#042206"><span style="color:lime">*`espacios`*</span></span> entre <span style="background-color:#042206"><span style="color:lime">`\u`</span></span> y <span style="background-color:#042206"><span style="color:lime">`\W`</span></span> son una separación que queremos que se muestre entre parámetro y parámetro en el *promt* resultante.
- <span style="background-color:#042206"><span style="color:lime">`\W`</span></span> indica que muestre el directorio de trabajo. Si, en su lugar empleamos la minúscula -<span style="background-color:#042206"><span style="color:lime">`\w`</span></span>- mostrará la ruta completa del directorio de trabajo.
- El <span style="background-color:#042206"><span style="color:lime">*`espacio`*</span></span> entre <span style="background-color:#042206"><span style="color:lime">`\W`</span></span> y <span style="background-color:#042206"><span style="color:lime">`:>`</span></span> indica una separación de 1 espacio que deseamos que se muestre entre ambos parámetros en el *prompt* resultante.
- <span style="background-color:#042206"><span style="color:lime">`:>`</span></span> son caracteres extra que actuarán como un separador que se nos antoja que aparezca. Puede sustituírse por otros caracteres.
- El <span style="background-color:#042206"><span style="color:lime">*`espacio`*</span></span> entre <span style="background-color:#042206"><span style="color:lime">`:>`</span></span> y <span style="background-color:#042206"><span style="color:lime">`"`</span></span> es una separación de 1 espacio que deseamos que se muestre al final del *prompt*, justo antes del punto en el que el usuario comenzará a introducir comandos, órdenes.  

<br>
<br>
### Añadir colores  
Si se desea, por ejemplo, que el color (<small>*foreground*</small>) de las letras del *prompt* sea **amarillo** (<small><span style="background-color:#042206"><span style="color:lime">`[\e[33m\]`</span></span></small>) basta con agregar los códigos <span style="background-color:#042206"><span style="color:lime">`[\e[33m\]`</span></span> y <span style="background-color:#042206"><span style="color:lime">`[\e[m\]`</span></span> respectivamente al inicio y final de cada parámetro:  
<br>
<span style="background-color:#042206"><span style="color:lime">`export PS1="\[\e[33m\]\\$\[\e[m\]\[\e[33m\]\@hijosdeinit\[\e[m\]\[\e[33m\] \[\e[m\]\[\e[33m\] \[\e[m\]\[\e[33m\]\W\[\e[m\]\[\e[33m\] \[\e[m\]\[\e[33m\]:>\[\e[m\] "`</span></span>  
<br>
<a href="/assets/img/2021-07-11-howto_modificacion_promt_cli/prompt_bash_personalizado.png" target="_blank"><img src="/assets/img/2021-07-11-howto_modificacion_promt_cli/prompt_bash_personalizado.png" alt="personalización del 'prompt bash' mediante la aplicación 'ezprompt'" width="800"/></a>  
<small>Mi 'prompt bash' personalizado'</small>  
<br>

<br>
Cada color tiene su código:
- amarillo: <span style="background-color:#042206"><span style="color:lime">`[\e[33m\]`</span></span>
- rojo: <span style="background-color:#042206"><span style="color:lime">`[\e[31m\]`</span></span>
- verde: <span style="background-color:#042206"><span style="color:lime">`[\e[32m\]`</span></span>
- azul: <span style="background-color:#042206"><span style="color:lime">`[\e[34m\]`</span></span>
- cian: <span style="background-color:#042206"><span style="color:lime">`[\e[36m\]`</span></span>
- magenta: <span style="background-color:#042206"><span style="color:lime">`[\e[35m\]`</span></span>
- negro: <span style="background-color:#042206"><span style="color:lime">`[\e[30m\]`</span></span>
- blanco: <span style="background-color:#042206"><span style="color:lime">`[\e[37m\]`</span></span>  

También puede establecerse el color de fondo (<small>*background*</small>) de las letras del *prompt*.  

<br>
<br>
### Configurar el *prompt* con ayuda de una aplicación (interfaz gráfico)
Si no se desea memorizar toda esa nomenclatura, existen aplicaciones que permiten establecer los parámetros a añadir al archivo '/home/usuario/.bashrc' o '/root/.bashrc'  
Una de esas aplicaciones es la página web [ezprompt](https://ezprompt.net/). Permite que, desde el navegador web, podamos construir la línea de parámetros que posteriormente añadiremos al final del archivo '.bashrc'  
<br>
<a href="/assets/img/2021-07-11-howto_modificacion_promt_cli/ezprompt_web.png" target="_blank"><img src="/assets/img/2021-07-11-howto_modificacion_promt_cli/ezprompt_web.png" alt="personalización del 'prompt bash' mediante la aplicación 'ezprompt'" width="600"/></a>  
<small>Personalización del 'prompt bash' mediante la aplicación 'ezprompt'</small>  
<br>

<br>
<br>
Entradas relaccionadas:  
- [[HowTo] Incorporar colores, estilos y efectos en scripts de bash](https://hijosdeinit.gitlab.io/howto_colores_y_efectos_en_scripts_bash/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://awebytes.wordpress.com/2020/07/08/linux-bash-git-bash-personalizar-prompt-de-terminal-ocultar-datos-por-default/">AweBytes</a>  
<a href="https://askubuntu.com/questions/984060/export-ps1-for-customizing-shell-prompt">AskUbuntu - customizing shell prompt</a>  
<a href="https://courses.cs.washington.edu/courses/cse374/16wi/lectures/PS1-guide.html">courses.cs.washington.edu</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
