---
layout: post
title: "[HowTo] Mejor forma de formatear un dispositivo de almacenamiento USB (*pendrive*) desde Windows"
date: 2021-07-09
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "almacenamiento", "sd", "microsd", "sdhc", "pendrive", "formatear", "formato", "ntfs", "fat", "fat32", "tarjeta", "hp usb disk storage format tool", "sd association", "sd memory card formatter", "format", "diskpart", "fdisk", "explorador de archivos", "administrador de discos", "windows", "msdos"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Existen diversas formas de formatear un *pendrive* USB desde Windows.  
La mejor forma, a mi entender, es mediante una herramienta que hace unos años creó Hewlett Packard: **'HP USB Disk Storage Format Tool'**.
'HP USB Disk Storage Format Tool' es para los dispositivos de almacenamiento USB (*pendrives*) lo que 'SD Memory Card Formatter' es a las tarjetas SD o microSD.  
'HP USB Disk Storage Format Tool' también sirve para formatear otros tipos de dispositivos de almacenamiento USB y tarjetas SD ó microSD.  

<br>
## 'HP USB Disk Storage Format Tool'
'HP USB Disk Storage Format Tool' es una aplicación para formatear *pendrives* USB que funciona únicamente en Windows y en MacOS. No he encontrado enlace de descarga en la página oficial de HP, mas en algunos sitios de relativa confianza que existen en la red puede descargarse.  
[Descargar 'HP USB Disk Storage Format Tool v. 2.2.3'](https://hijosdeinit.gitlab.io/assets/docs/2021-07-09-howto_formatear_SD_desde_windows/HPUSBDisk.exe)  

<br>
<a href="/assets/img/2021-07-09-howto_formatear_dispositivo_almacenamiento_usb_desde_windows/HP-USB-Disk-Storage-Format-Tool-v2.2.3.png" target="_blank"><img src="/assets/img/2021-07-09-howto_formatear_dispositivo_almacenamiento_usb_desde_windows/HP-USB-Disk-Storage-Format-Tool-v2.2.3.png" alt="interfaz de 'HP USB Disk Storage Format Tool'" width="400"/></a>  
<small>interfaz de 'HP USB Disk Storage Format Tool'</small>  
<br>

Probablemente 'HP USB Disk Storage Format Tool' podría funcionar en GNU Linux mediante [Wine](https://www.winehq.org/). Yo no lo he probado... aún.  

<br>
<br>
## Otras formas de formatear *pendrives USB* desde Windows:
- Herramienta -interfaz gráfico- que Windows incorpora por defecto [desde el 'Explorador de Archivos'(<small>click derecho sobre la unidad → formatear)</small>), o desde 'Administrador de discos'].
- Desde línea de comandos MS-DOS: Comando ['format'](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/format) [<span style="background-color:#042206"><span style="color:lime">`format '*letra_de_la_unidad*</span></span>], que puede ser acompañado de los comandos / herramientas [<span style="background-color:#042206"><span style="color:lime">'diskpart'</span></span>](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/diskpart) y/o [<span style="background-color:#042206"><span style="color:lime">'fdisk'</span></span>](https://www.computerhope.com/fdiskhlp.htm).  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Mejor forma de formatear SD desde Windows ô MacOS](https://hijosdeinit.gitlab.io/howto_formatear_SD_desde_windows/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="@@@@@@@@@@@@@@@@@@@@@@@@@@@">@@@@@@@@@@@@@@@@@@@@@@@@@@@</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
