---
layout: post
title: "[HowTo] Configurando y añadiendo extensiones a 'vim'. (Comienzo con vim. III)"
date: 2022-03-17
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "vi", "vim", "vino", "vinagre", "visudo", "emacs", "micro", "ed", "editor", "editor de textos", "texto", "texto plano", "latex", "pandoc", "extensiones", "vimrc", "syntax highlight", "resaltado de sintaxis", "airline", "powerline", "personalizacion", "vundle", "plugins", "gruvbox", "markdown", "youcompleteme", "tabular", "nerdtree"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small><small>Entorno:cpu Core2Duo, 6gb RAM, gpu gforce 7600gt, GNU Linux Debian 10 Buster (*netinstall*) stable, kernel 4.19.0-18-amd64, gnome, i3wm. 'vim' 8.1</small></small>  

<br>
Después de utilizar esporádicamente 'vim' tal como viene de serie, decidí personalizarlo un poco para ganar en productividad y, también, mejorar su aspecto visual.  
Básicamente el procedimiento se puede resumir en:
- incorporación de unas pocas modificaciones en '.vimrc'
- incorporación de 5 extensiones ('vundle', '',)  

<br>
## Estado inicial de 'vim'
'vim' 8.1 de serie, sin configuraciones personalizadas.
~~~bash
vim --version
~~~
~~~
VIM - Vi IMproved 8.1 (2018 May 18, compiled Jun 15 2019 16:41:15)
Parches incluidos: 1-875, 878, 884, 948, 1046, 1365-1368, 1382, 1401
Modificado por team+vim@tracker.debian.org
Compilado por team+vim@tracker.debian.org
Versión "enorme" sin interfaz gráfica (GUI).
~~~
~~~
archivo "vimrc" del sistema: "$VIM/vimrc"
archivo "vimrc" del usuario: "$HOME/.vimrc"
2º archivo "vimrc" del usuario: "~/.vim/vimrc"
 archivo "exrc" del usuario: "$HOME/.exrc"
  defaults file: "$VIMRUNTIME/defaults.vim"
       predefinido para $VIM: "/usr/share/vim"
~~~
Tampoco hay ningún *plugin* instalado. Desde 'vim', el comando <span style="background-color:#042206"><span style="color:lime">`:scriptnames`</span></span> no lista ningún plugin.  

<br>
Por otro lado,  el directorio <span style="background-color:#042206"><span style="color:lime">'~/.vim'</span></span> no existe en este sistema.  
Tampoco existe el archivo <span style="background-color:#042206"><span style="color:lime">'~/.vimrc'</span></span>  

<br>
<br>
<br>
<br>
## MI PEQUEÑA PERSONALIZACIÓN DE 'vim'
## Creación del directorio '~/.vim' y el archivo '~/.vimrc' para mi usuario
~~~bash
mkdir ~/.vim
cp /etc/vim/vimrc ~/.vimrc
~~~

<br>
## Incorporo a 'vim' el gestor de plugins 'vundle'
Creo el subdirectorio donde, cuando instale **'Vundle'** -un gestor de *plugins* para 'vim'- éste albergará los *plugins*.
> A parte de 'vundle', existen otros gestores de plugins para 'vim': por ejemplo ['vim-plug'](https://github.com/junegunn/vim-plug)  

~~~bash
mkdir ~/.vim/bundle
~~~
<br>
Clono el repositorio de 'Vundle' en '~/.vim/bundle/Vundle.vim'
~~~bash
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
~~~
<br>
Edito el archivo '~/.vimrc' [<small><span style="background-color:#042206"><span style="color:lime">`nano ~/.vimrc`</span></span></small>] para "instalarle" 'vundle' a 'vim' y, de paso, agregarle algunas funcionalidades más (autoguardado, resaltado de sintaxis, etc...). Lo hago añadiendo estas líneas:
~~~
set nocompatible
filetype off
syntax on
let g:auto_save = 1
let g:auto_save_in_insert_mode = 1
" inicio de lo añadido por mí, copiado de atareao.es:
" be iMproved, required
" required
" establece la ruta para incluir Vundle e lo inicializa
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" o pasa una ruta donde Vundle debería instalar los complementos
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" Los complementos tienen que ir entre las dos líneas
" los complementos después de la siguiente línea
" ------------------------------------------------------
" ------------------------------------------------------
" los complementos antes de la línea anterior

call vundle#end()

" requerido
filetype plugin indent on
" requerido
" To ignore plugin indent changes, instead use:
"filetype plugin on
" fin de lo agregado por mí, copiado de atareao
~~~
<br>
Ejecuto 'vim' y, desde el propio 'vim' ejecuto <span style="background-color:#042206"><span style="color:lime">`:PluginInstall`</span></span>
Se instala el *plugin* 'Vundle'.  
Para salir del entorno de plugins de 'vim' ejecuto <span style="background-color:#042206"><span style="color:lime">`:close`</span></span>  
Salgo de 'vim' ejecutando <span style="background-color:#042206"><span style="color:lime">`:q!`</span></span>  

<br>
### Añado a '~/.vimrc' plugins que me interesan y deseo que se instalen
- [vim-airline](https://vimawesome.com/plugin/vim-airline-superman): «Lean & mean status/tabline for vim that's light as air.»
- vim-airline-themes: pack de temas visuales oficiales de 'airline'
- [tabular](https://vimawesome.com/plugin/tabular): «text filtering and alignment»
- [vim-markdown](https://vimawesome.com/plugin/vim-markdown-enchanted): «vim markdown runtime files»
- [nerdtree](https://vimawesome.com/plugin/nerdtree-are-made-of): «a tree explorer plugin for vim»
- [gruvbox](https://vimawesome.com/plugin/gruvbox): «retro groove color scheme for vim»  

~~~bash
nano ~/.vimrc
~~~
~~~
" Los complementos tienen que ir entre las dos líneas
" los complementos después de la siguiente línea
" ------------------------------------------------------
Plugin 'vim-airline/vim-airline'
Plugin 'vim-airline/vim-airline-themes'
Plugin 'godlygeek/tabular'
Plugin 'plasticboy/vim-markdown'
Plugin 'preservim/nerdtree'
Plugin 'morhetz/gruvbox'
" ------------------------------------------------------
" los complementos antes de la línea anterior
~~~
<br>
Para instalar estos *plugins*, un procedimiento familiar: Arranco 'vim' y, desde el propio 'vim' ejecuto <span style="background-color:#042206"><span style="color:lime">`:PluginInstall`</span></span>  

<br>
Activo el **tema visual** 'gruvbox' y configuro el **aspecto visual** (barra 'airline', tema oscuro, esquema de colores, etcétera) editando el archivo '~/.vimrc':
~~~
set background=dark
set t_Co=256

" Esquema de color para vim
" =============================================================
" Configuración para color de esquema gruvbox
"
let g:gruvbox_italic=1
colorscheme gruvbox
let g:airline_theme='gruvbox'

let g:airline#extensions#tabline#enabled = 1 " Enable the list of buffers

"let g:airline_left_sep = '▶'
let g:airline_powerline_fonts = 2

if (has("termguicolors"))
  set termguicolors
endif
" =============================================================
~~~

<br>
### Instalo en el sistema las tipografías que necesita 'powerline' para mostrarse en todo su explendor
Para que la estupenda barra 'powerline' que el plugin 'vim-powerline', valga la redundancia, se muestre correctamente, a parte de que en '~/.vimrc' exista la línea <span style="background-color:#042206"><span style="color:lime">`let g:airline_powerline_fonts = 2`</span></span> es necesario que en el sistema operativo esté instalado el paquete de tipografías 'powerline': **'fonts-powerline'**
~~~
sudo apt-get update
sudo apt install fonts-powerline
~~~

<br>
### Agrego algunos parámetros / configuraciones más a '~/.vimrc'
~~~bash
nano ~/.vimrc
~~~
~~~
let python_highlight_all = 1 "activa python highlight
let g:airline#extensions#tabline#enabled = 1 "Activa la lista de buffers
set number "mostrar números de línea
set cursorline "mostrar línea debajo del cursor horizontalmente
set cursorcolumn "mostrar línea bajo el cursor verticalmente
set showmatch "mostrar la pareja de [] {} y ()
set encoding=utf-8 "encoding

" Keymaps
" =====================================
map <C-n> :NERDTreeToggle<CR> " Abrir NerdTree
map <F2> :belowright terminal<CR> " Abrir terminal
" =====================================
~~~

<br>
### El resultado
Reinicio el sistema [<small><span style="background-color:#042206"><span style="color:lime">`sudo reboot`</span></span></small>] y, 'vim' luce así:  

<br>
<a href="/assets/img/2022-03-17-howto_comienzo_con_vim_3__extensiones_y_configuracion_inicial/vim_personalizado.png" target="_blank"><img src="/assets/img/2022-03-17-howto_comienzo_con_vim_3__extensiones_y_configuracion_inicial/vim_personalizado.png" alt="'vim' ligeramente personalizado" width="500"/></a>  
<small>'vim' ligeramente personalizado</small>  
<br>

<br>
Me hubiera gustado incorporar / probar el plugin ['youcompleteme'](https://vimawesome.com/plugin/youcompleteme), mas éste al parecer es incompatible con la versión que tengo de 'vim', que es la que existe en los repositorios de Debian.
~~~
YouCompleteMe unavailable: requires Vim 8.1.2269+.
Pulse INTRO o escriba una orden para continuar
~~~
En otra ocasión probaré a agregar el plugin 'YouCompleteMe' mediante el [método que propone atareao en esta entrada de su blog](https://atareao.es/tutorial/vim/autocompletado-en-vim/)  

<br>
<br>
<br>
Entradas relacionadas:
- [[HowTo] Comienzo con 'vim', II: descendientes / variantes de 'vi'](https://hijosdeinit.gitlab.io/howto_comienzo_con_vim_2_variantes_de_vim/)
- [Juegos para aprender 'vi': 'vimadventures', 'vimtutor', 'openvim tutorial'](https://hijosdeinit.gitlab.io/vimadventures-vimtutor_juegos_para_aprender_vi/)
- [[HowTo] El editor (IDE) atom y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [[HowTo] Evitar que 'atom' elimine espacios en blanco al final de línea](https://hijosdeinit.gitlab.io/howto_atom_espacios_blanco_final_linea/)
- [[HowTo] Agregar idiomas al editor (IDE) atom. Ponerlo en español](https://hijosdeinit.gitlab.io/howto_poner_atom_en_espa%C3%B1ol/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- [[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)
- [[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados](https://hijosdeinit.gitlab.io/howto_micro_editor_texto_debian/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [[HowTo] Añadir resaltado de sintaxis al editor de textos NANO](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [[HowTo] nano: parámetros de arranque útiles](https://hijosdeinit.gitlab.io/howto_parametros_utiles_arranque_nano/)
- [En nano no existe overtyping](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/)
- [[HowTo] Visualización de 'markdown' en la línea de comandos (CLI): 'MDLESS'](https://hijosdeinit.gitlab.io/howto_mdless_visor_markdown_cli/)
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
<a href="https://atareao.es/tutorial/vim/los-complementos-en-vim/">atareao.es - los complementos en 'vim'</a>  
<a href="https://atareao.es/tutorial/vim/autocompletado-en-vim/">atareao.es - autocompletado en 'vim'</a>  
<a href="https://atareao.es/tutorial/vim/markdown-con-vim/">atareao.es - markdown con 'vim'</a></small>  

<br>
<br>
<br>
<br>
