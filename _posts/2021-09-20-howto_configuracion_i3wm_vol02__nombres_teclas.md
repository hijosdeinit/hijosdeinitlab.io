---
layout: post
title: "[HowTo] Configuración de 'i3wm', vol.02: los nombres de las teclas"
date: 2021-09-20
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "i3wm", "configuracion", "config", "bindings", "keybindings", "atajos de teclado", "shortcut", "servidor x", "x11", "gestor de ventanas", "windows manager"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
El gestor de ventanas ['i3wm'] -de cuya [instalación en Debian 10 se trató en una entrada de este blog]()- se configura fundamentalmente mediante el archivo <span style="background-color:#042206"><span style="color:lime">`~/.config/i3/config`</span></span>
En este archivo se establecen los **atajos de teclado** siguiendo una nomenclatura concreta. Mas, ¿en qué consiste dicha nomenclatura? ¿qué nombre reciben las teclas según 'i3wm'? Si establecemos en la configuración un atajo de teclado y una de las teclas está mal denominada se producirá un error.  
El gestor de ventanas 'i3wm' acepta los nombres convencionales que reciben las teclas según el servidor X, lo que también se coonoce como ***' X11 Keysym Names'***. He aquí los más comunes:
* Home → Home
* End → End
* PageUp → Prior
* PageDown → Next
* ↑ arrow → Up
* ↓ arrow → Down
* → arrow → Right
* ← arrow → Left
* Return → Return
* Space → space
* Tab → Tab
* Backspace ⌫ → Backspace
* Delete ⌦ → Delete
* Escape → Escape
* Control → Control
* Alt →  Alt
* Barra espaciadora → space
* Mayúsculas (shift) → Shift
* Bloqueo de mayúsculas → Caps_Lock
* F1 → F1
* F2 → F2
* F3 → F3
* ... F12 → F12
* "tecla windows" → Super
* ▤ Menu → Menu
* PrintScreen → Print
* ScrollLock → Scroll_Lock
* Pause → Pause/Break
* NumLock → Num_Lock
* keypad + → KP_Add
* keypad - → KP_Subtract
* keypad `*` → KP_Multiply
* keypad / → KP_Divide
* keypad enter → KP_Enter
* keypad . → KP_Decimal
* keypad 0 → KP_0
* keypad 1 → KP_1
* keypad 2 → KP_2  

<br>
Para conocer el nombre que le da el servidor X a una tecla basta con ejecutar en la terminal:
~~~bash
xev | grep keysym
~~~
... se abrirá una ventana. Todo lo que tecleemos en ella será recogido por el comando que hemos ejecutado en la terminal y éste nos irá diciendo "cómo se llama" cada una de las teclas que hayamos pulsado.  

<br>
<a href="/assets/img/2021-09-20-howto_configuracion_i3wm_vol02__nombres_teclas/nombres_teclas_i3wm__servidor_X.png" target="_blank"><img src="/assets/img/2021-09-20-howto_configuracion_i3wm_vol02__nombres_teclas/nombres_teclas_i3wm__servidor_X.png" alt="Averiguando el nombre de las teclas mediante el comando'xev'" width="600"/></a>  
<small>Averiguando el "nombre" de las teclas mediante el comando'xev'</small>  
<br>

Para terminar, desde la terminal donde lanzóse el comando 'xev', basta con pulsar 'Ctrl' + 'C'.  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Instalación de i3wm en Debian 10 vía repositorio oficial](https://hijosdeinit.gitlab.io/howto_i3wm_debian_10_netbook/)
- [[HowTo] Configuración de 'i3wm', vol.01: Cambiar el fondo de pantalla (wallpaper) en OpenBox, i3wm, etc... sin software extra, bajo Debian y derivados](https://hijosdeinit.gitlab.io/howto_cambiar_fondopantalla_Debian_openbox_i3wm/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="">zzz</a>  
<a href="">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
