---
layout: post
title: "El tiempo da o quita razones. Una mirada atrás a la 'era de la descarga directa'"
date: 2020-04-20
author: Termita
category: "descargas"
tags: ["sofa y doritos", "suscripcion", "leechers", "descarga directa", "premoniciones", "comodidad", "visionarios", "megaupload", "p2p", "paganos", "netflix", "hbo", "amazon prime"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Desconozco cómo llegué ayer a un viejo hilo que me hizo sonreir.  
En esto del internet todo va por modas... por eras.  
2009 pertenece a la era "de la Descarga Directa". ¿Se acuerdan? Megaupload, Rapidshare y decenas de otras similares empresas "de alojamiento" que a continuación surgieron como setas. Antes lo que triunfaba era el vilipendiado 'p2p'... y mucho antes: *Napster*, el 'IRC'... ¿se acuerdan?  
En la era "de la Descarga Directa" por primera vez contemplamos, sorprendidos, a gente estar dispuesta a **pagar** una suscripción (¿les suena?) por descargar material supuestamente pirata, es decir, dispuestos a llenarle los bolsillos a una legión de servidores que prestaban ancho de banda y discos duros.  
La descarga directa atrapó a mucha gente, convirtiéndolos en *yonkis* obsesionados con la velocidad y la comodidad. A mi entender la mentalidad general comenzó a cambiar de tal forma que luego, años más tarde, el gérmen del "estríming" de pago y las suscripciones calaría fácil.  
<br>
En 2009 algunos visionarios del P2P avisaron de que aquello de la descarga directa no tenía futuro y sí muchos inconvenientes. En enero del 2012 se produjo el batacazo, el cierre de Megaupload, la pérdida de kilotones y kilotones de material, y ya nada volvió a ser como antes.  
<br>
Invierno de 2009, pleno apogeo de la descarga directa... Las palabras de unos pocos visionarios que lo vieron venir y que apostaban por seguir con las comunidades y el peer to peer de toda la vida no fueron bien recibidas. Profetas predicando en tierra yerma:  

<br>
<a href="/assets/img/2020-04-20-tiempo_da_quita_razones__una_mirada_atras_era_descargadirecta/descargasdirectas_01.png" target="_blank"><img src="/assets/img/2020-04-20-tiempo_da_quita_razones__una_mirada_atras_era_descargadirecta/descargasdirectas_01.png" alt="debate1" width="500"/></a>  
<br>

<br>
<a href="/assets/img/2020-04-20-tiempo_da_quita_razones__una_mirada_atras_era_descargadirecta/descargasdirectas_02.png" target="_blank"><img src="/assets/img/2020-04-20-tiempo_da_quita_razones__una_mirada_atras_era_descargadirecta/descargasdirectas_02.png" alt="debate2" width="500"/></a>  
<br>

<br>
<a href="/assets/img/2020-04-20-tiempo_da_quita_razones__una_mirada_atras_era_descargadirecta/descargasdirectas_03.png" target="_blank"><img src="/assets/img/2020-04-20-tiempo_da_quita_razones__una_mirada_atras_era_descargadirecta/descargasdirectas_03.png" alt="debate3" width="500"/></a>  
<br>

<br>
Da gusto repasar la historia. Explica muchas cosas, por ejemplo a *Netflix*.  

<br>
<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="">zzzzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

