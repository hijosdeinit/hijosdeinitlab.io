---
layout: post
title: "[HowTo] Mostrar el progreso de un backup con 'dd'"
date: 2021-02-13
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "dd", "progress", "verbose", "backup", "copia de seguridad", "pv", "dialog"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
[Una de las formas de hacer copias de seguridad es mediante el comando 'dd'](https://hijosdeinit.gitlab.io/howto-crear-desde-el-terminal-la-iso-de/).  
Sin embargo, **por sí solo** -sin acompañarlo de parámetros-, el comando 'dd' **no muestra apenas información mientras realiza su trabajo**.  

<br>
Una de las formas de hacer que la tarea que está realizando 'dd' muestre su progreso es [ejecutándolo conjuntamente con 'pv'](https://askubuntu.com/a/215590) y/o '[dialog]()'.  
> <small>«**pv** - Pipe Viewer - is a terminal-based tool for monitoring the progress of data through a pipeline. It can be inserted into any normal pipeline between two processes to give a visual indication of how quickly data is passing through, how long it has taken, how near to completion it is, and an estimate of how long it will be until completion».</small>  

> <small>«**dialog** display dialog boxes from shell scripts. 'Dialog' is a program that will let you present a variety of questions or display messages using dialog boxes from a shell script».</small>

<br>
<br>
Sin embargo, 'dd' en 'GNU Coreutils 8.24+' (<small>a partir de Ubuntu 16.04</small>) posee un nuevo parámetro llamado <big>'status'</big> que muestra el progreso de la tarea.  
Por ejemplo:  
~~~bash
dd if=/dev/sdc1 of=/home/miusuario/backup.dd status=progress
~~~
... y la salida será algo parecido a esto:
> Output
462858752 bytes (463 MB, 441 MiB) copied, 38 s, 12,2 MB/s  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://askubuntu.com/a/363881">AskUbuntu - status=progress</a>  
<a href="https://www.cyberciti.biz/faq/linux-unix-dd-command-show-progress-while-coping/">nixCraft - pv, dialog</a>  
<a href="https://askubuntu.com/a/363846">AskUbuntu - pv, dialog</a>  
<a href="https://askubuntu.com/a/215590">AskUbuntu - pv</a>  
<a href="https://www.mankier.com/1/dd">Mankier - dd</a>  
<a href="https://www.mankier.com/1/dialog">Mankier - dialog</a>  
<a href="https://www.mankier.com/1/pv">Mankier - pv</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
