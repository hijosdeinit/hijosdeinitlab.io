---
layout: post
title: "[HowTo] Descargar videos de 'documania.tv'"
date: 2021-09-01
author: Termita
category: "descargas"
tags: ["documania.tv", "documentales", "torrent", "p2p", "subtítulos", "web 4.0", "descargar", "descargas", "audio", "video", "multimedia", "youtube-dl", "youtube", "rumble", "codigo", "privacidad", "tracking", "anonimato", "offline"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Revisión 20240414: EL PROCEDIMIENTO DEJÓ DE FUNCIONAR, tal como he comprobado tras ser advertido por Gabriel, un visitante de este blog. Mi agradecimiento por el **feedback*.  

<br>
<br>
<small>['Documania.tv'](https://www.documaniatv.com) es una página web que tiene un montón de contenido audiovisual -documentales y pseudo-documentales- disponibles para **visualizar** *online* mas no para descargarlos (<small>desconozco si un usuario "registrado" puede descargar</small>).  
Aunque el servicio es -según mi primera impresión- aparentemente "gratuíto", detesto la visualización *online* tan asociada al -tan en boga- **consumo** "por suscripción" y *streaming* inherentes a 'NetFlix', 'HBO', 'Disney', 'Amazon Prime Video' y otros como 'seriespepito' (pirata) o *iptvs* varias (de pago y piratas casi todas): Soy partidario de "poseer" y reproducir -*offline*, en el momento que yo establezca y en la máquina que yo designe- todo aquello que considero merezca la pena visualizar, aunque sea -como es el caso de la **inmensa mayoría de contenidos** que repaso- material al que de ninguna otra manera -salvo gratuítamente- trataría de acceder.  
Esto forma parte de la filosofía del ***lonchafinismo***, cuya máxima premisa es "va a consumir su puta madre". Las putas y la cocaína que se las paguen otros.  
Y, ciertamente, 'youtube-dl' (<small>porque, estimado *Youtube*, el consumo, la monetización / cuantificación no va conmigo</small>) "nos tiene a muchos muy mal acostumbrados".  
Bromas a parte, una de las cosas que más detesto en este mundo es el **abuso** para obtener **lucro** o la **mendicidad 4.0** ('patreon', 'youtuberismo' calderillófilo y demás ralea). No veo mal "piratear" cosas, SÍ lucrarse a costa de ello, es decir, a costa de personas que no pueden ni quieren pagar por acceder a material **ajeno** al que, salvo gratuítamente, jamás estaría dispuesto a acceder.  
Aunque en principio, como ya he dicho, 'documania.tv' es un servicio "pirata" que -quizás por mi "ceguera publicitaria" o por la acción de 'PiHole' o el *adblocker*, o las 3 cosas simultáneamente- desconozco si tiene ánimo de lucro o publicidad para que algunas pesetillas caigan en sus bolsillos, a ellos también les aplico esa directriz particular de no visualizar nada *online*.  
Para ello, evidentemente, es fundamental <big>**DESCARGAR**</big>, por las buenas o por las malas.</small>  

<br>
<br>
## Cómo descargar de 'documania.tv'
El procedimiento es sencillo, muy obvio, de casi todo el mundo sabido, un "gol de Señor" en toda regla: Sirva este método que aquí enuncio para personas que desconocen que ojeando el código fuente de una página web, muchas veces se puede "sacar" la URL de un video.  
Desde nuestro navegador web favorito, una vez localizado y cargado en la web [documania.tv](https://www.documaniatv.com) el vídeo del que deseamos apropiarnos:  

<br>
### 1. Visualizar el código fuente de la página de video
El código de la web 'documanía.tv' no está ofuscado.  
click derecho → ver código fuente de la página  

<br>
<a href="/assets/img/2021-09-01-howto_descargar_documaniatv/navegadorweb_vercodigofuente.png" target="_blank"><img src="/assets/img/2021-09-01-howto_descargar_documaniatv/navegadorweb_vercodigofuente.png" alt="visualización del código fuente de la página web del video" width="800"/></a>  
<small>visualización del código fuente de la página web del video</small>  
<br>

<br>
### 2. Búsqueda de la URL del video en el código fuente de la página
En el código fuente buscamos (pulsando <span style="background-color:#042206"><span style="color:lime">ctrl+f</span></span>) la *cadena de caracteres*: <span style="background-color:#042206"><span style="color:lime">`type:`</span></span>  
También, como alternativa, puede buscarse la cadena de caracteres <span style="background-color:#042206"><span style="color:lime">`mp4`</span></span>.  

<br>
<a href="/assets/img/2021-09-01-howto_descargar_documaniatv/navegadorweb_vercodigofuente_obtencion_de_url_video_documania.png" target="_blank"><img src="/assets/img/2021-09-01-howto_descargar_documaniatv/navegadorweb_vercodigofuente_obtencion_de_url_video_documania.png" alt="Búsqueda de la url del video en el código fuente de su página web" width="800"/></a>  
<small>Búsqueda de la url del video en el código fuente de su página web</small>  
<br>

<br>
### 3. Cópiese la *url* que tiene asignada el parámetro <span style="background-color:#042206"><span style="color:lime">`file:`</span></span> justo en la línea superior.
<a href="/assets/img/2021-09-01-howto_descargar_documaniatv/navegadorweb_vercodigofuente_url_video_documania.png" target="_blank"><img src="/assets/img/2021-09-01-howto_descargar_documaniatv/navegadorweb_vercodigofuente_url_video_documania.png" alt="url del video" width="800"/></a>  
<small>url del video</small>  
<br>

<br>
### 4. Descarga del video
Desde línea de comandos (terminal): <span style="background-color:#042206"><span style="color:lime">`wget `'*urlqueacabamosdecopiar*'</span></span>  

<br>
<br>
### Profundizando
<small>Este procedimiento es burdo -prácticamente un *gol de Señor*-, mas para páginas web "más complejas" -código ofuscado, etcétera- existen métodos ahí fuera más sofisticados que impican crackeo y trabajos más avanzados a los que yo, por mi propio desconocimiento, no tengo acceso.  
Este es el caso, por ejemplo, de los *scripts* elaborados por *Mhyst* para la desofuscación del código -para posterior expolio masivo y a voluntad- de páginas web que servían contenidos multimedia "ajenos" previo "consumo" de publicidad o registro o, incluso (!), "pago": 'EliteTorrent', 'DivxTotal', 'SeriesBlanco', 'Pepecine' y 'MejorTorrent'.  
Todos estos, una vez *crackeados*, permitían ver lo que fuera sin acceder a la web. Eran *webscrappers*.  
De 'SeriesBlanco' Mhyst hizo varios scripts que permitían descargar todos los enlaces de una serie a una base de datos local y luego consultar la base de datos y ver *online*. 'Seriesblanco' desapareció, los de 'EliteTorrent' ofuscaron su web; la última en funcionar fue 'MejorTorrent'.  
Ninguna ofuscación se resiste a unas horas de análisis.</small>  

<br>
<small>"Querer es poder"; considero que las obviedades que señalo en el artículo probablemente no caerán en saco roto si se anima a algún posible lector -más inteligente o curioso que yo- a aprender y emprender todo tipo de actuaciones para vaciar, expoliar y saquear contenidos -muchos de ellos doblemente ajenos (ajenos al usuario y ajenos al que los creó, típico de las páginas piratas con ánimo de lucro)- que, salvo gratis, jamás se consumirían.</small>  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Obtención de 'ID' (embed) de un video de 'Rumble.com' y descargarlo mediante 'youtube-dl'](https://hijosdeinit.gitlab.io/howto_obtencion_id_video_rumble_para_descargarlo_youtubedl/)
- [[HowTo] Mejor forma de instalar 'youtube-dl' en Debian y derivados. I](https://hijosdeinit.gitlab.io/howto_mejor_instalacion_youtube-dl/)
- [[HowTo] Mejor forma de incorporar la última versión de 'youtube-dl' en Debian y derivados. II](https://hijosdeinit.gitlab.io/howto_youtube-dl_debian_netinstall/)
- [Páginas útiles para todo aquel que descarga 'cosas' de internet](https://hijosdeinit.gitlab.io/Paginas_utiles_para_todo_aquel_que_descarga_de_internet/)
- [[HowTo] Descargar subtítulos de Youtube, nativos y/o autogenerados en el idioma que queramos](https://hijosdeinit.gitlab.io/howto_descargar_subtitulos_youtube_nativos_autogenerados/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="@@@@@@@@@@@@@@@@@@@@@@@@@@@">@@@@@@@@@@@@@@@@@@@@@@@@@@@</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
