---
layout: post
title: "'reto python' de atareao"
date: 2022-02-11
author: Termita
category: "programacion"
tags: ["python", "programacion", "software", "sistemas operativos", "gnu linux", "linux", "windows", "macos", "ide", "pycharm", "vim", "neovim", "vscodium", "vscode", "formacion", "aprendizaje", "productividad", "reto", "atareao"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
[Atareao](https://atareao.es/quien-soy/) ha lanzado a la comunidad un reto fundamentado en la máxima 'a programar se aprende programando': noveles y veteranos en el lenguaje de programación [python](https://www.python.org) pueden participar cooperando en programar una aplicación -en python- que organice las descargas en un subdirectorio concreto.  
Dicha aplicación puede ser realizada para GNU Linux, Windows o MacOs. Asimismo ha de ser configurable vía, por ejemplo, un archivo de texto plano.  

<br>
Es, en definitiva, un reto para aprender 'python'.  
Para desarrollar el código atareao sugiere uno de estos 'IDE': 'vim'/'vim'/'neovim', 'Visual Studio Code'/'VS Codium' o 'pycharm'.  

<br>
Me voy a unir a esta iniciativa. Me parece brillante. Creo que utilizaré 'geany' -del que ya se trató en [esta entrada de este blog]() o el IDE de python ['IDLE'](https://docs.python.org/3/library/idle.html).  

<br>
Más detalles en la [página web de atareao](https://atareao.es/podcast/un-proyecto-para-aprender-python/) y/o en [este capítulo de su podcast](https://anchor.fm/s/5a5b39c/podcast/play/47453575/https%3A%2F%2Fd3ctxlq1ktw2nl.cloudfront.net%2Fstaging) y/o en [este grupo de Telegram](https://t.me/atareao_con_linux).  

<br>
<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
