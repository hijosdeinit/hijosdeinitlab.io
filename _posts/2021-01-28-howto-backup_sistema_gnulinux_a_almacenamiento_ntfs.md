---
layout: post
title: "[HowTo] Transferir un backup completo de un sistema GNU Linux a un disco NTFS"
date: 2021-01-28
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "tar", "borg backup", "copia de seguridad", "backup", "rsync", "dd", "gpg", "ext3", "ext4", "ntfs", "mkisofs"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<big>**NO**</big> es buena idea guardar copias de seguridad "completas" de un sistema operativo GNU Linux en un disco formateado en NTFS, sobre todo cuando alguna de esas copias de seguridad se han realizado mediante 'rsync'.  
¿Por qué?  
- Porque hay atributos de los archivos que, existiendo cuando son albergados por un sistema operativo GNU Linux en una partición o disco formateados -por ejemplo- en ext4, no se conservan al transferirlos a una partición o disco formateados en NTFS.
- Porque las normas que rigen los nombres de los archivos no son las mismas en las particiones características de GNU Linux que en una partición NTFS.
- Porque -por consiguiente- utilidades como 'rsync', que son frecuentemente utilizadas para hacer respaldos de sistemas GNU Linux, experimentan errores cuando el destino de la copia de seguridad es una partición o disco formateados en NTFS.  

<br>
Cuando se hace un backup total interesa que todos los datos mantengan sus propiedades.  
<br>
<br>
☠ Para ello, lo primero que puede ocurrírsele a uno es <big>**comprimir mediante 'tar'**</big> el directorio de una partición o disco ext4 donde -mediante 'rsync', por ejemplo- se respaldó un sistema GNU Linux y luego transferirlo a la partición o disco NTFS.  
<span style="color:lime"><small>`sudo tar -czvf ficherocomprimidoresultante.tar.gz /ficheroocarpetaacomprimir 2>/tmp/errorescompresion.log.txt`</small></span>  
Sin embargo, la compresión mediante 'tar' suele dar errores cuando trata de comprimir ciertos directorios relacionados con 'docker' o temporales.  
Un usuario experimentado en GNU Linux sabrá a ciencia cierta si esos errores son tolerables. En mi caso, que no es otro que el de un usuario básico, esos errores implican desconfianza. Por consiguiente, **descarto el empleo de 'tar'**.  

<br>
☠ <big>**'Borg Backup'**</big> es capaz de guardar una copia de seguridad en un sólo archivo. A continuación podríamos transferirlo a un disco o partición NTFS.  
Sin embargo mi experiencia con 'Borg Backup' -tanto como plugin de Open Media Vault como directamente desde línea de comandos- no ha sido satisfactoria cuando éste ha de respaldar archivos y directorios relacionados con 'docker', temporales, etcétera. **Descartado también**.  

<br>
☠ Emplear <big>**'rsync'**</big> para respaldar el sistema directamente en una partición o disco NTFS, como señalé antes, queda **evidentemente descartado**.  

<br>
<br>
## Solución que se me ha ocurrido
1. Crear -mediante 'gnome-disks', por ejemplo- en un disco extraíble una partición ext4 de tamaño ligeramente superior al de la copia de seguridad que deseamos transferir.
2. Transferir mediante 'rsync' [<span style="color:lime">`sudo rsync -avxlHpEXoth --progress --devices --specials --exclude /ficheroodirectorioaexcluir /ficheroodirectorioorigen /directoriodestino`</span>] la copia de seguridad a dicha partición ext4 recién creada.
3. Hacer una 'imagen' de dicha partición -que, por ejemplo, se llama 'sdd2'- mediante 'dd': <span style="color:lime">`sudo dd if=/dev/sdd2 of=/home/miusuario/`*nombredelbackup.dd*` bs=4M status=progress; sync`</span>
4. (opcional) Comprimir -'tar'- y/o cifrar -'gpg'- dicha 'imagen': comprimir [<span style="color:lime">`sudo tar -czvf ficherocomprimidoresultante.tar.gz /ficheroacomprimir 2>/tmp/errorescompresion.log.txt`</span>], cifrar [<span style="color:lime">`sudo gpg --verbose --always-trust --output nombredeficheroresultantecifrado.gpg --encrypt --recipient identificadordeclavedecifrado ficheroorigenquequeremoscifrar`</span>]. <small>Cabe señalar que al cifrar un archivo mediante 'gpg' se lleva a cabo una cierta compresión.</small>
5. Transferirla al disco o partición NTFS donde tuvimos la mala idea de guardar las copias de seguridad.
6. (opcional) Borrar, reparticionar, formatear, etc... el disco extraíble que empleamos en el 'paso 1'.  

<br>
<br>
☠ Sabiendo que mediante el comando <big>**'mkisofs'**</big> [<span style="color:lime">`sudo mkisofs -o output_imagen.iso nombrededirectorioinput`</span>] [se puede crear una 'ISO' a partir de un directorio](https://hijosdeinit.gitlab.io/howto-crear-desde-el-terminal-la-iso-de/) podría caer en la tentación de ahorrarme parte del proceso.  
Sin embargo sospecho que el estándar 'ISO' no es compatible con algunas características y propiedades de los sistemas de archivos que suele emplear GNU Linux. **Descartado**.  

<br>
<br>
<br>
<br>
<br>
