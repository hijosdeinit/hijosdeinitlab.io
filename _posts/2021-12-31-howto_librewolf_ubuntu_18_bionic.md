---
layout: post
title: "[HowTo] 'LibreWolf' (*appimage*) en Ubuntu 18 'bionic'"
date: 2021-12-31
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "ubuntu", "web browser", "navegador", "internet", "internet 4.0", "min browser", "min", "min browser v.1.20", "chrome", "chromium", "edge", "firefox", "waterfox", "librefox", "librewolf", "icecat", "iceweasel", "basilisk", "privacidad", "anonimato", "respeto", "appimage", "nueva paqueteria"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## ¿Qué es 'LibreWof'?
'LibreWolf' [1](https://librewolf-community.gitlab.io/) [2](https://librewolf.net) es un fork del navegador web Firefox.  
Los creadores de 'LibreWolf' lo describen como «enfocado en la privacidad, la seguridad y la libertad», en contraposición al [en ocasiones polémico 'Mozilla Firefox'](https://www.scss.tcd.ie/Doug.Leith/pubs/browser_privacy.pdf) cuyo código modifican.  
'LibreWolf' asegura ofrecer:
- Ausencia de Telemetría: «sin "experimentos", sin *adware*, sin molestias o distracciones innecesarias».
- Sin "sincronización en la nube (*cloud sync*) mediante cuenta en Firefox".
- Incorpora proveedores privados de búsquedas: 'Searx', 'Qwant', 'DuckDuckGo' (configurado por defecto), etc...
- Incorpora 'ublock origin', el bloqueador de scripts y anuncios.
- Sin 'Pocket' (sin botón "añadir a Pocket").
- Sin contenido esponsorizado / recomendado en la página de inicio.
- Los *snippets* de Firefox para añadir noticias / tips en una nueva pestaña han sido eliminados de la configuración.
- Sin accesos directos esponsorizados.
- Protección frente a *tracking* establecida por defecto en modo 'estricto'.
- Configurado por defecto para que *Cookies* e historial sean eliminados al cerrar el navegador.
- Configurado por defecto en modo 'sólo https'.  

<br>
<br>
## Instalación de 'librewolf' en Debian 10 *buster*
Los métodos de instalación nativa que [se describen en la página oficial de 'librewolf'](https://librewolf.net/installation/debian/) no me han servido en Ubuntu 18 *bionic*; quizás se debe a que están descrito para versiones más modernas de Debian o Ubuntu.  
Por consiguiente he recurrido a la "nueva paquetería", concretamente a 'appimage'.  

Tengo por costumbre ubicar las 'appimage' en '/home/miusuario/_#Programas/'  
~~~bash
mkdir ~/_#Programas/LibreWolf
cd ~/_#Programas/LibreWolf
wget https://gitlab.com/api/v4/projects/24386000/packages/generic/librewolf/95.0.2-1/LibreWolf.x86_64.AppImage
sudo chmod +x LibreWolf.x86_64.AppImage
~~~

<br>
## Ejecución de la *appimage* de 'librewolf'
Para ejecutar 'librewolf':
Desde el subdirectorio donde se ha descargado la 'appimage' de 'librewolf' (`cd /home/miusuario/_#Programas/`)
~~~bash
./LibreWolf-89.0.2-1.x86_64.AppImage
~~~
... o bien, desde el "explorador de archivos" (nautilus, etc...) bastará con hacer 'doble click' sobre el archivo 'LibreWolf-89.0.2-1.x86_64.AppImage'.  

## Postinstalación
Aunque de serie 'librewolf' está configurado óptimamente para la privacidad, reviso la configuración de 'librewolf'.  
Activo 'historial' en los ajustes de 'librewolf', condición necesaria para que "recuerde" la sesión en curso.  

<br>
<br>
Entradas relacionadas:  
- [¿Harto de Firefox? Alternativas: WaterFox, LibreFox, LibreWolf](https://hijosdeinit.gitlab.io/WaterFox_LibreFox_LibreWolf_BadWolf/)
- [[HowTo] Instalación NATIVA de 'epiphany' ('gnome-web') en Ubuntu y familia](https://hijosdeinit.gitlab.io/howto_instalacion_nativa_epiphany_browser_ubuntu/)
- [[HowTo] Instalación de MicroSoft Edge (dev) en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalar_ms_edge_chromium_dev_en_debian_derivados/)
- ["Instalación de 'amfora', navegador gemini para línea de comandos, en Debian y derivados"](https://hijosdeinit.gitlab.io/howto_instalacion_amphora_gemini_web_browser/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://librewolf.net/installation/debian/">librewolf.net - instrucciones de instalación - debian / ubuntu</a>  
<a href="https://librewolf.net/installation/linux/">librewolf.net - otros métodos de instalación - linux</a>  
<a href="https://gitlab.com/librewolf-community/browser/appimage/-/releases">librewolf releases (gitlab)</a>  
<a href="https://laboratoriolinux.es/index.php/-noticias-mundo-linux-/software/31078-librewolf-firefox-orientado-a-la-privacidad-y-seguridad.html">LaboratorioLinux</a>
<a href="https://itsfoss.com/librewolf/">ItsFoss</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

