---
layout: post
title: "[HowTo] Eliminar clave 'gpg' cuyo 'id' desconocemos. (Debian y derivados)"
date: 2021-06-28
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "add-apt-repository", "software-properties-common", "repositorios", "ppa", "sources.list", "apt", "aptitude", "debian", "gnu linux", "linux", "apt-key", "apt-key add", "apt-key del", "apt-key list", "hexagesimal"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>En Debian y derivados, así como en otras "familias" de GNU Linux, una de las formas de agregar la clave '.gpg' de un repositorio es mediante el comando ['apt-key'](https://manpages.ubuntu.com/manpages/xenial/man8/apt-key.8.html): <span style="background-color:#042206"><span style="color:lime">`sudo apt-key add`</span></span>.  
Para ver la lista de claves 'gpg' instaladas en el sistema basta con ejecutar el comando 'apt-key list': <span style="background-color:#042206"><span style="color:lime">`sudo apt-key list`</span></span>.</small>  

<br>
<big>**‼**</big> Sin embargo, en Debian 10 Buster o Ubuntu, suele ocurrir que en la lista de claves mostrada por el comando 'apt-key list' **no aparece explícito el 'id' de la clave**, necesario -entre otras cosas- para, llegado el caso, eliminarla:
~~~
pub   dsa1024 2009-10-31 [SC]
      375B 9363 E5C7 7BC2 A588  2FD5 83D6 5478 6C12 344E
uid        [desconocida] EDuke32 repository <zebioman@free.fr>
sub   elg4096 2009-10-31 [E]
~~~

<br>
En Debian 10, Ubuntu y otros sistemas GNU Linux que no muestran explícitamente el 'id' de las claves 'gpg' cuando se ejecuta `sudo apt-key list`, para averiguarlo basta con tomar los <big>**8 caracteres hexagesimales finales de la clave**</big>.  

<br>
Por consiguiente, en el ejemplo anterior -clave gpg del repositorio 'EDuke32'- el 'id' de la clave 'gpg' es **'6C12344E'** y, para eliminarla bastaría con ejecutar el comando <span style="background-color:#042206"><span style="color:lime">`sudo apt-key del` *id_de_la_clave_gpg*</span></span>:
~~~bash
sudo apt-key del 6C12344E
~~~

<br>
**ℹ** Es recomendable eliminar de '/etc/apt/sources.list' el repositorio cuya clave 'gpg' se acaba de eliminar [<small>`nano /etc/apt/sources.list`</small>] si se desea evitar que, cuando se actualice el contenido de los repositorios [<small>`sudo apt-get update`</small>], se produzca el poco relevante error *"The following signatures couldn't be verified because the public key is not available: NO_PUBKEY"*.

<br>
<br>
Entradas relacionadas:  
- [[HowTo] '/etc/apt/sources.list': repositorios en Debian y derivados](https://hijosdeinit.gitlab.io/howto_repositorios_sources.list_debian_y_derivados/)
- [[HowTo] Agregar repositorio 'debian unstable' a Debian 10 y similares](https://hijosdeinit.gitlab.io/howto_agregar_repositorio_debian_unstable_debian_y_derivados/)
- [[HowTo] Instalación de una versión más actualizada de un paquete desde los repositorios oficiales de Debian: 'backports'](https://hijosdeinit.gitlab.io/howto_instalacion_version_mas_actualizada_desde_repositorios_Debian_backports/)
- [[HowTo] Instalar el comando 'add-apt-repository' en Debian](https://hijosdeinit.gitlab.io/howto_add-apt-repository_en_debian_y_derivados/)
- [[HowTo] Eliminar repositorio agregado a mano ('add-apt-repository') en Debian y derivados](https://hijosdeinit.gitlab.io/howto_eliminar_repositorios_agregados_manualmente_addaptrepository_Debian_derivados/)
- [[HowTo] Solucionar error de GPG cuando la clave pública de un repositorio no está disponible. (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_solucionar_error_gpg_clave_publica_repositorio_Debian_y_derivados/)
- [El repositorio de firmware / drivers para GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- ['RaspBian' / 'RaspBerry OS' incorpora furtivamente -sin aviso ni permiso- un repositorio de 'Microsoft'. Así no vamos bien](https://hijosdeinit.gitlab.io/repositorio_microsoft_instalado_furtivamente_en_Raspbian_RaspBerryOs/)
- [[HowTo] Extirpar todo rastro del repositorio de MicroSoft que Raspbian / RaspBerry Os instala furtivamente](https://hijosdeinit.gitlab.io/howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://askubuntu.com/a/846877">Wesam en AskUbuntu</a>  
<a href="https://askubuntu.com/questions/107177/how-can-i-remove-gpg-key-that-i-added-using-apt-key-add">Corey en AskUbuntu</a>  
<a href="https://manpages.ubuntu.com/manpages/xenial/man8/apt-key.8.html">manpages.ubuntu.com</a></small>  


