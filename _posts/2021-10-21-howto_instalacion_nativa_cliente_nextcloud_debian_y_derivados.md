---
layout: post
title: "[HowTo] Instalación nativa del cliente oficial NextCloud en Debian 10 y derivados"
date: 2021-10-21
author: Termita
category: "redes"
tags: ["redes", "sistemas operativos", "nextcloud", "cliente", "nube", "redes", "sincronizacion", "backup", "respaldo", "gnu linux", "linux", "debian", "appimage", "nueva paquetería"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>NextCloud es una excelente "nube" personal que, una vez desplegada (generalmente en un servidor), permite a su dueño ser autosuficiente, desvincularse de las "nubes" públicas como 'Google Drive', 'DropBox', etcétera...</small>  

<br>
Se puede sincronizar con nuestro servidor NextCloud de diversas formas, tal como se señala en [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_Varios_metodos_acceso_Linux-cuenta_NextCloud/).  
Una de ellas es mediante el **cliente oficial** de NextCloud para nuestro sistema operativo. El cliente oficial es multiplataforma, es decir, [tiene versiones para GNU Linux, Windows, macOS, Android e IOS](https://nextcloud.com/install/#install-clients).  
<br>
No obstante, el cliente oficial para GNU Linux que se distribuye en la página oficial -valga la redundancia- de NextCloud es una aplicación ***'appimage'*** (<span>nueva paquetería, con una vocación de "universalidad"</span>).  
A muchos no nos gusta este tipo de paquetería y preferimos instalar las cosas **de forma NATIVA**, es decir, lo que en Debian y derivados se hace mediante el comando <span style="background-color:#042206"><span style="color:lime">`apt`</span></span>.  

<br>
## Procedimiento de instalación NATIVA del cliente oficial NextCloud en Debian 10 y derivados
~~~bash
sudo apt-get update
sudo apt install nextcloud-desktop
~~~
Se instalará así -desde los repositorios oficiales de Debian- una versión nativa del cliente NextCloud oficial, la última testeada por los desarrolladores de nuestro Debian (o derivado). Generalmente se trata de una versión antigua; generalmente no se puede disponer de todo en la vida, no hay luz sin sombra y yo prefiero sacrificar "estar al día". La versión 'appimage' del cliente que se descarga desde la página oficial de NextCloud SÍ es la última versión estable.  

<br>
**ℹ** Si el sistema no encontrara 'nextcloud-desktop' en los **repositorios oficiales de Debian**, es posible que previamente sea necesario agregar el **repositorio no-oficial** ('nextcloud-devs' que contiene la versión del cliente nextcloud instalable vía apt: <span style="background-color:#042206"><span style="color:lime">`sudo add-apt-repository ppa:nextcloud-devs/client`</span></span>  

<br>
**ℹ** Si se deseara instalar en Debian 10 *Buster* de forma nativa la **última versión** del cliente oficial de NextCloud lo mejor es **COMPILAR** el **código fuente** que se puede encontrar en el [repositorio oficial de NextCloud en GitHub](https://github.com/nextcloud/desktop) siguiendo [ESTAS INSTRUCCIONES](https://github.com/nextcloud/desktop/wiki).  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Varios métodos de acceso desde GNU Linux a cuenta NextCloud](https://hijosdeinit.gitlab.io/howto_Varios_metodos_acceso_Linux-cuenta_NextCloud/)
- [[HowTo] Acceso desde Nautilus mediante webdav a cuenta NextCloud](https://hijosdeinit.gitlab.io/howto_Acceso_desde_Nautilus_mediante-webdav_a_cuenta_NextCloud/)
- [[HowTo] Montar webdav desde línea de comandos en cliente GNU Linux](https://hijosdeinit.gitlab.io/howto_montar-webdav_desde_cli/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
