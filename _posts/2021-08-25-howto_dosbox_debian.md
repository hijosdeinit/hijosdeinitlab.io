---
layout: post
title: "[HowTo] 'dosbox' en Debian y derivados"
date: 2021-08-25
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "windows", "windows 10", "msdos", "dos", "virtualizacion", "maquinas virtuales", "disco duro virtual", "almacenamiento virtual", "virtual box", "vmware workstation", "vmware player", "qemu", "gnome boxes", "vmware esxxi", "vmware esx", "proxmox ve", "parallels desktop", "xen", "docker", "kubernetes", "podman", "wine", "dosbox", "dosemu", "dosemu2", "scummvm", "emulacion", "emulador"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Hace tiempo me planteé la posibilidad de utilizar mi procesador de texto favorito -*wordperfect 5.0*, un programa para 'MS DOS' de finales de los 80 y principios de los 90- en máquinas actuales y/o bajo GNU Linux. Una de las formas de llevar a cabo esto es mediante el empleo de emuladores de 'msdos' como 'DosBox' y 'DosEmu2'.  
[Sobre 'DosEmu2' existe un artículo en este blog](https://hijosdeinit.gitlab.io/howto_dosemu2_debian/).  
<small>(Otras opciones podrían consistir en crear máquinas virtuales de 'msdos' o 'Windows antiguos' mediante 'VMware', 'VirtualBox', 'Qemu', 'Gnome Boxes' y/o similares).</small>  

<br>
['DosBox'](https://www.dosbox.com/information.php?page=0) es un emulador de 'MS DOS' que ha sido portado a las más diversas plataformas: Windows, BeOS, Linux, MacOS, etcétera.  
Emula también 'CPU:286/386 *realmode*/*protected* mode', Directory FileSystem/XMS/EMS, gráficos Tandy/Hercules/CGA/EGA/VGA/VESA, tarjetas de sonido *SoundBlaster*/*Gravis Ultra* con una excelente compatibilidad con los juegos y programas más viejos.  
'DosBox' es de código abierto y totalmente libre.  

<br>
'DosBox' puede incorporarse a nuestro sistema obteniéndolo desde el [apartado de descargas oficiales de 'DosBox'](https://www.dosbox.com/download.php?main=1), aunque en los repositorios oficiales de Debian (y distiribuciones derivadas) existe una versión (no tan actualizada).  

<br>
<a href="/assets/img/2021-08-25-howto_dosbox_debian/wordperfect_en_dosbox.png" target="_blank"><img src="/assets/img/2021-08-25-howto_dosbox_debian/wordperfect_en_dosbox.png" alt="'WordPerfect 5.0' para MSDOS, en GNU Linux mediante 'DosBox'" width="800"/></a>  
<small>'WordPerfect 5.0' para MSDOS, en GNU Linux mediante 'DosBox'</small>  
<br>

<br>
## Instalar 'DosBox' en Debian 10 *buster* y derivados
~~~bash
sudo apt-get update
sudo apt install dosbox
~~~

<br>
Consulto la versión de 'DosBox' instalada:
~~~bash
dosbox --version
~~~
~~~
DOSBox version 0.74-2, copyright 2002-2018 DOSBox Team.

DOSBox is written by the DOSBox Team (See AUTHORS file))
DOSBox comes with ABSOLUTELY NO WARRANTY.  This is free software,
and you are welcome to redistribute it under certain conditions;
please read the COPYING file thoroughly before doing so.
~~~

<br>
## Configurar 'DosBox' en Debian y derivados
La configuración de 'DosBox 0.74-2' está en el archivo ['/home/*usuario*/.dosbox/dosbox-0.74-2.conf'](https://www.dosbox.com/wiki/Dosbox.conf).  
Por ejemplo, para utilizar 'wordperfect', hice copia de seguridad de  '/home/*usuario*/.dosbox/dosbox-0.74-2.conf' <small>[<span style="background-color:#042206"><span style="color:lime">`sudo cp /home/*usuario*/.dosbox/dosbox-0.74-2.conf /home/*usuario*/.dosbox/dosbox-0.74-2.conf.bkp001`</span></span>]</small> y configuré algunos [parámetros](https://www.dosbox.com/wiki/Dosbox.conf) así:
~~~
# This is the configuration file for DOSBox 0.74-2. (Please use the latest version of DOSBox)
# Lines starting with a # are comment lines and are ignored by DOSBox.
# They are used to (briefly) document the effect of each option.

[sdl]
#       fullscreen: Start dosbox directly in fullscreen. (Press ALT-Enter to go back)
#       fulldouble: Use double buffering in fullscreen. It can reduce screen flickering, but it can also result in a slow DOSBox.
#   fullresolution: What resolution to use for fullscreen: original, desktop or fixed size (e.g. 1024x768).
#                     Using your monitor's native resolution (desktop) with aspect=true might give the best results.
#                     If you end up with small window on a large screen, try an output different from surface.
#                     On Windows 10 with display scaling (Scale and layout) set to a value above 100%, it is recommended
#                     to use a lower full/windowresolution, in order to avoid window size problems.
# windowresolution: Scale the window to this size IF the output device supports hardware scaling.
#                     (output=surface does not!)
#           output: What video system to use for output.
#                   Possible values: surface, overlay, opengl, openglnb.
#         autolock: Mouse will automatically lock, if you click on the screen. (Press CTRL-F10 to unlock)
#      sensitivity: Mouse sensitivity.
#      waitonerror: Wait before closing the console if dosbox has an error.
#         priority: Priority levels for dosbox. Second entry behind the comma is for when dosbox is not focused/minimized.
#                     pause is only valid for the second entry.
#                   Possible values: lowest, lower, normal, higher, highest, pause.
#       mapperfile: File used to load/save the key/event mappings from. Resetmapper only works with the defaul value.
#     usescancodes: Avoid usage of symkeys, might not work on all operating systems.

fullscreen=false
#fullscreen=true
fulldouble=false
#fullresolution=original
fullresolution=desktop
#windowresolution=original
windowresolution=1600x900
#output=surface
output=overlay
autolock=true
sensitivity=100
waitonerror=true
priority=higher,normal
mapperfile=mapper-0.74-2.map
usescancodes=true

[dosbox]
# language: Select another language file.
#  machine: The type of machine DOSBox tries to emulate.
#           Possible values: hercules, cga, tandy, pcjr, ega, vgaonly, svga_s3, svga_et3000, svga_et4000, svga_paradise, vesa_nolfb, vesa_oldvbe.
# captures: Directory where things like wave, midi, screenshot get captured.
#  memsize: Amount of memory DOSBox has in megabytes.
#             This value is best left at its default to avoid problems with some games,
#             though few games might require a higher value.
#             There is generally no speed advantage when raising this value.

language=
machine=svga_s3
captures=capture
memsize=16

[render]
# frameskip: How many frames DOSBox skips before drawing one.
#    aspect: Do aspect correction, if your output method doesn't support scaling this can slow things down!
#    scaler: Scaler used to enlarge/enhance low resolution modes. If 'forced' is appended,
#              then the scaler will be used even if the result might not be desired.
#              To fit a scaler in the resolution used at full screen may require a border or side bars,
#              to fill the screen entirely, depending on your hardware, a different scaler/fullresolution might work.
#            Possible values: none, normal2x, normal3x, advmame2x, advmame3x, advinterp2x, advinterp3x, hq2x, hq3x, 2xsai, super2xsai, supereagle, tv2x, tv3x, rgb2x, rgb3x, scan2x, scan3x.

frameskip=0
#aspect=false
aspect=true
scaler=normal2x

[cpu]
#      core: CPU Core used in emulation. auto will switch to dynamic if available and
#            appropriate.
#            Possible values: auto, dynamic, normal, simple.
#   cputype: CPU Type used in emulation. auto is the fastest choice.
#            Possible values: auto, 386, 386_slow, 486_slow, pentium_slow, 386_prefetch.
#    cycles: Amount of instructions DOSBox tries to emulate each millisecond.
#            Setting this value too high results in sound dropouts and lags.
#            Cycles can be set in 3 ways:
#              'auto'          tries to guess what a game needs.
#                              It usually works, but can fail for certain games.
#              'fixed #number' will set a fixed amount of cycles. This is what you usually
#                              need if 'auto' fails. (Example: fixed 4000).
#              'max'           will allocate as much cycles as your computer is able to
#                              handle.
#            Possible values: auto, fixed, max.
#   cycleup: Amount of cycles to decrease/increase with keycombos.(CTRL-F11/CTRL-F12)
# cycledown: Setting it lower than 100 will be a percentage.

core=auto
cputype=auto
cycles=auto
cycleup=10
cycledown=20

[mixer]
#   nosound: Enable silent mode, sound is still emulated though.
#      rate: Mixer sample rate, setting any device's rate higher than this will probably lower their sound quality.
#            Possible values: 44100, 48000, 32000, 22050, 16000, 11025, 8000, 49716.
# blocksize: Mixer block size, larger blocks might help sound stuttering but sound will also be more lagged.
#            Possible values: 1024, 2048, 4096, 8192, 512, 256.
# prebuffer: How many milliseconds of data to keep on top of the blocksize.

nosound=false
rate=44100
blocksize=1024
prebuffer=25

[midi]
#     mpu401: Type of MPU-401 to emulate.
#             Possible values: intelligent, uart, none.
# mididevice: Device that will receive the MIDI data from MPU-401.
#             Possible values: default, win32, alsa, oss, coreaudio, coremidi, none.
# midiconfig: Special configuration options for the device driver. This is usually the id of the device you want to use
#               (find the id with mixer/listmidi).
#               Or in the case of coreaudio, you can specify a soundfont here.
#               See the README/Manual for more details.

mpu401=intelligent
mididevice=default
midiconfig=

[sblaster]
#  sbtype: Type of Soundblaster to emulate. gb is Gameblaster.
#          Possible values: sb1, sb2, sbpro1, sbpro2, sb16, gb, none.
#  sbbase: The IO address of the soundblaster.
#          Possible values: 220, 240, 260, 280, 2a0, 2c0, 2e0, 300.
#     irq: The IRQ number of the soundblaster.
#          Possible values: 7, 5, 3, 9, 10, 11, 12.
#     dma: The DMA number of the soundblaster.
#          Possible values: 1, 5, 0, 3, 6, 7.
#    hdma: The High DMA number of the soundblaster.
#          Possible values: 1, 5, 0, 3, 6, 7.
# sbmixer: Allow the soundblaster mixer to modify the DOSBox mixer.
# oplmode: Type of OPL emulation. On 'auto' the mode is determined by sblaster type. All OPL modes are Adlib-compatible, except for 'cms'.
#          Possible values: auto, cms, opl2, dualopl2, opl3, none.
#  oplemu: Provider for the OPL emulation. compat might provide better quality (see oplrate as well).
#          Possible values: default, compat, fast.
# oplrate: Sample rate of OPL music emulation. Use 49716 for highest quality (set the mixer rate accordingly).
#          Possible values: 44100, 49716, 48000, 32000, 22050, 16000, 11025, 8000.

sbtype=sb16
sbbase=220
irq=7
dma=1
hdma=5
sbmixer=true
oplmode=auto
oplemu=default
oplrate=44100

[gus]
#      gus: Enable the Gravis Ultrasound emulation.
#  gusrate: Sample rate of Ultrasound emulation.
#           Possible values: 44100, 48000, 32000, 22050, 16000, 11025, 8000, 49716.
#  gusbase: The IO base address of the Gravis Ultrasound.
#           Possible values: 240, 220, 260, 280, 2a0, 2c0, 2e0, 300.
#   gusirq: The IRQ number of the Gravis Ultrasound.
#           Possible values: 5, 3, 7, 9, 10, 11, 12.
#   gusdma: The DMA channel of the Gravis Ultrasound.
#           Possible values: 3, 0, 1, 5, 6, 7.
# ultradir: Path to Ultrasound directory. In this directory
#           there should be a MIDI directory that contains
#           the patch files for GUS playback. Patch sets used
#           with Timidity should work fine.

gus=false
gusrate=44100
gusbase=240
gusirq=5
gusdma=3
ultradir=C:\ULTRASND

[speaker]
# pcspeaker: Enable PC-Speaker emulation.
#    pcrate: Sample rate of the PC-Speaker sound generation.
#            Possible values: 44100, 48000, 32000, 22050, 16000, 11025, 8000, 49716.
#     tandy: Enable Tandy Sound System emulation. For 'auto', emulation is present only if machine is set to 'tandy'.
#            Possible values: auto, on, off.
# tandyrate: Sample rate of the Tandy 3-Voice generation.
#            Possible values: 44100, 48000, 32000, 22050, 16000, 11025, 8000, 49716.
#    disney: Enable Disney Sound Source emulation. (Covox Voice Master and Speech Thing compatible).

pcspeaker=true
pcrate=44100
tandy=auto
tandyrate=44100
disney=true

[joystick]
# joysticktype: Type of joystick to emulate: auto (default), none,
#               2axis (supports two joysticks),
#               4axis (supports one joystick, first joystick used),
#               4axis_2 (supports one joystick, second joystick used),
#               fcs (Thrustmaster), ch (CH Flightstick).
#               none disables joystick emulation.
#               auto chooses emulation depending on real joystick(s).
#               (Remember to reset dosbox's mapperfile if you saved it earlier)
#               Possible values: auto, 2axis, 4axis, 4axis_2, fcs, ch, none.
#        timed: enable timed intervals for axis. Experiment with this option, if your joystick drifts (away).
#     autofire: continuously fires as long as you keep the button pressed.
#       swap34: swap the 3rd and the 4th axis. Can be useful for certain joysticks.
#   buttonwrap: enable button wrapping at the number of emulated buttons.

joysticktype=auto
timed=true
autofire=false
swap34=false
buttonwrap=false

[serial]
# serial1: set type of device connected to com port.
#          Can be disabled, dummy, modem, nullmodem, directserial.
#          Additional parameters must be in the same line in the form of
#          parameter:value. Parameter for all types is irq (optional).
#          for directserial: realport (required), rxdelay (optional).
#                           (realport:COM1 realport:ttyS0).
#          for modem: listenport (optional).
#          for nullmodem: server, rxdelay, txdelay, telnet, usedtr,
#                         transparent, port, inhsocket (all optional).
#          Example: serial1=modem listenport:5000
#          Possible values: dummy, disabled, modem, nullmodem, directserial.
# serial2: see serial1
#          Possible values: dummy, disabled, modem, nullmodem, directserial.
# serial3: see serial1
#          Possible values: dummy, disabled, modem, nullmodem, directserial.
# serial4: see serial1
#          Possible values: dummy, disabled, modem, nullmodem, directserial.

serial1=dummy
serial2=dummy
serial3=disabled
serial4=disabled

[dos]
#            xms: Enable XMS support.
#            ems: Enable EMS support.
#            umb: Enable UMB support.
# keyboardlayout: Language code of the keyboard layout (or none).

xms=true
ems=true
umb=true
keyboardlayout=auto

[ipx]
# ipx: Enable ipx over UDP/IP emulation.

ipx=false

[autoexec]
# Lines in this section will be run at startup.
# You can put your MOUNT lines here.
keyb sp
mount c /home/miusuario/Programas/ProgramasDOS
PATH C:\WP51\
~~~ 

<br>
## Arrancar 'DosBox' y operaciones habituales
Para arrancar 'DosBox':
~~~bash
dosbox
~~~

Manual / Ayuda de 'DosBox':
~~~bash
man dosbox
~~~

Montar un directorio de la máquina "anfitrión" (GNU Linux Debian), por ejemplo '/home/*miusuario*/Programas/ProgramasDOS': <span style="background-color:#042206"><span style="color:lime">`mount C /home/miusuario/Programas/ProgramasDOS`</span></span>  

Establecer teclado español
~~~
keyb sp
~~~

Atajos de teclado:  
CTRL + F12: Aumentar los ciclos. (Hasta el máximo que soporte el equipo)  
CTRL + F11: Disminuir los ciclos. (Llega hasta 1)  
CTRL + F7: Disminuye el *frameskip* (Mínimo 0)  
CTRL + F8: Aumenta el *frameskip* (Maximo 10)  
ALT + Enter: Activar Pantalla Completa. Para salir, utiliza el mismo comando.  
CTRL + F1: Muestra la pantalla de configuración del mapa de teclado.  
CTRL + F4: Recarga el directorio montado. Útil si se ha incluido ficheros después de arrancar 'DOSBox'.  
CTRL + F5: Guarda un pantallazo y lo guarda en el directorio 'capture'  
CTRL + ALT + F5: Inicia o detiene la grabación en formato AVI de lo sucedido en la máquina virtual. <small>Atención con este paso, solo se recomienda el uso a usuarios ya avanzados.</small>  
CTRL + F6: Inicia o detiene la grabación en formato WAV del sonido.  
CTRL + F9: Cierra inmediatamente 'DOSBox'.  
CTRL + F10: Captura o suelta el foco del ratón de la máquina virtual.  

<br>
<big>**!**</big>  
Los atajos de teclado propios de 'DosBox' pueden interferir con los atajos de teclado de las aplicaciones que corramos en él -por ejemplo 'WordPerfect'- o bien con los atajos de teclado del propio sistema operativo anfitrión.  
En caso de interferencia hay que configurar atajos nuevos para 'DosBox'. Generalmente basta con cambiar las teclas 'meta', que por defecto son 'ctrl' y 'alt', por otras "más raras".  
Cuando se personalizan los atajos de teclado de 'DosBox' se crea un archivo: '/home/*tuusuario*/.dosbox/mapper-0.74-2.map'. Borrando dicho archivo se vuelve a la configuración por defecto de los atajos de teclado de 'DosBox'.  

<br>
<br>
Entradas relacionadas:  
- [](https://hijosdeinit.gitlab.io/howto_dosemu2_debian/)
- [Software de virtualización (máquinas virtuales)](https://hijosdeinit.gitlab.io/utilidades_maquinas_virtuales/)
- [[HowTo] cygwin: GNU Linux en Windows](https://hijosdeinit.gitlab.io/howto_cygwin_GNU_Linux_en_Windows/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.linuxadictos.com/dosbox-en-linux.html">LinuxAdictos - 'DosBox' en GNU Linux</a>  
<a href="https://www.adslzone.net/foro/videojuegos-y-consolas.58/tutorial-dosbox-configuracion-emulador-ms-dos.172851/">jose-X-lito en foro AdslZone - configuracion 'DosBox'</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
