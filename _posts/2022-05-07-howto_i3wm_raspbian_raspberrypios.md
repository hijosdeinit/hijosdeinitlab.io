---
layout: post
title: "[HowTo] Instalación de 'i3wm' en 'RaspBian' o 'RaspBerry Pi OS' vía repositorio oficial"
date: 2022-05-07
author: Termita
category: "sistemas operativos"
tags: ["raspbian", "raspberry pi os", "raspios", "debian", "i3wm", "i3status", "i3lock", "dmenu", "window manager", "sistemas operativos", "gnu linux", "linux", "escritorio", "interfaz grafico", "desktop", "gui"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small><small>Entorno: RaspBerry Pi 3b, RaspBerry Pi OS x64 desktop</small></small>  

<br>
<br>
RaspBerry Pi 3b es una máquina con unas características de hardware bastante limitadas.  

El gestor de ventanas que incorpora por defecto la versión con escritorio de 'RaspBerry OS' -antaño 'RaspBian'- es ligero mas, existiendo gestores de ventanas más livianos aún, sustituirlo por 'i3wm' repercutirá en un menor consumo de recursos.  

<br>
## Instalar 'i3' en 'RaspBerry OS'
~~~bash
sudo apt-get update
sudo apt install i3
~~~

<br>
En otros sistemas bastaría con hacer eso y seleccionar en la pantalla de login el gestor de ventanas 'i3'.  
Sin embargo, la pantalla de login de 'RaspBerry OS' NO permite elegir con qué gestor de ventanas queremos trabajar en la sesión. Para solucionar esto ha que crear / editar el archivo '~/.xinitrc'
~~~bash
nano ~/.xinitrc
~~~
... e insertar esta línea
> exec i3  

La próxima vez que iniciemos sesión (o que ejecutemos desde línea de comandos pura `startx`) el gestor de ventanas será 'i3'.

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Configuración de 'i3wm', vol.02: los nombres de las teclas](https://hijosdeinit.gitlab.io/howto_configuracion_i3wm_vol02__nombres_teclas/)
- [[HowTo] Configuración de 'i3wm', vol.01: Cambiar el fondo de pantalla (wallpaper) en OpenBox, i3wm, etc... sin software extra, bajo Debian y derivados](https://hijosdeinit.gitlab.io/howto_cambiar_fondopantalla_Debian_openbox_i3wm/)  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  
<br>
<br>
<br>
<br>
<br>
<br>
