---
layout: post
title: "Releases de NextCloud"
date: 2021-01-15
author: Termita
category: "sistemas operativos"
tags: ["nextcloud", "releases", "changelog", "nextcloudpi", "versiones", "nube", "servidor", "linux", "sistemas operativos"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
[*Changelog*. Todas las versiones estables de NextCloud](https://nextcloud.com/changelog/)  
Se pueden consultar y descargar  

<br>
[*Release Channels*. Índice explicativo de todas las versiones](https://nextcloud.com/release-channels/)  
Se pueden consultar y descargar  
A grandes rasgos hay 4 canales de *releases* de Nextcloud:
- *Production*: canal utilizado para suministrar la versión de 'NextCloud Enterprise' a los clientes. Es una versión especial sólo para clientes.
- *Stable*: canal que suministra la última versión de NextCloud "estable", es decir, con un riesgo mínimo de fallo. «Note that Nextcloud does staged rollouts, making releases incrementally available to the user base over time. We are very careful about new major releases, usually only making them available after the first minor bug fix release. To update sooner to the new version, use the Beta channel. Around big releases the Beta channel delivers the new major version.»
- *Beta*: «Betas and release candidates are the versions of choice for testers in preparation for an Nextcloud release. They provide a short-term-stable point to report bugs against. Grab these to make sure the upcoming release works well on your infrastructure. Automatically generated snapshot tarballs and betas track daily Nextcloud development. These are suitable to check if bugs are fixed but can break any time.»
- *Daily*: «Daily builds are meant to reproduce bugs as well as to enable testers to follow the development of the next major version.»  

<br>
<br>
<small>«With millions of Nextcloud users, there are a wide variety of needs and requirements for a private cloud server. Release Channels allow you to pick a versioning strategy that fits you.  
You can choose a channel of your preference in the Administrator settings in your Nextcloud instance if you have installed Nextcloud from an archive. Learn more about the release channels below.  
NOTE that you can only upgrade to a newer version. Skipping major versions when upgrading and downgrading to older versions is not supported by Nextcloud. If you went via 'beta' to 14.0.0rc4 and stable is on 13.0.5, you have to wait until 14.0.0 or later is in stable until a new update will become available.</small>  

<small>Where is my new stable release?  
Nextcloud makes new versions incrementally available to user installations in the Stable channel. When a major new version comes out, we wait about one week and only when no problems are found we start the roll out in steps of about 20% of our user base per week. If you maintain several servers, this means some get it sooner than others. Sometimes, an issue is found that was not caught during our pre-release testing, so we delay the roll-out until the fix is available. As a result new release is typically only available in the stable channel after the first minor release. Sometimes it can take even a bit longer. Users can always upgrade sooner by choosing the beta channel, which typically tracks stable releases immediately after publishing.</small>  

<small>After upgrading to the new major release from the Beta channel, you can switch back to stable where you will be notified of minor releases as usual.</small>  

<small>If you are looking for our release- and maintenance schedule, you can find that in Github.</small>  

<small>Note that, of course, we do not release unstable software. We spend a third of our development cycle on testing and only release Nextcloud if we and our paid and volunteer testers are unable to find any more issues. Unfortunately, Nextcloud is big and complicated software and with hundreds of apps users can install, Nextcloud can be used in thousands of different ways, on different platforms and with different configurations. We encourage users to join our testing efforts, trying out beta releases and release candidates so we can fix issues before release. Only testing YOUR use case can guarantee it will be perfect for you!»</small>  

<br>
<small>«If you are looking to grab an archive of a specific version you can find links to them in our changelog.  
Note that releases don't show up in the updater app right away. We usually stagger releases out to watch the impact and hold off in case very serious problems pop up. In practice, most bugfix releases are available within a week, major releases to a proportion of the users on release day in the Stable channel; we will increase that percentage usually to 100% after the first bugfix release.Packages for the Release channels might be available in distributions, docker images, Snap and so on, this is up to the packagers and communities developing for those platforms.  
To upgrade in the safest way possible, always update to the latest minor release before upgrading to a new version. As an extreme example, to upgrade from 9 all the way to 12.0.5, upgrade 9.0.x to 9.0.6, then upgrade to 10.0.6, 11.0.7, then 12.0.5. We try to set up our updater to do this automatically for you whenever possible.»</small>  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.atareao.es/ubuntu/que-hacer-cuando-no-funciona-youtube-dl-en-ubuntu/">Atareao</a>  
<br>
<br>
<br>
<br>
<br>
<br>
