---
layout: post
title: "[HowTo] Instalación de 'unrar-nonfree' en Ubuntu 20.04 *focal* 64bits"
date: 2021-11-07
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "ubuntu", "ubuntu 20", "focal", "unrar", "rar", "compresion", "unrar-free", "unrar-nonfree", "tar", "gzip", "p7zip", "p7zip-full", "zip", "unzip", "unace", "bzip2", "arj", "lzip", "lzma", "gzip", "unar"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno: cpu Intel® Core™ i7-8750H CPU @ 2.20GHz × 12, 16gb RAM, gpu NVIDIA GeForce GTX 1050 Mobile, Ubuntu 20.04.3 LTS x86_64, kernel 5.11.0-38-generic #42~20.04.1-Ubuntu SMP Tue Sep 28 20:41:07 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux</small>  

<br>
Es conveniente, en mi caso, tener más formatos de compresión de los que trae por defecto nuestra distribución GNU Linux.  
Uno de los soportes de compresión más interesantes es el formato 'rar', aunque éste no es libre.  

<br>
¿Por qué instalo una versión 'no libre' ([*'non free'*](https://packages.ubuntu.com/source/focal/unrar-nonfree))?  
Porque la versión libre -'free'- no va tan bien como la privativa -'non-free'-.  
En mi ubuntu 20.04 'unrar-free' no está instalado por defecto: no hará falta desinstalarlo previamente mediante `sudo apt remove --purge unrar-free`. A diferencia de RaspBian -y de Debian, creo-, donde hay que instalar 'unrar-nonfree' a partir del código fuente, en Ubuntu 20.04 si consultamos *caché* de `apt` observamos que el paquete 'unrar' en realidad es la versión ['unrar-nonfree'](https://github.com/debian-calibre/unrar-nonfree), <small>**siempre y cuando el repositorio 'multiverse' esté activado en '/etc/apt/sources.list'**</small>.:
~~~bash
apt-cache show unrar
~~~
~~~
Package: unrar
Architecture: amd64
Version: 1:5.6.6-2build1
Priority: optional
Section: multiverse/utils
Source: unrar-nonfree
Origin: Ubuntu
Maintainer: Ubuntu Developers <ubuntu-devel-discuss@lists.ubuntu.com>
Original-Maintainer: Martin Meredith <mez@debian.org>
Bugs: https://bugs.launchpad.net/ubuntu/+filebug
Installed-Size: 396
Depends: libc6 (>= 2.14), libgcc-s1 (>= 3.0), libstdc++6 (>= 5)
Conflicts: rar (<= 2.60-1)
Filename: pool/multiverse/u/unrar-nonfree/unrar_5.6.6-2build1_amd64.deb
Size: 113316
MD5sum: 5ec74cce263430f0bbd9ae43049b8d76SHA1: 69bd0f63b245f7b7edc5723674ab8cb07e7836f4SHA256: 31cc5e16f915a29a96d083238f071852adc52a2acacdce57cd8176d7fd1407ef
Homepage: http://www.rarlabs.com/
Description-es: Extractor para archivos .rar (versión no libre) Unrar puede extraer archivos de archivadores .rar. Si quiere crear archivadores .rar, instale el paquete rar.
Description-md5: 363f696bb115028ffafb265ccc9f32fb
~~~

<br>
## Instalación de 'unrar-nonfree'
~~~bash
sudo apt-get update
sudo apt install unrar
~~~
**siempre y cuando el repositorio 'multiverse' esté activado en '/etc/apt/sources.list'**.  
Se instalará así 'unrar-nonfree' y no 'unrar-free'.  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
