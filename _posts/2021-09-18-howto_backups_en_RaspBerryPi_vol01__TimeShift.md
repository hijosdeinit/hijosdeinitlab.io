---
layout: post
title: "[HowTo] Backups en RaspBerryPi, vol.01: 'TimeShift'"
date: 2021-09-18
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "tar", "comprimir", "empaquetar", "respaldo", "backup", "copia de seguridad", "compresores", "gzip", "rar", "unrar", "7zip", "lzma", "zip", "unzip", "arj", "log", "registro", "registro de errores", "actividad", "entrada", "salida", "input", "output", "rsync", "raspberry pi", "raspberry pi os", "raspbian", "armhf", "arm64", "timeshift", "borg backup"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## Antecedentes
<small>Desde que comencé a trastear con RaspBerry Pi los respaldos adquirieron un valor fundamental, más que con otras arquitecturas y sistemas.  
Generalmente al principio suele emplearse el soporte SD para almacenar el sistema operativo y los datos, y es un soporte frágil, que se deteriora rápidamente, con todos los incovenientes que eso conlleva. Luego, enseguida -en muchas ocasiones por la vía tremenda, aprendiendo a base de golpes- se incorpora un dispositivo de almacenamiento USB para arrancar y almacenar los datos, cosa que dota al sistema de mayor fiabilidad. Mas, tratándose de un entorno de experimentación, nunca está uno plenamente a salvo de fallos, sobre todo humanos.  
Para ahorrar tiempo y esfuerzo, la copia de seguridad -o *backup*, o respaldo, que mil nombres recibe la misma cosa- frecuente y sistemática es fundamental.  
He hecho respaldos en caliente, sobre todo mediante 'dd' y 'rsync', respaldos con máquina apagada, conectando el dispositivo de almacenamiento a otra computadora y respaldando mediante 'dd' o 'Acronis TrueImage', he hecho respaldos totales, respaldos incrementales...  
Mas, aún conociendo la existencia -que no el funcionamiento- de herramientas excelentes como ['Borg Backup'](https://www.borgbackup.org/), ['rdiff-backup'](https://rdiff-backup.net/), ['Deja Dup'](), ['rsnapshot'](http://www.rsnapshot.org/), ['BackInTime'](https://github.com/bit-team/backintime), ['LuckyBackup](http://luckybackup.sourceforge.net/), ['TimeVault'](https://wiki.ubuntu.com/TimeVault) o ['Restic'](https://restic.net/), siempre eché de menos disponer de **'TimeShift'** en RaspBerry Pi. Porque, lamentablemente, hasta hace bien poco, 'TimeShift' **NO** era desarrollado para arquitecturas 'ARM' como es el caso de RaspBerry Pi.</small>  

<br>
<a href="/assets/img/2021-09-18-howto_backups_en_RaspBerryPi_vol01__TimeShift/timeshift.png" target="_blank"><img src="/assets/img/2021-09-18-howto_backups_en_RaspBerryPi_vol01__TimeShift/timeshift.png" alt="logo de 'TimeShift'" width="300"/></a>  
<br>

<br>
## 'TimeShift' para arquitecturas armhf y arm64
Hoy he tenido conocimiento de que [<big>TimeShift</big>](https://teejeetech.com/timeshift/) tiene versión para arquitecturas 'armhf' y 'arm64'. Excelente noticia y, para no desaprovecharla, he procedido a instalar en mi RaspBerry Pi 3b esa fantástica herramienta que en PC tantas veces me ha sacado de apuros.  
'TimeShift' es un software para hacer copias de seguridad incrementales de los sistemas GNU Linux. Funciona tanto desde el entorno gráfico como desde línea de comandos, emulada o pura y dura. Permite retornar al punto de restauración que se desee, cosa muy útil cuando tanto va el cántaro a la fuente que éste se acaba rompiendo.  

<br>
<small>**ENTORNO**  
RaspBerry Pi 3b
Sistema Operativo: GNU Linux RaspBerry Pi OS (*Buster* 2021-05-07), versión con escritorio "mínimo"
Arranque y Almacenamiento: dispositivo de almacenamiento USB 64gb particionado en 3 particiones: partición 1 [FAT (boot)] | partición 2 [ext4 (rootfs)] | partición 3 [ext4 (omvprobe)]</small>  

<br>
<br>
## Instalación de 'Timeshift' armhf en RaspBerry OS
<small>['RaspBerry Pi OS'](https://www.raspberrypi.org/software/) es el sistema operativo oficial de la 'Fundación RaspBerry' para 'RaspBerry Pi'. Es un derivado de GNU Linux 'Debian', como lo era el antecesor de 'RaspBerry Pi OS': 'Raspbian'.</small>  
<br>
El procedimiento de instalación de la versión de 'TimeShift' para arquitecturas 'armhf' o 'arm64' en 'RaspBerry Pi OS' es sencillo:  

<br>
### 1. Descárguese la última versión estable
En el apartado ['Releases'](https://github.com/teejee2008/timeshift/releases) del [repositorio de 'TimeShift' en GitHub](https://github.com/teejee2008/timeshift) escójase la versión que se desee, siempre y cuando sea para arquitecturas 'armhf' o 'arm64'. Quizás lo recomendable es escoger la última versión estable.  
En estas fechas la última versión estable es la [20.11.1](https://github.com/teejee2008/timeshift/releases/tag/v20.11.1). Como RaspBerry Pi 3b es arquitectura arm, no arm64, descárguese el archivo de instalación '.deb' correspondiente:
~~~bash
wget 'https://github.com/teejee2008/timeshift/releases/download/v20.11.1/timeshift_20.11.1_armhf.deb'
~~~

<br>
### 2. Instálense las dependencias
<small>Podría ir a lo bruto -saltarme este paso- e intalar el paquete '.deb' de 'TimeShift' descargado sabiendo que se produciría un error en la instalación por carecer aún el sistema de las dependencias requeridas ['libgee-0.8-2' (>= 0.8.3), 'btrfs-progs' y 'btrfs-tools'] que subsanaría posteriormente ejecutando <span style="background-color:#042206"><span style="color:lime">`sudo apt --fix-broken install`</span></span> y luego, de nuevo, ejecutando la instalación del mencionado paquete.  
Mas vale prevenir que curar; hagamos las cosas bien desde el principio, instálense primero las dependencias:</small>
~~~bash
sudo apt-get update
sudo apt install libgee-0.8-2
sudo apt install btrfs-progs
sudo apt install btrfs-tools
~~~

<br>
### 3. Instálese el paquete de 'TimeShift' anteriormente descargado
~~~bash
sudo dpkg -i timeshift_20.11.1_armhf.deb
~~~

<br>
Una vez instalado, 'TimeShift' puede ejecutarse gráficamente (<small>menú → 'Herramientas del sistema' → 'TimeShift'</small>) o bien desde la Terminal.  
Desde línea de comandos -la terminal-, ejecutando <span style="background-color:#042206"><span style="color:lime">`timeshift --help`</span></span>, se pueden consultar los diferentes parámetros, que son bien sencillos:
~~~
Timeshift v20.11.1 by Tony George (teejeetech@gmail.com)

Syntax:
  timeshift --check
  timeshift --create [OPTIONS]
  timeshift --restore [OPTIONS]
  timeshift --delete-[all] [OPTIONS]
  timeshift --list-{snapshots|devices} [OPTIONS]

Options:

List:
  --list[-snapshots]         List snapshots
  --list-devices             List devices

Backup:
  --check                    Create snapshot if scheduled
  --create                   Create snapshot (even if not scheduled)
  --comments <string>        Set snapshot description
  --tags {O,B,H,D,W,M}       Add tags to snapshot (default: O)

Restore:
  --restore                  Restore snapshot
  --clone                    Clone current system
  --snapshot <name>          Specify snapshot to restore
  --target[-device] <device> Specify target device
  --grub[-device] <device>   Specify device for installing GRUB2 bootloader
  --skip-grub                Skip GRUB2 reinstall

Delete:
  --delete                   Delete snapshot
  --delete-all               Delete all snapshots

Global:
  --snapshot-device <device> Specify backup device (default: config)
  --yes                      Answer YES to all confirmation prompts
  --btrfs                    Switch to BTRFS mode (default: config)
  --rsync                    Switch to RSYNC mode (default: config)
  --debug                    Show additional debug messages
  --verbose                  Show rsync output (default)
  --quiet                    Hide rsync output
  --scripted                 Run in non-interactive mode
  --help                     Show all options

Examples:

timeshift --list
timeshift --list --snapshot-device /dev/sda1
timeshift --create --comments "after update" --tags D
timeshift --restore 
timeshift --restore --snapshot '2014-10-12_16-29-08' --target /dev/sda1
timeshift --delete  --snapshot '2014-10-12_16-29-08'
timeshift --delete-all 

Notes:

  1) --create will always create a new snapshot
  2) --check will create a snapshot only if a scheduled snapshot is due
  3) Use --restore without other options to select options interactively
  4) UUID can be specified instead of device name
  5) Default values will be loaded from app config if options are not specified
~~~

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Instalación de 'timeshift' en Debian y derivados](https://hijosdeinit.gitlab.io/howto_timeshift_debian/)
- [[HowTo] Transferir un backup completo de un sistema GNU Linux a un disco NTFS](https://hijosdeinit.gitlab.io/howto-backup_sistema_gnulinux_a_almacenamiento_ntfs/)
- [[HowTo] Mostrar el progreso de un backup con 'dd'](https://hijosdeinit.gitlab.io/howto_dd_progreso_verbose/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="">zzz</a>  
<a href="">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
