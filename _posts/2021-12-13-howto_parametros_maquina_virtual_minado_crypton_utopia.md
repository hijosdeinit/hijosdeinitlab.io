---
layout: post
title: "[HowTo] Parámetros de las máquinas virtuales Debian para minar crypton (utopia)"
date: 2021-12-13
author: Termita
category: "redes"
tags: ["redes", "internet 4.0", "web", "gopher", "gemini", "utopia", "crypton", "bitcoin", "criptodivisas", "minado", "sistemas operativos", "memoria", "ram", "memoria ram", "seguridad", "maquina virtual", "sandbox", "desconfianza", "liveusb", "maquina virtual", "virtualizacion", "privacidad", "docker", "uam", "virtualbox", "vmware", "docker", "bridge", "puente", "upnp"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
(sin docker, es decir 1 bot minero por máquina virtual)  

<br>
cpu: 4 núcleos  
ram: 4272 mb.  
hdd virtual: 8 gb. (7gb. primaria,ext4 | 1gb. lógica,swap)  
s.o. máquina: Debian 10 *buster* ô Debian 11 *bullseye*  
red: bridged (puente), permitir promiscuidad: toda.  

<br>
sistema operativo anfitrión: Windows 10.  
<br>
**NO HE LOGRADO AÚN HACER FUNCIONAR CORRECTAMENTE EL BOT MINERO EN UNA MÁQUINA VIRTUAL CON LINUX COMO ANFITRIÓN PORQUE NO HE LOGRADO -AÚN- HACER FUNCIONAR LA CONEXIÓN DE RED PUENTE ("*bridged*")**  
La solución pudiera estar en alguno de estos enlaces:
- [*1](https://www.golinuxhub.com/2015/10/how-to-configure-bridged-network-in/)
- [*2](https://www.linuxbabe.com/virtualbox/a-pretty-good-introduction-to-virtualbox-bridged-networking-mode)
- [*3](https://linuxhint.com/use-virtualbox-bridged-adapter/)


<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
