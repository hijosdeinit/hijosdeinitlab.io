---
layout: post
title: "[HowTo] Encadenar comandos en la línea de comandos (CLI) de GNU Linux"
date: 2021-02-15
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "linea de comandos", "cli", "terminal", "productividad", "lotes", "nohup", "bash"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Una forma de ser productivo es vencer la tentación de quedarse mirando la terminal mientras el comando que acabamos de lanzar termine su trabajo, para luego lanzar otro comando.  
Para evitar eso en GNU Linux existen los scripts, comandos como 'nohup' y parámetros como <span style="background-color:black">**` && \`**</span>  
Y de eso último es de lo que tratará esta entrada.  

<br>
Mediante <span style="background-color:black">**` && \`**</span> se pueden encadenar tareas en la línea de comandos de GNU Linux: <span style="color:lime">`trabajo1 && \trabajo2 && \trabajo3`</span>  
Por ejemplo, si se desea crear un directorio y luego mover un archivo a dicha carpeta:
~~~bash
mkdir /home/usuario/directorio && \mv /home/usuario/archivo /home/usuario/directorio/
~~~
... y así se pueden ir encadenando tantos comandos como se deseen.  
En [esta entrada de este blog se explica cómo hacer que el sistema avise con un sonido cuando termine de ejecutar un comando](https://hijosdeinit.gitlab.io/howto_sonido_de_aviso_cuando_termina_un_comando_cli/) utilizando precisamente el parámetro <span style="background-color:black">**` && \`**</span>  

<br>
El límite está en el ingenio, en la propia creatividad.  

<br>
Por ejemplo, si deseamos ejecutar una secuencia de trabajos en un servidor remoto y dejarlo trabajando sin que las tareas que iniciemos se paren al desconectar nuestra máquina cliente de dicho servidor o, incluso, al apagarla (nuestra máquina cliente), bastaría con:  

<br>
1. Conectar al servidor por 'SSH'
~~~bash
ssh usuario@servidor
~~~

<br>
2. Crear un script
~~~bash
nano script_trabajoporlotes.sh
~~~
... escribir en su interior:
~~~bash
#!/bin/bash
mkdir /home/usuario/directorio && \mv /home/usuario/archivo /home/usuario/directorio/
~~~
...guardar (ctrl+s) y salir (ctrl+x)

<br>
3. Darle permisos de ejecución al script
~~~bash
chmod +x script_trabajoporlotes.sh
~~~

<br>
4. Ejecutar el script mediante 'nohup'
~~~bash
nohup script_trabajoporlotes.sh &
~~~

<br>
5. Desconectar y, si queremos, apagar nuestra máquina cliente.
~~~bash
logout
~~~

<br>
Cuando, más tarde, volvamos a conectar al servidor podemos revisar el archivo 'nohup.out', que crea automáticamente el comando 'nohup' para guardar los 'logs' del trabajo, para ver el registro de las tareas que ordenamos.  

<br>
<br>
<br>
<br>
<br>
<br>
