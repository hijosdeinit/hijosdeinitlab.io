---
layout: post
title: '[HowTo] Conocer el estado de carga de la bateria desde línea de comandos (CLI)'
date: 2020-09-29
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "batería", "electricidad", "notebook", "ordenador portátil", "atom", "Compaq Mini", "acpi", "ibam", "conky"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Desde línea de comandos podemos conocer el estado de carga de la batería de diversas formas:  

<br>
## ACPI (*Advanced Configuration and Power Interface*)
~~~bash
acpi
~~~
Añadiendo algún parámetro se puede obtener información extra:
~~~bash
acpi -V
~~~

Si no tenemos 'acpi' instalado:
~~~bash
sudo apt-get update
sudo apt install acpi
~~~

<br>
## IBAM (*the Intelligent Battery Monitor*)
Es probable que 'ibam' no funcione con kernels modernos.
El comando funciona así:
~~~bash
ibam --battery
~~~

Junto a 'ibam' se instala automáticamente el programa ['gnuplot'](http://www.gnuplot.info/), que permite ver gráficas con los datos de la batería.  

Si no tenemos 'ibam' instalado
~~~bash
sudo apt-get update
sudo apt install ibam
~~~

<br>
<br>
Los datos de estas utilidades -'IBAM' y/o 'ACPI'- pueden ser "leídos" y mostrados por [**Conky**](http://conky.sourceforge.net/config_settings.html), el panel de control en pantalla ligero y versátil.  

<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[UbunLog](https://ubunlog.com/conocer-el-estado-de-la-bateria-desde-la-terminal/)  
[atareao.es](https://www.atareao.es/podcast/personalizacion-extrema-de-linux-con-conky/)  
[UbunLog](https://ubunlog.com/conkywizard-configurar-conky-facilmente/)</small>  
<br>
<br>
<br>
<br>
