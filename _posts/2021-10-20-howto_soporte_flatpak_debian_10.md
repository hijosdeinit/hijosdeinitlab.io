---
layout: post
title: "[HowTo] nueva paquetería vol.02: incorporación de soporte 'flatpak' a Debian 10 y derivados"
date: 2021-10-20
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "nueva paqueteria", "snap", "appimage", "electron", "flatpak", "deb", "dpkg", "apt", "software"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Debian 10 *Buster* por defecto no trae activada la paquetería ['flatpak'](https://flatpak.org/), al menos por lo que he podido observar en una instalación de tipo ***'netinstall'***.  
Me disgusta la nueva paquetería -prefiero instalar las cosas de forma nativa o compilando directamente- aunque he de reconocer que es una opción a tener en cuenta cuando, por ejemplo, se desea ejecutar aplicaciones como 'Adobe Acrobat Reader' en linux que, de forma nativa, carecen una instalación típica; o también cuando, al tratar de instalar algo de forma nativa obtenemos errores y no somos capaces de resolverlos en un tiempo razonable.  

<br>
## Qué es 'flatpak'
«Flatpak, conocido como 'xdg-app' hasta mayo de 2016, es una utilidad para despliegue, administración de paquetes universales y virtualización de aplicaciones para entornos de escritorio de GNU/Linux. La utilidad proporciona un entorno de aislamiento de procesos llamado Bubblewrap, en el que los usuarios pueden ejecutar aplicaciones aisladas del resto del sistema. Flatpak es una manera de instalar aplicaciones independientes de la distribución o versión de Gnu/Linux. Las aplicaciones que utilizan Flatpak necesitan permiso del usuario para controlar dispositivos de hardware o acceder a los archivos del usuario.  
Existen disponibles paquetes flatpak, oficiales o desarrollados por terceros, para cientos de aplicaciones. Entre ellas están las más populares como LibreOffice, VLC, Gimp, Inkscape, Blender, darktable, Krita, Audacity, FileZilla, MonoDevelop Las aplicaciones están disponibles en ]flathub.org](https://flathub.org/home). Hay también aplicaciones adicionales de entorno de oficina y también de juegos. Las versiones de estas app suelen estar actualizadas a las últimas versiones disponibles de manera rápida. Esto permite por ejemplo instalar las últimas versiones en distribuciones con ciclos de actualización más lentos de sus versiones en sus repositorios, además de evitar tener que adaptar las versiones de cada aplicación y sus actualizaciones a cada distribución.»  

<br>
> Página Web oficial: [https://flatpak.org/](https://flatpak.org/)  
Repositorio de Github: [https://github.com/flatpak/flatpak](https://github.com/flatpak/flatpak)  
Repositorio "Tienda de aplicaciones" (**FlatHub**): [https://flathub.org/home](https://flathub.org/home)  

<br>
## Instalación del soporte para paquetería 'flatpak' en Debian (y derivados)
En Debian y derivados el soporte'Flatpak' se instala y activa así de sencillo:
~~~bash
sudo apt-get update
sudo apt install flatpak
~~~
En sistemas GNU Linux que utilicen 'Gnome', es conveniente instalar también el plugin 'flatpak' de 'Gnome':
~~~bash
sudo apt install gnome-software-plugin-flatpak
~~~
Si se desea también se puede añadir 'FlatHub' a nuestro sistema. (<small>'FlatHub' es la 'tienda', es decir, el repositorio oficial de aplicaciones 'flatpak', que es el equivalente a 'snapcraft' en 'snap'</small>):
~~~bash
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
~~~
Después de todo este procedimiento, para rematar la faena y que esta nueva paquetería funcione correctamente hay que reiniciar el sistema (`sudo reboot`).  
A partir de ese momento se puede instalar paquetes 'flatpak'.  

<br>
## Instalación y desinstalación de paquetes 'flatpak'
Instalación
~~~bash
flatpak install flathub nombredelpaqueteflatpak
~~~
Desinstalación:
~~~bash
flatpak uninstall flathub nombredelpaqueteflatpak
~~~

<br>
## Actualización de aplicaciones 'flatpak'
Actualización de todas las aplicaciones:
~~~bash
flatpak update
~~~
Actualizar de una aplicación específica:
~~~bash
flatpak update nombreaplicacionflatpak
~~~

<br>
## Ejecución de aplicaciones 'flatpak' instaladas
~~~bash
flatpak run nombreaplicacionflatpak
~~~

<br>
## Otros comandos de 'flatpak' útiles y/o habituales
Eliminar entornos de ejecución no utilizados:
~~~bash
flatpak uninstall --unused
~~~
<br>
Eliminar todas las aplicaciones:
~~~bash
flatpak uninstall --all
~~~
<br>
Listar aplicaciones instaladas:
~~~bash
flatpak list
~~~
<br>
Listar solo las aplicaciones instaladas, no los entornos de ejecución:
~~~bash
flatpak list --app
~~~
<br>
Listar solo las aplicaciones disponibles en el repositorio:
~~~bash
flatpak remote-ls --app
~~~
<br>
Ver los detalles de una aplicación:
~~~bash
flatpak info nombreaplicacionflatpak
~~~

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] nueva paquetería, vol.01: 'snap' en Debian 10 Buster (y derivados)](https://hijosdeinit.gitlab.io/howto_snap_en_debian_10_buster_y_derivados/)
- [Nueva paquetería, vol. 03: cosas MOLESTAS de 'flatpak'](https://hijosdeinit.gitlab.io/cosas_molestas_flatpak_nuevapaqueteria/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/Flatpak">Wikipedia - flatpak</a>  
<a href="https://flatpak.org/setup/Debian/">FlatPak.org</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
