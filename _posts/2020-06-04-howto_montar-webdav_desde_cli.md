---
layout: post
title: '[HowTo] Montar webdav desde línea de comandos en cliente GNU Linux'
date: 2020-06-04
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "webdav", "redes", "networking", "compartir", "nextcloud", "davfs2", "dav", "CLI"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En [esta entrada del blog](https://hijosdeinit.gitlab.io/howto_Varios_metodos_acceso_Linux-cuenta_NextCloud/) se explica los diferentes métodos para acceder a una cuenta de NextCloud desde Ubuntu y derivados.  
Uno de los métodos señalados era estableciendo un acceso **davs://** (webdav seguro) desde línea de comandos a una cuenta específica del servidor webdav de NextCloud.  

NextCloud -software de sincronización de ficheros cliente-servidor- emplea el protocolo **webdav**.  

<br>
## Instalación de 'davfs2'
Para que el montaje se ejecute correctamente es fundamental que el **paquete [davfs2](https://wiki.archlinux.org/index.php/Davfs2) esté instalado en la máquina cliente**:
~~~bash
sudo apt-get update
sudo apt install davfs2
~~~
Durante la instalación de davfs2 solicitará que determinemos si sólo el superusuario tendrá capacidad de montar webdav o si también cualquier usuario podrá hacerlo.  
Este aspecto se puede modificar más adelante ejecutando
~~~bash
sudo dpkg-reconfigure davfs2
~~~

<br>
## Procedimiento para MONTAR un recurso 'webdav' remoto desde línea de comandos (CLI)
En GNU Linux montar la carpeta que un servidor comparte mediante [webdav](https://es.wikipedia.org/wiki/WebDAV) -NextCloud, por ejemplo- se puede realizar desde la línea de comandos de la máquina cliente con un simple comando:
~~~bash
sudo mkdir /mnt/puntomontajewebdav
sudo mount -t davfs -o noexec https://ipdelservidor/remote.php/webdav/ /mnt/puntodemontajewebdav
~~~
Donde '/mnt/puntodemontajewebdav' es el subdirectorio -que previamente habremos creado- donde se montará.  
<br>
El sistema responderá algo así:
~~~
Please enter the username to authenticate with server
https://192.168.0.zz/remote.php/webdav/ or hit enter for none.
  Username: xxxxxxxxxxxx
Please enter the password to authenticate user vicz with server
https://192.168.0.zz/remote.php/webdav/ or hit enter for none.
  Password:  
/sbin/mount.davfs: the server certificate does not match the server name
/sbin/mount.davfs: the server certificate is not trusted
  issuer:      localhost
  subject:     localhost
  identity:    localhost
  fingerprint: xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx:xx
You only should accept this certificate, if you can
verify the fingerprint! The server might be faked
or there might be a man-in-the-middle-attack.
Accept certificate for this session? [y,N] y
/sbin/mount.davfs: warning: the server does not support locks
~~~

<br>
Como he señalado, esto serviría -por ejemplo- para montar la cuenta webdav de un usuario de NextCloud.  

<br>
Sin embargo, con el comando anterior, un usuario convencional carecería de privilegios de escritura en lo montado.  
Para montar un recurso 'webdav' permitiendo que un usuario convencional no sólo lea, sino también escriba, el comando ha de ser este otro:
~~~bash
sudo mount -t davfs -o noexec -o uid=nºuid gid=nºgid https://ipdelservidor/remote.php/webdav/ /mnt/puntodemontajewebdav
~~~
En este blog hay una [entrada que explica cómo conocer los valores del 'identificador de usuario (uid)' y del 'identificador de grupo (gid)' de un usuario](https://hijosdeinit.gitlab.io/howto_averiguar_gid_uid_de_un_usuario_debian_derivados/).  

<br>
## Configurar el montaje automático de un recurso 'webdav' remoto
Para que el sistema monte automáticamente el recurso 'webdav' hay que editar el archivo '/etc/fstab'
~~~bash
sudo nano /etc/fstab
~~~
... y agregarle una línea según esta nomenclatura: <span style="background-color:#042206"><span style="color:lime">`urldelservidorwebdav    puntodemontaje    davfs rw,noauto,user 0 0`</span></span>  
Por ejemplo:
~~~
https://ipdelservidor/remote.php/webdav/   /mnt/puntodemontajewebdav   davfs rw,noauto,user 0 0
~~~

<br>
## Automatización de la autentificación
**!** <big>NO</big> recomiendo en absoluto esto porque la contraseña se almacena en texto plano y, aunque el archivo sólo lo pueda leer el superusuario, en la práctica hay formas de romper eso.  
Para no tener que introducir usuario y contraseña del recurso webdav cada vez que éste se monte hay que editar el archivo '~/.davfs2/secrets'
~~~
nano ~/.davfs2/secrets
~~~
... y se le añade una línea siguiendo el siguiente patrón: <span style="background-color:#042206"><span style="color:lime">`urldelservidorwebdav      usuariodelservidorwebdav      contraseñadelusuariodelservidorwebdav`</span></span>  
Por ejemplo:
~~~
https://ipdelservidor/remote.php/webdav/   pepe   micontraseña
~~~
Guardamos los cambios con <span style="background-color:#042206"><span style="color:lime">Ctrl + o</span></span>  
Salimos del editor 'nano' pulsando <span style="background-color:#042206"><span style="color:lime">Ctrl + x</span></span>  
Cambiamos los permisos del archivo '~/.davfs2/secrets' para que sólo el superusuario pueda leerlo y escribir en él.  
~~~bash
sudo chmod 600 ~/.davfs2/secrets
~~~

<br>
## Procedimiento para DESMONTAR un recurso 'webdav' remoto desde línea de comandos (CLI)
~~~bash
sudo umount /mnt/puntodemontajewebdav
~~~
> /sbin/umount.davfs: waiting while mount.davfs (pid 4998) synchronizes the cache .. OK  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Acceso desde Nautilus mediante webdav a cuenta NextCloud](https://hijosdeinit.gitlab.io/howto_Acceso_desde_Nautilus_mediante-webdav_a_cuenta_NextCloud/)
- [[HowTo] Varios métodos de acceso desde GNU Linux a cuenta NextCloud](https://hijosdeinit.gitlab.io/howto_Varios_metodos_acceso_Linux-cuenta_NextCloud/)
- [Instalación nativa del cliente oficial NextCloud en Debian 10 y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_nativa_cliente_nextcloud_debian_y_derivados/)  

<br>
<br>
<br>
---  
<small>Fuentes:  
[sysblogd](https://sysblogd.wordpress.com/2007/09/09/ubuntu-mounting-remote-filesystem-using-davfs2-fuse/)  
[sleeplessbeastie](https://blog.sleeplessbeastie.eu/2017/09/04/how-to-mount-webdav-share/)  
[webdav.io](https://webdav.io/linux-webdav-mount-how-to-mount-webdav-on-linux/)  
[usuario p-na en SuperUser.com](https://superuser.com/a/320640)  
[wiki archlinux](https://wiki.archlinux.org/title/Davfs2)
[GeekLand.eu](https://geekland.eu/montar-servidor-webdav-en-un-sistema-de-archivos-con-davfs2/)</small>  

<br>
<br>
<br>
<br>
