---
layout: post
title: "[HowTo] Compilar 'bucklespring' en Debian y derivados"
date: 2021-04-02
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "sonido", "typewriter", "cherry", "teclado", "IBM", "keyboard", "teclas", "pulsación", "editor de texto", "buckle", "bucklespring", "typewriter-sounds", "keypress", "tickeys", "focus writer", "writemonkey"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
**ℹ** <small>En sistemas cuyos escritorios utilizan [*'Wayland'*](https://wayland.freedesktop.org/) 'bucklespring' no tiene toda su funcionalidad: por ejemplo, es frecuente que las pulsaciones al trabajar en un emulador de terminal ('gnome-terminal', 'lxterminal', etc...) no emitan ningún sonido.  
Cuando, por ejemplo, 'Gnome' funciona bajo las 'X' y no bajo Wayland no hay problemas / disfunciones de ese tipo.</small>  

<br>
En [otra entrada de este blog](https://hijosdeinit.gitlab.io/howto_BuckleSpring_sonido_pulsaciones_teclado/) hablé de 'bucklespring' y otras aplicaciones que hacen que se emita un sonido con cada pulsación del teclado.  
'Bucklespring', que está en los repositorios de Debian y derivados (se puede instalar desde ahí), puede incorporarse también al sistema **compilando su código fuente**.  

<br>
Esto se hace así:

<br>
## 1. Preparar el sistema para la compilación de software
En [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_preparar_debian_para_compilar/) se explica cómo hacerlo, los paquetes que -una vez instalados en el sistema- nos permitirán compilar todo tipo de software a partir de su código fuente.  
Es importante que el paquete 'pkg-config' -además de, obviamente, 'build-essential' y 'linux-headers'- esté instalado.  

<br>
## 2. Instalar las dependencias que requiere 'bucklespring'
~~~bash
sudo apt-get update
sudo apt install libopenal-dev
sudo apt install libalure-dev
sudo apt install libxtst-dev
sudo apt install libinput10
sudo apt install libinput-dev
~~~

<br>
## 3. (opcional) Crear el directorio donde realizaremos la compilación del código fuente de 'bucklespring'
~~~bash
mkdir /home/miusuario/Programas
~~~

<br>
## 4. Obtener el código fuente de 'bucklespring' en ese directorio que acabamos de crear
~~~
cd /home/miusuario/Programas
git clone https://github.com/zevv/bucklespring.git
~~~
Se descargará el código fuente en un directorio llamado 'bucklespring'.

<br>
## 5. Compilar el código fuente de 'bucklespring'
~~~bash
cd /home/miusuario/Programas/bucklespring
make
~~~
**ℹ** <small>En sistemas de escritorio que emplean 'Wayland' -que, como señalé anteriormente, suelen provocar que algunas prestaciones de 'bucklespring' no funcionen- compilar 'bucklespring' así hace que el sonido de las pulsaciones sólo se produzca en las aplicaciones de escritorio mas no en los emuladores de terminal como 'gnome-terminal' o 'lxterminal'.</small>  
<small> Si, en sistemas de escritorio que emplean 'Wayland', se desea que 'bucklespring' dote de sonido a las pulsaciones realizadas en ventanas de emuladores de terminal, habrá que compilarlo así:</small> 
~~~bash
sudo make libinput=1
~~~
<small>Sin embargo, en ese caso, al ejecutar 'bucklespring' (`sudo ./buckle`) las teclas sólo sonarán bajo el emulador de terminal, no en el resto de aplicaciones de escritorio.  
Puede pensarse que instalando el paquete de 'bucklespring' que se halla en los repositorios oficiales (`sudo apt install bucklespring`) evitaría estos inconvenientes. Negativo, no es así.  
**En sistemas de escritorio que utilizan el tradicional servidor 'X' para su escritorio no existe ninguno de estos problemas.**</small>  

<br>
## 6. Ejecutar el programa 'bucklespring' recién compilado
Desde la carpeta donde se llevó a cabo la compilación, '/home/miusuario/Programas/bucklespring' en el ejemplo, basta con ejecutar:
~~~bash
./buckle &
~~~
ô, si estando "en Wayland" (y habiendo compilado mediante el comando `make libinput=1`) deseamos sonido en una ventana de terminal:
~~~bash
sudo ./buckle &
~~~

## 7. Terminar el programa 'bucklespring'
Con el parámetro **<big>`&`</big>** 'bucklespring' se ejecutó en **segundo plano**.  
Para retornar a ese proceso basta con ejecutar:
~~~bash
fg
~~~
... para a continuación terminarlo pulsando las teclas 'ctrl' y 'c' simultáneamente.  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://github.com/zevv/bucklespring">BuckleSpring - repositorio oficial - instrucciones</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
