---
layout: post
title: "Incorporación de plugins 'nyquist' a 'Audacity' (Debian y derivados)"
date: 2021-07-10
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "ubuntu", "audacity", "plugin", "editor de audio", "audio", "multimedia", "nyquist", "spyware", "adware", "telemetría", "forks", "audacium", "tenacity"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
[Audacity](https://github.com/audacity/audacity) es un conocido -y [últimamente polémico]()- editor de audio de código abierto. Existe para varias plataformas: GNU Linux, Windows, MacOS.  
Las prestaciones de Audacity pueden ser aumentadas incorporando [*plugins*](https://www.audacityteam.org/download/plug-ins/).  
Para incorporar un *plugin* ['nyquist'](https://wiki.audacityteam.org/wiki/Nyquist_Effect_Plug-ins) a 'Audacity' basta con:  
1. Copiarlo como superusuario al subdirectorio '/usr/share/audacity/plug-ins'
2. Ejecutar 'Audacity' y activar (*'enable'*) el plugin [Effect → Add/Remove Plug-ins]  

«**Nyquist** is a programming language for sound synthesis and analysis based on the Lisp programming language. It is an extension of the XLISP dialect of Lisp.  
The Nyquist programming language and interpreter were written by Roger Dannenberg (co-founder of Audacity) at Carnegie Mellon University, with support from Yamaha Corporation and IBM.  
Nyquist was contemporary with the birth of Audacity and is essentially Audacity’s own user-customisable plug-in format.»  

<br>
<br>
Entradas relacionadas:  
- [La privacidad hoy (enero 2022) en 'Audacity'. Tras la tempestad](https://hijosdeinit.gitlab.io/privacidad_audacity_hoy/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://manual.audacityteam.org/man/installing_effect_generator_and_analyzer_plug_ins_on_linux.html">manual.audacityteam.org</a>  
<a href="https://www.audacityteam.org/about/nyquist/">Audacity - nyquist</a>  
<a href="https://wiki.audacityteam.org/wiki/Download_Nyquist_Plug-ins">wiki.audacityteam.org - otro listado de plugins 'nyquist'</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

