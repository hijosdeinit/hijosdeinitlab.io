---
layout: post
title: "Directorios donde 'iTunes' almacena respaldos"
date: 2019-06-23
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "apple", "ipad", "iphone", "ipod", "itunes", "apps", "backup", "respaldo", "apple store", "ios", "macOS", "windows", "hack", "crack"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Dicen que Apple lo pone todo siempre fácil. Debo ser un zoquete porque el entorno 'iTunes' es bastante poco intuitivo para mí, y supongo que a alguno más le ocurrirá.  
¿Dónde guarda iTunes "las cosas"?.  
Entre otros sitios, aquí:
- `C:\Users\`***tuusuario***`\AppData\Roaming\Apple Computer\MobileSync\Backup`
- `C:\Users\`***tuusuario***`\Music\iTunes`  

<br>
<br>
<br>
Entradas relacionadas:  
- []()
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href=""></a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
