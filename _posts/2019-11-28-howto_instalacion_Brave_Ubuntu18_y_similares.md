---
layout: post
title: "[HowTo] Instalación del navegador web 'Brave' en Ubuntu 18 (y similares)"
date: 2019-11-28
author: Termita
category: "sistemas operativos"
tags: ["brave", "browser", "web browser", "http", "https", "navegador", "navegador web", "privacidad", "sistemas operativos", "ubuntu", "debian", "curl", "gnu linux", "linux", "internet 4.0", "software", "apt"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Brave es un navegador web que supuestamente hace hincapié en la privacidad.  
Según la [página oficial](https://web.archive.org/web/20190901063153/https://brave-browser.readthedocs.io/en/latest/installing-brave.html) el procedimiento para instalar 'Brave' en GNU Linux Ubuntu 18 es el siguiente:
~~~bash
sudo apt install apt-transport-https curl

curl -s https://brave-browser-apt-release.s3.brave.com/brave-core.asc | sudo apt-key --keyring /etc/apt/trusted.gpg.d/brave-browser-release.gpg add -

source /etc/os-release

echo "deb [arch=amd64] https://brave-browser-apt-release.s3.brave.com/ $UBUNTU_CODENAME main" | sudo tee /etc/apt/sources.list.d/brave-browser-release-${UBUNTU_CODENAME}.list

sudo apt-get update

sudo apt install brave-browser
~~~
(*) El uso de 'curl' tiene sus riesgos, aunque en este caso sea para añadir claves gpg y fuentes (sources). Nunca está de más revisar el código.  

<br>
<br>
Entradas relaccionadas:  
[[HowTo] Instalación del navegador 'Brave' en Debian 10 64bits y derivados - mayo 2021](https://hijosdeinit.gitlab.io/howto_instalacion_Brave_Debian10_y_derivados/)

<br>
<br>
<br>

---  
<small>Fuentes:  
<a href="https://web.archive.org/web/20190901063153/https://brave-browser.readthedocs.io/en/latest/installing-brave
.html">Brave - documentación oficial</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
