---
layout: post
title: '[HowTo] Monitorización de RaspBerry Pi [volumen 3]: temperaturas de CPU y GPU, frecuencia y voltaje desde línea de comandos (CLI)'
date: 2019-06-24
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "almacenamiento", "cliente", "memoria", "RAM", "Monitorix", "Monitoriza-Tu-Raspberry", "monitorización", "procesos", "Raspberry-Pi-Status", "rpi-monitor", "servicios", "servidor", "temperatura", "voltaje", "frecuencia", "servidor", "seguridad", "gnu linux", "linux", "debian", "cli", "raspberry"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
A parte de [rpi-monitor](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_1/), de Monitorix -del que ya se escribió en [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_Monitorix_instalacion_configuracion_uso_basico/)-, y de RaspBerry-Pi-Status y Monitoriza-tu-RaspBerry -de los que se trató en [esta otra entrada del blog](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_2/)- existen otras utilidades o comandos para monitorizar -remotamente o no- RaspberryPi.  

Anteriormente se señalaron varios de los interfaces que existen para monitorizar temperaturas, voltaje, procesos, almacenamiento, etcétera.  
Hay datos que se pueden obtener directamente desde línea de comandos (CLI) -remotamente o no-, sin instalar ningún interface cliente-servidor.  
En este caso se trata de monitorizar las <big>**temperaturas de CPU y GPU**</big>, la <big>**frecuencia del procesador**</big> y el <big>**voltaje**</big> directamente desde línea de comandos (CLI).  

<br>
(i) En lo que a visualizar las temperaturas de CPU y GPU de RaspBerry Pi respecta, [he leído](https://raspberrypi.stackexchange.com/a/37607) que el chip BCM2835/6 utilizado en modelos como Pi 1 y Pi 2 es de tipo [SoC](https://en.wikipedia.org/wiki/System_on_a_chip) -*System on a Chip*-, y por consiguiente con una sola temperatura; la temperatura de GPU será la misma que la temperatura de CPU.

<br>
<br>
## Monitorizar temperatura de CPU
~~~bash
cat /sys/class/thermal/thermal_zone0/temp
~~~

<br>
## Monitorizar temperatura de GPU
~~~bash
vcgencmd measure_temp
~~~

<br>
## Monitorizar frecuencia de CPU
~~~bash
vcgencmd measure_clock arm
~~~

<br>
## Monitorizar voltaje
~~~bash
vcgencmd measure_volts core
~~~

---

<br>
## Un script en bash para dominarlos a todos
Este script monitorizará cada segundo ('watch' -n 1') temperaturas de CPU y GPU, frecuencia de CPU, y voltaje.  
Creamos el archivo del script:
~~~bash
nano monitor_tempcpu_tempgpu_freq_volt.sh
~~~
Y escribimos "dentro":

<br>
<small>`#!/bin/bash`</small>  
<small>`watch -n 1 "vcgencmd measure_clock arm; vcgencmd measure_temp; vcgencmd measure_volts core; echo tempCPU=$((`cat /sys/class/thermal/thermal_zone0/temp`/1000)).0\'C"`</small>  

<br>
Guardamos con Ctrl+o  
Salimos (del editor de texto 'nano' con Ctrl+x  
Damos permisos de ejecución al archivo:
~~~bash
chmod +x monitor_tempcpu_tempgpu_freq_volt.sh
~~~
Lo ejecutamos así:
~~~bash
bash monitor_tempcpu_tempgpu_freq_volt.sh
~~~

<br>
## Un script en C++ para comprobar la temperatura
Puede modificarse para que muestre también la frecuencia, voltaje, supuesta temperatura de GPU.  
~~~cpp
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
double get_temp(bool use_farenheit);
int main(int argc,char *argv[])
{
  // Display the temperature to the user.
  printf("Temperature = %3.3f'C or %3.3f'F\n",get_temp(false),get_temp(true));
  return 0;
}
// Returns the temp in Farenheit or Celcius. Returns -1000 if something went wrong.
double get_temp(bool use_farenheit)
{
  const int BUFFER_SIZE = 100;
  char data[BUFFER_SIZE];
  FILE *finput;
  size_t bytes_read;
  double temp = -1000;
  finput = fopen("/sys/class/thermal/thermal_zone0/temp","r");
  if (finput != NULL) {
    memset(data,0,BUFFER_SIZE);
    fread(data,BUFFER_SIZE,1,finput);
    temp = atoi(data);
    temp /= 1000;
    if (use_farenheit) {
      temp = temp * 9 / 5 + 32;
    }
    fclose(finput);
  }
  return temp;
}
~~~

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Monitorización de RaspBerry Pi [volumen 1]: rpi-monitor](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_1/)
- [[HowTo] Monitorización de RaspBerry Pi [volumen 2]: Monitorix, Raspberry-Pi-Status y Monitoriza-Tu-Raspberry](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_2/)
- [[HowTo] Monitorix: instalación, configuracion, uso básico](https://hijosdeinit.gitlab.io/howto_Monitorix_instalacion_configuracion_uso_basico/)
- [[HowTo] 5 comandos para monitorizar la memoria RAM (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_monitorizar_memoria_ram_debian_derivados/)
- [[HowTo] Incorporar monitor de sistema a CrunchBang++ y parientes similares](https://hijosdeinit.gitlab.io/howto_monitor_sistema_en_Crunchbang_Linux_derivados/)  

<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[elinux](https://elinux.org/RPI_vcgencmd_usage)  
[alteageek](https://alteageek.com/2016/04/24/como-medir-la-temperatura-de-nuestra-raspberry/)  
[miraquelodije](http://miraquelodije.blogspot.com/2013/03/comprueba-la-temperatura-de-tu.html)  
[linuxize](https://linuxize.com/post/linux-watch-command/)</small>  
<br>
<br>
<br>
<br>
