---
layout: post
title: "'Expresiones regulares' y 'Secuencias de escape' útiles en las búsquedas: BARRA INVERSA"
date: 2020-06-15
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gedit", "gnome", "secuencias de escape", "expresiones regulares", "búsqueda", "reemplazar", "regex", "regexp", "editor de texto", "editor", "grep", "awk", "sed"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
La **barra inversa** o **contrabarra** <span style="color:lime"><big>**`\`**</big></span> es uno de los elementos que sirven para construir **expresiones regulares**, como también sirven:  
- el **punto** <span style="color:lime">**`.`**</span>  
- el **signo de admiración** <span style="color:lime">**`!`**</span>  
- los **corchetes** <span style="color:lime">**`[]`**</span>  
- la **barra** <span style="color:lime">**`|`**</span>  
- el **signo de dólar** <span style="color:lime">**`$`**</span>  
- el **acento circunflejo** <span style="color:lime">**`^`**</span>  
- los **paréntesis** <span style="color:lime">**`()`**</span></small>  
- el **signo de interrogación** <span style="color:lime">**`?`**</span>  
- las **llaves** <span style="color:lime">**`{}`**</span>  
- el **asterisco** <span style="color:lime">**`*`**</span>  
- el **signo de suma** <span style="color:lime">**`+`**</span>  
- los **"grupos anónimos"**  

<br>
«La barra inversa se utiliza para escapar el siguiente carácter de la expresión de búsqueda de forma que este adquiera un significado especial o deje de tenerlo. O sea, la barra inversa no se utiliza nunca por sí sola, sino en combinación con otros caracteres. Al utilizarlo por ejemplo en combinación con el punto <span style="color:lime">`\.`</span> este deja de tener su significado normal y se comporta como un carácter literal.  
De la misma forma, cuando se coloca la barra inversa seguida de cualquiera de los caracteres especiales que discutiremos a continuación, estos dejan de tener su significado especial y se convierten en caracteres de búsqueda literal.  
Como ya se mencionó con anterioridad, la barra inversa también puede darle significado especial a caracteres que no lo tienen».  

<br>
Una lista de algunas de estas combinaciones:
- <span style="color:lime"><big>`\t`</big></span> — Representa un tabulador.
- <span style="color:lime"><big>`\r`</big></span> — Representa el "retorno de carro" o "regreso al inicio" o sea el lugar en que la línea vuelve a iniciar.
- <span style="color:lime"><big>`\n`</big></span> — Representa la "nueva línea" el carácter por medio del cual una línea da inicio. <small>Es necesario recordar que en Windows es necesaria una combinación de <span style="color:lime">`\r\n`</span> para comenzar una nueva línea, mientras que en Unix solamente se usa \n y en Mac_OS clásico se usa solamente <span style="color:lime">`\r`</span>.</small>
- <span style="color:lime"><big>`\a`</big></span> — Representa una "campana" o "beep" que se produce al imprimir este carácter.
- <span style="color:lime"><big>`\e`</big></span> — Representa la tecla "Esc" o "Escape"
- <span style="color:lime"><big>`\f`</big></span> — Representa un salto de página
- <span style="color:lime"><big>`\v`</big></span> — Representa un tabulador vertical
- <span style="color:lime"><big>`\x`</big></span> — Se utiliza para representar caracteres ASCII o ANSI si conoce su código. De esta forma, si se busca el símbolo de derechos de autor y la fuente en la que se busca utiliza el conjunto de caracteres latín-1 es posible encontrarlo utilizando <span style="color:lime">`\xA9`</span>.
- <span style="color:lime"><big>`\u`</big></span> — Se utiliza para representar caracteres Unicode si se conoce su código. `\u00A2` representa el símbolo de centavos. No todos los motores de Expresiones Regulares soportan Unicode. El .Net Framework lo hace, pero el EditPad Pro no, por ejemplo.
- <span style="color:lime"><big>`\d`</big></span> — Representa un dígito del 0 al 9.
- <span style="color:lime"><big>`\w`</big></span> — Representa cualquier carácter alfanumérico.
- <span style="color:lime"><big>`\s`</big></span> — Representa un espacio en blanco.
- <span style="color:lime"><big>`\D`</big></span> — Representa cualquier carácter que no sea un dígito del 0 al 9.
- <span style="color:lime"><big>`\W`</big></span> — Representa cualquier carácter no alfanumérico.
- <span style="color:lime"><big>`\S`</big></span> — Representa cualquier carácter que no sea un espacio en blanco.
- <span style="color:lime"><big>`\A`</big></span> — Representa el inicio de la cadena. No un carácter sino una posición.
- <span style="color:lime"><big>`\Z`</big></span> — Representa el final de la cadena. No un carácter sino una posición.
- <span style="color:lime"><big>`\b`</big></span> — Marca la posición de una palabra limitada por espacios en blanco, puntuación o el inicio/final de una cadena.
- <span style="color:lime"><big>`\B`</big></span> — Marca la posición entre dos caracteres alfanuméricos o dos no-alfanuméricos.








En el editor de textos *gedit* con ctrl+h podemos buscar y reemplazar cadenas de texto, caracteres, etcétera.  
En esta tarea es muy interesante la implementación de expresiones regulares y secuencias de escape que, por ejemplo, permiten sustituir saltos de línea, retornos de carro, tabulaciones, etcétera por lo que consideremos oportuno.  
<br>
~~~
\n
Nueva línea

\r
Retorno de carro

\t
Tabulación
~~~
<br>
<br>

#### Anexo
En C++ las secuencias de escape son:
~~~
Secuencia
de escape		Descripción				Representación

\'		Comillas simple					byte 0x27
\"		Comillas doble					byte 0x22
\?		Signo de interrogación				byte 0x3f
\\		Barra invertida					byte 0x5c
\0		Carácter nulo					byte 0x00
\a		Pitido						byte 0x07
\b		Retorno						byte 0x08
\f		Nueva página					byte 0x0c
\n		Nueva línea					byte 0x0a
\r		Retorno de carro				byte 0x0d
\t		Tabulación horizontal				byte 0x09
\v		Tabulación vertical				byte 0x0b
\nnn		Valor en formato octal				byte nnn
\xnn		Valor en formato hexadecimal			byte nn
\unnnn  	Valor en código Unicode.
		Puede dar como resultado varios caracteres.	código U+nnnn
\Unnnnnnnn	Valor en código Unicode.
		Puede dar como resultado varios caracteres.	código U+nnnnnnnn
~~~

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[wikipedia - expresiones regulares](https://es.wikipedia.org/wiki/Expresi%C3%B3n_regular)  
[help.gnome.org](https://help.gnome.org/users/gedit/stable/gedit-replace.html.es)  
[cppreference](https://es.cppreference.com/w/cpp/language/escape)</small>  

<br>
<br>
<br>
<br>
