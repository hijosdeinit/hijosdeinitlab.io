---
layout: post
title: "febrero 2021: Mirror de archivo 'iso' de Open Media Vault 4 para RaspBerry Pi"
date: 2021-02-13
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "open media vault", "omv", "omv 4", "raspberry pi", "iso", "mirror", "descargar", "descargas", "armbian", "arm", "nas", "RaspberryPi", "Raspbian"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Tengo entendido que hasta finales de 2019 el método para montar 'Open Media Vault' (OMV) en RaspBerry Pi era el tradicional mediante "imagen dedicada", como si de un sistema operativo se tratara. Un procedimiento similar al que se llevaba a cabo para instalar Raspbian, Ubuntu Mate, Libreelec, etcétera en RaspBerry Pi:  
Se descargaba un fichero que contenía una imagen de disco -.iso en este caso-, se "quemaba" en una tarjeta microsd o en un dispositivo de almacenamiento USB, se conectaba a RaspberryPi y se arrancaba.  

<br>
Aquella imagen .img de Open Media Vault ('OMV_4_Raspberry_Pi_2_3_3Plus.img.xz') corría bajo el sistema operativo ArmBian y funcionaba en RaspBerry Pi hasta los modelos 3B+. Desconozco (aún) si funcionaría en RaspBerry Pi 4; probablemenente no.  

<br>
Open Media Vault actualmente ya no se instala así (mediante una imagen de disco).  
Ahora, partiendo de un sistema operativo base preexistente, es decir, ya instalado por el usuario -Raspbian Lite, por ejemplo- se instalan los paquetes y dependencias de OMV, generalmente mediante un script.  

<br>
Por alguna razón Open Media Vault ha eliminado la antigua imagen .iso que distribuía a través de Sourceforge e insta a los usuarios a seguir el procedimiento nuevo (instalar OMV mediane un script ejecutado en un sistema operativo RaspBian preexistente):  
<small>[Adden-B-Installing_OMV5_on_an%20R-PI.pdf](https://github.com/OpenMediaVault-Plugin-Developers/docs/blob/master/Adden-B-Installing_OMV5_on_an%20R-PI.pdf)  
[Adden-A-Installing_OMV5_on_Armbian.pdf](https://github.com/OpenMediaVault-Plugin-Developers/docs/blob/master/Adden-A-Installing_OMV5_on_Armbian.pdf)  
[Adden-C-Installing_OMV5_on_32-bit_i386.pdf](https://github.com/OpenMediaVault-Plugin-Developers/docs/blob/master/Adden-C-Installing_OMV5_on_32-bit_i386.pdf)</small>  

<br>
~~Pues bien, todo aquel que desee testear aún el antiguo procedimiento y no encuentre la .ISO en el antiguo repositorio de OMV en SourceForge puede descargarla desde [AQUÍ](https://sourceforge.mirrorservice.org/o/op/openmediavault/Raspberry%20Pi%20images/):~~  
~~[OMV_4_Raspberry_Pi_2_3_3Plus.img.xz](https://sourceforge.mirrorservice.org/o/op/openmediavault/Raspberry%20Pi%20images/OMV_4_Raspberry_Pi_2_3_3Plus.img.xz) 2018-07-19 13:00  265M~~  
~~[readme.txt](https://sourceforge.mirrorservice.org/o/op/openmediavault/Raspberry%20Pi%20images/readme.txt) 2018-07-22 14:32  1.6K~~  

<br>
<br>
**Revisión / edición 2021-02-13**  
<br>
El antiguo mirror de la Universidad de Kent, que era el único lugar de descarga de la iso de OMV para RaspBerry Pi ('OMV_4_Raspberry_Pi_2_3_3Plus.img.xz'), ya NO sirve el archivo:
> <small>«SourceForge content has moved  
Sorry, the SourceForge content you're looking for can no longer be found here. Please go to the SourceForge website to find the files that you want.  
If you've followed a link here from another site please let them know that they need to update it.»</small>  

<br>
<a href="/assets/img/2021-02-13-donde_descargar_iso_open_media_vault_para_raspberry_pi/2021-02-13_mirror_iso_omv4_en_universidad_kent_no_operativo.png" target="_blank"><img src="/assets/img/2021-02-13-donde_descargar_iso_open_media_vault_para_raspberry_pi/2021-02-13_mirror_iso_omv4_en_universidad_kent_no_operativo.png" alt="iso OMV 4 para RaspBerry Pi no disponible en mirror sourceforge de Universidad de Kent" width="800"/></a>  
<small>"iso" de OMV 4 para RaspBerry Pi ya no disponible en mirror sourceforge de Universidad de Kent</small>  

<br>
<br>
<big>No obstante</big>, todo aquel que desee testear aún el antiguo procedimiento y no encuentre la .ISO en el antiguo repositorio de OMV en SourceForge puede descargarla desde [<big>**AQUÍ**</big> (archive.org Wayback Machine)](https://web.archive.org/web/20180716100050/http://kaiser-edv.de/tmp/jk4NM5/OMV_3_0_99_RaspberryPi_2_3_4.9.80.img.xz).

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://linuxhint.com/openmediavault_raspberry_pi_3/">LinuxHint</a>  
<a href="https://colaboratorio.net/javierinsitu/colaboratorio/2018/openmediavault/">Colaboratorio.net - openmediavault</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
