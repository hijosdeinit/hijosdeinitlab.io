---
layout: post
title: "[HowTo] Extracción de las contraseñas de las redes inalámbricas almacenadas en Windows 10"
date: 2022-04-12
author: Termita
category: "seguridad"
tags: ["seguridad", "sistemas operativos", "redes", "redes inalambricas", "wireless", "inalambrica", "conexiones", "perfiles", "WPA", "WEP", "inseguridad", "vulnerabilidad", "windows", "windows 10", "windows 10 home", "problema", "netsh", "profile", "wlan show profile"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Windows almacena un perfil para cada conexión de red inalámbrica. En cada uno de esos perfiles se guarda la configuración ip específica y también la contraseña de la red, si ésta es inalámbrica.  

<br>
Mas <big>**¿cómo averiguar la contraseña inalámbrica de uno de estos perfiles?**</big>  

<br>
<br>
## 1. Ábrase una 'power shell'
menú inicio → power shell  

<br>
## 2. Ejecútese el comando `netsh`
~~~
netsh
~~~
Se abrirá el *prompt* (o línea de comandos) de 'netsh'  

<br>
## 3. (desde el *prompt* de 'netsh') Ejecútese el comando `wlan show profile`
~~~
wlan show profile
~~~
Se listarán todos los perfiles almacenados en Windows 10, entre los cuales estará el perfil cuya contraseña de red wireless queremos conocer.  

<br>
## 4. (desde el *prompt* de 'netsh') Ejecútese el comando `wlan show profile "NombreDelPerfilCuyaContraseñaDeseamosConocer" key=clear`
~~~
wlan show profile "NombreDelPerfilCuyaContraseñaDeseamosConocer" key=clear
~~~
Se mostrará toda la información sobre el perfil.  
Y es precisamenten, entre toda esa información, en el apartado **'Contenido de la clave'** se mostrará en texto plano **la clave wireless** de esa conexión de red.  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
