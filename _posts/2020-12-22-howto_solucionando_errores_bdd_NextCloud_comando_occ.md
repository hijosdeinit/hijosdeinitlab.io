---
layout: post
title: "[HowTo] Solucionando errores en la base de datos de NextCloud mediante el comando 'occ'"
date: 2020-12-22
author: Termita
category: "sistemas operativos"
tags: ["nextcloud", "nube", "web 4.0", "occ", "error", "base de datos", "database", "missing indices"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En NextCloud, en Configuración → Administración → Vista General, se pueden observar los avisos de seguridad y configuración, así como si existe alguna actualización disponible.  

Otro lugar donde aparecen avisos es en Configuración → Administración → Registros.  

Algunas veces después de actualizar NextCloud aparecen mensajes de error.  
Podemos encontrarnos mensajes como este:  

<span style="color:lime"><small>`Some indices are missing in the database. Because adding indexes to large tables may take some time, they have not been added automatically. By executing “occ db: add-missing-indices”, missing indexes can be added manually while the instance is running. Once the indexes have been added, queries to these tables are usually faster.
Missing index “share_with_index” in the “oc_share” table.
Missing index “parent_index” in the “oc_share” table.
Missing index “fs_mtime” in the “oc_filecache” table.`</small></span>  

ô  
<span style="color:lime"><small>`Some columns in the database are missing a conversion to big int. Due to the fact that changing column types on big tables could take some time they were not changed automatically. By running ‘occ db:convert-filecache-bigint’ those pending changes could be applied manually. This operation needs to be made while the instance is offline. For further details read the documentation page about this`</small></span>  
»  

Estos errores se solucionan generalmente mediante el comando **'occ'** que incorpora NextCloud. Mas, <big>¿Cómo se emplea el comando 'occ'?</big>  
<br>
Hay que abrir una terminal o línea de comandos.  
<small>(*) Si NextCloud -o NextCloudPi- está desplegado mediante docker, primero averiguamos el identificador del docker mediante <span style="color:lime">`sudo docker ps`</span> y luego ejecutamos <span style="color:lime">`sudo docker exec -i -t identificadordeldocker /bin/bash`</span>, tal como se explica en [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_entrar_a_la_linea_de_comandos_de_un_contenedor_docker/).</small>  
Hay que Activar el modo mantenimiento de NextCloud
~~~
sudo -u www-data php /var/www/nextcloud/occ maintenance:mode --on
~~~
<small>(*) Cuando hayamos terminado todo desactivaremos el modo mantenimiento con el comando `sudo -u www-data php /var/www/nextcloud/occ maintenance:mode --off`</small>  

Hay que acceder al directorio '/var/www/nextcloud', donde se ubica el comando 'occ'.
~~~
cd /var/www/nextcloud
~~~
Desde ahí se puede ejecutar el comando 'occ': <span style="color:lime">`sudo -u www-data php occ db:loquenosindicanextcloudacercadelerror`</span>. Por ejemplo, para reparar índices "perdidos" de la bdd:
~~~
sudo -u www-data php occ db:add-missing-indices
~~~
ô para reparar columnas "grandes"
~~~
sudo -u www-data php occ db:add-missing-columns
~~~
ô, para aplicar conversión a 'big int' de tablas y/o columnas de la bdd que así lo requieren:
~~~
sudo -u www-data php occ db:convert-filecache-bigint
~~~
ô para solucionar el error ocasionado por la falta de algunas claves primarias:
~~~
sudo -u www-data php /var/www/nextcloud/occ db:add-missing-primary-keys
~~~

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://help.nextcloud.com/t/some-indices-are-missing-in-the-database-how-to-add-them-manually/37852/2" target="_blank">castillo 92 en help.nextcloud.com</a>  
<br>
<br>
<br>
<br>
<br>
<br>
