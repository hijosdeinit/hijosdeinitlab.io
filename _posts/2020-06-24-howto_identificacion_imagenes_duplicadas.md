---
layout: post
title: '[HowTo] Identificación de imagenes duplicadas'
date: 2020-06-24
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "imagenes", "duplicados", "multimedia", "archivos", "búsqueda", "linux", "windows"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Antaño el común de los mortales hacía fotografías con una cámara fotográfica. Solía haber una por familia.
Hoy cualquier dispositivo hace fotografías y cada uno de los integrantes de una unidad familiar o grupo de trabajo tiene varios de esos en su haber, todos volcando fotografías. Resultado: Desorden, caos.  
Por eso es necesaria una disciplina, un orden, a la hora de transferir el material.  
Existen programas que, de no tenerla, ayudan a encontrar duplicados de, por ejemplo, las fotografías.  

Algunos programas:   
- [digikam](https://www.digikam.org/about/). <small>Linux (electron), Windows, MacOs</small>
- [Find Same Image Ok](https://www.softwareok.com/?Download=Find.Same.Images.OK). <small>Windows</small>
- [FSlint](https://www.pixelbeat.org/fslint/). <small>Linux</small>
- [Image Comparator](https://sourceforge.net/projects/imagecomparator/). <small>Windows</small>
- [findimagedupes / encontrarimagedupes](http://www.jhnc.org/findimagedupes/). <small>Linux</small>
- [Geequie](http://www.geeqie.org/). <small>Anteriormente GQView. Linux, MacOS</small>
- [fdupes](http://premium.caribe.net/~adrian2/fdupes.html). <small>Linux</small>
- [imgSeek](https://sourceforge.net/projects/imgseek/). <small>Linux</small>
- [Visipics](http://www.visipics.info/index.php?title=Main_Page). <small>Windows</small>
- [dupeGuru Picture Edition](https://dupeguru.voltaicideas.net/). <small>Linux, Windows, MacOS</small>
- [Anti-Twin](http://www.joerg-rosenthal.com/en/antitwin/). <small>Windows</small>

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[ubuntu.dokry.com](https://ubuntu.dokry.com/4358/como-puedo-encontrar-fotos-duplicadas.html)</small>  

<br>
<br>
<br>
<br>
