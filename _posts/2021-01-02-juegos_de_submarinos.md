---
layout: post
title: "Juegos de submarinos"
date: 2021-01-02
author: Termita
category: "juegos"
tags: ["submarino", "juegos", "simulación", "simulador", "mar", "guerra", "silent hunter", "harpoon", "fleet command", "hunter killer", "sub command akula seawolf", "dangerous waters", "cold waters", "uboat", "wolfpack", "aces fo the deep"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---

<br>
<a href="/assets/img/2021-01-02-juegos_de_submarinos/silenthunter_green.jpg" target="_blank"><img src="/assets/img/2021-01-02-juegos_de_submarinos/silenthunter_green.jpg" alt="submarino en un simulador para PC" width="800"/></a>  
<small>submarino en un simulador para PC</small>  
<br>

[GATO (1984)](https://en.wikipedia.org/wiki/Gato_(video_game))  
[Sub Battle Simulator (1987)](https://en.wikipedia.org/wiki/Sub_Battle_Simulator)  
[688 Attack Sub (1989)](https://en.wikipedia.org/wiki/688_Attack_Sub)  
[Aces of the Deep (1994)](https://en.wikipedia.org/wiki/Aces_of_the_Deep)  
[Jane’s 688(I) Hunter/Killer (1997)](https://en.wikipedia.org/wiki/688(I)_Hunter/Killer)  
[*saga* Harpoon (1989-2010~)](https://en.wikipedia.org/wiki/Harpoon_(series) {}  
[Jane's Fleet Command (1999)](https://store.steampowered.com/app/2910/Fleet_Command/) {1'99€}    
[Jane's 688(I) Hunter/Killer (1997)](https://store.steampowered.com/app/2900/688I_HunterKiller/) {1'99€}    
[Sub Command 'Akula Seawolf' (2001)](https://store.steampowered.com/app/2920/Sub_Command/) {1'99€}  
[Dangerous Waters (2006)](https://store.steampowered.com/app/1600/Dangerous_Waters/) {2'99€}  
[*saga* Silent Hunter (1996-2010)](https://store.steampowered.com/app/48110/Silent_Hunter_5_Battle_of_the_Atlantic/) {}  
[Command: Modern Air Naval Operations (2013)](https://en.wikipedia.org/wiki/Command:_Modern_Air_Naval_Operations)  
[Cold Waters (2017)](https://store.steampowered.com/app/541210/Cold_Waters/) {18'49€}  
[Uboat (en desarrollo)](https://store.steampowered.com/app/494840/UBOAT/) {13'24€}  
[WolfPack (en desarrollo)](http://www.wolfpackgame.com/spanish.php) [{29'99€}](https://store.steampowered.com/app/490920/Wolfpack/?l=spanish)  
[Red Storm Rising]()  

<br>
<br>
La inmensa mayoría de estos juegos de submarinos tienen <big>**expansiones y modificaciones**</big> hechas por comunidades de usuarios como la de [SubSim](https://www.subsim.com/radioroom/index.php).  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://en.wikipedia.org/wiki/Submarine_simulator">Wikipedia - Submarine simulator</a>  
<a href="https://www.subsim.com/radioroom/forumdisplay.php?f=173">SUBSIM Radio Room Forums > Sub/Naval + Other Games > Classic Subsims</a> · <a href="https://hijosdeinit.gitlab.io/assets/docs/2021-01-02-juegos_de_submarinos/listado_simuladores_submarinos.pdf">**descargar .pdf**</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

