---
layout: post
title: "[HowTo] Instalación de 'openssh-server' (servidor SSH) en Debian y derivados"
date: 2019-10-31
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "ssh", "redes", "acceso remoto", "remote", "secure shell", "gnu linux", "debian", "debian 11", "bullseye", "cortafuegos", "firewall", "server", "ufw", "uncomplicated firewall", "22"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Entorno: cpu Core2Duo, 6gb RAM, gpu gforce 7600gt, Live USB - GNU Linux Debian 11 Buster LXDE, kernel 5.10.0-9-amd64  

<br>
~~~bash
sudo apt-get update
sudo apt install openssh-server
sudo systemctl enable ssh
sudo systemctl status sshd
~~~
Si el servidor estuviera parado, para arrancarlo:
~~~bash
sudo systemctl restart sshd
~~~
Con eso debería bastar, mas no está de más asegurarse de un par de cosas:
- configuración (archivo de configuración del servidor ssh ('/etc/ssh/sshd_config')
- cortafuegos ('ufw', por ejemplo)  

<br>
<br>
## '/etc/ssh/sshd_config', el archivo de configuración del servidor ssh
Conviene repasarlo y modificarlo si fuera menester. A veces se producen [errores como el que se trata en esta publicación de este blog](https://hijosdeinit.gitlab.io/howto_error_ssh_Permission_denied_publickey_debian11bullseye/) que se deben a configuraciones por defecto "inesperadas".  

<br>
<br>
## Adecuación del cortafuegos (*firewall*)
Generalmente en sistemas de escritorio no es necesario tocar el cortafuegos.  
No ocurre así con los servidores GNU Linux. En muchos de esos casos es necesario implementar reglas en el cortafuegos.  
Si el cortafuegos está en manos de **'ufw'** (*uncomplicated firewall*), se configuraría así:
~~~bash
sudo ufw allow ssh
~~~
ô
~~~bash
sudo ufw allow 22/tcp comment 'Open port ssh tcp port 22'
~~~
(i) El puerto TCP '22' es el que generalmente emplea SSH; en caso de estar utilizando otro puerto, basta con sustituir el '22' por ese otro puerto.  

<br>
Para comprobar que la regla que acabamos de establecer está activa:
~~~bash
sudo ufw status
~~~
 
<br>
Otros comandos, que sirven para precisar más las cosas:
~~~bash
sudo ufw allow from 192.168.0.100 to any port 22
sudo ufw allow from 192.168.1.0/24 to any port 22 proto tcp
~~~
En este caso se limitan las conexiones SSH a unas 'ip' y/o protocolos concretos.  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] solucion al error 'Permission denied (publickey)' en servidor SSH ('openssh-server') [Debian 11 y derivados]](https://hijosdeinit.gitlab.io/howto_error_ssh_Permission_denied_publickey_debian11bullseye/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://techviewleo.com/install-configure-ssh-server-on-debian/">TechViewLeo</a>  
<a href="https://linuxhint.com/enable-ssh-server-debian/">LinuxHint</a>  
<a href="https://www.cyberciti.biz/faq/ufw-allow-incoming-ssh-connections-from-a-specific-ip-address-subnet-on-ubuntu-debian/">CyberCiti.biz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
