---
layout: post
title: "'Fediverso' de redes descentralizadas"
date: 2022-01-20
author: Termita
category: "internet 4.0"
tags: ["fediverso", "internet 4.0", "nodos", "instancias", "instancia", "descentralización", "federacion", "redes descentralizadas", "redes federadas", "federacion", "blockchain", "red social", "redes sociales", "mastodon", "fosstodon", "diapora", "quitter", "gnu social", "friendi", "friendica", "hubzilla", "identi.ca", "identi", "pump.io", "pump", "pixelfed", "mediagoblin", "funkwhale", "devtube", "peertube", "misskey", "invidious", "yewtu.be", "web 4.0", "descargar", "descargas", "streaming", "codigo abierto", "open source", "privacidad", "tracking", "rastreo", "anonimato", "autosuficiencia"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Dicen que ningunear es una buena estrategia. Pocos -sean personas físicas, corporaciones o instituciones del gobierno- soportan que demasiados les "ignoren" o anulen.  
La sociedad posmoderna ha adquirido una enorme y enfermiza dependencia de las redes sociales hegemónicas.  
En el corto tiempo que éstas llevan vivas han demostrado su negatividad sobradamente invadiendo la privacidad de sus "usados", maltratándolos, manipulándolos mediante publicidad y propaganda, cuando no censura, y fomentando un ambiente tóxico de pelea constante que, empresas como 'Facebook' y 'Twitter', bien conocen como una de las formas más eficaces de que el "usado" pase el mayor tiempo posible en sus plataformas.  
El poder de esas redes sociales hegemónicas está realmente en esa hegemonía -valga la redundancia- que le otorgan sus "usados". Sin usuarios no hay lucro y adviene la ruina; y de eso se trata.  
Qué mejor correctivo que pasar el menor tiempo posible en ellas, o darse de baja, o ignorarlas y suplirlas con el, más saludable, libre y útil, 'Fediverso'.</small>  

<br>
<big>***['Fediverso'](https://fediverse.party/)***</big> -de *'federación'* y *'diverso'*- es el término con el que se define a una serie de redes sociales abiertas y descentralizadas, competidoras -aún sin éxito- contra las redes tradicionales -masivas, y comerciales-.  

<br>
<a href="/assets/img/2022-01-20-Fediverso_redes_descentralizada/Fediverse_logo_proposal.svg.png" target="_blank"><img src="/assets/img/2022-01-20-Fediverso_redes_descentralizada/Fediverse_logo_proposal.svg.png" alt="zzz" width="200"/></a>  
<br>

«El 'fediverso' nació en 2008, poco después de la creación de Twitter (en 2006), pero hasta 2012 el 'fediverso' estaba centralizado en una única instancia -['identi.ca'](https://identi.ca/)-, que posteriormente se dividió en ['pump.io'](http://pump.io/) y ['GNU Social'](https://gnusocial.network/), su máximo exponente hasta 2016».
<br>
El software que sustenta al 'Fediverso' es software libre, de código abierto.  
Son redes descentralizadas porque no están alojadas en un sólo servidor, sino en multitud de ellos, y es el propio usuario el que escoge en cuál abrir su cuenta y/o incluso crear el suyo propio, su propio nodo o instancia.  
El nick del usuario está compuesto por su "nombre" acompañado del nombre de la instancia: <span style="background-color:#042206"><span style="color:lime">`nombreusuario@instancia`</span></span>.  
La existencia de múltiples servidores / nodos / instancias no impide en absoluto que los usuarios de éstas pueden interactuar entre sí intercambiando información, imagenes, video, audio, etc...  
La libertad es cuasi absoluta, supuestamente no existe censura.  
El respeto a la privacidad del usuario -que es usuario, no "usado", como ocurre en las rrss hegemónicas y tradicionales- es absoluto.  
Dado que son redes descentralizadas, aunque se deseara, es difícil hacer seguimiento de la actividad de un usuario.  
<br>
«Existe entre los usuarios del 'fediverso' una voluntad de no sometimiento a las grandes compañías, de control de los contenidos publicados y accedidos, de liberarse de la publicidad / propaganda masiva y la venta de datos personales y de huir de los tensos enfrentamientos que provocan los temas de actualidad delicados en las redes sociales tradicionales».
<br>
El ambiente es menos hostil y tóxico que en las redes sociales tradicionales -centralizadas, privativas y comerciales-. Podría decirse que prima la cooperación, el debate y el intercambio de ideas, no el "diálogo de besugos" tan frecuente en orinales como 'Twitter', 'Facebook' o 'Instagram'.  
<br>
«Precisamente, el 'fediverso' se nutre de manera principal de usuarios de esas otras redes, desencantados por el clima de tensión, pero también enfadados por las nuevas normas que rigen su funcionamiento. Es posible, no obstante, que muchos de estos éxodos masivos desde Facebook y Twitter se producen cíclicamente una o dos veces al año, los incrementos de usuarios más notables se experimentaron en 2016, cuando la red líder de microblogging cambió el algoritmo que regulaba lo que aparecía en el timeline, y en 2018, después del cierre de muchas cuentas -algunas con miles de seguidores- por utilizar expresiones presuntamente ofensivas.
<br>
«Los escándalos de filtración de datos en Facebook, el caso *'Cambridge Analytica'*, los ataques de gobiernos y corporaciones a la neutralidad de la red, las iniciativas de control de los flujos de información -censura, *fast checkers* a sueldo, etc...- en la red por parte de las instituciones hegemónicas de poder a raiz del "coronavirus", y el movimiento creciente en pro de devolver el poder de internet a los usuarios también han beneficiado al 'Fediverso'».  
«El principal obstáculo que impide una mayor expansión del 'fediverso' es que los usuarios regresan a los canales tradicionales al no encontrar a sus contactos en estas plataformas descentralizadas», que son poco "populares".  

<br>
<br>
## Los protocolos que se utilizan en el 'fediverso'
Las diferentes redes descentralizadas que componen el 'fediverso' utilizan alguno de los siguientes **protocolos**:
- protocolo [ActivityPub](https://en.wikipedia.org/wiki/ActivityPub)
- protocolo [Diaspora Network](https://en.wikipedia.org/wiki/Diaspora_(social_network))
- protocolo [OStatus](https://en.wikipedia.org/wiki/OStatus)
- protocolo [Zot & Zot/6](https://en.wikipedia.org/w/index.php?title=Zot_(protocol)&action=edit&redlink=1)  

<br>
<a href="/assets/img/2022-01-20-Fediverso_redes_descentralizada/How-the-Fediverse-connects__protocolos.jpg" target="_blank"><img src="/assets/img/2022-01-20-Fediverso_redes_descentralizada/How-the-Fediverse-connects__protocolos.jpg" alt="Protocolos empleados en el fediverso y cómo las diferentes redes descentralizadas interactúan entre ellas en base a dichos protocolos" width="500"/></a>  
<small>Protocolos empleados en el 'fediverso' y cómo las diferentes redes descentralizadas que lo componen interactúan entre ellas en base a dichos protocolos. <small>By Imke Senst,Mike Kuketz,RockyIII - https://commons.wikimedia.org/wiki/File:Fediverse_small_information.png https://social.tchncs.de/@kuketzblog/107045136773063674, CC BY-SA 4.0, https://commons.wikimedia.org/w/index.php?curid=111695835</small></small>  
<br>


<br>
<br>
## Redes que componen el 'fediverso'
Algunas de ellas se asemejan -en su estilo y/o en su funcion de *microblogging*- vagamente a 'Twitter'
> 'Mastodon', 'Misskey', 'Gnu Social' o 'Pleroma'.  

Otras incluyen más comunicación y opciones de interacción, lo cual las hace comparables al desaparecido 'Google+' o 'Facebook'
> 'Friendica' o 'HubZilla'.  

<br>
Una lista bastante exhaustiva de todas las redes que componen el 'fediverso' puede consultarse [en fediverse.party](https://fediverse.party/en/miscellaneous)  
<br>
Las más destacables son:
- [Diaspora](https://diaspora.social/) [<small>«Online world where you are in control. Own your data. Choose your audience. Be who you want to be»</small>]
- [GNU Social](https://gnusocial.network/) [<small>«Connecting free and independent communities across the web»</small>]
- [Mastodon](https://mastodon.social/) [<small>«Social networking back in your hands. Find your perfect community. Take control of your content. Es la más conocida y la que más usuarios tiene. Para comenzar a usarla es necesario elegir una instancia en función de los intereses, las normas de funcionamiento o el tipo de conversación que se prefiera, y hacer una solicitud para que te admitan. La instancia más conocida es la del creador de la red, Eugene Rochko, y se llama simplemente [‘social’](https://mastodon.social/). Otra instancia de 'Mastodon' interesante es ['Fosstodon'](https://fosstodon.org), for anyone who is interested in technology; particularly free & open source software.»</small>]
- [PeerTube](https://joinpeertube.org/) [<small>«Decentralized video hosting. Take back the control of your videos. Es una plataforma de vídeos similar a YouTube o Vimeo. Su lanzamiento se ha financiado mediante un crowdfunding lanzado por sus creadores»</small>]
- [PixelFed](https://pixelfed.org/) [<small>«Federated social image sharing for everyone. El denominado ‘Instagram libre’. Permite publicar fotos que pueden ser retocadas con filtros y muestra las publicaciones de nuestros compañeros de instancia en orden cronológico. Sus creadores están trabajando para permitir que los usuarios de distintas instancias puedan interactuar entre ellos y para crear sus propias ‘stories’.»</small>]
- [Friendi.ca](https://friendi.ca/) [<small>«Personal network with no central authority or ownership. Keep in contact with people you care about. Está considerada como la alternativa a Facebook. Creada en 2010, permite compartir contactos con Twitter o Diaspora (otra red social) y publicar al mismo tiempo en 'WordPress', 'Facebook' o 'Twitter'.»</small>]
- [HubZilla](https://hubzilla.org) [<small>«Feature-rich social plattform. Create channels with a decentralized nomadic identity»</small>]
- [MissKey](https://join.misskey.page/en-US/) [<small>«Sophisticated microblogging with personality. Provides many additional features like calendar, emoji reactions, polls, games and many other widgets»</small>]
- [Pleroma](https://pleroma.social/) [<small>«Microblogging platform with excellent performance and low resource consumption. Has many features: polls, custom themes, markdown support»</small>]
- [FunkWhale](https://funkwhale.audio/) [<small>«A social plattform to enjoy and share audio. Is a community-driven project that lets you listen and share music and audio within a decentralized, open network. Una especie de 'Spotify' libre.»</small>]  

<br>
<br>
<br>
Otras:
- ['Quitter'](https://www.quitter.is/)
- ['WriteFreely'](https://writefreely.org/) (<small>*An open source platform for building a writing space on the web*</small>)
- ['Mobilizon'](https://mobilizon.org/es/) (<small>red libre y federada para administración de eventos y grupos</small>)
- ['BookWyrm'](https://bookwyrm.social/) (<small>*Talk about books, track your reading, and find out what your friends are reading, on your own terms. BookWyrm is ad-free, anti-corporate, and federated. You can seamlessly follow and interact with users on other BookWyrm instances, and on services like Mastodon*</small>)
- ['Plume'](https://joinplu.me/) (<small>*A federated blogging application* equivalente a 'Blogger'</small>)
- ['Dev.Tube'](https://github.com/watch-devtube)
- ['MediaGoblin'](https://mediagoblin.org/) (<small>*media publishing platform that anyone can run. Decentralized alternative to Flickr, YouTube, SoundCloud, etc*</small>)
- etc, etc...  

<br>
En próximas entradas de este blog se tratará, en la medida de lo posible, de describir cada una de estas redes descentralizadas.  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] 'WhaleBird', un cliente de escritorio para el 'fediverso' ('Mastodon', 'Misskey', 'Pleroma') en Debian y derivados](https://hijosdeinit.gitlab.io/howto_whalebird_cliente_escritorio_para_el_fediverso_mastodon_misskey_pleroma_debian_y_derivados/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://en.wikipedia.org/wiki/Fediverse">Wikipedia - fediverse</a>  
<a href="https://joinfediverse.wiki/Main_Page">JoinFediverse - wiki</a>  
<a href="https://www.arimetrics.com/glosario-digital/fediverso">AricMetrics - glosario - fediverso</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
