---
layout: post
title: "[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'"
date: 2020-12-21
author: Termita
category: "sistemas operativos"
tags: ["nextcloud", "MarkDown Editor", "Plain Text Editor", "Text", "web 4.0", "apps", "programación", "markdown", "texto plano", "previsualización", "herramientas", "código"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>[NextCloud](https://nextcloud.com/) es una "nube personal" libre, de código abierto. Tras instalarla en un ordenador, permite que cualquiera pueda albergar por sí mismo sus archivos y acceder a ellos desde cualquier sitio.</small>  

<small>NextCloud permite una especie de añadidos: [aplicaciones para los más diversos menesteres que se le pueden "instalar dentro" para incrementar su funcionalidad](https://apps.nextcloud.com/).</small>  
<br>
Una de estas aplicaciones es 'MarkDown Editor'. Tengo entendido que esta aplicación antaño permitía escribir código markdown a la derecha mientras que a la izquierda aparecía la transcripción, es decir, la previsualización.  

‼ En NextCloud 20 'Markdown Editor' aparece  sólo **a una columna**: se escribe el código y apenas el sistema lo reconoce lo transforma ahí mismo, como si de una especie de corrector ortográfico se tratara.  
<br>
<a href="/assets/img/2020-12-21-nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/20201221_MarkDownEditor_1columna.png" target="_blank"><img src="/assets/img/2020-12-21-nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/20201221_MarkDownEditor_1columna.png" alt="(NextCloud 20) MarkDown Editor a 1 columna" width="800"/></a>  
<small>(NextCloud 20) MarkDown Editor a 1 columna ‼</small>  
<br>
<br>
**A mí eso no me interesa. Lo que quiero es escribir código y, de vez en cuando, echar un ojo a la derecha para ver el resultado**.  

La razón de que MarkDown Editor sólo muestre una columna es que esta aplicación depende de otra app: 'Plain Text Editor', tal como señalan en [help.nextcloud.com](https://help.nextcloud.com/t/solved-does-markdown-editor-no-longer-work/75909/2) y [aquí](https://github.com/icewind1991/files_markdown/issues/135).  

Por consiguiente, para disponer en NextCloud de un editor de markdown a dos columnas **hay que instalar y activar también la aplicación 'Plain Text Editor'**.  
Una vez hecho eso ya podremos escribir en markdown con la previsualización a la derecha.  
<br>
<a href="/assets/img/2020-12-21-nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/20201221_MarkDownEditor_clickderecho_abrirconPlainTextEditor.png" target="_blank"><img src="/assets/img/2020-12-21-nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/20201221_MarkDownEditor_clickderecho_abrirconPlainTextEditor.png" alt="click derecho, 'Edite en un editor de texto simple'" width="800"/></a>  
<small>click derecho, "Edite en un editor de texto simple"</small>  
<br>
<a href="/assets/img/2020-12-21-nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/20201221_MarkDownEditor_2columnas_ok.png" target="_blank"><img src="/assets/img/2020-12-21-nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/20201221_MarkDownEditor_2columnas_ok.png" alt="(NextCloud 20) MarkDown Editor a 2 columnas. Perfecto." width="800"/></a>  
<small>(NextCloud 20) MarkDown Editor a 2 columnas. Perfecto.</small>  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] El editor (IDE) atom y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)
- [[HowTo] Evitar que 'atom' elimine espacios en blanco al final de línea](https://hijosdeinit.gitlab.io/howto_atom_espacios_blanco_final_linea/)
- [[HowTo] Agregar idiomas al editor (IDE) atom. Ponerlo en español](https://hijosdeinit.gitlab.io/howto_poner_atom_en_espa%C3%B1ol/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- [[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)
- [[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados](https://hijosdeinit.gitlab.io/howto_micro_editor_texto_debian/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [[HowTo] Añadir resaltado de sintaxis al editor de textos NANO](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [[HowTo] nano: parámetros de arranque útiles](https://hijosdeinit.gitlab.io/howto_parametros_utiles_arranque_nano/)
- [En nano no existe overtyping](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/)
- [[HowTo] Comienzo con vim. I](https://hijosdeinit.gitlab.io/howto_comienzo_con_vim_1/)
- [[HowTo] Visualización de 'markdown' en la línea de comandos (CLI): 'MDLESS'](https://hijosdeinit.gitlab.io/howto_mdless_visor_markdown_cli/)
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://help.nextcloud.com/t/solved-does-markdown-editor-no-longer-work/75909/2" target="_blank">help.nextcloud.com</a>  
<a href="https://github.com/icewind1991/files_markdown/issues/135" target="_blank">icewind1991 github</a>  
<br>
<br>
<br>
<br>
