---
layout: post
title: "[HowTo] 'pdfjoin': concatenar 2 ó más documentos '.pdf' desde línea de comandos"
date: 2020-04-03
author: Termita
category: "sistemas operativos"
tags: ["pdfjoin", "productividad", "pdf", "sistemas operativos", "gnu linux", "linux", "CLI", "convertir", "exportar"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
~~~bash
pdfjoin --rotateoversize false input1.pdf input2.pdf --outfile output.pdf
~~~
<br>
(ℹ) La función del parámetro `--rotateoversize false` es evitar que al construirse el documento pdf resultante se giren algunas páginas porque "no caben".  

<br>
<br>
El comando 'pdfjoin' forma parte del paquete ['texlive-extra-utils'](https://packages.debian.org/buster/texlive-extra-utils), que -en Debian y derivados- se puede instalar así:
~~~bash
sudo apt-get update
sudo apt install texlive-extra-utils
~~~

<br>
No obstante, si no se desea instalar paquete alguno, basta con crear una simple función llegado el momento, tal como se explica en [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_concatenar_archivos_pdf_desde_linea_de_comandos_sin_instalar_nada/):  
<span style="background-color:#042206"><span style="color:lime">`function combine_pdfs() {     output_file=$1;     files=($2);     gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOutputFile=$output_file "${files[@]}"; }`</span></span>  

<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
<br>
<br>
