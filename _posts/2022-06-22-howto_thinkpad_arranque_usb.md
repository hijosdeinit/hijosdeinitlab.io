---
layout: post
title: "[HowTo] Arrancar ThinkPad desde USB (peleándome con un ThinkPad, vol.01)"
date: 2022-06-22
author: Termita
category: "hardware"
tags: ["hardware", "ibm", "exIBM", "lenovo", "thinkpad", "t470", "boot menu", "bios", "uefi", "fast boot", "secure boot", "live usb", "gnu linux", "Windows", "Windows 10", "sistemas operativos", "instalacion", "arranque", "booteable", "bootable", "teclado", "keyboard", "teclas", "pulsacion", "ibm model f", "ibm model m", "unicomp", "switch", "pulsadores"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Arrancar desde un dispositivo interno -p.ej. un dispositivo USB- es el primer paso para sacar a un ThinkPad T470 de la jaula de Windows 10 en que es entregado preso.  
El ordenador portátil Lenovo ThinkPad T470 viene con Windows 10 instalado de serie, mas ¿quien no quiere un sistema operativo de verdad en lugar de uno "de juguete"? Hay que instalarle GNU Linux. Y para ello no conozco otra manera que aquella en la que el ordenador arranca e instala desde un medio de almacenamiento externo (USB, por ejemplo), cosa que -recién extraído de la caja- no hará si no se deshabilita el parámetro <big>'***fast boot***'</big>, que trae activado por defecto.  
Para deshabilitar '*fast boot*' uno de los procedimientos -al menos el que he seguido yo- es el siguiente:
<br>
- Desde Windows 10: Opciones de energía → comportamiento del botón de encendido: deshabilitese '*fast boot*'.  
- Apáguese la máquina.  
- Conéctese a la máquina el dispositivo de almacenamiento externo que contenga la instalación de GNU Linux.  
- Enciéndase la máquina y -en el caso del ThinkPad T470 y otros muchos- púlsese repetidamente la tecla <big>**F12**</big> y finalmente aparecerá el '**boot menu**', es decir, el menú de arranque que nos permitirá elegir arrancar desde un medio externo -USB, por ejemplo-.  

<small>Si en lugar de 'F12' pulsáramos 'F1', en el ThinkPad T470 y otros se accederá a la configuración BIOS.</small>  

<br>
<br>
<br>
Entradas relacionadas:  
- (ThinkPad T470)(https://hijosdeinit.gitlab.io/thinkpad_t470/)
- [Teclados Mecánicos De Hoy. La Herencia Del '*Buckling Spring*'](https://hijosdeinit.gitlab.io/buckling_spring_teclados_mecanicos/)
- []()  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[]()  
[]()</small>  

<br>
<br>
<br>
<br>
