---
layout: post
title: "'Midnight-Green', mi tema visual favorito para i3wm en Debian y derivados"
date: 2022-02-12
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "ubuntu", "temas", "tema visual", "themes", "apariencia", "dark", "oscuro", "negro", "yaru", "yaru-dark", "midnight", "midnight-green", "visual", "aspecto", "i3wm", "i3", "lxde", "lxappearance", "unity", "gnome", "unity-tweak-tool", "gnome-tweak-tool", "retoques", "ubuntu tweak", "tweak tool"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small><small>Entorno:
cpu Core2Duo, 6gb RAM, gpu gforce 7600gt, GNU Linux Debian 10 Buster (*netinstall*) stable, kernel 4.19.0-18-amd64, gnome, i3wm</small></small>  

<br>
[<big>***'midnight-green'***</big>](https://github.com/i-mint/midnight) es mi tema visual favorito para el gestor de ventanas 'i3' en Debian 10 y derivados.  
'Midnight' es una suite de temas oscuros de diversas tonalidades, y de todos ellos la variante que prefiero es la verde ('midnight-green').  

<br>
<a href="/assets/img/2022-02-12-midnight-green_mi_tema_visual_favorito_para_i3wm_debian_10/lxappearance.png" target="_blank"><img src="/assets/img/2022-02-12-midnight-green_mi_tema_visual_favorito_para_i3wm_debian_10/lxappearance.png" alt="tema visual 'midnight-green'" width="500"/></a>  
<small>tema visual 'midnight-green'</small>  
<br>

Es una suite de temas oscuros que se actualiza y revisa con mucha frecuencia, es decir, está siendo mantenida activamente.  

<br>
A parte de la variante verde existen, como señalé antes, otras:
- Midnight-GreenNight
- Midnight-Blue
- Midnight-BlueNight
- Midnight-DarkYellow
- Midnight-Gray
- Midnight-GrayNight
- Midnight-Orange
- Midnight-OrangeNight
- Midnight-Red
- Midnight-RedNight  

<br>
<a href="/assets/img/2022-02-12-midnight-green_mi_tema_visual_favorito_para_i3wm_debian_10/midnight__themes.png" target="_blank"><img src="/assets/img/2022-02-12-midnight-green_mi_tema_visual_favorito_para_i3wm_debian_10/midnight__themes.png" alt="gama de temas visuales 'midnight'" width="500"/></a>  
<small>gama de temas visuales 'midnight'</small>  
<br>

<br>
<br>
### Procedimiento de instalación y activación de este -u otro- tema visual en Debian (gestor de ventanas 'i3wm', por ejemplo)
En [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_incorporacion_temas_visuales_i3wm_debian10/) se detalla el sencillo procedimiento, que básicamente consiste en:
1. Descargar el tema visual desde [pling / gnome-look](https://www.pling.com/p/1273208/) o su [repositorio oficial en GitHub](https://github.com/i-mint/midnight)
2. Descomprimirlo
3. Mover el subdirectorio resultante de la descompresión a '/usr/share/themes'
4. Seleccionar el tema mediante la herramienta 'lxappearance'  

<br>
<br>
### Otros temas oscuros interesantes
<small>Se instalan y configuran siguiendo procedimientos similares.  
- [Equilux](https://www.gnome-look.org/p/1182169/)
- [Ambiance DS BlueSB12](https://www.gnome-look.org/p/1013664/)
- [Deepen Dark](https://www.gnome-look.org/p/1190867/)  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Incorporación de temas visuales a i3wm en Debian 10 y derivados](https://hijosdeinit.gitlab.io/howto_incorporacion_temas_visuales_i3wm_debian10/)
- [[HowTo] Incorporar temas oscuros a Ubuntu 16.04.7 LTS](https://hijosdeinit.gitlab.io/howto_tema_oscuro_yaru_dark_ubuntu_1604/)  


<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
