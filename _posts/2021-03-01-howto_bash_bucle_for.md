---
layout: post
title: "[HowTo] Utilización del bucle bash 'for' para realizar trabajos por lotes"
date: 2021-03-01
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "granel", "lotes", "bash", "for", "productividad"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Puede que el alguna ocasión deseemos ejecutar en la terminal un comando que no acepta trabajo por lotes, es decir, a granel.  

<br>
Para ilustrarlo bien valdría un ejemplo: el comando 'pdftotext'.  
Ese comando, que sirve para transformar un archivo '.pdf' a texto plano '.txt', no acepta realizar su trabajo por lotes.  
Básicamente su uso es este: `pdftotext -layout nombredelarchivo.pdf`  

<br>
Si deseara que hiciera su tarea de convertir a .txt todos los archivos .pdf de un directorio en concreto bastaría con combinarlo con un bucle bash <big>'**for**'</big>.  
Así, una vez situados en el directorio:
~~~
for file in *.pdf; do pdftotext -layout "$file"; done
~~~

<br>
**Valga este ejemplo para adaptarlo a otros comandos que no permitan trabajo por lotes**.

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://ubunlog.com/pdftotext-convierte-pdf-texto/">UbunLog</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
