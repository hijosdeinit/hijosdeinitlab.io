---
layout: post
title: "[HowTo] el metabuscador 'FuckOffGoogle' y su proyecto de libertad"
date: 2021-09-03
author: Termita
category: "internet 4.0"
tags: ["internet 4.0", "buscador", "metabuscador", "altavista", "searx", "frontend", "fuckoffgoogle", "privacidad", "ads", "publicidad", "no ads", "tracking", "traqueo", "rastreo", "seguimiento", "cookies", "neutralidad", "perfil", "monetizacion", "google", "servidor", "descentralizacion", "open source", "libre", "libertad", "seguridad", "web", "tor", "proxy", "invidious", "activismo"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
##### <small>**ANTECEDENTES y CONTEXTO**</small>  
<small>Hace muchos años 'Google' ('Alphabet') y/o su **"buscador"** ni siquiera existían. A principios y mediados de los años 90 del siglo pasado la web estaba compuesta sobre todo por servidores de instituciones educativas, científicas, y algunas páginas personales ubicadas en servidores "de pago" o sitios "gratuítos" como ['Geocities'](https://es.wikipedia.org/wiki/GeoCities).  
Por entonces, disponer de una cuenta de correo electrónico era un "privilegio" disponible tan sólo para unos pocos internautas afortunados que, generalmente, formaban parte de alguna institución o, que -de su bolsillo- pagaban por disponer de ello. Luego -1996- llegó [Hotmail](https://es.wikipedia.org/wiki/Outlook_(correo_web)#Lanzamiento_de_Hotmail) y el panorama al respecto cambió: una cuenta de correo electrónico "gratuíta" a cambio de tus datos, de tu privacidad, de ser comprado y vendido como un objeto. Y así fue, años más tarde -1997-, Microsoft adquirió Hotmail, con toda su cartera de "clientes".  
La publicidad en aquellos años era escasísima, limitada a algunos *banners*, y generalmente poco intrusiva. La cuantificación, catalogación, rastreo y monetización del internauta brillaba por su ausencia. [Amazon](https://en.wikipedia.org/wiki/Amazon_(company) (1994) era una simple librería pionera en la venta de su producto -libros- por internet, y lo que hoy es [Netflix](https://en.wikipedia.org/wiki/Netflix), por entonces no era ni siquiera el videoclub que fue (se fundó en 1997).  
El **buscador de referencia** era [Altavista](https://digital.com/altavista/), cuya URL era 'http://altavista.digital.com' y fue lanzado por [Digital Equipment Corporation](https://es.wikipedia.org/wiki/Digital_Equipment_Corporation) -una empresa desarrolladora de megacomputadoras- en 1995 para poner a prueba las capacidades de uno de sus superordenadores. A su sombra estaba el buscador de [Yahoo](https://es.wikipedia.org/wiki/Yahoo!), nacido en 1994, cuyo objeto era (y es) el lucro, comprar y vender.  
En [este blog se le dedicó un artículo a 'Altavista, su historia y su "legado"'](https://hijosdeinit.gitlab.io/internet1.0_la_era_Altavista/), un buscador -el mejor de su era- que no fue creado para tomar internet por asalto o aprovecharse de una oportunidad comercial en auge.  
<small>'Alphabet'-'Google', 'Amazon', 'Yahoo', 'Microsoft', 'Hotmail' / 'MSN', 'Netflix'..., hay que ver cómo, escribiendo sobre buscadores, han ido saliendo retratados todos los enemigos del internauta de a pie, que -constituídos en *'Big Tech'*- también lo son -tal como están las cosas hoy- de la humanidad, a la cual maltratan.</small></small>  
<br>
<a href="/assets/img/2021-09-03-howto_fuckofgoogle_metabuscador/altavista1995.jpg" target="_blank"><img src="/assets/img/2021-09-03-howto_fuckofgoogle_metabuscador/altavista1995.jpg" alt="el buscador 'Altavista' en 1995" width="300"/></a>  
<small><small>el buscador 'Altavista' en 1995</small></small>  
<br>

<br>
# 'FuckOffGoogle'
['FuckOffGoogle'](https://search.fuckoffgoogle.net/) es un metabuscador de internet cuyo objeto es, a parte de mostrar los resultados de las búsquedas de sus usuarios, el respeto a la privacidad. Es por esto -en esta era del internet 4.0 y la cuantificación, rastreo, análisis y monetización del usuario- por lo que 'FuckOffGoogle' es un *rara avis*.  
'FuckOffGoogle' está íntimamente relacionado con otro metabuscador, [searX]((https://searx.me)), en cuyo software se basa / utiliza, tan libre como desinteresado en el rastreo y análisis de los usuarios. Hay una [entrada de este blog dedicada al metabuscador 'searX', padre de 'FuckOffGoogle'](https://hijosdeinit.gitlab.io/howto_searx_metabuscador/).  
A diferencia de 'searX' -que es un software libre instalable en cualquier servidor (instancia) y que, como tal, dispone de multitud de instancias a parte de la instancia oficial- 'FuckOffGoogle' es tan solo una instancia "personalizada" del software de metabúsqueda 'searX'.  

<br>
<a href="/assets/img/2021-09-03-howto_fuckofgoogle_metabuscador/fuckoffgoogle.png" target="_blank"><img src="/assets/img/2021-09-03-howto_fuckofgoogle_metabuscador/fuckoffgoogle.png" alt="página web principal del metabuscador 'FuckOffGoogle'" width="600"/></a>  
<small>página web principal del metabuscador 'FuckOffGoogle'</small>  
<br>

Los [responsables de 'FuckOffGoogle'](https://fuckoffgoogle.de/) lo definen de la siguiente manera:
> «Somos un Buscador de Internet donde los resultados de búsqueda se obtienen (vía proxy) de otros, tales como, Google, Yahoo, Bing, entre otros; para así **asegurar que los usuario no revelen ningún dato personal o de comportamiento a estas empresas**.  
Por ende, sus **resultados son neutrales, es decir, no están influenciados por el perfil en línea de cada usuario**. O dicho de otra manera: Los usuarios se mantienen **fuera de la 'burbuja de filtros' diseñada para ofrecer anuncios publicitarios en sus búsquedas**. Anuncios en los que es muy probable que hagan clic posteriormente.  
Este servicio te lo ofrecen los vecinos de Kreuzberg (Berlín) y una multitud internacional que actúa conjuntamente para [echar al 'Campus de Google' de nuestro barrio] y a Google (y su mundo) de nuestras vidas. ¡Únete a nosotros! Nuestro servidor se esfuerza por respetar tu privacidad: no almacenamos ningún registro.»  

<br>
Y, en lo que respecta a sus **motivos e inquietudes** -el por qué del proyecto-, éstos señalan:
> <small>«Fuck Off 'Google'! ¡No dejes a 'Google' & co. apoderarse de nuestras vidas y nuestros espacios!  
'Google' roba y explota nuestros datos para su provecho y ha convertido ese asunto en norma. Asimismo coloniza nuestros espacios físicos. 'Google' canceló su plan de levantar un 'Campus Google' en  Kreuzberg, Berlin, por las presiones del vecindario; aún continúa con su expansión a lo largo y ancho del mundo.  
Como la descentralizada red de personas que somos, queremos mantener nuestras vidas y espacios libres de esta compañia [evasora de las leyes e impuestos](https://wiki.fuckoffgoogle.de/no-tax-no-law) y también de sus afines, y mantener una oposición al futuro distópico que ofrecen.  
Los proyectos de 'Google' en Berlin, Toronto, Rennes, San José, etc... incrementaron el precio de los alquileres; los edificios comprados por 'Google' y sus *startups* fomentaron la especulación inmobiliaria y la destrucción de los medios de vida locales.  
'Google' viola sistemáticamente lo derechos humanos mediante censura generalizada y vigilancia masiva.  
'Google' sistemáticamente evade los impuestos y legislación de la Unión Europea. ¿Queremos una compañía así como vecino?  
Queremos un mundo donde nuestras comunicaciones y datos estén en nuestras propias manos, donde la tecnología pueda ser confiable y esté bajo nuestro control.  
'Google' tiene un rol predominante en el enfoque de la tecnología hacia un futuro distópico donde los humanos sean controlados por algoritmos.  
Como multitud diversa que somos, podemos organizarnos para resistir y patear a 'Google' & co y echarlo fuera de nuestras vidas y espacios. Además, compartamos con alegría el conocimiento sobre los comportamientos de explotación de esta empresa y promovamos tecnologías descentralizadas. Cualquier persona pueden participar aportando material de investigación sobre cualquiera de nuestros temas.  
Por favor contáctenos para sugerir adiciones o modificaciones de contenido, enviar documentos y propuestas para investigación y acción.
Siéntase libre de participar aportando en nuestra [wiki](https://wiki.fuckoffgoogle.de/), suscribirse a nuestro [boletín](https://fuckoffgoogle.de/subscribe-to-the-newsletter), unirse a nuestro [chat (experimental)](https://organize.fuckoffgoogle.de/#FuckOffGoogle:fuckoffgoogle.de) o [contactarnos](https://fuckoffgoogle.de/contact).»  

<br>
## Prestaciones de 'FuckOffGoogle'
- utiliza el [software de 'searX'](https://github.com/searx/searx), que es software libre, <small>actualmente en su versión 1.0.0</small>
- no rastrea al usuario
- no analiza al usuario
- no comparte datos con terceros
- los resultados son neutrales (<small>NO resultados personalizados de nuestros perfiles dentro de Google y otros motores de búsqueda</small>)
- sin publicidad
- soporta más de 70 motores de búsqueda
- fácil integración con cualquier motor de búsqueda
- cookies desactivadas por defecto
- conexiones seguras y cifradas (HTTPS/SSL)
- descentralizado
- su idiosincracia, sus rasgos esenciales, favorecen la seguridad, la intimidad y la libertad  

<br>
<a href="/assets/img/2021-09-03-howto_fuckofgoogle_metabuscador/fuckofgoogle_preferencias.png" target="_blank"><img src="/assets/img/2021-09-03-howto_fuckofgoogle_metabuscador/fuckofgoogle_preferencias.png" alt="configuración de las 'preferencias' de 'FuckOffGoogle'" width="600"/></a>  
<small>configuración de las 'preferencias' de 'FuckOffGoogle'</small>  
<br>

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] 'searX', un metabuscador *hackable* que busca la privacidad](https://hijosdeinit.gitlab.io/howto_searx_metabuscador/)
- [[HowTo] 'QWant': otro buscador respetuoso con la privacidad](https://hijosdeinit.gitlab.io/howto_qwant_buscador_web_respetuoso_privacidad/)
- ['DuckDuckGo' NO es de fiar](https://hijosdeinit.gitlab.io/googlizacion_de_duckduckgo_censurando_contenidos_adulterando_busquedas/)
- [[HowTo] Buscadores web respetuosos con el internauta](https://hijosdeinit.gitlab.io/buscadores_web_respetuosos/)
- ['Altavista' y su era. Internet 1.0](https://hijosdeinit.gitlab.io/internet1.0_la_era_Altavista/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/Searx">Wikipedia - searX</a>  
<a href="https://blog.desdelinux.net/fuck-off-google-searx-2-interesantes-proyectos-conocer-utilizar/">DesdeLinux</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
