---
layout: post
title: '[HowTo] Comprobar si una conexión de acceso a internet está sometida a CG-NAT'
date: 2020-08-30
author: Termita
category: "redes"
tags: ["sistemas operativos", "redes", "internet", "dns", "ISP", "privacidad", "CG-NAT", "IPV-4", "ip pública", "puertos", "latencia", "ping", "servicios", "servidor", "test", "auditoría"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
De CG-NAT y de sus repercusiones negativas en los accesos domésticos a internet trató [este artículo](https://hijosdeinit.gitlab.io/cgnat/) de este blog.  
Mas...

## ¿Cómo saber si nuestra conexión a internet está sometida a CGNAT?

1. La ip "pública" que vemos en el interfaz de configuración e información del router no se corresponde con la ip pública real.  
Esto se constata si accedemos a cualesquiera de las páginas que nos informan de cuál es nuestra ip pública -por ejemplo, [checkip.dyndns.com](https://checkip.dyndns.com)- y la comparamos con la que efectivamente nos señala el interfaz de configuración de nuestro propio router.  
Si no coinciden significa que nuestra conexión trabaja con CGNAT.  

<br>
<a href="/assets/img/2020-08-30-howto_comprobar_cgnat/2020-08-30_ip_pública_02.png" target="_blank"><img src="/assets/img/2020-08-30-howto_comprobar_cgnat/2020-08-30_ip_pública_02.png" alt="ip pública que nos señala nuestro router" width="800"/></a>  
<small>ip pública que nos señala nuestro router</small>  

![ip pública que nos señala checkip.dyndns.com]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-08-30-howto_comprobar_cgnat/2020-08-30_ip_pública_01.png)  
<small>ip pública que nos señala checkip.dyndns.com</small>  

<br>
2. Si la ip "pública" que vemos en el interfaz de configuración e información del router es de tipo 100.64.0.0/10 -no se parece a una "ip pública" convencional- = CGNAT.  

<br>
3. Si cuando efectuamos 'traceroute' ('tracert' en Windows) a nuestra ip pública hay más de 1 salto en el resultado = CGNAT  

![traceroute señalando 1 sólo salto]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-08-30-howto_comprobar_cgnat/2020-08-30_traceroute.png)
<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[RedesZone](https://www.redeszone.net/2017/02/19/como-saber-si-mi-ip-es-publica-o-si-mi-operador-me-ha-asignado-una-ip-privada-con-carrier-grade-nat/)  
[BandaAncha](https://bandaancha.eu/foros/duda-sobre-tengo-cg-nat-1732661)  
[Naseros](https://naseros.com/2017/05/22/solucion-al-problema-de-conexion-remota-desde-una-red-exterior/)</small>  
<br>
<br>
<br>
<br>
