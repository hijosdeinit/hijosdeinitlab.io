---
layout: post
title: "[HowTo] Averiguar el 'gid' y el 'uid' de un usuario en Debian y derivados"
date: 2021-10-21
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gid", "uid", "usuario", "user", "mount", "group", "grupo", "privilegios", "permisos"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Conocer el **identificador de grupo** -['gid'](https://es.wikipedia.org/wiki/Identificador_de_grupo)- y el **identificador de usuario** -['uid'](https://es.wikipedia.org/wiki/Identificador_de_usuario)- de un usuario me sirvió para poder montar (<small><span style="background-color:#042206"><span style="color:lime">'sudo mount'</span></span></small>) con permisos de lectura y escritura para un usuario convencional. Tengo la certeza de que sirve para más cosas.  
Los valores del 'gid' y el 'uid' se guardan en el fichero '/etc/passwd'.  

<br>
## Averigurar el 'gid' (¿principal?) de un usuario
~~~bash
sudo id -g nombredeusuario
~~~
> 1000  

(i) <small>Para conocer todos los 'gid' de un usuario: <span style="background-color:#042206"><span style="color:lime">`sudo id -G nombredeusuario`</span></span></small>


<br>
## Averiguar el 'uid' de un usuario
~~~bash
sudo id -u nombredeusuario
~~~
> 1000  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Montar webdav desde línea de comandos en cliente GNU Linux](https://hijosdeinit.gitlab.io/howto_montar-webdav_desde_cli/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://techlandia.com/13112435/como-encontrar-el-uid-y-el-gid">TechLandia</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
