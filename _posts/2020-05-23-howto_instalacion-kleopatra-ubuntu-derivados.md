---
layout: post
title: "[HowTo] Instalación de Kleopatra en Ubuntu y derivados"
date: 2020-05-23
author: Termita
category: "cifrado"
tags: ["sistemas operativos", "Linux", "encriptación", "cifrado", "criptografía", "gpg", "GnuPG", "OpenPGP", "archivos", "Windows", "kleopatra", "kde", "Ubuntu", "Debian", "clave privada", "clave pública"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
[**Kleopatra**](https://kde.org/applications/en/utilities/org.kde.kleopatra) es un gestor de certificados y un interfaz gráfico (GUI) para GnuPG (gpg).  
Este software almacena los certificados OpenPGP y las claves. Está gestionado por [KDE](https://www.kde.org/) y hay versión para Windows y GNU Linux.  
Kleopatra en combinación con el cliente de correo electrónico [KMail](https://kde.org/applications/office/org.kde.kmail2) permite incorporar sus características criptográficas a la comunicación vía email.  

En Ubuntu y derivados (Debian), Kleopatra se instala así:
~~~
sudo apt-get update
sudo apt install kleopatra
~~~

Pese a que en la instalación no se produce ningún error, Kleopatra, que está mantenido por KDE, NO ha funcionado en Ubuntu 18.04 bajo **Gnome**.
Entre otras cosas, a simple vista se observa además este error:  
> La autocomprobación de configuración de GnuPG ha fallado. Código de error: 1 Diagnóstico: gpgconf: error al ejecutar '/usr/lib/gnupg/scdaemon': probablemente no está instalado scdaemon:Smartcards:/usr/lib/gnupg/scdaemon:0:0:  

Quizás deba probar a instalar el paquete más actualizado... o a lo mejor Gnome es incompatible con esta aplicación KDE (Kleopatra). Continuaré probando.

De momento continuaré probando con:
- Integración de cifrado/descifrado en el gestor de archivos (Nautilus) mediante 'seahorse-nautilus'
- [GnuPG Shell](https://www.tech-faq.com/gnupg-shell.html)  

<br>
<br>
[Manual de Kleopatra](https://docs.kde.org/stable5/en/pim/kleopatra/index.html)  
<br>
<br>
Para desinstalar Kleopatra:
~~~
sudo apt-get purge --auto-remove kleopatra
~~~  

<br>
<br>
