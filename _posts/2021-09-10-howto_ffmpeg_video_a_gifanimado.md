---
layout: post
title: "[HowTo] creación de gif animado a partir de un video (ffmpeg)"
date: 2021-09-10
author: Termita
category: "multimedia"
tags: ["ffmpeg", "gif animado", "video"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Un <big>**gif animado**</big> puede ser muy útil. Entre otras cosas permite insertar animaciones en sitios -p.ej. páginas web- donde la inserción de video es más compleja.  

<br>
Dado un fichero de video, ¿cómo crear a partir de éste un gif animado?  
> <span style="background-color:#042206"><span style="color:lime">`ffmpeg -i `*input_ficherodevideo* *output_fichero_gifanimado_resultante*</span></span>  

Esto convertiría cada cuadro (*frame*) de video y crearía un gif animado con las mismas dimensiones que el video original y un tamaño (kb, mb, etc...) semejante.  

<br>
Si se desea ajustar parámetros para adecuar / optimizar el resultado a nuestras necesidades (menor tamaño en kb), el comando se puede "refinar" controlando las dimensiones (<small>**x**, **y**. Unas dimensiones de '450' pueden ser suficientes</small>), los cuadros por segundo (<small>no necesitamos un *framerate* de 30 ó 60 cuadros por segundo, con 5 puede ser suficiente</small>). Por ejemplo:  
> <span style="background-color:#042206"><span style="color:lime">`ffmpeg -i `*input_ficherodevideo* `-vf fps=5,scale=450:-1 `*output_fichero_gifanimado_resultante*</span></span>  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Extracción de imagenes de un video mediante ffmpeg (GNU Linux)](https://hijosdeinit.gitlab.io/howto_ffmpeg_extraer_fotogramas_de_archivo_video/)
- [[HowTo] Convertir vídeo '.ogv' a otros formatos](https://hijosdeinit.gitlab.io/howto_convertir_ogv_a_otros_formatos/)
- [[HowTo] Reparar el índice de un video dañado](https://hijosdeinit.gitlab.io/howto_reparar_indice_de_un_video/)
- [[HowTo] Incorporar pista de audio a una pista de video](https://hijosdeinit.gitlab.io/howto_incorporar_pista_audio_a_video/)
- [[HowTo] Trocear un video](https://hijosdeinit.gitlab.io/howto_trocear_video/)
- [[HowTo] Cálculo del bitrate de un video para su recompresión en 2 pasadas](https://hijosdeinit.gitlab.io/howto_calculo_bitrate_video_recompresion_2_pasadas_ffmpeg_handbrake/)
- [[HowTo] reparar mediante mencoder archivo de video dañado](https://hijosdeinit.gitlab.io/howto_reparar_mediante_mencoder_archivo_de_video_da%C3%B1ado/)
- [[HowTo] Extraer mediante ffmpeg el audio de un archivo de video](https://hijosdeinit.gitlab.io/howto-ffmpeg-Extraer-audio-de-archivo-de-video/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://compendiumdev.storychief.io/how-to-convert-video-to-animated-gif-using-ffmpeg">copendiumdev.storychief.io</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
