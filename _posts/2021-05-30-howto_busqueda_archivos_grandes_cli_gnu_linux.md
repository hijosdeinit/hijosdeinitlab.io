---
layout: post
title: "[HowTo] Búsqueda de archivos grandes desde línea de comandos en GNU Linux"
date: 2021-05-30
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "busqueda", "du", "archivos", "directorios", "linea de comandos", "cli"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## 1. Mediante el comando 'find'
Búsqueda, mediante el comando 'find', de los archivos y/o directorios de mayor tamaño en un disco:  
<span style="background-color:#042206"><span style="color:lime"><big>**`sudo find` *rutadondedeseamosbuscar* `-printf '%s %p\n'| sort -nr | head -`*númeroderesultadosquequeremosvisualizar***</big></span></span>  

<br>
Por ejemplo, para buscar los 10 archivos y/o directorios de mayor tamaño en la raíz del sistema ('/'):
~~~bash
sudo find / -printf '%s %p\n'| sort -nr | head -10
~~~

<br>
Si se desea buscar tan sólo <big>**archivos**</big> bastará con añadir el parámetro <span style="background-color:#042206"><span style="color:lime">`-type f`</span></span>. Y, si por el contrario exclusivamente se desea buscar <big>**directorios**</big> el parámetro a añadir es <span style="background-color:#042206"><span style="color:lime">`-type d`</span></span>:  
<span style="background-color:#042206"><span style="color:lime">`sudo find` *rutadondedeseamosbuscar* `-type d -printf '%s %p\n'| sort -nr | head -`*númeroderesultadosquequeremosvisualizar*</span></span>  
Por ejemplo:
~~~bash
sudo find / -type d -printf '%s %p\n'| sort -nr | head -10
~~~
<span style="background-color:#042206"><span style="color:lime">`sudo find` *rutadondedeseamosbuscar* `-type f -printf '%s %p\n'| sort -nr | head -`*númeroderesultadosquequeremosvisualizar*</span></span>  
Por ejemplo:
~~~bash
sudo find / -type f -printf '%s %p\n'| sort -nr | head -10
~~~

<br>
Naturalmente se pueden añadir más filtros (véase [<span style="background-color:#042206"><span style="color:lime">`man find`</span></span>](https://www.mankier.com/1/find)) agregando parámetros como <span style="background-color:#042206"><span style="color:lime">`-iname`</span></span> para limitar la búsqueda a tan sólo ciertos nombres o extensiones:  
<span style="background-color:#042206"><span style="color:lime">`sudo find` *rutadondedeseamosbuscar* `-type f -iname "`*filtrodenombreoextensiondearchivo*` -printf '%s %p\n'| sort -nr | head -`*númeroderesultadosquequeremosvisualizar*</span></span>  
Por ejemplo:
~~~bash
sudo find / -type f -iname "*.txt" -printf '%s %p\n'| sort -nr | head -10
~~~
... mostrará los 10 mayores ***archivos*** de extensión ***'.txt'*** existentes en raíz (y todos sus subdirectorios).  

<br>
## 2. Mediante el comando 'du' acompañado de 'sort' y 'head'
Desde la terminal, una vez **ubicados en la ruta donde deseamos buscar** (mediante el comando <span style="background-color:#042206"><span style="color:lime">`cd`</span></span>):  
<span style="background-color:#042206"><span style="color:lime"><big>`du -hsx * | sort -rh | head -10`</big></span></span>  
... mostrará los 10 mayores archivos existentes en el interior de esa ruta.  
Explicación de los parámetros:  

<br>
* `du -hsx *`  
[du](https://www.mankier.com/1/du)  
`-h` <small>muestra los tamaños de archivo de forma humanamente legible (p.ej. 1K 234M 2G).</small>  
`-s` <small>muestra sólo el total para cada argumento.</small>  
`-x` <small>descarta directorios de sistemas de archivos diferentes.</small>  
`*` <small>cualquir nombre de archivo, cualquier extensión.</small>  
* `sort -rh`  
[sort](https://www.mankier.com/1/sort)  
`-r` <small>reverse the result of comparisons</small>  
`-h` <small>muestra los resultados de forma humanamente legible.</small>  
* `head -10`  
[head](https://www.mankier.com/1p/head)  
`-10` <small>indica la cantidad máxima de resultados que se desea visualizar.</small>  

<br>
<br>
Entradas relacionadas:  
- [Búsquedas desde la línea de comandos en GNU Linux](https://hijosdeinit.gitlab.io/howto_busquedas_CLI_gnulinux/)
- [[HowTo] Cálculo del número de archivos y subdirectorios que contiene un directorio mediante 'find'](https://hijosdeinit.gitlab.io/howto_calcular_numero_archivos_y_o_subdirectorios__find/)  

<br>
<br>
<br>

---  
<small>Fuentes:  
<a href="https://blog.desdelinux.net/busca-los-directorios-o-archivos-mas-grande-de-tu-disco-duro-con-find/">DesdeLinux</a>  
<a href="https://fututel.com/es/tutoriales-guias-manuales-videotutoriales/2711-ver-y-enumerar-los-10-archivos-mas-grandes-en-linux">fututel</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
