---
layout: post
title: "[HowTo] Instalación del navegador web 'Brave' en Debian 10 (64 bits) y derivados"
date: 2021-05-24
author: Termita
category: "sistemas operativos"
tags: ["brave", "browser", "web browser", "http", "https", "navegador", "navegador web", "privacidad", "sistemas operativos", "ubuntu", "debian", "debian 10", "buster", "curl", "gnu linux", "linux", "internet 4.0", "software", "apt"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Brave es un navegador web que supuestamente hace hincapié en la privacidad.  
Según la [página oficial](https://brave.com/linux/) el procedimiento para instalar 'Brave' en GNU Linux Ubuntu 18 es el siguiente:
~~~bash
sudo apt-get update

sudo apt install apt-transport-https

sudo apt install curl

sudo apt install gnupg

sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg

echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list

sudo apt-get update

sudo apt install brave-browser
~~~
(*) El uso de 'curl' tiene sus riesgos, aunque en este caso sea para añadir claves gpg y fuentes (sources). Nunca está de más revisar el código.  

<br>
<br>
Entradas relaccionadas:  
[[HowTo] Instalación del navegador web 'Brave' en Ubuntu 18 y similares - septiembre 2019](https://hijosdeinit.gitlab.io/howto_instalacion_Brave_Ubuntu18_y_similares/)  

<br>
<br>
<br>

--- --- ---  
<small>Fuentes:  
<a href="https://brave.com/linux/">Brave - documentación oficial</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
