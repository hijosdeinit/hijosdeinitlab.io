---
layout: post
title: "[HowTo] Instalación (y desinstalación) de Google Chrome en Debian 64bits y derivados"
date: 2021-10-20
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "internet 4.0", "navegador", "web", "browser", "web browser", "gnu linux", "linux", "firefox", "chrome", "chromium", "opera", "librewolf", "waterfox", "midori", "brave", "iceweasel", "gnome web", "web browser", "navegador web", "privacidad", "cohortes", "publicidad", "maltrato", "bdsm", "sadomasoquismo"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>¿Para qué un ser humano que se respete a sí mismo podría querer instalar 'Chrome' el navegador web de Google? Masoquismos a parte, siempre hay una buena causa... y, además, ¿qué necesidad hay de juzgar? Juzga y serás juzgado.  
*Excusatio non petita, accusatio manifesta*. En mi caso, procedí a instalar 'Chrome' porque al proceder con la instalación nativa (mediante `apt`) de un software de "gestión del conocimiento" llamado ['Obsidian' - <small>véase entrada en este blog</small>]() obtenía un mensaje de error relacionado con 'chrome-sandbox'(`chmod: no se puede acceder a '/opt/obsidian/chrome-sandbox': No existe el fichero o el directorio`). Mas una vez instalado 'Chrome' constaté mediante el tan recurrente recurso al 'acierto-error' que la intranscendente ausencia del navegador de Google en mi sistema GNU Linux no era la causa del error. Así que eché un vistazo al nuevo 'Chrome', saqué nuevas conclusiones y procedí a su completa desinstalación. No lo echaré de menos, como tampoco lo echaba de menos antes.</small>  

<br>
## Procedimiento de instalación de 'Google Chrome' en Debian 10 y derivados
Google descontinuó la versión de 32bits de 'Chrome' y actualmente sólo desarrolla la versión de 64bits. En lo que a 'Chrome' respecta, si hoy se desesa incorporar al sistema una versión de 32bits, hay 2 alternativas:
- 'Chromium' para 32bits
- [Última versión existente de 'Chrome' para 32 bits ('google-chrome-stable_48.0.2564.116-1_i386.deb')](https://archive.org/download/google-chrome-stable_48.0.2564.116-1_i386/google-chrome-stable_48.0.2564.116-1_i386.deb).  
<br>
Me centraré en la instalación de 'Chrome' en su versión de 64bits:
~~~bash
sudo apt-get update
wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
sudo apt install ./google-chrome-stable_current_amd64.deb
~~~
Durante la instalación se creará el archivo '/etc/apt/sources.list.d/google-chrome.list' en nuestro sistema. Este archivo contiene una única línea, que corresponde al repositorio que 'Google' mantiene para 'Chrome':
> `deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main`  

## Postinstalación de 'Chrome': recortándole las alas
En casos como el que nos ocupa, la postinstalación, por supuesto, suele consistir en eliminar todos esos parámetros de violación sistemática de la privacidad que el software -'Chrome' en este caso' trae activados por defecto: sistema de cohortes, telemetría, envío de datos de uso, etcétera.  
Recién instalado, cuando ejecutamos Google Chrome por primera vez, éste pregunta si deseamos que comparta con su amo -que obviamente no somos nosotros, sino Google- datos relacionados con errores del navegador y telemetría (datos de nuestro uso).  
Tengo por costumbre darle un **NO** a estos manejos tan en boga hoy (<small>aunque, a saber... porque para las entendederas de una corporación como Google un "no" puede convertirse un "sí"; a eso se arriesga uno cuando masoquea con los productos del que, probablemente, se considera el "amo de internet"</small>).  
Mas, a parte de ese detalle, hay algunos más; aunque, a diferencia de éste, éstos están activados por defecto y -como cabe esperar del dueño del navegador hegemónico 'Chrome'- NO, no son por nuestro bien.  
Y son esas configuraciones relacionadas con nuestro **perfil publicitario**, el cuento ese de las **cohortes**, la cuantificación de nuestra actividad y, por consiguiente, intimidad... el maltrato en definitiva.  
Todos estos parámetros, es necesario **deshabilitarlos**.  
Y, finalmente, otras cosas como el "autocompletado", el "guardado de contraseñas", el "guardado de los métodos de pago", el "acceso a dispositivos como el micrófono o la cámara", etcétera.  
Los **deshabilito** también, total... ¿para qué?.  

<br>
Es todo lo enunciado trabajo cuasi vano, pues a continuación ha de venir inexorablemente el procedimiento más importante, de cuyo éxito jamás tendremos certeza absoluta ya que -desconozco si se habrán dado cuenta- 'Chrome' es, como todo lo que vemos que hace Google o las grandes tecnológicas aka *Big Tech*, lo que antaño todos llamábamos ***MALWARE***:

<br>
## Procedimiento de desinstalación de 'Google Chrome' en Debian 10 y derivados
~~~bash
sudo apt purge google-chrome-stable
sudo apt autoremove
~~~

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://linoxide.com/install-google-chrome-on-debian/">Linoxide</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
