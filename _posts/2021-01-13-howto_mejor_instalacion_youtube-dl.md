---
layout: post
title: "[HowTo] Mejor forma de instalar 'youtube-dl' en Debian y derivados. I"
date: 2021-01-13
author: Termita
category: "sistemas operativos"
tags: ["multimedia", "ffmpeg", "video", "youtube-dl", "Debian", "gnu linux", "linux", "sistemas operativos", "pista", "audio", "subtitulos", "descargas", "degoogled", "tracking", "privacidad", "web 4.0", "freetube", "newpipe"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
A mi entender, **la mejor forma de instalar 'youtube-dl' es descargándolo desde su [página oficial](https://youtube-dl.org/)**.  
<br>
Porque instalarlo desde los repositorios oficiales de Debian implica que muy probablemente la versión de 'youtube-dl' que obtengamos no será la más actualizada, y eso -dado que el "software" del servicio Youtube se actualiza constantemente- puede suponer que youtube-dl no trabaje correctamente.  
Cuando Youtube actualiza el software de su servicio, los creadores de 'youtube-dl' actualizan el programa.  
Instalar 'youtube-dl' desde repositorios no-oficiales como es el de [WebUpd8](http://www.webupd8.org/) supone exponer, en mayor o menor medida, el sistema operativo, o bien, que dicho repositorio tarde o temprano desaparezca... o tarde en incorporar laúltima versión.  
Por eso, siempre que se pueda, hay que recurrir al desarrollador original.  

<br>
Instalar 'youtube-dl' descargando el paquete directamente desde el [sitio oficial](https://youtube-dl.org/) o el [repositorio oficial](https://github.com/ytdl-org/youtube-dl/releases) -que tanto monta, monta tanto- es realmente sencillo:
~~~bash
sudo wget https://yt-dl.org/downloads/latest/youtube-dl -O /usr/local/bin/youtube-dl
sudo chmod a+rx /usr/local/bin/youtube-dl
~~~

Comprobamos que todo está correcto ejecutando en línea de comandos la orden para actualizar 'youtube-dl', por ejemplo:
~~~bash
youtube-dl -U
~~~
... a lo que el sistema responderá algo parecido a esto:
~~~
youtube-dl is up-to-date (2021.01.08)
~~~

<br>
<br>
En [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_youtube-dl_debian_netinstall/) se trata de **otra "mejor forma"** de instalar 'youtube-dl' en nuestro sistema Debian.  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.atareao.es/ubuntu/que-hacer-cuando-no-funciona-youtube-dl-en-ubuntu/">Atareao</a>  
<br>
<br>
<br>
<br>
<br>
<br>
