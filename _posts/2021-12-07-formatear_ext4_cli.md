---
layout: post
title: "[HowTo] Dar formato ext4 a un disco o partición linux desde línea de comandos (CLI)"
date: 2021-12-07
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "GNU Linux", "Linux", "ext4", "formato", "format", "mkfs.ext4"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
El disco ha de estar desmontado (`umount /dev/sdxx`), y el sistema de archivos del disco o partición que queremos formatear ha de ser linux (emplear 'fdisk' (opción 't') para ello si fuera menester).  
~~bash
mkfs.ext4 /dev/sdxx
~~~
  
<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://kwilson.io/blog/format-a-linux-disk-as-ext4-from-the-command-line/">kwilson.io - format linux disk as ext4</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
