---
layout: post
title: "[HowTo] Incorporando una unidad de diskette en 2021. vol. I"
date: 2021-06-12
author: Termita
category: "hardware"
tags: ["hardware", "diskette", "floppy disk", "floppy", "sistemas operativos", "linux", "gnu linux", "Windows", "sistema de archivos", "format", "formato", "retromatica", "286", "386", "486", "nostalgia", "retro", "vintage", "sneakernet"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Quería volver a experimentar lo que era trabajar con los viejos discos flexibles, también llamados *floppies* o *diskettes*, y mirar de sacarles partido. Conservo cientos de ellos, amén de unas cuantas unidades lectoras. El tableteo característico de la *diskettera*, el ritual de insertar o extraer el diskette... pueden despertar algo parecido a la nostalgia por aquello que uno experimentaba cuando siendo adolescente operaba y aprendía con un 286.  

<br>
La "era de los diskettes" es también denominada <big>*"sneakernet"*</big> dado que, a falta de una red de uso generalizado como es internet hoy, mediante estos dispositivos de almacenamiento se realizaba la mayoría de intercambios de datos. Todo *diskette* era obviamente susceptible de ser copiado en otro diskette, y así se había creado una especie de red en la que datos de todo tipo -incluídos virus informáticos como eran aquellos *Pakistani Brain*, *Telefónica* o *Stoned*- podían llegar a darle varias vueltas al mundo.  
Antes de que se popularizaran las redes, los archivos adjuntados por correo electrónico, el P2P y las unidades de almacenamiento flash usb, los usuarios de PC utilizábamos *diskettes* para, entre otras cosas, compartir o transferir archivos.  
En aquella época donde la mayoría de las computadoras no tendían precisamente a la portabilidad, el *diskette* facilitaba a las personas la libertad de usar o respaldar sus datos sin importar dónde éstos hubieran sido generados.  
La irrupción de los CD-ROM grabables y las unidades de almacenamiento USB acabaron con el reinado del disco flexible.  

<br>
Existen varios tipos de discos flexibles, cada uno con su lectora específica. Yo tuve 2 tipos:
<br>
<big>**De 5 1/4 pulgadas**</big>  
Conocí 2 tipos:
- De "Media Densidad": 512 Kb de capacidad
- De "Alta Densidad": 1'2 Mb de capacidad  

<br>
<a href="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/diskettera5,25.png" target="_blank"><img src="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/diskettera5,25.png" alt="Unidad de diskette de 5 1/4" width="600"/></a>  
<small>Unidad de *diskette* de 5 1/4</small>  
<br>

<br>
<big>**De 3 1/2 pulgadas**</big>  
De aparición posterior a los diskettes y disketteras de 5 1/4. Conocí 2 tipos:
- De "Media Densidad": 720 Kb de capacidad
- De "Alta Densidad": 1'4 Mb de capacidad  

<br>
<a href="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/diskettera3,5_diskette3,5.png" target="_blank"><img src="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/diskettera3,5_diskette3,5.png" alt="Unidad de diskette de 3 1/2" width="600"/></a>  
<small>Unidad de *diskette* de 3 1/2</small>  
<br>

<br>
Al igual que los discos duros, las *disketteras* necesitaban estar conectadas a la controladora de la placa base mediante un **cable de datos** y recibir, a través de otro cable, **alimentación** eléctrica de la fuente de alimentación.  

<br>
<a href="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/berg_conector_datos_diskettera_3,5.png" target="_blank"><img src="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/berg_conector_datos_diskettera_3,5.png" alt="cable 'Berg' de alimentación eléctrica de una diskettera de 3 1/2" width="200"/></a>  
<small>Cable 'Berg' de alimentación eléctrica de una **diskettera** de 3 1/2</small>  
<br>

<a href="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/cable_datos_diskettera_3,5.png" target="_blank"><img src="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/cable_datos_diskettera_3,5.png" alt="cable de datos de una diskettera de 3 1/2" width="300"/></a>  
<small>Cable de datos de una diskettera de 3 1/2</small>  
<br>

<br>
<br>
## ¿Para qué sirve una unidad de *diskette* hoy en día?  
A parte de la cuestión emocional:  
- Para no desaprovechar / despilfarrar los centenares de diskettes que muchos de nosotros aún no hemos tirado a la basura. <small>Generalmente los diskettes de primera generación, que además eran los más caros, son los más fiables</small>
- Para respaldar su contenido. Es muy probable que contengan información que ni recordábamos que estaba ahí, software difícil de conseguir hoy dia, etcétera.
- Para aprender a gestionarlos en GNU Linux: qué formatos admiten, cómo se formatean, etcétera.
- Para experimentar e innovar. Afirmativo, innovar, porque tecnologías -software y hardware- actuales pueden tratar de incorporarse a una tecnología vieja. Imagínese preparar un diskette cifrado, o un sistema de autentificación a partir de un frágil diskette, por ejemplo.  

<br>
<br>
## Cómo conectar una unidad de *diskette* a una computadora
Para empezar:  

<big>**1**</big> Si el ordenador es moderno y su placa base <big>NO</big> tiene conector específico para unidades de diskette hay 2 opciones:  

<br>
1.a. Adquirir un "conversor" que permita conectar (datos y alimentación) nuestra vieja *diskettera* por usb.  
Estos adaptadores suelen ser únicamente compatibles con *disketteras*/*diskettes* de alta densidad.  
<br>
<a href="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/adaptador_34pin_144mb_floppy-a-USB.jpg" target="_blank"><img src="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/adaptador_34pin_144mb_floppy-a-USB.jpg" alt="adaptador 34 pin floppy 3 1/2 1'44mb a USB" width="600"/></a>  
<small>Adaptador 34 pin diskette 3 1/2 1'44mb a USB</small>  
<br>
<a href="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/adaptador_34pin_144mb_floppy-a-USB_conectado.png" target="_blank"><img src="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/adaptador_34pin_144mb_floppy-a-USB_conectado.png" alt="conexión adaptador 34 pin floppy 3 1/2 1'44mb a USB" width="600"/></a>  
<small>Conexión de *diskettera* 3 1/2 mediante adaptador '34 pin floppy 3 1/2 1'44mb a USB'</small>  
<br>

1.b. Adquirir una *diskettera* usb moderna (es decir, ya hecha).  
Estas unidades suelen ser compatibles sólo con *diskettes* de alta densidad.  
<br>
<a href="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/diskettera3,5_usb.png" target="_blank"><img src="/assets/img/2021-06-12-howto_diskettera_floppy_linux_hoy_vol01/diskettera3,5_usb.png" alt="diskettera de 3 1/2 1'44 Mb USB moderna" width="600"/></a>  
<small>diskettera de 3 1/2 1'44 Mb USB moderna</small>  
<br>

<br>
<big>**2**</big> Si el ordenador es relativamente viejo y su placa base <big>SÍ</big> tiene conector específico para unidades de diskette -las placas del 2007, por ejemplo, aún tenían ese conector- hay 3 opciones:  
<br>
2.a. Conectar la diskettera de forma convencional (cable de datos a conector de datos, alimentación a fuente de alimentación).  
<br>
2.b. Adquirir un "conversor" que permita conectar (datos y alimentación) nuestra vieja diskettera por usb.  
<br>
2.c. Adquirir una diskettera usb moderna (es decir, ya hecha).  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Incorporando una unidad de diskette en 2021. vol. II: bregando con diskettes en GNU Linux](https://hijosdeinit.gitlab.io/howto_diskettera_floppy_linux_hoy_vol02/)
- [GNU Linux en 1 diskette: 'Floppinux', 'MuLinux'](https://hijosdeinit.gitlab.io/linux_en_1_diskette/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.silicon.co.uk/data-storage/storage/tales-tech-history-floppy-disk-209049">Tales in tech history: the floppy disk</a>  
<a href="https://en.wikipedia.org/wiki/Sneakernet">Wikipedia - sneakernet</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
