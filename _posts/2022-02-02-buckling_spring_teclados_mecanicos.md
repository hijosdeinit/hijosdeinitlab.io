---
layout: post
title: "Teclados mecánicos de hoy. La herencia del '*Buckling Spring*'"
date: 2022-02-02
author: Termita
category: "hardware"
tags: ["hardware", "sonido", "typewriter", "cherry", "teclado", "IBM", "keyboard", "teclas", "pulsación", "bucklespring", "typewriter-sounds", "keypress", "buckling spring", "ibm model f", "ibm model m", "unicomp", "switch", "pulsadores"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En el fondo es una tontería prescindible... mas a mí siempre me agradó el sonido de las teclas de los teclados de antes. Con la llegada de los teclados modernos se extendió la "era del silencio" y aquellos entrañables, sólidos y ruidosos teclados mecánicos quedaron relegados. Con qué inconsciencia a finales de los 90 tiré a la basura aquellos teclados, víctimas de la modernidad.  
Echo de menos los teclados Cherry, aquellos teclados de IBM... su sonido y la experiencia al pulsar aquellas teclas.  

![teclado mecánico]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-06-04-[HowTo]_BuckleSpring_sonido_pulsaciones_teclado/model-m_teclado.png)  

<br>
Es curioso, hay cosas que pareciera cambiaron a peor. Hay quienes dicen que los teclados son una de estas cosas.  
Confieso que echo de menos aquellos teclados mecánicos de los 80 y 90 del siglo pasado más por su sonido, su tacto y su robustez que por su rapidez. Mi velocidad no varía significativamente con un teclado de membrana de lo que antaño era con un "obsoleto" y, hoy obscenamente caro, teclado "buckling spring". Quizás antes, cuando lo que tenía era un teclado clónico de IBM, escribía más deprisa, mas también era más joven, con más reflejos y más prisa. Lo que es innegable es que las sensaciones no eran equiparables a las que los teclados modernos dan.  

<br>
## ¿Qué es 'Buckling Spring'?
Es una patente de IBM -[U.S. Patent 4,118,611](https://patents.google.com/patent/US4118611) y [U.S. Patent 4,528,431](https://patents.google.com/patent/US4528431)-, la ingeniería, en definitiva, que había detrás de teclados como el 'model B' de los años 80. Era un sistema de muelle que al ser comprimido inclinaba repentinamente una placa que, a su vez, impactaba en una membrana, cerraba el circuíto eléctrico y "generaba" la pulsación de cada una de las teclas. Era, como he señalado antes, un sistema muy robusto, sin latencia alguna, y con un tacto y sonido muy característicos. Aquellos teclados eran muy caros de fabricar, comparados con los actuales.  

<br>
![tecla mecánica]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-06-04-[HowTo]_BuckleSpring_sonido_pulsaciones_teclado/buckle.gif)
<br>

<br>
## Los teclados mecánicos en la actualidad
Años más tarde, el "Buckle Spring" y aquellos teclados de 2 kg con conexiones DIN o PS/2 se consideraron obsoletos. Los fabricantes vendían los ligerísimos teclados de membrana, los perfiles bajos, otros eran ergonómicos, otros tenían la apariencia de una tableta de pastillas de chicle, las conexiones siguieron siendo ps/2 para luego convertirse en USB. Todo eso era considerado algo moderno y positivo, apartado de aquellos teclados mecánicos ancestrales y toscos.  
Hoy se les echa de menos, se han puesto incluso de moda, y se pagan auténticas fortunas por lo que hoy llaman *vintage*, es decir -en este caso- aquello que antaño -considerado vetusto y obsoleto- acababa en el cubo de la basura. En esta mirada al pasado confluyen esnobismo / moda, nostalgia y pragmatismo. Hay, incluso, toda una actividad -un auténtico *hobby*- de *upgradeo* de variantes modernas de aquellos teclados mecánicos: hay diversos tipos de *switches*, amortiguadores, luces, lubricaciones, sonidos, longitud de pulsación, perfiles, etcétera...  
Asimismo aún existe una empresa -[Unicomp](https://www.pckeyboard.com/)- fundada en 1996 por antiguos trabajadores de IBM que adquirieron la licencia de 'Buckling Spring' de 'IBM' que fabrica teclados prácticamente idénticos a aquellos 'Model M' o 'Model F' ochenteros. Son caros, los hay rondando los 100$.  

![](/assets/img/2022-02-02-buckling_spring_teclados_mecanicos/Unicomp-spacesaver-104.jpg)  

![](/assets/img/2022-02-02-buckling_spring_teclados_mecanicos/unicomp_new_model_M.jpg)  

En el mundo de los jugones (*gamers*) y de los programadores también se han popularizado los teclados mecánicos. Los fabricantes se han apresurado a poner en el mercado multitud de ellos: con luz, programables, con *switches* intercambiables, con conectores chapados en oro, etc, etc...  
En lo que a mí respecta... acabo de comprarme de oferta (39€, y me parece un precio desproporcionado) un teclado de esos, de una marca / empresa que dicen que es española (aunque casi con total seguridad fabricado en esa modélica democracia llamada China, paraíso proletario). Es una especie de imitación moderna y actualizada de aquellos vetustos teclados de IBM con los que a finales de los ochenta y principios de los noventa pasábamos las horas muertas tecleando frente a WordPerfect, dbaseIII o F29 'retaliator'. Tableteos inolvidables. Pienso que me gustará su sonido y que, siendo programable -se puede configurar para que se iluminen las teclas que uno quiera-, me servirá en mi aprendizaje de 'vim'. De la velocidad, ¿qué decir? Que no espero gran cosa; los años no pasan en balde.  
Quién sabe, igual dentro de 50 años valdrá su peso en oro. Mas yo creo que no.  

<br>
<br>
<br>
Entradas relacionadas:  
- [https://hijosdeinit.gitlab.io/howto_BuckleSpring_sonido_pulsaciones_teclado/](https://hijosdeinit.gitlab.io/howto_BuckleSpring_sonido_pulsaciones_teclado/)
- []()  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[Wikipedia - 'Buckling Spring'](https://en.wikipedia.org/wiki/Buckling_spring)  
[The Buckling Spring](https://www.bucklingspring.com/)</small>  

<br>
<br>
<br>
<br>
