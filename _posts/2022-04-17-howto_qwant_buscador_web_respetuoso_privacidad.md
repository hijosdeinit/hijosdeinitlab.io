---
layout: post
title: "[HowTo] 'QWant': otro buscador respetuoso con la privacidad"
date: 2022-04-17
author: Termita
category: "internet 4.0"
tags: ["internet 4.0", "buscador", "metabuscador", "web", "altavista", "qwant", "searx", "fuckoffgoogle", "privacidad", "ads", "anuncios", "publicidad", "no ads", "tracking", "traqueo", "rastreo", "seguimiento", "cookies", "neutralidad", "perfil", "monetizacion", "google", "servidor", "libertad"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
# 'QWant'
['Qwant'](https://www.qwant.com/) es un buscador web europeo que, desde  2013 -fecha de su creación-, asegura garantizar la privacidad del internauta, ya que, dicen no recoge datos personales de las consultas (ip, etc...) y prohíben la ejecución de rastreadores publicitarios y la creación de perfiles con fines comerciales.  
Asimismo, asegura garantizar la neutralidad e imparcialidad de los resultados de la búsquedas. No censuran ni priorizan resultados.  
Hay quienes -en esta era del internet 4.0 y la cuantificación, rastreo, análisis y monetización- lo consideran el motor de búsqueda más respetuoso con sus usuarios.  

<br>
'QWant' ofrece una [extensión](https://addons.mozilla.org/en-US/firefox/addon/qwantcom-for-firefox/) para navegadores como 'Mozilla Firefox' que permite establecerlo como motor de búsqueda principal.  
Sin embargo, cabe advertir, que los desarrolladores de 'Mozilla Firefox' «no monitorizan la seguridad de esta extensión y no se hacen responsables de ésta». Queda, pues, sujeta al propio arbitrio del usuario.  

<br>
<br>
<br>
##### <small>**ANTECEDENTES y CONTEXTO**</small>  
<small>Hace muchos años 'Google' ('Alphabet') y/o su **"buscador"** ni siquiera existían. A principios y mediados de los años 90 del siglo pasado la web estaba compuesta sobre todo por servidores de instituciones educativas, científicas, y algunas páginas personales ubicadas en servidores "de pago" o sitios "gratuítos" como ['Geocities'](https://es.wikipedia.org/wiki/GeoCities).  
Por entonces, disponer de una cuenta de correo electrónico era un "privilegio" disponible tan sólo para unos pocos internautas afortunados que, generalmente, formaban parte de alguna institución o, que -de su bolsillo- pagaban por disponer de ello. Luego -1996- llegó [Hotmail](https://es.wikipedia.org/wiki/Outlook_(correo_web)#Lanzamiento_de_Hotmail) y el panorama al respecto cambió: una cuenta de correo electrónico "gratuíta" a cambio de tus datos, de tu privacidad, de ser comprado y vendido como un objeto. Y así fue, años más tarde -1997-, Microsoft adquirió Hotmail, con toda su cartera de "clientes".  
La publicidad en aquellos años era escasísima, limitada a algunos *banners*, y generalmente poco intrusiva. La cuantificación, catalogación, rastreo y monetización del internauta brillaba por su ausencia. [Amazon](https://en.wikipedia.org/wiki/Amazon_(company) (1994) era una simple librería pionera en la venta de su producto -libros- por internet, y lo que hoy es [Netflix](https://en.wikipedia.org/wiki/Netflix), por entonces no era ni siquiera el videoclub que fue (se fundó en 1997).  
El **buscador de referencia** era [Altavista](https://digital.com/altavista/), cuya URL era 'http://altavista.digital.com' y fue lanzado por [Digital Equipment Corporation](https://es.wikipedia.org/wiki/Digital_Equipment_Corporation) -una empresa desarrolladora de megacomputadoras- en 1995 para poner a prueba las capacidades de uno de sus superordenadores. A su sombra estaba el buscador de [Yahoo](https://es.wikipedia.org/wiki/Yahoo!), nacido en 1994, cuyo objeto era (y es) el lucro, comprar y vender.  
En [este blog se le dedicó un artículo a 'Altavista, su historia y su "legado"'](https://hijosdeinit.gitlab.io/internet1.0_la_era_Altavista/), un buscador -el mejor de su era- que no fue creado para tomar internet por asalto o aprovecharse de una oportunidad comercial en auge.  
<small>'Alphabet'-'Google', 'Amazon', 'Yahoo', 'Microsoft', 'Hotmail' / 'MSN', 'Netflix'..., hay que ver cómo, escribiendo sobre buscadores, han ido saliendo retratados todos los enemigos del internauta de a pie, que -constituídos en *'Big Tech'*- también lo son -tal como están las cosas hoy- de la humanidad, a la cual maltratan.</small></small>  
<br>
<a href="/assets/img/2021-09-03-howto_fuckofgoogle_metabuscador/altavista1995.jpg" target="_blank"><img src="/assets/img/2021-09-03-howto_fuckofgoogle_metabuscador/altavista1995.jpg" alt="el buscador 'Altavista' en 1995" width="300"/></a>  
<small><small>el buscador 'Altavista' en 1995</small></small>  
<br>

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] 'searX', un metabuscador *hackable* que busca la privacidad](https://hijosdeinit.gitlab.io/howto_searx_metabuscador/)
- [[HowTo] el metabuscador 'FuckOffGoogle y su proyecto de libertad](https://hijosdeinit.gitlab.io/howto_fuckofgoogle_metabuscador/)
- ['DuckDuckGo' NO es de fiar](https://hijosdeinit.gitlab.io/googlizacion_de_duckduckgo_censurando_contenidos_adulterando_busquedas/)
- [[HowTo] Buscadores web respetuosos con el internauta](https://hijosdeinit.gitlab.io/buscadores_web_respetuosos/)
- ['Altavista' y su era. Internet 1.0](https://hijosdeinit.gitlab.io/internet1.0_la_era_Altavista/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/Qwant">Wikipedia - 'QWant'"</a>  
<a href="https://www.adslzone.net/noticias/internet/qwant-buscador-privado-internet/>AdslZone"</a>  
<<a href="zzz">zzz</a>/small>  

<br>
<br>
<br>
<br>
<br>
<br>
