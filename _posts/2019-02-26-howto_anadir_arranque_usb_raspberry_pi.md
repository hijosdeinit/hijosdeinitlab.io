---
layout: post
title: '[HowTo] Añadir soporte de arranque usb a RaspBerry Pi 3b'
date: 2019-02-26
author: Termita
category: "hardware"
tags: ["hardware", "RaspBerry Pi", "usb", "hdd usb", "disco duro", "pendrive", "usb stick", "microSD", "SD", "arranque", "boot", "firmware", "Raspbian", "sistemas operativos"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
(*) Este procedimiento se puede llevar a cabo desde la propia Raspberry Pi, o bien de forma remota (por SSH, por ejemplo).  

1. Arrancamos RaspBian en RaspBerry Pi (obviamente desde microSD pues aún no tendremos activado el arranque desde USB).  

2. De forma remota -por SSH- o bien desde la propia RaspBerry Pi:  

Actualizamos el sistema
~~~bash
sudo apt-get update
sudo apt-get upgrade
~~~

Y a continuación <big>**procedemos a activar el soporte de arranque USB**</big>, que quedará **perpetuamente activado** en la máquina. Esto significa que, aunque cambiemos de sistema operativo, RaspBerry Pi seguirá podiendo arrancar desde dispositivos USB.
~~~bash
echo program_usb_boot_mode=1 | sudo tee -a /boot/config.txt
sudo reboot
~~~

Para comprobar que el procedimiento se ha realizado correctamente basta con ejecutar:
~~~bash
vcgencmd otp_dump | grep 17:
~~~
>17:3020000a  

Si el sistema "contesta" '17:3020000a' significa que todo ha salido bien.
<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[atareao](https://www.atareao.es/tutorial/raspberry-pi-primeros-pasos/volando-con-la-raspberry-desde-usb/#)</small>  
<br>
<br>
<br>
<br>
