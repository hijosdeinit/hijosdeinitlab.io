---
layout: post
title: '[HowTo] Provocar analisis disco del sistema en GNU Linux'
date: 2020-09-26
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "disco", "particion", "linux", "e2fsck", "ext4"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Cuando intentamos realizar un análisis de integridad del disco que contiene un sistema operativo Linux que está funcionado -es decir, que está arrancado- sencillamente el sistema lo no permite.  

## ¿Cómo forzar el análisis del disco del sistema en GNU Linux?  
Desconozco la forma de llevarlo a cabo en caliente.
Sin embargo, si en **raíz** (/) creamos un archivo vacío de nombre 'forcefsck', el sistema comprobará el disco al reiniciar.  
~~~bash
sudo touch /forcefsck
sudo reboot
~~~

<br>
<br>
## ¿Cómo saber cuándo fue la última vez que se comprobó un disco?
~~~
sudo tune2fs -l /dev/sdxx | grep checked
~~~
Por ejemplo:
~~~
sudo tune2fs -l /dev/sda1 | grep checked
~~~

<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[linux uprising](https://www.linuxuprising.com/2019/05/how-to-force-fsck-filesystem.html)</small>  

<br>
<br>
<br>
<br>
