---
layout: post
title: "¿Cargadores (de teléfonos móviles) con 'premio'?"
date: 2020-04-20
author: Termita
category: "seguridad"
tags: ["sistemas operativos", "otg", "cargador", "bateria", "energia", "malware", "virus", "troyano", "hack", "crack", "seguridad", "cifrado", "ramsonware", "android", "windows mobile", "ios", "apple", "google", "microsoft", "xiaomi", "redes", "internet"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Hace nada hablaban en 'Elotrolado' de un *malware* que los israelíes estaban probando para, previa infección de un sistema que tuviera "restringido o monitorizado" el acceso convencional a su disco duro, extraer datos del disco duro haciendo que los ventiladores emitieran unos sonidos concretos que serían recogidos localmente por un micrófono. Algo un tanto desconcertante.... Supongo que de alguna manera el malware se saltará los privilegios de lectura de los archivos, por ejemplo. Es posible que sea cierto... o no, mas la maniobra resulta inquietante.  
<br>
Pues bien, hoy dicen que [uno de los chips de un cargador de 'Xiaomi' es vulnerable](https://www.adslzone.net/noticias/productos/xiaomi-cargador-65w-retirada/) dado que no emplea ningún tipo de cifrado en su *firmware*, con lo que éste no ha de ser desencriptado para modificarse.  
<br>
Chips, firmware... en los cargadores. Y eso se conecta vía USB a un móvil que es capaz, no sólo de cargar su batería, sino de transmitir datos o incorporar dispositivos de almacenamiento por ahí.  
<br>
Creo que fue en 'DekNet' o en 'Reality Cracking'... o en 'Sobre la marcha'... o quizás en 'Wintablet'... no recuerdo... que -hace casi un año- se mencionó la paradoja de que una persona pusiese todos los medios posibles para salvaguardar la privacidad y la seguridad de su sistema mientras que, por otro lado, adquiriera -ya no sólo un móvil chino o de marca poco fiable (nada es fiable, pero unas cosas lo son más que otras)- sino cualquier tipo de cargador, a cual menos confiable.
Resulta asimismo gracioso el amancebamiento de móviles de mil euros con cargadores y cables de a 1 €: ricachones follando con putas callejeras, Porsche Cayenne repostando 5€ en la gasolinera del Alcampo.  
<br>
Es posible que si nos diera por abrir los cargadores y cables usb que adquirimos nos lleváramos una sorpresa.  
<br>
Cámaras ip, cargadores, cables, teléfonos móviles, routers, procesadores, placas base... pendrives, discos duros.... Da que pensar.  

<br>
<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="">zzzzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

