---
layout: post
title: "[HowTo] Desplegar 'Slax' en un disco o en una unidad de almacenamiento usb"
date: 2021-12-07
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "GNU Linux", "Linux", "slax", "instalacion", "bootinst", "liveusb", "persistente"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>[Slax](https://www.slax.org/) es «Slax is a modern, portable, small and fast Linux operating system with modular approach and outstanding design. It runs directly from your USB flash drive without installing, so you can carry it everywhere you go in your pocket. Despite its small size, Slax provides nice graphical user interface and wise selection of pre-installed programs, such as a Web browser, Terminal, and more.Slax is now based on Debian, which gives you the ability to benefit from its entire ecosystem. Tens of thousands of prebuilt packages with applications, all within reach thanks to apt command.»</small>  

<br>
!['Slax'](https://www.slax.org/images/new/Laptop-with-on-screen.png)  
<br>
Descomprímase el subdirectorio '/slax' -contenido en el archivo ''.iso' de 'Slax'- en la raiz del dispositivo de almacenamiento que se desea albergue ese sistema operativo.  
A continuación, sitúese en el subdirectorio '/slax/boot' de ese dispositivo de almacenamiento, y desde la terminal ejecútese como superusuario:
~~~bash
./bootinst.sh
~~~

El sistema así construído es **PERSISTENTE**, es decir, conserva los cambios.  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.slax.org/starting.php">slax.org - comenzando</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
