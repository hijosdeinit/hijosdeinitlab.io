---
layout: post
title: "[HowTo] Una forma ortodoxa de crear '.iso' a partir de un DVD o CD mediante 'dd'"
date: 2021-11-17
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "dd", "respaldo", "backup", "iso", "img", "imagen", "block size", "bs", "count", "isoinfo"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
La construcción de imagenes '.iso' a partir de CD ô DVD no es compatible con ciertos tamaños de bloque (*'block size'*).  
Por consiguiente, la mejor forma de construir la orden que, mediante el comando 'dd', ejecutaremos para construir una '.iso' a partir de un CD ô DVD es averiguando los parámetros relativos al 'tamaño de bloque' (*<span style="background-color:#042206"><span style="color:lime">bs</span></span>*) y 'tamaño del volumen' (*<span style="background-color:#042206"><span style="color:lime">count</span></span>*).  

<br>
Con el comando <big>**'isoinfo'**</big>, y -por ilustrarlo con un ejemplo- suponiendo que el DVD es el dispositivo '/dev/sr1':  
~~~bash
sudo isoinfo -d -i /dev/sr0
~~~
El comando "responderá":
~~~
CD-ROM is in ISO 9660 format
System id: LINUX
Volume id: CDROM
Volume set id:
Publisher id:
Data preparer id:
Application id: MKISOFS ISO 9660/HFS FILESYSTEM BUILDER & CDRECORD CD-R/DVD CREATOR (C) 1993 E.YOUNGDALE (C) 1997 J.PEARSON/J.SCHILLING
Copyright File id:
Abstract File id:
Bibliographic File id:
Volume set size is: 1
Volume set sequence number is: 1
Logical block size is: 2048
Volume size is: 920831
El Torito VD version 1 found, boot catalog is in sector 382
Joliet with UCS level 3 found
Rock Ridge signatures version 1 found
Eltorito validation header:
Hid 1
Arch 0 (x86)
ID ''
Key 55 AA
Eltorito defaultboot header:
Bootid 88 (bootable)
Boot media 0 (No Emulation Boot)
Load segment 0
Sys type 0
Nsect 4
Bootoff 17F 383
~~~
De esta información tomamos 2 valores:
> <span style="background-color:#042206"><span style="color:lime">`Logical block size is: 2048`</span></span> y <span style="background-color:#042206"><span style="color:lime">`Volume size is: 920831`</span></span>  

<br>
Y, a partir de éstas, constrúyase la orden del comando <big>**'dd'**</big> que, en el ejemplo que nos ocupa, ha de ser:
~~~bash
sudo dd if=/dev/sr1 of=juegaLinEx_2005.iso bs=2048 count=920831 conv=fsync status=progress
~~~
El parámetro 'count' es opcional.  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://linux.101hacks.com/unix/create-iso-file-from-cd-dvd/">LinuxHacks</a></small>  
<br>
<br>
<br>
<br>
<br>
<br>
