---
layout: post
title: "[HowTo] Solucionar error en 'imagemagick' (comando 'convert') y 'ghostscript' al convertir imagenes a '.pdf' en Debian 10 y derivados"
date: 2021-06-13
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "imagemagick", "ghostscript", "convert", "imagen", "pdf", "conversion", "exportar"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Entorno: Debian 10 Buster, versión imagemagick: 6 , versión ghostscript (`gs --version`): 9.27 .  

<br>
En Debian 10 Buster, de serie, el comando 'convert', que pertenece al paquete 'imagemagick' y sirve -entre otras cosas- para convertir imagenes a '.pdf' (<span style="background-color:#042206"><span style="color:lime">`convert imagen1 imagen2 archivo.pdf`</span></span>), no cumple su cometido al hacer esa tarea y "obsequia" con un error:  
> convert-im6.q16: attempt to perform an operation not allowed by the security policy `PDF' @ error/constitute.c/IsCoderAuthorized/408.  

<br>
Solución:  

<br>
1 . Respáldese el archivo '/etc/ImageMagick-6/policy.xml'
~~~bash
sudo cp /etc/ImageMagick-6/policy.xml /etc/ImageMagick-6/policy.xml.bkp001
~~~

<br>
2 . Edítese el archivo '/etc/ImageMagick-6/policy.xml'
~~~bash
sudo nano /etc/ImageMagick-6/policy.xml
~~~
... borrando o comentando la línea: <span style="background-color:#042206"><span style="color:lime">`<policy domain="coder" rights="none" pattern="PDF" />`</span></span>  

<br>
Básicamente lo que se consigue así es anular la configuración `<policy domain="coder" rights="none" pattern="PDF" />`.  

<br>
<br>

--- --- ---  
<small>Fuentes:  
<a href="https://stackoverflow.com/questions/52998331/imagemagick-security-policy-pdf-blocking-conversion">Stack Overflow</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
