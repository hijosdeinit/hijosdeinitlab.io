---
layout: post
title: "'sudo', algunos parámetros útiles"
date: 2021-05-20
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "Debian", "liveusb", "livecd", "persistente", "dd", "iso", "boot", "sudo", "contraseña", "password", "sudoers", "sudoers.d", "root", "usuario"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Consultar la configuración de 'sudo' activa para el usuario:
~~~bash
sudo --list
~~~

<br>
Resetear las credenciales de sudo guardadas temporalmente para que vuelva a solicitar contraseña.
~~~bash
sudo -k
~~~

<br>
Similar al parámetro anterior. Borra las credenciales del usuario *cacheadas* temporalmente. Este parámetro no puede usarse en conjunción con un comando.
~~~
sudo -K
~~~

<br>
<br>

Entradas relacionadas:  
- [Contraseña de 'sudo'. (retocando Debian 10 liveusb persistente, vol.01)](https://hijosdeinit.gitlab.io/howto_password_sudo_retocando_debian10_liveusb_persistente_vol.01/)
- [[HowTo] 'sudo' en Debian y derivados: agregar usuario a 'sudoers'](https://hijosdeinit.gitlab.io/howto_agregar_usuario_a_sudoers_Debian/)
- [Incorporar nuestro usuario a 'sudoers'](https://hijosdeinit.gitlab.io/howto_incorporar_usuario_a_sudoers/)  

<br>
<br>
<br>

--- --- ---  
<small>Fuentes:  
<a href="https://unix.stackexchange.com/questions/8528/forcing-sudo-to-prompt-for-a-password">unix.stackexchange.com</a>
<a href="https://www.enmimaquinafunciona.com/pregunta/41478/-el-sudo-no-pide-una-contrasena-incluso-despues-de-multiples-reinicios-">EnMiMaquinaFunciona</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
