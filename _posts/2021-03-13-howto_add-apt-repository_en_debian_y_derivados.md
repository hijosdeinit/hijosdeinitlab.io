---
layout: post
title: "[HowTo] Instalar el comando 'add-apt-repository' en Debian"
date: 2021-03-13
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "add-apt-repository", "software-properties-common", "repositorios", "ppa", "sources.list", "apt", "aptitude", "debian", "gnu linux", "linux"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Recién instalado Debian (y algunos derivados) -sobre todo cuando se ha llevado a cabo una instalación 'netinstall', básica- el sistema viene bastante "pelado": hay que dotarle de la capacidad 'sudo', etcétera...  

Otra de las ausencias que, de serie, se observan en Debian es que el comando 'add-apt-repository', que sirve para añadir repositorios y sus ppa, no está.  
Para que esté hay que instalar el paquete 'software-properties-common', del cual forma parte 'add-apt-repository'
~~~
sudo apt-get install software-properties-common
~~~
Asunto resuelto.  

<br>
<br>
Funcionamiento del comando 'add-apt-repository':
~~~bash
sudo add-apt-repository ppa:nombredelppadelrepositorio
~~~
Por ejemplo:
~~~bash
sudo add-apt-repository ppa:wereturtle/ppa
~~~

<br>
<br>
Entradas relacionadas:  
- [[HowTo] '/etc/apt/sources.list': repositorios en Debian y derivados](https://hijosdeinit.gitlab.io/howto_repositorios_sources.list_debian_y_derivados/)
- [[HowTo] Agregar repositorio 'debian unstable' a Debian 10 y similares](https://hijosdeinit.gitlab.io/howto_agregar_repositorio_debian_unstable_debian_y_derivados/)
- [[HowTo] Instalación de una versión más actualizada de un paquete desde los repositorios oficiales de Debian: 'backports'](https://hijosdeinit.gitlab.io/howto_instalacion_version_mas_actualizada_desde_repositorios_Debian_backports/)
- [[HowTo] Eliminar repositorio agregado a mano ('add-apt-repository') en Debian y derivados](https://hijosdeinit.gitlab.io/howto_eliminar_repositorios_agregados_manualmente_addaptrepository_Debian_derivados/)
- [[HowTo] Solucionar error de GPG cuando la clave pública de un repositorio no está disponible. (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_solucionar_error_gpg_clave_publica_repositorio_Debian_y_derivados/)
- [[HowTo] Eliminar Clave 'Gpg' Cuyo 'Id' Desconocemos. (Debian Y Derivados)](https://hijosdeinit.gitlab.io/howto_eliminar_clave_gpg_id_desconocida_agregada_mediante_apt-key_add_/)
- [El repositorio de firmware / drivers para GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- ['RaspBian' / 'RaspBerry OS' incorpora furtivamente -sin aviso ni permiso- un repositorio de 'Microsoft'. Así no vamos bien](https://hijosdeinit.gitlab.io/repositorio_microsoft_instalado_furtivamente_en_Raspbian_RaspBerryOs/)
- [[HowTo] Extirpar todo rastro del repositorio de MicroSoft que Raspbian / RaspBerry Os instala furtivamente](https://hijosdeinit.gitlab.io/howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- []()  

<br>
<br>
<br>

--- --- ---

<small>Fuentes:  
<a href="https://itsfoss.com/add-apt-repository-command-not-found/">ItsFoss</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
