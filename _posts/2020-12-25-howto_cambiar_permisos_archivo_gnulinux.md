---
layout: post
title: "[HowTo] Cambiar los permisos de un archivo/directorio en GNU Linux"
date: 2020-12-25
author: Termita
category: "sistemas operativos"
tags: ["propietario", "grupo", "permisos", "chown", "gnu linux", "sistemas operativos", "sudo", "usuario", "grupo", "otros", "lectura", "escritura", "ejecución"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Para cambiar los permisos de un archivo o una carpeta en GNU Linux el procedimiento desde línea de comandos es:  
sudo chmod *númerooctalcorrespondientealospermisos* *archivoôdirectorio*  
<br>
Por ejemplo:
~~~bash
sudo chmod 775 /var/www/kanboard/config.php
~~~

<br>
En el ejemplo, '775' es el número octal.  
El número octal corresponde a los permisos de lectura, escritura, ejecución del archivo o directorio. Los "valores" de la lectura, la escritura y la ejecución son:  
- r = lectura = 4
- w = escritura = 2
- x = ejecución = 1  

Por consiguiente:
> `rwx` = lectura, escritura y ejecución = 7  
`rw-` = lectura y escritura = 6  
`r-x` = lectura y ejecución = 5  
`r--` = lectura = 4  
`-wx` = escritura y ejecución = 3  
`-w-` = escritura = 2  
`--x` = ejecución = 1  
`---` = sin permisos = 0  

<br>
El número octal consta de 3 cifras:  
>- usuario
- grupo
- otros  
<br>

Cada una de las cifras equivale a los permisos (lectura, escritura y ejecución) correspondientes a 'usuario', 'grupo' y 'otros'  

Por consiguiente '775' significa:  
usuario = rwx = 7  
grupo = rwx = 7  
otros = r-x = 5  
<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://blog.desdelinux.net/permisos-basicos-en-gnulinux-con-chmod/" target="blank">DesdeLinux</a>  
<br>
<br>
<br>
<br>
