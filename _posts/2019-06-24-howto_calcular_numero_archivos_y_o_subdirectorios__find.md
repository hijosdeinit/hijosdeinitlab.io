---
layout: post
title: "[HowTo] Cálculo del número de archivos y subdirectorios que contiene un directorio mediante 'find'"
date: 2019-06-24
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "windows", "backup", "respaldo", "comparacion", "catalogacion", "comparar", "comparar contenidos", "contenido", "almacenamiento", "disco", "directorio", "archivo", "fichero", "diff", "diffmerge", "total commander", "winmerge", "tar", "md5", "md5sum", "suma de verificacion", "sha256", "sha256sum", "transferir", "copiar", "sincronizar", "sort", "find", "calculo", "almacenamiento", "cli"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Para calcular / contar la cantidad de elementos -archivos y/o subdirectorios- que contiene un directorio se puede usar el comando <big>'find'</big>:

<br>
## Contar ficheros y subdirectorios
~~~bash
find . | wc -l
~~~

<br>
## Contar sólo archivos
~~~bash
find . -type f | wc -l
~~~

<br>
## Contar sólo subdirectorios
~~~bash
find . -type d | wc -l
~~~

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Averiguar el tamaño de un directorio desde línea de comandos (CLI) mediante 'du'](https://hijosdeinit.gitlab.io/howto_averiguar_tama%C3%B1o_directorio__cli_find_du/)
- [[HowTo] Búsquedas desde la línea de comandos GNU Linux](https://hijosdeinit.gitlab.io/howto_busquedas_CLI_gnulinux/)
- [[HowTo] Búsqueda de archivos grandes desde línea de comandos en GNU Linux](https://hijosdeinit.gitlab.io/howto_busqueda_archivos_grandes_cli_gnu_linux/)
- [[HowTo] Comprobación de que un directorio y sus subdirectorios se han transferido correctamente a su destino](https://hijosdeinit.gitlab.io/howto_comprobacion_copiado_correcto_directorios_archivos/)
- [[HowTo] Comparación de la suma de verificación md5 de varios directorios mediante 'tar'](https://hijosdeinit.gitlab.io/howto_comparacion_md5sum_varios_directorios_mediante_tar/)
- [[HowTo] Comparación de discos, directorios y archivos](https://hijosdeinit.gitlab.io/howto_comparacion_discos_directorios_archivos/)
- [[HowTo] 'diff'. Comparar directorios ô archivos](https://hijosdeinit.gitlab.io/howto_comparar_directorios_o_archivos/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="@@@@@@@@@@@@@@@@@">@@@@@@@@@@@</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
