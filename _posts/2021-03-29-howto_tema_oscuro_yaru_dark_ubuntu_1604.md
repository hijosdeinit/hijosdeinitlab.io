---
layout: post
title: "[HowTo] Incorporar temas oscuros a Ubuntu 16.04.7 LTS"
date: 2021-03-29
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "ubuntu", "temas", "themes", "apariencia", "dark", "oscuro", "negro", "yaru", "yaru-dark", "visual", "aspecto", "unity", "gnome", "unity-tweak-tool", "gnome-tweak-tool", "retoques", "ubuntu tweak", "tweak tool", "i3", "i3wm", "midnight", "midnight-green", "lxappearance"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>¿Qué hago aún con Ubuntu 16.04 LTS? Desde hace tiempo lo tengo instalado en un ordenador con un procesador de doble núcleo (intel Core2Duo). Llegué a la versión 16.04 LTS desde la versión 14.04 LTS, que fue la inicialmente instalada en la máquina. El sistema desempeña su cometido perfectamente.</small>  
<small>No obstante, tengo presente que el soporte regular de Ubuntu 16.04 se acaba en 2021, fecha en que habrá llegado a su fin de ciclo. En ese momento deberé dejar de aferrarme a esa versión y plantearme hacia donde ir para no usar un sistema sin soporte y esquivar el riesgo para la seguiridad que esto supondría. Probablemente el destino será Ubuntu 18.04 -en agosto de 2020 se lanzó la versión 18.04.5 (que incorpora todos los parches acumulados desde la anterior versión de mantenimiento y el soporte actualizado de la edición de escritorio para que pueda funcionar en configuraciones de hardware más recientes)- o versiones más actuales.</small>

<br>
El procedimiento para incorporar un tema visual (*theme*) oscuro a un sistema operativo como Ubuntu 16.04 es el siguiente:  

<br>
### 1. Instalar 'gnome-tweak-tool', también conocido como 'Retoques', 'Ubuntu Tweak' o 'Tweak Tool'
Desde el Centro de software de Ubuntu basta con buscar 'Herramienta de personalización' (*configuración avanzada de GNOME 3*) e instalarlo.  
> Ubuntu Tweak es un [proyecto descontinuado](https://ubunlog.com/adios-ubuntu-tweak/), aunque aún puede instalarse, bien desde repositorio oficial Ubuntu o bien desde [su propio repositorio en Github](https://github.com/tualatrix/ubuntu-tweak).
> Era una herramienta escrita en Python que nos permitía personalizar nuestro Ubuntu en un ábanico de posibilidades enorme. Con ella podíamos personalizar desde la apariencia y el comportamiento del Dash de Unity, hasta el tema GTK+ de las ventanas, o incluso el tamaño de la fuente del sistema. Es más, según podíamos ver en su web, que ya perece haber desaparecido, estas eran (son) algunas de sus característica más notables:  
>> - Información básica del sistema (distribución, kernel, CPU, memoria)
- GNOME Session Control.
- Inicio automático de aplicaciones.
- Personalizar la pantalla de bienvenida.
- Ajustar los efectos de Compiz.
- Establecer las preferencias de Nautilus.
- Gestionar la energía del sistema.
- Mostrar y ocultar elementos del escritorio: iconos, volúmenes, papelera, icono de red.
- Establecer la seguridad del sistema.
- Instalar aplicaciones de terceros.
- Modificar las preferencias del GNOME Panel.
- Hacer limpieza del sistema: paquetes no necesarios y caché.
- Establecer los atajos de teclado.  

<small>También podría interesar instalar 'Ajustes de Unity' (*'unity-tweak-tool'*) y/o 'Configuración del tema' (*'gtk-theme-config'*).  

<br>
### 2. Descargar el tema visual, por ejemplo 'yaru-dark'
Desde [aquí (gnome-look)](https://www.gnome-look.org/p/1252100/U) por ejemplo.  
Generalmente es un archivo comprimido.  

<br>
### 3. Instalar el tema
3.1 Descomprímase el tema descargado.  
3.2 Muévase -como superusuario (con `sudo`)- el directorio que acabamos de descomprimir -contiene el tema- al directorio `/usr/share/themes`.  

<br>
### 4. Configurar el tema, por ejemplo 'yaru-dark'
4.1 Ejecútese la aplicación 'Retoques' (*'ubuntu-tweak-took'*).  
4.2 Selecciónese el tema visual 'yaru-dark'.

<br>
<br>
### Otros temas oscuros interesantes
<small>Se instalan y configuran siguiendo el mismo procedimiento.  
- [Equilux](https://www.gnome-look.org/p/1182169/)
- [Ambiance DS BlueSB12](https://www.gnome-look.org/p/1013664/)
- [Deepen Dark](https://www.gnome-look.org/p/1190867/)  


<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Incorporación de temas visuales a i3wm en Debian 10 y derivados](https://hijosdeinit.gitlab.io/howto_incorporacion_temas_visuales_i3wm_debian10/)
- ['Midnight-Green', mi tema visual favorito para i3wm en Debian y derivados](https://hijosdeinit.gitlab.io/midnight-green_mi_tema_visual_favorito_para_i3wm_debian_10/)  


<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.esbuntu.com/2018/01/5-temas-oscuros-para-ubuntu-que.html">EsBuntu</a>  
<a href="https://www.muylinux.com/2020/08/14/ubuntu-16-04-7-18-04-5-lts/">MuyLinux - Actualizaciones Ubuntu 16.04.7 y 18.04.5</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
