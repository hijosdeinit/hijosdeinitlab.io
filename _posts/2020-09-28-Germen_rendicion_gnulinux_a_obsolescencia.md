---
layout: post
title: 'Gérmen de rendición a la obsolescencia en GNU Linux'
date: 2020-09-28
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "osbsolescencia", "Debian", "lonchafinismo", "GNU Linux", "desarroladores", "32 bits", "64 bits", "x32", "x64", "x386", "i686", "ordenadores viejos"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
GNU Linux tradicionalmente posibilita que, por vieja que sea una máquina, si ésta enciende, pueda y deba trabajar.  
Últimamente veo que frente a la tradicional concepción lonchafinista del "no tirar nada" que abundaba en el entorno de usuarios y desarrolladores de GNU Linux, va cogiendo fuerza otra corriente, más propensa a conformarse con la obsolescencia.  
Ya hace tiempo hay desarrolladores de distribuciones que abandonan arquitecturas que, lamentablemente (a mi juicio), consideran obsoletas como, por ejemplo, 32 bits y enfocan su trabajo exclusivamente en las máquinas que consideran hegemónicas como, por ejemplo, 64 bits.  
Lo peor -siempre a mi entender- es que cada vez hay más usuarios que comprenden eso.  
De momento, afortunadamente, no es este el caso de Debian. Debian sí desarrolla para 32 bits, por ejemplo, y continuará haciéndolo. La cuestión es si los desarrolladores de aplicaciones continuarán haciéndolo para 32 bits... de tal forma que en un futuro no muy lejano podemos hallarnos con el problema de tener un sistema operativo "base" totalmente actualizado plagado de aplicaciones que no lo estén (o que ni siquiera estén).  

<br>
<br>
Entradas relacionadas:  
- [Mirando cara a cara al abismo de la incoherencia posmoderna: un vistazo a la Tecnofilia creditofágica](https://hijosdeinit.gitlab.io/tenofilia_creditofagica/)
- [[HowTo] Mantener vivo un iPad antiguo, vol.01: instalar aplicaciones 'no tenidas antes'](https://hijosdeinit.gitlab.io/howto_mantener_vivo_ipad_antiguo_vol01__instalar_aplicaciones_no_tenidas_antes/)
- [[HowTo] Instalación netinstall de Debian en netbook Compaq Mini](https://hijosdeinit.gitlab.io/howto_instalacion_netinstall_Debian_netbook_compaqmini/)
- [[HowTo] búsqueda de páginas web ‘desaparecidas’, directamente en la caché de Google](https://hijosdeinit.gitlab.io/howto_busqueda_en_cache_de_google_paginas_desaparecidas/)
- [Saque todo lo que pueda de los soportes ópticos (cd/dvd), si no lo ha hecho ya](https://hijosdeinit.gitlab.io/saque-todo-lo-que-pueda-de-los-soportes/)
- [Otra faceta de la vida útil de las memorias usb](https://hijosdeinit.gitlab.io/otra-faceta-vida-util-memoria-usb/)
- [Qué pedirle a un ‘pendrive’ USB](https://hijosdeinit.gitlab.io/requisitos_de_un_pendrive/)
- ["Gratis"](https://hijosdeinit.gitlab.io/lo_gratis/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
