---
layout: post
title: "[HowTo] vaciar *caché* de 'apt' en Debian (y derivados)"
date: 2021-10-21
author: Termita
category: "redes"
tags: ["redes", "sistemas operativos", "nextcloud", "cliente", "nube", "redes", "sincronizacion", "backup", "respaldo", "gnu linux", "linux", "debian", "appimage", "nueva paquetería"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Cuando instalamos un paquete desde los repositorios, utilizando el comando <span style="background-color:#042206"><span style="color:lime">'apt'</span></span> o mediante el centro de software, el gestor de paquetes <span style="background-color:#042206"><span style="color:lime">'apt'</span></span> descarga el paquete y sus dependencias y lo mantiene -aún cuando la instalación ha finalizado- en el subdirectorio '/var/cache/apt/archives'.  
Los paquetes parcialmente descargados se alojan en el subdirectorio '/var/cache/apt/archives/partial'.  

<br>
## ¿Por qué se conserva el paquete en *caché*?
Por si acaso, y por rapidez.  
Esos paquetes descargados cuando instalamos software no son borrados de '/var/cache/apt/archives' inmediatamente una vez la instalación ha concluído en previsión de que ordenemos reinstalar dicho software; entonces el sistema buscará los paquetes necesarios en la *caché* ('/var/cache/apt/archives') y -si están- los utilizará en lugar de descargarlos de nuevo, eso sí, siempre y cuando no exista una versión superior / nueva en los repositorios remotos.  
Este método es mucho más rápido y comedido con la utilización del ancho de banda. Desconozco cuánto tiempo las distribuciones GNU Linux conservan en *caché* los paquetes del software instalado.  

<br>
## Averiguación el espacio que está ocupando la caché de 'apt'
Dado que la *caché* de 'apt' está en el subdirectorio '/var/cache/apt/archives':
~~~bash
du -sh /var/cache/apt/archives
~~~
... y como los paquetes parcialmente descargados se alojan en '/var/cache/apt/archives/partial':
~~~bash
du -sh /var/cache/apt/archives/partial
~~~

<br>
## ¿Por qué limpiar la *caché* de 'apt'?
Para liberar espacio, fundamentalmente.  

<br>
## Procedimiento para purgar la *caché* de 'apt'
~~~bash
sudo apt-get clean
~~~
Esto borrará el contenido del subdirectorio '/var/cache/apt/archives' excepto el archivo 'lock'.  
<br>
El parámetro 'autoclean' borrará sólo los paquetes obsoletos del subdirectorio '/var/cache/apt/archives'.
~~~bash
sudo apt-get autoclean
~~~
<br>
Existen, asimismo, herramientas que realizan algo similar, eso sí, con un interfaz gráfico:
- ['stacer'](https://oguzhaninan.github.io/Stacer-Web/)
- ['bleachbit'](https://www.bleachbit.org/)  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://itsfoss.com/clear-apt-cache/">ItsFoss</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
