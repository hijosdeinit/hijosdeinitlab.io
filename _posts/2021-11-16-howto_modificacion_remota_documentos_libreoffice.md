---
layout: post
title: "[HowTo] Modificación remota ('webdav') de archivos de 'LibreOffice'"
date: 2021-11-16
author: Termita
category: "redes"
tags: [""]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<big>**EN CONSTRUCCION**</big>  
<br>
<br>
<br>


<small><small>Entorno: cpu Intel i7-8750H (12) @ 4.100GHz, 16gb RAM, gpu nvidia GeForce GTX 1050 Mobile, Ubuntu 20.04.3 LTS x86_64, kernel 5.11.0-40-generic</small></small>  

<br>
<br>
<small>['Odysee'](https://odysee.com) -o *lbry*- es una plataforma de streaming de video gratuíta que, a diferencia de 'Youtube', mantiene cierto respeto por sus usuarios.</small>  

<br>
Cuando de subir un video a la plataforma se trata, ['Odysee' señala en sus normas y procedimientos](https://odysee.com/@OdyseeHelp:b/uploadguide:1) que el formato de video al efecto más adecuado es <big>**'.mp4, H.264 video, AAC audio'**</big>.  
<br>
Para recomprimir un video a este formato -para posteriormente subirlo a Odysee- **'ffmpeg'** haría bien el trabajo:  
<br>
<big><span style="background-color:#042206"><span style="color:lime">`ffmpeg -i input_video -c:v libx264 -preset slow -crf 20 -c:a aac -b:a 160k -vf format=yuv420p -movflags +faststart output_video.mp4`</span></span></big>  
<br>
Donde:  
<span style="background-color:#042206"><span style="color:lime">`input_video`</span></span> is the input file.  
<span style="background-color:#042206"><span style="color:lime">`-c:v libx264`</span></span> selects the video encoder libx264, which is a H.264 encoder.  
<span style="background-color:#042206"><span style="color:lime">`-preset slow`</span></span> selects the slow x264 encoding preset. Default preset is medium. Use the slowest preset that you have patience for.  
<span style="background-color:#042206"><span style="color:lime">`-crf 20`</span></span> selects a CRF value of 20 which will result in a high quality output. Default value is 23. A lower value is a higher quality. Use the highest value that gives an acceptable quality.  
<span style="background-color:#042206"><span style="color:lime">`-c:a aac`</span></span> selects the audio encoder aac, which is the built-in FFmpeg AAC encoder.  
<span style="background-color:#042206"><span style="color:lime">`-b:a 160k`</span></span> encodes the audio with a bitrate of 160k.  
<span style="background-color:#042206"><span style="color:lime">`-vf format=yuv420p`</span></span> chooses YUV 4:2:0 chroma-subsampling which is recommended for H.264 compatibility.  
<span style="background-color:#042206"><span style="color:lime">`-movflags +faststart`</span></span> is an option for MP4 output that move some data to the beginning of the file after encoding is finished. This allows the video to begin playing faster if it is watched via progressive download playback.  
<span style="background-color:#042206"><span style="color:lime">`output_video.mp4`</span></span> is the output file.  

<br>
<br>
Si se deseara, asimismo, extraer una imagen descriptiva o *'thumbnail'*: <span style="background-color:#042206"><span style="color:lime">`ffmpeg -i 2021-07-11-howto_modificacion_prompt_cli.mp4 -ss 00:00:01 -t 5 -r 30 -f image2 image%03d.jpg`</span></span>  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://superuser.com/a/1551095">llogan en SuperUser.com</a>  
<a href="https://lbry.com/faq/how-to-publish">lbry - how to publish</a>  |  [descargar '.pdf'](/assets/docs/2021-11-15-howto_recomprimir_videos_para_odysee/lbry-odysee_howto_publish.pdf)  
<a href="https://lbry.com/faq/video-publishing-guide">lbry - video publishing guide</a>  |  [descargar '.pdf'](/assets/docs/2021-11-15-howto_recomprimir_videos_para_odysee/lbry-odysee_videopublishing_guide.pdf)  
<a href="https://odysee.com/@OdyseeHelp:b/uploadguide:1">Odysee - upload guide</a></small>  
<br>
<br>
<br>
<br>
<br>
<br>
