---
layout: post
title: "[HowTo] concatenar 2 ó más documentos '.pdf' desde línea de comandos sin instalar nada"
date: 2021-06-27
author: Termita
category: "sistemas operativos"
tags: ["pdfjoin", "productividad", "pdf", "sistemas operativos", "gnu linux", "linux", "CLI", "convertir", "exportar"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Para unir 2 ó más documentos '.pdf' en uno solo existe un comando llamado 'pdfjoin'. Sin embargo para disponer de esta utilidad es necesario instalar un paquete ('texlive-extra-utils').  
¿Para qué instalar paquetes cuando la línea de comandos de GNU Linux, por sí misma y sin agregarle nada, es lo suficientemente potente como para hacer el trabajo sin pestañear?</small>  

<br>
<br>
## Crear una función para concatenar documentos 'pdf' desde línea de comandos
Basta con crear una función -'combine_pdfs'- cada vez que vayamos a concatenar 'pdf'.  
<span style="background-color:#042206"><span style="color:lime">`function combine_pdfs() {     output_file=$1;     files=($2);     gs -dBATCH -dNOPAUSE -q -sDEVICE=pdfwrite -dAutoRotatePages=/None -sOutputFile=$output_file "${files[@]}"; }`</span></span>  

<br>
## ¿Cómo concatenar documentos 'pdf' mediante la recién creada función 'combine_pdfs'?
<span style="background-color:#042206"><span style="color:lime">`combine_pdfs `*documento_pdf_resultante.pdf* `'`*documentoinput1.pdf* *documentoinput2.pdf* *documentoinput3.pdf*`'`</span></span>  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] pdfjoin: concatenar 2 ó más documentos '.pdf'](https://hijosdeinit.gitlab.io/howto_pdfjoin_concatenar_pdf/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://askubuntu.com/questions/1244384/where-is-pdfjoin-ubuntu-18-04-and-later/1244572#1244572"> Michael Jacob Mathew en AskUbuntu.com</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
