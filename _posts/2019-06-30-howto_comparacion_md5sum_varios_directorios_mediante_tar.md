---
layout: post
title: "[HowTo] Comparación de la suma de verificación md5 de varios directorios mediante 'tar'"
date: 2019-06-30
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "windows", "backup", "respaldo", "comparacion", "catalogacion", "comparar", "comparar contenidos", "contenido", "almacenamiento", "disco", "directorio", "archivo", "fichero", "calculo", "diff", "diffmerge", "total commander", "winmerge", "tar", "md5", "md5sum", "suma de verificacion", "sha256", "sha256sum", "transferir", "copiar", "sincronizar", "cli"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Comparando la suma de verificación -por ejemplo md5- de varios directorios podemos conocer si son **idénticos**.  
Una de las formas de hacer esto -con cada directorio- es mediante 'tar':
~~~bash
tar -cf - nombredeldirectorio | md5sum
~~~
Ô MEJOR AÚN:
~~~bash
tar -cvzpf - nombredeldirectorio | md5sum
~~~

<br>
Con este método en realidad **NO** se generará ningún archivo comprimido persistente: sólo comprimirá y calculará el md5sum.  

<br>
<small>**(*)** Sin embargo, pienso que hay veces que queremos comparar carpetas que contienen archivos de sistema, enlaces simbólicos, archivos temporales y eso "tar -cf" es posible que no lo tenga en cuenta. Debería profundizar más en este asunto.</small>  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Averiguar el tamaño de un directorio desde línea de comandos (CLI) mediante 'du'](https://hijosdeinit.gitlab.io/howto_averiguar_tama%C3%B1o_directorio__cli_find_du/)
- [[HowTo] Cálculo del número de archivos y subdirectorios que contiene un directorio mediante 'find'](https://hijosdeinit.gitlab.io/howto_calcular_numero_archivos_y_o_subdirectorios__find/)
- [[HowTo] Comprobación de que un directorio y sus subdirectorios se han transferido correctamente a su destino](https://hijosdeinit.gitlab.io/howto_comprobacion_copiado_correcto_directorios_archivos/)
- [[HowTo] Comparación de discos, directorios y archivos](https://hijosdeinit.gitlab.io/howto_comparacion_discos_directorios_archivos/)
- [[HowTo] 'diff'. Comparar directorios ô archivos](https://hijosdeinit.gitlab.io/howto_comparar_directorios_o_archivos/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://unix.stackexchange.com/questions/35832/how-do-i-get-the-md5-sum-of-a-directorys-contents-as-one-sum/35834">StackExchange.com</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
