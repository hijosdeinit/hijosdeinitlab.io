---
layout: post
title: "[HowTo] El editor (IDE) 'pycharm' en Debian y derivados"
date: 2022-02-14
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "pycharm", "idle", "geany", "atom", "editor de texto", "ide", "programacion", "python", "bash", "script", "bash scripting", "markdown", "texto", "texto plano", "código", "editor", "gnu linux", "debian", "vi", "vim", "nano", "emacs", "atom", "eclipse", "sublime text", "visual studio code", "vscodium", "codium", "vscode", "netbeans"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Hace un tiempo hice un [listado de los editores de texto / IDE que me parecían interesantes en un artículo de este blog](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/).</small>  
<small>Un IDE (*Integrated Developement Environment*) consta, entre otras cosas, de un editor de texto con muchos extras como, por ejemplo, resaltado de sintaxis, autocompletado.</small>  
<small>Uno de los editores de aquella lista, específico -en este caso- para el lenguaje de programación 'python', era ['PyCharm'](https://www.jetbrains.com/pycharm/).</small>  

<br>
<a href="/assets/img/2022-02-14-howto_IDE_python_pycharm/PyCharm_2021.1_Community_Edition_screenshot.png" target="_blank"><img src="/assets/img/2022-02-14-howto_IDE_python_pycharm/PyCharm_2021.1_Community_Edition_screenshot.png" alt="'Pycharm'" width="600"/></a>  
<small>'Pycharm, un IDE específico para python' <small>By Software:JetBrainsScreenshot:Vulphere - Self-taken; derivative work, Apache License 2.0, https://commons.wikimedia.org/w/index.php?curid=104429683</small></small>  
<br>

<br>
<br>
## 'Pycharm'
['Pycharm'](https://www.jetbrains.com/pycharm/) es un entorno de desarrollo integrado (IDE) orientado específicamente a la programación mediante el lenguaje Python. Está desarrollado en Java y Python por la compañía checa 'JetBrains' (formalmente conocida como 'IntelliJ').  
Provee análisis de código, implementación de plugins, un *debuggert* gráfico, «an integrated unit tester, integration with version control systems (VCSes), and supports web development with Django as well as data science with Anaconda».  
'Pycharm' es multiplataforma -dispone de versiones para GNU Linux, macOS y Windows-.  
Existen 2 "versiones" de Pycharm:
- *COMMUNITY*: gratuíta
- *PROFESSIONAL*: de pago  

La edición *Community* está lanzada bajo licencia Apache. La edición *Professional* tiene características adicionales y está lanzada «under a subscription-funded proprietary license and also an educational version».  

<br>
<br>
## Instalación de 'Pycharm' en Debian 11 y derivados
Existen 3 formas de instalar 'Pycharm' en Debian 11:
- nueva paquetería: snapcraft (snap)
- nueva paquetería: flatpak
- de forma convencional / "nativa"  

Los detalles sobre cómo instalar 'pycharm' mediante *snap* o *flatpak* están en los enlaces al final de esta entrada, en el apartado "fuentes".  
<br>
Suelo prescindir de la nueva paquetería; instalé la versión 'community' de 'pycharm' de forma convencional.  

<br>
Es importante asegurarse de que se disponen de las dependencias que pudiera necesitar 'pycharm':
~~~bash
sudo apt-get update
sudo apt install python3 python3-dev python3-distutils
~~~

<br>
[Descárguese desde la página oficial](https://www.jetbrains.com/pycharm/download/) el archivo *tarball* (<small>.tar.gz</small>) correspondiente a la última versión de 'pycharm'. En mi caso escogí la versión 'community', es decir, la versión que no es "de pago".  

<br>
Descomprímase en un subdirectorio creado al efecto el
~~~bash
cd ~/Descargas
mkdir pycharm
tar -xvf pycharm-community-*.tar.gz -C pycharm/ --strip-components 1
~~~

<br>
Como superusuario, muévase el subdirectorio, que ahora contiene los archivos que acabamos de descomprimir, a '/opt'
~~~bash
sudo mv pycharm /opt/
~~~

<br>
Despliéguese 'pycharm' ejecutando el script 'pycharm.sh'
~~~bash
sh /opt/pycharm/bin/pycharm.sh
~~~

<br>
<a href="/assets/img/2022-02-14-howto_IDE_python_pycharm/pycharm_despliegue.png" target="_blank"><img src="/assets/img/2022-02-14-howto_IDE_python_pycharm/pycharm_despliegue.png" alt="'Pycharm' recién desplegado" width="600"/></a>  
<small>'Pycharm' recién desplegado</small>  
<br>

<br>
<a href="/assets/img/2022-02-14-howto_IDE_python_pycharm/pycharm_pantalladebienvenida.png" target="_blank"><img src="/assets/img/2022-02-14-howto_IDE_python_pycharm/pycharm_pantalladebienvenida.png" alt="'Pycharm' pantalla de bienvenida" width="600"/></a>  
<small>'Pycharm' pantalla de bienvenida</small>  
<br>

Como se ha visto, si deseamos lanzar 'pycharm' desde la terminal, bastará con situarnos en '/opt/pycharm/bin' y ejecutar:
~~~bash
pycharm.sh &
~~~

<br>
(opcional) Créense los **accesos directos, rutas (*path*)**, etc... que consideremos más apropiados para lanzar 'pycharm' con comodidad:  
<br>
Si deseamos lanzar 'pycharm' desde la terminal, indiferentemente del subdirectorio en que estemos situados, será necesario establecer una ruta:
~~~bash
echo 'export PATH="$PATH:/opt/pycharm/bin"' >> ~/.bashrc
source ~/.bashrc
~~~
<br>
Si deseamos un acceso directo en el escritorio bastará con crearlo:
~~~bash
nano ~/Desktop/Pycharm.desktop
~~~
En este archivo debemos escribir estas líneas:
~~~
[Desktop Entry]
Version=1.0
Type=Application
Name=Pycharm
Comment=IDE
Exec=/opt/pycharm/bin/pycharm.sh
Icon=/opt/pycharm/bin/pycharm.png
Terminal=false
StartupNotify=false
Categories=Application;Development;
~~~
... y hacerlo ejecutable
~~~bash
chmod u+x ~/Desktop/Pycharm.desktop
~~~
<br>
La última línea (<small>Categories=Application;Development;</small>), si el acceso directo lo copiamos al "menú", indicaría en qué apartado de éste aparecería; en este caso, según la ]nomenclatura que entornos de escritorio como LXDE tienen](https://specifications.freedesktop.org/menu-spec/latest/apa.html), en "Programming"  
Colocar el acceso directo a 'pycharm' en el "menú inicio" es sencillo:
~~~bash
sudo cp ~/Desktop/Pycharm.desktop /usr/share/applications
~~~

<br>
<br>
<br>
Extras  
En la página oficial de 'Pycharm' existe una completa y bien organizada [colección de recursos de aprendizaje sobre este IDE](https://www.jetbrains.com/pycharm/guide/).  

<br>
<a href="/assets/img/2022-02-14-howto_IDE_python_pycharm/pycharm_learning_project.png" target="_blank"><img src="/assets/img/2022-02-14-howto_IDE_python_pycharm/pycharm_learning_project.png" alt="'Pycharm' learning project" width="600"/></a>  
<small>'Pycharm' *learning project*</small>  
<br>

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] El editor (IDE) 'atom' y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [[HowTo] Agregar idiomas al editor (IDE) atom. Ponerlo en español](https://hijosdeinit.gitlab.io/howto_poner_atom_en_espa%C3%B1ol/)
- [[HowTo] Evitar que 'atom' elimine espacios en blanco al final de línea](https://hijosdeinit.gitlab.io/howto_atom_espacios_blanco_final_linea/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)
- [[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.how2shout.com/linux/2-ways-to-install-pycharm-python-ide-on-debian-11-bullseye-linux/">how2shout</a>  
<a href="https://codepre.com/en/como-instalar-pycharm-en-debian-11.html">codepre</a>  
<a href="https://www.linuxcapable.com/how-to-install-pycharm-on-debian-11-bullseye/">LinuxCapable</a>  
<a href="https://www.jetbrains.com/help/pycharm/installation-guide.html">jetbrains - guía de instalación de 'pycharm'</a>  
<a href="https://en.wikipedia.org/wiki/PyCharm">Wikipedia - 'pycharm'</a>  
<a href="https://rileymacdonald.ca/2017/06/02/adding-main-menu-items-to-lxde-lubuntu-v16-04-17-04/">RyleyMacDonald.ca - adición de entradas al menú de lxde</a>  
<a href="https://lkubaski.wordpress.com/2012/06/29/adding-lxde-start-menu-and-desktop-shortcuts/">lkubaski</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
