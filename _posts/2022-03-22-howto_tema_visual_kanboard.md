---
layout: post
title: "[HowTo] Temas visuales en 'Kanboard'"
date: 2022-03-22
author: Termita
category: "productividad"
tags: ["productividad", "redes", "sistemas operativos", "gnu linux", "linux", "kanboard", "kanban", "gestion de proyectos", "organizacion", "tareas", "gantt", "tema visual"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small><small>entorno: RaspBerry Pi 4b 4gb RAM, SD y HDD usb. RaspBian Buster 32bits. 'Kanboard' en contenedor 'docker'</small></small>  

<br>
<br>
['Kanboard'](https://kanboard.org) es un software de gestión de proyectos mediante el [sistema 'kanban'](https://es.wikipedia.org/wiki/Kanban).  
> <small>El sistema 'kanban' es uno de muchos métodos de organización. Otros son ['GTD'](https://en.wikipedia.org/wiki/Getting_Things_Done) o ['Pomodoro'](https://en.wikipedia.org/wiki/Pomodoro_Technique).</small>  

<br>
'Kanboard' es software libre y de código abierto. Se instala / hospeda en un servidor y a él acceden sus usuarios, vía navegador o 'apps'.  
La instalación puede ser "nativa" (instalando el servidor ('anginx', 'lighthttpd', etc...), las dependencias ('php', etcétera...) y el propio software) o mediante 'docker'.  

<br>
En 'Kanboard' el tema visual por defecto es, a mi juicio, demasiado luminoso.  
Kanboard permite la adición de prestaciones mediante [plugins](https://kanboard.org/plugins.html) y también incorporar temas visuales nuevos.  

<br>
Existen diversos temas visuales para 'Kanboard'. Son destacables el que ha creado [draghici.net](https://draghici.net/2019/12/28/kanext-a-theme-for-kanboard/) -'kanext'-, y otros de libre acceso en [diversos repositorios de 'GitHub'](https://github.com/topics/kanboard-theme).  

<br>
<br>
## Procedimiento para cambiar el tema visual de 'kanboard'
Me he decidido por el tema visual ['Nebula'](https://github.com/kenlog/Nebula).  
Antes de establecer un nuevo tema visual es conveniente comprobar que éste sea compatible con la versión de 'Kanboard' que tenemos instalada. En mi caso 'Nebula' lo es.  
En mi caso 'kanboard' está funcionando en un contenedor 'docker'. Dentro de ese contenedor, el software de 'kanboard' está alojado en el subdirectorio '/var/www/app'  
Dentro de ese subdirectorio existe otro subdirectorio llamado 'plugins'. Y es precisamente en ese subdirectorio donde habrá que colocar el tema visual 'Nebula'.

<br>
Accedo a la consola del contenedor 'docker' que alberga 'kanboard'. Esto puede hacerse desde 'portainer' -una especie de gestor gráfico de 'docker'- o directamente desde línea de comandos (<small>`sudo docker exec -it nombredelcontenedor /bin/bash`</small>).  

<br>
Me sitúo en el subdirectorio '/var/www/apps/plugins' y descargo el archivo .zip que contiene el tema visual 'Nebula'. Lo descomprimo y borro ese .zip, que de nada me servirá ya. Salgo de la consola del docker.
~~~bash
cd /var/www/apps/plugins
wget 'https://github.com/kenlog/Nebula/releases/download/1.3.6/Nebula-v1.3.6.zip'
unzip Nebula-v1.3.6.zip
rm Nebula-v1.3.6.zip
exit
~~~
<br>
**Reinicio** el contenedor desde 'portainer'. Accedo desde el navegador web y constato que el tema visual ha cambiado.  

<br>
<br>
Entradas relacionadas:  
- []()
- []()  
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
