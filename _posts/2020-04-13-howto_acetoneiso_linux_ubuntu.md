---
layout: post
title: "[HowTo] Acetone ISO en GNU Linux Ubuntu"
date: 2020-04-13
author: Termita
category: "sistemas operativos"
tags: ["acetone iso", "quemar", "burn", "iso", "mds", "mdf", "bin", "cue", "toc", "nrg", "daa", "dmg", "cdi", "b5i", "bwi", "pdi", "daemon tools", "nero", "alkohol", "cd", "dvd", "imagen", "cdrkit", "cdrdao", "growisofs", "rip", "avi", "rar", "proteccion", "anticopia", "mp3", "mpg", "flv", "mov", "montar", "gnome-disks"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
![acetoneiso]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-04-13-howto_acetoneiso_linux_ubuntu/AcetoneISO_logo.png)  
<br>
<br>
[AcetoneISO](https://launchpad.net/ubuntu/+source/acetoneiso) es una aplicación para grabar y manipular -montar, convertir, etcétera...- soportes de almacenamiento óptico (cd, dvd) y sus imagenes.  
Escrito en Qt, este software está destinado a todas aquellas personas que buscan un 'Daemon Tools para Linux'. Sin embargo, AcetoneISO no emula ninguna protección anticopia durante el montaje.  
AcetoneISO también soporta Direct Access Archive (*.daa) porque usa las imágenes no libres y propietarias PowerISO Software de Linux como backend mientras se convierten imágenes a ISO.  
En versiones recientes (a partir de 2010), AcetoneISO también obtuvo soporte nativo para borrar discos ópticos de CD/DVD y grabar imágenes ISO/CUE/TOC en CD-R/RW y DVD-+R/RW (incluyendo DL) gracias a herramientas externas de código abierto como cdrkit, cdrdao y growisofs.  
AcetoneISO está actualmente traducido a: inglés, italiano, polaco, español, rumano, húngaro, alemán, checo y ruso.  

<br>
### Características
- Montar automáticamente ISO, BIN, MDF y NRG sin necesidad de insertar la contraseña de administrador. Por el momento, sólo se admiten imágenes de una sola pista.
- Grabar discos ópticos ISO/TOC/CUE a CD-R/RW
- Grabar imágenes ISO en DVD-+R/RW (incluyendo DL)
- Una utilidad nativa para discos CD-RW, DVD-RW y DVD-RW en blanco.
- Una bonita pantalla que muestra las imágenes actuales montadas y la posibilidad de hacer clic en ella para volver a abrir rápidamente la imagen montada.
- Convertir 2 tipos de imagen ISO: bin mdf nrg img daa dmg cdi b5i bwi pdi
- Extraer el contenido de las imágenes a una carpeta: bin mdf nrg img daa dmg cdi b5i bwi pdi
- Reproducir una imagen de película en DVD con Kaffeine / VLC / SMplayer con auto-cover descargar desde Amazon
- Generar una ISO desde una carpeta o CD/DVD
- Comprobar el archivo MD5 de una imagen y/o generarlo en un archivo de texto
- Calcular ShaSums de imágenes en 128, 256 y 384 bits
- Cifrar / Descifrar una imagen
- Dividir / Fusionar imagen en X megabytes
- Comprimir una imagen en formato 7z con una relación alta.
- Copiar un CD PSX a *.bin para que funcione con emuladores ePSXe/pSX.
- Restaurar un archivo CUE perdido de *.bin *.img
- Convertir Mac OS *.dmg a una imagen montable
- Montar una imagen en una carpeta especificada por el usuario
- Crear una base de datos de imágenes para gestionar grandes colecciones
- Extraer el archivo de imagen de arranque de un CD/DVD o ISO
- Copia de seguridad de un CD-Audio a una imagen *.bin
- Localización completa para inglés, italiano, francés, español y polaco.
- Utilidad rápida y sencilla para copiar un DVD a Xvid AVI
- Utilidad rápida y sencilla para convertir un vídeo genérico (avi, mpeg, mov, wmv, asf) a Xvid AVI
- Utilidad rápida y sencilla para convertir un vídeo FLV a AVI
- Utilidad para descargar videos de YouTube y Metacafe.
- Extraer audio de un archivo de vídeo
- Extraer un archivo *.rar que tenga una contraseña
- Utilidad para convertir cualquier video para Sony PSP PlayStation Portable
- Historial de visualización que muestra todas las imágenes que se montan a tiempo  

<br>
<br>
### Limitaciones
- No emula el montaje de protección anticopia como 'DAEMON Tools'
- No se puede montar correctamente una imagen de varias sesiones. Sólo se mostrará la primera pista.
- La conversión de una imagen de varias sesiones a ISO provocará una pérdida de datos. Sólo se convertirá la primera pista.
- La conversión de imágenes a ISO sólo es posible en la arquitectura de CPU x86 y x86-64 debido a las limitaciones de PowerISO.

<br>
<br>
### Instalación en gnu linux Ubuntu
~~~
sudo apt-get update
sudo apt install acetoneiso
~~~
Para que funcione la conversión de diversos formatos de imagen de cd/dvd a ISO hay que incorporar ['PowerISO'](https://www.poweriso.com/poweriso-1.3.tar.gz) a 'AcetoneISO':
~~~
wget https://www.poweriso.com/poweriso-1.3.tar.gz
~~~
Hay que descomprimir el contenido de ese archivo en `/home/*usuario*/.acetoneiso/`  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/AcetoneISO" target="_blank">WikiPedia</a>  
<br>
<br>
<br>
<br>
