---
layout: post
title: 'Repositorio con todos los resaltados de sintaxis (.nanorc) para nano'
date: 2020-05-25
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "editores de texto", "nano", "resaltado de sintaxis", "nanorc", "markdown", "bash", "programación", "yaml", "python", "CLI", "syntax highlightning", "lenguaje de marcas"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Generalmente el editor de texto [**nano**](https://www.nano-editor.org/) viene instalado por defecto en la mayoría de distribuciones GNU Linux. No ocurre así con ciertas prestaciones como el **resaltado de sintaxis**.  

<br>
En <big>[este repositorio](https://github.com/serialhex/nano-highlight)</big> se encuentran los archivos *.nanorc* que requiere *nano* para resaltar la sintaxis de la mayoría de lenguajes.  
Basta con descargar el archivo .nanorc correspondiente al lenguaje cuyo resaltado necesitamos incorporar a nano y copiarlo (el fichero) en la carpeta 'usr/share/nano'.  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Añadir resaltado de sintaxis al editor de textos NANO](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [[HowTo] Atajos de teclado del editor 'nano'](https://hijosdeinit.gitlab.io/howto_atajos_teclado_nano/)
- [[HowTo] Mostrar el número de línea en el editor 'nano' cuando Alt+Shift+3 no funciona](https://hijosdeinit.gitlab.io/nano__mostrar_numeros_linea_cuando_altshift3_nofunciona/)
- [[HowTo] nano: parámetros de arranque útiles](https://hijosdeinit.gitlab.io/howto_parametros_utiles_arranque_nano/)
- [En nano no existe overtyping](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/)
- [Expresiones regulares no aceptadas por el editor de texto 'nano'](https://hijosdeinit.gitlab.io/regexp_no_aceptadas_por_nano/)
- [[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados](https://hijosdeinit.gitlab.io/howto_micro_editor_texto_debian/)
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)
- [[HowTo] Visualización de 'markdown' en la línea de comandos (CLI): 'MDLESS'](https://hijosdeinit.gitlab.io/howto_mdless_visor_markdown_cli/)
- [[HowTo] El editor (IDE) atom y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- [[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)  

<br>
<br>
<br>
<br>
---
<small>Fuentes:
[ourcodeworld](https://ourcodeworld.com/articles/read/807/how-to-enable-syntax-highlighting-for-markdown-files-in-gnu-nano)  
[victorhckinthefreeworld](https://victorhckinthefreeworld.com/2018/02/13/flipando-en-colores-con-el-editor-nano/)</small>  

<br>
<br>
<br>
<br>
