---
layout: post
title: "[HowTo] 'Tron' multijugador via SSH (línea de comandos)"
date: 2021-11-27
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "juegos", "linea de comandos", "terminal", "cli", "ssh", "tron"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
['sshtron'](http://sshtron.zachlatta.com/) es un juego basado en la película de 1982 que lleva el mismo nombre y que trataba de un programador que se introduce -literalmente- en un mundo electrónico en el interior de su ordenador, un juego donde los jugadores deben eliminarse unos a otros mediante la estela que dejan sus motocicletas.  
Inspirándose en la famosa obra de ciencia ficción, «TRON» nos introducía en aquel evocador entorno cibernético de luz, juegos letales y maliciosos programas.  

<br>
<a href="/assets/img/2021-11-27-tron_online_ssh_terminal/ssh_tron_gameplay.gif" target="_blank"><img src="/assets/img/2021-11-27-tron_online_ssh_terminal/ssh_tron_gameplay.gif" alt="'Tron' multijugador por 'ssh'" width="500"/></a>  
<br>

<br>
## Jugar a 'Tron' en el servidor oficial
Es posible jugar a 'Tron' a través de 'ssh' conectándose al servidor de 'zachlatta'
~~~bash
ssh sshtron.zachlatta.com
~~~
... o bien [creando (y conectando a) nuestro propio servidor](https://github.com/zachlatta/sshtron#running-your-own-copy), cosa que no he hecho aún, aunque según los desarrolladores el procedimiento es el que se enuncia a continuación.  

<br>
<br>
## Jugar a 'Tron' en tu propio servidor 'ssh' (instalación nativa)
«Clone the project and cd into its directory.»
~~~bash
git clone https://github.com/zachlatta/sshtron.git
cd sshtron
~~~
«These instructions assume that you have your GOPATH setup correctly.
Create an RSA public/private keypair in the current directory for the server to use. Don't give it a passphrase.»
~~~bash
ssh-keygen -t rsa -f id_rsa
~~~
«Download dependencies and compile the project»
~~~bash
go get && go build
~~~
«Run it! You can set PORT to customize the HTTP port it serves on and SSH_PORT
# to customize the SSH port it serves on»
~~~bash
./sshtron
~~~

<br>
<br>
## Jugar a 'Tron' en tu propio servidor 'ssh' ("instalación" mediante docker)
«Clone the project and cd into its directory.  
Build the SSHTron Docker image»
~~~bash
docker build -t sshtron .
~~~


«Spin up the container with always-restart policy»
~~~bash
docker run -t -d -p 2022:2022 --restart always --name sshtron sshtron
~~~
«For Raspberry Pi, use the following to build the Docker image:»
~~~bash
docker build -t sshtron --build-arg BASE_IMAGE=resin/raspberry-pi-golang:latest .
~~~

<br>
<br>
<br>
(i) Todos los jugadores conectados al servidor jugarán simultáneamente, unos contra otros.  

<br>
<br>
Hay [una especie de *fork* del proyecto](https://github.com/jpillora/ssh-tron). No lo he probado aún, tampoco.  
 
<br>
<br>
Entradas relacionadas:  
- [Listado oficial de juegos para línea de comandos de Debian](https://hijosdeinit.gitlab.io/listado_oficial_juegos_terminal_cli_debian/)
- [[HowTo] 'zangband', juego *roguelike* para la terminal ('CLI')](https://hijosdeinit.gitlab.io/howto_zangband_juego_cli/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  
<br>
<br>
<br>
<br>
<br>
<br>
