---
layout: post
title: "[HowTo] nueva paquetería, vol.01: 'snap' en Debian 10 Buster (y derivados)"
date: 2021-05-19
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "ubuntu", "snap", "nueva paqueteria", "software", "apt", "snapd", "wine", "flatpak"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Debian 10 *Buster* por defecto no trae activada la paquetería 'snap', al menos por lo que he podido observar en una instalación de tipo 'netinstall'.  
Me disgusta la nueva paquetería -prefiero instalar las cosas de forma nativa o compilando directamente- aunque he de reconocer que es una opción a tener en cuenta cuando, por ejemplo, se desea ejecutar aplicaciones como 'Adobe Acrobat Reader' en linux que, de forma nativa, carecen una instalación típica; o también cuando, al tratar de instalar algo de forma nativa obtenemos errores y no somos capaces de resolverlos en un tiempo razonable.  

<br>
## Qué es 'snap'
La entidad que desarrolla Ubuntu -*Canonical*- creó el sistema 'snappy', al cual describen como un sistema de paquetes universal que posibilita que éstos funcionen en cualquier distribución.  
Los paquetes 'snap' son alicaciones compiladas incluyendo sus dependencias y bibliotecas. Son fáciles y rápidos de instalar, y se ejecutan en un entorno aislado. Pueden recibir las últimas actualizaciones del software.  
Uno de sus defectos es que consumen más recursos que sus homólogos instalados de forma "nativa" (mediante `sudo apt install` o complilación, por ejemplo).

<br>
## Instalación del soporte para paquetería 'snap' en Debian (y derivados)
'Snap' se instala y activa así:
~~~bash
sudo apt-get update
sudo apt install snapd
sudo snap install core
~~~

<br>
## Búsqueda de un paquete 'snap' en 'snapcraft', el repositorio de aplicaciones 'snap'
~~~bash
sudo snap find nombredelpaquetesnap
~~~

<br>
## Instalación de paquetes 'snap'
A partir de ese momento se puede instalar paquetes 'snap'
~~~bash
sudo snap install nombredelpaquetesnap
~~~
A veces puede ser necesario agregar el parámetro `--dangerous` al comando de instalación:
~~~bash
sudo snap install nombredelpaquetesnap --dangerous
~~~
Puede buscarse y descargarse paquetes 'snap' en la ["tienda oficial": snapcraft](https://snapcraft.io/store).

<br>
## Ejecución de un paquete 'snap' ya instalado
~~~bash
snap run nombredelpaquetesnap
~~~

<br>
## Comandos básicos de 'snap'
Se puede consultar la ayuda de 'snap' mediante `man snap` o `snap help --all`.  
He aquí también una [guía](https://www.mankier.com/8/snap).  

<br>
## Actualización de paquetes 'snap' instalados
~~~bash
sudo snap refresh nombredelpaquetesnap
~~~

<br>
## Desinstalación de paquetes 'snap'
Y también se pueden desinstalar paquetes 'snap'
~~~bash
sudo snap remove nombredelpaquetesnap
~~~

<br>
<br>
## Desactivación / Desinstalación completa del sistema de paquetería 'snap'
<br>
### 1. Listar los paquetes 'snap' instalados
~~~bash
sudo snap list
~~~

<br>
### 2. Desinstalar cada uno de los paquetes 'snap' instalados
~~~bash
sudo snap remove nombredelpaquetesnapinstalado
~~~

<br>
### 3. (Si procediera) Desmontar *Snap Core Service*
~~~bash
df
~~~
Tomamos nota del número a continuación de '/snap/core/'
~~~bash
sudo umount /snap/core/númeroanteriormentemencinado
~~~

<br>
### 4. Desinstalar el "sistema" 'snap'.
~~~bash
sudo apt autoremove --purge snapd
~~~

<br>
### 5. Eliminación de los "restos"
Eliminación del directorio de la aplicación
~~~bash
sudo rm -rf ~/snap
~~~
Eliminación del directorio '/var/cache/snapd'
~~~bash
sudo rm -rf /var/cache/snapd
~~~
Eliminación de otros directorios que pudieran existir
~~~bash
sudo rm -rf /snap
sudo rm -rf /var/snap
sudo rm -rf /var/lib/snapd
~~~

<br>
### 6. Purgado final
~~~bash
sudo apt purge snapd
~~~

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] nueva paquetería vol.02: incorporación de soporte 'flatpak' a Debian 10 y derivados](https://hijosdeinit.gitlab.io/howto_soporte_flatpak_debian_10/)
- [Nueva paquetería, vol. 03: cosas MOLESTAS de 'flatpak'](https://hijosdeinit.gitlab.io/cosas_molestas_flatpak_nuevapaqueteria/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://snapcraft.io/install/acrordrdc/debian">snapcraft.io</a>  
<a href="https://www.sololinux.es/que-es-y-como-instalar-snap/">SoloLinux - instalación de 'snap'</a>  
<a href="https://www.sololinux.es/como-eliminar-snap-completamente-del-sistema/">SoloLinux - desinstalación de 'snap'</a>  
<a href="https://salmorejogeek.com/2020/04/26/como-eliminar-por-completo-snap-de-ubuntu-20-04-focal-fossa/">SalmorejoGeek</a>  
<a href="https://itsfoss.com/install-snap-linux/">itsFoss - instalación de 'snap' en gnu linux</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
