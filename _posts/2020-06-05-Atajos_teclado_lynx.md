---
layout: post
title: 'Atajos de teclado en Lynx'
date: 2020-06-05
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "lynx", "navegador", "web", "texto", "sin distracciones", "privacidad", "CLI", "cli", "web browser", "gopher", "gemini"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
[Lynx](https://invisible-island.net/lynx/) es un navegador web / gopher para línea de comandos. No muestra imagenes, obviamente, sino puro texto plano e hiperenlaces. Es perfecto para una navegación sin distracciones, a la vieja usanza.  
Es multiplataforma, es decir, puede instalarse en sistemas operativos Unix, como GNU Linux, y en Windows.  

Lynx recognizes many single-character commands. This is an overview to their default bindings, with links to more detailed documentation.

<br>
## Movimiento
~~~
Down arrow     - Highlight next topic
Up arrow       - Highlight previous topic
Right arrow,   - Jump to highlighted topic
Return, Enter  - Follow selected link
Left arrow     - Return to previous topic
~~~

<br>
## Scrolling
~~~
+              - Scroll down to next page (Page-Down)
-              - Scroll up to previous page (Page-Up)
SPACE          - Scroll down to next page (Page-Down)
b              - Scroll up to previous page (Page-Up)
CTRL-A         - Go to first page of the current document (Home)
CTRL-E         - Go to last page of the current document (End)
CTRL-B         - Scroll up to previous page (Page-Up)
CTRL-F         - Scroll down to next page (Page-Down)
CTRL-N         - Go forward two lines in the current document
CTRL-P         - Go back two lines in the current document
)              - Go forward half a page in the current document
(              - Go back half a page in the current document
^              - Go to the first link on the current line
$              - Go to the last link on the current line
<              - Go to the previous link in the current column
>              - Go to the next link in the current column
#              - Go to Toolbar or Banner in the current document
~~~

<br>
## Dired
~~~
c              - Create a new file
d              - Download selected file
e              - Edit selected file
f              - Show a full menu of options for current file
m              - Modify the name or location of selected file
r              - Remove selected file
t              - Tag highlighted file
u              - Upload a file into the current directory
~~~

<br>
## Otros
~~~
? (or h)       - Help (this screen)
a              - Add the current link to a bookmark file
c              - Send a comment to the document owner
d              - Download the current link
e              - Edit the current file
E              - Edit the current link's URL (or ACTION) and use that as a goto URL.
g              - Goto a user specified URL or file
G              - Edit the current document's URL and use that as a goto URL.
i              - Show an index of documents
j              - Execute a jump operation
k              - Show list of actual key mappings
l              - List references (links) in current document
m              - Return to main screen
o              - Set your options
p              - Print to a file, mail, printers, or other
q              - Quit (Capital “Q” for quick quit)
/              - Search for a string within the current document
s              - Enter a search string for an external search
n              - Go to the next search string
N              - Go to the previous search string
v              - View a bookmark file
V              - Go to the Visited Links Page
x              - Force submission of form or link with no-cache
z              - Cancel transfer in progress
[backspace]    - Go to the History Page
=              - Show info about current document, URL and link
\              - Toggle document source/rendered view
!              - Spawn your default shell
'              - Toggle "historical" vs minimal or valid comment parsing
_              - Clear all authorization info for this session
`              - Toggle minimal or valid comment parsing
*              - Toggle image_links mode on and off
@              - Toggle raw 8-bit translations or CJK mode on or off
.              - Run external program on the current link.
,              - Run external program on the current document.
{              - Shift the screen left.
}              - Shift the screen right.
|              - Toggle line-wrap mode.  When line-wrap is off, you may use { and } to shift the screen left/right.  The screen width is set to 999.
~              - Toggle parsing of nested tables (experimental).
[              - Toggle pseudo_inlines mode on and off
]              - Send a HEAD request for the current doc or link
"              - Toggle valid or "soft" double-quote parsing
CTRL-R         - Reload current file and refresh the screen
CTRL-L         - Refresh the screen
CTRL-V         - Outside of a text input line or field, switch to alternative parsing of HTML.
               - In a form text input field, CTRL-V prompts for a key command (allows escaping from the field). Note that on most UNIX hosts, CTRL-V is bound via stty to the lnext (literal-next) code but the exact behavior of that is implementation specific.  On Solaris you must type CTRL-V twice to use it, since it quotes the following keystroke.
CTRL-U         - Inside text input line or field, erase input line (more input line commands)
               - Outside of text input or field, undo returning to previous topic.
CTRL-G         - Cancel input or transfer
CTRL-T         - Toggle trace mode on and off
;              - View the Lynx Trace Log for the current session
CTRL-K         - Invoke the Cookie Jar Page
CTRL-X         - Invoke the Cache Jar Page
numbers        - Invoke the prompt Follow link (or goto link or page) number: or the Select option (or page) number: prompt
~~~

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[Wikipedia - lynx web brower](https://en.wikipedia.org/wiki/Lynx_%28web_browser%29)  
[lynx.browser.org](http://lynx.browser.org/)</small>  

<br>
<br>
<br>
<br>
