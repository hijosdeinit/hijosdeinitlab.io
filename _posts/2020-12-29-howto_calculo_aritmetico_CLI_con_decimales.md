---
layout: post
title: "[HowTo] Cálculo aritmético desde línea de comandos (con decimales incluso)"
date: 2020-12-29
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "calculo", "calculadora", "aritmetica", "echo", "decimales", "bash", "cli", "línea de comandos", "terminal"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Desde línea de comandos:
~~~
expr "3.575*2.980" | bc -l
~~~
ô
~~~
echo "3.575/2.980" | bc -l
~~~

El comando 'expr' es incapaz de manejar decimales (y también 'echo'), de ahí que se le acompañe de 'bc', que hará que sí que pueda tratar con decimales.  

<br>
Otros comandos que permiten hacer operaciones aritméticas son: 'calc' o 'let'  

<br>
<br>
<br>
<br>
