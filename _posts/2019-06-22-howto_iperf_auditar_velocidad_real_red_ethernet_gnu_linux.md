---
layout: post
title: "[HowTo] Auditar la velocidad máxima de una red ethernet mediante 'iperf' en GNU Linux"
date: 2019-06-22
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "redes", "linux", "gnu linux", "debian", "ubuntu", "windows", "ethtool", "miitool", "full duplex", "half duplex", "ethernet", "gigabit", "iperf", "dmesg", "test", "auditoria", "velocidad", "transferencia", "ip", "ifconfig", "net-tools"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
[iPerf](https://iperf.fr/) es un programa cliente-servidor que mide la velocidad máxima que alcanzan dos máquinas conectadas en una red local.  
De esta forma podemos conocer la velocidad máxima que nos permitirá el switch o router que hay "por medio" y tratar de obtener más rendimiento modificando configuraciones como el MTU de la red.  
He probado Iperf en Linux, mas funciona también en Windows.  

<br>
### Instalación
En Debian y derivados se instala así:
~~~bash
sudo apt install iperf
~~~
O también se puede [descargar los paquetes '.deb' de 'iperf' y su librería en su versión más actualizada desde la página oficial](https://iperf.fr/iperf-download.php). Ahora se llama **'iperf3'**.  

<br>
### Modo de empleo:
Debemos lanzar iperf en modo servidor en una de las máquinas, y en la otra en modo cliente.  

<br>
Modo **SERVIDOR** en máquina 1, que se mantendrá a la escucha:
~~~bash
iperf -s
~~~

<br>
<a href="/assets/img/2019-06-22-howto_iperf_auditar_velocidad_real_red_ethernet_gnu_linux/iperf_servidor.png" target="_blank"><img src="/assets/img/2019-06-22-howto_iperf_auditar_velocidad_real_red_ethernet_gnu_linux/iperf_servidor.png" alt="'iperf', lado servidor" width="500"/></a>  
<small>'iperf', lado servidor</small>  
<br>

(*) Cuando hayamos terminado las pruebas podemos cerrarlo con Ctrl+C  

<br>
Modo **CLIENTE** en máquina 2
~~~bash
iperf -c ipdelamáquina1quehacedeservidor
~~~

<br>
<a href="/assets/img/2019-06-22-howto_iperf_auditar_velocidad_real_red_ethernet_gnu_linux/iperf_cliente.png" target="_blank"><img src="/assets/img/2019-06-22-howto_iperf_auditar_velocidad_real_red_ethernet_gnu_linux/iperf_cliente.png" alt="'iperf' en el lado del cliente" width="500"/></a>  
<small>'iperf' en el lado del cliente</small>  
<br>

(*) El test es breve y enseguida da resultados. Podemos lanzarlo varias veces y añadirle parámetros.  
<br>
Las tasas de transferencia en una red local pueden llegar a ser muy decepcionantes. No es raro, en una red gigabit ethernet, tener velocidades de tan sólo 8MB/s  

<br>
<br>
Entradas relacionadas:
- [[HowTo] Averiguar si una tarjeta de red está trabajando en modo 'full duplex' en Debian 11 y derivados](https://hijosdeinit.gitlab.io/howto_averiguar_modo_fullduplex_tarjeta_red/)
- [[HowTo] Instalación de tarjetas de red inalámbricas con chip Realtek 'RTL8812AU' en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_RTL8812AU_debian/)
- [[HowTo] Reactivación de tarjeta wireless ac 'Realtek RTL8812AU' tras actualización del kernel en Debian y derivados](https://hijosdeinit.gitlab.io/howto_reactivacion_tarjetawirelessac_realtek_rtl8812au_tras_actualizacion_kernel_debian_derivados/)
- [[HowTo] Instalación de los controladores de la tarjeta wireless Realtek RTL8822CE. (Debian 10 y derivados)](https://hijosdeinit.gitlab.io/howto_instalacion_controladores_tarjeta_wireless_Realtek_rtl8822ce/)
- [El repositorio de firmare/drivers de GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- [[HowTo] Preparar Debian y derivados para compilar software](https://hijosdeinit.gitlab.io/howto_preparar_debian_para_compilar/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.redeszone.net/redes/iperf-manual-para-medir-ancho-de-banda-entre-dos-ordenadores-en-lan/">RedesZone</a>  
<a href="http://logica10mobile.blogspot.com/2011/02/la-verdadera-velocidad-de-transferencia.html">logica10mobile</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
