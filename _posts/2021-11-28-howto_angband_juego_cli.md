---
layout: post
title: "[HowTo] 'angband', juego *roguelike* para la terminal ('CLI'), padre de 'zangband'"
date: 2021-11-27
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "juegos", "linea de comandos", "terminal", "cli", "zangband", "angband", "roguelike", "mangband", "moria", "nethack", "umoria"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Debian pone a disposición de todos un [listado oficial de juegos para terminal de Debian](https://blends.debian.org/games/tasks/console).</small>  

<br> 
Uno de estos juegos es [<big>**'angband'**</big>](http://zangband.org/).  
@@@@@['Zangband' en 'SourceForge'](https://sourceforge.net/projects/zangband/)  

<br>
<a href="/assets/img/2021-11-27-howto_zangband_juego_cli/angband.png" target="_blank"><img src="/assets/img/2021-11-27-howto_zangband_juego_cli/angband.png" alt="'Angband'" width="500"/></a>  
<small>'AngBand'</small>  
<br>

<br>
<a href="/assets/img/2021-11-27-howto_zangband_juego_cli/zangband.jpg" target="_blank"><img src="/assets/img/2021-11-27-howto_zangband_juego_cli/zangband.jpg" alt="'Zangband'" width="700"/></a>  
<small>'ZangBand'</small>  
<br>

<br>
'Zangband' es un juego de rol *roguelike* libre que está disponible para casi cualquier arquitectura y/o sistema operativo (Windows, Mac OSX, DOS, GNU Linux).  
> <small>*Roguelike*, también denominado *videojuego de calabozos* o *videojuego de mazmorras* es un subgénero de los videojuegos de rol que se caracteriza por una aventura a través de mazmorras, a través de niveles generados por procedimientos al azar, videojuegos basados en turnos, gráficos basados en fichas y la muerte permanente del personaje del jugador. La mayoría de los videojuegos de mazmorras se basan en una narrativa de fantasía alta, que refleja su influencia de los juegos de rol de mesa como Dungeons & Dragons.</small>  
<small>Aunque los videojuegos de mazmorras Apple Manor y Sword of Fargoal son anteriores a este, el videojuego de 1980 llamado Rogue, el cual es un videojuego basado en ASCII que se ejecuta en el terminal o emulador de terminal, es considerado como el precursor y el homónimo del género, con videojuegos derivados de reflejos de Rogue y sus gráficos basados en caracteres o sprites. Estos videojuegos se popularizaron entre los estudiantes universitarios y los programadores de computadoras de la década 1980 y 1990, lo que condujo a un gran número de derivaciones y variantes, pero se adhirieron a estos elementos de juego comunes, a menudo titulados como la "Interpretación de Berlín". Algunas de las variantes más conocidas incluyen a *'Hack'*, *'NetHack'*, *'Ancient Domains of Mystery'*, *'Moria'*, *'Angband'*, *'Tales of Maj'Eyal'* y *'Dungeon Crawl Stone Soup'*.</small>  

<br>
El juego 'RPG' (*role playing game*) 'Zangband' fue lanzado en 1999. Los desarrolladores del juego -'Zangband dev team'- lo describen así:
> «The game of Zangband is a single player computer role playing game set in the world (although 'worlds' might perhaps be more accurate) of Amber created by the author Roger Zelazny. Zangband is a variant of the game [Angband](https://rephial.org/) which is descended from the game [Moria](http://beej.us/moria/) which is in turn based upon the game Rogue (hence Zangband's classification as a roguelike game). Rogue was, perhaps, the first computer game to use the terminal screen graphically with ASCII characters representing the player, dungeon features, objects and monsters as opposed to the text adventures of its day.»  



<small>Del antiguo juego 'Moria' -del cual procede 'Angband' y, por consiguiente, 'Zangband'- existe hoy un *fork* llamado ['uMoria'](https://umoria.org/).</small>  
<small>Otro juego de mazmorras interesante es ['Legerdemain'](https://www.freegamesutopia.com/game/legerdemain/333/), aunque pareciera no es *open source*.  

<br>
<br>
Entradas relacionadas:  
- [Listado oficial de juegos para línea de comandos de Debian](https://hijosdeinit.gitlab.io/listado_oficial_juegos_terminal_cli_debian/)
- [https://hijosdeinit.gitlab.io/tron_online_ssh_terminal/](https://hijosdeinit.gitlab.io/tron_online_ssh_terminal/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/Videojuego_de_mazmorras">WikiPedia - videojuego de mazmorras</a>  
<a href="https://es.wikipedia.org/wiki/Angband">WikiPedia - 'Angband'</a>  
<a href="https://es.wikipedia.org/wiki/Moria_(videojuego)">WikiPedia - 'Moria'</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
