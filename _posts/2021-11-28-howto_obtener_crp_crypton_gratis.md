---
layout: post
title: "[HowTo] Obtener unos céntimos de 'CRP' ('crypton') gratis"
date: 2021-11-28
author: Termita
category: "criptodivisas"
tags: ["internet 4.0", "utopia", "utopia ecosystem", "ecosistema utopia", "crypton", "crp", "redes", "idyll", "web", "gopher", "gemini", "utopia", "crypton", "bitcoin", "criptodivisas","minado", "sistemas operativos", "memoria", "ram", "memoria ram", "seguridad", "maquina virtual", "sandbox", "desconfianza", "liveusb", "maquina virtual", "virtualizacion", "privacidad"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
'Crypton' ('CRP') es la criptodivisa del [ecosistema Utopia](https://u.is/en/), una supuesta internet "alternativa" basada en la privacidad que estoy probando todo lo a fondo que puedo en una máquina virtual dada mi desconfianza en todo presunto vendedor de crecepelo que aparece por ahí.  

<br>
<a href="/assets/img/2021-11-28-howto_obtener_crp_crypton_gratis/utopia_ecosystem.png" target="_blank"><img src="/assets/img/2021-11-28-howto_obtener_crp_crypton_gratis/utopia_ecosystem.png" alt="'Utopia'" width="300"/></a>  
<br>

Se puede obtener CRP de 3 formas:
- minándolo mediante un bot que hace uso de la memoria RAM de tu ordenador
- llevando a cabo algún tipo de transacción, el equivalente a una compra o venta convencional.
- aprovechando alguna promoción, regalo o recompensa  

<br>
En lo que hace referencia a este tercer método uno de los procedimientos es el siguiente:
1. Desde el navegador 'Idyll' abrir la página 'http://faucet'
2. Introducir tu clave pública del ecosistema 'Utopia'
3. Introducir correctamente el 'captcha'  

Hecho.

<br>
<a href="/assets/img/2021-11-28-howto_obtener_crp_crypton_gratis/crp_faucet.png" target="_blank"><img src="/assets/img/2021-11-28-howto_obtener_crp_crypton_gratis/crp_faucet.png" alt="obtención de unas migajas de CRP a través de http://faucet" width="600"/></a>  
<br>
 
<br>
<br>
Entradas relacionadas:  
- [[HowTo] bot de minado 'crypton' (ecosistema 'Utopia') SIN activar 'upnp'](https://hijosdeinit.gitlab.io/howto_eludir_activacion_upnp_en_bot_minado_crpton_utopia/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  
<br>
<br>
<br>
<br>
<br>
<br>
