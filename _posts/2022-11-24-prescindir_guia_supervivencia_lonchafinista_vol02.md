---
layout: post
title: "Loco por PRESCINDIR: una guía de supervivencia lonchafinista (vol.02)"
date: 2022-11-24
author: Termita
category: "internet 4.0"
tags: ["lonchafinismo", "ahorro", "diogenes", "fatiga por suscripcion", "suscripcion", "baja", "servicios", "gafam", "prescindir", "futil", "innecesario"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Lograr ahorrar dinero o recursos (tiempo, por ejemplo) suele ser fuente de satisfacción para nosotros, esos a los que los mariscófilos, posando puño en alto ante las cámaras, aún hoy se refieren como famélica legión. A eso vamos, no se preocupen.  
Cierto suele ser también, a mi entender, el adagio que señala que «por ahorrar un duro somos propensos a gastar duro y medio».  
El equilibrio es importante.  

Buscar una cosa a mejor precio tiene sus exigencias en tiempo o esfuerzo, recursos valiosos... Está bien ahorrar en "tarifas" y mordidas, renegociar... demuestra que uno comienza a ser consciente de que tiene un problema de dependencia.  
Mas, ¿y **renunciar** a esa cosa, qué coste / beneficio tiene?  

<br>
Eso último, que raramente solemos planteárnoslo, suele ser menos exigente, proporciona igual o mayor satisfacción y, es generalmente más sencillo, dado que la mayoría de lo que consumimos resulta ser totalmente innecesario, cuando no un derroche. Yo pienso que la mayoría de esas cosas nos hace más tontos.  
Ciñéndome al tema que me ocupa, o sea, la tecnología: suscripciones a servicios de *streaming* (<small>'torrent' y 'amule' existen, maeses, reembarquemos en el navío de bandera negra y tibias cruzadas</small>), "nubes" (<small>érase una vez una nube suscepible de ser cifrada, ilimitada y supuestamente "gratuíta" (aún) llamada 'Telegram'</small>), redundancias, limosnas (<small>hoy todo el mundo pida dinero por internet y hay quienes dejan caer unas monedas: términos como premium, patreon u *originals* (<small>algún día tocará hablar de la empresa Ivoox</small>) se repiten hasta la arcada en esta red de la mendicidad *cuatropuntocero*</small>), etc, tarifas de datos sobredimensionadas que nunca agotamos a fin de mes, conexiones de banda ancha que nos vienen grandes (<small>¿realmente usted necesita 300mb simétricos? ¿los usa efectivamente? y/o efectivamente también, aquel al que usted por ello paga, ¿se los proporciona? ¿no es verdad que le están tomando el pelo, mintiéndole?</small>), tarifas planas en llamadas telefónicas que nunca usamos, duplicidades varias (conexiones "potentes" a internet en móvil y en casa), tecnologías obsoletas impuestas por el proveedor (telefonía fija, por ejemplo), chismes que adquirimos para hacer lo mismo que hacían aquellos a los que sustituímos, impuestos, permisos, etcétera, etcétera...  
Y de eso va -de reestructurar, de despedir, de despachar, de prescindir, de darle la patada a todo eso que acabo de enumerar- este volumen 2 de la guía de supervivencia lonchafinista.  

<br>
Y ya que estamos a vueltas con el refranero castellano, conviene recordar algunos -refranes- más:  
«Contra el vicio de pedir la virtud de no dar».  
«Un tonto y su dinero nunca están demasiado tiempo juntos».  

<br>
Quizás es ese uno de los motivos de la existencia de los ricos y de los pobres, porque entre los primeros hay tantos codiciosos como manirrotos y despreocupados entre los segundos. Y la naturaleza, en su inmensa sabiduría, tiende a equilibrar las cosas, y un río de dinero -que no de honra- fluye de los bolsillos de los segundos hacia las arcas de los primeros. El simplón experimentará envidia, el despierto se preguntará "por qué".  
«Va a consumir su puta madre», decían en burbuja.info cuando aquella crisis inmobiliaria devastó el "mundo libre". Eso quedó grabado a fuego.  
«La deuda es esclavitud» y Amazon te ofrece pagar sus artículos a plazos. Bezos, que dicen es el hombre mas rico del mundo, evidentemente, no te quiere bien. Tampoco procura tu bien ese que pareciera es su compadre, el tal señor Cofidis, patrón de la tecnofilia creditofágica, mecenas del ciclismo.  

<br>
Cualquier cosa que me anuncian por internet va inexorablemente a la libreta donde me apunto toda esa serie de cosas que me importan una puta mierda y que jamás compraré ni contribuiré a sustentar. Un agujero negro sin par cuando se trata de complementar y reforzar los habituales, mas no infalibles (todo se andará), 'PiHole', 'AdGuard' o **'ad nauseam'** que tantos de nosotros desplegamos en nuestros tan dignos como miserables servidores domésticos. Oh, tacaños recicladores, desconfiados conspiranoicos.  

<br>
Necesito, ahora más que nunca, seguir escurriendo el bulto, ser un miserable, un agrio rata, un rancio que le espeta a lo futil -que es casi todo- un rotundo "NO", como aquel que reiteradamente hay que responderle a cualquiera de las preguntas que ese *spyware* llamado MicroSoft Windows 10 haga durante su terrorífico y nauseabundo proceso de instalación, después de cada uno de los manidos e interminables "espere-un-momento".  
No consumir, escaquear impuestos, comisiones, renovar filiación a la muy honorable cofradía de la virgen del puño cerrado.  
Porque casi todo recurso que no me reservo y despilfarro y regalo a mercaderes es ocupado en putas y en cocaína, imagino. En lugar de que, a costa de usted, se tome una cerveza el director del banco, mejor llévese dignamente sus ahorros y tómesela usted, y a ver si este tinglado posmoderno, hipócrita, ratero y extractivo a costa del lomo de ustedes y del mío propio entra en *default* de una vez por todas y vemos ni que sea a unos pocos dellos, durmiendo entre cartones. ¿Caerá esa breva?  
Hay que ver, que cada vez que pienso en la quiebra de todos esos gigantes con pies de barro -Google, Microsoft, Amazon, Facebook / Meta, Twitter, Tesla, Oracle, Netflix, Disney, Telefónica Movistar, Vodafone, Prisa, Vocento.., o cualquiera de los estados que componen el hoy posmoderno y absurdo occidente... o aquellos que, sin que se les llame, me molestan con anuncios y propagandas, o me piden que me trague sus cookies, *legitimate interest* y traqueos varios mientras pacíficamente navego por intenet- pareciera se me estíra la comisura de las fauces en zorruno rictus mixtura de jubilo y mofa.  
Me llaman mala persona. Son días de *blackfraidai*, cómprese algo, insensato, porque usted lo vale y dice Bezos que mañana estará más caro.  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Internet del 2022 a velocidades de 256k: una guía de supervivencia lonchafinista (vol.01)](https://hijosdeinit.gitlab.io/howto_internauta_2022_a_256kbps/)
- [Instalación de 'amfora', navegador gemini para línea de comandos, en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_amphora_gemini_web_browser/)
- ['Gopher' hoy](https://hijosdeinit.gitlab.io/Gopher_hoy/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="@@@">@@@</a>  
<a href="@@@">@@@</a>  
<a href="@@@">@@@</a>  
<a href="@@@">@@@</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>










zzzz  

