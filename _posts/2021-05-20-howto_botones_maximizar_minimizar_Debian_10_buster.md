---
layout: post
title: "[HowTo] Incorporar los botones de 'maximizar'/'minimizar' a las ventanas en Debian 10 (Gnome)"
date: 2021-05-20
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "wm", "ventanas", "gestor de ventanas", "gnome", "botones", "windows manager", "display manager"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Constato que en el entorno Gnome de una instalación "netinstall" de Debian 10 los botones de **maximizar** y de **minimizar** no aparecen por defecto en las ventanas.  
Esto se debe a que los desarrolladores de 'Gnome' decidieron que sus usuarios no necesitaban esos botones en las ventanas, tan sólo el botón de **cerrar**.
<br>
Tal cual, sin configurar la aparición de estos botones, el maximizado o minimizado de una ventana se puede llevar a cabo sin necesidar de pulsar en ningún botón:  
<br>
Maximizar:
- click derecho (o mayúsculas + barra espaciadora) en la ventana → maximizar
- doble click en la ventana  

<br>
Minimizar:
- click derecho (o mayúsculas + barra espaciadora) en la ventana → minimizar
- super + h  

<br>
<br>
Asimismo, los botones de maximizar y/o minimizar pueden ser habilitados de 2 formas:
- mediante la herramienta 'Retoques' ('gnome-tweak-tool').
- por línea de comandos

<br>
## Habilitar los botones maximizar / minimizar mediante la herramienta 'Retoques' ('gnome-tweak-tool')
'Retoques' → Barras de título de las ventanas → Botones de la barra de título:
- Maximizar: activado
- Minimizar: activado  

<br>
<a href="/assets/img/2021-05-20-howto_botones_maximizar_minimizar_Debian_10_buster/20210520_habilitando_botones_maximizar_minimizar_gnome-tweak-tool.png" target="_blank"><img src="/assets/img/2021-05-20-howto_botones_maximizar_minimizar_Debian_10_buster/20210520_habilitando_botones_maximizar_minimizar_gnome-tweak-tool.png" alt="Habilitando los botones 'maximizar'/'minimizar' desde 'gnome-tweak-tool' ('Retoques')" width="800"/></a>  
<small>Habilitando los botones 'maximizar'/'minimizar' desde 'gnome-tweak-tool' ('Retoques')</small>  
<br>

<br>
## Habilitar los botones maximizar / minimizar por línea de comandos
Esto último no lo he probado mas teóricamente funciona, sin necesidad de recurrir a 'gnome-tweak-tool':
~~~bash
gsettings set org.gnome.desktop.wm.preferences button-layout ":minimize,maximize,close"
~~~
La razón de recurrir a ejecutar un comando como ese y no bastar con editar un simple archivo de configuración en texto plano es que 'Gnome' usa 'dconf' -una especie de base de datos de configuración en binario, similar al registro de MS Windows- para su configuración.

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.linuxquestions.org/questions/debian-26/minimize-windows-in-debian-10-a-4175685689/">LinuxQuestions</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
