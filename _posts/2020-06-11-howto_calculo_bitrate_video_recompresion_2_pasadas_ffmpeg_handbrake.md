---
layout: post
title: '[HowTo] Cálculo del bitrate de un video para su recompresión en 2 pasadas'
date: 2020-06-11
author: Termita
category: "multimedia"
tags: ["sistemas operativos", "multimedia", "2 pass", "2 pasadas", "HandBrake", "bitrate", "ffmpeg"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
La forma más eficaz de recomprimir un video -para que ocupe menos- con la menor pérdida posible es mediante [ffmpeg](), en **dos pasadas** y aplicando el **bitrate** adecuado.  
El bitrate es la cantidad de datos por segundo que tendrá el video. Cuantos más Kb/s tenga será de mejor calidad pero el tamaño resultante será también considerable.  
El bitrate **adecuado** no es más que aquel que, atendiendo a las características del video origen (su códec de audio y de video, su duración, etc...) y al tamaño del video resultante que deseamos, hará que éste tenga la menor pérdida de calidad posible.  
La recompresión en dos pasos se puede hacer por línea de comandos (CLI) o bien a través de un interfaz gráfico (gui) como puede ser **HandBrake**.  
Para calcular el bitrate adecuado que habrá de aplicarse como parámetro existe una **fórmula**:  
<small>                              [tamaño en Mb deseado del video resultante] x []
bitrate de video que aplicaremos  =  --------------------------------------------------------  -  [bitrate del audio]  -  10
				               [duración del video en segundos]
</small>

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Extraer mediante ffmpeg el audio de un archivo de video](https://hijosdeinit.gitlab.io/howto-ffmpeg-Extraer-audio-de-archivo-de-video/)
- [[HowTo] Convertir vídeo ‘.ogv’ a otros formatos](https://hijosdeinit.gitlab.io/howto_convertir_ogv_a_otros_formatos/)
- [[HowTo] Incorporar pista de audio a una pista de video](https://hijosdeinit.gitlab.io/howto_incorporar_pista_audio_a_video/)
- [[HowTo] Trocear un video](https://hijosdeinit.gitlab.io/howto_trocear_video/)
- [[HowTo] instalacion de HandBrake en Ubuntu y derivados](https://hijosdeinit.gitlab.io/howto_instalacion-handbrake-ubuntu-apt/)
- [[HowTo] Extracción de imagenes de un video mediante ffmpeg (GNU Linux)](https://hijosdeinit.gitlab.io/howto_ffmpeg_extraer_fotogramas_de_archivo_video/)  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[]()  
[]()  
[]()</small>  
<br>
<br>
<br>
<br>
