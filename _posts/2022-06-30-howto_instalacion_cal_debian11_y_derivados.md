---
layout: post
title: "[HowTo] Instalar el comando 'cal' en Debian y derivados"
date: 2022-06-30
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "GNU linux", "linux", "Debian", "cal", "calendario", "apt", "cli", "linea de comandos", "terminal", "instalacion", "instalar"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
['cal'](https://www.mankier.com/1/cal) es un comando para la terminal de GNU Linux que muestra el calendario (*'cal' es la abreviatura de 'calendario'*).  
Es un comando muy simple y apenas posee parámetros.  

<br>
No es bueno dar por hecho las cosas. Yo lo hice y acabo de percatarme que el comando 'cal' no viene por defecto en Debian 11.3 (y es posible que tampoco en algunas otras distribuciones GNU Linux). El comando 'date', que muestra la fecha en la línea de comandos, sin embargo, sí está instalado por defecto, tal como esperaba.  

<br>  
En Debian y derivados el comando 'cal' forma parte del [paquete 'ncal'](https://packages.debian.org/bullseye/ncal). Para instalarlo basta con ejecutar:
~~~bash
sudo apt-get update
sudo apt install ncal
~~~

<br>
Un ejemplo de empleo del comando 'ncal' es:  
<span style="background-color:#042206"><span style="color:lime">`ncal -b`</span></span>:
~~~
     Mayo 2024        
lu ma mi ju vi sá do  
       1  2  3  4  5  
 6  7  8  9 10 11 12  
13 14 15 16 17 18 19  
20 21 22 23 24 25 26  
27 28 29 30 31        
~~~  

<br>
<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[LinuxHint - cal](https://linuxhint.com/linux-cal-command/)  
[]()</small>  

<br>
<br>
<br>
<br>
