---
layout: post
title: "Colores HTML, un par de enlaces"
date: 2020-04-25
author: Termita
category: "programación"
tags: ["web", "programación", "html", "diseño"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---

**ℹ** [htmlcolorcodes](https://htmlcolorcodes.com/es/)  
En esta página web se pueden consultar los códigos de color HTML, códigos de color Hex, RGB y HSL valores con nuestra selección de colores, tablas de colores y también los [nombres de colores HTML](https://htmlcolorcodes.com/es/) con su correspondiente nombre, código Hex y valor RGB.  
<br>
<br>
**ℹ** [EncyColorPedia](https://encycolorpedia.es/)  
Pintura, Colores web, Colores HTML, nombre de Colores. Una especie de enciclopedia.
