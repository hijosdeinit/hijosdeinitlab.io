---
layout: post
title: "[HowTo] Amule como servicio / demonio. vol.01"
date: 2022-01-14
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "amule", "amule-daemon", "demonio", "servicio", "amuleweb", "amulegui", "amulecmd", "transmission", "transmission-cli", "transmission-daemon", "Debian", "directorio", "archivo", "configuracion", "settings.json", "stats.json", "torrent", "resume", "descargas", "p2p", "ed2k", "kad", "headless"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>entorno: RaspBerry Pi 4b 4gb RAM, SD y HDD usb. RaspBian Buster 32bits.</small>  

<br>
<br>
De ['amule'](http://www.amule.org/), la conocida utilidad de descarga p2p, no es imprescindible instalar el programa "entero", interfaz gráfica incluída. <small>Lo mismo ocurre con 'Transmission', el gestor de descargas 'torrent'.</small>  
<small>Es este el campo semántico de lo que suele denominarse ["***headless***"](https://techoverflow.net/2019/05/17/what-is-a-headless-program-or-application/): cuando un programa o sistema opera sin un interfaz gráfico.</small>  
<br>
Basta con instalar en una máquina un servicio o demonio -'amule-daemon' o 'amuled'- y, desde el navegador web o el comando 'amulecmd' de otra máquina remota, se pueden gestionar las descargas.  

<br>
<br>
## Procedimiento para incorporar a un sistema Debian el demonio/servicio 'amule-daemon'/'amuled'
<br>
### Créese el usuario 'amule'
~~~bash
sudo adduser amule
~~~

<br>
### Establézcase contraseña para el usuario 'amule'
~~~bash
sudo passwd amule
~~~

<br>
### Ábranse en el router los puertos necesarios para que 'amule' pueda trabajar
Los puertos necesarios son:
- 4062 TCP [que sirve para ]
- 4072 UDP [que sirve para ]
- 4065 UDP [que sirve para ]  

Si se deseara gestionar 'amule-daemon' desde el exterior de la red local y no hubiera una VPN configurada, sería necesario también abrir los puertos que usan el interfaz web, el interfaz remoto gui ('amulegui') y el interfaz remoto cli ('amulecmd').
- 4011 TCP [interfaz web]
- 4012 TCP [interfaz remoto gui ('amulegui') e interfaz remoto cli ('amulecmd')]  

<br>
### Instálese 'amule-daemon'
'amule-daemon' es el demonio que hará que, sin necesidad de interfaz gráfico, 'amule' funcione como una especie de servidor en el que gestionar -tanto de forma remota como de forma local- descargas 'ed2k' y 'kad'.
~~~bash
sudo apt-get update
sudo apt install amule-daemon
~~~
El sistema recomienda instalar 'amule-utils'. <small>Yo no lo hice... aunque es posible que más tarde, por probar, lo haga.</small>  
<br>
Compruébese el estado del demonio / servicio 'emule-daemon'
~~~bash
sudo systemctl status amule-daemon.service
~~~
~~~
● amule-daemon.service - LSB: Daemonized version of aMule.
   Loaded: loaded (/etc/init.d/amule-daemon; generated)
   Active: active (exited) since Sat 2022-01-15 16:33:00 CET; 4min 46s ago
~~~

<br>
### Ejecútese 'amuled'
'amuled' podríase decir que es equivalente a 'amule-daemon'. La diferencia es que 'amule-daemon' es un demonio y 'amuled' es un comando que, aunque 'amule-daemon' no esté corriendo, permite en un momento dado ejecutarlo; en tal caso, cuando se cierre 'amuled', en principio, se cerraría también 'amule-daemon'.
Como usuario 'amule'
~~~bash
su amule
amuled
~~~
Se produce un **error**, que solucionaremos **más adelante**.  
Lo importante es que con la primera ejecución de 'amuled' se generan los **archivos de configuración** para el usuario 'amule', entre los cuales cabe destacar /home/amule/.aMule/amule.conf'  

<br>
### Configúrese 'amule-daemon'
Desactívese el demonio / servicio 'amule-daemon'
~~~bash
sudo systemctl stop amule-daemon.service
~~~
Hágase copia de seguridad de '/etc/default/amule-daemon' (<small>aunque ya exite una</small>)
~~~
sudo cp /etc/default/amule-daemon /etc/default/amule-daemon.bkp001
~~~
Configúrese 'amule-daemon' editando **'/etc/default/amule-daemon'**
~~~bash
sudo nano /etc/default/amule-daemon
~~~
Por defecto lo encontrarán así:
~~~
# Configuration for /etc/init.d/amule-daemon

# The init.d script will only run if this variable non-empty.
AMULED_USER=""

# You can set this variable to make the daemon use an alternative HOME.
# The daemon will use $AMULED_HOME/.aMule as the directory, so if you
# want to have $AMULED_HOME the real root (with an Incoming and Temp
# directories), you can do `ln -s . $AMULED_HOME/.aMule`.
AMULED_HOME=""
~~~
Déjese ASÍ:
~~~
# Configuration for /etc/init.d/amule-daemon

# The init.d script will only run if this variable non-empty.
AMULED_USER="amule"

# You can set this variable to make the daemon use an alternative HOME.
# The daemon will use $AMULED_HOME/.aMule as the directory, so if you
# want to have $AMULED_HOME the real root (with an Incoming and Temp
# directories), you can do `ln -s . $AMULED_HOME/.aMule`.
AMULED_HOME="/home/amule"
~~~

Iníciese el servicio / demonio 'amule-daemon'
~~~bash
sudo systemctl restart amule-daemon.service
~~~
Se producirá nuevamente un **error**, el mismo de antes; resumiendo:
> ERROR: aMule daemon cannot be used when external connections are disabled. To enable External Connections, use either a normal aMule, start amuled with the option --ec-config or set the key "AcceptExternalConnections" to 1 in the file ~/.aMule/amule.conf  

Para solucionar el error hay que activar **'external connections'** en '/home/amule/.aMule/amule.conf'. Y ya puestos, cámbiense otros parámetros:  
Procédase a configurar 'amule-daemon' editando el archivo de configuración '/home/amule/.aMule/amule.conf'  
<br>
Créense los subdirectorios donde irán las descargas completadas y las descargas en curso. Por ejemplo:
~~~bash
mkdir /home/amule/Descargas/completados_amule
mkdir /home/amule/Descargas/encurso_emule
~~~
Establézcase propietario y grupo de ambos subdirectorios a amule:amule, es decir usuario 'amule', grupo 'amule':
~~~bash
sudo chown amule:amule /home/amule/Descargas/completados_amule
sudo chown amule:amule /home/amule/Descargas/encurso_emule
~~~
<br>
Párese el servicio / demonio 'amule-daemon'
~~~bash
sudo systemctl stop amule-daemon.service
~~~
Genérense *hashes* de contraseña a partir de la *contraseña deseada* para el servicio / demonio 'amule-daemon' (<small>esto es así porque en el archivo de configuración '/home/amule/.aMule/amule.conf' no pueden -ni deben- ir las contraseñas en texto plano, aunque, en cierta manera, podría ser un absurdo dado que lo que se cifra con md5 puede también descifrarse ¿?</small>)
~~~bash
echo -n contraseñadeseada | md5sum
~~~
ô
~~~bash
echo -n contraseñadeseada|md5sum|cut -d ' ' -f1
~~~
Ese *hash md5* generado a partir de la contraseña que hayamos decidido se empleará a continuación...  
<br>
Como usuario 'amule':
~~~bash
su amule
nano /home/amule/.aMule/amule.conf
~~~
... modifíquense las siguientes líneas:
~~~
Nick=elnickquetedélagana

AcceptExternalConnections=1

[WebServer]
Enabled=1
Password=hasdelacontraseñadeseada

[eMule]
Port=4662
UDPPort=4672

[ExternalConnect]
ECPort=4612
ECPassword=hashdelacontraseñadeseada

#TempDir=/home/amule/.aMule/Temp
TempDir=/home/amule/Descargas/encurso_amule
#IncomingDir=/home/amule/.aMule/Incoming
IncomingDir=/home/amule/Descargas/completados_amule
~~~

Iníciese el servicio / demonio 'amule-daemon'
~~~bash
sudo systemctl start amule-daemon.service
~~~
Vía navegador -desde cualquier máquina de la red local- ya es posible acceder al [interfaz web de control de 'amule-daemon'](http://ipdelservidor:4011/login.php)  

<br>
<a href="/assets/img/2022-01-15-howto_incorporar_tema_visual_atractivo_al_interfaz_web_amule/interfaz_web_amuled_por_defecto.png" target="_blank"><img src="/assets/img/2022-01-15-howto_incorporar_tema_visual_atractivo_al_interfaz_web_amule/interfaz_web_amuled_por_defecto.png" alt="interfaz web -remota o local- de 'amule-daemon' por defecto" width="500"/></a>  
<small>interfaz web -remota o local- de 'amule-daemon' por defecto</small>  
<br>

A parte de mediante el navegador web, otras formas de controlar 'amule-daemon', el demonio de 'amule' que se ejecuta en nuestro "servidor" -desde el propio servidor (localhost), desde otra máquina ubicada en la red local, o desde otra máquina ubicada fuera de la red local (<small>vía VPN o directamente si se han abierto en el router los puertos 4711 y/o 4712</small>), son:
- mediante el comando 'amulegui' (<small>puerto 4712</small>)  
- mediante el comando 'amulecmd' (<small>puerto 4711</small>)  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Amule como servicio / demonio. vol.02: compartir directorios con el demonio de 'amule'](https://hijosdeinit.gitlab.io/howto_amule_compartir_directorios/)
- [[HowTo] directorios y archivos de transmission-daemon en Debian y derivados](https://hijosdeinit.gitlab.io/howto_directorios_de_transmission_cli_en_debian_y_derivados/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
