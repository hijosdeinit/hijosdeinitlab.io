---
layout: post
title: "[HowTo] Expandir (*unshorten*) URLs acortadas (*shortened*)"
date: 2022-01-14
author: Termita
category: "internet 4.0"
tags: ["internet 4.0", "shorten", "shortened", "unshortened", "unshorten", "expandir", "desacortar", "bitly", "bit.ly", "buscador", "metabuscador", "altavista", "searx", "frontend", "fuckoffgoogle", "privacidad", "ads", "publicidad", "no ads", "tracking", "traqueo", "rastreo", "seguimiento", "cookies", "google", "descentralizacion", "libertad", "seguridad", "curl"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
¿Qué significa URL?  
¿Qué es acortar una URL?
Ventajas
Inconvenientes (¿Por qué puede interesar expandir / desacortar una url?)
Procedimiento para expandir / desacortar una URL  
~~~bash
curl --head "urlacortada"
~~~
<br>
Por ejemplo, <span style="background-color:#042206"><span style="color:lime">`curl --head "https://bit.ly/3ej3Vna"`</span></span> daría como resultado:  
> HTTP/2 301
server: nginx
date: Fri, 14 Jan 2022 14:22:00 GMT
content-type: text/html; charset=utf-8
content-length: 144
cache-control: private, max-age=90
content-security-policy: referrer always;
location: https://gitlab.com/ryzen-controller-team/ryzen-controller
referrer-policy: unsafe-url
via: 1.1 google
alt-svc: h3=":443"; ma=2592000,h3-29=":443"; ma=2592000  

... es decir: **'https://gitlab.com/ryzen-controller-team/ryzen-controller'**  

<br>
<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/Searx">Wikipedia - searX</a>  
<a href="https://blog.desdelinux.net/fuck-off-google-searx-2-interesantes-proyectos-conocer-utilizar/">DesdeLinux</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
