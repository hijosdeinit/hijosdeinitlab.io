---
layout: post
title: "Cuando se mezcla software libre con propaganda política"
date: 2022-02-20
author: Termita
category: "internet 4.0"
tags: ["internet 4.0", "open source", "gnu linux", "linux", "codigo abierto", "software libre", "propaganda", "publicidad", "propaganda politica", "secta", "ideologia", "fanboys", "adblock", "hack", "mendicidad 2.0", "sacerdotes", "evangelizadores"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
"El gato es mío y hago con él lo que quiero".  
Entiendo, criatura.  
Mas, señalarle cuán desagradable es lo que acontece cuando comiendo un estupendo plato de lentejas los dientes encuentran, de la peor de las maneras posibles, una piedra.  
Y mezclar churras con merinas no es que sea muy coherente. Pero el gato es suyo. Allá usted y su gato.  

<br>
Un ejemplo servirá para ilustrarlo:  
[Mediados del año 2020, web de la distribución ligera y minimalista 'Crunchbang plus plus'](https://web.archive.org/web/20200926173219/https://crunchbangplusplus.org/):  

<br>
<a href="/assets/img/2022-02-20-software_libre_mezclado_politica/crunchbangplusplus_evangelizadoresblacklivesmatters.png" target="_blank"><img src="/assets/img/2022-02-20-software_libre_mezclado_politica/crunchbangplusplus_evangelizadoresblacklivesmatters.png" alt="los muchachos de CrunchBang ++ haciendo propaganda de mierdas ideológicas" width="500"/></a>  
<small>los muchachos de CrunchBang ++ haciendo propaganda de mierdas ideológicas</small>  
<br>

Glosario:  
'BLM': *Black Lives Matters*, movimiento político del primer mundo, es decir -traducido a mi lenguaje y mi manera de ver las cosas-, movimiento "a-mí-qué-me-importa".

Y a todo esto: ¿Qué tan relevante es que usted ande llorando por los rincones por política? Yo solo vine a descargar una distribución GNU Linux, no a que me adoctrinen.  

Y lo mismo se aplica a alguna excelente página web que trata de software libre: propaganda ideológica posmoderna -o posmoderne- mezclada con contenidos tecnológicos.  
Muchachos, sé que el perro es vuestro y con él hacéis lo que queréis, mas... quien os visita, ¿por qué lo hace? ¿por temas tecnológicos ô por estupidas ideologías de niñatos del primer mundo?  

Esto -al igual que la tan habitual mendicidad (pedir limosna vía patreon, paypal, etcétera) al final resulta como la publicidad de Youtube o de esta inflada web cuatropuntocero: Todo aquel producto que, logrando saltarse el agujero de 'pihole', me interrumpa con publicidad / propaganda es producto que jamás compraré.  
Y, mientras tanto, el mundo seguirá girando... y el Sol amaneciendo por el oriente y desapareciendo por el poniente. Lo importante, lo verdadero y lo auténtico tiene una extraña querencia a perdurar, a prevalecer por encima de efímeras modas, sin que para ello sea necesario "soporte" ni propaganda alguna.  

<br>
<br>
<br>
Entradas relacionadas:
- []()
- []()  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  
<br>
<br>
<br>
<br>
<br>
<br>
