---
layout: post
title: "[HowTo] Buscadores web respetuosos con el internauta"
date: 2022-04-17
author: Termita
category: "internet 4.0"
tags: ["internet 4.0", "buscadores", "buscador", "web", "www", "motores de busqueda", "motor de busqueda", "altavista", "google", "bing", "duckduckgo","fuckoffgoogle", "searx", "yandex", "qwant", "brave", "instancia", "red descentralizada", "privacidad", "anonimato", "maltrato", "ad", "anuncio", "censura", "neutralidad de la red", "filtro", "algoritmo"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
[SearX](https://searx.space/)  
[FuckOffGoogle](https://search.fuckoffgoogle.net/)  
[QWant](https://www.qwant.com/)  
[Yandex](https://yandex.com/)  
[Brave](https://search.brave.com/)  

<br>
<br>
<br>


<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] 'searX', un metabuscador *hackable* que busca la privacidad](https://hijosdeinit.gitlab.io/howto_searx_metabuscador/)
- ['DuckDuckGo' NO es de fiar](https://hijosdeinit.gitlab.io/googlizacion_de_duckduckgo_censurando_contenidos_adulterando_busquedas/)
- [[HowTo] 'QWant': otro buscador respetuoso con la privacidad](https://hijosdeinit.gitlab.io/howto_qwant_buscador_web_respetuoso_privacidad/)
- ['Altavista' y su era. Internet 1.0](https://hijosdeinit.gitlab.io/internet1.0_la_era_Altavista/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
