---
layout: post
title: "'RaspBian' / 'RaspBerry OS' incorpora furtivamente -sin aviso ni permiso- un repositorio de 'Microsoft'. Así no vamos bien"
date: 2021-02-05
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "raspbian", "raspberry os", "repositorio", "microsoft", "vs code", "visual studio code", "desconfianza", "mala praxis", "seguridad", "privacidad", "microsoft.gpg", "hosts", "apt", "raspberry-sys-mods", "vscode.list"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
El asunto salió a la luz hace pocos días, el pasado 29 de enero de 2021 concretamente. Y lo descubrió involuntariamente un usuario llamado [GarrickAndersLyng](https://www.raspberrypi.org/forums/memberlist.php?mode=viewprofile&u=364215&sid=dd13870a3254a59d2d96aa62646e0ad5) mientras solucionaba algunos incidentes que estaba experimentando con el paquete 'sudo'.  
Se percató, asombrado, de que su servidor se conectaba a un repositorio de Microsoft: `packages.microsoft.com`.  
<br>
<a href="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/garrickanderslyng_msrepodescubierto.png" target="_blank"><img src="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/garrickanderslyng_msrepodescubierto.png" alt="Garrick Anders descubre repositorio furtivo de Microsoft en RaspBerry OS" width="800"/></a>  
<small>**‼** GarrickAndersLyng descubre un repositorio de Microsoft que RaspBian y RaspBerry OS incorpora a tu sistema sin pedir permiso y sin previo aviso</small>  
<br>
<br>
En una actualización reciente -enero 2021- de Raspbian / RaspBerry Os se había instalado un repositorio apt de Microsoft sin el conocimiento de la persona que administraba el sistema, y esto no era un caso aislado sino que afectaba a TODOS los sistemas RaspBian / RaspBerry Os actualizados.  
<br>
De todas las repercusiones que esto tiene, la más "inofensiva" es que cada vez que un dispositivo que tuviera instalado (y al día) 'RaspBian' / 'RaspBerry Os' se actualice hará 'ping' a un servidor de Microsoft. Mas hay repercusiones más graves, que enunciaré más adelante.  
Entre otras cosas, la **telemetría** de Microsoft tiene mala -y merecida- reputación en la comunidad Linux.  
Por otro lado, se dice que los desarrolladores de la Fundación RaspBerry comunicaron la incorporación del repositorio de Microsoft en el "listado de cambios" (*changelog*) correspondiente a la actualización.  
<br>
<br>
Para empeorar aún más las cosas, los administradores del foro oficial de RaspBerry Pi rápidamente bloquearon y cerraron algunos hilos que hacían referencia al asunto; calificaron a esos hilos de 'Microsoft bashing'.  

<br>
No es la primera vez que "incidentes" parecidos han salpicado al software libre o al código abierto. Muchos recordarán las configuraciones por defecto de Mozilla Firefox, su vinculación -y favores- a Google, distribuciones de Linux con enlaces a Amazon, etcétera, etcétera...  
<br>
Dicen que el escepticismo -y la desconfianza- suele ser rasgo de hombres inteligentes y precavidos.  

<br>
<br>
<br>
## ¿Qué ES ese repositorio de Microsoft furtivamente incorporado a RaspBian / RaspBerry OS?
Los archivos '/etc/apt/sources.list.d/vscode.list' y '/etc/apt/trusted.gpg.d/microsoft.gpg', relacionados con el repositorio de Microsoft, son creados tras la actualización del sistema mediante un script de postinstalación procedente de un paquete llamado **'raspberrypi-sys-mods'**, versión 20210125, albergado en el repositorio de la Fundación RaspBerry.  
Ejecutando <span style="color:lime">`apt show raspberrypi-sys-mods`</span> se señala a [un repositorio de GitHub como *homepage* del paquete](https://github.com/RPi-Distro/raspberrypi-sys-mods). <small>Mas los cambios no fueron publicados hasta el día 3 de febrero de 2021, casi 2 semanas después de que el paquete fuera construído y horas después de que la comunidad comenzara a hablar acerca del incidente.</small>  
> Package: raspberrypi-sys-mods  
Version: 20210125  
Priority: optional  
Section: admin  
Maintainer: Serge Schneider <serge@raspberrypi.com>  
Installed-Size: 75.8 kB  
Depends: libcap2-bin, systemd (>= 230), gettext-base  
Recommends: rfkill, rpi-eeprom  
Homepage: https://github.com/RPi-Distro/raspberrypi-sys-mods  
Download-Size: 12.3 kB  
APT-Manual-Installed: yes  
APT-Sources: http://archive.raspberrypi.org/debian buster/main armhf Packages  
Description: System tweaks for the Raspberry Pi  
 Various modifications to improve the performance or user experience.  

<a href="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/sudoaptshowraspberrypisysmods.png" target="_blank"><img src="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/sudoaptshowraspberrypisysmods.png" alt="sudo apt show raspberrypi-sys-mods" width="800"/></a>  
<br>
<br>
Ejecutando <span style="color:lime">`lsb_release -a`</span> se muestran las características del sistema operativo que estoy analizando:
>No LSB modules are available.  
Distributor ID:	Raspbian  
Description:	Raspbian GNU/Linux 10 (buster)  
Release:	10  
Codename:	buster  

<br>
<a href="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/lsbreleasea.png" target="_blank"><img src="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/lsbreleasea.png" alt="Características del sistema analizado" width="800"/></a>  
<br>
<br>
Ejecutando <span style="color:lime">`ls -l /etc/apt/sources.list.d/`</span> se lista un archivo llamado `vscode.list` de fecha 2021-01-29, fecha en que supuestamente realicé la última actualización del sistema:
> -rw-r--r-- 1 root root 203 Jan 29 06:09 vscode.list  

<br>
<a href="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/lssourceslistd.png" target="_blank"><img src="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/lssourceslistd.png" alt="vscode.list" width="800"/></a>  
<br>
<br>
Ejecutando <span style="color:lime">`cat /etc/apt/sources.list.d/vscode.list`</span> se muestra lo que contiene:  
> deb [arch=amd64,arm64,armhf] http://packages.microsoft.com/repos/code stable main  

<br>
<a href="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/catvscodelist.png" target="_blank"><img src="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/catvscodelist.png" alt="Contenido de vscode.list" width="800"/></a>  
<br>
<br>
Ejecutando <span style="color:lime">`ls -l /etc/apt/trusted.gpg.d/`</span> se listan las claves 'gpg'. Ahí aparece la clave 'gpg' de Microsoft:
> -rw-r--r-- 1 root root  641 Jan 29 06:09 microsoft.gpg  

<br>
<a href="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/microsoftgpg.png" target="_blank"><img src="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/microsoftgpg.png" alt="Clave gpg de Microsoft" width="800"/></a>  
<br>
<br>
Ejecutando <span style="color:lime">`curl -s http://packages.microsoft.com/repos/code/dists/stable/main/binary-arm64/Packages | grep "^Package: " | cut -d" " -f2 | sort -u`</span> se puede analizar el contenido de ese repositorio de Microsoft que se incorporó furtivamente al sistema.
> code  
code-exploration  
code-insiders  

<br>
<a href="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/contenido_packages_repo_ms.png" target="_blank"><img src="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/contenido_packages_repo_ms.png" alt="Contenido del repositorio de Microsoft que se incorporó furtivamente al sistema" width="800"/></a>  

<br>
Contiene 'VS Code IDE' para RaspBerry Pi.  

<br>
<br>
En principio podría decirse que el motivo de la incorporación -furtiva- de ese repositorio es para, llegado el caso, facilitar la instalación y/o actividad de Microsoft 'Visual Studio Code', que dicen que es el IDE recomendado para 'RaspBerry Pico'.  
El repositorio se habría "añadido" al sistema en una de las actualizaciones.  
Cabe decir que el hecho de que este repositorio exista en el sistema 'RaspBian' o 'RaspBerry OS' no significa que, como consecuencia, se haya instalado automáticamente software.  
Asimismo es reseñable que RaspBerry Pi no es 100% Open Souce. Al igual que las CPU/GPU de Intel y AMD, viene con un firmware "cerrado". Aunque, claro está, esto no justifica que sea lícita la instalación secreta -sin conocimiento del administrador- de repositorios y claves gpg de software no deseado.  
Al fin y al cabo, ¿no es eso lo que hace el ***malware***?  
¿Se acuerdan ustedes de cuando [Sony distribuía cds de música con un *rootkit* que se instalaba en tu ordenador](https://en.wikipedia.org/wiki/Sony_BMG_copy_protection_rootkit_scandal)? Me disgusta pensar que vamos por ese campo semántico.  

<br>
<big>Muy mal asunto, evidentemente</big>. Resumiendo:
1. Nuevos repositorios no deben ser añadidos a un sistema sin permiso del propietario / administrador. Llegado el momento, éste será capaz de llevar a cabo el procedimiento de instalación de software según considere conveniente, sin necesidad de ese tipo de "ayudas".  

2. Jamás antes, Debian o Ubuntu, por poner un ejemplo, había modificado el archivo 'sources.list' sin consentimiento del administrador del sistema. <small>Barrunto que los desarrolladores de Debian -del cual procede 'RaspBian' y 'RaspBerryPi OS'- no verán con buenos ojos que su trabajo sea aprovechado por la Fundación RaspBerryPi para los menesteres que hoy nos ocupan.</small>

3. Es absurdo considerar que 'Microsoft Visual Studio Code' vaya a ser instalado en un servidor gnu linux que, entre otras cosas, carezca de escritorio / interfaz gráfico. Por consiguiente, ¿qué sentido tiene un repositorio que supuestamente pretende facilitar la instalación de un IDE que requiere de la existencia de un escritorio / interfaz gráfico en un servidor que carece de él?

4. Una cosa es que, en una actualización, el sistema cambie automáticamente un fichero de configuración existente (ya instalado y aceptado) y otra -bien diferente- es añadir furtivamente y sin conocimiento del administrador un repositorio desde una fuente totalmente diferente.

5. Microsoft Visual Studio Code incorpora **telemetría**, motivo por el cual incorporar repositorios que tengan relación con él a un sistema gnu linux sin permiso ni aviso es doblemente grave.

6. Incorporar secretamente, sin conocimiento del administrador, repositorios y claves gpg de **software no deseado** son prácticas propias del **malware**.  

7. Incorporar a la fuerza un repositorio de Microsoft en RaspBian / RaspBerry OS permite que Microsoft controle el software que se instala, dando lugar a que -por ejemplo- si el usuario instala un programa (<span style="color:lime">`sudo apt install `*programa*</span>) pudiera obtenerse una aplicación distribuída y modificada por Microsoft. Quizás Microsoft no desee hacer nada malo, mas ¿por qué obligar al administrador de un sistema Linux a confiar ciegamente en ello?.

8. Muchos usuarios de GNU Linux -por ejemplo, aquellos que trabajan en *infosec*/*IT*- **jamás confiarán** en Microsoft, y -ahora- tampoco en RaspBian / RaspBerry Pi OS.

9. Microsoft podría recoger más información -ip, por ejemplo- acerca de los usuarios de RaspBerry Pi y de Linux (muchos de los cuales intentan reducir su rastro digital) y construir perfiles de ellos: Cada `sudo apt-get update`, repito, hace un ping al repositorio de Microsoft. Si, desde la misma ip pública con la que RaspBerry Pi "sale" a internet, hay más máquinas logueadas en el ecosistema Microsoft -'GitHub', 'Bing', 'Office/Live'-, esta empresa puede identificarlas, asociarlas y rastrearlas.

10. Es de todos conocida la trayectoria de Microsoft, la intención que mueve su mera existencia, la posición que mantuvo respecto al software libre, así como la evolución de ésta hasta la actualidad. Desconfiar no es una opción descabellada, a juicio de muchos.  

11. De ser cierto que los desarrolladores de la 'Fundación RaspBerry Pi' comunicaron que iba a incorporarse un repositorio de Microsoft en el "listado de cambios" (*changelog*) correspondiente a la actualización, cabe señalar:
- Que los usuarios medios y avanzados -que son los que generalmente leen los listados de cambios aka *changelog*- generalmente NO necesitan que se les "ayude" a agregar repositorios, saben hacerlo por ellos mismos... y hasta compilar cosas cuando es necesario.
- Que los usuarios inexpertos no suelen leer los listados de cambios (*changelog*), así que el asunto había de pasar necesariamente inadvertido para ellos.  

12. Un simple diálogo durante la actualización preguntando si se desea incorporar el repositorio de Microsoft y el motivo hubiera bastado. Es muy habitual que durante la actualización de un sistema éste solicite la interacción del usuario / administrador para que tome decisiones: por ejemplo cuando se va a cambiar un archivo de configuración el sistema pregunta qué decisión desea tomarse (mantener, sobreescribir, etc...) como, por ejemplo, sucede cuando el software 'Open Media Vault' se actualiza, etcétera.  

<br>
La [**respuesta de la 'Fundación RaspBerry Pi'**](https://www.raspberrypi.org/forums/viewtopic.php?f=63&t=301011&start=25#p1810752), a través de uno de sus desarrolladores, fue agradecer el feedback y negarse a deshacer el entuerto alegando que agregar sistemáticamente y sin consentimiento el repositorio de Microsoft facilita la "primera experiencia" a gente que desea usar herramientas como 'VSCode':  
<br>
<a href="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/incidente_msrepo_raspbian_raspberryos_raspberryfoundationdev.png" target="_blank"><img src="/assets/img/2021-02-05-repositorio_microsoft_instalado_sin_permiso_sin_aviso_en_Raspbian_RaspBerryOs/incidente_msrepo_raspbian_raspberryos_raspberryfoundationdev.png" alt="La 'Fundación RaspBerry' se niega a dejar de agregar sistemáticamente y sin consentimiento el repositorio de Microsoft" width="800"/></a>  
<br>
Pareciera más bien que la 'Fundación RaspBerry Pi' lo que desea es **facilitarle** las cosas a Microsoft y su ecosistema, y NO tanto al usuario que, sobra decirlo, no tiene más que hacer algo tan sencillo como [esto para disponer de MS 'VSCode' sin telemetría](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/), cosa muy respetable aún habiendo [alternativas IDE iguales o mejores](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/).
<br>
<br>
<br>
## Soluciones y medidas
Ante este "incidente" el usuario puede decidir "no hacer nada" o bien **tomar alguna de las siguientes medidas**:
- Dejar de usar 'RaspBian' o 'RaspBerry OS'. Alternativas: [Arch Linux arm](https://archlinuxarm.org/), [Ubuntu for RPi](https://ubuntu.com/download/raspberry-pi), [OpenSUSE for PPi](https://en.opensuse.org/HCL:Raspberry_Pi), [FreeBSD for RPi](https://wiki.freebsd.org/action/show/arm/Raspberry%20Pi?action=show&redirect=FreeBSD%2Farm%2FRaspberry+Pi), [OpenBSD for RPi](https://www.openbsd.org/arm64.html), [NetBSD for RPi](https://wiki.netbsd.org/ports/evbarm/raspberry_pi/).
- <big>[Bloquear "los cambios" que sin permiso se hicieron](https://hijosdeinit.gitlab.io/howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/)</big> y continuar utilizando 'RaspBian' / 'RaspBerry OS'.
- Ídem y además [utilizar 'VS Code' de forma segura, **sin telemetría**](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/): 'VSCodium'.  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] '/etc/apt/sources.list': repositorios en Debian y derivados](https://hijosdeinit.gitlab.io/howto_repositorios_sources.list_debian_y_derivados/)
- [[HowTo] Agregar repositorio 'debian unstable' a Debian 10 y similares](https://hijosdeinit.gitlab.io/howto_agregar_repositorio_debian_unstable_debian_y_derivados/)
- [[HowTo] Instalación de una versión más actualizada de un paquete desde los repositorios oficiales de Debian: 'backports'](https://hijosdeinit.gitlab.io/howto_instalacion_version_mas_actualizada_desde_repositorios_Debian_backports/)
- [[HowTo] Instalar el comando 'add-apt-repository' en Debian](https://hijosdeinit.gitlab.io/howto_add-apt-repository_en_debian_y_derivados/)
- [[HowTo] Eliminar repositorio agregado a mano ('add-apt-repository') en Debian y derivados](https://hijosdeinit.gitlab.io/howto_eliminar_repositorios_agregados_manualmente_addaptrepository_Debian_derivados/)
- [[HowTo] Solucionar error de GPG cuando la clave pública de un repositorio no está disponible. (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_solucionar_error_gpg_clave_publica_repositorio_Debian_y_derivados/)
- [[HowTo] Eliminar Clave 'Gpg' Cuyo 'Id' Desconocemos. (Debian Y Derivados)](https://hijosdeinit.gitlab.io/howto_eliminar_clave_gpg_id_desconocida_agregada_mediante_apt-key_add_/)
- [El repositorio de firmware / drivers para GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- [[HowTo] Extirpar todo rastro del repositorio de MicroSoft que Raspbian / RaspBerry Os instala furtivamente](https://hijosdeinit.gitlab.io/howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.raspberrypi.org/forums/viewtopic.php?f=63&t=301011&sid=0fa1cddfde69bc0f9813f451e30378d7">GarrickAndersLyng en raspberrypi.org/forums</a>  
<a href="https://www.raspberrypi.org/forums/viewtopic.php?f=63&t=301068">best400 en raspberrypi.org/forums</a>  
<a href="https://www.reddit.com/r/linux/comments/lbu0t1/microsoft_repo_installed_on_all_raspberry_pis/?sort=old">fortysix_n_2 en Reddit</a>  
<a href="https://www.reddit.com/r/raspberry_pi/comments/lciynh/heads_up_microsoft_repo_secretly_installed_on_all/">DDzwiedziu en Reddit</a>  
<a href="https://www.cyberciti.biz/linux-news/heads-up-microsoft-repo-secretly-installed-on-all-raspberry-pis-linux-os/">nixCraft</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
