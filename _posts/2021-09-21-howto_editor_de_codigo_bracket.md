---
layout: post
title: "[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados"
date: 2021-09-21
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "editor de codigo", "codigo", "editor", "atom", "visual studio code", "sublime text", "nano", "micro", "ide", "programación", "productividad", "brackets"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Andaba yo buscando un editor de código que fuera más ligero que 'Atom', apto para GNU Linux de 32 bits y que me permitiera ver cómo se muestra el texto según el tipo de fuente elegida cuando di con un par de alternativas a los famosos 'Visual Studio Code', 'Sublime Text' (o el ya mencionado 'Atom'). Eran los editores ['Light Table'](http://lighttable.com/) y 'Brackets'.  
'Light Table' sólo funciona en arquitecturas de 64 bits y quedó descartado. Probé, pues, 'Brackets'.</small>  

<br>
## 'Brackets'
['Brackets'](https://brackets.io/index.html) es un *fork* de un [editor de código que, con el mismo nombre y bajo licencia MIT, mantiene la empresa 'Adobe'](https://github.com/adobe/brackets).  
'Brackets' es un «moderno editor de código **enfocado sobre todo a HTML, CSS y JavaScript**». Es de código abierto y multiplataforma (tiene versiones para GNU Linux, Windows y MacOS).  
Desde su [repositorio de GitHub](https://github.com/brackets-cont/brackets) pueden descargarse las diferentes versiones y su código fuente.  
<br>
Asimismo este editor tiene varias <big>**extensiones**</big>, que están listadas en el [apartado correspondiente de la página oficial](https://registry.brackets.io/) y [aquí](https://ingo-richter.io/BracketsExtensionTweetBot/), y [explicadas en su *wiki*](https://github.com/adobe/brackets/wiki/Brackets-Extensions).  
Las extensiones del *fork* 'Brackets' son **compatibles** con las del proyecto original del que parte. Algunas de las extensiones son:
- [Emmet](https://github.com/emmetio/brackets-emmet): «High-speed HTML and CSS workflow».
- [Beautify](https://github.com/drewhamlett/brackets-beautify): «Format JavaScript, HTML, and CSS files».
- [File Icons](https://github.com/ivogabe/Brackets-Icons): «File icons in Brackets’ file tree».
- [Indent Guides](https://github.com/lkcampbell/brackets-indent-guides): «Show indent guides in the code editor».
- [Git](https://github.com/zaggino/brackets-git): «Git integration for Brackets».
- [Autoprefixer](https://github.com/mikaeljorhult/brackets-autoprefixer): «Parse CSS and add vendor prefixes automatically».
- [W3C Validation](https://github.com/cfjedimaster/brackets-w3cvalidation): «Simple W3C Validator».  

<br>
## Instalación de 'Brackets' en Debian y derivados
<small>Entorno (<small>donde he realizado el procedimiento</small>):   
Ubuntu Mate 32bits  
Netbook Atom, 2gb RAM</small>  
<br>
'Brackets' funciona en máquinas con procesadores de 32 bits. Existen 3 formas de instalarlo:
- a) [Compilando el código fuente](https://github.com/brackets-cont/brackets/releases)
- b) A través del binario o instalador correspondiente a nuestra arquitectura y sistema operativo: <small>No he sido capaz de encontrarlo en el repositorio oficial></small>.
- c) A través del [repositorio oficial de 'Brackets' para nuestra distribución GNU Linux.  

### c. Instalación de 'Brackets' mediante su repositorio oficial para Debian y derivados
~~~bash
sudo add-apt-repository ppa:webupd8team/brackets
sudo apt-get update
sudo apt install brackets
~~~

<br>
## Ejecución y funcionamiento del editor de código 'Brackets'
En la página oficial existe [un manual de usuario](https://github.com/adobe/brackets/wiki/How-to-Use-Brackets).  
<br>
Ejecútese 'Brackets' de la forma en que ejecutamos habitualmente los programas en nuestra distribución GNU Linux: desde menú, mediante lanzador, ejecutando el comando `brackets` desde la línea de comandos, etc  

<br>
<a href="/assets/img/2021-09-21-howto_editor_de_codigo_bracket/brackets-ui.png" target="_blank"><img src="/assets/img/2021-09-21-howto_editor_de_codigo_bracket/brackets-ui.png" alt="el interfaz del IDE 'Brackets'" width="800"/></a>  
<small>El interfaz del IDE 'Brackets'</small>  
<br>

## ¡Rechaza la telemetría!
En la primera ejecución aparecerá una advertencia que señala que la **telemetría** está activada por defecto, y que ésta puede desactivarse desde las Opciones. Es muy recomendable desactivarla ['Ayuda' → 'Reporte de estadísticas'] para no enviar ningún dato -por inofensivo y anónimo que digan que sea- a los responsables de 'Brackets'.  
Lamentablemente, al igual que ocurre con otros editores de código como 'Atom', 'Visual Studio Code', etc..., o de audio como 'Audacity', la telemetría "por defecto" viene siendo algo habitual, <small>motivada quizás por un afán de obtener réditos. Claro está que uno es libre de utilizar o no un software -de dejarse usar o no-, mas es evidente que toda esa moda es típica del mundo posmoderno en el que vivimos, el mundo de la internet 4.0 donde la mendicidad y el maltrato vía invasión de la privacidad se está viendo como algo "normal", incluso en el *open source*. Y no lo es.</small>  

<br>
<a href="/assets/img/2021-09-21-howto_editor_de_codigo_bracket/brackets_telemetria.png" target="_blank"><img src="/assets/img/2021-09-21-howto_editor_de_codigo_bracket/brackets_telemetria.png" alt="telemetría activada por defecto, ¡hay que desactivarla!" width="800"/></a>  
<small>telemetría activada por defecto, ¡hay que desactivarla! (desde los ajustes)</small>  
<br>

<br>
<br>
Entradas relacionadas:  
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)
- [[HowTo] El editor (IDE) atom y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [[HowTo] Evitar que 'atom' elimine espacios en blanco al final de línea](https://hijosdeinit.gitlab.io/howto_atom_espacios_blanco_final_linea/)
- [[HowTo] Agregar idiomas al editor (IDE) atom. Ponerlo en español](https://hijosdeinit.gitlab.io/howto_poner_atom_en_espa%C3%B1ol/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- [[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados](https://hijosdeinit.gitlab.io/howto_micro_editor_texto_debian/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [[HowTo] Añadir resaltado de sintaxis al editor de textos NANO](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [[HowTo] nano: parámetros de arranque útiles](https://hijosdeinit.gitlab.io/howto_parametros_utiles_arranque_nano/)
- [En nano no existe overtyping](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/)
- [[HowTo] Comienzo con vim. I](https://hijosdeinit.gitlab.io/howto_comienzo_con_vim_1/)
- [[HowTo] Visualización de 'markdown' en la línea de comandos (CLI): 'MDLESS'](https://hijosdeinit.gitlab.io/howto_mdless_visor_markdown_cli/)
- [[HowTo] Comienzo con vim. I](https://hijosdeinit.gitlab.io/howto_comienzo_con_vim_1/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.linuxadictos.com/instalar-brackets-debianubuntu-derivadas.html">LinuxAdictos</a>  
<a href="https://beatrizruizcorvillo.es/brackets-editor-html/">BeatrizRuizCorvillo - 'Brackets'</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
