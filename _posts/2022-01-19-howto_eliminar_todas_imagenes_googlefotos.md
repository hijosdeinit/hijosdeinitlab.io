---
layout: post
title: "[HowTo] Purgado en masa de todas las fotografías en 'google photos'"
date: 2022-01-19
author: Termita
category: "internet 4.0"
tags: ["google photos", "google", "degoogled", "degooglizar", "libertad", "autosuficiencia", "respeto", "privacidad", "independencia", "fotografias", "imagenes", "fotos", "nube", "google one", "google drive", "almacenamiento", "sincronizacion", "android", "navegador web", "web", "gratis", "servicios", "Ivoox", "Alphabet", "Google", "Facebook", "mendicidad 4.0", "patreon", "mercaderes", "mercantil", "suscripción", "incerteza", "dependencia", "web 4.0"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Echándole un cable al prójimo, ayudándole a *desgooglizarse* -liberarse del ecosistema 'Google'-, una persona me comentó que el interfaz de 'Google Photos' NO facilitaba **eliminar todo** el contenido "de una tacada", y que marcar miles de fotografías una por una era algo -evidentemente- tedioso y costoso en tiempo, bien preciado por lo escaso.  
No lo ponen facil esos miserables: ninguna opción, ningún botón... nada.  
Para subir "a saco" las fotografías del móvil, sin embargo, sí existen todas las facilidades, mas ¡ay!, pareciera que a 'Google' no le hace gracia el proceso inverso.  
<br>
No obstante, nada hay imposible para un español con cinco dedos en ambas manos.  

<br>
### Procedimiento para borrar a granel todas las imagenes que se tienen en 'Google Fotos'
Desde el navegador web de un ordenador, primero hay que marcar la primera fotografía haciendo *click* en su esquina superior izquierda.  

<br>
<a href="/assets/img/2022-01-19-howto_eliminar_todas_imagenes_googlefotos/purga_googlephotos_inicioseleccion.png" target="_blank"><img src="/assets/img/2022-01-19-howto_eliminar_todas_imagenes_googlefotos/purga_googlephotos_inicioseleccion.png" alt="purgando 'Google Photos': inicio de la selección" width="500"/></a>  
<small>purgando 'Google Photos': inicio de la selección</small>  
<br>

**i** Si se tienen muchas imagenes lo mejor es hacer *zoom out* en el navegador. De esta forma cabrán el máximo de imagenes en pantalla.  
La mayoría de navegadores web hacen *zoom out* mediante la combinación de teclas <span style="background-color:#042206"><span style="color:lime">'ctrl'</span></span> + <span style="background-color:#042206"><span style="color:lime">'-'</span></span>.  
<br>
Pulsando repetidamente la tecla <span style="background-color:#042206"><span style="color:lime">'Av.Pág'</span></span> desplácese hasta la **última fotografía**.
<br>
Púlsese la tecla <span style="background-color:#042206"><span style="color:lime">'shift'</span></span> (<small>también llamada 'mayúsculas'</small>) y, SIN SOLTARLA, hágase <span style="background-color:#042206"><span style="color:lime">'click'</span></span> sobre esa última fotografía.  
Quedaran **todas** las fotografías seleccionadas.  
Ya pueden descansar esos dedos.  
<br>
<a href="/assets/img/2022-01-19-howto_eliminar_todas_imagenes_googlefotos/purga_googlephotos_finseleccion.png" target="_blank"><img src="/assets/img/2022-01-19-howto_eliminar_todas_imagenes_googlefotos/purga_googlephotos_finseleccion.png" alt="purgando 'Google Photos': fin de la selección" width="500"/></a>  
<small>purgando 'Google Photos': fin de la selección</small>  
<br>

A continuación, lo más importante: púlsese el icono de 'borrar' -una papelera- en la esquina superior derecha, y bórrense las fotos.  
<br>
Diríjase a la 'papelera'. Púlsese 'vaciar papelera', en la esquina superior derecha.  

<br>
<br>
A tener en cuenta:  

<br>
<br>
Los discos duros y SSD son muy baratos hoy. Nadie garantiza que la "empresa" -llámese 'Google', llámese como sea- realmente lleve a cabo un borrado total de los archivos, sin guardarse la información para sí.  

<br>
<br>
Los teléfonos móviles suelen tener activada por defecto la **sincronización de las fotos con 'la nube'** (con 'google photos', por ejemplo). Para evitar que las fotografías vuelvan a replicarse en ésta -es decir, en servicios que no son de uno, sino que son de empresas cuyo interés no es otro que ganar dinero a costa de la privacidad de los "usados" (que no usuarios)-, es indispensable **desactivar** eso o no.  
<br>
A su discreción queda, porque libertad hay hasta para volarse los sesos, ¿verdad?.  

<br>
<br>
<br>
Entradas relacionadas:  
- ["Gratis"](https://hijosdeinit.gitlab.io/lo_gratis/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
