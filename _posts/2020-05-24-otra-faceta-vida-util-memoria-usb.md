---
layout: post
title: 'Otra faceta de la vida útil de las memorias usb'
date: 2020-05-24
author: Termita
category: "almacenamiento"
tags: ["pendrive", "elementos", "intemperie", "memoria usb", "usb", "cdrom", "luz", "humedad", "backup", "respaldo", "almacenamiento", "problemas", "humedad", "óxido", "disk rot", "obsolescencia", "rotura"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Las celdas que componen las memorias flash, los soportes de almacenamiento usb, etc... tienen un número limitado de ciclos de escritura. Esto es palpable rápidamente cuanto se ejecuta un sistema operativo en uno de estos dispositivos -una tarjeta sd, por ejemplo- y éste comienza rápidamente a deteriorarse hasta que pronto deja de funcionar por agotarse su capacidad de ser escrito. Los propietarios de las microcomputadoras Raspberry Pi saben esto bien.

Mas existe otro enemigo de los dispositivos de almacenamiento flash como son los pendrives usb: la oxidación y el calentamiento por mala disipación.
No dejan de ser chismes que tienen muchas partes metálicas, soldaduras, etc. y que son transportados de aquí para allá sufriendo, incluso, los efectos de los elementos.  
<br>
![Deterioro físico de una memoria usb]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-05-24-otra-faceta-vida-util-memoria-usb/deteriorofisicopendrive.png)  
<small>Deterioro material de una memoria usb: oxidación y efectos del sobrecalentamiento por mala disipación</small>  
<br>
El diseño puede ser deficiente, además, con lo que el problema se agrava: mala estanqueidad favorece la oxidación, mala disipación potencia los efectos del calor.  
<br>
Es posible que no sea un soporte tan fiable para almacenar información como nos pensábamos. [La mala fama la tenían los cds y dvds grabables](https://hijosdeinit.gitlab.io/saque-todo-lo-que-pueda-de-los-soportes/) :)
