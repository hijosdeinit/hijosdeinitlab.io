---
layout: post
title: "'vi' es prioritario"
date: 2022-06-14
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "vi", "vim", "vino", "vinagre", "visudo", "neovim", "spacevim", "lunarvim", "elvis", "lemmy", "cream", "kakoune", "levee", "nvi", "emacs", "micro", "ed", "editor", "editor de textos", "texto", "texto plano", "latex", "pandoc"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Mientras escribo esto soy consciente de mi aún completa ignorancia acerca de 'vi' y derivados y que, cuanto más experimento con éstos, más soy consciente dello. Admito que mis progresos los estaba realizando hasta ayer utilizando 'vim' (*'vi improved'* ('vi' mejorado)).  
Sin embargo:
- A diferencia de 'vim' y similares, que no siempre están instalados por defecto en la mayoría de distribuciones GNU Linux, 'vi' casi siempre -creo que Ubuntu, una de las excepciones, trae por defecto 'vim-tiny'- 'vi' está presente en esos sistemas y, juraría que también, en "emuladores" como ['cygwin']().
- Las nociones de 'vi' sirven también para 'vim', sobre todo si en este último se activa el 'modo de compatibilidad' [<small>`:set compatible`</small>]
- Por otro lado, aplicaciones como 'cadaver' (cliente webdav para la terminal) hacen uso de 'vi' para la edición de archivos.  

<br>
Por todo esto, considero que me resulta más conveniente y prioritario **CENTRARME EN 'VI'** y sus peculiaridades:  
Por ejemplo, en 'vi' los cambios en el texto realizados en modo inserción se muestran de una manera muy particular -son, por decirlo de alguna manera, plenamente visibles-, ô las teclas correspondientes a las "flechas" de dirección -esas teclas a la que los nobeles aún recurrimos tanto- no funcionan exactamente como se esperaría en el modo inserción, sino que imprimen en pantalla los caracteres 'A', 'B', 'C' o 'D'. Cuando un novato como yo, tras haberse "acostumbrado" a 'vim' se percata de esto, cosa que ocurre cuasi inmediatamente, adviene la sorpresa y el desconcierto. De evitar cosas así se trata, a mi entender.  

<br>
<br>
Entradas relacionadas:  
- [Comienzo Con 'Vim', II: Descendientes / Variantes De 'Vi'](https://hijosdeinit.gitlab.io/howto_comienzo_con_vim_2_variantes_de_vim/)
- [[HowTo] Comienzo con vim. I](https://hijosdeinit.gitlab.io/howto_comienzo_con_vim_1/)
- [Juegos para aprender 'vi': 'vimadventures', 'vimtutor', 'openvim tutorial'](https://hijosdeinit.gitlab.io/vimadventures-vimtutor_juegos_para_aprender_vi/)  


