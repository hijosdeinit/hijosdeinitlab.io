---
layout: post
title: "El repositorio de firmware / drivers para GNU Linux"
date: 2021-05-25
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "ubuntu", "realtek", "controladores privativos", "non-free", "rtl8822ce", "c8822", "10ec:c822", "rtlwifi", "firmware", "drivers", "controladores", "hardware", "repositorio"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Existe un <big>**[repositorio de firmware / drivers para GNU Linux](https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git)**</big> desde el que se puede obtener en un fichero comprimido '.tar.gz' la última versión de la colección (de firmwares / drivers para GNU Linux).  

<br>
Un ejemplo de su Utilidad y formas de empleo:  

<br>
«The drivers for the Realtek 8822CE are not currently available in the firmware-realtek package

The easiest fix for this is to grab the latest tarball of the linux-firmware repo at https://git.kernel.org/pub/scm/linux/kernel/git/firmware/linux-firmware.git and put it on a usb stick, then when the **installer** reaches the network setup, enter a shell (via go back -> shell) and run the following:  

<span style="background-color:#042206"><span style="color:lime">`mount /dev/sdb1 /mnt`</span></span> # replace sdb1 with your usb (run blkid to check)  
<span style="background-color:#042206"><span style="color:lime">`lsmod | grep rtw88`</span></span> # identify the modules relating to the rtw88 driver that we'll need to unload  
<span style="background-color:#042206"><span style="color:lime">`modprobe -r rtwpci rtw88`</span></span> # unload the modules  
<span style="background-color:#042206"><span style="color:lime">`cp /mnt/linux-firmware/rtw88/* /lib/firmware/rt88/`</span></span> # install the required drivers  
<span style="background-color:#042206"><span style="color:lime">`modprobe rtw88`</span></span> # load the new drivers  
<span style="background-color:#042206"><span style="color:lime">`exit`</span></span>  

The network setup step should now succeed and prompt you to connect to a network.»  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] '/etc/apt/sources.list': repositorios en Debian y derivados](https://hijosdeinit.gitlab.io/howto_repositorios_sources.list_debian_y_derivados/)
- [[HowTo] Agregar repositorio 'debian unstable' a Debian 10 y similares](https://hijosdeinit.gitlab.io/howto_agregar_repositorio_debian_unstable_debian_y_derivados/)
- [[HowTo] Instalación de una versión más actualizada de un paquete desde los repositorios oficiales de Debian: 'backports'](https://hijosdeinit.gitlab.io/howto_instalacion_version_mas_actualizada_desde_repositorios_Debian_backports/)
- [[HowTo] Instalar el comando 'add-apt-repository' en Debian](https://hijosdeinit.gitlab.io/howto_add-apt-repository_en_debian_y_derivados/)
- [[HowTo] Eliminar repositorio agregado a mano ('add-apt-repository') en Debian y derivados](https://hijosdeinit.gitlab.io/howto_eliminar_repositorios_agregados_manualmente_addaptrepository_Debian_derivados/)
- [[HowTo] Solucionar error de GPG cuando la clave pública de un repositorio no está disponible. (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_solucionar_error_gpg_clave_publica_repositorio_Debian_y_derivados/)
- [[HowTo] Eliminar Clave 'Gpg' Cuyo 'Id' Desconocemos. (Debian Y Derivados)](https://hijosdeinit.gitlab.io/howto_eliminar_clave_gpg_id_desconocida_agregada_mediante_apt-key_add_/)
- ['RaspBian' / 'RaspBerry OS' incorpora furtivamente -sin aviso ni permiso- un repositorio de 'Microsoft'. Así no vamos bien](https://hijosdeinit.gitlab.io/repositorio_microsoft_instalado_furtivamente_en_Raspbian_RaspBerryOs/)
- [[HowTo] Extirpar todo rastro del repositorio de MicroSoft que Raspbian / RaspBerry Os instala furtivamente](https://hijosdeinit.gitlab.io/howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://wiki.debian.org/InstallingDebianOn/Lenovo/Yoga%20S730/buster">wiki.debian.org</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

