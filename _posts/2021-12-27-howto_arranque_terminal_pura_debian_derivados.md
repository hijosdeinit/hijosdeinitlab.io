---
layout: post
title: "[HowTo] Seleccionar el tipo de arranque en Debian y derivados: alternando entre arranque en entorno gráfico y arranque en terminal pura (tty)"
date: 2021-12-27
author: Termita
category: "sistemas operativos"
tags: ["getty", "get tty", "tty", "sistemas operativos", "CLI", "linea de comandos", "linux", "GNU linux", "systemctl", "graphical", "multi-user", "terminal", "gnome-terminal", "termux", "screen"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Es muy habitual que nuestro sistema operativo GNU Linux arranque por defecto en un entorno gráfico.  
Cuando hablamos de la línea de comandos a veces se confunde una ***terminal getty*** (tty1, por ejemplo) con un ***emulador de terminal*** (gnome-terminal, por ejemplo).  
De la terminal pura -*getty*- se trató en [esta entrada de este blog](https://hijosdeinit.gitlab.io/getty/).  

<br>
<br>
Ejecutando
~~~bash
sudo systemctl get-default
~~~
... si el sistema está configurado para arrancar automáticamente en entorno gráfico responderá:
> graphical.target

<br>
... y si, en cambio, el sistema estuviera configurado para arrancar automáticamente en línea de comandos "pura" (tty) respondería:
> multi-user.target

<br>
Si deseamos que **arranque en "modo línea de comandos"**, es decir, que al arrancar no cargue el entorno gráfico y directamente muestre una "terminal pura" (<big>**tty**</big>):
~~~bash
sudo systemctl enable multi-user.target --force
sudo systemctl set-default multi-user.target
~~~

Si, por el contrario, deseamos que **arranque en entorno gráfico** (ventanitas, escritorio y todas esas cosas):
~~~bash
sudo systemctl set-default graphical.target
~~~

<br>
<br>
Entradas relacionadas:
- ['terminal getty' vs. 'emulador de terminal'](https://hijosdeinit.gitlab.io/getty/)
- []()

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.linuxuprising.com/2020/01/how-to-boot-to-console-text-mode-in.html" target="_blank">LinuxUprising</a></small>  

<br>
<br>
<br>
<br>
