---
layout: post
title: "[HowTo] Identificar los diferentes modelos de Nintendo Wii"
date: 2021-02-21
author: Termita
category: "hardware"
tags: ["hardware", "consolas", "videojuegos", "juegos", "nintendo", "wii", "modelos"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
EN CONSTRUCCIÓN  



# Wii RVL-001
<a href="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/nintendo_wii_blanca.jpg" target="_blank"><img src="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/nintendo_wii_blanca.jpg" alt="Wii RVL-001" width="200"/></a>  
<small>Wii RVL-001</small>  
<br>
Son los modelos "primigenios". El primero se lanzó en noviembre de 2006 y comenzó a venderse en España a partir del 8 de diciembre de 2006.  
Las primeras consolas son todas de color blanco, pero a partir de agosto -Japón- y noviembre -Europa- de 2009 aparecieron los primeros modelos de color negro y, posteriormente, se incorporó a la gama el color rojo.  
Disponen de conexiones para mandos de Game Cube.  
No dispone de ningún tipo de conexión alta definición -se queda en 576i- y se conecta a la televisión a través de RCA o componentes.  
Los caracteres iniciales del **número de serie** varían en función de la región a que pertenezca la consola:
- Europa: LEH.
- América (USA Canadá y México): LU.
- Sur del Pacifico: LAH.  

<br>
<a href="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/wii_numero_de_serie.jpg" target="_blank"><img src="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/wii_numero_de_serie.jpg" alt="número de serie de consola Wii" width="300"/></a>  
<small>número de serie de consola Wii</small>  

<br>
## Especificaciones técnicas
· **CPU**: Procesador IBM PowerPC, nombre en clave «Broadway» (*IBM Broadway*) (con tecnología *SOI CMOS* de 90 nm). 729 MHz  

· **GPU**: ATI «Hollywood» manufacturado con tecnología *SOI CMOS* de 90 nm. 243 MHz  

· **RAM**: 24 MB 1T-SRAM, 3 MB EDRAM  

· **VRAM**: Bus GDDR3. 64 MB  

· **Medio**: Wii Optical Disc (12 cm), GameCube Optical Disc (8 cm)  

· **Almacenamiento**: Memoria interna flash, almacenamiento expandible vía tarjetas SD y SDHC, tarjeta de memoria de GameCube (para salvar partidas de juegos de esa consola), Mask ROM por Macronix. 512 MB / 32 GB, respectivamente.  

· **Controladores**:
>- Físicos: USB 2.0
- Inalámbricos: Bluetooth, Wi-Fi  

· **Puertos I/O**: USB 2.0 (2), ranura para tarjetas SD (1; soporta tarjetas SDHC a partir del Menú 4,0), barra sensor (1), capacidad hasta para 16 controladores Wii Remote (10 en modo estándar, 6 en un modo de tiempo) conectados de forma inalámbrica por Bluetooth, puertos para mandos de GameCube (4), puertos para expansión de memoria GameCube (2), barra de sensores (1), puerto de accesorios en la parte trasera del Wii Remote (1), entrada para teclado USB opcional en el Message Board del Canal Wii Shop, y el Canal Internet (a partir de la actualización 3,1), módulo inalámbrico Mitsumi DWM-W004 WiFi 802.11b/g, compatibilidad con USB 2,0 para adaptador LAN Ethernet, puerto «multisalida AV».  

· **Comunicación en red**: Tecnología WiFi (Red inalámbrica) incorporada, la cual permite una conexión a internet IEEE802.11 b/g), Compatible con adaptador de USB 2.0 a Ethernet LAN.  

· **Audio**: Compatible con Dolby Pro Logic II, y altavoz interno en el controlador.  

· **Resolución de vídeo**: 480p (PAL/NTSC), 480i (PAL/NTSC) y 576i (PAL/SECAM), pantalla panorámica anamórfica 4:3 y 16:9  

· **Formatos compatibles**: GameCube Optical Disc (GOD) de 8 cm de diámetro  

· **Salida audio-vídeo**:	Salida A/V: Componente (YPBPR; solo para consolas NTSC y PAL), vídeo compuesto, soporte para formato panorámico 16:9  

· **Dimensiones y peso**: Aproximadamente 157 mm (altura) x 215'4 mm (profundidad) x 44 mm (ancho) y pesa aproximadamente 1'2 Kg.  

· **Consumo de energía**:	18 W cuando está encendida, 9,6 W en modo de espera durante la conexión WiiConnect24 y 1,3 W en modo de espera.  

<br>
## Submodelos
Por orden de aparición:  

### D2A, DMS o D2B
Apenas existen diferencias entre ellos.  

<a href="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/d2a,dms,d2b.png" target="_blank"><img src="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/d2a,dms,d2b.png" alt="D2A, DMS y D2B" width="500"/></a>  
<small>chips de la controladora del lector; de izqierda a derecha: D2A, DMS y D2B</small>  

Las consolas Nintendo Wii que incorporan un chip D2A, DMS o D2B tienen un número de serie **anterior a**:
- Europa: LEH1361
- América: LU5297, LU3188, LU1050
- Sur del Pacífico: LAH1019  

Por otra parte, los chips D2B frecuentemente llevan algunas **patillas cortadas**, hecho que dificultaba el montaje de mecanismos electrónicos que permitiesen cargar *backups* (los famosos *chips*).

<br>
### D2C
<a href="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/d2c.png" target="_blank"><img src="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/d2c.png" alt="chip del lector D2C" width="300"/></a>  
<small>chip D2C de la controladora del lector</small>  

Las consolas Nintendo Wii que incorporan un chip D2C tienen un número de serie **mayor** que:
- Europa: LEH1361
- América: LU5297, LU3188, LU1050
- Sur del Pacífico: LAH1019  

... y **menor** que:
- Europa: LEH1837 / LEF  

<br>
### D2C2
<a href="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/d2c2.png" target="_blank"><img src="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/d2c2.png" alt="chip del lector D2C2" width="300"/></a>  
<small>chip D2C2 de la controladora del lector</small>  

El chip D2C2 suele hallarse en aquellas Wii cuyo número de serie sea **superior** a LEH1837xxxxx y **menor** a LEH200xxxxx y esté comprada a partir de mayo del 2008.  

<br>
### D2E
<a href="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/d2e.png" target="_blank"><img src="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/d2e.png" alt="chip del lector D2E" width="300"/></a>  
<small>chip D2E de la controladora del lector</small>  

El chip D2E se encuentra en aquellas consolas cuyo número de serie es **mayor** a LEH200xxxxxx.  
A partir de la introducción del chip D2E y sucesivos, comenzó a encontrarse resina epoxy adherida que hacía dificultosa la modificación para cargar *backups*.

<br>
### D3, D3-2
<a href="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/D3-2.jpg" target="_blank"><img src="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/D3-2.jpg" alt="chip del lector D3-2" width="300"/></a>  
<small>chip D3-2 de la controladora del lector</small>  

<br>
### D4
<a href="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/d4.jpg" target="_blank"><img src="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/d4.jpg" alt="chip D4 de la controladora del lector" width="300"/></a>  
<small>chip D4 de la controladora del lector</small>  

<br>
<br>
<br>
# Wii RVL-101 [Family Edition]
<a href="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/wii_family_edition.jpg" target="_blank"><img src="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/wii_family_edition.jpg" alt="Wii RVL-101 *family edition*" width="200"/></a>  
<small>Wii RVL-101 *family edition*</small>  
<br>
Apareció el 10 de octubre de 2011  
Podían ser de colores diferentes al blanco: negro o azul, por ejemplo.  
NO disponen de conexiones para mandos de Game Cube, así que pierden la retrocompatibilidad con esa plataforma.  

<br>
<br>
<br>
# Wii RVL-201 [Mini]
<a href="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/wii_mini.jpg" target="_blank"><img src="/assets/img/2021-02-21-howto_identificar_diferentes_modelos_nintendo_wii/wii_mini.jpg" alt="Wii RVL-201 *Mini*" width="200"/></a>  
<small>Wii RVL-201 *Mini*</small>  
<br>
Es un rediseño más pequeño del Wii con una unidad de disco de carga superior y una combinación de los colores negro y rojo.  
Esta pequeña consola fue anunciada el 27 de noviembre de 2012 y comenzó a venderse -99,99$- a partir de diciembre de 2012.  
No es compatible con los juegos de 'Game Cube'.
Su hardware, no obstante, es casi igual al de la Wii convencional. Continúa olvidando las posibilidades multimedia oficiales y de red que un dispositivo así podría tener. Su definición sigue siendo 576i, como los anteriores modelos.  
Sólo dispone de 1 puerto USB 2.0  
Carece de lector de tarjetas SD.  
Carece de conectividad online.  

<br>
---
<br>
<br>
# Otras formas y herramientas de identificación





<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/Wii">wikipedia - wii</a>  
<a href="@@@@@@@@@@@@">@@@@@@@@@@@@</a>  
<a href="@@@@@@@@@@@@">@@@@@@@@@@@@</a>  
<a href="@@@@@@@@@@@@">@@@@@@@@@@@@</a>  
<a href="@@@@@@@@@@@@">@@@@@@@@@@@@</a>  
<a href="@@@@@@@@@@@@">@@@@@@@@@@@@</a>  
<a href="@@@@@@@@@@@@">@@@@@@@@@@@@</a>  
<a href="@@@@@@@@@@@@">@@@@@@@@@@@@</a>  
<a href="https://wii.scenebeta.com/tutorial/como-saber-cual-es-la-placa-de-mi-consola-wii">wii.scenebeta.com</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
