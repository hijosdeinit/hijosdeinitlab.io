---
layout: post
title: "Qué pedirle a un 'pendrive' USB"
date: 2021-01-01
author: Termita
category: "hardware"
tags: ["hardware", "pendrive", "almacenamiento", "dispositivo de almacenamiento usb", "usb", "flash", "flash memory", "memoria flash", "nand", "celdas", "escritura", "lectura", "obsolescencia", "diseño", "error", "fallo", "hdd", "ssd", "disco", "diskette", "floppy"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Da igual el precio o la marca; existen -a mi juicio- defectos en muchos de esos pequeños dispositivos de almacenamiento USB llamados *'pendrives'*. Son cuestiones que, pienso, no hubiera costado mucho pulirlas antes de ponerse a fabricar y que, no sólo atañen a la calidad del componente, sino también al diseño. Como suele decirse: «cuesta lo mismo 
hacer las cosas mal que bien».  

<br>
## ¿Qué pedirle a un *'pendrive'* USB?
- Minimalismo, que su carcasa sea mínima. <small>Tenemos *'pendrives'* carísimos, de altas prestaciones incluso, que -debido al grosor de la carcasa- literalmente "no caben": no permiten conectarle ninguna otra cosa al lado o encima. Hay un estándar en el tipo de conector, mas no hay un estándar en el tamaño del chisme. Se supone que, en muchos casos, el dueño va a guardar información "valiosa" en él; mal asunto tener un dispositivo de almacenamiento mal conectado.</small>
- Que su carcasa sea poco aparatosa y/o artificiosa. <small>Tenemos cacharros de estos cuya carcasa es una especie de mecanismo mecánico que suele activarse en el momento menos oportuno; no es la primera vez que al conectar uno de esos literalmente la presión hace saltar el artilugio o resorte y oculta el conector del *'pendrive'*. ¿Se acuerdan vds de los cuchillos de broma, esos cuya hoja se oculta dentro del mango al ejercer presión en la punta? Exactamente eso.</small>
- Que tenga led indicador de actividad. <small>Es importante saber cuándo y cuánto está trabajando uno de esos dispositivos.</small>
- Calidad: Que sea fiable y duradero. <small>No es la primera vez que uno de estos cacharros falla -deja de permitir la escritura-, sorprendentemente, antes de tiempo. Y no siempre se trata de dispositivos baratos, de marcas blancas, adquiridos en tiendas chinas; eso ocurre también con algunos *'pendrives'* calificados en los mentideros de la red como "pata negra". Es sabido que ese tipo de almacenamiento tiene unos ciclos limitados de escritura, mas en ocasiones es escandaloso lo poco que tardan sus celdas en "agotarse".</small>
- Calidad: Rapidez. <small>Si en las especificaciones señala que es rápido, que en la práctica lo sea, escribiendo y leyendo.</small>  

<br>
<br>
Entradas relacionadas:  
- [Qué pedirle a un cable](https://hijosdeinit.gitlab.io/requisitos_de_un_cable/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
