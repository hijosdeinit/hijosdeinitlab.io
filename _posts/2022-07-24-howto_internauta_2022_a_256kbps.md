---
layout: post
title: "[HowTo] Internet del 2022 a velocidades de 256k: una guía de supervivencia lonchafinista (vol.01)"
date: 2022-07-24
author: Termita
category: "redes"
tags: ["redes", "redes moviles", "3g", "4g", "gprs", "adsl", "256k", "512k", "internet 1.0", "internet 4.0", "lonchafinismo", "zonas aisladas", "sistemas operativos", "linux", "gnu linux", "internet", "navegacion", "web", "protocolo", "http", "gopher", "gemini", "etica", "privacidad", "texto plano", "usenet"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## Internet *lonchafinista* en pleno 2022
No necesito más. Mi tarifa de datos móviles es 1€ por 100mb mensuales a velocidad *cuatrogé*, **agotados los cuales la velocidad se convierte en algo parecido a aquella de 256k que teníamos a principios de este siglo en las conexiones ADSL**.  
Llegado ese momento se manifiestan varios inconvenientes cuando [he de pasar el tiempo con computadoras en un pedazo de tierra de labranza aislada y sin conexión de banda ancha "cableada", a merced de una red de datos móviles]() lonchafinista distribuída por mi teléfono móvil en "modo router":
- La navegación -sobre todo si se emplean navegadores para escritorio- es muy lenta porque la web actual está demasiado "hinchada", tiene sobrepeso (<small>y esto se debe más al interés de los traficantes de datos personales y servicios que al beneficio del usuario</small>).
- el reducido ancho de banda impide la transferencia de archivos "pesados".  
Todo esto me devuelve -en parte- al internet de los noventa / principios del dosmil. Y digo "en parte" porque antaño la web era más ligera y la experiencia de navegación -a similar ancho de banda- era más rápida que ahora.  

<br>
## ... y una serie reflexiones que acontecieron
Como suele acontecer cuando los tiempos de espera o de carga son más largos de lo deseado, a mí han venido diversas **reflexiones**, porque la lentitud es prima hermana de la introspección y el tiempo ocioso no siempre es el padre de todos los vicios.  
Yo aún recuerdo cuando, a mediados de los noventa, pensábamos que internet -aquel internet donde el dominio .edu aportaba más que un .com- lo iba a cambiar todo y, al extenderse su uso a la mayoría de habitantes del planeta, el ser humano tendría a su alcance todo el conocimiento y lo aprovecharía.  
<br>
Acertamos en que internet lo cambió todo, mas no en lo segundo:  
La popularización de internet **no** ha tenido como consecuencia un enriquecimiento cultural del hombre, sino -paradójicamente- más **ignorancia**. Porque así se le llama, no tan solo a la carencia de conocimiento (harto aberrante cuando existen medios para corregirla), sino a la adquisición de conocimiento erróneo, defectuoso y/o sesgado (véase la desinformación imperante y campante, por no decir preponderante, por ejemplo). La "revolución de internet" guarda un significativo paralelismo con aquella anterior "revolución de la **televisión**"; de hecho la primera está sustituyendo rápidamente a la segunda, ahogándola, extinguiéndola. Hoy la televisión pareciera está dando sus últimos estertores. Mas la actitud generalizada del "consumidor" -de televisión o de internet, tanto da- es la misma.  

<br>
Pensábamos que el **conocimiento en internet sería libre, sin control**, y hete aquí que hoy tenemos censura, ataques a la **neutralidad de la red** y potentes iniciatias por parte de estados, corporaciones, mercaderes, *lobbies* y *thinktanks* para **controlar** de forma total lo que iba a ser un oasis de libertad.  
<br>
Creímos inocentemente que lo gratuíto y altruísta iba a ser preponderante en el internet del futuro, sin letra pequeña. Y hoy vemos que lo que tenemos es una legión de internautas mendigando dinero a los usuarios y una serie de corporaciones convirtiendo al internauta en adicto a esos "servicios gratuítos" que ofrecen y cuyas condiciones de uso cambian de una forma que harto recuerda a aquella leyenda urbana callejera de "la primera dosis es gratis, a partir de la segunda las condiciones cambiarán, tendrás que pagar, y a partir de la décima... mismo precio pero más proporción de corte".  
<br>
Aquellas redes **p2p**, creadas antaño, hoy son atacadas y censuradas desde diversos frentes. A los servidores hoy los llaman **"nube"**; siguen siendo "el ordenador de otra persona". El **consumo por suscripción** va implantándose, incluso entre aquellos que -hace no demasiados años- veían ridículo pagar por acceder a algo de lo que, salvo gratuitamente, prescindirían. El porqueyolovalguismo lorealista es tal que ya se habla de una nueva patología: el 'agotamiento por suscripción'.  
"No tendrás nada y serás feliz" rebuznaron los psicópatas en el 'foro de Davos'. Y hete aquí que el terreno ya les fue allanado por compañías / plataformas como NetFlix o Disney a las que sus usuarios -o usados- **pagan por consumir contenidos que, aunque (repito) hayan comprado, jamás poseerán**. Y pensar que no hace demasiado cuando, por ejemplo, una película merecía ser comprada, y la comprabas, ésta pasaba a ser tuya... para siempre.  

<br>
<br>
## Una estrategia de supervivencia a 256k
Y también he pensado en diseñar mi estrategia para afrontar mi experiencia en la red a través de velocidades del pasado:
- utilización de navegadores web para la terminal
- utilización de filtros contra la publicidad
- utilización de descargadores / recompresores para acceder a videos "en streaming", como son 'yt-dlp' o 'yt-downloader', que permiten -no sólo descargar casi cualquier video- sino recomprimirlo, reducirlo, extayendo -si cabe- el audio.
- utilización de gestores de descarga, o comandos de descarga, que permiten reanudar descargas interrumpidas.
- utilización de protocolos y "ecosistemas" alternativos al web: [BBS](), gopher, [gemini](), etcétera...
- trabajar en remoto cuando se trata de gestionar archivos pesados: conectando desde mi limitadísima red de datos móviles -vía vpn, por ejemplo- con servidores remotos (como el mío propio, allá en la urbe).
- quién sabe si sumando la conexión de datos móviles de mi esposa -tan lonchafinista como la mía- y mediante "balanceo de carga" podría duplicar la velocidad... quién sabe... lo que sí que sé es que lo intentaré.  

<br>
<br>
Entradas relacionadas:  
- [Instalación de 'amfora', navegador gemini para línea de comandos, en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_amphora_gemini_web_browser/)
- ['Gopher' hoy](https://hijosdeinit.gitlab.io/Gopher_hoy/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="@@@">@@@</a>  
<a href="@@@">@@@</a>  
<a href="@@@">@@@</a>  
<a href="@@@">@@@</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
