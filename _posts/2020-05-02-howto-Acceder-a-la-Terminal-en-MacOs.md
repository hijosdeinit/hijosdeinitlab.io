---
layout: post
title: "[HowTo] Acceder a la Terminal en Mac Os"
date: 2020-05-02
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "Mac Os", "Terminal", "CLI", "Apple"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Finder → Aplicaciones → Utilidades → Terminal  
<br>
<br>
--- --- ---
<br>
<small>Fuentes:  
<br>
<a href="https://es.ccm.net/faq/6484-mac-os-como-abrir-el-terminal" target="_blank">es.ccm.net</a></small>
