---
layout: post
title: "[HowTo] Instalación del 'MICROCODE' del Procesador (CPU) en Debian y derivados"
date: 2021-06-23
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "Debian", "microcode", "cpu", "gpu", "procesador", "intel", "amd", "ati", "nvidia", "drivers", "controladores", "firmware", "synaptic", "apt"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
El 'microcode' es un firmware que controla la forma de funcionar de los procesadores -CPU (~~¿y GPU?~~)-. Es importante incorporar el último 'microcode' al sistema operativo GNU Linux en pro de una mayor estabilidad y seguridad del sistema.  
Instalando el correspondiente 'microcode' del procesador (~~y ¿la tarjeta gráfica?~~) uno se asegura de recibir sus actualizaciones.  

<br>
## ¿Cómo incorporar el 'microcode' del procesador (CPU) a un sistema operativo GNU Linux Debian (o derivado)
Mediante el gestor de paquetes <big>**'Synaptic'**<big>, por ejemplo:  

1. Botón de actualizar repositorios  
(desde la terminal ejecutar `sudo apt-get update` también serviría)  

2. búsquese en el nombre y la descripción de los paquetes la cadena de caracteres 'microcode'.  

3. Selecciónese e Instálese -entre los resultados de la búsqueda- el paquete correspondiente a la plataforma -Intel o AMD- de nuestro procesador.  

<br>
<br>

Entradas relacionadas:  
- []()  

<br>
<br>
<br>

--- --- ---  
<small>Fuentes:  
<a href="https://averagelinuxuser.com/debian-10-after-install/">AverageLinuxUser</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
