---
layout: post
title: "[HowTo] 'newsbeuter' y/o 'newsboat', agregadores rss desde el terminal"
date: 2022-02-10
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "cli", "linea de comandos", "newsbeuter", "newsboat", "feed", "rss", "atom", "productividad"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Conocí 'newsbeuter' a través de Mhyst en su [canal 'Reality Cracking'](https://odysee.com/@realitycracking:4/leer-rss-en-la-terminal:5)  

['newsbeuter'](https://github.com/akrennmair/newsbeuter) -ya descontinuado- y su *fork* ['newsboat'](https://github.com/newsboat/newsboat) -mantenido activamente-, son lectores de feeds RSS/Atom para la línea de comandos.  
Un [lector de feeds](https://en.wikipedia.org/wiki/News_aggregator) muestra inmediata y directamente las actualizaciones de aquellos recursos web -blogs, páginas web, etc- que se le indiquen. De esta forma se pueden revisar todos en un sólo interfaz, de un sólo vistazo.  
Muchas veces el feed incluye todo el texto del artículo, hecho que propicia no haber de recurrir al navegador web para leerlo completamente.  
'newsbeuter' fue escrito originalmente por Andreas Krenmair en 2007 y lanzado bajo licencia MIT.  
Está diseñado para sistemas operativos de tipo Unix como GNU Linux y FreeBSD (NetBSD no está soportado).  
Se le considera el "['mutt']() de los agregadores de feeds rss". Soporta la mayoría de formatos de feed, incluyendo RSS y Atom, y puede importar y exportar listas de suscripciones en formato 'OPML'.  
Formando parte de 'newsbeuter' existe ['podbeuter'](https://www.mankier.com/1/podbeuter), elemento enfocado a la suscripción a feeds de podcasts.  
En 2017 el proyecto dejó de ser mantenido y los desarrolladores originales aconsejaron a los usuarios cambiarse a 'newsboat', un fork de 'newsbeuter' mantenido activamente.  

<br>
<a href="/assets/img/2022-02-10-howto_newsbeuter_newsboat_rss/newsbeuter.png" target="_blank"><img src="/assets/img/2022-02-10-howto_newsbeuter_newsboat_rss/newsbeuter.png" alt="'newsbeuter', el padre de 'newsboat'" width="500"/></a>  
<small>'newsbeuter', el padre de 'newsboat'</small>  
<br>

<br>
<br>
## Instalación de 'newsbeuter' y/o 'newsboat' en Debian 10 y derivados
Puede ser instalado de diversas formas:
- mediante nueva paquetería (*snap*): `sudo snap install newsboat`
- [binarios precompilados](https://newsboat.org/releases/2.26/docs/newsboat.html#_pre_built_binaries)
- compiládolo a partir de su código fuente
- desde los repositorios oficiales de Debian  

Escogí instalarlos desde los repositorios oficiales de Debian.  
<br>
En Debian 10, al instalar 'newsbeuter' desde los repositorios también se instala su fork, 'newsboat'  
~~~bash
sudo apt-get update
sudo apt install newsbeuter
~~~
Si sólo se desea instalar 'newsboat', es decir el *fork* activamente mantenido:
~~~bash
sudo apt-get update
sudo apt install newsboat
~~~

<br>
<br>
## Ejecución y configuración de 'newsbeuter' y/o 'newsboat'
'newsbeuter' se ejecuta así:
~~~bash
newsbeuter
~~~
'newsboat' se ejecuta así:
~~~bash
newsboat
~~~
<br>
La primera vez que se ejecuta 'newsbeuter' se crea el **archivo de configuración** en el directorio '~/.newsbeuter'.  
Asimismo comunica que no existen urls de feeds agregados:
> Error: no hay URLs configuradas. Por favor inserta RSS en el fichero '~/.newsbeuter/urls' o importa un fichero OPML.  

Por consiguiente, es necesario crear el archivo '~/.newsbeuter/urls' e incorporarle los feeds que nos interesen:
~~~bash
nano ~/.newsbeuter/urls
~~~

La primera vez que se ejecuta **'newsboat'**, migra configuraciones y datos desde el directorio '~/.newsbeuter'.  
En la [página oficial de 'newsboat'](https://newsboat.org/releases/2.26/docs/newsboat.html) existe una amplia documentación que detalla, entre otras cosas, los atajos de teclado.  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://en.wikipedia.org/wiki/Newsbeuter">Wikipedia - 'newsbeuter'</a>  
<a href="http://tuxdiary.com/2014/08/09/newsbeuter/">TuxDiary</a>  
<a href="https://fossbytes.com/how-to-use-newsboat-rss-feed-reader-on-linux/">FossBytes</a>  
<a href="https://en.wikipedia.org/wiki/Comparison_of_feed_aggregators">Wikipedia - comparación de agregadores de feeds rss / atom</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
