---
layout: post
title: "Pruebo 'min' browser (v.1.20)"
date: 2021-06-30
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "ubuntu", "web browser", "navegador", "internet", "internet 4.0", "min browser", "min", "min browser v.1.20", "chrome", "chromium", "edge", "firefox", "waterfox", "librefox", "librewolf", "icecat", "iceweasel", "privacidad", "anonimato", "respeto"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno:
cpu Core2Duo, 6gb RAM, gpu gforce 7600gt, GNU Linux Debian 10 Buster (*netinstall*) stable, kernel 4.19.0-17-amd64, gnome</small>  

<br>
## ¿Qué es 'min' browser?
['min'](https://minbrowser.org/) es un navegador web minimalista, supuestamente ligero, basado en 'Chromium'. Su interfaz está programada en 'javascript', 'CSS' y 'html'.  
No es un navegador web enfocado a la privacidad.  

<br>
La instalación de 'min' browser es sencilla:
Basta con descargar la última versión desde la [página oficial de 'min'](). <small>La versión más actualizada mientras escribo esto es la v.1.20</small>
~~~bash
wget https://github.com/minbrowser/min/releases/download/v1.20.0/min_1.20.0_amd64.deb
~~~
... y ejecutar
~~~bash
sudo dpkg -i min_1.20.0_amd64.deb
~~~
Supuestamente 'min' browser está basado en 'electron', aunque por lo que veo se suministra un paquete '.deb'. Yo soy un completo ignorante en lo que a nueva paquetería (*electron*, *snap*, *appimage*) respecta.  

<br>
<br>
## Mis impresiones de la versión 1.20, cosas mejorables / corregibles:  
1. La configuración por defecto tiene sospechosamente activada 1 opción poco respectuosa con la privacidad.  
Es esta sinvergonzonería algo bastante habitual y llamativo en 'Firefox': opciones de compartición de datos, aunque sea de depuración, **activadas por defecto** en lugar de preguntar en la primera ejecución, de manera explícita, si se desean activar o no, o -mejor aún- mantenerlas por defecto desactivadas para que sea el propio usuario el que si quiere -o si se acuerda o si por casualidad se las encuentra- las active o no.  
En el caso de 'min' browser la configuración poco respetuosa con la privacidad que trae activada por defecto es la que hace referencia a 'ENVIAR ESTADÍSTICAS DE USO'. Muy mal.  
<br>
<a href="/assets/img/2021-06-30-defectos_actuales_MIN_web_browser/min_brower_defectos_configuracion_pordefecto_poco_respetuosa_privacidad.png" target="_blank"><img src="/assets/img/2021-06-30-defectos_actuales_MIN_web_browser/min_brower_defectos_configuracion_pordefecto_poco_respetuosa_privacidad.png" alt="configuración por defecto poco respetuosa con la privacidad" width="600"/></a>  
<small>'min' browser v.1.20: una configuración por defecto poco respetuosa con la privacidad</small>  
<br>


2. En ocasiones, los nombres de las pestañas no se corresponden con el contenido, sino con el contenido de la página que se cargó anteriormente.  
Por ejemplo: Si estando en la página web 'https://atareao.es' cargo la url 'https://hijosdeinit.gitlab.io', la etiqueta de la pestaña seguirá siendo "inicio - atareao".  

<br>
<br>
## Mea Culpa
**Por defecto** (es decir, recién instalado), en ordenadores veteranos como el mío (véase descripción al inicio de esta entrada), con aceleradoras gráficas antiguas, 'min' es menos ligero que su competencia, pese a su minimalismo  
Esto puede corregirse, como con casi todos los navegadores, lanzándolo con **parámetros que lo optimizan**.  
Una ventana con una pestaña de 'Firefox' u otro navegador convencional funcionaba muchísimo más fluida que en 'min' browser recién instalado.  
Esto se notaba sobre todo en el 'scroll', que en 'min' iba a trompicones.  
Este defecto pudiera estar relacionado con mi sistema operativo -cuya aceleración gráfica (drivers *nouveau*) sospecho no está del todo bien configurada- o bien con la [ralentización que se daba en el navegador 'Chromium' cuando el *smooth scrolling* estaba activado](https://bgr.com/tech/fix-slow-chrome-browser-scrolling-lag-4831185/).  
En 'min' no hay forma de acceder a la configuración de *smooth scrolling* al modo de su pariente 'Chromium': dirigirse a la *url* 'min://flags/#disable-smooth-scrolling' para desactivarlo es futil, no existe.  
Sin embargo, en el repositorio de 'min' en 'GitHub' [señalan](https://github.com/minbrowser/min/issues/558#issuecomment-389758430) que para deshabilitar el *smooth scrolling* hay que editar -como superusuario- el archivo '/usr/lib/min/resources/app/main/main.js'
~~~bash
sudo nano /usr/lib/min/resources/app/main/main.js
~~~
... y añadirle la línea `'app.commandLine.appendSwitch('disable-smooth-scrolling');'`, que permitiría lanzar 'min' con la opción `--disable-smooth-scrolling`:
~~~bash
min --disable-smooth-scrolling
~~~
<br>
El interfaz, no obstante, seguía siendo desesperantemente lento y tragón (de recursos). Ralentizaba, además, todo el sistema operativo.  
<big>**Solución**</big>: lanzar 'min' con la opción `--disable-gpu`, bien desde la terminal o bien modificando el "lanzador". Y eso es aplicable a otros navegadores basados en Chromium.
~~~bash
min --disable-gpu
~~~

<br>
Los "Artefactos gráficos" en la visualización de "fondos dinámicos" (en ordenadores y/o aceleradoras gráficas antiguas) de páginas web son cosa mía, relacionado con mi configuración gráfica, que sospecho no es del todo compatible con la aceleración gráfica que suele utilizar 'Chromium'.  
Esto también me ocurre con el navegador 'Brave', que -creo recordar-, al igual que 'min', está basado en 'Chromium'.  
<br>
<a href="/assets/img/2021-06-30-defectos_actuales_MIN_web_browser/min_brower_defectos_artefactos_graficos.png" target="_blank"><img src="/assets/img/2021-06-30-defectos_actuales_MIN_web_browser/min_brower_defectos_artefactos_graficos.png" alt="artefactos" width="600"/></a>  
<small>"artefactos" gráficos</small>  
<br>
**Solución**: lanzar 'min' con la opción `--disable-gpu`, bien desde la terminal o bien modificando el "lanzador". Y eso es aplicable a otros navegadores basados en Chromium.
~~~bash
min --disable-gpu
~~~

<br>
<br>
Entradas relacionadas:  
- [¿Harto de Firefox? Alternativas: WaterFox, LibreFox, LibreWolf](https://hijosdeinit.gitlab.io/WaterFox_LibreFox_LibreWolf_BadWolf/)
- [[HowTo] Instalación NATIVA de 'epiphany' ('gnome-web') en Ubuntu y familia](https://hijosdeinit.gitlab.io/howto_instalacion_nativa_epiphany_browser_ubuntu/)
- [[HowTo] Instalación de MicroSoft Edge (dev) en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalar_ms_edge_chromium_dev_en_debian_derivados/)
- ["Instalación de 'amfora', navegador gemini para línea de comandos, en Debian y derivados"](https://hijosdeinit.gitlab.io/howto_instalacion_amphora_gemini_web_browser/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://bgr.com/tech/fix-slow-chrome-browser-scrolling-lag-4831185/">bgr.com - scrolling lag en Chromium</a>  
<a href="https://github.com/minbrowser/min/issues/558#issuecomment-389758430">'min'- GitHub - issues</a>  
<a href="https://peter.sh/experiments/chromium-command-line-switches/">bgr.com - scrolling lag en Chromium</a>  
<a href="https://laboratoriolinux.es/index.php/-noticias-mundo-linux-/software/30196-min-browser-1-20-llega-con-mejoras-para-ventanas-emergentes-mas-motores-de-busqueda-y-mas.html">LaboratorioLinux</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

