---
layout: post
title: "[HowTo] Incorporando una unidad de diskette en 2021. vol. II: bregando con diskettes en GNU Linux"
date: 2021-06-14
author: Termita
category: "hardware"
tags: ["hardware", "diskette", "floppy disk", "floppy", "sistemas operativos", "linux", "gnu linux", "Windows", "sistema de archivos", "format", "formato", "retromatica", "286", "386", "486", "nostalgia", "retro", "vintage", "sneakernet"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## Creación de una imagen "vacía" de disquette
~~~bash
dd bs=512 count=2880 if=/dev/zero of=/tmp/imagendisquete.img
~~~

<br>
## Formatear una imagen "vacía" de diskette
~~~bash
/sbin/mkfs.msdos /tmp/imagendisquete.img
file /tmp/imagendisquete.img
~~~

<br>
## Montar la imagen de un diskette
~~~bash
mkdir /media/floppy1/
mount -o loop /tmp/imagendisquete.img /media/floppy1/
~~~

<br>
## Creación de una imagen a partir de un disquette real
~~~bash
dd if=/dev/fd0 of=disquette.img
~~~

<br>
## Copiar una imagen de disquete a un disquette real.
~~~bash
dd if=disquette.img of=/dev/fd0
~~~

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Incorporando una unidad de diskette en 2021. vol. I](https://hijosdeinit.gitlab.io/howto_diskettera_floppy_linux_hoy_vol01/)
- [GNU Linux en 1 diskette: 'Floppinux', 'MuLinux'](https://hijosdeinit.gitlab.io/linux_en_1_diskette/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://56k.es/fanta/trabajar-en-2022-con-disquetes-sobre-gnulinux/">56k.es</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
