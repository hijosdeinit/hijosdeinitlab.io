---
layout: post
title: '[HowTo] Instalación netinstall de Debian en netbook Compaq Mini'
date: 2020-09-28
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "netinstall", "Debian", "lonchafinismo", "netbook", "atom", "Compaq Mini", "openbox", "lxde", "32 bits", "64 bits", "x32", "x64", "x386", "i686", "ordenadores viejos", "resurrección"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
## Antecedentes
Tenía en un cajón un viejo netbook Compaq Mini. Sus especificaciones son humildes, mas para eso está GNU Linux, que tradicionalmente posibilita que, por vieja que sea una máquina, si ésta enciende pueda y deba trabajar.  
<br>
<a href="/assets/img/2020-09-28-howto_instalacion_netinstall_Debian_netbook_compaqmini/compaq_mini.png" target="_blank"><img src="/assets/img/2020-09-28-howto_instalacion_netinstall_Debian_netbook_compaqmini/compaq_mini.png" alt="Compaq Mini CQ10-100" width="400"/></a>  
<small>Compaq Mini CQ10-100</small>  

<br>
[Últimamente](https://hijosdeinit.gitlab.io/Germen_rendicion_gnulinux_a_obsolescencia/) veo que frente a la tradicional concepción lonchafinista del "no tirar nada" que abundaba en el entorno de usuarios y desarrolladores de GNU Linux, va cogiendo fuerza otra corriente, más propensa a conformarse con la obsolescencia.  
Ya hace tiempo hay desarrolladores de distribuciones que abandonan arquitecturas que, lamentablemente (a mi juicio), consideran obsoletas como, por ejemplo, 32 bits y enfocan su trabajo exclusivamente en las máquinas que consideran hegemónicas como, por ejemplo, 64 bits.  
Lo peor -siempre a mi entender- es que cada vez hay más usuarios que comprenden eso.  
De momento, afortunadamente, no es este el caso de Debian. Debian sí desarrolla para 32 bits, por ejemplo, y continuará haciéndolo. La cuestión es si los desarrolladores de aplicaciones continuarán haciéndolo para 32 bits... de tal forma que en un futuro no muy lejano podemos hallarnos con el problema de tener un sistema operativo "base" totalmente actualizado plagado de aplicaciones que no lo estén.  
Mas, pese a todo, hoy la situación es aún correcta. Instalé Debian en este vetusto netbook Compaq Mini.  
Sus especificaciones, muy humildes como ya he dicho, son básicamente:
~~~
Compaq Mini CQ10-100
Resolución: 1024x600
CPU: Intel Atom N270 (2) @ 1.600GHz
RAM: 1gb. (ampliable a 2gb.)
GPU: Intel Mobile 945GM/GMS/GME, 943/940GML Express
USB: 3 usb (2.0)
~~~

<br>
## Procedimiento

### Descarga de Debian 10 (netinstall) y herramientas útiles


### Quemado de la ISO de Debian 10 (netinstall)


### Instalación de Debian 10 (netinstall) en netbook Compaq Mini


<br>
## Configuración adicional




~~~
       _,met$$$$$gg.          nervic@pizarro 
    ,g$$$$$$$$$$$$$$$P.       -------------- 
  ,g$$P"     """Y$$.".        OS: Debian GNU/Linux 10 (buster) i686 
 ,$$P'              `$$$.     Host: Compaq Mini CQ10-100 0493200000001C00000300000 
',$$P       ,ggs.     `$$b:   Kernel: 4.19.0-11-686-pae 
`d$$'     ,$P"'   .    $$$    Uptime: 13 mins 
 $$P      d$'     ,    $$P    Packages: 1145 (dpkg) 
 $$:      $$.   -    ,d$$'    Shell: bash 5.0.3 
 $$;      Y$b._   _,d$P'      Resolution: 1024x600 
 Y$$.    `.`"Y$$$$P"'         WM: Openbox 
 `$$b      "-.__              WM Theme: Nightmare 
  `Y$$                        Theme: Adwaita-dark [GTK2/3] 
   `Y$$.                      Icons: HighContrast [GTK2/3] 
     `$$b.                    Terminal: lxterminal 
       `Y$$b.                 Terminal Font: Monospace 10 
          `"Y$b._             CPU: Intel Atom N270 (2) @ 1.600GHz 
              `"""            GPU: Intel Mobile 945GM/GMS/GME, 943/940GML Express 
                              Memory: 203MiB / 994MiB 
~~~


<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[]()  
[]()</small>  
<br>
<br>
<br>
<br>
