---
layout: post
title: "Protocolo 'GEMINI': retornando a la senda de 'Gopher' en pro de una navegación sin aditivos"
date: 2021-05-31
author: Termita
category: "internet 4.0"
tags: ["internet 4.0", "sistemas operativos", "linux", "gnu linux", "internet", "navegacion", "web", "protocolo", "http", "gopher", "gemini", "etica", "privacidad", "texto plano", "bombadillo", "amfora", "agate", "jetforce", "lagrange", "kristall"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Cuando porté este blog desde google-'Blogger' a 'Gitlab' (Jekyll) buscaba minimalismo, eficiencia, pragmatismo, eficacia, primar el contenido sobre la estética barroca que caracteriza la web actual y, sobre todo, respetarme a mí mismo y al eventual navegante. Ser coherente, en definitiva, con lo que pienso del estado actual de las cosas.  
Como internauta iniciado a principios de los años 90 conocía 'Gopher', 'Telnet', 'Usenet' y otros protocolos que coexistían con la incipiente 'world wide web'.  
No ha mucho tuve conocimiento de 'Gemini', un nuevo protocolo "paralelo" a la 'web' que despertó todo mi interés y que me está haciendo plantear, si no portar todo el contenido del blog, crear un espejo -*'gemlog'* o similar- bajo dicho protocolo.  

<br>
<a href="/assets/img/2021-05-31-protocolo_Gemini_retornando_al_camino_Gopher_solucionando_deficiencias_web/internet_oldschool.jpg" target="_blank"><img src="/assets/img/2021-05-31-protocolo_Gemini_retornando_al_camino_Gopher_solucionando_deficiencias_web/internet_oldschool.jpg" alt="internet de la vieja escuela, alegoría" width="500"/></a>  
<br>

<br>
El [protocolo 'Gemini'](https://gemini.circumlunar.space/) es una deriva actual del protocolo 'Gopher', una alternativa al protocolo 'web' (http).  
Cuando nació el protocolo Web ya existía el protocolo 'Gopher'. El primero se impuso sobre el segundo y, con el devenir del tiempo, dio lugar a lo que hoy conocemos: la web moderna, con todos sus claros y todos sus oscuros.  
'Gemini' es una vuelta a lo que debió ser internet, un retorno a aquel punto desde el cual subsanar errores esenciales que han ido acontenciendo y se han ido imponiendo hasta el punto en que hoy todo el mundo pareciera los ve como algo normal y habitual.  
El protocolo 'Gemini' se aleja del concepto de internet como revista, plagado de artificios, con la información sepultada entre mil filigranas supuestamente estéticas y una pléyade de scripts que en nada potencian la adquisición de conocimiento, con el internauta convertido en mercancía, catalogado, cuantificado y con su privacidad siempre expuesta cuando no invadida.  

El protocolo 'Gemini' se fundamenta en el poder del **texto plano**, sin aditivos y filigranas innecesarios, primando el contenido y respetando al navegante.  
El código se escribe en 'markdown', el conocido lenguaje de marcas.  

<br>
Si para acceder a un sitio de internet bajo protocolo 'Web' la URL comienza por ***'http://'***, para acceder a un espacio de internet que sirve contenidos a través del protocolo 'Gemini' la URL comienza por ***'gemini://'***. En el caso de 'Gopher' es ***'gopher://'***.  

<br>
<a href="/assets/img/2021-05-31-protocolo_Gemini_retornando_al_camino_Gopher_solucionando_deficiencias_web/20210601_project_Gemini.png" target="_blank"><img src="/assets/img/2021-05-31-protocolo_Gemini_retornando_al_camino_Gopher_solucionando_deficiencias_web/20210601_project_Gemini.png" alt="Homepage del Proyecto 'Gemini'" width="800"/></a>  
<small>*Homepage* del Proyecto 'Gemini'</small>  
<br>

No cualquier navegador sirve para explorar los contenidos que se suministran a través de 'Gemini' -o de 'Gopher'-.  

<br>
En el caso de 'Gopher', navegadores web para la línea de comandos (CLI) como 'lynx' permiten de serie acceder al protocolo 'gopher'.  
Para los navegadores web habituales, como son 'Mozilla Firefox' y 'Google Chrome', existe una extensión que posibilita navegar por los contenidos de 'Gopher'.  

<br>
Por el contrario, en el caso de 'Gemini' para acceder a los contenidos que bajo este protocolo se sirven a la red hay que emplear un navegador específicamente diseñado para el efecto.  
En [gemini.circumlunar](https://gemini.circumlunar.space/clients.html) hay una lista de navegadores para 'Gemini' y otros protocolos.  
Existen navegadores **multiprotocolo** -permiten navegar por 'Gemini', 'Gopher', 'Finger', 'Telnet' y/o'Web'- como ['Bombadillo'](https://bombadillo.colorfield.space/) y otros, navegadores para la línea de comandos -['Amfora'](https://github.com/makeworld-the-better-one/amfora), [gplaces](https://github.com/dimkr/gplaces), etcétera- y también navegadores para el entorno gráfico como son ['LaGrange'](https://gmi.skyjake.fi/lagrange/), ['Kristall'](https://github.com/MasterQ32/kristall), etcétera.

<br>
También se puede navegar por 'Gemini' **desde un navegador convencional -sin instalar absolutamente nada en nuestro sistema-** a través de páginas web que actúan como *proxies*: [portal.mozz.us](https://portal.mozz.us/)  

<br>
Asimismo se puede navegar por 'Gemini' o 'Gopher' **via terminal, sin instalar nada**, conectando por ssh al servidor **'gemini.circumlunar.space'**, que permite -desde línea de comandos, evidentemente- utilizar los navegadores 'bombadillo', 'amfora' y 'AV-98'
~~~bash
ssh kiosk@gemini.circumlunar.space
~~~

<br>
<br>
Para servir contenidos propios en el ecosistema 'Gemini' existen varias opciones:
- Solicitar hosting en uno de los varios servidores 'Gemini' existentes: en [gemini://gemini.circumlunar.space/servers/](gemini://gemini.circumlunar.space/servers/) hay un listado de servidores que pueden proporcionar alojamiento gratuíto
- Autoalojar una cápsula 'Gemini' en tu propio servidor. Para ello existe software servidor como, por ejemplo: [Agate](https://github.com/mbrubeck/agate) ô [Jetforce](https://github.com/michael-lazar/jetforce)  

<br>
<br>
Entradas relacionadas:  
- [Instalación de 'amfora', navegador gemini para línea de comandos, en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_amphora_gemini_web_browser/)
- [[HowTo] 'bombadillo', navegador gemini para línea de comandos, en Debian y derivados: instalación y primeros pasos](https://hijosdeinit.gitlab.io/howto_bombadillo_gemini_web_browser_debian_y_derivados/)
- ['Gopher' hoy](https://hijosdeinit.gitlab.io/Gopher_hoy/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://geminiprotocol.net/clients.html">GeminiProtocol.net - clientes</a>  
<a href="https://programadorwebvalencia.com/introduccion-protocolo-gemini/">ProgramadorWebValencia - introducción al protocolo Gemini</a>  
<a href="@@@">@@@</a>  
<a href="@@@">@@@</a>  
<a href="@@@">@@@</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
