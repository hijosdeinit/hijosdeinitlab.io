---
layout: post
title: "[HowTo] Aumentar el tamaño del disco duro virtual de una máquina virtual de Virtual Box"
date: 2021-02-09
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "virtualizacion", "almacenamiento", "maquinas virtuales", "disco duro", "disco duro virtual", "almacenamiento virtual", "virtual box", "windows", "windows 10"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Es posible que cuando diseñamos una máquina virtual nos hayamos quedado cortos a la hora de asignar espacio para su disco duro (virtual).
Si éste lo hemos establecido como <big>"**dinámico**"</big>, cosa que hace Virtual Box por defecto, podremos redimensionarlo.  
**‼** Sin embargo, si lo establecimos de tipo <big>"fijo"</big>, este procedimiento NO sirve.  

<br>
<br>
## Procedimiento
Escenario: Windows 10 como sistema anfitrión de una máquina virtual de Virtual Box corriendo Windows 10.  

<br>
1. En Virtual Box establecemos el nuevo tamaño  
Herramientas → DiscosDuros → Propiedades : Establecemos el nuevo tamaño.  
<br>
<a href="/assets/img/2021-02-09-howto_aumentar_tamaño_discodurovirtualdinamico_maquinavirtual_virtualbox/virtualbox_propiedades_discodurovirtual.png" target="_blank"><img src="/assets/img/2021-02-09-howto_aumentar_tamaño_discodurovirtualdinamico_maquinavirtual_virtualbox/virtualbox_propiedades_discodurovirtual.png" alt="Establecimiento del nuevo tamaño desde las propiedades de los discos duros" width="800"/></a>  
<small>Establecimiento del nuevo tamaño desde las propiedades de los discos duros</small>  
<br>

2. Arrancamos la máquina virtual Windows 10  

3. Administrador de Archivos > click derecho en "Este Equipo" > Administrar > Administración de Discos  

4. Click derecho sobre la partición que queremos aumentar > Extender Volumen  
<br>
<a href="/assets/img/2021-02-09-howto_aumentar_tamaño_discodurovirtualdinamico_maquinavirtual_virtualbox/aumentarparticiondiscodurovirtual.jpg" target="_blank"><img src="/assets/img/2021-02-09-howto_aumentar_tamaño_discodurovirtualdinamico_maquinavirtual_virtualbox/aumentarparticiondiscodurovirtual.jpg" alt="Extender volumen" width="800"/></a>  
<small>Extender volumen</small>  

<br>
<br>
Entradas relacionadas:  
- [Software de virtualización (máquinas virtuales)](https://hijosdeinit.gitlab.io/utilidades_maquinas_virtuales/)
- [[HowTo] Instalación de 'Oracle VirtualBox' 6.1 64bits en Debian 10 y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_virtualbox_debian10_y_derivados/)  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.raulprietofernandez.net/blog/virtualizacion/como-redimensionar-un-disco-virtual-en-virtualbox">raulprietofernandez.net</a>  
<a href="http://duenaslerin.com/aumentar-tamano-disco-virtual-virtualbox">duenaslerin</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
