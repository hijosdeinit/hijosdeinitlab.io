---
layout: post
title: "[HowTo] Aligerar, reducir y/o recomprimir audio mediante 'ffmpeg'"
date: 2022-01-29
author: Termita
category: "multimedia"
tags: ["video", "audio", "multimedia", "mp3", "mp4", "h264", "aac", "ffmpeg", "avconv", "aligerar", "reducir", "recomprimir", "recompresion", "extraer", "thumbnail", "sistemas operativos", "gnu linux", "linux", "debian", "ubuntu", "windows", "macos"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small><small>Entorno:
cpu Core2Duo, 6gb RAM, gpu gforce 7600gt, GNU Linux Debian 10 Buster (*netinstall*) stable, kernel 4.19.0-18-amd64, gnome</small></small>  

<br>
<br>
<br>
Hace unos días el usuario de GNU Linux [Crispin](https://mas.to/@crispin) mostraba un script que había hecho para reducir notablemente el tamaño de los archivos de audio. Utilizaba ['avconv'](https://www.libav.org/avconv.html), comando que forma parte de [libav](https://www.libav.org), un *fork* -[ya descontinuado](https://en.wikipedia.org/wiki/Libav#cite_note-5)- de [ffmpeg](https://ffmpeg.org/):
~~~bash
for F in *.mp3; do avconv -i "$F" -sn -vn -ar 22050 "${F%mp3}ogg"; done; rm *.mp3; for F in *.ogg; do avconv -i "$F" -sn -vn -ar 22050 "${F%ogg}mp3"; done; rm *.ogg
~~~
Del script se puede extraer el comando y los parámetros, que son:
~~~bash
avconv -i input -sn -vn -ar 22050 output
~~~
Por ejemplo: <span style="background-color:#042206"><span style="color:lime">`avconv -i input.mp3 -sn -vn -ar 22050 output.mp3`</span></span>  

<br>
<br>
Como yo no tengo instalado 'avconf' y este comando es primo hermano de **'ffmpeg'**, adaptando ligeramente el comando también funciona:
~~~bash
ffmpeg -i input -sn -vn -ar 22050 output
~~~
Por ejemplo: <span style="background-color:#042206"><span style="color:lime">`ffmpeg -i input.mp3 -sn -vn -ar 22050 output.mp3`</span></span>  

<br>
<br>
<br>
Este comando puede servir de complemento a la extracción de audio en formato '.aac' de videos (<small><span style="background-color:#042206"><span style="color:lime">`ffmpeg -i 'Una_tregua_en_los_medios_para_ir_por_los_pequeños.-385b83c44b96461f2cac3f87ae84d5f04bfdacaa.mp4' -vn -acodec copy 'Una_tregua_en_los_medios_para_ir_por_los_pequeños.-385b83c44b96461f2cac3f87ae84d5f04bfdacaa.aac'`</span></span></small>), es decir, para extraer la base '.aac' a recodificar -<small>mediante el comando explicado en esta entrada</small>- en '.mp3' reduciendo el "peso".  

<br>
<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://mas.to/@crispin/107653972801055372">Crispin en Mastodon</a>  
<a href="https://soundbridge.io/audio-formats-file-types/">SoundBridge.io - tipos de archivo de audio</a> [<small>descargar .pdf</small>](/assets/docs/2022-01-29-howto_aligerar_reducir_recomprimir_audio_mediante_ffmpeg/audio_formats_file_types.pdf)</small>  
<br>
<br>
<br>
<br>
<br>
<br>
