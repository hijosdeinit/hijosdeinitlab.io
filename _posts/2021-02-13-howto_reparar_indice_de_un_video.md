---
layout: post
title: "[HowTo] Reparar el índice de un video dañado"
date: 2021-02-13
author: Termita
category: "multimedia"
tags: ["multimedia", "gnu linux", "linux", "mencoder", "vlc", "indice", "avi", "mkv", "mp4", "recuperar", "reparar"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Cuando el índice de un archivo de vídeo se corrompe hay que "reconstruirlo".  
Esto se puede hacer de varias formas:  

<br>
## Mediante el reproductor de vídeo VLC
Al abrir un archivo de video cuyo índice esté dañado, VLC preguntará si se desea reconstruirlo. No obstante la reparación **no será persistente**, es decir, no se reescribirá el archivo de video.  

<br>
## Mediante el comando 'mencoder'
El índice reconstruído se escribirá en el archivo de vídeo, es decir, la reparación **será persistente**.  
Desde línea de comandos:
~~~bash
mencoder -idx input.avi -ovc copy -oac copy -o output.avi
~~~

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://techlandia.com/arrancar-cd-ventana-comandos-como_123535/">Techlandia</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
