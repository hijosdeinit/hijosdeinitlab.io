---
layout: post
title: "[HowTo] 'WhaleBird', un cliente de escritorio para el 'fediverso' ('Mastodon', 'Misskey' y 'Pleroma') en Debian y derivados"
date: 2022-01-24
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "fediverso", "internet 4.0", "nodos", "instancias", "instancia", "descentralización", "federacion", "redes descentralizadas", "redes federadas", "federacion", "blockchain", "red social", "redes sociales", "mastodon", "fosstodon", "diapora", "quitter", "gnu social", "friendi", "friendica", "hubzilla", "identi.ca", "identi", "pump.io", "pump", "pixelfed", "mediagoblin", "funkwhale", "devtube", "peertube", "misskey", "invidious", "yewtu.be", "web 4.0", "descargar", "descargas", "streaming", "codigo abierto", "open source", "privacidad", "tracking", "rastreo", "anonimato", "autosuficiencia", "whalebird", "tootle", "toot"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small><small>Entorno:
cpu Core2Duo, 6gb RAM, gpu gforce 7600gt, GNU Linux Debian 10 Buster (*netinstall*) stable, kernel 4.19.0-18-amd64, gnome</small></small>  

<br>
<br>
['whalebird'](https://whalebird.social/en/desktop/contents) es una aplicación cliente para las redes sociales descentralizadas ['Mastodon'](https://mastodon.social/), ['Pleroma'](https://pleroma.social/) y ['Misskey'](https://join.misskey.page/en-US/).  
<small>Esas redes forman parte del 'fediverso'. [Sobre el 'fediverso' y las redes que lo componen existe en este blog un artículo](https://hijosdeinit.gitlab.io/Fediverso_redes_descentralizada/)</small>.
<br>
Whalebird es una aplicación para el escritorio, es decir, es un interfaz gráfico, no un programa para la línea de comandos.  
Whalebird es [multiplataforma](https://whalebird.social/en/desktop/contents/downloads), tiene versiones no sólo para GNU Linux, sino también para Windows, y macOS. Soporta arquitecturas de 64bits, 32bits (<small>sólo he sido capaz de encontrar versión para Windows 32bits</small>) y arm.  
Existen paquetes de instalación nativa para las diferentes distribuciones GNU Linux, 'snap' y 'appimage'; todos ellos de 64bits.  

<br>
<br>
## Procedimiento de instalación de 'whalebird' en Debian 10 y derivados
Como, en su configuración de repositorios "estándar", Debian 10 *Buster* desde `apt` no encontraría el paquete, y desde la "tienda de software" instalaría el ['snap'](https://snapcraft.io/whalebird) (<small>nueva paquetería, <small>[ya hablamos de ella en una saga de artículos de este blog](https://hijosdeinit.gitlab.io/howto_snap_en_debian_10_buster_y_derivados/)</small></small>) y no una versión nativa, y yo prefiero instalar los programas <big>**de forma nativa**</big>:
1. Descárguese desde el apartado ['releases'](https://github.com/h3poteto/whalebird-desktop/releases) del [repositorio de 'Whalebird' en GitHub](https://github.com/h3poteto/whalebird-desktop) el "instalador" '.deb' más reciente y apropiado para el sistema. En mi caso, en el momento de escribir esta entrada, éste es el '.deb' de la versión 4.5.0 64bits.
2. Mediante el comando 'apt' instalo el paquete '.deb' descargado. Podría haberlo hecho también mediante `sudo dpkg -i nombredelpaquete.deb`.  

Resumiendo, estos son los comandos:
~~~bash
cd ~/Descargas
wget 'https://github.com/h3poteto/whalebird-desktop/releases/download/4.5.0/Whalebird-4.5.0-linux-x64.deb'
sudo apt install ./Whalebird-4.5.0-linux-x64.deb
~~~

<br>
<br>
## Funcionamiento y valoración
<br>
Estéticamente, 'Whalebird' es un programa muy bonito, sobre todo cuando se le aplica un tema oscuro, aunque esto evidentemente va a gustos.  
La aplicación es **'electron'** y **consume bastantes más recursos de lo deseable** (comparado, por ejemplo, con el cliente web (desde navegador web)), porque -en este ordenador core2duo del 2007- se le nota cierta carencia de fluidez.  

<br>
<a href="/assets/img/2022-01-24-howto_whalebird_cliente_escritorio_para_el_fediverso_mastodon_misskey_pleroma_debian_y_derivados/whalebird_debian.png" target="_blank"><img src="/assets/img/2022-01-24-howto_whalebird_cliente_escritorio_para_el_fediverso_mastodon_misskey_pleroma_debian_y_derivados/whalebird_debian.png" alt="'Whalebird'" width="500"/></a>  
<small>'Whalebird'</small>  
<br>

El interfaz permite ocultar el menú del programa. Mas, sin embargo, ¡no he sido capaz de averiguar cómo -desde ese mismo interfaz gráfico- hacer que vuelva aparecer!.  
Para conseguir eso -<small>que vuelva a aparecer el menú del programa</small>- hay que editar el archivo '~/.config/Whalebird/storage/preferences.json':
~~~bash
nano ~/.config/Whalebird/storage/preferences.json
~~~
... y cambiar la entrada <span style="background-color:#042206"><span style="color:lime">`"menu":{"autoHideMenu":true}`</span></span> a <span style="background-color:#042206"><span style="color:lime">`"menu":{"autoHideMenu":false}`</span></span>  

<br>
Otros cliente del 'fediverso' -aunque sólo centrados en 'Mastodon'- son:
- ['Tootle'](https://apps.gnome.org/app/com.github.bleakgrey.tootle/) [<small>repositorio de 'Tootle' en GitHub</small>](https://github.com/bleakgrey/tootle). Para entorno gráfico, es decir, escritorio.
- ['toot'](https://github.com/ihabunek/toot). Para línea de comandos (CLI)  

Aún no los he probado. Apenas lo haga, en este blog se tratará.  

<br>
<br>
<br>
Entradas relacionadas:  
- ['Fediverso' de redes descentralizadas](https://hijosdeinit.gitlab.io/Fediverso_redes_descentralizada/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
