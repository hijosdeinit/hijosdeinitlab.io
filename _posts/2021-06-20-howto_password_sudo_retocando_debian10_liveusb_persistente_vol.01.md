---
layout: post
title: "Contraseña de 'sudo'. (retocando Debian 10 liveusb persistente, vol.01)"
date: 2021-06-20
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "Debian", "liveusb", "livecd", "livedvd", "persistente", "persistencia", "casper-rw", "casper", "dd", "iso", "boot", "sudo", "visudo", "contraseña", "password", "sudoers", "sudoers.d", "live", "root", "usuario", "grep", "find"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>La ejecución como superusuario de un comando entraña una gran "responsabilidad". Con 'sudo' se "invoca" al superusuario para ejecutar un comando que entrañe riesgo. Por ello es necesario introducir la contraseña del usuario previamente.  
Cuando se proporciona correctamente la contraseña es posible seguir ejecutando comandos mediante 'sudo' sin que pida contraseña durante un tiempo variable (y configurable mediante <span style="background-color:#042206"><span style="color:lime">`sudo visudo`</span></span>).  
Y esto es así en todas -o prácticamente todas- las distribuciones GNU Linux, con sus particularidades.</small>  

<br>
<small>Mas hay una </small>**excepción**<small>: los sistemas operativos GNU Linux de tipo</small> ***LIVE***<small>: *LiveCD*, *LiveDVD*, *LiveUSB*.</small>  
<small>Este tipo de sistemas operativos son prácticamente "universales" (funcionan en casi cualquier máquina de una arquitectura compatible), no requieren ser instalados y se ejecutan sin dejar huella en la máquina que los ejecute. Suelen ser muy útiles para probar un sistema operativo GNU Linux antes de instalarlo, como medio de instalación, y también como herramienta de emergencia o forense. No son sistemas ideados para trabajar de forma constante y habitual.  
En un Linux "live" el usuario, cuyo nombre y contraseña es genérico y por todos conocido, generalmente tiene privilegios de superusuario, pertenece al grupo 'sudoers' y, como tal, puede ejecutar comandos mediante 'sudo'. PERO, a diferencia de lo que ocurre en un sistema GNU Linux convencional, es decir, "instalado", la ejecución de 'sudo' por este usuario tan sumamente privilegiado **NO REQUIERE DE CONTRASEÑA**.  
Esto supone un fallo a nivel de seguridad que suelen heredar sus "hijos": los sistemas operativos GNU Linux **LIVE persistentes**.  
Un sistema operativo "Live persistente" está desplegado generalmente en un dispositivo de almacenamiento USB y, al igual que sus "padres" -livecd, livedvd o liveusb no persistentes-, pueden ser ejecutados en cualquier ordenador siempre que la arquitectura de éste le corresponda. Sin embargo, a diferencia de aquéllos, un sistema operativo GNU Linux "Live" persistente -dado que se halla desplegado en un dispositivo de almacenamiento "escribible" y "reescribible"- conserva el trabajo realizado por el usuario: sus archivos nuevos, sus configuraciones y reconfiguraciones, sus nuevos programas instalados, etcétera. Generalmente hay en el dispositivo una partición donde se "guarda" la persistencia.  
Un sistema operativo GNU Linux Live persistente -a diferencia de livecd, livedvd y liveusb no persistente- podría servir, no como sistema de emergencia, de prueba o de instalación, sino como sistema para trabajar habitualmente. Por ello es **altamente recomendable** anular o modificar ciertas características negativas heredadas de sus "progenitores" los livecd, livedvd y liveusb no persistentes.  
Y uno de estos defectos hereditarios es el hecho de que las sesiones de línea de comandos "pura" (*getty*) se abran con el usuario ya autentificado o la **carencia de contraseña 'sudo'**.</small>  

<br>
<br>
## ¿Cómo hacer que el sistema solicite contraseña antes de ejecutar 'sudo'?
<small>ENTORNO:
**liveusb persistente Debian 10 x386 LXDE** creado a partir de .iso 'debian-live-10.9.0-i386-lxde+nonfree.iso' siguiendo el [método que se detalla en esta entrada del blog](https://hijosdeinit.gitlab.io/howto_creacion_liveusb_persistente_debian_10/).</small>  

<br>
1. Prolegómeno 1: Póngasele contraseña al usuario root  
~~~bash
sudo passwd root
~~~

2. Prolegómeno 2: Asegúrese de [que el usuario pertenezca al grupo 'sudoers'](https://hijosdeinit.gitlab.io/howto_agregar_usuario_a_sudoers_Debian/).  
<span style="background-color:#042206"><span style="color:lime">`id `*miusuario*</span></span>  
y/ô  
<span style="background-color:#042206"><span style="color:lime">`groups `*miusuario*</span></span>  

3. Prolegómeno 3: Compruébese en qué estado está 'sudo'  
~~~bash
sudo --list
~~~

4. Edición / purga del archivo '/etc/sudoers'  
~~~bash
sudo cp /etc/sudoers /etc/sudoers.bkp001
sudo visudo
~~~
...'nano' abrirá el archivo '/etc/sudoers' y dentro de éste hay que buscar y extirpar el parámetro **'NOPASSWD'**  
De paso, compruébese que en el contenido exite algo parecido a este fragmento:
~~~
# User privilege specification
root    ALL=(ALL:ALL) ALL
user    ALL=(ALL:ALL) ALL
~~~

4. Búsqueda / purga en todo el sistema de la cadena de texto 'NOPASSWD'.  
Y esto es así porque en el liveusb persistente (Debian 10) que construí, a pesar de todo, 'sudo' seguía sin solicitar contraseña. Hasta que encontré el archivo 'etc/sudoers.d/live' que en su interior establecía `user ALL=(ALL) NOPASSWD: ALL` y pude anularlo comentándolo y dejándolo así `#user ALL=(ALL) NOPASSWD: ALL`.
~~~bash
sudo grep -r -i -l -s "NOPASSWD" /
~~~
Por consiguiente, si esa búsqueda **proporcionara resultados**, es decir, archivos que contienen la cadena de caracteres "NOPASSWD", edítense y -como seguro será necesario- quítese dicho parámetro ('NOPASSWD').  

5. Un reinicio nunca viene mal.  
~~~bash
sudo reboot
~~~

<br>
<br>

Entradas relacionadas:  
- [[HowTo] 'sudo' en Debian y derivados: agregar usuario a 'sudoers'](https://hijosdeinit.gitlab.io/howto_agregar_usuario_a_sudoers_Debian/)
- [Incorporar nuestro usuario a 'sudoers'](https://hijosdeinit.gitlab.io/howto_incorporar_usuario_a_sudoers/)
- [[HowTo] Búsquedas desde la línea de comandos GNU Linux](https://hijosdeinit.gitlab.io/howto_busquedas_CLI_gnulinux/)  

<br>
<br>
<br>

--- --- ---  
<small>Fuentes:  
<a href="https://unix.stackexchange.com/questions/8528/forcing-sudo-to-prompt-for-a-password">unix.stackexchange.com</a>  
<a href="https://www.enmimaquinafunciona.com/pregunta/41478/-el-sudo-no-pide-una-contrasena-incluso-despues-de-multiples-reinicios-">EnMiMaquinaFunciona</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
