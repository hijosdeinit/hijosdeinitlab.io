---
layout: post
title: "'msdos.club' (web y podcast): obsoletos pero orgullosos"
date: 2021-10-20
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "msdos", "dos", "windows", "emulacion", "dosbox", "dosemu", "virtualizacion", "vmware", "virtualbox", "boxes", "software", "retro", "retroinformatica", "retrocomputacion", "arcaico", "arqueologia"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Hace apenas 2 días el sr. 'Harckus' en el [canal de Telegram de 'Reality Cracking'](https://t.me/rcracking) nos descubrió uno de sus últimos hallazgos: una página web y un podcast llamados 'msDOS.club'. Se trataba de una especie de club de informática clásica cuyo lema es "obsoletos pero orgullosos".</small>  
> [web MSDOS club](https://msdos.club)  
[podcast MSDOS club](https://msdos.club/episodios/)  
[feed del podcast MSDOS club: https://msdos.club/podfeed/feed.xml](https://msdos.club/episodios/)  

<br>
'MS-DOS Club', según sus creadores, «es un espacio donde compartir la pasión que sentimos por un sistema y unos juegos que, aunque a día de hoy están obsoletos, nos APASIONAN».  
Es básicamente una web para compartir viejas y nuevas experiencias con el Sistema Operativo 'MS-DOS' y un podcast mensual hablando exclusivamente del sistema.  

<br>
<a href="/assets/img/2021-10-20-msdos.club_msdos_sigue_vivo/DOSCLBLOGO.jpg" target="_blank"><img src="/assets/img/2021-10-20-msdos.club_msdos_sigue_vivo/DOSCLBLOGO.jpg" alt="msdos.club'" width="500"/></a>  
<br>

Cualquiera que cumpla alguno de los siguientes puntos muy probablemente se sentirá atraído por los contenidos de 'MSDOS club':
- Te criaste jugando aventuras gráficas como 'Maniac Mansion', 'King Quest' o 'Monkey Island'.
- Intercambiabas diskettes con juegos en el patio del colegio.
- Fotocopiabas las claves anticopia de los juegos o te hacías las tuyas propias a mano.
- Jugabas a 'Wolfenstein 3D' o 'DOOM' sin utilizar el ratón para apuntar como en los FPS de hoy en día.
- Te encantaban los puzles de los 'Lemmings', 'Gobliins' o 'The incredible machine'.
- Hacías agujeritos a los diskettes de baja densidad para poder formatearlos en alta densidad y duplicar su tamaño útil.
- Te sentías en tu salsa como alcalde de 'SimCity'.
- Le dabas cera a la guerra entre Orcos y Humanos en 'Warcraft' o disfrutabas de las batallas en 'Dune II'.
- Disfrutabas de la exploración de mazmorras en los 'Eye of the Beholder', 'Ultima Underworld', 'Dark Heart of Uukrul' o 'Dungeon Master'.
- Eres capaz de redactar y maquetar un texto en WordPerfect sin necesidad de echar la mano al ratón.
- Piensas que cuando uno produce, las distracciones y los artificios son contraproducentes.
- Simplemente quieres aprender más cosas de este maravilloso sistema y sus juegos.  

<br>
Es un secreto a voces que MSDOS o sus derivados o *forks* como FreeDOS siguen vivos hoy en día. Cierto es también que no goza de la enorme cantidad de usuarios que tenía a principios de los 90 del siglo pasado, cuando para el PC no habían demasiadas alternativas.  

<br>
Los motivos que mueven a los usuarios de MSDOS y sistemas similares pueden ser diversos:
- Nostalgia
- *Gaming*: disfrutar de una experiencia plena y real al jugar a títulos viejos sin haber de recurrir a emulación.
- Aprovechamiento de computadoras viejas, muchas de las cuales hoy se cotizan a un precio mayor que el de muchos ordenadores modernos.
- Producción: "más nuevo" no necesariamente significa "mejor", y MSDOS tiene algunas aplicaciones que, aún hoy día, son más productivas que las actuales. Es conocido que, por ejemplo, muchos bufetes de abogados continúan utilizando WordPerfect 5.1 porque, sencillamente, es más productivo y les permite desempeñar más eficientemente su trabajo.  

<br>
No nos vayamos a pensar que en los 90 no se producía frente a un teclado y una pantalla, ni se escribían textos, ni se realizaban cálculos, ni se programaba, ni se daba rienda suelta a la creatividad, etćetera, etcétera...  
Es posible, por otro lado, que el mayor beneficio de que cada vez se requieran máquinas y sistemas más potentes para tareas tan simples como acceder a la información en la red o escribir un memorando no repercuta en el usuario, sino en las grandes empresas tecnológicas, que son las que fabrican esas máquinas, las que producen un goteo constante de sistemas operativos y aplicaciones que enseguida dejan obsoletos a sus precedentes, y las que requieren que las máquinas de los usuarios -¿usados?- cuya intimidad violan sistemáticamente sean cada vez más potentes precisamente para que la tarea de "ordeñar" sus datos sea cada vez más exhaustiva, rentable y lucrativa.  
Y no, negativo, esas reflexiones finales NO son fruto de la creencia en que "cualquier tiempo pasado fue mejor", mas... ¿se acuerdan de aquella expresión castellana que dice algo así como "ser puta y pagar la cama"? Pues eso, que el término "progreso" es muy elástico.  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Creación de liveUSB persistente FreeDOS](https://hijosdeinit.gitlab.io/howto_creacion_liveusb_persistente_FreeDos/)
- [Protocolo 'GEMINI': retornando a la senda de 'Gopher' en pro de una navegación sin aditivos](https://hijosdeinit.gitlab.io/protocolo_Gemini_retornando_al_camino_Gopher_solucionando_deficiencias_web/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
