---
layout: post
title: "En nano no existe overtyping"
date: 2020-12-26
author: Termita
category: "productividad"
tags: ["nano", "editor de texto", "vim", "vi", "emacs", "texto", "programación", "texto plano", "latex", "herramientas", "código", "markdown", "org mode", "pandoc", "overtyping", "modo inserción", "insert", "sobreescritura"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
No existe *overtyping* en nano, mi editor de texto favorito.  
Y es una lástima porque sería una característica muy útil que desconozco por qué no está implementada.  

<br>
<big>**El *overtyping* es el *modo inserción*, *insert mode*, *modo sobreescritura*, que permite que lo que escribamos sustituya a lo que ya hay escrito.**</big>  

<br>
Hay quienes dicen que el *overtyping* es innecesario, y otros que es algo negativo. Quizás, aún yendo nano por su versión 4, a esto se deba que no haya sido implementada.  
Quizás me saldría a cuenta aprender vi/vim.  

<br>
<small>[En esta entrada de este blog se señala otra de las **carencias** de 'nano': la no aceptación de algunos metacaracteres de las secuencias de escape o expresiones regulares](https://hijosdeinit.gitlab.io/regexp_no_aceptadas_por_nano/)</small>  

<br>
<br>
<br>
Entradas relacionadas:  
- [Expresiones regulares no aceptadas por el editor de texto 'nano'](https://hijosdeinit.gitlab.io/regexp_no_aceptadas_por_nano/)
- [[HowTo] Mostrar el número de línea en el editor 'nano' cuando Alt+Shift+3 no funciona](https://hijosdeinit.gitlab.io/nano__mostrar_numeros_linea_cuando_altshift3_nofunciona/)
- [[HowTo] Atajos de teclado del editor 'nano'](https://hijosdeinit.gitlab.io/howto_atajos_teclado_nano/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Añadir resaltado de sintaxis al editor de textos NANO](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [[HowTo] nano: parámetros de arranque útiles](https://hijosdeinit.gitlab.io/howto_parametros_utiles_arranque_nano/)
- [[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados](https://hijosdeinit.gitlab.io/howto_micro_editor_texto_debian/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)  

<br>
<br>
<br>
<br>
