---
layout: post
title: '[HowTo] Descargar subtítulos de Youtube, nativos y/o autogenerados en el idioma que queramos'
date: 2020-10-09
author: Termita
category: "descargas"
tags: ["sistemas operativos", "youtube-dl", "subtitulos", "idioma", "descargas", "multimedia", "subs", "youtube", "degoogled"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Un video de Youtube puede tener subtítulos "nativos" -los que su creador incorporó al subir el video- o "autogenerados".  
Los subtítulos "nativos" son una transcripción exacta, se corresponden perfectamente con la locución, no tienen faltas de ortografía, ni de sintaxis. No obstante, no siempre están en el idioma que a uno le interesa.  
Los subtítulos "autogenerados" son generados de forma automática por Youtube y, si uno lo desea, se pueden obtener prácticamente en el idioma que uno quiera (traducción automática). No obstante, no son una transcripción exacta de las locuciones y tienen alguna que otra incorrección sintáctica.  
[Youtube-dl]()
Puede, por consiguiente, interesarnos descargar los subtítulos autogenerados, por ejemplo, cuando:
- los subtítulos "nativos" no están el idioma que nos interese.
- no existen subtítulos "nativos".

<br>
<br>
## Comprobar si un vídeo tiene subtítulos "nativos"
~~~bash
youtube-dl --list-subs urldelvideo
~~~
Por ejemplo:
~~~bash
youtube-dl --list-subs https://www.youtube.com/watch?v=r9NS6Uq9-zk
~~~
nos responderá que, en este ejemplo, sí hay subtítulos nativos (en y fr):
><small>Available subtitles for r9NS6Uq9-zk:  
Language formats  
en       vtt, ttml, srv3, srv2, srv1  
fr       vtt, ttml, srv3, srv2, srv1</small>  

<br>
<br>
## Descargar los subtítulos nativos de un vídeo
~~~bash
youtube-dl --write-sub --sub-lang códigoidioma urldelvideo
~~~
Por ejemplo, descargaremos los subtítulos nativos en inglés (en), uno de los dos idiomas disponibles de forma nativa, pero no descargaremos el video (`--skip-download`):
~~~bash
youtube-dl --write-sub --sub-lang en --skip-download  https://www.youtube.com/watch?v=r9NS6Uq9-zk
~~~


<br>
## Descargar los subtítulos autogenerados de un vídeo
(en el idioma que nos dé la gana y aunque no existan subtítulos nativos)  
~~~bash
youtube-dl --write-auto-sub --sub-lang códigoidioma urldelvideo
~~~
Por ejemplo, descargaremos los subtítulos autogenerados en español (es), pero no descargaremos el video(`--skip-download`):
~~~bash
youtube-dl --write-auto-sub --sub-lang es --skip-download https://www.youtube.com/watch?v=r9NS6Uq9-zk
~~~

<br>
<br>
## Extra: google2srt
Hay una aplicación -para Windows- llamada ['Google2Srt'](https://sourceforge.net/projects/google2srt/) que es un interfaz visual para las operaciones descritas.
<br>
<br>
<br>
<br>
<br>
