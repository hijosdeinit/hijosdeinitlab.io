---
layout: post
title: "Listado de editores de código / IDE interesantes"
date: 2021-01-18
author: Termita
category: "sistemas operativos"
tags: ["ide", "editor", "código", "texto", "texto plano", "programación", "sistemas operativos", "gnu linux", "linux", "vi", "vim", "nano", "emacs", "atom", "eclipse", "sublime text", "visual studio code", "vscodium", "codium", "vscode", "netbeans", "micro", "kakoune", "idle", "pycharm", "geany", "cudatext"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Estos son, de los que he tenido conocimiento, los que me parecen interesantes:  

<br>
## [Vi](http://ex-vi.cvs.sourceforge.net/ex-vi/ex-vi/)
... y sus "derivados", de los cuales he oído hablar de:
- [**'vim'**](https://vim.org)
- [neoVim](https://neovim.io/)
- [gvim]()
- [spacevim](https://spacevim.org)  

<br>
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/spaceVIM.png" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/spaceVIM.png" alt="'spaceVIM'" width="700"/></a>  
<small>'spaceVIM'</small>  
<br>

<br>
## [eMacs](https://www.gnu.org/software/emacs/)  
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/emacs_arch.png" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/emacs_arch.png" alt="'eMacs'" width="700"/></a>  
<small>'eMacs'</small>  
<br>

<br>
## [joe](https://joe-editor.sourceforge.io/)
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/joe.png" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/joe.png" alt="'joe' editor" width="700"/></a>  
<small>'joe' editor</small>  
<br>

<br>
## [amp](https://amp.rs/)
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/amp_editor.png" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/amp_editor.png" alt="'amp' editor" width="700"/></a>  
<small>'amp' editor</small>  
<br>

<br>
## [geany](https://www.geany.org)
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/arc-dark-geany.png" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/arc-dark-geany.png" alt="'geany'" width="700"/></a>  
<small>'geany'</small>  
<br>

<br>
## [Atom](https://atom.io/)
<small>Tratado en [este hilo de este blog](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/). **DESCONTINUADO en diciembre de 2022**</small>  
<br>
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/atom.webp" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/atom.webp" alt="'Atom'" width="700"/></a>  
<small>'Atom'</small>  
<br>

<br>
## [Pulsar](https://pulsar-edit.dev/)
<small>Es la continuación del desarrollo del editor 'atom', tras ser éste abandonado por GitHub tras ser comprado por Microsoft. </small>  
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/pulsar.png" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/pulsar.png" alt="'Pulsar'" width="700"/></a>  
<small>'Pulsar'</small>  
<br> 

<br>
## [Eclipse](https://www.eclipse.org/)  
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/eclipse.webp" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/eclipse.webp" alt="'Eclipse'" width="700"/></a>  
<small>'Eclipse'</small>  
<br>

<br>
## [Sublime Text](https://www.sublimetext.com/)  
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/sublimetext.webp" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/sublimetext.webp" alt="'Sublime Text'" width="700"/></a>  
<small>'Sublime Text'</small>  
<br>

<br>
## [VSCodium (Visual Studio Code sin telemetría)](https://vscodium.com/)  
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/vscodium.png" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/vscodium.png" alt="'VSCodium" width="700"/></a>  
<small>'VSCodium'</small>  
<br>

<br>
## [Visual Studio Code](https://code.visualstudio.com/)  
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/visual-studio-code.png" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/visual-studio-code.png" alt="'Visual Studio Code'" width="700"/></a>  
<small>'Visual Studio Code'</small>  
<br>

<br>
## [NetBeans](https://netbeans.apache.org/)  
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/netbeans.png" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/netbeans.png" alt="Apache 'NetBeans'" width="700"/></a>  
<small>Apache 'NetBeans'</small>  
<br>

<br>
## ['Brackets'](https://brackets.io/index.html)
<small>Tratado en [este hilo de este blog](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)</small>  
<br>
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/brackets-ui.webp" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/brackets-ui.webp" alt="'Brackets'" width="700"/></a>  
<small>'Brackets'</small>  
<br>

<br>
## ['Light Table'](http://lighttable.com/)  
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/Light-Table.jpg" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/Light-Table.jpg" alt="'Light Table'" width="700"/></a>  
<small>'Light Table'</small>  
<br>

<br>
## [CudaText](https://cudatext.github.io/index.html)
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/cudatext-dark.png" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/cudatext-dark.png" alt="'CudaText'" width="700"/></a>  
<small>'CudaText'</small>  
<br>

<br>
## [TextAdept](https://orbitalquark.github.io/textadept/index.html)
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/textadept.png" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/textadept.png" alt="'TextAdept'" width="700"/></a>  
<small>TextAdept</small>  
<br>

<br>
## [Tilde](https://os.ghalkes.nl/tilde)
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/tilde.png" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/tilde.png" alt="'Tilde'" width="700"/></a>  
<small>'Tilde'</small>  
<br>

<br>
## [PyCharm](https://www.jetbrains.com/es-es/pycharm/)
<small>Es software propietario.</small>  
<a href="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/pycharm.jpg" target="_blank"><img src="/assets/img/2021-01-18-listado_IDE_o_editores_de_codigo/pycharm.jpg" alt="'PyCharm'" width="700"/></a>  
<small>'PyCharm'</small>  
<br>

<br>
<br>
## Anexo: Otros editores, que no específicamente de código, interesantes
- [nano](https://www.nano-editor.org/)
- [micro](https://micro-editor.github.io/)
- [ghostwriter](https://wereturtle.github.io/ghostwriter/)
- [wordgrinder](http://cowlark.com/wordgrinder/)
- [utext](https://atareao.es/aplicacion/utext-un-editor-de-texto-markdown-para-ubuntu/)
- [kakoune](https://kakoune.org/)
- (apps de NextCloud:) 'Text', 'Plain Text Editor' y 'MarkDown Editor'
- [WordPerfect](https://github.com/taviso/wpunix/)  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] El editor (IDE) atom y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [[HowTo] Evitar que 'atom' elimine espacios en blanco al final de línea](https://hijosdeinit.gitlab.io/howto_atom_espacios_blanco_final_linea/)
- [[HowTo] Agregar idiomas al editor (IDE) atom. Ponerlo en español](https://hijosdeinit.gitlab.io/howto_poner_atom_en_espa%C3%B1ol/)
- [[HowTo] El editor (IDE) 'geany' en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_geany_en_debian_y_derivados/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- [[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)
- [[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados](https://hijosdeinit.gitlab.io/howto_micro_editor_texto_debian/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [[HowTo] Añadir resaltado de sintaxis al editor de textos NANO](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [[HowTo] nano: parámetros de arranque útiles](https://hijosdeinit.gitlab.io/howto_parametros_utiles_arranque_nano/)
- [En nano no existe overtyping](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/)
- [[HowTo] Comienzo con vim. I](https://hijosdeinit.gitlab.io/howto_comienzo_con_vim_1/)
- [[HowTo] Visualización de 'markdown' en la línea de comandos (CLI): 'MDLESS'](https://hijosdeinit.gitlab.io/howto_mdless_visor_markdown_cli/)
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
