---
layout: post
title: "[HowTo] Actualización del firmware de un SSD Toshiba / Kioxia"
date: 2022-01-01
author: Termita
category: "hardware"
tags: ["hardware", "firmware", "actualizacion", "bug", "gnu linux", "linux", "fedora", "windows", "ssd", "hdd", "toshiba", "kioxia", "live usb", "dmg"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
El soporte de los SSD Toshiba corre ahora a cargo de ['Kioxia'](https://personal.kioxia.com/es-es/support/legacy-products.html).  
Este es el caso de mi SSD Toshiba TR200.  

<br>
<a href="/assets/img/2022-01-01-howto-actualizacion_firmware_ssd_toshiba_kioxia/tr200-ssd.jpg" target="_blank"><img src="/assets/img/2022-01-01-howto-actualizacion_firmware_ssd_toshiba_kioxia/tr200-ssd.jpg" alt="SSD Toshiba TR200" width="300"/></a>  
<small>SSD Toshiba TR200</small>  
<br>

Cuando se detectan fallos o aspectos susceptibles de mejorar, los principales fabricantes lanzan actualizaciones. En el caso de, por ejemplo, los dispositivos SSD (*discos de estado sólido*) o de los HDD (discos duros convencionales, mecánicos) esto se hace **actualizando el firmware**.  

<br>
## ¿Cómo actualizar el firmware de un SSD Toshiba / Kioxia?
Veo que para el SSD Toshiba TR200, que de fábrica trae el firmware **SBFA13.3**, hay una actualización de firmware del 15 de marzo de 2021: v12.a, v13.6, v15.5, v16.5, v17.5.  
> **Version 13.6** (13.1, 13.3, 13.4 --> 13.6)   March 15, 2021 Important: For TR200 with v13.1, 13.3 and 13.4 firmware update aims to address critical bugs and/or improve the reliability and performance of your SSD. It is strongly recommended that you update your SSD. Improvements Fixes bug.  

'Kioxia' llama a los productos "heredados" de Toshiba 'Legacy products'. He [aquí, en su página oficial](https://personal.kioxia.com/es-es/software/legacy-ssd-utility.html), un listado de este hardware, entre el cual está mi SSD Toshiba TR200.  
<br>
Para actualizar el firmware a un SSD Toshiba / Kioxia hay 2 opciones:
1. Descargar e instalar ***Legacy SSD Utility***, un programa para Windows y/o GNU Linux (<small>la versión para GNU Linux está bastante anticuada y es probable que no funcione con los últimos 'legacy products' de Toshiba / Kioxia</small>) que sirve para actualizar el SSD **siempre y cuando** éste no sea el que alberga el sistema operativo que se está ejecutando.
2. Descargar, descomprimir y "quemar" como liveusb un sistema operativo arrancable y basado en GNU Linux (Fedora) que contiene la aplicación ***Legacy SSD Utility*** que, tras arrancar el ordenador desde él, servirá para actualizar el firmware del SSD.  

En el caso de esa 2ª opción, que es la que yo he probado, tras descomprimir el archivo descargado, para quemar el archivo .dmg resultante basta con emplear el comando 'dd' o 'gnome-disks' o 'Balena Etcher'.  

<br>
<a href="/assets/img/2022-01-01-howto-actualizacion_firmware_ssd_toshiba_kioxia/0.png" target="_blank"><img src="/assets/img/2022-01-01-howto-actualizacion_firmware_ssd_toshiba_kioxia/0.png" alt="Firmware actualizado: SBFA13.6" width="600"/></a>  
<small>Firmware actualizado: **SBFA13.6**</small>  
<br>

**!** Para realizar la actualización del firmware es necesario tener conectividad con internet.  
<br>
A día de hoy la versión de *Legacy SSD Utility* más actualizada y compatible con el SSD Toshiba TR200 es la versión 5.3.0004.  
Se puede descargar desde [aquí](https://personal.kioxia.com/es-es/software/legacy-ssd-utility.html) y sirve, no sólo para actualizar el firmware, sino también para mantenimiento, ajustes o comprobación de la salud de la unidad SSD.

<br>
<a href="/assets/img/2022-01-01-howto-actualizacion_firmware_ssd_toshiba_kioxia/versiones_ssd_utility_kioxia_toshiba.png" target="_blank"><img src="/assets/img/2022-01-01-howto-actualizacion_firmware_ssd_toshiba_kioxia/versiones_ssd_utility_kioxia_toshiba.png" alt="Listado de versiones y compatibilidad de Kioxia Legacy SSD Utility" width="700"/></a>  
<small>Listado de versiones y compatibilidad de Kioxia Legacy SSD Utility</small>  
<br>

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>

--- --- ---
<small>
Fuentes:  
[kioxia - support - legacy products](https://personal.kioxia.com/es-es/support/legacy-products.html)  
[kioxia - legacy ssd utility](https://personal.kioxia.com/es-es/software/legacy-ssd-utility.html)  
[kioxia - ssd utility](https://personal.kioxia.com/es-es/software/ssd-utility.html)  
</small>

<br>
<br>
<br>
