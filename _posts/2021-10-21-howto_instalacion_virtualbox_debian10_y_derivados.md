---
layout: post
title: "[HowTo] Instalación de 'Oracle VirtualBox' 6.1 64bits en Debian 10 y derivados"
date: 2021-10-21
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "maquinas virtuales", "virtual", "vm", "disco duro virtual", "almacenamiento virtual", "virtualbox", "vmware workstation", "vmware player", "qemu", "gnome boxes", "vmware esxxi", "vmware esx", "proxmox ve", "parallels desktop", "xen", "docker", "kubernetes", "podman", "wine", "dosbox", "scummvm", "emulacion", "emulador", "gnu linux", "linux", "debian", "buster", "windows", "windows 10", "virtualizacion"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno: cpu Core2Duo, 6gb RAM, gpu gforce 7600gt, GNU Linux Debian 10 Buster (netinstall) stable, kernel 4.19.0-18-amd64, gnome</small>  

<br>
En este momento, en la [sección oficial de descarga](https://download.virtualbox.org/virtualbox/)la versión más reciente apropiada para este sistema es ['Virtual Box'](https://www.virtualbox.org/) 6.1.  
Añado repositorio de 'Oracle Virtual Box' y las claves de autentificación para éste.
~~~bash
sudo apt-get update
wget -q https://www.virtualbox.org/download/oracle_vbox_2016.asc -O- | sudo apt-key add -
wget -q https://www.virtualbox.org/download/oracle_vbox.asc -O- | sudo apt-key add -
sudo apt install software-properties-common
sudo add-apt-repository "deb [arch=amd64] http://download.virtualbox.org/virtualbox/debian $(lsb_release -cs) contrib"
~~~
<br>
Instalo 'VirtualBox'
~~~bash
sudo apt-get update
sudo apt install virtualbox-6.1
~~~
<br>
Descargo e instalo, asímismo, 'guest additions' apropiadas
~~~bash
wget https://download.virtualbox.org/virtualbox/6.1.28/Oracle_VM_VirtualBox_Extension_Pack-6.1.28.vbox-extpack
sudo VBoxManage extpack install Oracle_VM_VirtualBox_Extension_Pack-6.1.28.vbox-extpack
~~~
... y las incorporo a VirtualBox abriéndolas con dicho programa.  

<br>
**i** Cuando creemos una máquina virtual es muy recomendable agregarle las extensiones de Virtual Box, generalmente montando en el cd "virtual" las *guest additions*  

<br>
<br>
Entradas relacionadas:  
- [Software de virtualización (máquinas virtuales)](https://hijosdeinit.gitlab.io/utilidades_maquinas_virtuales/)
- [[HowTo] Aumentar el tamaño del disco duro virtual de una máquina virtual de Virtual Box](https://hijosdeinit.gitlab.io/howto_aumentar_tama%C3%B1o_discodurovirtualdinamico_maquinavirtual_virtualbox/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://linuxize.com/post/how-to-install-virtualbox-on-debian-10/">Linuxize</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
