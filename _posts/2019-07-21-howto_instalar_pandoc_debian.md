---
layout: post
title: "[HowTo] Instalación de pandoc en Debian y/o derivados"
date: 2019-07-21
author: Termita
category: "sistemas operativos"
tags: ["pandoc", "pdf", "html", "txt", "web", "productividad", "offline", "sistemas operativos", "gnu linux", "linux", "wkhtmltopdf", "latex", "debian", "ubuntu", "odt", "tex", "CLI", "convertir", "exportar"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<big>[Pandoc](https://pandoc.org/)</big> es un conversor de texto a diversos formatos (pdf, odt, tex, etc...)  
<br>
Para instalar la última versión de 'Pandoc' en Debian se puede descargar el .deb desde [su repositorio oficial ubicado en Github](https://github.com/jgm/pandoc/releases).  
Luego bastará con ejecutar desde línea de comandos:
~~~bash
dpkg -i archivodescargado.deb 
~~~

<br>
<br>
Otras entradas de este blog que tratan de 'pandoc' y/o la conversión de contenidos web a .pdf u otros formatos son:
- [[HowTo] guardar una página web en formato .pdf mediante wkhtmltopdf](https://hijosdeinit.gitlab.io/howto_web_a_pdf_wkhmltopdf/)
- [[HowTo] guardar una página web en formato .pdf mediante pandoc](https://hijosdeinit.gitlab.io/howto_web_a_pdf_pandoc/)
- [[HowTo] Conversión básica de textos y/o web mediante pandoc](https://hijosdeinit.gitlab.io/howto_conversion_basica_textos_y_web_mediante_pandoc/)  

<br>
<br>
<br>
--- --- ---
<small>Fuentes:
<a href="https://github.com/jgm/pandoc/releases">Repositorio oficial Pandoc (releases)</a>  
<a href="https://pandoc.org/">Pandoc.org</a>  
<a href="https://es.wikibooks.org/wiki/Manual_de_LaTeX/Otros/Exportar_a_otros_formatos">WikiBooks</a>  
<a href="https://es.wikipedia.org/wiki/LaTeX">Wikipedia - Latex</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
