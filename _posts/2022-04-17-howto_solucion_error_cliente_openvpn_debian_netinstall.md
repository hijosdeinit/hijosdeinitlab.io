---
layout: post
title: "[HowTo] Solución error '.ovpn could not be read or does not contain recognized VPN connection information' en cliente openvpn en Debian 10 (*netinstalled*) y derivados"
date: 2022-04-17
author: Termita
category: "redes"
tags: ["redes", "sistemas operativos", "error", "problema", "openvpn", "cliente", "ovpn", "certificado", "vpn", "pivpn"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small><small>Entorno: máquina virtual ('Virtual Box') Debian 10 *netinstall*, Gnome, i3wm.</small></small>  

<br>
<br>
Una instalación mínima -*netinstall*- de Debian 10 no "trae" por defecto algunos componentes como, por ejemplo, el cliente 'vpn' / 'openvpn'.  
Y esto es así aunque durante la instalación hayamos incorporado el entorno 'Gnome', que de serie instala algunos paquetes y utilidades como por ejemplo el gestor de redes 'network-manager', o el gestor de discos 'gnome-disks'.  

<br>
No obstante, incorporar soporte 'vpn' a Debian' es sencillo.  
Debería bastar con:
~~~bash
sudo apt-get update
sudo apt install openvpn
~~~
... Y seguidamente, por ejemplo, mediante el gestor de conexiones de 'network-manager' (para 'i3wm' es muy útil 'nm-applet') o directamente desde el terminal (`sudo openvpn --config /ruta/archivoovpn.ovpn`) podríamos establecer conexiones 'vpn'.  

<br>
Y, como dije, debería bastar con eso; mas es posible que **al tratar de configurar / establecer una conexión 'vpn' importando un perfil mediante un archivo '.ovpn'** nos encontremos este **error**:
> **'-----.ovpn' could not be read or does not contain recognized VPN connection information. Error: unknown PPTP file extension**  

Esto es debido a que falta algún componente, como por ejemplo: **'network-manager-openvpn-gnome'**. Solución: instalarlo
~~~bash
sudo apt-get update
sudo apt install network-manager-openvpn-gnome
sudo service network-manager restart
~~~
En mi caso, desconozco el por qué, hube de reiniciar.  

<br>
<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
