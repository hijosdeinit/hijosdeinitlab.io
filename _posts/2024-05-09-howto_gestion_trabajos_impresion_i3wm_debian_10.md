---
layout: post
title: "[HowTo] Gestión de trabajos de impresión desde i3wm en Debian 10 (Gnome)"
date: 2024-05-09
author: Termita
category: "sistemas operativos"
tags: ["debian", "i3wm", "i3status", "i3lock", "dmenu", "window manager", "sistemas operativos", "gnu linux", "linux", "escritorio", "interfaz grafico", "desktop", "gui", "gnome", "impresoras", "printers", "cups", "system-config-printer", "trabajos de impresion", "print jobs"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Uno de los problemas con que me suelo encontrar cuando utilizo el gestor de ventanas ['i3wm'](https://i3wm.org) es el desconocimiento del comando que lanza algunas aplicaciones, por ejemplo las que trae consigo 'Gnome'.  
<span style="background-color:#042206"><span style="color:lime">`gnome-disks`</span></span>, o <span style="background-color:#042206"><span style="color:lime">`nautilus`</span></span>, que, respectivamente, lanzan el administrador de discos de Gnome, o el explorador de archivos 'Nautilus', son algunos de esos comandos. En 'i3wm' averiguarlos la primera vez puede costar y, es posible, que esto le pase a más gente.  

<br>
Hoy quise consultar la **cola de impresión** de mi impresora desde una computadora *Core2Duo* con Debian 10 *Buster* cuyo entorno 'Gnome', en su día, acompañé a posteriori de 'i3wm'.  
En mi entorno hay 2 formas de hacer esto:
- mediante `system-config-printer`
- mediante `cups`  

<br>
<br>
## Mediante `system-config-printer`
El comando / nombre de la aplicación de 'Gnome' que sirve para gestionar gráficamente la impresión es: <big>[`system-config-printer`](https://github.com/OpenPrinting/system-config-printer)</big>. Por consiguiente el procedimiento para lanzarlo en 'i3wm' es:
- Abrir 'dmenú' pulsando la combinación de teclas <span style="background-color:#042206"><span style="color:lime">**Ctrl**</span></span> + <span style="background-color:#042206"><span style="color:lime">**d**</span></span>
- y, a continuación, escribir <span style="background-color:#042206"><span style="color:lime">**`system-config-printer`**</span></span> y pulsar <span style="background-color:#042206"><span style="color:lime">**enter**</span></span>  

<br>
<br>
## Mediante CUPS
Otra forma, más "universal" es hacer uso de <big>['cups'](https://openprinting.github.io/cups/)</big>. El procedimiento:
- Ábrase un navegador web, Mozilla Firefox por ejemplo
- navéguese a la *url* <span style="background-color:#042206"><span style="color:lime">**`http://localhost:631/jobs`**</span></span> (o, en el caso de que se desee consultar o gestionar la impresión remotamente: <span style="background-color:#042206"><span style="color:lime">**`http://ipdelamáquina:631/jobs`**</span></span>)  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Configuración de 'i3wm', vol.02: los nombres de las teclas](https://hijosdeinit.gitlab.io/howto_configuracion_i3wm_vol02__nombres_teclas/)
- [[HowTo] Configuración de 'i3wm', vol.01: Cambiar el fondo de pantalla (wallpaper) en OpenBox, i3wm, etc... sin software extra, bajo Debian y derivados](https://hijosdeinit.gitlab.io/howto_cambiar_fondopantalla_Debian_openbox_i3wm/)  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  

<br>
<br>
<br>
<br>
<br>
<br>
