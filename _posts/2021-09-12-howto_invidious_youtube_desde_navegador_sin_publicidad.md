---
layout: post
title: "[HowTo] 'invidious', 'Youtube' desde el navegador sin publicidad ni rastreo"
date: 2021-09-12
author: Termita
category: "internet 4.0"
tags: ["invidious", "yewtu.be", "subtítulos", "web 4.0", "descargar", "descargas", "audio", "video", "multimedia", "youtube-dl", "youtube", "rumble", "streaming", "codigo", "privacidad", "tracking", "rastreo", "anonimato", "offline", "autosuficiencia", "freetube", "newpipe", "frontend"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>En septiembre de 2020 dejó de funcionar la página web ['INVIDIO.US'](https://invidio.us), que permitía visualizar, descargar y un montón de cosas más, los videos de 'Youtube', sin que existiera *tracking*, cuantificación o monetización del usuario. Era una especie de *front-end* para 'Youtube' respetuoso con la privacidad, sin publicidad y con prestaciones "extras".</small>  

<br>
Se publicó entonces [<big>**'INVIDIOUS'**</big>](https://invidious.io/), el software que funcionaba en aquel "desaparecido" servidor 'invidio.us', una aplicación web de código abierto lista para instalar en cualquier servidor. Lejos de desaparecer, el sistema se había descentralizado.  
Han surgido así multitud "instancias", es decir, una miríada de servidores 'invidious' por todo el planeta -<small>¿a qué esperas para montar el tuyo propio?</small>- que hoy permiten a cualquier internauta acceder a los contenidos de 'YouTube' salvaguardando su privacidad y aportándole prestaciones.  

<br>
<a href="/assets/img/2021-09-12-howto_invidious_youtube_desde_navegador_sin_publicidad/invidious.png" target="_blank"><img src="/assets/img/2021-09-12-howto_invidious_youtube_desde_navegador_sin_publicidad/invidious.png" alt="'invidious'" width="500"/></a>  
<small>proyecto invidious</small>  
<br>

<br>
## Características de 'invidious'
- autohospedado (<small>si no se desea utilizar una de las instancias públicas</small>)
- ligero
- de código abierto
- instalable en cualquier servidor particular, descentralizado
- permite acceder a contenidos de 'YouTube' en lugares donde éste está bloqueado
- acceso sin publicidad a contenidos de 'Youtube'
- sin monetización
- sin rastreo ni cuantificación
- permibe búsquedas, suscripciones, notificaciones, etcétera, sin que a 'Google' -<small>propietario de 'Youtube'</small>- le conste
- permite importación / exportación de suscripciones de 'Youtube', ['NewPipe'](https://hijosdeinit.gitlab.io/newpipe-contenidos-de-youtube-como-si/) y ['FreeTube'](https://hijosdeinit.gitlab.io/howto_freetube_youtube_sin_publicidad/)
- permite importación del historial de visualizaciones de 'NewPipe'
- permite importación / exportación de los datos de usuario de 'invidious'
- permite embeber videos en la web
- permite descargar los contenidos de 'Youtube' (<small>en 'Youtube', por el contrario, esa sencilla "ventaja" es de pago</small>)
- permite modo "sólo audio" (<small>reproducción exclusivamente del sonido, prescindiendo del video. Esto redunda, p.ej. en ahorro de ancho de banda</small>).
- (<small>dispositivos móviles</small>) permite reproducir los videos de 'Youtube' con la pantalla apagada
- no requiere *javascript* activado
- temas visuales (oscuro, claro, etc)
- página de inicio personalizable
- soporte de comentarios en 'ReddIt'
- multilenguaje
- existen extensiones para el navegador Mozilla Firefox -['invidition'](https://addons.mozilla.org/es/firefox/addon/invidition/)- que redirigen todos los enlaces de 'Youtube' a una instancia de 'invidious'
- su [API de desarrollo está disponible](https://docs.invidious.io/API.md)
- no utiliza las APIs oficiales de 'Youtube'
- no *'Contributor License Agreement (CLA)'*

<br>
<br>
## ¿Cómo se puede utilizar 'invidious'?
'Invidious' es a 'Youtube' lo que ['searX'](https://hijosdeinit.gitlab.io/howto_searx_metabuscador/) es a los buscadores. Puede utilizarse de 2 formas:
A) Utilizando una de las muchas instancias de 'invidious' existentes en el mundo, sin más complicación.  
B) Instalándolo en tu propio servidor (crear tu propia y particular instancia).  

<br>
### ¬<big>A</big>. Utilizando una de las instancias públicas de 'invidious'
<a href="/assets/img/2021-09-12-howto_invidious_youtube_desde_navegador_sin_publicidad/instancia_invidious_ajena_yewtube.png" target="_blank"><img src="/assets/img/2021-09-12-howto_invidious_youtube_desde_navegador_sin_publicidad/instancia_invidious_ajena_yewtube.png" alt="visualización de un video de 'Youtube' desde 'yewtu.be', una de las muchas instancias públicas de 'invidious'" width="800"/></a>  
<small>visualización de un video de 'Youtube' desde 'yewtu.be', una de las muchas instancias públicas de 'invidious'</small>  

#### A.1 Manualmente desde el navegador
Basta con acceder desde el navegador a cualquiera de las instancias públicas de 'invidious' existentes.  
He aquí una [lista de las instancias de invidious que hay en la red](https://github.com/iv-org/documentation/blob/master/Invidious-Instances.md).  
[Otra lista de las instancias de invidious que hay en la red](https://redirect.invidious.io/).  
Mi instancia pública de 'invidious' preferida es [yewtu.be](https://yewtu.be)  
<br>
#### A.2 Automáticamente desde el navegador 'Mozilla Firefox'
Esto se logra mediante la ya mencionada extensión ['invidition'](https://addons.mozilla.org/es/firefox/addon/invidition/), que redirige todos los contenidos de 'Youtube' a la instancia de 'invidious' que hayamos especificado.  
<br>

<br>
### ¬<big>B</big>. Instalando 'invidious' en tu propio servidor (*hosting*)
El software para crear tu propia instancia de 'invidious' en tu servidor está publicado en [su repositorio de GitHub](https://github.com/iv-org/invidious).  
La documentación de 'invidious' está publicada también en el [apartado correspondiente de su repositorio de GitHub](https://github.com/iv-org/documentation), <small>el sitio que recomiendo para empezar</small>.  
Los **procedimientos** de instalación de 'invidious' están detallados en el [subapartado 'installation" del apartado "documentación" de su repositorio en GitHub](https://github.com/iv-org/documentation/blob/master/Installation.md), que además tiene un *mirror* / réplica / espejo en [su página oficial de documentación](https://docs.invidious.io/Installation.md).  
(i) Tengo entendido que el software de 'invidious' está preparado para ser instalado en servidores corriendo en máquinas x86. Esto significa que AÚN no funcionará en arquitecturas 'ARM' como es RaspBerry Pi.  
Existen varias formas de instalar 'invidious' en un servidor particular.  
- B1. [Mediante script de instalación / actualización](https://github.com/iv-org/documentation/blob/master/Installation.md#automated-installation)  
- B2. [Mediante *'docker'*](https://github.com/iv-org/documentation/blob/master/Installation.md#docker)  
- B3. [De forma **nativa**](https://github.com/iv-org/documentation/blob/master/Installation.md#manual-installation)  

<br>
#### B3. Instalar 'invidious' de forma **nativa** en un servidor Debian
No lo he llevado a cabo aún, mas el procedimiento -según la documentación oficial- es el siguiente:
~~~bash
sudo apt-get update
curl -fsSL https://crystal-lang.org/install.sh | sudo bash
sudo apt install libssl-dev libxml2-dev libyaml-dev libgmp-dev libreadline-dev postgresql librsvg2-bin libsqlite3-dev zlib1g-dev libpcre3-dev libevent-dev
useradd -m invidious
sudo -i -u invidious
git clone https://github.com/iv-org/invidious
exit
sudo systemctl enable --now postgresql
sudo -i -u postgres
psql -c "CREATE USER kemal WITH PASSWORD 'kemal';" # Change 'kemal' here to a stronger password, and update `password` in config/config.yml
createdb -O kemal invidious
psql invidious kemal < /home/invidious/invidious/config/sql/channels.sql
psql invidious kemal < /home/invidious/invidious/config/sql/videos.sql
psql invidious kemal < /home/invidious/invidious/config/sql/channel_videos.sql
psql invidious kemal < /home/invidious/invidious/config/sql/users.sql
psql invidious kemal < /home/invidious/invidious/config/sql/session_ids.sql
psql invidious kemal < /home/invidious/invidious/config/sql/nonces.sql
psql invidious kemal < /home/invidious/invidious/config/sql/annotations.sql
psql invidious kemal < /home/invidious/invidious/config/sql/playlists.sql
psql invidious kemal < /home/invidious/invidious/config/sql/playlist_videos.sql
exit
sudo -i -u invidious
cd invidious
shards update && shards install
crystal build src/invidious.cr --release
# test compiled binary
./invidious # stop with ctrl c
exit
sudo cp /home/invidious/invidious/invidious.service /etc/systemd/system/invidious.service
sudo systemctl enable --now invidious.service
echo "/home/invidious/invidious/invidious.log {
rotate 4
weekly
notifempty
missingok
compress
minsize 1048576
}" | sudo tee /etc/logrotate.d/invidious.logrotate
sudo chmod 0644 /etc/logrotate.d/invidious.logrotate
~~~

<br>
Sea cual sea el tipo de instalación realizada es importante, a continuación, revisar los [procedimientos de post-instalación](https://github.com/iv-org/documentation/blob/master/Installation.md#post-install-configuration), [de configuración](https://github.com/iv-org/documentation/blob/master/Configuration.md) y, llegado el momento, [de actualización](https://github.com/iv-org/documentation/blob/master/Updating.md).  

<br>
<br>
## Manejando nuestro servidor de 'invidious'
[Uso de 'invidious'](https://github.com/iv-org/documentation/blob/master/Installation.md#usage).  
~~~
./invidious -h
~~~
~~~
Usage: invidious [arguments]
    -b HOST, --bind HOST             Host to bind (defaults to 0.0.0.0)
    -p PORT, --port PORT             Port to listen for connections (defaults to 3000)
    -s, --ssl                        Enables SSL
    --ssl-key-file FILE              SSL key file
    --ssl-cert-file FILE             SSL certificate file
    -h, --help                       Shows this help
    -c THREADS, --channel-threads=THREADS
                                     Number of threads for refreshing channels (default: 1)
    -f THREADS, --feed-threads=THREADS
                                     Number of threads for refreshing feeds (default: 1)
    -o OUTPUT, --output=OUTPUT       Redirect output (default: STDOUT)
    -v, --version                    Print version
~~~

<br>
<br>
<br>
Entradas relacionadas:  
- [NewPipe: contenidos de youtube con privacidad y con privilegios de cliente de pago (sin pagarles un céntimo)](https://hijosdeinit.gitlab.io/newpipe-contenidos-de-youtube-como-si/)
- [[HowTo] Mejor forma de incorporar la última versión de 'youtube-dl' en Debian y derivados. II](https://hijosdeinit.gitlab.io/howto_youtube-dl_debian_netinstall/)
- [[HowTo] el metabuscador SEARX](https://hijosdeinit.gitlab.io/howto_searx_metabuscador/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="@@@@@@@@@@@@@@@@@@@@@@@@@@@">@@@@@@@@@@@@@@@@@@@@@@@@@@@</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
