---
layout: post
title: "Recomendaciones de Mhyst (noviembre de 2022): predicando en el desierto. vol. 02"
date: 2022-11-06
author: Termita
category: "productividad"
tags: ["Mhyst", "vim", "asciidoc", "git", "pass", "password store", "webtorrent", "peerflix", "kodi", "tacones", "multimedia", "autodesk sketchbook", "texto plano", "vi", "vim", "linux", "pass", "gnu linux", "debian", "open source", "software libre"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
6 de noviembre de 2022, Mhyst -[Reality Cracking](http://feeds.feedburner.com/rcracking)- es nuevamente entrevistado en un mentidero dedicado al consumo compulsivo de tecnología de pago.  

<br>
Aunque sospecho que sus palabras cayeron en tierra yerma, estas eran las recomendaciones de Mhyst, un estímulo quizás -siendo optimista- para que la parroquia de aquel podcast / videocanal dejara de pagarle los vicios a alguna de esas famosas corporaciones de servicios de software -y/o hardware-:  
- GNU Linux -como era de esperar-. Mención especial a [Debian](https://www.debian.org/), "madre" de muchas distribuciones, por encima de Ubuntu y Linux Mint.
- [artha](https://artha.sourceforge.net/): un *thesaurus* en lengua inglesa de rápido acceso
- [Writer's Cafe](http://www.writerscafe.co.uk/download.htm): creado por [Anthemion Software](http://www.anthemion.co.uk/), es un editor orientado a escritores de libros
- [ywriter](http://www.spacejock.com/yWriter.html): editor orientado a escritores de libros
- [amule](http://www.amule.org/): gestor de descargas P2P (*Donkey*)
- [transmission-remote](https://transmissionbt.com/addons): gestor remoto para Transmission *headless* 
- [mutt](http://www.mutt.org/): gestor de correo electrónico
- [Calibre](https://calibre-ebook.com/es): visor de libros electrónicos
- [Calibre companion](): visor de libros electrónicos para teléfono móvil *smartphone*; "de pago"
- [Gimp](https://www.gimp.org/): editor de imagenes
- [Blender](https://www.blender.org/): editor 3D
- [OBS Studio](https://obsproject.com/es/): editor de audio / video específico para *streaming*
- [GNU Cash](https://www.gnucash.org/index.phtml?lang=es_ES): utilidad de contabilidad doméstica
- [filezilla](https://filezilla-project.org/): gestor FTP
- [twonky media](https://www.lynxtechnology.com/twonky-server): servidor DLNA
- [Serveo](https://serveo.net): pseudoVPN, una especie de conexión *reversa*, parecido a [ngrok](https://ngrok.com) o [localtunnel](https://localtunnel.me) o [portmap](https://portmap.io)
- ['vim'](https://victorhck.gitlab.io/comandos_vim/)
- ['AsciiDoc'](https://asciidoc.org/), un lenguaje de marcas como lo son ['MarkDown'](https://joedicastro.com/pages/markdown.html) o ['reStructuredText'](https://www.sphinx-doc.org/es/master/usage/restructuredtext/index.html).
- ['pass' (Password Store)](https://www.passwordstore.org/): gestor de contraseñas  

Compartió asimismo una reflexiónn trascendente: traten de no habituarse a "los servicios de internet" y tampoco a utilizar el móvil para las cosas importantes. Esas dependencia puedes ser catastróficas. El doble factor de autentificación, por poner un ejemplo, puede ser vapuleado por un tercero con una inversión mínima.  

A parte de software, Mhyst recomendó los libros:
- *La sangre manda*, de Stephen King.
- la trilogía *Musashi* (*La leyenda del samurai*, *El camino de la espada*, *La luz perfecta*), de Eiji Yosikawa.
- *Desobediencia civil*, de Henry David Thoreau  

<br>
<br>
Entradas relacionadas:  
- [Recomendaciones de software de Mhyst (2020): predicando en el desierto](https://hijosdeinit.gitlab.io/MhystRecomendaciones_software_2020_predicando_en_el_desierto/)
- []()  

<br>
<br>
<br>
<br>
<br>
<br>
<br>
