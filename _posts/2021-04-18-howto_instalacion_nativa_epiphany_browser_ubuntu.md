---
layout: post
title: "[HowTo] Instalación NATIVA de 'epiphany' ('gnome-web') en Ubuntu y familia"
date: 2021-04-18
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "ubuntu", "snap", "nueva paqueteria", "software", "apt", "browser", "epiphany browser", "epiphany", "navegador web", "navegador", "web browser", "icecat", "iceweasel", "internet", "internet 4.0", "privacidad", "anonimato", "respeto"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
'Epiphany' es un navegador web ligero, sencillo y versátil, libre, de código abierto.  
'Epiphany' últimamente también recibe el nombre de 'Gnome-Web'.  
Cuando en determinados sistemas operativos GNU Linux como Ubuntu que pareciera han apostado por la "nueva paquetería" (snap, appimage, flatpak, etc...) uno trata de instalar 'epiphany' a través del interfaz gráfico (tienda de aplicaciones, por ejemplo) lo que se instala no es de forma nativa (apt), sino un paquete 'snap'.  
A mí no me gusta la nueva paquetería porque prefiero instalar las cosas con `sudo apt install'.  

## Instalar "de forma nativa" 'epiphany' en Ubuntu y similares
~~~bash
sudo apt-get update
sudo apt install epiphany-browser
~~~

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://en.wikipedia.org/wiki/GNOME_Web">Wikipedia - Gnome Web</a>  
<a href="https://wiki.gnome.org/Apps/Web">wiki.gnome.org - Gnome Web</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
