---
layout: post
title: "La importancia de respaldar tu sesión de 'Firefox ESR' antes de actualizar el sistema"
date: 2021-06-06
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "Debian", "liveusb", "livecd", "persistente", "dd", "iso", "boot"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno: Core2Duo 6gb RAM, Debian 10 *Buster* (*netinstall*), 'Firefox ESR' en su día instalado manualmente.</small>  

<br>
<big>**☠‼**</big> Antecedentes:  
2021, 06 de junio: Actualizo el sistema mediante <span style="background-color:#042206"><span style="color:lime">`sudo apt update`</span></span>.  
'Firefox ESR' es actualizado en el proceso. Como resultado:
- **la sesión de navegación se perdió**
- **las extensiones se perdieron**.  

<br>
Conclusión: es conveniente respaldar periódicamente -y sobre todo antes de las actualizaciones del sistema- la configuración de 'Firefox ESR', o mejor aún todas las configuraciones.  
<small>**ℹ** Una de las formas más sencillas de respaldar nuestro 'Firefox ESR' es, por ejemplo, comprimiendo -con permisos de superusuario si fuere necesario- la carpeta <span style="background-color:#042206"><span style="color:lime">'home/tuusuario/.mozilla'</span></span>.</small>  
<small><span style="background-color:#042206"><span style="color:lime">`sudo 7z a -t7z -m0=lzma -mx=9 -mfb=64 -md=32m -ms=on -v1490m -p .mozilla_`*fecha*`.7z .mozilla/`</span></span></small>  

<br>
## Solución al problema ya consumado: restaurar la sesión de 'Firefox ESR' desde el respaldo que deberíamos tener
1. Arránquese 'Firefox ESR' y configurar que en cada inicio restaure la sesión de navegación.  
2. Ciérrese 'Firefox ESR'
3. Restáurese la sesión  
<small>**ℹ** El archivo <span style="background-color:#042206"><span style="color:lime">'home/usuario/.mozilla/firefox/xxxxxxxx.default-esr/sessionstore-backups/previous.jsonlz4'</span></span> del respaldo es el que contiene el backup de la sesión previa de navegación (aquella que perdimos al actualizar). Ésta se guarda cada vez que se cierra Firefox.</small>  
<small>**ℹ** El archivo <span style="background-color:#042206"><span style="color:lime">'home/nervic/.mozilla/firefox/xxxxxxxx.default-esr/sessionstore.jsonlz4'</span></span> es el que contiene la sesión de navegación en nuestro Firefox. Como tras actualizar todo esto se perdió, probablemente ese archivo no existirá.</small>  
Al grano, solucionemos el entuerto:  
<span style="background-color:#042206"><span style="color:lime">`cp /rutadelrespaldo/.mozilla/firefox/xxxxxxxx.default-esr/sessionstore-backups/previous.jsonlz4 /home/tuusuario/.mozilla/firefox/xxxxxxxx.default-esr/sessionstore.jsonlz4`</span></span>  

<br>
<small>Anotación: Es posible que el comando hayamos de lanzarlo como superusuarios ('<span style="background-color:#042206"><span style="color:lime">sudo</span></span>').</small>  

<br>
Al arrancar 'Firefox ESR' se restaurará la sesión que perdimos al actualizar.  

<br>
<br>

Entradas relacionadas:  
- []()  

<br>
<br>
<br>

--- --- ---  
<small>Fuentes:  
<a href=""></a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
