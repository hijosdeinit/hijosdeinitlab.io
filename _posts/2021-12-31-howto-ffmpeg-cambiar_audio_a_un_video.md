---
layout: post
title: "[HowTo] Cambiar el audio a un video mediante 'ffmpeg'"
date: 2021-12-31
author: Termita
category: "multimedia"
tags: ["avi", "error", "problema", "indice", "mencoder", "ffmpeg", "audio", "video", "multimedia", "extraer", "insertar"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Suponiendo que, por ejemplo, tenemos **un video** con el audio en **inglés** y queremos ponerle el audio en **español** procedente de **otro video**.  

<br>
Si el audio que deseamos está en otro video, habrá que extraerlo. No hará falta recodificar:
~~~bash
ffmpeg -i input-video_español.avi -vn -acodec copy output-audio_español.aac
~~~

<br>
## 1. Dejar el video sin audio
~~~bash
ffmpeg -i input-video_ingles.avi -c copy -an output-video_sinaudio.avi
~~~

<br>
## 2. Insertar el nuevo audio en el video
`ffmpeg -i input-video_sinaudio.mp4 -i input-audio_español.aac -c copy -map 0:v:0 -map 1:a:0 output-video.mp4`  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Convertir vídeo ‘.ogv’ a otros formatos](https://hijosdeinit.gitlab.io/howto_convertir_ogv_a_otros_formatos/)
- [[HowTo] Incorporar pista de audio a una pista de video](https://hijosdeinit.gitlab.io/howto_incorporar_pista_audio_a_video/)
- [[HowTo] Trocear un video](https://hijosdeinit.gitlab.io/howto_trocear_video/)
- [[HowTo] Cálculo del bitrate de un video para su recompresión en 2 pasadas](https://hijosdeinit.gitlab.io/howto_calculo_bitrate_video_recompresion_2_pasadas_ffmpeg_handbrake/)
- [[HowTo] instalacion de HandBrake en Ubuntu y derivados](https://hijosdeinit.gitlab.io/howto_instalacion-handbrake-ubuntu-apt/)
- [[HowTo] Extracción de imagenes de un video mediante ffmpeg (GNU Linux)](https://hijosdeinit.gitlab.io/howto_ffmpeg_extraer_fotogramas_de_archivo_video/)  

<br>
<br>
<br>

--- --- ---
<small>
Fuentes:  
[superuser](https://superuser.com/questions/268985/remove-audio-from-video-file-with-ffmpeg)  
[stackoverflow](https://stackoverflow.com/questions/9913032/how-can-i-extract-audio-from-video-with-ffmpeg)  
[superuser](https://superuser.com/questions/590201/add-audio-to-video-using-ffmpeg)  
</small>

<br>
<br>
<br>
