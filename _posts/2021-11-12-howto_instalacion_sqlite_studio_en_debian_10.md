---
layout: post
title: "[HowTo] Instalación de 'SqliteStudio' en Debian 10 y derivados"
date: 2021-11-12
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "sqlite", "bbdd", "base de datos", "database", "dbase", "mysql", "mariadb", "mongodb", "sql", "gnu linux", "linux", "debian", "ubuntu"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno: Entorno: cpu Core2Duo, 6gb RAM, gpu gforce 7600gt, GNU Linux Debian 10 Buster (netinstall) stable, kernel 4.19.0-18-amd64, gnome</small>  

<br>
<br>
['SQLiteStudio'](https://sqlitestudio.pl/about/) es una aplicación de escritorio para consultar y editar archivos de bases de dato SQLite.  
El proyecto fue inicialmente implementado en 'Tcl/Tk'; sin embargo la versión 3.0.0 fue completamente reescrita en 'C++/Qt'.  
'SQLiteStudio' es software de código abierto (*'open source'*) lanzado bajo licencia GPL (v3), de uso libre para cualquier propósito, y es multiplataforma: GNU Linux, macOS, Windows.  
A parte de su página web oficial, ['SQLiteStudio' mantiene un repositorio en 'Github'](https://github.com/pawelsalawa/sqlitestudio).  

<br>
<br>
## "Instalación" de 'SQLiteStudio'
Básicamente el procedimiento de **incorporación** de 'SQLiteStudio' a Debian y derivados consiste en:
- Creación del subdirectorio que albergará el programa 'SQLiteStudio'
- Descarga del software desde la [sección 'releases' del repositorio de 'SQLiteStudio' en 'GitHub'](https://github.com/pawelsalawa/sqlitestudio/releases)
- Descompresión en el subdirectorio que albergará el programa 'SQLiteStudio'
- Corrección de la dependencia '/lib/x86_64-linux-gnu/libxcb-util.so.1'  

<br>
Así:  

<br>
~~~bash
mkdir ~/Programas/sqlitestudio
cd ~/Programas/sqlitestudio
wget https://github.com/pawelsalawa/sqlitestudio/releases/download/3.3.3/sqlitestudio-3.3.3.tar.xz
~~~
<br>
Descomprimase 'sqlitestudio-3.3.3.tar.xz'  
<br>
Corríjase la dependencia '/lib/x86_64-linux-gnu/libxcb-util.so.1':  
<span style="background-color:#042206"><span style="color:lime">`sudo ln -s /lib/x86_64-linux-gnu/libxcb-util.so.0.0.0 /lib/x86_64-linux-gnu/libxcb-util.so.1`</span></span>  

<br>
... y esto hay que hacerlo **PARA EVITAR**, cuando ejecutemos 'SQLiteStudio', el siguiente mensaje de **error**:
~~~
qt.qpa.plugin: Could not load the Qt platform plugin "xcb" in "" even though it was found.
This application failed to start because no Qt platform plugin could be initialized. Reinstalling the application may fix this problem.
Available platform plugins are: xcb.
Abortado
~~~

<br>
<br>
## Ejecución de 'SQLiteStudio'
En el subdirectorio donde se descomprimió 'SQLiteStudio' es constatable que existen 2 versiones del programa:
- versión gráfica ('sqlitestudio')
- versión para línea de comandos ('sqlitestudiocli')  

Por consiguiente, dependiendo de la versión que deseemos ejecutar, respectivamente:
~~~bash
./sqlitestudio
~~~
ô
~~~bash
./sqlitestudiocli
~~~

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Instalación de 'sqlite' en Debian 10 y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_sqlite_en_debian_10/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://github.com/pawelsalawa/sqlitestudio/issues/4045#issuecomment-808631078">luckymedog en GitHub.com/pawelsalawa/sqlitestudio/issues</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
