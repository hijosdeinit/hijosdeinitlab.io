---
layout: post
title: "[HowTo] directorios y archivos de transmission-daemon en Debian y derivados"
date: 2021-03-01
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "transmission", "transmission-cli", "transmission-daemon", "Debian", "directorio", "archivo", "configuracion", "settings.json", "stats.json", "torrent", "resume", "descargas", "blocklists", "p2p", "ed2k", "kad", "amule", "amule-daemon", "demonio", "servicio", "amuleweb", "amulegui", "amulecmd"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En muchas ocasiones para descargar mediante el protocolo 'torrent' basta con instalar el demonio de transmission -'transmission-daemon'- en una máquina para, a continuación, ir añadiéndole las descargas de forma remota (desde otras máquinas). De esta forma centralizamos las descargas.  
Esto es muy habitual, por ejemplo, en aquellas RaspBerry Pi que actúan como servidor.  

'transmission-daemon', una vez instalado, posee los siguientes archivos y directorios:  
- '/etc/init.d/transmission-daemon' = el script init de 'transmission-daemon'  
- '/var/lib/transmsision-daemon/info/' = directorio vinculado a '~/.config/transmission-daemon/'  
- '/var/lib/transmsision-daemon/info/settings.json' = archivo vinculado a '/etc/transmission-daemon/settings.json'. Es el [archivo de configuración](https://trac.transmissionbt.com/wiki/MovedToGitHub/EditConfigFiles).  
- '/var/lib/transmission-daemon/info/stats.json' =  archivo json que contiene las estadísticas de la sesión.  
- '/var/lib/transmission-daemon/info/torrents/' = directorio que contiene los archivos '.torrent' que han sido añadidos a Transmission.  
- '/var/lib/transmission-daemon/info/resume/' = Este dierctorio contiene los archivos '.resume' que mantienen la información de cada uno de los archivos torrent, tal como qué partes han sido descargadas, el directorio donde se almacenaron los datas descargados, etcétera.  
- '/var/lib/transmission-daemon/info/blocklists/' = Este directorio contiene las [listas de bloqueo](https://trac.transmissionbt.com/wiki/MovedToGitHub/Blocklists).  
- '/var/lib/transmission-daemon/info/dht.dat'  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Incorporar un tema visual más atractivo al interfaz web de 'amule-daemon'](https://hijosdeinit.gitlab.io/howto_incorporar_tema_visual_atractivo_al_interfaz_web_amule/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://trac.transmissionbt.com/wiki/MovedToGitHub/ConfigFiles">trac.transmissionbt.com</a>  
<a href="https://trac.transmissionbt.com/wiki/UnixServer/Debian">trac.transmissionbt.com - debian</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
