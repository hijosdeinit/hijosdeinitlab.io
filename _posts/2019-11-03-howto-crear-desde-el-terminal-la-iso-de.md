---
layout: post
title: '[HowTo] Crear desde el Terminal la ISO de un soporte de almacenamiento o de una carpeta'
date: 2019-11-03
author: Termita
category: "sistemas operativos"
tags: ["cdrom", "backup", "linux", "progress", "mkisofs", "genisoimage", "sistemas operativos", "dd", "iso", "cli", "dvdrom", "almacenamiento"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Si vamos a construir una .ISO a partir de un dispositivo de almacenamiento primero comprobaremos su nombre, es decir '/dev/xxxx'
~~~bash
sudo mount
~~~
ô también se puede consultar con un
~~~bash
ls /dev
~~~
Y procedemos a construir la .ISO. La nomenclatura es esta:
~~~bash
dd if=/dev/xxxx of=/rutadondequeremosguardarlaiso/nombrearchivo.iso status=progress
~~~
<small>(**ℹ**) El parámetro **status=progress** nos permite conocer el progreso de la operación de 'dd', que por defecto es mudo. [Este artículo de este blog trata de cómo mostrar cómo progresa el trabajo del comando 'dd'](https://hijosdeinit.gitlab.io/howto_dd_progreso_verbose/).</small>  
<br>
<br>
Por ejemplo:  
<br>
Si queremos almacenar en nuestro escritorio una .ISO de un dvd que está en '/dev/dvd'
~~~bash
dd if=/dev/dvd of=/home/usuario/Escritorio/dvdrom.iso status=progress
~~~  
<br>
ô, tratándose de un una unidad lectora de dvd por usb (/dev/sr0):
~~~bash
dd if=/dev/sr0 of=/home/usuario/Escritorio/dvdrom.iso status=progress
~~~  
<br>
Si queremos almacenar en nuestro escritorio una .ISO de un cd que está en /dev/cdrom
~~~bash
dd if=/dev/cdrom of=/home/usuario/Escritorio/cdrom.iso status=progress
~~~  
<br>
Si queremos crear una .ISO de una carpeta, el procedimiento es el siguiente:
~~~bash
mkisofs -o /rutadondequeremosguardarlaiso/nombredearchivo.iso /carpetaquequeremosrespaldareniso
~~~
Hay que tener ojo con esto porque los nombres de los archivos -si son largos, con caracteres especiales, etcétera- pudieran no respetarse en la .iso creada.  
**Revisión/Actualización septiembre 2021**: Parece ser que 'mkisofs' ahora recibe el nombre de [**'genisoimage'**](https://manpages.debian.org/testing/genisoimage/mkisofs.1.en.html). El comando anterior quedaría, por consiguiente, adaptado así:
~~~bash
genisoimage -o /rutadondequeremosguardarlaiso/nombredearchivo.iso /carpetaquequeremosrespaldareniso
~~~

<br>
<br>
<br>
---
<br>
<small>Fuentes:  
[DesdeLinux](https://blog.desdelinux.net/como-crear-un-iso-desde-el-terminal/)  
[manpages.debian.org - genisoimage](https://manpages.debian.org/testing/genisoimage/mkisofs.1.en.html)</small>  

<br>
<br>
<br>
