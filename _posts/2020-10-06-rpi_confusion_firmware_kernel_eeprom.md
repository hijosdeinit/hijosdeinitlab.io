---
layout: post
title: 'En la comunidad RaspBerry Pi los términos "firmware" y "kernel" aparecen confusos'
date: 2020-10-06
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "hardware", "firmware", "kernel", "eeprom", "rpi-update", "rpi-eeprom", "rpi-eeprom-images", "upgrade", "downgrade"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
¿El script 'rpi-update' modifica el firmware de RaspBerry Pi -lo que está grabado en la eeprom de la placa- o sólo actualiza el kernel del sistema operativo?  
En algunos tutoriales de por ahí hay información confusa y/o ambiguedad.  
A mi entender una cosa es upgradear / downgradear el firmware de RaspBerry Pi, que queda grabado para siempre, aunque cambies el sistema operativo...  
... y otra cosa es actualizar el kernel -rpi-update- del sistema operativo RaspBian.  
Corríjanme si me equivoco. Echo de menos un tuto que trate con propiedad estos temas.  

A mi entender, 'rpi-update' actualiza el kernel del sistema operativo linux que alberga la microsd y/o el hdd usb. Mas juraría que esos cambios se "pierden" si arrancamos con otro sistema operativo.  

El firmware propiamente dicho sospecho que se escribe en la eeprom de la placa de la RaspBerry, está relacionado con los programas 'rpi-eeprom' y 'rpi-eeprom-images', y son cambios perpetuos que perduran por encima de los diferentes sistemas operativos que se ejecuten en la máquina.  
El firmware se puede upgradear o downgradear.  

Es todo un tanto lioso. Echo de menos un artículo concluyente que hablara con propiedad del tema.  

<br>
<br>
<br>
<br>
<br>
