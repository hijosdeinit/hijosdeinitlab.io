---
layout: post
title: "[HowTo] Creación de liveUSB persistente FreeDOS"
date: 2021-09-09
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "freedos", "msdos", "4dos", "gnu linux", "linux", "unix", "minix", "retrocomputing", "retrocomputacion"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
['FreeDOS'](http://www.freedos.org/) (anteriormente 'Free-DOS' y 'PD-DOS') es un sistema operativo libre para computadoras IBM PC compatibles. Pretende proporcionar un entorno completo compatible con [DOS](https://es.wikipedia.org/wiki/DOS) para la ejecución de software heredado y el soporte de sistemas integrados. FreeDOS puede ser arrancado desde una memoria USB y está diseñado para funcionar bajo virtualización o emulación x86.
 
A diferencia de la mayoría de las versiones de [MS-DOS](https://es.wikipedia.org/wiki/DOS), FreeDOS está compuesto por software libre y de código abierto, licenciado bajo los términos de la GNU General Public License. Por lo tanto, su distribución básica no requiere derechos de licencia o derechos de autor y se permite la creación de distribuciones personalizadas. Sin embargo, otros paquetes que forman parte del proyecto FreeDOS incluyen software no GPL que se considera conveniente preservar, como el 4DOS, que se distribuye bajo [licencia MIT](https://es.wikipedia.org/wiki/Licencia_MIT) modificada.  
<small>Podría decirse que 'FreeDos', **salvando matices y particularidades**, es a 'MSDOS' lo que ['minix'](https://es.wikipedia.org/wiki/MINIX) -o ['minix3'](http://www.minix3.org/)- o ['linux'](https://www.kernel.org/) es a ['Unix'](https://www.opengroup.org/membership/forums/platform/unix)</small>.  

<br>
<a href="/assets/img/2021-09-09-howto_creacion_liveusb_persistente_FreeDos/freedos.png" target="_blank"><img src="/assets/img/2021-09-09-howto_creacion_liveusb_persistente_FreeDos/freedos.png" alt="'Blinky', la mascota de FreeDOS desde 2014" width="400"/></a>  
<small>'Blinky', la mascota de FreeDOS desde 2014</small>  
<br>

'FreeDOS' incluye algunas características que no estaban presentes en MS-DOS:
- Controlador Ultra-DMA y soporte para discos grandes (LBA).
- Soporte para el sistema de archivos [FAT32](https://es.wikipedia.org/wiki/FAT32).
- Controlador ['DOSLFN'](http://adoxa.altervista.org/doslfn/) para usar los nombres de archivo largos de VFAT.
- Licencia libre (GPL).
- Soporte de internacionalización definida por el usuario.  

El proyecto 'FreeDOS' comenzó a proporcionar una alternativa a 'MS-DOS' cuando Microsoft anunció en 1994 que dejaría de vender y dar soporte a su 'MS-DOS'.  
'FreeDOS' es un proyecto que ***"está al día"***, es decir, que -a diferencia de MSDOS- está actualizado: sigue manteniéndose y desarrollándose. Esto lo hace especialmente útil para trabajar hoy día en máquinas antiguas o, incluso, como sistema operativo "puente" para instalar Windows.  

<br>
<br>
## Procedimiento para crear un disco de arranque liveUSB persistente del sistema operativo FreeDOS
#### ¬ Descárguese FreeDos
En la [sección de descargas de la página oficial de 'FreeDos'](https://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/distributions/1.2/) existen varias opciones. Ya que lo que se desea es elaborar un *live usb*, escójase la [**distribución "usb full"** de la versión estable, que a día de hoy es la 1.2](https://www.ibiblio.org/pub/micro/pc-stuff/freedos/files/distributions/1.2/FD12FULL.zip).  

#### ¬ 1. Descomprímase y "quémese" en el dispositivo de almacenamiento USB
Para ello puede utilizarse ['rufus'](https://rufus.ie/es/) o el [comando 'dd'](https://hijosdeinit.gitlab.io/howto_formas_creacion_de_sd_o_dispositivo_almacenamiento_usb_arranque_raspberrypi/).  
No son necesarios parámetros especiales.    

#### ¬ 2. (<small>en el recién creado usb</small>) Modifíquese lo siguiente:
2.1. El subdirectorio '/FDSETUP/BIN' ha de ser movido a '/', es decir a la raíz del dispositivo usb.  

<br>
2.2. Bórrese el archivo '/SETUP.BAT'  

<br>
2.3. Edítese el archivo '/FDCONFIG.SYS'  
... para que quede así:
~~~
!COUNTRY=001,858:\BIN\COUNTRY.SYS
!LASTDRIVE=Z
!BUFFERS=20
!FILES=40

DOS=HIGH
DOS=UMB
DOSDATA=UMB

DEVICE=\BIN\HIMEMX.EXE

SHELLHIGH=COMMAND.COM \BIN /E:2048 /P=\AUTOEXEC.BAT
~~~

<br>
2.4. Edítese el archivo '/AUTOEXEC.BAT'
... para que quede así:
~~~
@echo off
SET DOSDIR=
SET LANG=
SET PATH=%dosdir%\BIN

SET DIRCMD=/P /OGN /Y

rem SET TEMP=%dosdir%\TEMP
rem SET TMP=%TEMP%

rem SET NLSPATH=%dosdir%\NLS
rem SET HELPPATH=%dosdir%\HELP
rem SET BLASTER=A220 I5 D1 H5 P330
rem SET COPYCMD=/-Y

DEVLOAD /H /Q %dosdir%\BIN\UDVD2.SYS /D:FDCD0001

SHSUCDX /QQ /D3

rem SHSUCDHD /QQ /F:FDBOOTCD.ISO

FDAPM APMDOS

rem SHARE

rem NLSFUNC %dosdir%\BIN\COUNTRY.SYS
rem DISPLAY CON=(EGA),858,2)
rem MODE CON CP PREP=((858) %dosdir%\CPI\EGA.CPX)
rem KEYB US,858,%dosdir%\bin\keyboard.sys
rem CHCP 858
rem PCNTPK INT=0x60
rem DHCP
rem MOUSE

rem DEVLOAD /H /Q %dosdir%\BIN\UIDE.SYS /H /D:FDCD0001 /S5

SHSUCDX /QQ /~ /D:?SHSU-CDR,D /D:?SHSU-CDH,D /D:?FDCD0001,D /D:?FDCD0002,D /D:?FDCD0003,D

rem MEM /C /N

SHSUCDX /D

rem DOSLFN

rem LBACACHE.COM buf 20 flop

SET AUTOFILE=%0
SET CFGFILE=\FDCONFIG.SYS
alias reboot=fdapm warmboot
alias reset=fdisk /reboot
alias halt=fdapm poweroff
alias shutdown=fdapm poweroff

rem alias cfg=edit %cfgfile%
rem alias auto=edit %0

:SkipLanguageData
vecho /p Done processing startup files /fCyan FDCONFIG.SYS /a7 and /fCyan AUTOEXEC.BAT /a7/p
~~~

Asunto concluído.  

<br>
## Arrancando con 'FreeDOS'
Una vez insertado el dispositivo *liveusb* 'FreeDOS' y encendida la máquina de la forma adecuada para que arranque desde éste, si todo ha ido bien, veremos el *prompt* de FreeDOS.  
Una de las cosas que primero constataremos es que el teclado no está en español. No hay problema, esto se soluciona ejecutando:
~~~
keyb sp
~~~
Para no tener que andar haciendo eso siempre, basta con añadir 'keyb sp' al archivo '/autoexec.bat'.  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://spa.amuddycup.com/635868-how-to-install-freedos-onto-GCVWGA-article">spa.amuddycup.com</a>  
<a href="http://freedos.sourceforge.net/wiki/index.php/How_to_Create_a_USB_Boot_Disk_Using_FreeDOS">wiki - freedos.sourceforge.net</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
