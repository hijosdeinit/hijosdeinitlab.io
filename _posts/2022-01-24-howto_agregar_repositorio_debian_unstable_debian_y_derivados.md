---
layout: post
title: "[HowTo] Agregar repositorio 'debian unstable' a Debian 10 y similares"
date: 2022-01-24
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "debian", "debian 10", "bullseye", "repositorios", "sources.list", "apt", "software", "main", "contrib", "non-free", "backports", "unstable", "security"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Hay aplicaciones que no están en los repositorios convencionales oficiales de Debian. Este es el caso del repositorio llamado 'unstable' oficial de Debian, que contiene programas que, si no lo agregamos al listado de repositorios de nuestro sistema, no podremos instalar con `sudo apt install` sino que habremos de recurrir a otros procedimientos (descarga y compilación manual del programa, descarga del binario apropiado, etcétera.).
En el archivo <span style="background-color:#042206"><span style="color:lime">'/etc/apt/sources.list'</span></span> se encuentran listados los repositorios a los que se conectará nuestro sistema cuando, generalmente mediante el comando <span style="background-color:#042206"><span style="color:lime">`apt`</span></span>, instalamos o actualizamos software en un sistema operativo GNU Linux basado en Debian.  

<br> He aquí una muestra del contenido del archivo '/etc/apt/sources.list', con los repositorios 'main', 'contrib', 'non-free', 'security' y 'backports'.   <small>(i) Es de Debian 11 *Bullseye*, mas se puede adaptar a versiones de Debian anteriores.</small>
~~~
deb http://deb.debian.org/debian bullseye main contrib non-free
deb-src http://deb.debian.org/debian bullseye main contrib non-free
# debian-security
deb http://deb.debian.org/debian-security/ bullseye-security main contrib non-free
deb-src http://deb.debian.org/debian-security/ bullseye-security main contrib non-free
# bullseye-updates
deb http://deb.debian.org/debian bullseye-updates main contrib non-free
deb-src http://deb.debian.org/debian bullseye-updates main contrib non-free
# backports
deb http://deb.debian.org/debian bullseye-backports main contrib non-free
deb-src http://deb.debian.org/debian bullseye-backports main contrib non-free
~~~

<br>
Al grano:  

<br>
## ¿Cómo agregar el repositorio 'unstable' a nuestro sistema Debian?
~~~bash
sudo nano '/etc/apt/sources.list'
~~~
... añádanse estas 2 líneas
~~~
deb https://deb.debian.org/debian/ unstable main contrib non-free
deb-src https://deb.debian.org/debian/ unstable main contrib non-free
~~~
~~~bash
sudo apt-get update
~~~

<br>
<br>
## **¡PRECAUCIÓN!**
Actualizar el sistema con el repositorio *Debian 'Unstable'* activado, hará que el sistema se actualice -valga la redundancia- a la última versión 'unstable' de Debian.  
Eso puede ser un efecto no deseado, como fue mi caso: Olvidé deshabilitar el repositorio 'unstable' y al actualizar el sistema -`sudo apt-get update`, `sudo apt upgrade`-, mi Debian 10 se convirtió en Debian 11 "unstable"; muchas cosas dejaron de funcionar, sobre todo el interfaz gráfico (mi tarjeta gráfica 'nvidia' es muy antigua (*legacy*) y eso tiene sus complicaciones, supongo).  
Por eso, si -por ejemplo- hemos activado el repositorio *Debian 'unstable' **sólo** porque deseamos instalar un programa que no está en los otros repositorios ('stable', 'backports', etc), conviene deshabilitar ese repositorio antes de actualizar el sistema.  
Asimismo, siempre es buena idea hacer respaldo del sistema antes de actualizarlo. ['TimeShift'](https://github.com/teejee2008/timeshift) es un sistema de backup incremental fácil de utilizar e instalar, y muy eficaz.  

<br>
<br>
## ¿Cómo deshabilitar el repositorio 'unstable' en nuestro sistema Debian?
~~~bash
sudo nano '/etc/apt/sources.list'
~~~
... COMÉNTENSE estas 2 líneas
~~~
#deb https://deb.debian.org/debian/ unstable main contrib non-free
#deb-src https://deb.debian.org/debian/ unstable main contrib non-free
~~~
~~~bash
sudo apt-get update
~~~

<br>
<br>
Entradas relacionadas:  
- [[HowTo] '/etc/apt/sources.list': repositorios en Debian y derivados](https://hijosdeinit.gitlab.io/howto_repositorios_sources.list_debian_y_derivados/)
- [[HowTo] Instalación de una versión más actualizada de un paquete desde los repositorios oficiales de Debian: 'backports'](https://hijosdeinit.gitlab.io/howto_instalacion_version_mas_actualizada_desde_repositorios_Debian_backports/)
- [[HowTo] Instalar el comando 'add-apt-repository' en Debian](https://hijosdeinit.gitlab.io/howto_add-apt-repository_en_debian_y_derivados/)
- [[HowTo] Eliminar repositorio agregado a mano ('add-apt-repository') en Debian y derivados](https://hijosdeinit.gitlab.io/howto_eliminar_repositorios_agregados_manualmente_addaptrepository_Debian_derivados/)
- [[HowTo] Solucionar error de GPG cuando la clave pública de un repositorio no está disponible. (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_solucionar_error_gpg_clave_publica_repositorio_Debian_y_derivados/)
- [[HowTo] Eliminar Clave 'Gpg' Cuyo 'Id' Desconocemos. (Debian Y Derivados)](https://hijosdeinit.gitlab.io/howto_eliminar_clave_gpg_id_desconocida_agregada_mediante_apt-key_add_/)
- [El repositorio de firmware / drivers para GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- ['RaspBian' / 'RaspBerry OS' incorpora furtivamente -sin aviso ni permiso- un repositorio de 'Microsoft'. Así no vamos bien](https://hijosdeinit.gitlab.io/repositorio_microsoft_instalado_furtivamente_en_Raspbian_RaspBerryOs/)
- [[HowTo] Extirpar todo rastro del repositorio de MicroSoft que Raspbian / RaspBerry Os instala furtivamente](https://hijosdeinit.gitlab.io/howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://unix.stackexchange.com/a/644351">usuario GAD3R en unix.stackexchange.com</a>  
<a href="https://wiki.debian.org/SourcesList">wiki.debian.org</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
