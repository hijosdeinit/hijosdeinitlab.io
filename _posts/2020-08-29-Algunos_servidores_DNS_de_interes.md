---
layout: post
title: "Algunos servidores DNS de interés"
date: 2020-08-29
author: Termita
category: "redes"
tags: ["sistemas operativos", "redes", "internet", "dns", "ISP", "privacidad"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
#### LOWI
> 212.166.132.102  
> 212.166.132.96  


#### VODAFONE
> 212.166.210.80  
> 212.166.132.104  


#### MOVISTAR
> 80.58.61.250  
> 80.58.61.254  


#### OPENDNS
> 208.67.222.222  
> 208.67.220.220  

> 208.67.222.123 (block adult content)  
> 208.67.220.123 (block adult content)  

OpenDNS ofrece las siguientes direcciones de servidor de nombres de dominio (IPv4) para uso público:  
> 208.67.222.222 (OpenDNS Home Free/VIP)  
> 208.67.220.220 (OpenDNS Home Free/VIP)  
> 208.67.222.123 (OpenDNS FamilyShield)  
> 208.67.220.123 (OpenDNS FamilyShield)  

En algunas configuraciones DNS se precisa de unas 3ª y 4ª direcciones DNS adicionales:  
> 208.67.222.220 (Direcciones DNS adicionales)  
> 208.67.220.222 (Direcciones DNS adicionales)  

Servicios DNS IPv6
Actualmente OpenDNS solo provee de resolución recursiva de dominios para los sitios web basados en IPv6. Cuando globalmente la comunidad de Internet adopte IPv6, OpenDNS activará, en su panel de control, las funciones de seguridad y filtrado de contenido web, incluyendo las protecciones contra malware, botnet y phising.  
2620:0:ccc::2  
2620:0:ccd::2  



#### GOOGLE
> 8.8.8.8  
> 8.8.4.4  


#### CLOUDFLARE
> 1.1.1.1  
> 1.0.0.1  

> 2606:4700:4700::1111 (ipv6)  
> 2606:4700:4700::1001 (ipv6)  


#### OTROS

IBM (Quad9) – 9.9.9.9 y 149.112.112.112  
Comodo Secure DNS – 8.26.56.26 y 8.20.247.20 (a punto de desaparecer a favor de Comodo Dome)  
Comodo Dome – 8.26.56.10 y 8.20.247.10  
Norton ConnectSafe – 199.85.126.10 y 199.85.127.10 (descatalogadas, aunque aún funcionan)  
OpenNIC – 96.90.175.167 y 193.183.98.154  
UltraDNS – 156.154.70.1, 156.154.71.1  
UltraDNS Family – 156.154.70.3 y 156.154.71.3  
Level3 – 209.244.0.3 y 209.244.0.4  
Verisign – 64.6.64.6 y 64.6.65.6  
WATCH – 84.200.69.80 y 84.200.70.40  
DNS Advantage – 156.154.70.1 y 156.154.71.1  
SafeDNS – 195.46.39.39 y 195.46.39.40  
SmartViper – 208.76.50.50 y 208.76.51.51  
Dyn – 216.146.35.35 y 216.146.36.36  
FreeDNS – 37.235.1.174 y 37.235.1.177  
Alternate DNS – 198.101.242.72 y 23.253.163.53  
DNS – 77.88.8.8 y 77.88.8.1  
dk – 91.239.100.100 y 89.233.43.71  
Hurricane Electric – 74.82.42.42  
puntCAT – 109.69.8.51  
GreenTeamDNS – 81.218.119.11 y 209.88.198.133  

<br>
<br>
<br>
<br>
<br>