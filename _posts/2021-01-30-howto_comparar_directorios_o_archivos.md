---
layout: post
title: "[HowTo] 'diff'. Comparar directorios ô archivos"
date: 2021-01-30
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "windows", "diff", "comparar", "comparación", "cambios", "diferencias", "diffmerge", "winmerge", "fichero", "archivo", "directorio", "subdirectorio", "backup", "respaldo", "sincronización", "transferir", "copiar", "sincronizar", "diffmerge", "winmerge", "total commander", "tar", "md4", "sha256", "suma de verificacion", "cli"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Para comparar los contenidos de 2 directorios ó 2 archivos en GNU Linux se puede emplear el comando <big>'diff'</big> desde línea de comandos:
~~~bash
diff archivo1 archivo2
~~~
ô
~~~bash
diff -r -q directorio1 directorio2
~~~
Donde:  
* '-r' indica que los directorios se comparen recursivamente, es decir, con todos sus subdirectorios
* '-q' indica que ha de mostrar en el resultado únicamente los archivos que sean diferentes  

<br>
<br>
Para realizar esto desde un entorno gráfico existen aplicaciones multiplataforma -GNU Linux, Windows, MacOS- basadas en el comando 'diff' como son:
- [DiffMerge](https://www.sourcegear.com/diffmerge/)
- [WinMerge](https://winmerge.org/)  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Averiguar el tamaño de un directorio desde línea de comandos (CLI) mediante 'du'](https://hijosdeinit.gitlab.io/howto_averiguar_tama%C3%B1o_directorio__cli_find_du/)
- [[HowTo] Cálculo del número de archivos y subdirectorios que contiene un directorio mediante 'find'](https://hijosdeinit.gitlab.io/howto_calcular_numero_archivos_y_o_subdirectorios__find/)
- [[HowTo] Comprobación de que un directorio y sus subdirectorios se han transferido correctamente a su destino](https://hijosdeinit.gitlab.io/howto_comprobacion_copiado_correcto_directorios_archivos/)
- [[HowTo] Comparación de la suma de verificación md5 de varios directorios mediante 'tar'](https://hijosdeinit.gitlab.io/howto_comparacion_md5sum_varios_directorios_mediante_tar/)
- [[HowTo] Comparación de discos, directorios y archivos](https://hijosdeinit.gitlab.io/howto_comparacion_discos_directorios_archivos/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://geekland.eu/comparar-directorios-y-archivos-comando-diff-linux/">GeekLand.eu</a>  
<a href="https://www.dreamingbytes.com/diffmerge-compara-archivos-y-carpetas-en-windows-mac-y-linux/">DreamingBytes</a></small>  
<br>
<br>
<br>
<br>
<br>
<br>
