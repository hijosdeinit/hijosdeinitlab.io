---
layout: post
title: '[HowTo] Monitorix: instalación, configuracion, uso básico'
date: 2020-06-05
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "cpu", "core", "hardware", "procesador", "nproc", "lscpu", "cpuinfo", "CLI", "gnome", "gnome-system-monitor", "xfce4-taskmanager", "lxtask", "htop", "top", "conky", "monitorix", "monitor de sistema", "monitorización"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
[Monitorix](https://www.monitorix.org/) <small>es una herramienta de monitorización de sistemas libre, de código abierto y ligera. Está diseñada para monitorizar tantos servicios como recursos del sistema sea posible. Ha sido creada para ser utilizada en servidores que están en producción, y debido a su simplicidad y pequeño tamaño puede ser -igualmente- empleada en dispositivos embebidos.  
Fundamentalmente consiste en 2 programas:</small>
- <small>*"monitorix"*: el recolector, un demonio escrito en Perl que es iniciado automáticamente junto a otros servicios del sistema.</small>
- <small>*"monitorix.cgi"*: un script CGI.</small>  

<small>Monitorix incluye, desde la versión 3.0, su propio servidor http integrado que, por defecto, escucha en el puerto 8080. Por consiguiente, no es necesario instalar un servidor web de terceros como son *Apache*, *Nginx* o *Lighttpd* (aunque también puede configurarse para que funcione con cualquiera de esos servidores).  
El desarrollo de Monitorix fue emprendido inicialmente para monitorizar sistemas *Red Hat*, *Fedora* y *CentOS Linux*, con lo que este proyecto fue hecho adecuándose a este tipo de distribuciones. Hoy funciona en diferentes distribuciones GNU Linux e, incluso, en otros sistemas UNIX como *FreeBSD*, *OpenBSD* y *NetBSD*.  
Algunas de sus prestaciones son:</small>  
- <small>Gráficos de la carga del sistema y demanda del servicio del sistema.</small>
- <small>Gráficos procedentes de los sensores de temperatura CPU / GPU</small>
- <small>Gráficos de la temperatura y salud del disco.</small>
- <small>Gráficos del tráfico de red / puerto y netstat</small>
- <small>Estadísticas de correo</small>
- <small>Estadísticas del servidor web (Apache, Nginx, Lighttpd)</small>
- <small>MySQL carga y estadísticas</small>
- <small>Squid proxy de estadísticas</small>
- <small>Estadísticas servidor / cliente NFS</small>
- <small>Sensor **Raspberry Pi**</small>
- <small>Estadísticas de memcached</small>
- <small>Fail2ban</small>
- <small>Monitoriza servidores remotos (Multihost)</small>
- <small>Permite a los usuarios ver estadísticas en gráficos o en tablas de texto sin formato de forma diaria, semanal, mensual o anual</small>
- <small>Ofrece la capacidad de hacer zoom de gráficos para una mejor vista</small>
- <small>Tiene la capacidad de definir el número de gráficos por fila</small>  
<br>
![estadísticas sistema]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-06-05-[HowTo]_Monitorix_instalacion_configuracion_uso_basico/monitorix_system.png)  

<br>
[Más **capturas de pantalla** del interfaz de Monitorix](https://www.monitorix.org/screenshots.html)  

<br>
[Este es el repositorio de Monitorix en GitHub](https://github.com/mikaku/Monitorix).  

<br>
<br>
## Instalación de *Monitorix*
Monitorix se puede instalar de varias formas. Dos de ellas son:  

<br>
### A.
Instalación de la versión existente en los repositorios oficiales de nuestra distribución GNU Linux. Por ejemplo, en Debian y derivados el comando para instalar desde repositorio oficial es:
~~~
sudo apt-get update
sudo apt install monitorix
~~~  

<br>
### B.
Instalación de la última versión estable de Monitorix (y sus dependencias)  
Hay que descargar primero la [última versión estable](https://www.monitorix.org/downloads.html) desde la página oficial de Monitorix. El archivo descargado tendrá un nombre similar a 'monitorix*.deb'
~~~
sudo apt-get update
sudo apt install rrdtool perl libwww-perl libmailtools-perl libmime-lite-perl librrds-perl libdbi-perl libxml-simple-perl libhttp-server-simple-perl libconfig-general-perl libio-socket-ssl-perl
dpkg -i monitorix*.deb
~~~
Y para finalizar comprobamos y solucionamos cualquier problema de dependencias que pudiera existir:
~~~
sudo apt-get install -f
~~~

<br>
<br>
## Acceso a *Monitorix*
Comprobamos el estado del servicio monitorix
~~~
sudo systemctl status monitorix
~~~
El servicio se puede parar, iniciar o reiniciar sustituyendo 'status' por 'stop', 'start' o 'restart' respectivamente.

Accedemos a las estadísticas desde el navegador de la propia máquina:
~~~
http://localhost:8080
~~~
O desde otro ordenador (si en los cortafuegos hemos "abierto" el puerto 8080 (ℹ en el apartado configuración señalo cómo se hace esto))
~~~
http://ipdelamáquina:8080
~~~
ℹ El puerto que emplea el servidor web integrado de Monitorix puede cambiarse modificando la configuración en '/etc/monitorix/monitorix.conf'  
<br>
![estadísticas puerto de red]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-06-05-[HowTo]_Monitorix_instalacion_configuracion_uso_basico/monitorix_network_port.png)
<br>
<br>
## Configuración de *Monitorix*
Para habilitar que se pueda acceder desde otro ordenador a las estadísticas de Monitorix en la máquina donde éste está instalado hay que habilitar el puerto 8080 en su cortafuegos (el de la máquina donde está instalado Monitorix).  
Por ejemplo, en el caso de *ufw* esto se haría así:
~~~
sudo ufw allow 8080
~~~
De esta forma se podrá acceder a las estadísticas de Monitorix desde cualquier máquina de la red local (VPN incluída, obviamente).  
Si se desea acceder a esas estadísticas desde el exterior de la red local habría que disponer, además, de una VPN o bien abrir el puerto 8080 también en el router.  
<br>
**'/etc/monitorix/monitorix.conf'** es el archivo que contiene la configuración de Monitorix.
Si queremos ajustar parámetros, incorporar prestaciones, etcétera... conviene hacer una copia de seguridad antes de comenzar a modificarlo:
~~~
sudo cp -pRvf /etc/monitorix/monitorix.conf /etc/monitorix/monitorix.conf.bak001
~~~
La documentación de Monitorix es exhaustiva y puede consultarse [**<big>aquí</big>**](https://www.monitorix.org/documentation.html).  
<br>
![estadísticas sensores genéricos]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-06-05-[HowTo]_Monitorix_instalacion_configuracion_uso_basico/monitorix_gensens.png)
<br>
<br>
Por ejemplo, <big>**para configurar acceso mediante usuario y contraseña a las estadísticas de Monitorix**</big>:  

Primero hay que instalar el paquete que contiene el script htpasswd que sirve para encriptar la contraseña deseada e incorporarla al fichero '/var/lib/monitorix/htpasswd':
~~~
sudo systemctl stop monitorix
sudo apt-get update
sudo apt install apache2-utils
~~~

A continuación hay que editar '/etc/monitorix/monitorix.conf'
~~~
sudo nano /etc/monitorix/monitorix.conf
~~~
y dejar este fragmento así:
~~~bash
<httpd_builtin>
        enabled = y
        host =
        port = 8080
        user = nobody
        group = nobody
        log_file = /var/log/monitorix-httpd
        hosts_deny =
        hosts_allow =
        <auth>
                enabled = y
                msg = Monitorix: Restricted access
                htpasswd = /var/lib/monitorix/htpasswd
        </auth>
</httpd_builtin>
~~~
Ctrl+O (guardamos) y Ctrl+X (cerramos)  
Lo siguiente es crear el fichero '/var/lib/monitorix/htpasswd':
~~~
sudo touch /var/lib/monitorix/htpasswd
~~~
Y finalmente ejecutar el script 'htpasswd' que se incorporó al sistema cuando instalamos el paquete 'apache2-utils':
~~~
sudo htpasswd -d /var/lib/monitorix/htpasswd nombreusuario
~~~
Pedirá contraseña, la introduciremos y el script la cifrará e incorporará al fichero '/var/lib/monitorix/htpasswd' que creamos anteriormente.
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[Monitorix - Documentation](https://www.monitorix.org/documentation.html)  
[HowToForge](https://www.howtoforge.com/tutorial/performance-monitoring-with-monitorix-on-ubuntu-16-04/)  
[UbunLog](https://ubunlog.com/monitorix-una-herramienta-para-el-monitoreo-del-sistema/)</small>  
<br>
<br>
<br>
<br>
