---
layout: post
title: "El poder del lonchafinismo con un teléfono móvil viejo, vol01: Kazam Trooper 450 del año 2015)"
date: 2022-01-22
author: Termita
category: "hardware"
tags: ["hardware", "firmware", "sistemas operativos", "software", "flash", "rom", "custom rom", "lineage os", "degoogle", "root", "kazam trooper 450", "kazam", "trooper", "android 4", "android 4.4.2", "apple", "ipad", "iphone", "ipod", "itunes", "apps", "obsolescencia", "derecho a reparar", "apple store", "ios", "macOS", "windows", "hack", "crack", "retromatica", "lonchafinismo", "libertad", "recursos", "bloatware"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>En este mundo posmoderno cuando compramos una máquina no somos conscientes de que en realidad lo que estamos haciendo es pagar un dinero a cambio de usar una máquina durante un periodo de tiempo muy pequeño: apenas 5 años en muchas ocasiones.  
No es el caso que hoy me ocupa, en absoluto, el caso de un producto marca Apple en el que esto es más grave aún, sino de un móvil que ya en su día era "barato" (70€).  
La obsolescencia programada en máquinas que han sido diseñadas para durar -no estropearse- pero para quedarse obsoletas al cabo de unos años, es la práctica común y exagerada hoy día. Antaño existía, mas no a los niveles que vemos hoy, cuando además éstas impiden el "derecho a reparar" sus máquinas.  
Nos enfrentamos, pues, a una obsolescencia de las máquinas:
... a nivel ***software***: pasados unos años el sistema operativo, las aplicaciones, el soporte, etc... dejan de funcionar y/o de tener soporte.
... a nivel ***hardware***: dificultad, por no decir, imposibilidad de reparar, actualizar, mejorar, acceder a recambios, etcétera.
Quizás si todos fuéramos conscientes de todo esto en todo momento no compraríamos tan a la ligera esas bonitas máquinas que, al cabo de menos de una década -aún funcionando correctamente- nos van a *dejar tirados* sobre todo a nivel software.</small>  

<br>
<br>
*Smartphone* del año 2015 marca 'Kazam', modelo 'Trooper 450'
- Dimensions: 71.2 x 144.5 x 9.2 mm
- Weight: 148 g
- SoC: MediaTek MT6582M
- CPU: ARM Cortex-A7, 1300 MHz, Cores: 4
- GPU: ARM Mali-400 MP2, 416 MHz, Cores: 2
- RAM: 512 MB, 533 MHz
- Storage: 4 GB
- Memory cards: microSD, microSDHC
- Display: 5 in, IPS, 480 x 854 pixels, 24 bit
- Battery: 2000 mAh, Li-Ion
- OS: Android 4.4.2 KitKat
- Camera: 2560 x 1920 pixels, 1920 x 1080 pixels, 30 fps
- SIM card: Micro-SIM
- Wi-Fi: b, g, n, Wi-Fi Hotspot, Wi-Fi Direct
- USB: 2.0, Micro USB
- Bluetooth: 4.0
- Positioning: GPS, A-GPS
- 3G  

<br>
<a href="/assets/img/2022-01-22-el_poder_de_un_telefono_movil_viejo_vol01_kazam_trooper_450/kazam_trooper_450_rooteado.jpg" target="_blank"><img src="/assets/img/2022-01-22-el_poder_de_un_telefono_movil_viejo_vol01_kazam_trooper_450/kazam_trooper_450_rooteado.jpg" alt="zzz" width="300"/></a>  
<br>

Pues bien, ese es mi teléfono movil en 2022. La única pega es que echo de menos los teléfonos "no-inteligentes" como el [Motorola v3]() -menos quebraderos de cabeza, más toscos- y este no es de aquellos.  
Ortega decía 'Yo soy yo y mis circunstancias'. Ciertamente mis circunstancias no requieren otra máquina.  
Atendiendo a los datos de renta *per cápita* y de cualificación profesional de la población española, juraría que la mayoría de mis conciudadanos están en la mayoría de mis circunstancias. Mas, ¿cómo es posible que estos datos no se correspondan con las características de los smartphones superventas y el alto grado de renovación (deshecho y compra) de éstos en nuestro país?  
Se me ocurren muchas explicaciones, todas ellas -cabe señalar- contradictorias con la doctrina de la "calentología" o el cuidado medioambiental tan políticamente correcta como numerosa la tropa de fanáticos de los teléfonos móviles que *della* *facen* gala:
- moda, status y corriente dominante de pensamiento, según los cuales cambiarse de *smartphone* cada poco tiempo ni es malo, ni está mal visto, ni crea remordimiento alguno y sí es indicador de status, dinamismo y modernidad.
- porqueyolovalguismo. ¿Se acuerdan de aquellos ninjas (*no income, no job, no assets*) cipotecados por 300 kilotones a 30 años cuando en 2006 llegó aquella crisis inmobiliaria y financiera y asoló nuestro país? Segundas residencias, hipotecas que incluían adosado, beémeúve 320D y viaje al caribe, etcétera. Hubo dolor cuando la realidad, que es muy terca, mostró su cara más amarga. El lorealismo porqueyolovalguista siempre conlleva una mala adaptación al entorno, que tarde o temprano te destruirá.
- infantilismo e inmadurez: ¿Se imaginan a sus abuelos, que a los 20 años ya habían vivido una guerra e incluso ya tenían hijos, babeando con la mirada perdida absortos en una pantallita viendo gatitos? ¿Y el arado y la yunta de mulos para cuándo? Es esta una sociedad enferma, plagada de personas que son incapaces de estar a solas consigo mismos.
- pasión por la tecnología y lo novedoso. Sí, unos pocos son así; saben lo que compran, aunque no lo necesiten ni se lo merezcan. Algunos rechazan los ordenadores convencionales, sentencian la muerte del ordenador como "dispositivo único" en pro de los "teléfonos inteligentes", afirmación que se me antoja precipitada, por no decir descabellada o irreal. A algunos les sobra el dinero, a otros no. Suelen vender lo viejo después de comprar lo nuevo (quizás porque así las mermas son menos dolorosas, qué sé yo). A uno de estos le compré un ordenador portátil de tipo "gamer" por la mitad de lo que le costó unos meses atrás. Impecable. Yo no daba crédito. Daba que pensar presenciar como conforme pasaban las semanas iba rebajando el precio sin que nadie se interesara.  

<br>
En resumen: Yo no me merezco ni necesito otra cosa. Y me vendrá bien para incrementar aún más el poco apego que le tengo a este tipo de chismes.  

<br>
Porque recuperar un dispositivo así en 2022 tiene muchas ventajas (y algún que otro inconveniente):  

<br>
<br>
### Inconvenientes
- Versión antigua de 'Android'-4.4.2 'Kit-kat'-, juraría que plagada de agujeros de seguridad (no se piensen que las versiones modernas no lo están, pero la lógica hace sospechar que las versiones "abandonadas" están más expuestas). Habrá que ser cauto para minimizar riesgos. Nada de almacenar material personal, nada de banca, etc... Este móvil ha de ser tratado como lo que es: un teléfono que permite consultar chorradas por internet cuando no estás en casa. Aspecto positivo: no será elemento que ayude a disipar la naturaleza desconfiada o paranoica que todo usuario de tecnologías conectadas a la red habría de tener.  
- La batería ha vivido épocas mejores, no es nueva y no dura lo que duraba al principio. Una batería de reemplazo es cara, dado el valor irrisorio de este terminal. Habrá que usar poco el dispositivo. Como no hay cara sin cruz, existe un aspecto positivo: más tiempo para MÍ y para la vida real.
- Pantalla irritantemente poco sensible a las pulsaciones y, por consiguiente, poco "colaboradora" en momentos de prisa. </small>Aspecto positivo: no pulsaciones accidentales, autocontrol de las emociones.</small>
- Pantalla con poca resolución. <small>Aspecto positivo: jamás me quedaré en trance delante de ella, quizás me libre de ser atropellado por algún anormal en su patinete por la acera, también absorto en el 'Instagram' de su *smartphone*.</small>
- No es 4g, y -por consiguiente- tampoco 5g, es decir, no es veloz en internet. Eso no es un inconveniente para alguien como yo, mas hay que señalarlo. <small>Aspecto positivo: mi tarifa de 100 Mb. -sí, cien megas, ha leído usted bien- al mes por 1€ se adaptará bien, dado que cuando se consumen esos 100 Mb. se sigue disponiendo de conexión a internet mas la velocidad de navegación baja drásticamente. En esas ocasiones, un teléfono 3g navegaría a la misma ridícula velocidad que uno 5g.</small>
- Su conectividad Wifi no es 5ghz (wireless ac), sino 2'4ghz (wireless g).
- CPU poco potente, que la hacen incompatible con aplicaciones exigentes, como lo son los "juegos". <small>Aspecto positivo: Uso de aplicaciones espartanas, de esas que plagan el universo del software libre para dispositivos móviles, centrándose en una sola cosa a la vez (nada de multitarea, es decir, hacer muchas cosas y todas a medias). Me permitirá no malacostumbrarme, seguir siendo frugal.</small>
- RAM: notablemente escasa. <small>Ídem.</small>
- Almacenamiento escaso, ampliable -eso sí- mediante tarjeta SD. <small>Aspecto positivo: un refuerzo para no almacenar. Mientras quepa el software imprescindible, es suficiente.</small>
- Incompatible con las últimas versiones de muchas -cada vez más- aplicaciones. <small>Aspecto positivo: un teléfono es para hacer llamadas telefónicas, toda prestación que aumente eso de una manera coherente y sana es regalo del cielo.</small>
- Cada vez cuesta más encontrar en [*'F-Droid'*](https://f-droid.org/) *apps* compatibles con un *smartphone* tan viejo. Esto sí es un inconveniente preocupante.
- GPS bastante torpe. <small>Aspecto positivo: Refuerzo de la confianza habida en los mapas convencionales en soporte papel, los navegadores GPS tradicionales "offline" (bendito iGo) y en la capacidad de orientación. Y, por supuesto, esto redundará en mayor autonomía y menos información sobre mi ubicación regalada a los muchachos de Google.</small>
- Cámara fotográfica de pésima calidad. <small>Aspecto positivo: menos datos personales dentro de ese chisme y, de paso, continuar viviendo la vida, no fotografiándola ni almacenándola cual diógenes digital. La fotografía es un arte digno de ser llevado a cabo con cámaras fotográficas de verdad, no con teléfonos móviles. Si algo merece la pena ser fotografiado con calidad -y pocas cosas lo son-, fotografíese con el chisme apropiado.</small>
- Al rootearlo y eliminar aplicaciones -*bloatware*- del fabricante como, por ejemplo, la app de la "linterna"... es complicado que la app de código abierto que la sustituya aparezca en el menú deslizante superior, en el vacío que aquella dejó. No existe aspecto positivo para esto, salvo que habré de investigar (/aprender) cómo solucionarlo.
- Aparenta más de lo que es. Eso no es bueno para un tipo humilde como yo. <small>Aspecto positivo: no predispone a consultarlo en público y, sobre todo, en compañía de compañeros o familiares, a los que es menester prestárseles la atención y respeto que merecen.</small>

<br>
<br>
### Ventajas
- Excelente para rootearlo nada más empieza uno con él. Teléfonos vetustos como este incitan a la experimentación. El juguete no son sus aplicaciones, sino las "tripas" del sistema en sí, en las que puede hurgarse sin miedo al desastre.
- Ideal para optimizarlo / aligerarlo eliminando todo rastro de Google y sus servicios, sin pudor alguno, y de paso reforzar la privacidad.
- Ideal para optimizarlo / aligerarlo eliminando todo el *bloatware* del fabricante -Kazam- y sustituirlo por software libre.
- Permite modificarlo a fondo -hardware y software- sin miedo a romper nada: desoldar cámara delantera, micrófono, incorporar interruptores, crear baterías... lo que a uno se le ocurra siempre que el tiempo lo permita. Sin modificar, si uno es un flipado de la privacidad, por supuesto, un jack de auriculares con micrófono capado en la salida de audio / entrada de micrófono y un pegote de pintura en la cámara delantera, harán el apaño.
- Su lentitud, su escaso ancho de banda, su poca autonomía (que ocasiona que apagar los servicios (wifi, bluetooth, gps, datos móviles) depués de usarlos sea cuasi un automatismo) y su limitadísima capacidad de proceso (colapsarlo es fácil) lo hace *per se* poco favorable al envío fluído de información privada a corporaciones y/o gobiernos.
- Muy apropiado para llevar una vida frugal y consumir poco: gastar en él poco tiempo de vida humana, poco ancho de banda, 0 compras compulsivas / absurdas.
- Muy apropiado para no perder de vista que para ser productivo hay que echar mano siempre de un ordenador. Porque para trabajar están los ordenadores. El tiempo, ese bien preciado por lo escaso que es, ¿por qué malgastarlo frente a un móvil en tareas para cuyo desempeño, con una computadora, se tardaría menos de la mitad (y cometiendo menos errores)?
- Muy apropiado para ser productivo en el trabajo y no despistarse con cosas que nada tienen que ver con la actividad por cuyo desempeño uno cobra un salario.
- Muy resistente, puede ser incluso empleado como arma arrojadiza sin miedo.
- Su batería carga muy rápido.
- Su tamaño, comparando con las dimensiones de los teléfonos hoy en boga, es reducido, lo cual lo hace muy manejable. No soy partidario de las grandes pantallas panorámicas tan de moda hoy.
- Es capaz de reproducir sonido a un Volumen muy alto, lo cual es perfecto para reproducir podcast o, llegado el caso, para su uso como despertador.
- Dispone de radio "convencional" y *jack* de salida/entrada de audio, algo a mi juicio muy positivo y que, sin embargo, cada vez menos *smartphones* actuales tienen.  

<br>
<br>
<br>
Entradas relacionadas:  
- [Mirando cara a cara al abismo de la incoherencia posmoderna: un vistazo a la Tecnofilia creditofágica](https://hijosdeinit.gitlab.io/tenofilia_creditofagica/)
- [https://hijosdeinit.gitlab.io/Germen_rendicion_gnulinux_a_obsolescencia/](https://hijosdeinit.gitlab.io/Germen_rendicion_gnulinux_a_obsolescencia/)
- [[HowTo] Instalación netinstall de Debian en netbook Compaq Mini](https://hijosdeinit.gitlab.io/howto_instalacion_netinstall_Debian_netbook_compaqmini/)
- [[HowTo] Mantener vivo un iPad antiguo, vol.01: instalar aplicaciones 'no tenidas antes'](https://hijosdeinit.gitlab.io/howto_mantener_vivo_ipad_antiguo_vol01__instalar_aplicaciones_no_tenidas_antes/)
- [[HowTo] búsqueda de páginas web ‘desaparecidas’, directamente en la caché de Google](https://hijosdeinit.gitlab.io/howto_busqueda_en_cache_de_google_paginas_desaparecidas/)
- [Saque todo lo que pueda de los soportes ópticos (cd/dvd), si no lo ha hecho ya](https://hijosdeinit.gitlab.io/saque-todo-lo-que-pueda-de-los-soportes/)
- [Otra faceta de la vida útil de las memorias usb](https://hijosdeinit.gitlab.io/otra-faceta-vida-util-memoria-usb/)
- [Qué pedirle a un ‘pendrive’ USB](https://hijosdeinit.gitlab.io/requisitos_de_un_pendrive/)
- ["Gratis"](https://hijosdeinit.gitlab.io/lo_gratis/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
