---
layout: post
title: "[HowTo] Mejor forma de incorporar la última versión de 'youtube-dl' en Debian y derivados. II"
date: 2021-04-03
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "youtube-dl", "youtube", "audio", "video", "descargas", "python", "python 3", "python 2", "pip", "pip3", "degoogled", "privacidad", "anonimato", "tracking", "rastreo"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Hay sistemas operativos como Ubuntu que traen de serie el programa ['youtube-dl'](http://ytdl-org.github.io/youtube-dl/index.html), que permite descargar video y audio de Youtube y sitios de internet similares.  
Sin embargo, NO es una versión actualizada. En los repositorios de Ubuntu la versión de 'youtube-dl' es la última versión que el equipo de Canonical ha testeado a conciencia, NO la versión más reciente del desarrollador.  
Teniendo en cuenta que Google -el propietario de Youtube- cambia constantemente las características de su sitio, entre otras cosas para evitar que la gente acceda sin consumir publicidad a los videos que alberga, es importante que la versión de 'youtube-dl' que tengamos instalada en nuestro sistema sea la última.  
Sistemas operativos como Debian -por lo menos en su instalación *netinstall*- no traen 'youtube-dl' por defecto. Por consiguiente habremos de instalarlo a mano, lo cual, si lo que deseamos es tener la versión más actualizada del desarrollador, nos evitará tener que desinstalar previamente.  

<br>
En [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_mejor_instalacion_youtube-dl/) ya se señaló la mejor forma de incorporar la versión actualizada de 'youtube-dl'. Pues bien, he aquí **<big>otra manera</big>**.  

<br>
<br>
## Procedimiento de instalación de 'youtube-dl' mediante 'pip'

### Dependencias necesarias  
Necesitaremos tener 'pip' instalado.  
Para 'Python 3', 'pip3' se instala así:
~~~bash
sudo apt-get update
sudo apt install python3-pip
pip3 --version
~~~
Para 'Python 2', 'pip' se instala así:
~~~bash
sudo apt-get update
sudo apt install python-pip
pip --version
~~~

<br>
### Instalación de la última versión de 'youtube-dl' mediante 'pip'
Si utilizamos 'pip3', es decir, 'pip' para 'Python 3':
~~~bash
sudo pip3 install --upgrade youtube-dl
~~~
Si utilizamos 'pip', es decir, 'pip' para 'Python 2':
~~~bash
sudo pip install --upgrade youtube-dl
~~~

<br>
### Actualización de la última versión de 'youtube-dl' mediante 'pip'
Es el mismo procedimiento que para la instalación, de ahí el parámetro `--upgrade`.  

<br>
### ¿Y si tengo Ubuntu (o similar) y 'youtube-dl' ya venía de serie?
Sencillo:
- Desinstálese 'youtube-dl' vía apt (`sudo apt --purge remove youtube-dl`)
- Reinstálese 'youtube-dl' mediante el procedimiento señalado anteriormente.  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Mejor forma de incorporar la última versión de 'youtube-dl' en Debian y derivados. II](https://hijosdeinit.gitlab.io/howto_youtube-dl_debian_netinstall/)
- [[HowTo] 'invidious', 'Youtube' desde el navegador sin publicidad ni rastreo](https://hijosdeinit.gitlab.io/howto_invidious_youtube_desde_navegador_sin_publicidad/)
- [NewPipe: contenidos de youtube con privacidad y con privilegios de cliente de pago (sin pagarles un céntimo)](https://hijosdeinit.gitlab.io/newpipe-contenidos-de-youtube-como-si/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="http://ytdl-org.github.io/youtube-dl/download.html">youtube-dl - página oficial</a>  
<a href="https://linuxize.com/post/how-to-install-pip-on-debian-10/">Linuxize - instalación de pip</a>  
<a href="https://www.makeuseof.com/apt-get-uninstall/">MakeUseOf - desinstalación de software mediante apt</a><small>  

<br>
<br>
<br>
<br>
<br>
<br>
