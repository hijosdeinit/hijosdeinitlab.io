---
layout: post
title: "Listado de métodos / sistemas de backup en GNU Linux"
date: 2019-12-07
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "tar", "comprimir", "empaquetar", "respaldo", "backup", "copia de seguridad", "compresores", "gzip", "rar", "unrar", "7zip", "lzma", "zip", "unzip", "arj", "log", "registro", "registro de errores", "actividad", "entrada", "salida", "input", "output", "rsync", "timeshift", "borg backup", "borg", "acronis trueimage", "trueimage", "norton ghost", "ghost", "clonezilla", "tar", "fog project", "rdiff", "rdiff backup", "lucky backup", "deja dup", "deja", "back in time", "get your data back", "backup ninja", "rsnapshot", "rclone"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
* Acronis TrueImage (<small>software propietario, posibilidad liveusb</small>)
* Norton Ghost (<small>software propietario</small>)
* Clonezilla
* tar
* Fog Project
* rsync
* rdiff-backup
* lucky-backup
* deja-dup
* TimeShift
* Borg Backup
* Back-in-Time
* ¿get your data back?
* backup ninja
* rsnapshot
* ¿rclone?  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Instalación de 'timeshift' en Debian y derivados](https://hijosdeinit.gitlab.io/howto_timeshift_debian/)
- [[HowTo] Transferir un backup completo de un sistema GNU Linux a un disco NTFS](https://hijosdeinit.gitlab.io/howto-backup_sistema_gnulinux_a_almacenamiento_ntfs/)
- [[HowTo] Mostrar el progreso de un backup con 'dd'](https://hijosdeinit.gitlab.io/howto_dd_progreso_verbose/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="">zzz</a>  
<a href="">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
