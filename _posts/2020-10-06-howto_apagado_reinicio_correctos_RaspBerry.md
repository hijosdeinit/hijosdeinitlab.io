---
layout: post
title: '[HowTo] Apagado y Reinicio correctos en RaspBerry Pi'
date: 2020-10-06
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "RaspBerry Pi", "seguridad", "hardware", "apagado", "reinicio", "halt", "poweroff", "shutdown", "init", "reboot"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Es conveniente apagar -y reiniciar- las máquinas correctamente. Todo el mundo lo sabe, y RaspBerry Pi no es la excepción.  
Esta máquina no trae botón de apagado/encendido de serie y, si se trabaja desde línea de comandos, no existe un menú desde el cual seleccionar con el ratón el apagado/reinicio.  

<br>
## Reinicio correcto
<br>
### reboot
~~~bash
sudo reboot
~~~

<br>
### shutdown
~~~bash
sudo shutdown -r now
~~~
~~~bash
sudo shutdown -r 10
~~~

<br>
### init
~~~bash
sudo init 6
~~~


<br>
<br>
## Apagado correcto
<br>
### halt
~~~bash
sudo halt
~~~

<br>
### poweroff
~~~bash
sudo poweroff
~~~

<br>
### shutdown
~~~bash
sudo shutdown -h now
~~~
~~~bash
sudo shutdown -h 10
~~~

<br>
### init
~~~
sudo init 0
~~~

<br>
Cuando el proceso de apagado termina el led verde parpadea varias veces, el led rojo se mantiene encendido. Después de esto es seguro desconectar la máquina de la electricidad.  

<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[shellhacks](https://www.shellhacks.com/raspberry-pi-shutdown-reboot-safely-command/)</small>  

<br>
<br>
<br>
<br>
