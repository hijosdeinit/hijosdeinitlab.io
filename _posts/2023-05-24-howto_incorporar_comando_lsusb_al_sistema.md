---
layout: post
title: "[HowTo] Incorporar el comando 'lsusb' al sistema"
date: 2023-05-24
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "debian", "sistemas operativos", "gnu linux", "linux", "debian", "lsusb", "usbutils", "instalacion", "howto"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
'Debian 11 x64 lxde *nonfree*' no incorpora de serie el comando <big><span style="background-color:#042206"><span style="color:lime">`lsusb`</span></span></big>, sí el comando 'lspci' ('pciutils'). <small>A veces uno da por hecho que un comando está -porque siempre suele estar- y, cuando éste hace falta, se constata que efectivamente no se encuentra instalado. Algo similar me ocurrió, en otra ocasión, con el comando 'ifconfig'</small>.  

El comando <span style="background-color:#042206"><span style="color:lime">'lsusb'</span></span> forma parte del paquete 'usbutils'. Para instalarlo:
~~~bash
sudo apt-get update
sudo apt install usbutils
~~~

<br>
<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[HowtoGeek](https://www.howtogeek.com/devops/how-to-use-lsusb-in-linux-with-a-practical-example/)  
[cyberithub](https://www.cyberithub.com/how-to-install-lspci-lsscsi-lsusb-and-lsblk-tools-in-redhat-centos-7/)</small>  
<br>
<br>
<br>
<br>
