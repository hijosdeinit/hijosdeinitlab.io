---
layout: post
title: "[HowTo] 'vnc viewer' ('realvnc') en Debian 10. Visor vnc para controlar RaspBerry Pi"
date: 2021-09-12
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "vnc viewer", "realvnc", "vnc", "remoto", "control remoto", "x forwarding", "productividad", "raspberry pi", "ssh"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
'RaspBerry Pi OS', al igual que 'RaspBian' -su antecesor-, trae de serie -aunque desactivado (hay que activarlo a mano)- el **servidor** VNC de 'RealVNC'.  
¿Qué software **cliente** utilizar en las máquinas cliente, valga la redundancia? El cliente de 'RealVNC', ['vnc viewer'](https://www.realvnc.com/es/connect/download/viewer/), es una buena opción en mi caso. En principio es *open source*.  

<br>
## Instalación de 'vnc viewer' en Debian 10
<small>**Entorno:**  
Debian 10 32bits liveusb persistente.  
PC Intel Core2Duo  
RaspBerry Pi 3b</small>  
<br>
1. [Descárguese desde la página oficial](https://www.realvnc.com/es/connect/download/viewer/) el paquete '.deb' adecuado al sistema y arquitectura que vamos a utilizar.  
En mi caso, que voy a instalarlo en una máquina Debian x86 de 32 bits:
~~~
wget 'https://www.realvnc.com/download/file/viewer.files/VNC-Viewer-6.21.406-Linux-x86.deb'
~~~

2. Instálese
~~~bash
sudo dpkg -i VNC-Viewer-6.21.406-Linux-x86.deb
~~~

<br>
## Ejecución y funcionamiento
El comando que lanza este visor vnc es `vncviewer`. Tras la instalación habrá un enlace en alguna parte: en el lanzador, en el menú de ejecución, etc.  
Una vez lanzado, pulsando <span style="background-color:#042206"><span style="color:lime">`ctrl`</span></span>+ <span style="background-color:#042206"><span style="color:lime">`n`</span></span> ha de crearse una nueva conexión.  

<br>
<a href="/assets/img/2021-09-12-howto_vncviewer_realvnc_un_visor_vnc_para_controlar_raspberrypi/vncviewer.png" target="_blank"><img src="/assets/img/2021-09-12-howto_vncviewer_realvnc_un_visor_vnc_para_controlar_raspberrypi/vncviewer.png" alt="'vncviewer'" width="600"/></a>  
<small>'vncviewer'</small>  
<br>

Introdúzcase la ip de RaspBerry Pi, su correspondiente usuario y su contraseña... y, finalmente, púlsese en 'Aceptar'. De esta forma conectaremos remotamente vía VNC a nuestra RaspBerry Pi y se habrá creado un perfil en 'vnc viewer' que no es más que un acceso directo para facilitar la conexión en las siguientes ocasiones.  
Aunque RaspBerry Pi no esté conectada a una pantalla, [se puede iniciar sesión vía vnc](https://www.raspberrypi.org/documentation/computers/remote-access.html).  

<br>
<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.raspberrypi.org/documentation/computers/remote-access.html">raspberrypi.org - documentation - remote access</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
