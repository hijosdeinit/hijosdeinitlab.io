---
layout: post
title: "[HowTo] 'Kakoune', un editor de texto alternativo a 'Vi' / 'Vim' en Debian y derivados"
date: 2022-01-09
author: Termita
category: "sistemas operativos"
tags: ["ide", "editor", "código", "texto", "texto plano", "programación", "sistemas operativos", "gnu linux", "linux", "vi", "vim", "nano", "emacs", "atom", "eclipse", "sublime text", "visual studio code", "vscodium", "codium", "vscode", "netbeans", "micro", "kakoune", "gnu linux", "linux", "debian", "windows"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
['kakoune'](https://kakoune.org/) es un editor de texto para la terminal que se asemeja a 'vi' / 'vim', entre otras cosas porque es 'modal' e incorpora atajos de teclado similares. En la [*wiki* de 'kakoune' detallan las diferencias](https://github.com/mawww/kakoune/wiki/Migrating-from-Vim).  
El repositorio de 'kakoune' en GitHub es: [https://github.com/mawww/kakoune](https://github.com/mawww/kakoune)  
Características:
- Es multiplataforma -GNU Linux, MacOS, Windows- y multiarquitectura -32bits, 64bits, ¿arm?-.  
- It is interactive, predictible, and fast.
- Supports multiple selections.
- Supports syntax highlighting.
- It operates in two modes: normal and insertion.
- Uses less keystrokes making it fast.
- Supports auto-information display.
- Also supports extensive auto-completion.
- Offers multiple text editing tools.
- It supports working with external programs.
- Supports advanced text manipulation primitives.
- Uses powerful handling primitives such as regex matches, filtering, splitting, aligning, text objects and more.  

Multiple selections as a central way of interacting  
Powerful selection manipulation primitives  
   Select all regex matches in current selections  
   Keep selections containing/not containing a match for a given regex  
   Split current selections with a regex  
   Text objects (paragraph, sentence, nestable blocks)  
Powerful text manipulation primitives  
   Align selections  
   Rotate selection contents  
   Case manipulation  
   Indentation  
   Piping each selection to external filter  
Client-Server architecture  
   Multiple clients on the same editing session  
   Use tmux or your X11 window manager to manage windows  
Simple interaction with external programs  
Automatic contextual help  
Automatic as you type completion  
Macros  
Hooks  
Syntax Highlighting  
   Supports multiple languages in the same buffer  
   Highlight a buffer differently in different windows  

<br>
<a href="/assets/img/2022-01-09-howto_kakoune_editor_de_texto/kakoune.png" target="_blank"><img src="/assets/img/2022-01-09-howto_kakoune_editor_de_texto/kakoune.png" alt="'kakoune'" width="500"/></a>  
<small>'kakoune'</small>  
<br>

<br>
<br>
## Instalación de 'kakoune' en Debian y derivados

<br>
### a. Instalación de 'kakoune' de forma nativa mediante desde los repositorios ('apt') en Debian y derivados
~~~bash
sudo apt-get update
sudo apt install kakoune
~~~

<br>
### b. Instalación de 'kakoune' a partir del código fuente (compilando)
Es necesario que el sistema tenga [instalados los componentes necesarios para compilar software]().  
~~~bash
sudo apt update && sudo apt install build-essential libncurses5-dev libncursesw5-dev asciidoc
cd Downloads/
git clone http://github.com/mawww/kakoune.git
cd kakoune/src
make
make man
sudo make install
~~~
<small>NO he probado aún este procedimiento porque instalé 'kakoune' mediante 'apt'.</small>  

<br>
<br>
### c. Instalación de 'kakoune' mediante *snap* en Debian y derivados
**ℹ** <small>Requisito imprescindible es tener [*snap* instalado y activado en el sistema](https://hijosdeinit.gitlab.io/howto_snap_en_debian_10_buster_y_derivados/)</small>
~~~bash
sudo snap install kakoune --classic
~~~
> kakoune v2021.08.28 from LukeWH installed  

**ℹ** Para desinstalarlo **supuestamente** bastaría con:
~~~bash
sudo snap remove kakoune
~~~

<br>
<br>
## Ejecución de 'kakoune'
Para lanzarlo basta con ejecutar:
~~~bash
kakoune
~~~
ô
~~~bash
kak
~~~

<br>
<a href="/assets/img/2022-01-09-howto_kakoune_editor_de_texto/kakoune-tmux.png" target="_blank"><img src="/assets/img/2022-01-09-howto_kakoune_editor_de_texto/kakoune-tmux.png" alt="'kakoune' en 'tmux'" width="500"/></a>  
<small>'kakoune' en 'tmux'</small>  
<br>

En el [repositorio oficial de 'kakoune' en 'GitLab' hay un manual de usuario muy completo](https://github.com/mawww/kakoune).  
Asimismo existe un [foro oficial](https://discuss.kakoune.com) y un canal de IRC -`#kakoune`- en el servidor 'Libera IRC' donde consultar y resolver dudas, ayudar a otros usuarios o plantear mejoras.  

<br>
## Configuración de 'kakoune'
'kakoune', al igual que 'vim' y similares, [dispone de bastantes *plugins*](https://kakoune.org/plugins.html) para añadirle prestaciones o hacerlo más "amable".  
 
<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] El editor (IDE) atom y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [[HowTo] Evitar que 'atom' elimine espacios en blanco al final de línea](https://hijosdeinit.gitlab.io/howto_atom_espacios_blanco_final_linea/)
- [[HowTo] Agregar idiomas al editor (IDE) atom. Ponerlo en español](https://hijosdeinit.gitlab.io/howto_poner_atom_en_espa%C3%B1ol/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- [[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)
- [[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados](https://hijosdeinit.gitlab.io/howto_micro_editor_texto_debian/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [[HowTo] Añadir resaltado de sintaxis al editor de textos NANO](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [[HowTo] nano: parámetros de arranque útiles](https://hijosdeinit.gitlab.io/howto_parametros_utiles_arranque_nano/)
- [En nano no existe overtyping](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/)
- [[HowTo] Comienzo con vim. I](https://hijosdeinit.gitlab.io/howto_comienzo_con_vim_1/)
- [[HowTo] Visualización de 'markdown' en la línea de comandos (CLI): 'MDLESS'](https://hijosdeinit.gitlab.io/howto_mdless_visor_markdown_cli/)
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.tecmint.com/kakoune-better-code-editor-for-linux/">TecMint</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
