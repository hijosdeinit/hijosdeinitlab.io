---
layout: post
title: "Alternativas a 'Youtube' menos tóxicas"
date: 2021-06-13
author: Termita
category: "internet 4.0"
tags: ["internet 4.0", "multimedia", "privacidad", "censura", "screencast", "monetizacion", "cuantificacion", "respeto", "libertad", "grandes tecnologicas", "video", "audio", "youtube", "red social", "odysee", "lbry", "bitchute", "rumble", "archive.org", "peertube", "invidious", "yewtu.be", "frontend"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<br>
> Todo libro que valga la pena prohibir es un libro que vale la pena leer. (Isaac Asimov)  

<br>
Las grandes tecnológicas -propietarias de las plataformas de video en streaming preponderantes- son corporaciones transnacionales que tienen hoy un poder inmenso. No hay visos de que esta tendencia vaya a cambiar a corto o medio plazo.  
Tienen productos hegemónicos, que todo el mundo usa, y gratuítos en los que en realidad el producto es el propio usuario. En los últimos años se observa que no basta con invadir la privacidad de las personas, cuantificar y monetizar: Por diversas razones llevan aplicando **censura** de contenidos. Los motivos no siempre son transparentes ni comunicados. Una de estas poco respetuosas plataformas es 'Youtube', propiedad del gigante tecnológico 'Alphabet' (Google).  

<br>
He aquí algunas plataformas que respetan -[de momento](https://hijosdeinit.gitlab.io/lo_gratis/)- un poco más al usuario:
- [peertube](https://peertube.tv/): libre y descentralizada.
- [Odysee](https://odysee.com/) / [lbry](https://lbry.com/get?src=lbrytv-retired)
- [Bitchute](https://www.bitchute.com/)
- [Rumble](https://rumble.com/)
- [Archive.org](https://archive.org/)
- [superocho](https://superocho.org)
- [invidio.us](https://redirect.invidious.io/): diversas instancias. Es un frontend de acceso a 'Youtube' que permite reproducir sus videos sin experimentar las malas artes de 'Google'. <small>[En este blog se dedicó una entrada a este respetuoso frontend para 'youtube'](https://hijosdeinit.gitlab.io/howto_invidious_youtube_desde_navegador_sin_publicidad/)</small>.   

<br>
<br>
Entradas relacionadas:  
- ["Gratis"](https://hijosdeinit.gitlab.io/lo_gratis/)
- ['NewPipe: contenidos de youtube con privacidad y con privilegios de cliente de pago (sin pagarles un céntimo)](https://hijosdeinit.gitlab.io/newpipe-contenidos-de-youtube-como-si/)
- [[HowTo] 'invidious', 'Youtube' desde el navegador sin publicidad ni rastreo](https://hijosdeinit.gitlab.io/howto_invidious_youtube_desde_navegador_sin_publicidad/)
- [[HowTo] Obtención de 'ID' (embed) de un video de 'Rumble.com' y descargarlo mediante 'youtube-dl'](https://hijosdeinit.gitlab.io/howto_obtencion_id_video_rumble_para_descargarlo_youtubedl/)
- [[HowTo] Mejor forma de incorporar la última versión de 'youtube-dl' en Debian y derivados. II](https://hijosdeinit.gitlab.io/howto_youtube-dl_debian_netinstall/)
- [[HowTo] Mejor forma de instalar 'youtube-dl' en Debian y derivados. I](https://hijosdeinit.gitlab.io/howto_mejor_instalacion_youtube-dl/)
- [[HowTo] Descargar subtítulos de Youtube, nativos y/o autogenerados en el idioma que queramos](https://hijosdeinit.gitlab.io/howto_descargar_subtitulos_youtube_nativos_autogenerados/)  

<br>
<br>
<br>
<br>
<br>
<br>
