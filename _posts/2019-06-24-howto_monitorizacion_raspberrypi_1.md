---
layout: post
title: '[HowTo] Monitorización de RaspBerry Pi [volumen 1]: rpi-monitor'
date: 2019-06-24
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "almacenamiento", "cliente", "memoria", "RAM", "Monitorix", "Monitoriza-Tu-Raspberry", "monitorización", "procesos", "Raspberry-Pi-Status", "rpi-monitor", "servicios", "servidor", "temperatura", "voltaje", "frecuencia", "raspberry", "servidor", "seguridad", "gnu linux", "linux", "debian", "cli"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Instalé [rpi-monitor](https://rpi-experiences.blogspot.com/) siguiendo las [instrucciones de Atareao en su web](https://www.atareao.es/tutorial/raspberry-pi-primeros-pasos/monitorizar-la-raspberry-pi-con-rpi-monitor/).

Iniciar
~~~bash
sudo systemctl start rpimonitor
~~~

Parar
~~~bash
sudo systemctl stop rpimonitor
~~~

Reiniciar (hay que hacerlo siempre que cambiemos algún parámetro de la configuración)
~~~bash
sudo systemctl restart rpimonitor
~~~

Estado
~~~bash
systemctl status rpimonitor
~~~

<br>
<br>
Me quedan dos cosas por hacer:  
1. Averiguar cómo obtener esos datos directamente desde línea de comandos.  
2. Pulir y configurar todo un poco más en rpi-monitor; hacer que muestre los procesos y la carga que suponen al sistema, que monitorice la red, etc... Dejarlo parecido a esta imagen:  
<br>
<a href="/assets/img/2019-06-24-howto_monitorizacion_raspberrypi_1/RPi-Monitor_3ColumnsStatusPage (1).png" target="_blank"><img src="/assets/img/2019-06-24-howto_monitorizacion_raspberrypi_1/RPi-Monitor_3ColumnsStatusPage (1).png" alt="interfaz rpi-monitor customizada" width="800"/></a>  
<small>interfaz rpi-monitor *customizada*</small>  

<br>
Podría empezar por este mensaje de rpi-monitor:  
>«To activate network monitoring, edit and customize network.conf  
Help is available in man pages:  
man rpimonitord or man rpimonitord.conf»  

Para finalizar, señalar que exiten algunas aplicaciones más para monitorizar de forma remota RaspberryPi. De eso trata [otra de las entradas de este blog](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_2/):

Utilidad Monitoriza-Tu-Raspberry (de Atareao.es)  
[https://github.com/atareao/monitoriza_tu_raspberry](https://github.com/atareao/monitoriza_tu_raspberry)  
[https://www.atareao.es/podcast/monitorizacion-o-que-pasa-en-mi-raspberry/](https://www.atareao.es/podcast/monitorizacion-o-que-pasa-en-mi-raspberry/)  

<br>
Raspberry-Pi-Status  
[https://geekytheory.com/panel-de-monitorizacion-para-raspberry-pi-con-node-js](https://geekytheory.com/panel-de-monitorizacion-para-raspberry-pi-con-node-js)  
[https://www.somosbinarios.es/monitor-raspberry-pi-navegador/](https://www.somosbinarios.es/monitor-raspberry-pi-navegador/)  
[https://github.com/GeekyTheory/Raspberry-Pi-Status](https://github.com/GeekyTheory/Raspberry-Pi-Status)  

<br>
Monitorix  
[https://www.monitorix.org/](https://www.monitorix.org/)  
[hijosdeinit](https://hijosdeinit.gitlab.io/howto_Monitorix_instalacion_configuracion_uso_basico/)  
[https://www.forocoches.com/foro/showthread.php?t=4705042](https://www.forocoches.com/foro/showthread.php?t=4705042)  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Monitorización de RaspBerry Pi [volumen 2]: Monitorix, Raspberry-Pi-Status y Monitoriza-Tu-Raspberry](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_2/)
- [[HowTo] Monitorización de RaspBerry Pi [volumen 3]: temperaturas de CPU y GPU, frecuencia y voltaje desde línea de comandos (CLI)](https://hijosdeinit.gitlab.io/howto_monitorizacion_raspberrypi_3_temperaturas/)
- [[HowTo] Monitorix: instalación, configuracion, uso básico](https://hijosdeinit.gitlab.io/howto_Monitorix_instalacion_configuracion_uso_basico/)
- [[HowTo] 5 comandos para monitorizar la memoria RAM (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_monitorizar_memoria_ram_debian_derivados/)
- [[HowTo] Incorporar monitor de sistema a CrunchBang++ y parientes similares](https://hijosdeinit.gitlab.io/howto_monitor_sistema_en_Crunchbang_Linux_derivados/)  

<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[atareao.es](https://www.atareao.es/tutorial/raspberry-pi-primeros-pasos/monitorizar-la-raspberry-pi-con-rpi-monitor/)  
[rpi-experiences](https://rpi-experiences.blogspot.com/)  
[rpi-monitor](https://github.com/XavierBerger/RPi-Monitor)</small>  
<br>
<br>
<br>
<br>
