---
layout: post
title: "[HowTo] Instalación de tarjeta de red inalámbricas Realtek 'RTL8822BE' en Debian 11 y derivados"
date: 2021-11-28
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "ubuntu", "compilar", "compilacion", "make", "install", "instalar", "instalacion", "controlador", "driver", "modulo", "kernel", "clean", "desinstalacion", "desinstalar", "RTL8812AU", "realtek", "wireless", "aircrack", "linux-headrs", "dkms", "firmware"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno: Lenovo Legion; cpu Intel® Core™ i7-8750H CPU @ 2.20GHz × 12, 16gb RAM, gpu NVIDIA GeForce GTX 1050 Mobile, LiveUSB Debian 11 64bits LXDE, kernel Linux Debian 5.10.0-9-amd64 #1 SMP Debian 5.10.70-1 (2021-09-30) x86_64 GNU/Linux</small>  

<br>
Como cabía esperar, el sistema operativo LiveUSB Debian 11 64bits LXDE que estoy utilizando no incorpora los controladores de mi tarjeta de red inalámbrica Realtek 'RTL8822BE'. El dispositivo en cuestión es este:
~~~bash
lspci
~~~
> 07:00.0 Network controller: Realtek Semiconductor Co., Ltd. RTL8822BE 802.11a/b/g/n/ac WiFi adapter  

<br>
<br>
<br>
~~~bash
sudo apt-get update
sudo apt install firmware-realtek
sudo reboot
~~~
Tras el reinicio, la tarjeta de red inalámbrica Realtek será detectada correctamente
~~~bash
sudo iwconfig
~~~
~~~
wlp7s0    IEEE 802.11  ESSID:off/any
Mode:Managed
Access Point: Not-Associated
Tx-Power=20 dBm
Retry short limit:7
RTS thr:off
Fragment thr:off
Encryption key:off
Power Management:on
~~~
... y ya podrá asociarse / conectarse.  
En LXDE, por ejemplo, esto puede hacerse mediante 'Conman': menú lxde → 'conman Settings'  

<br>
<br>
Entradas relaccionadas:  
- [[HowTo] Instalación de tarjetas de red inalámbricas con chip Realtek 'RTL8812AU' en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_RTL8812AU_debian/)
- [[HowTo] Reactivación de tarjeta wireless ac 'Realtek RTL8812AU' tras actualización del kernel en Debian y derivados](https://hijosdeinit.gitlab.io/howto_reactivacion_tarjetawirelessac_realtek_rtl8812au_tras_actualizacion_kernel_debian_derivados/)
- [[HowTo] Instalación de los controladores de la tarjeta wireless Realtek RTL8822CE. (Debian 10 y derivados)](https://hijosdeinit.gitlab.io/howto_instalacion_controladores_tarjeta_wireless_Realtek_rtl8822ce/)
- [El repositorio de firmare/drivers de GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- [[HowTo] Preparar Debian y derivados para compilar software](https://hijosdeinit.gitlab.io/howto_preparar_debian_para_compilar/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
