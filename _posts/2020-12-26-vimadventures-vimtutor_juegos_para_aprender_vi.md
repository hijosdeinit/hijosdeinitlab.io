---
layout: post
title: "Juegos para aprender 'vi': 'vimadventures', 'vimtutor', 'pacvim', 'openvim tutorial'"
date: 2020-12-26
author: Termita
category: "productividad"
tags: ["vim", "juegos", "formación", "aprendizaje", "editor de texto", "vi", "nano", "texto plano", "vimtutor", "vim adventures", "pacvim", "tutorial", "openvim tutorial"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Hay, varios "juegos" / tutoriales para practicar vim -o vi-:  
- 'vimtutor'
- 'pacvim'
- 'Vim Adventures'
- 'OpenVim Tutorial'  

<br>
<br>
## 'vimtutor'
Cuando instalamos 'vim' se instala también 'vimtutor'. Es un tutorial en la línea de aquellos que las aplicaciones -dbaseIII, wordperfect, etc- incorporaban en los años 80 y 90 del pasado siglo.
~~~bash
vimtutor
~~~
<br>
<a href="/assets/img/2020-12-26_vimadventures_vimtutor_juegos_para_aprender_vi/vimtutor.png" target="_blank"><img src="/assets/img/2020-12-26_vimadventures_vimtutor_juegos_para_aprender_vi/vimtutor.png" alt="'vimtutor'" width="500"/></a>  
<small>'vimtutor'</small>  
<br>

<br>
<br>
## 'pacvim'
['pacvim'](https://github.com/jmoon018/PacVim)  

<br>
<a href="/assets/img/2020-12-26_vimadventures_vimtutor_juegos_para_aprender_vi/pacvim.gif" target="_blank"><img src="/assets/img/2020-12-26_vimadventures_vimtutor_juegos_para_aprender_vi/pacvim.gif" alt="'pacvim'" width="500"/></a>  
<small>'pacvim'</small>  
<br>

Para instalarlo en Debian y derivados:
~~~bash
sudo apt-get update
sudo apt install libncurses5-dev
git clone https://github.com/jmoon018/PacVim.git
cd PacVim
sudo make install
~~~
Para ejecutarlo, desde el subdirectorio donde lo compilamos, basta con ejecutar
~~~bash
./pacvim
~~~

<br>
<br>
<br>
## 'Vim Adventures'
[Vim Adventures](https://vim-adventures.com/)  
[soluciones del juego 'Vim Adventures](https://github.com/pepers/vim-adventures)  

<br>
<a href="/assets/img/2020-12-26_vimadventures_vimtutor_juegos_para_aprender_vi/vimadventures.png" target="_blank"><img src="/assets/img/2020-12-26_vimadventures_vimtutor_juegos_para_aprender_vi/vimadventures.png" alt="'vim adventures'" width="500"/></a>  
<small>'Vim Adventures'</small>  
<br>

<br>
<br>
## 'OpenVim Tutorial'
['OpenVim Tutorial'](https://www.openvim.com/tutorial.html)

<br>
<a href="/assets/img/2020-12-26_vimadventures_vimtutor_juegos_para_aprender_vi/openvimtutorial.png" target="_blank"><img src="/assets/img/2020-12-26_vimadventures_vimtutor_juegos_para_aprender_vi/openvimtutorial.png" alt="'OpenVim Tutorial'" width="500"/></a>  
<small>'OpenVim Tutorial'</small>  
<br>

<br>
<br>
<br>
Entradas relacionadas:  
- []()
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz" target="_blank">zzz</a></small>  

<br>
<br>
<br>
<br>
