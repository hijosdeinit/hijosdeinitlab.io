---
layout: post
title: "ThinkPad T470"
date: 2022-06-22
author: Termita
category: "hardware"
tags: ["hardware", "ibm", "exIBM", "lenovo", "thinkpad", "t470", "boot menu", "bios", "uefi", "fast boot", "secure boot", "live usb", "gnu linux", "Windows", "Windows 10", "sistemas operativos", "instalacion", "arranque", "booteable", "bootable", "teclado", "keyboard", "teclas", "pulsacion", "ibm model f", "ibm model m", "unicomp", "switch", "pulsadores"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Las computadoras de IBM tienen numerosos seguidores. Tuve la ocasión de teclear en alguna -siempre ajena-: el modelo que creo fue el primer PC de la historia con su diskettera de 5 1/4 y su encantadora y diminuta pantalla de fósforo naranja, un PS2 486, y un portátil IBM ThinkPad de los de antes, cuando todavía se llamaban IBM. Máquinas, todas ellas, muy robustas, muy bien hechas... y también obscenamente caras.  
Tengo entendido que aquella división de portátiles de IBM acabó llamándose 'Lenovo' para, años más tarde, ser vendida a los chinos, que mantuvieron el nombre y el apelativo 'ThinkPad'. El hecho de que los nuevos dueños sean chinos no implica que la gama 'ThinkPad' haya perdido toda su esencia; ésta se mantiene, aunque con ciertos cambios.  
Más vale tarde que nunca, me he hecho con un ThinkPad "de los de ahora": un [Lenovo **ThinkPad** T470](). Tuvo otro amo antes, mas eso a estas alturas no importa. A pesar de ser "chino" aún transmite algunas sensaciones que me son familiares, reminiscencias de un pasado en el que las máquinas se hacían para durar, para poder ser reparadas. Lamentablemente muchos ThinkPad de hoy -entre ellos el mío- tienen el procesador soldado a la placa base, por poner un ejemplo.  
El teclado, no obstante, es -comparado con lo que hoy otras marcas y modelos venden- excelente, con buen tacto, buen recorrido y buen sonido.  El '*trackpoint*' o 'pointing stick' -ese botoncito rojo situado en medio del teclado- está presente, el color negro azabache, las formas cuadradas y la sensación de solidez se mantienen.  
Asimismo la máquina alberga 2 baterías -una interna y otra externa- Ambas le otorgan una autonomía de aproximadamente 6 horas.  

<br>
El chisme viene con Windows 10 preinstalado y merece una copia de seguridad total y una instalación de GNU Linux. Creo que me decidiré por Debian. Bien, basta de sentimentalismo y vayamos al grano... Instalaré Debian.  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Arrancar ThinkPad Desde USB (Peleándome Con un ThinkPad, Vol.01)](https://hijosdeinit.gitlab.io/howto_thinkpad_arranque_usb/)
- [Teclados Mecánicos De Hoy. La Herencia Del '*Buckling Spring*'](https://hijosdeinit.gitlab.io/buckling_spring_teclados_mecanicos/)
- []()  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[]()  
[]()</small>  

<br>
<br>
<br>
<br>
