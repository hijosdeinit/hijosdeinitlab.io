---
layout: post
title: "[HowTo] Corregir error de timeout cuando freshclam (clamav) actualiza la base de datos de malware"
date: 2021-01-02
author: Termita
category: "sistemas operativos"
tags: ["clamav", "freshclam", "antivirus", "malware", "freshclam.conf"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---


<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://askubuntu.com/a/1218883" target="_blank">Sebastian en AskUbuntu</a>  
<br>
<br>
<br>
<br>
