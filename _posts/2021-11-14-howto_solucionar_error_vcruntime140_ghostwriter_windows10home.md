---
layout: post
title: "[HowTo] Solución al error 'VCRUNTIME140_1' al ejecutar GhostWriter en Windows 10 Home"
date: 2021-11-14
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "windows", "windows 10", "windows 10 home", "vcruntime", "vcruntime140", "visual c", "librerias", "ghostwriter", "editor", "texto plano", "lenguaje de marcas", "markdown"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
El editor de texto 'GhostWriter' es multiplataforma, es decir, funciona también en Windows (<small>a parte de GNU Linux y macOS</small>).  
La versión de 'GhostWriter' para Windows es 'portable', es decir, no necesita instalación: basta con descargarlo, descomprimirlo en un subdirectorio y ejecutarlo.  
Sin embargo, al hacer esto, puede producirse un error y el programa no se ejecuta:
> «La ejecución no puede continuar porque no se encontró VCRUNTIME140_1.dll», «Falta VCRUNTIME140_1.dll en el equipo», ô «VCRUNTIME140_1.dll is missing»  

<br>
## ¿A qué se debe este error?
En el sistema falta la librería 'dll' de 'Visual Studio C' **'vcruntime140_1.dll'**, necesaria para que 'GhostWriter' funcione en Windows 10 Home.  

<br>
## ¿Cómo solucionarlo (y que 'GhostWriter' funcione)?
Es necesario que GhostWriter disponga de la 'vcruntime140_1.dll'.  
Esto se puede hacer de 2 formas:
a. Consiguiendo (desde otro ordenador similar, o desde internet (esto último no es recomendable) el archivo 'vcruntime140_1.dll' correspondiente a nuestra arquitectura -32bits ô 64bits- y copiándolo al subdirectorio de 'GhostWriter'.
b. [Descargando desde la página oficial de Microsoft el paquete *'Visual C++ Redistributable para Visual Studio 2015'*](https://www.microsoft.com/es-es/download/details.aspx?id=48145) (contiene esa y otras librerías) correspondiente a nuestra arquitectura -32bits ô 64bits- e instalándolo en el sistema.  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.tuexperto.com/2021/02/17/solucion-error-falta-vcruntime140-dll-equipo-windows-10-7-8/">TuExperto.com</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
