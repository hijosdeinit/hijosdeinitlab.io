---
layout: post
title: "[HowTo] Instalación de MicroSoft Edge (dev) en Debian y derivados"
date: 2021-02-14
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "dev", "microsoft", "edge", "chrome", "chromium", "web browser", "navegador web", "deb", "dpkg", "navegador", "browser", "internet", "internet 4.0", "Firefox", "WaterFox", "LibreFox", "LibreWolf", "min browser", "min", "icecat", "iceweasel", "privacidad", "respeto"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Edge, el navegador web de MicroSoft, heredero de aquel caído en desgracia Internet Explorer. ¿Por qué utilizar Edge teniendo alternativas más respetuosas, mejores? ¿Y por qué, además, utilizarlo en GNU Linux?  

<br>
Para explicarlo bien vale un ejemplo: Aún hay páginas web que están desarrolladas para Internet Explorer. Muestra de ello son muchos de los portales de la administración, del estado... esos claros exponentes de las carencias en usabilidad y depuración. Cuando a uno -con dinero público, que al parecer "no es de nadie"-le pagan sí o sí, haga bien o mal su trabajo, se crean paginitas tan mal construídas y tan incompatibles como las de la Seguridad Social, la Agencia Tributaria, la bolsa de trabajo del Institut Català de la Salut, etcétera.  
Muchos de estos portales lo advierten: no se hacen responsables de su mal funcionamiento si se emplea un navegador diferente a MicroSoft Internet Explorer.  
Y MicroSoft Edge es su heredero.  

<br>
Microsoft Edge es un navegador <big>**poco respetuoso con la privacidad**</big>, menos aún que Chrome y Firefox: no sólo recopila datos de usuario -como también hacen Chrome y Firefox- sino que además envía "identificadores persistentes" que sirven para asociar las solicitudes -ubicación, ip, detalles de las páginas visitadas, etcétera- a los servidores de Microsoft, sin que tal recopilación tenga que ver con las funciones de sugerencias de búsquda y autocompletar.  

<br>
Microsoft quiere que Edge sea multiplataforma, así que está desarrollando versiones para todos los sistemas operativos, GNU Linux incluído. La versión para Linux no es ni tan siquiera "beta", sino lo que MicroSoft denomina "dev", que supongo significará "en desarrollo".  
Microsoft **Edge está basado en Chromium** -el navegador que a su vez está basado en el navegador Chrome de Google-; tiene su propia tienda de extensiones y es, asimismo, compatible con las extensiones de Chrome.  

<br>
<br>
## Procedimiento para INSTALAR Microsoft Edge en Debian y derivados

1. Descargar el paquete '.deb' en la página oficial de '[Microsoft Edge Insider](https://www.microsoftedgeinsider.com/en-us/download/?platform=linux)'. <small>Existe también un paquete '.rpm' disponible para descargar.</small>

2. Instalar el paquete '.deb', bien haciendo doble click desde el interfaz gráfico o bien desde línea de comandos ejecutando:
~~~bash
dpkg -i paquetedescargado.deb
~~~

<br>
<br>
## Salvaguarda mínima de la privacidad en Microsoft Edge: configuración

<br>
<a href="/assets/img/2021-02-14-howto_instalar_ms_edge_chromium_dev_en_debian_derivados/msedge_linux_configuracion_privacidad.png" target="_blank"><img src="/assets/img/2021-02-14-howto_instalar_ms_edge_chromium_dev_en_debian_derivados/msedge_linux_configuracion_privacidad.png" alt="Para empezar, deshabilitar todo lo dehabilitable en el primer inicio de Edge" width="800"/></a>  
<small>Para empezar, deshabilitar todo lo dehabilitable (que no es todo, porque Microsoft no lo permite) en el primer inicio de Edge</small>  
<br>
... Y en el apartado de configuración:  

<br>
Perfiles → información personal → Guardar y rellenar la información personal: DESHABILITADO  
Perfiles → Contraseñas → Preguntar si quiere guardar sus contraseñas: DESHABILITADO  
Perfiles → Contraseñas → Iniciar sesión automáticamente: DESHABILITADO  
Perfiles → Contraseñas → Mostrar el botón "Mostrar contraseña" en los campos de contraseña: DESHABILITADO  
Perfiles → Contraseñas → Sugerir contraseñas seguras: DESHABILITADO  

<br>
Perfiles → Información de pago → Guardar y rellenar información de pago: DESHABILITADO  

<br>
Perfiles → Preferencias de perfil → Permitir el inicio de sesión único para sitios profesionales o educativos que usen este perfil: DESHABILITADO  

<br>
Privacidad, búsqueda y servicios → Prevención de seguimiento: HABILITADO [ESTRICTA]  
Privacidad, búsqueda y servicios → Privacidad → Enviar solicitudes de "No realizar seguimiento" → HABILITADO  
Privacidad, búsqueda y servicios → Datos de diagnóstico opcionales → Ayude a mejorar los productos de Microsoft enviando datos de diagnóstico opcionales sobre el uso del explorador, los sitios web que visita y los informes de bloqueo: DESHABILITADO  
Privacidad, búsqueda y servicios → Seguridad → SmartScreen de Microsoft Defender: DESHABILITADO  
Privacidad, búsqueda y servicios → Seguridad → Usa DNS seguro para especificar cómo buscar la dirección de red de los sitios web: HABILITADO [Usar el proveedor de servicios actual]  
Privacidad, búsqueda y servicios → Servicios → Usar un servicio web para corregir errores de navegación: DESHABILITADO  
Privacidad, búsqueda y servicios → Servicios → Sugerir sitios similares cuando no se puede encontrar un sitio web: DESHABILITADO  
Privacidad, búsqueda y servicios → Servicios → Mostrar sugerencias de Pinterest en Colecciones: DESHABILITADO  
Privacidad, búsqueda y servicios → Servicios → Ahorra tiempo y dinero con Compras en Microsoft Edge: DESHABILITADO  
Privacidad, búsqueda y servicios → Servicios → Barra de direcciones y búsqueda → Mostrarme sugerencias de búsqueda y sitios usando los caracteres que escriba → DESHABILITADO  
Privacidad, búsqueda y servicios → Servicios → Barra de direcciones y búsqueda → Mostrarme sugerencias del historial, los favoritos y otros datos de este dispositivo con mis caracteres escritos: DESHABILITADO  
Privacidad, búsqueda y servicios → Servicios → Barra de direcciones y búsqueda → Motor de búsqueda usado en la barra de direcciones: 'Startpage - Español (predeterminado'  
Privacidad, búsqueda y servicios → Servicios → Barra de direcciones y búsqueda → Buscar en nuevas pestañas usa el cuadro de búsqueda o la barra de direcciones: Cuadro de búsqueda (recomendado)  
Privacidad, búsqueda y servicios → Servicios → Barra de direcciones y búsqueda → Administrar motores de búsqueda: TODOS ELIMINADOS, menos STARTPAGE y DUCKDUCKGO  

<br>
En el inicio → Continuar donde lo dejó  

<br>
Página de la nueva pestaña → Carga previa de la página de pestaña nueva para una experiencia más rápida: DESHABILITADO  

<br>
Sistema → Seguir ejecutando aplicaciones en segundo plano cuando se cierra Microsoft Edge: DESHABILITADO  
Sistema → Usar aceleración de hardware cuando esté disponible: HABILITADO  
Sistema → Guardar recursos → Ahorra recursos con pestañas en espera → HABILITADO  
Sistema → Guardar recursos → Pasar a suspendidas las pestañas inactivas después del tiempo especificado: 2 horas de inactividad  
Sistema → Herramientas de desarrollo → Abrir "DevTools" al presionar la tecla F12: HABILITADO  

<br>
<br>
## Procedimiento para DESINSTALAR Microsoft Edge en Debian y derivados

Como prolegómeno antes de desinstalar un paquete nunca está de más comprobar si ese paquete está correctamente instalado en el sistema:
~~~bash
sudo dpkg -l | grep edge
~~~
> <small>ii  ***microsoft-edge-dev***  90.0.789.1-1   amd64   The web browser from Microsoft</small>  

A partir de ahí se puede escoger entre:  

a. Desinstalar el paquete completamente (**purgar**), incluídos sus archivos de configuración:  
~~~bash
dpkg -P microsoft-edge-dev
~~~

b. Desinstalar el paquete dejando en el sistema sus archivos de configuración  
~~~bash
dpkg -r microsoft-edge-dev
~~~

<br>
Para finalizar es recomendable comprobar que el paquete ha sido eliminado correctamente ejecutando:
~~~bash
sudo dpkg -l | grep edge
~~~

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.scss.tcd.ie/Doug.Leith/pubs/browser_privacy.pdf">Web browser privacy: what do browsers say when they phone home?</a>  
<a href="https://www.muylinux.com/2020/10/20/microsoft-edge-linux-dev/">MuyLinux</a>  
<a href="https://unix.stackexchange.com/a/236843">ihor_dvoretskyi en unix.stackexchange.com</a>  
<a href="https://www.muycomputer.com/2020/02/27/navegadores-web-y-tu-privacidad/">MuyComputer</a>  
<a href="https://www.atareao.es/software/internet/instalar-internet-explorer-en-ubuntu/">atareao.es</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
