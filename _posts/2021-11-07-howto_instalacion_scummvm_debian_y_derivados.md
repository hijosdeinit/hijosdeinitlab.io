---
layout: post
title: "[HowTo] Instalación de 'scummvm' en Debian y derivados"
date: 2021-11-07
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "debian", "ubuntu", "gnu linux", "gnu", "linux", "scummvm", "lucas", "lucas arts", "aventuras graficas", "retro", "retromatica", "msdos", "dosbox", "dosemu", "maquina virtual", "juegos", "games", "gaming", "monkey island", "maniac mansion", "day of the tentacle"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno: cpu Intel® Core™ i7-8750H CPU @ 2.20GHz × 12, 16gb RAM, gpu NVIDIA GeForce GTX 1050 Mobile, Ubuntu 20.04.3 LTS x86_64, kernel 5.11.0-38-generic #42~20.04.1-Ubuntu SMP Tue Sep 28 20:41:07 UTC 2021 x86_64 x86_64 x86_64 GNU/Linux</small>  

<br>
['scummvm'](https://www.scummvm.org/downloads/#release) (*Scumm Virtual Machine*, máquina virtual de Scumm en español) «es un programa informático que permite ejecutar las aventuras gráficas creadas originalmente para el motor SCUMM de LucasArts. ScummVM también soporta una variedad de juegos que no utilizan el motor SCUMM, realizados por compañías como Revolution Software o Adventure Soft.  
Como su nombre indica, ScummVM ejecuta los juegos a través de una máquina virtual, usando solamente sus archivos de datos, de manera que reemplaza los ejecutables con los que el juego fue originalmente lanzado. Esto permite ejecutar los juegos en sistemas para los cuales nunca fueron diseñados, como por ejemplo, wii, pocketPCs, PalmOS, Nintendo DS, PSP, PlayStation 3, Linux, Xbox o teléfonos móviles.

ScummVM se encuentra bajo la licencia GNU GPL por lo que es software libre.»  

<br>
La última versión que se distribuye en la página oficial es la 2.5.0, mientras que la que se puede instalar desde los repositorios de Ubuntu 20.04 y, seguramente, de Debian y otros derivados, es la 2.1.1.  
El repositorio de 'scummvm' en GitHub es [este](https://github.com/scummvm).  

<br>
<br>
## Instalación de 'scummvm' desde los repositorios oficiales de nuestra distribución
~~~bash
sudo apt-get update
sudo apt install scummvm
~~~

<br>
### Desinstalación / purgado de 'scummvm'
~~~bash
sudo apt remove --auto-remove scummvm
~~~
ô
~~~bash
sudo apt purge scummvm
~~~
ô
~~~bash
sudo apt purge --auto-remove scummvm
~~~

<br>
<br>
## Instalación de la última versión de 'scummvm' desde su página oficial
Desde la [sección de descargas de la página oficial del proyecto](https://www.scummvm.org/downloads/#release), descárguese el .deb correspondiente.  
Instálese.  
Por ejemplo, el paquete .deb de 'scummvm' para Ubuntu 20.04 64 bits, una vez descargado, se instala así:
~~~bash
dpkg -i scummvm_2.5.0-1_ubuntu20.04_amd64.deb
~~~

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://kreationnext.com/support/how-to-install-scummvm-on-debian-unstable-sid/">KreationNext</a>  
<a href="https://es.wikipedia.org/wiki/ScummVM">Wikipedia - scummvm</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
