---
layout: post
title: "[HowTo] Deshabilitar 'theming' en NextCloud mediante el comando 'occ'"
date: 2020-12-22
author: Termita
category: "sistemas operativos"
tags: ["nextcloud", "nube", "web 4.0", "occ", "error", "imagick", "imagemagick", "theming", "apps"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Un error relativamente frecuente en la página de diagnóstico de una nube NextCloud (Configuración → Administración → Vista General) es aquel que hace referencia a que php necesita algún paquete ausente, por ejemplo, 'imagick'.  
'imagick' está relacionado con 'imagemagick' y se desaconseja su empleo en servidores. El motivo: por seguridad, porque se le conocen vulnerabilidades.  
La manera de que ese error no aparezca es hacer que nuestra nube NextCloud no necesite 'imagemagick'.  
<big>¿Y qué componente de NextCloud requiere imagemagick?</big>: la "app" 'theming'.  
Deshabilitar 'theming' se puede realizar desde el interfaz web de NextCloud (apartado 'aplicaciones'), o bien de una manera expeditiva, desde línea de comandos, ejecutando el comando 'occ':
~~~
sudo -u www-data php /var/www/nextcloud/occ app:disable theming
~~~

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://help.nextcloud.com/t/imagick-missing-version-php-7-3/47765/25" target="_blank">LloydS en help.nextcloud.com</a>  
<br>
<br>
<br>
<br>
<br>
<br>
