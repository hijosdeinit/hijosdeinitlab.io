---
layout: post
title: "[HowTo] Configurar 'atom' para que muestre los espacios en blanco, saltos de linea y otros caracteres invisibles"
date: 2021-03-13
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "atom", "editor de texto", "ide", "programacion", "bash", "script", "bash scripting", "markdown", "texto", "texto plano", "código", "editor", "gnu linux", "debian", "vi", "vim", "nano", "emacs", "atom", "eclipse", "sublime text", "visual studio code", "vscodium", "codium", "vscode", "netbeans", "espacios en blanco", "salto de linea", "tabulacion"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Los espacios en blanco al final de una línea pueden ser algo necesario -como en el caso de MarkDown (señalan un salto de línea)- ô, por el contrario, algo molesto y generador incesante de errores.
En el caso de textos escritos en lenguaje MarkDown interesa, por ejemplo, que los dos espacios al final de algunas líneas se mantengan, [que no se borren automáticamente al guardar el documento](https://hijosdeinit.gitlab.io/howto_atom_espacios_blanco_final_linea/).  

<br>
## Configuración de Atom para que muestre los espacios en blanco, saltos de linea, tabulaciones y otros caracteres "invisibles" / "especiales"
Ajustes → Editor → Mostrar invisibles  

<br>
<br>
<br>
En [jonathanmb](https://jonathanmh.com/showing-indentation-spaces-tabs-invisibles-in-various-editors/) existe un listado de los procedimientos para mostrar los "caracteres invisibles" en diversos editores de texto como 'vim', 'sublime text', etcétera.  

<br>
<br>
<br>
Entradas relacionadas
- [[HowTo] Evitar Que 'Atom' Elimine Espacios En Blanco Al Final De Línea](https://hijosdeinit.gitlab.io/howto_atom_espacios_blanco_final_linea/)
- [[HowTo] El editor (IDE) atom y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [[HowTo] Agregar idiomas al editor (IDE) atom. Ponerlo en español](https://hijosdeinit.gitlab.io/howto_poner_atom_en_espa%C3%B1ol/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://jonathanmh.com/showing-indentation-spaces-tabs-invisibles-in-various-editors/">johnathanmb</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
