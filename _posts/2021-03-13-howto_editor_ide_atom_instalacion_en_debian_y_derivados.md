---
layout: post
title: "[HowTo] El editor (IDE) 'atom' y su instalacion en Debian y derivados"
date: 2021-03-13
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "atom", "editor de texto", "ide", "programacion", "bash", "script", "bash scripting", "markdown", "texto", "texto plano", "código", "editor", "gnu linux", "debian", "vi", "vim", "nano", "emacs", "atom", "eclipse", "sublime text", "visual studio code", "vscodium", "codium", "vscode", "netbeans"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
**‼ℹ** Actualización.  Diciembre 2022: El editor 'Atom' ha sido descontinuado. Sin profundizar en las características de la estrategia de 'Microsoft' -actual amo de 'GitHub' y, por extensión, de 'Atom'-, los usuarios de 'Atom' quizá encuentren en <big>[**'Pulsar'** (*fork* de 'Atom' que sí está siendo mantenido)](https://pulsar-edit.dev)</big>, una alternativa.  
En este blog existe [una entrada sobre 'Pulsar', el sucesor del malogrado 'Atom']().  

<br>
<br>
<small>Hace un tiempo hice un [listado de los editores de texto / IDE que me parecían interesantes en un artículo de este blog](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/).</small>  
<small>Un IDE (*Integrated Developement Environment*) consta, entre otras cosas, de un editor de texto con muchos extras como, por ejemplo, resaltado de sintaxis, autocompletado.</small>  
<small>Uno de los editores de aquella lista era [Atom](https://atom.io/)</small>.  

<br>
<br>
## Atom
Atom es un editor de código y texto creado en 2015 por [GitHub](https://github.com/), "entidad" que no hace demasiado fue adquirida por Microsoft. Sus desarrolladores lo denominan «un editor de textos hackeable para el siglo XXI»  
Es un software de código abierto basado en 'Electron' -anteriormente llamado 'Atom Shell'- que está escrito en [CoffeeScript](https://es.wikipedia.org/wiki/CoffeeScript) y [Less](https://es.wikipedia.org/wiki/LESS_(lenguaje_de_hojas_de_estilo)).  
Inicialmente los paquetes de extensión para Atom y todo lo que **no** forma parte del núcleo de Atom fueron lanzados bajo una licencia de código abierto.  
El 6 de mayo de 2014 el resto de Atom, incluyendo la aplicación de su núcleo, su gestor de paquetes y Electron -su *framework* de escritorio, fueron publicados como software libre y de código abierto bajo la [Licencia MIT](/assets/docs/2021-03-13-howto_editor_ide_atom_instalacion_en_debian_y_derivados/Licencia-MIT.pdf).  
Atom está construído mediante HTML, JavaScript, CSS e integración Node.js.  
Es multiplataforma, es decir, puede funcionar bajo casi cualquier sistema operativo: GNU Linux, Mac Os, Windows.  
Es totalmente customizable / *hackeable*: se le pueden agregar paquetes -tiene un gestor de paquetes integrado-, crear paquetes propios, agregar temas visuales (por defecto viene con el tema 'One Dark', que es personalizable), etcétera.  
La mayor parte de los paquetes tienen licencias de software libre y están desarrollados y mantenidos por su comunidad de usuarios.  
Control de versiones 'git' integrado.  
Posee un explorador de archivos integrado que facilita la apertura de un simple archivo, un proyecto entero o múltiples proyectos.  
Permite el trabajo mediante paneles -muy útil para trabajar con varios archivos simultáneamente-, autocompletado, resaltado de sintaxis, búsquedas y reemplazo, etcétera.  
A Atom se le puede agregar soporte para lenguajes de programación extra -a parte de los que soporta por defecto- mediante el sistema de paquetes. El soporte puede mejorarse mediante intérpretes, *debuggers* o *pipelines* que establezcan conexión entre Atom y software de terceros.  
Atom soporta, por defecto, los siguientes lenguajes:
- C
- C++
- C#
- Clojure
- COBOL
- CSS
- CoffeeScript
- D
- GitHub Flavored Markdown
- Go
- HTML
- Java
- JavaScript
- JSON
- Julia
- Less
- Make
- Mustache
- Nim
- Objective-C
- Perl
- PHP
- Python
- Racket15​
- Ruby
- Ruby on Rails
- Sass
- Scala
- Shell script
- SQL
- TOML
- TypeScript
- XML
- YAML
- MML  

<br>
<br>
## Instalar Atom en Debian y derivados
<small>**Sólo para 64bits**</small>  
1. Descárguese el paquete .deb de instalación desde la [página oficial](https://atom.io/download/deb).
2. Instálese ejecutando el .deb haciendo doble click sobre el archivo descargado (atom-amd64.deb) ô bien ejecutando:
~~~bash
sudo dkpkg -i atom-amd64.deb
~~~

<br>
<big>Otras formas</big> de instalar Atom en Debian y derivados serían:  

<br>
ø Desde repositorio oficial de Debian  
~~~
sudo apt-get update
sudo apt install atom
~~~

<br>
ø Desde repositorio oficial de Atom (agregando primero dicho repositorio a nuestro sistema)  
<small>**Sólo para 64bits**</small>  
~~~
wget -q https://packagecloud.io/AtomEditor/atom/gpgkey -O- | sudo apt-key add -
sudo apt-get update
sudo apt install apt-transport-https
sudo add-apt-repository "deb [arch=amd64] https://packagecloud.io/AtomEditor/atom/any/ any main"
sudo apt install atom
~~~

<br>
ø A través de *snap*  
<small>**desconozco si hay versión para 32bits**</small>  

<br>
ø Desde [repositorio no-oficial 32bits](https://launchpad.net/~webupd8team/+archive/ubuntu/atom)  
<small>**Para 32bits**</small>  
~~~
sudo add-apt-repository ppa:webupd8team/atom
sudo apt-get update
sudo apt-get install atom
~~~
Desinstalar 'atom' cuando es instalado de tal forma:
~~~
sudo apt-get remove atom
sudo add-apt-repository --remove ppa:webupd8team/atom
sudo apt-get autoremove
~~~
<small>Instalar 'atom' en un netbook con Debian 10 x32 mediante este método no funcionó: errores derivados de la clave gpg del repositorio que, seguramente, los usuarios avanzados sabrán subsanar.</small>  

<br>
ø Desde paquete .deb **no oficial** para arquitecturas de **32bits**  
Existe un [proyecto en SourceForge que suministra versiones no oficiales para 32bits](https://sourceforge.net/projects/atom-32-bit/files/) en [.deb](https://downloads.sourceforge.net/project/atom-32-bit/v1.54.0/Linux/atom-i386.deb?ts=1615658387&r=https%3A%2F%2Fsourceforge.net%2Fprojects%2Fatom-32-bit%2Ffiles%2Fv1.54.0%2FLinux%2Fatom-i386.deb%2Fdownload), .rpm e instalador para Windows.  
A mediados de marzo de 2021 la versión que proporcionaban era 1.54.0 (la versión oficial más nueva era en esos momentos 1.55.0).

<br>
ø [Compilando el código fuente](https://flight-manual.atom.io/hacking-atom/sections/hacking-on-atom-core/#platform-linux)  
Cuando traté de compilar 'atom' en un netbook con Debian 10 de 32bits obtuve un error:  
Había clonado el [repositorio git de atom](https://github.com/atom/atom) mediante `git clone https://github.com/atom/atom` e instalado las dependencias necesarias mediante `sudo apt install build-essential git libsecret-1-dev fakeroot rpm libx11-dev libxkbfile-dev`; mas a la hora de compilar ejecutando el script de compilación (`sudo bash /atom/script/build`) se producían diversos errores que achaco a que Debian 10 necesitaba un compilador C++ más moderno -C++ 11 por ejemplo- y para ello había que instalar los paquetes 'gcc-5' y 'g++-5', que pareciera no están fácilmente disponibles para Debian 10.  

<br>
<br>
## Desinstalar Atom en Debian y derivados
Si más adelante deseara desinstalarlo, el procedimiento -**si lo instalamos mediante paquete .deb**- sería el siguiente:  

<br>
Como prolegómeno antes de desinstalar un paquete nunca está de más comprobar si ese paquete está correctamente instalado en el sistema:
~~~bash
sudo dpkg -l | grep atom
~~~
> <small>ii  atom    1.55.0     amd64        A hackable text editor for the 21st Century.</small>  

<br>
A partir de ahí se puede escoger entre:  
a. Desinstalar el paquete completamente (**purgar**), incluídos sus archivos de configuración:
~~~bash
dpkg -P atom
~~~
b. Desinstalar el paquete dejando en el sistema sus archivos de configuración
~~~bash
dpkg -r atom
~~~

<br>
Para finalizar es recomendable comprobar que el paquete ha sido eliminado correctamente ejecutando:
~~~bash
sudo dpkg -l | grep atom
~~~

<br>
<br>
## Rechaza la telemetría
Recién instalado 'Atom', en el primer inicio, pregunta amablemente si deseamos enviar nuestros datos de uso a los responsables de la aplicación.  
Si a vd le gusta la privacidad quizás debería responder **NO**.

<br>
<a href="/assets/img/2021-03-13-howto_editor_ide_atom_instalacion_en_debian_y_derivados/atom_denegar_telemetria.png" target="_blank"><img src="/assets/img/2021-03-13-howto_editor_ide_atom_instalacion_en_debian_y_derivados/atom_denegar_telemetria.png" alt="Telemetría. ¿Desea vd pagar con sus datos a los responsables de 'Atom'?" width="800"/></a>  
<small>Telemetría. ¿Desea vd pagar con sus datos a los responsables de 'Atom'?</small>  
<br>

<br>
<br>
<br>
Entradas relacionadas:  
- []()
- [[HowTo] Agregar idiomas al editor (IDE) atom. Ponerlo en español](https://hijosdeinit.gitlab.io/howto_poner_atom_en_espa%C3%B1ol/)
- [[HowTo] Evitar que 'atom' elimine espacios en blanco al final de línea](https://hijosdeinit.gitlab.io/howto_atom_espacios_blanco_final_linea/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)
- [[HowTo] El editor (IDE) 'geany' en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_geany_en_debian_y_derivados/)
- [[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://ubunlog.com/editor-atom-instalacion-ubuntu/">UbunLog</a>  
<a href="https://itsfoss.com/install-atom-ubuntu/">ItsFoss</a>  
<a href="https://flight-manual.atom.io/hacking-atom/sections/hacking-on-atom-core/#platform-linux">atom.io - compilación</a>  
<a href="https://itsfoss.com/install-software-from-source-code/">ItsFoss</a>  
<a href="https://es.wikipedia.org/wiki/Atom_(software)">WikiPedia - Atom</a>  
<a href="https://atom.io/">Atom</a></small>  





<br>
<br>
<br>
<br>
<br>
<br>
