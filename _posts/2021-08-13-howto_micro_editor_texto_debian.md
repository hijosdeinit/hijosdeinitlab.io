---
layout: post
title: "[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados"
date: 2021-08-13
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "cli", "línea de comandos", "vi", "vim", "vino", "vinagre", "visudo", "emacs", "micro", "ed", "editor", "editor de textos", "texto", "texto plano", "latex", "pandoc", "txt", "markdown", "cli", "terminal", "resaltado de sintaxis", "syntax highlightning"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En la terminal, como seguramente tantos usuarios de GNU Linux, utilizo habitualmente el editor de texto ['nano'](https://www.nano-editor.org/). Aún no me he visto en la necesidad de pasarme a 'vim' o 'emacs'.  
'nano' está generalmente en todas las distribuciones, es un editor de texto sencillo, versátil y bastante completo que, combinado con un lenguaje de marcas como *markdown* cumple con todas mis espectativas (aunque 'nano' también lo hace).  
No obstante, no bastaba con tener en la lista de tareas por hacer el aprender 'vim' y 'emacs' como alternativas avanzadas a 'nano', que tuve conocimiento de la existencia de ['micro'](https://github.com/zyedidia/micro), un editor de texto para la línea de comandos algo más completo que 'nano'.   

<br>
<a href="/assets/img/2021-08-13-howto_micro_editor_texto_debian/micro_editor_texto.png" target="_blank"><img src="/assets/img/2021-08-13-howto_micro_editor_texto_debian/micro_editor_texto.png" alt="editor de texto 'micro'" width="800"/></a>  
<small>Editor de texto 'micro'</small>  
<br>

'micro' no está instalado por defecto en la inmensa mayoría de distribuciones GNU Linux ('nano' sí). Algo similar ocurre con 'vim' y 'emacs', aunque generalmente estos dos reputados editores de texto están en los repositorios oficiales de casi todas las distribuciones / familias.  
Salvo contadas excepciones -por ejemplo, antaño (a fecha 2022/04/09 ya no), Ubuntu {<span style="background-color:#042206"><span style="color:lime">`sudo apt install micro`</span></span> (<small>instalación nativa</small>) o <span style="background-color:#042206"><span style="color:lime">`sudo snap install micro --classic`</span></span> (<small>instalación mediante nueva paquetería (*snap*)</small>)}- 'micro' no suele encontrarse en los repositorios oficiales de las distribuciones GNU Linux, así que -en esos casos- su instalación puede llevarse a cabo:
- compilando el código fuente
- mediante un binario precompilado
- mediante 'script' oficial de instalación.  
La ventaja de instalar 'micro' mediante el script oficial, compilándolo o mediante un binario precompilado, a diferencia de hacerlo a través de los repositorios oficiales de las distribuciones GNU Linux, es que, de esta forma, se puede incorporar al sistema la última versión del programa.  

<br>
En GNU Linux, 'micro' requiere los paquetes 'wl-clipboard' (*wayland*) y 'xclip' o 'xsel' (X11) instalados para que el copiado y pegado funcione correctamente con aplicaciones externas.  
Si estos paquetes no están instalados, 'micro' utilizara un *clipboard' externo para el copiado y pegado, pero esas operaciones no funcionarán con aplicaciones externas.  

<br>
<br>
## Instalación de 'micro' compilando el código fuente
Es necesario que el sistema [esté preparado para llevar a cabo la compilación de software](https://hijosdeinit.gitlab.io/howto_preparar_debian_para_compilar/).  
Asimismo para compilar el código fuente de 'micro' es necesario que el sistema:
- tenga [instalado 'GO' 1.11 o superior](https://hijosdeinit.gitlab.io/howto_instalacion_GO_debian/)
- tenga activados los módulos de 'GO'  

~~~bash
mkdir ~/Programas
cd ~/Programas
git clone https://github.com/zyedidia/micro
cd micro
make build
~~~
Opcional: para poder ejecutar 'micro' desde cualquier directorio:
~~~bash
sudo mv micro /usr/local/bin
~~~
Para ejecutar 'micro':
~~~bash
./micro
~~~
Comprobar la versión de 'micro' instalada:
~~~bash
./micro -version
~~~
~~~
Version: 2.0.10
Commit hash: b9763856
Compiled on August 14, 2021
~~~
Para desinstalar 'micro' basta con borrar el binario y el directorio de configuración '~/.config/micro'

<br>
## Instalación de 'micro' a partir de un binario precompilado
En el [apartado "Releases" del repositorio oficial de 'micro' ubicado en 'GitHub'](https://github.com/zyedidia/micro/releases) se pueden descargar el binario precompilado de cada una de las diferentes versiones del programa. Puede descargarse, incluso, un ***.deb***, aunque éste fichero de instalación para Debian y derivados es sólo para arquitecturas de 64 bits.  
Generalmente interesa descargar la última versión estable del binario precompilado.  
~~~bash
wget https://github.com/zyedidia/micro/releases/download/v2.0.10/micro-2.0.10-linux32.tar.gz
tar -xzf micro-2.0.10-linux32.tar.gz
cd micro
~~~
Para ejecutar 'micro':
~~~bash
./micro
~~~
Para desinstalar 'micro' basta con borrar el binario y el directorio de configuración '~/.config/micro'

<br>
## Instalación de 'micro' mediante script oficial
Generalmente interesa descargar la última versión estable del script de instalación.  
Existe un script que puede instalar el último binario precompilado descargándolo desde [https://getmic.ro](https://getmic.ro.).  
<small>Es necesario tener 'curl' instalado.</small>
~~~bash
curl https://getmic.ro | bash
~~~
Este script ubicará el binario de 'micro' en el directorio desde el que se ejecute. No obstante, el binario puede ser reubicado / movido a cualquier directorio, por ejemplo '/usr/bin' ejecutando `sudo mv micro /usr/bin`  

Para desinstalar 'micro' basta con borrar el binario y el directorio de configuración '~/.config/micro'  

<br>
<a href="/assets/img/2021-08-13-howto_micro_editor_texto_debian/micro-logo.png" target="_blank"><img src="/assets/img/2021-08-13-howto_micro_editor_texto_debian/micro-logo.png" alt="editor de texto 'micro'" width="500"/></a>  

<br>
## Ejecutando, configurando y trabajando con el editor 'micro'
Para lanzarlo basta con ejecutar desde línea de comandos:
~~~bash
micro
~~~
<br>
En el repositorio oficial de 'micro' en 'GitHub hay un [<big>**manual de usuario**</big>](https://github.com/zyedidia/micro/blob/master/runtime/help/tutorial.md).  

<br>
<span style="background-color:#042206"><span style="color:lime">Alt</span></span> + <span style="background-color:#042206"><span style="color:lime">g</span></span> muestra / oculta los atajos de teclado -también llamados *bindings* o *key bindings*- en la zona inferior de la pantalla.  
<span style="background-color:#042206"><span style="color:lime">Ctrl</span></span> + <span style="background-color:#042206"><span style="color:lime">g</span></span> despliega / oculta la ayuda en la zona inferior de la pantalla.  
<span style="background-color:#042206"><span style="color:lime">Ctrl</span></span> + <span style="background-color:#042206"><span style="color:lime">e</span></span> activa el modo de ejecución de comandos.  

<br>
La configuración del entorno del editor 'micro' se puede hacer de 2 formas:  
a) Desde el propio programa, pulsando Ctrl + e y ejecutando el comando `set' acompañado del parámetro que se desea cambiar  
b) Desde el fichero de configuración '~/.config/micro/settings.json', editándolo a mano  

<br>
### Un ejemplo: Activar el *softwrap* o "recorte de líneas"
Un problema muy frecuente que se suele encontrar uno cuando arranca 'micro' por primera vez es que, en pantalla, las líneas se muestran en toda su longitud, sin cortarlas antes de que sobrepasen el ancho de pantalla, es decir, **sin *'softwrap'***. Esto es así porque el "recorte de líneas" (*softwrap*) está desactivado por defecto.  
Para activar *softwrap*, tenemos 2 opciones:  
<big>**a.**</big> Pulsar <span style="background-color:#042206"><span style="color:lime">Ctrl</span></span> + <span style="background-color:#042206"><span style="color:lime">e</span></span> y ejecutar <span style="background-color:#042206"><span style="color:lime">`set softwrap true`</span></span>, con lo cual, automáticamente, se añadiría la línea '"softwrap": true' al archivo de configuración '~/.config/micro/settings.json'  
ô  
<big>**b.**</big> Editar manualmente el archivo de configuración '~/.config/micro/settings.json' para añadirle a mano ese parámetro:
~~~bash
nano ~/.config/micro/settings.json
~~~
... y añadir la línea:
>"softwrap": true  

El archivo '~/.config/micro/settings.json', que hasta entonces estaba vacío (primer arranque, recién ejecutado por primera vez 'micro'), quedaría, por consiguiente... de así:
~~~json
{}
~~~
... a así:
~~~json
{
    "softwrap": true
}
~~~

<br>
<br>
## Anexo, guía de comandos del editor de texto 'micro'
~~~bash
micro -options
~~~

<br>
~~~
Usage: micro [OPTIONS] [FILE]...
-clean
    	Cleans the configuration directory
-config-dir dir
    	Specify a custom location for the configuration directory
[FILE]:LINE:COL (if the `parsecursor` option is enabled)
+LINE:COL
    	Specify a line and column to start the cursor at when opening a buffer
-options
    	Show all option help
-debug
    	Enable debug mode (enables logging to ./log.txt)
-version
    	Show the version number and information

Micro's plugin's can be managed at the command line with the following commands.
-plugin install [PLUGIN]...
    	Install plugin(s)
-plugin remove [PLUGIN]...
    	Remove plugin(s)
-plugin update [PLUGIN]...
    	Update plugin(s) (if no argument is given, updates all plugins)
-plugin search [PLUGIN]...
    	Search for a plugin
-plugin list
    	List installed plugins
-plugin available
    	List available plugins

Micro's options can also be set via command line arguments for quick
adjustments. For real configuration, please use the settings.json
file (see 'help options').

-option value
    	Set `option` to `value` for this session
    	For example: `micro -syntax off file.c`

Use `micro -options` to see the full list of configuration options
-autoindent value
    	Default value: 'true'
-autosave value
    	Default value: '0'
-autosu value
    	Default value: 'false'
-backup value
    	Default value: 'true'
-backupdir value
    	Default value: ''
-basename value
    	Default value: 'false'
-clipboard value
    	Default value: 'external'
-colorcolumn value
    	Default value: '0'
-colorscheme value
    	Default value: 'default'
-cursorline value
    	Default value: 'true'
-diffgutter value
    	Default value: 'false'
-divchars value
    	Default value: '|-'
-divreverse value
    	Default value: 'true'
-encoding value
    	Default value: 'utf-8'
-eofnewline value
    	Default value: 'true'
-fastdirty value
    	Default value: 'false'
-fileformat value
    	Default value: 'unix'
-filetype value
    	Default value: 'unknown'
-ignorecase value
    	Default value: 'true'
-incsearch value
    	Default value: 'true'
-indentchar value
    	Default value: ' '
-infobar value
    	Default value: 'true'
-keepautoindent value
    	Default value: 'false'
-keymenu value
    	Default value: 'false'
-matchbrace value
    	Default value: 'true'
-mkparents value
    	Default value: 'false'
-mouse value
    	Default value: 'true'
-parsecursor value
    	Default value: 'false'
-paste value
    	Default value: 'false'
-permbackup value
    	Default value: 'false'
-pluginchannels value
    	Default value: '[https://raw.githubusercontent.com/micro-editor/plugin-channel/master/channel.json]'
-pluginrepos value
    	Default value: '[]'
-readonly value
    	Default value: 'false'
-relativeruler value
    	Default value: 'false'
-rmtrailingws value
    	Default value: 'false'
-ruler value
    	Default value: 'true'
-savecursor value
    	Default value: 'false'
-savehistory value
    	Default value: 'true'
-saveundo value
    	Default value: 'false'
-scrollbar value
    	Default value: 'false'
-scrollmargin value
    	Default value: '3'
-scrollspeed value
    	Default value: '2'
-smartpaste value
    	Default value: 'true'
-softwrap value
    	Default value: 'false'
-splitbottom value
    	Default value: 'true'
-splitright value
    	Default value: 'true'
-statusformatl value
    	Default value: '$(filename) $(modified)($(line),$(col)) $(status.paste)| ft:$(opt:filetype) | $(opt:fileformat) | $(opt:encoding)'
-statusformatr value
    	Default value: '$(bind:ToggleKeyMenu): bindings, $(bind:ToggleHelp): help'
-statusline value
    	Default value: 'true'
-sucmd value
    	Default value: 'sudo'
-syntax value
    	Default value: 'true'
-tabmovement value
    	Default value: 'false'
-tabsize value
    	Default value: '4'
-tabstospaces value
    	Default value: 'false'
-useprimary value
    	Default value: 'true'
-wordwrap value
    	Default value: 'false'
-xterm value
    	Default value: 'false'
~~~

<br>
<br>
Entradas relacionadas:  
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)
- [En nano no existe overtyping](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/)
- [[HowTo] Comienzo con vim. I](https://hijosdeinit.gitlab.io/howto_comienzo_con_vim_1/)
- [[HowTo] Añadir resaltado de sintaxis al editor de textos NANO](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [[HowTo] nano: parámetros de arranque útiles](https://hijosdeinit.gitlab.io/howto_parametros_utiles_arranque_nano/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- [Expresiones regulares no aceptadas por el editor de texto 'nano'](https://hijosdeinit.gitlab.io/regexp_no_aceptadas_por_nano/)
- [[HowTo] El editor (IDE) atom y su instalacion en Debian y derivados]([HowTo] El editor (IDE) atom y su instalacion en Debian y derivados)
- [[HowTo] El editor (IDE) 'Brackets' y su instalación en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_de_codigo_bracket/)
- [[HowTo] Visualización de 'markdown' en la línea de comandos (CLI): 'MDLESS'](https://hijosdeinit.gitlab.io/howto_mdless_visor_markdown_cli/)
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://github.com/zyedidia/micro">'micro' readme - repositorio github</a>  
<a href="https://www.linuxhelp.com/how-to-install-micro-text-editor-in-ubuntu-18-04">LinuxHelp</a>  
<a href="https://linuxhint.com/install-micro-text-editor-linux/">LinuxHint</a>  
<a href="https://github.com/zyedidia/micro">'micro' readme - repositorio github</a>  
<a href="https://victorhckinthefreeworld.com/2017/04/25/micro-un-editor-de-texto-para-la-terminal-simple-sencillo-y-efectivo/">victorhckinthefreeworld</a>  
<a href="zzz">zzzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
