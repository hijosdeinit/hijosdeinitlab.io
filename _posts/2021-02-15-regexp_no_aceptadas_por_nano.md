---
layout: post
title: "Expresiones regulares no aceptadas por el editor de texto 'nano'"
date: 2021-02-15
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "linea de comandos", "cli", "terminal", "productividad", "lotes", "nano", "editor", "editor de texto", "expresiones regulares", "regular expressions", "regexp", "regex", "extended regular expressions", "ere", "secuencias de escape", "busqueda", "buscar", "reemplazar", "linea", "caracter", "retorno de carro"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Las ['expresiones regulares'](https://hijosdeinit.gitlab.io/expresiones_regulares_secuencias_de_escape/) y/o 'secuencias de escape' permiten buscar en un archivo tabulaciones, saltos de línea, espacios en blanco, finales de línea, etcétera.  

<br>
Sin embargo, lamentablemente el editor de texto 'nano' no permite la búsqueda de algunas expresiones regulares como la tabulación **'\t'**, el retorno de carro **'\r'**, o final de línea **'\n'**.  
<small>[En esta entrada de este blog ya se trató de otra de las carencias de 'nano': el overtyping (sobreescribir caracteres o modo de inserción)](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/).</small>  

<br>
Pareciera que 'nano' no soporta algunos metacaracteres de las expresiones regulares porque utiliza [**'ERE'** (***Extended Regular Expressions***)](https://en.wikibooks.org/wiki/Regular_Expressions/POSIX-Extended_Regular_Expressions).  

<br>
Por consiguiente, para buscar / reemplazar las expresiones regulares que no son compatibles con 'nano' hay 2 opciones:
- Utilizar otro editor de texto que sí sea compatible con estas expresiones regulares (vi/vim, gedit, etcétera).
- Emplear desde la terminal (CLI) comandos como 'sed', 'grep', 'awk', etcétera... que permiten la búsqueda / reemplazo de estas expresiones regulares.  

<br>
<br>
<br>
Entradas relacionadas:  
- [En nano no existe overtyping](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/)
- [[HowTo] Mostrar el número de línea en el editor 'nano' cuando Alt+Shift+3 no funciona](https://hijosdeinit.gitlab.io/nano__mostrar_numeros_linea_cuando_altshift3_nofunciona/)
- [[HowTo] Atajos de teclado del editor 'nano'](https://hijosdeinit.gitlab.io/howto_atajos_teclado_nano/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Añadir resaltado de sintaxis al editor de textos NANO](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [[HowTo] nano: parámetros de arranque útiles](https://hijosdeinit.gitlab.io/howto_parametros_utiles_arranque_nano/)
- [[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados](https://hijosdeinit.gitlab.io/howto_micro_editor_texto_debian/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://stackoverflow.com/a/29328745">CSᵠ en StackOverflow.com</a>  
<a href="https://en.wikibooks.org/wiki/Regular_Expressions/POSIX-Extended_Regular_Expressions">WikiBooks - Regular Expressions - POSIX - Extended Regular Expressions</a>  
<a href="https://www.regular-expressions.info/posix.html">regular-expressions.info</a>  
<a href="https://es.wikipedia.org/wiki/Expresi%C3%B3n_regular#La_barra_inversa_o_contrabarra_%22\%22">WikiPedia - expresiones regulares: barra inversa</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
