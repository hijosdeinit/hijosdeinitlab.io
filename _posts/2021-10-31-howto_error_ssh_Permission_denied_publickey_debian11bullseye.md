---
layout: post
title: "[HowTo] Una solución al error 'Permission denied (publickey)' en servidor SSH ('openssh-server') [Debian 11 y derivados]"
date: 2021-10-31
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "ssh", "redes", "acceso remoto", "remote", "secure shell", "gnu linux", "debian", "debian 11", "bullseye", "error", "troubleshooting", "passwordautentication"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno: cpu Core2Duo, 6gb RAM, gpu gforce 7600gt, Live USB - GNU Linux Debian 11 Buster LXDE, kernel 5.10.0-9-amd64</small>  

<br>
Probando Debian 11 *Bullseye* decidí instalarle el servidor 'openssh-server'. Al tratar de conectar, desde otra máquina, a ese servidor recién instalado obtuve un error:
> <big>***'Permission denied (publickey)'***</big>  

<br>
¿A qué se debía este error?  
<br>
Ese error se producía porque, al parecer, la última versión de 'openssh-server' para Debian 11 trae anulada la **'autentificación mediante contraseña' ('*PasswordAutentication'*)** en su configuración por defecto. Por consiguiente, la única forma de conectarse -a no ser que cambiemos dicha configuración por defecto- es mediante 'keys' (una especie de certificados). Yo prefiero seguir autentificándome mediante contraseña, así que procedí a retocar la configuración.  
Los parámetros de configuración del servidor SSH 'openssh-server' se encuentran definidos en el archivo '/etc/ssh/sshd_config'.  
<br>
Para habilitar la autentificación a la "vieja usanza", es decir, mediante contraseña, hay que editar el archivo '/etc/ssh/sshd_config':
~~~bash
sudo nano /etc/ssh/sshd_config
~~~
... y añadir esta línea: <big><span style="background-color:#042206"><span style="color:lime">`PasswordAuthentication yes`</span></span></big>  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Instalación de 'openssh-server' (servidor SSH) en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_servidor_ssh_debian_y_derivados/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
