---
layout: post
title: "[HowTo] Incorporar un tema visual más atractivo al interfaz web de 'amule-daemon'"
date: 2022-01-15
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "amule", "amule-daemon", "demonio", "servicio", "amuleweb", "amulegui", "amulecmd", "transmission", "transmission-cli", "transmission-daemon", "Debian", "directorio", "archivo", "configuracion", "settings.json", "stats.json", "torrent", "resume", "descargas", "p2p", "ed2k", "kad", "headless"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
De ['amule'](http://www.amule.org/), la conocida utilidad de descarga p2p, no es imprescindible instalar el programa "entero", interfaz gráfica incluída. <small>Lo mismo ocurre con 'Transmission', el gestor de descargas 'torrent'.</small>  
Basta con instalar en una máquina un servicio o demonio -'amule-daemon' o 'amuled'- y, desde el navegador web o el comando 'amulecmd' de otra máquina remota, se pueden gestionar las descargas.  
Es este el campo semántico de lo que suele denominarse ["***headless***"](https://techoverflow.net/2019/05/17/what-is-a-headless-program-or-application/): cuando un programa o sistema opera sin un interfaz gráfico.  

<br>
El interfaz web con que se gestiona remotamente el demonio o servicio de 'amule' tiene, de serie un aspecto muy tosco:

<br>
<a href="/assets/img/2022-01-15-howto_incorporar_tema_visual_atractivo_al_interfaz_web_amule/interfaz_web_amuled_por_defecto.png" target="_blank"><img src="/assets/img/2022-01-15-howto_incorporar_tema_visual_atractivo_al_interfaz_web_amule/interfaz_web_amuled_por_defecto.png" alt="interfaz de 'amule' por defecto" width="500"/></a>  
<small>interfaz de 'amule' por defecto</small>  
<br>

Sin embargo, como en tantas ocasiones, es posible incorporarle un **tema visual más atractivo**. Por ejemplo ['AmuleWebUI-Reloaded Material'](https://github.com/MatteoRagni/AmuleWebUI-Reloaded).  

<br>
<a href="/assets/img/2022-01-15-howto_incorporar_tema_visual_atractivo_al_interfaz_web_amule/interfaz_web_amuled_AmuleWebUI-Reloaded.png" target="_blank"><img src="/assets/img/2022-01-15-howto_incorporar_tema_visual_atractivo_al_interfaz_web_amule/interfaz_web_amuled_AmuleWebUI-Reloaded.png" alt="interfaz de 'amule' con el tema 'AmuleWebUI-Reloaded Material'" width="600"/></a>  
<small>interfaz de 'amule' con el tema 'AmuleWebUI-Reloaded Material'</small>  
<br>
<br>
<br>
### Procedimiento
En la máquina donde está instalado el demonio o servicio 'amule-daemon' / 'amuled':
~~~bash
sudo systemctl stop amule-daemon.service
cd /usr/share/amule/webserver
sudo git clone https://github.com/MatteoRagni/AmuleWebUI-Reloaded.git
~~~
... y como usuario 'amule' hay que editar el archivo de configuración '/home/amule/.aMule/amule.conf'
~~~bash
su amule
nano /home/amule/.aMule/amule.conf
~~~
y modificar esta línea para que quede así:
~~~
...
[Webserver]
...
Template=AmuleWebUI-Reloaded
...
~~~
Para finalizar cabe iniciar el servicio/demonio 'amule-daemon'
~~~bash
sudo systemctl stop amule-daemon.service
~~~

<br>
Desde cualquier máquina cliente, al dirigirse con el navegador web a `http://ip_de_la_máquina_que_ejecuta_emuled:puerto` se constatará inmediatamente el cambio de aspecto del interfaz.  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] directorios y archivos de transmission-daemon en Debian y derivados](https://hijosdeinit.gitlab.io/howto_directorios_de_transmission_cli_en_debian_y_derivados/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
