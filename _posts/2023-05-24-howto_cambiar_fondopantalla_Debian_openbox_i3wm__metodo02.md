---
layout: post
title: "[HowTo] Configuración de 'i3wm', vol.01b: Cambiar el fondo de pantalla (wallpaper) en OpenBox, i3wm, etc... sin software extra, bajo Debian y derivados"
date: 2021-02-01
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "openbox", "lxde", "lightdm", "i3wm", "wallpaper", "fondo de pantalla", "background", "svg", "imagen", "tema", "theme", "visual", "nitrogen", "feh", "oboinus", "gestor de ventanas", "windows manager"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---









En los gestores de ventanas sencillos como OpenBox o i3wm suele cambiarse el fondo de pantalla mediante algún pequeño programa que ha de instalarse a parte: ['nitrogen'](http://projects.l3ib.org/nitrogen/), ['feh'](https://feh.finalrewind.org/) u ['oboinus'](https://github.com/suniobo/oboinus).  

<br>
Sin embargo, **también es posible cambiar el fondo de pantalla sin necesidad de programas externos**. Se supone que uno de los motivos por los que se eligen gestores de ventanas sencillos o ligeros es que éstos apenas consumen recursos. No es plan de engordarlos.  
En el sistema operativo Debian que tengo instalado constato que éste establece el "fondo de pantalla" por defecto en el archivo `/usr/share/desktop-base/futureprototype-theme/login/background.svg`. <small>En otros sistemas puede estar ubicado en otro directorio / archivo; averiguese primero.</small>  
Se trata de una imagen vectorial (.svg) de apenas 24 kb. Es la magia de las imagenes vectoriales: ocupan poco para la calidad que ofrecen.  
> 3809711  24K -rw-r--r-- 1 root root  24K ene 15 23:24 background.svg  

Este archivo es el fondo de pantalla por defecto para la pantalla de login y para el escritorio.  
Por consiguiente, bastará con sustituirlo por otro archivo con la imagen que más nos satisfaga. En mi caso opté por una chuleta de los atajos de teclado de i3wm.
<br>
## Procedimiento
máquina: netbook de hace una década, 1gb de RAM.  
sistema operativo: Debian 10 (netinstall)  
wm: lxde, openbox, i3wm  
<br>
1. Averiguese la resolución máxima de la pantalla que se está empleando
2. Hágase una copia de seguridad de `/usr/share/desktop-base/futureprototype-theme/login/background.svg` ejecutando el siguiente comando <span style="color:lime">`sudo mv /usr/share/desktop-base/futureprototype-theme/login/background.svg /usr/share/desktop-base/futureprototype-theme/login/background.svg.bkp01`</span>
3. Créese una imagen vectorial .svg de la resolución máxima de la pantalla que se está utilizando y guárdese como `background.svg`. <small>[Inkscape](https://inkscape.org/es/) es una aplicación perfecta para crear imagenes vectoriales.</small>
4. Muévase esa imagen recién creada a nuestro gusto al directorio `/usr/share/desktop-base/futureprototype-theme/login/` como superusuario: <span style="color:lime">`sudo mv `*rutaubicacionimagensvgcreada*`/background.svg /usr/share/desktop-base/futureprototype-theme/login/`</span>  

<br>
Tras reiniciar el sistema, el fondo de pantalla habrá cambiado.  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Instalación de i3wm en Debian 10 vía repositorio oficial](https://hijosdeinit.gitlab.io/howto_i3wm_debian_10_netbook/)
- [[HowTo] Configuración de 'i3wm', vol.02: los nombres de las teclas](https://hijosdeinit.gitlab.io/howto_configuracion_i3wm_vol02__nombres_teclas/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.emezeta.com/articulos/openbox-personalizar-escritorio-linux">emezeta</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
