---
layout: post
title: "La privacidad hoy (enero 2022) en 'Audacity'. Tras la tempestad"
date: 2022-01-29
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "ubuntu", "audacity", "plugin", "editor de audio", "audio", "multimedia", "nyquist", "spyware", "adware", "telemetría", "forks", "audacium", "tenacity"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>[El año pasado se produjo un escándalo en torno a Audacity](http://yewtu.be/watch?v=vaX9LuOWRHE), cuya nueva "dirección" decidió incorporar telemetría a este -hasta el momento- súmamente bien considerado software de código abierto.  
Básicamente lo convirtieron en algo bastante parecido al *spyware*, cosa que actualmente no es un *rara avis* en el mundo de las aplicaciones informáticas privativas, sino que es algo generalizado. Inmediatamente la comunidad protestó y, dado que el código fuente es accesible para cualquiera, surgieron *forks* sin telemetría.  
Los dueños de Audacity recularon y limitaron la telemetría, algunas violaciones de la privacidad (recopilación de datos de uso, por ejemplo) y algunas exigencias bastante sospechosas (no permitían su uso por parte de menores de edad). Según los amos de Audacity ahora la telemetría y el envío de información a sus servidores, es voluntaria (puede denegarse).  
Y esta es la [situación hoy -finales de enero de 2022- que puede cosultarse en la página oficial de Audacity](https://www.audacityteam.org/about/desktop-privacy-notice/).</small>  

<br>
<a href="/assets/img/2022-01-29-privacidad_audacity_hoy/audacity_condiciones_privacidad_2022.png" target="_blank"><img src="/assets/img/2022-01-29-privacidad_audacity_hoy/audacity_condiciones_privacidad_2022.png" alt="condiciones de privacidad y telemetría en el 'nuevo' 'Audacity'" width="700"/></a>  
<small>condiciones de privacidad y telemetría en el 'nuevo' 'Audacity'</small>  
<br>

[Audacity - página web oficial](https://www.audacityteam.org)  
[Audacity - repositorio oficial en GitHub](https://github.com/audacity/audacity)  

<br>
<br>
El programa está aún hoy preparado para "chivarse" (si le dejas):
- chequeo de actualizaciones
- reportes de error  

Ahora bien, ¿nos fiamos de estos trileros? Quizás conviene romper la dependencia y ser largoplacista.  

<br>
Visto lo visto, y habiendo forks de Audacity, es posible que muchos deseemos no tener nada que ver con esa gente.  

<br>
## Forks
- [AUDACIUM](https://github.com/SartoxSoftware/audacium)
- [TENACITY](https://github.com/tenacityteam/tenacity)
- ["John Enigma's" Audacity](https://github.com/John-Enigma/audacity)  

<br>
<br>
<br>
Entradas relacionadas:  
- [Incorporación de plugins 'nyquist' a 'Audacity' (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_incorporacion_plugins_audacity/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.audacityteam.org/about/desktop-privacy-notice/">Audacity - Desktop privacy notice</a>  
<a href="https://www.linuxadictos.com/en/the-audacity-novel-continues-two-forks-appeared.html">LinuxAdictos</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

