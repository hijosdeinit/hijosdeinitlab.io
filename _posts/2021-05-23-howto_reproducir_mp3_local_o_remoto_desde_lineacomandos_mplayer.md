---
layout: post
title: "[HowTo] Reproducción de audio local o remoto desde línea de comandos mediante 'mplayer'"
date: 2021-05-23
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "multimedia", "mplayer", "linea de comandos", "terminal", "cli", "url", "mp3"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Mediante el comando 'mplayer' se puede reproducir un archivo de audio -local o remoto- directamente desde línea de comandos.
~~~bash
mplayer nombrearchivoaudio
~~~
Por ejemplo:
~~~bash
mplayer https://download.podcast.jcea.es/podcast1984-1-la_ley_mordaza.mp3
~~~
... reproducirá un episodio del podcast '1984' que está alojado remotamente, en "internet".  

<br>
~~~bash
man mplayer
~~~
... nos proporcionará una lista de los parámetros y atajos de teclado con los que controlar la reproducción. Por ejemplo, mediante la tecla 'repág' se avanza el audio 10 minutos.

Atajos de teclado:  
~~~
'←' y '→': Busca atrás/adelante 10 segundos.

'arriba' y 'abajo': Busca atrás/adelante 1 minuto.

'RePág' y 'AvPág': Busca atrás/adelante 10 minutos.

'[' y ']': Aumenta/disminuye la velocidad de reproducción en un 10%

'{' y '}': Dobla/divide la velocidad de reproducción.

'Retroceso': Devuelve la reproducción a la velocidad normal.

'<' y '>': retrocede/avanza en la lista de reproducción.

'INTRO': Adelanta en la lista de reproducción, incluso más allá del final.

'INICIO' y 'FIN': entrada siguiente/previa del árbol de reproducción en la lista actual.

'INS' y 'SUPR' (sólo lista ASX): siguiente/previa fuente alternativa.

'p' / 'ESPACIO': Pausa (pulsando otra vez se reanuda la reproducción).

'.': Paso adelante.  Pulsando una vez se detiene la película, cada pulsación posterior reproduce un cuadro y detiene la película otra vez (cualquier otra tecla devuelve al modo normal).

'q' / 'ESC': Detiene la reproducción y sale.

'U': Detiene la reproducción (y sale si no se usa -idle).

'+' y '-': Ajusta el retraso de audio en +/- 0.1 segundos.

'/' y '*': Disminuye/aumenta el volumen.

'9' y '0': Disminuye/aumenta el volumen.

'(' y ')': Ajusta el balance de audio hacia el canal izquierdo/derecho.

'm': Silencia el sonido.

'_' (sólo MPEG-TS, AVI y libavformat): Cicla entre las pistas de vídeo disponibles.

'#' (sólo DVD, Blu-ray, MPEG, Matroska, AVI y libavformat): Cicla entre las pistas de audio disponibles.

'TAB' (sólo MPEG-TS y libavformat): Cicla entre los programas disponibles.

'f': Activa/desactiva el modo de pantalla completa (véase también -fs).

'T': Activa/desactiva el modo siempre-visible (véase también -ontop).

'w' y 'e': Disminuye/aumenta el rango de pan y scan.

'o': Activa/desactiva los estados de OSD: nada / posición / posición + tiempo posición + tiempo + tiempo total.

'd': Activa/desactiva estado de eliminación de cuadros: nada / saltar  mostrar  /  saltar  decodificación  (véase '-framedrop' y '-hardframedrop').

'v': Activa/desactiva la visibilidad de los subtítulos.

'j' y 'J': Cicla entre los subtítulos disponibles.

'y' y 'g': Paso adelante/atrás en la lista de subtítulos.

'F': Activa/desactiva la visibilidad de "subtítulos forzados".

'a': Activa/desactiva la alineación de los subtítulos: arriba/ enmedio / abajo.

'x' y 'z': Ajusta el retraso de subtítulos en +/- 0,1 segundos.

'c' (sólo '-capture'): Inicia/detiene la captura del stream principal.

'r' y 't': Mueve los subtítulos arriba/abajo.

'i' (sólo modo '-edlout'): Define inicio y final de un salto EDL y lo escribe en el fichero dado.

's' (sólo captura de pantalla '-vf'): Hace una captura de pantalla.

'S' (sólo captura de pantalla '-vf'): Inicia/detiene la toma de capturas de pantalla.

'I': Muestra el nombre del fichero en el OSD.

'P': Muestra en el OSD la barra de progreso, el tiempo transcurrido y la duración total.

'!' y '@': Va al inicio del capítulo anterior/siguiente.

'D' (sólo 'kerndeint -vo xvmc', '-vo vdpau', '-vf yadif', '-vf'): Activa/desactiva el desentrelazador.

'A': Cicla entre los ángulo disponibles en el DVD.

(Las  teclas  siguientes  son  válidas  únicamente  cuando  se usa una salida de video acelerada por hardware (xv, (x)vidix, (x)mga, etc), el ecualizador por software (-vf eq o -vf eq2) o el filtro de matiz (-vf hue).)
'1' y '2': Ajusta el contraste.
'3' y '4': Ajusta el brillo.
'5' y '6': Ajusta el matiz.
'7' y '8': Ajusta la saturación.

(Las teclas siguientes son válidas únicamente cuando se usa quartz o el controlador de vídeo de corevideo.)
'command' + '0': Reduce la ventana de la película a la mitad de su tamaño original.
'command' + '1': Devuelve la ventana de la película a su tamaño original.
'command' + '2': Aumenta la ventana de la película al doble su tamaño original.
'command' + 'f': Activa/desactiva el modo de pantalla completa (véase también -fs).
'command' + '[' y 'command' + ']': Establece el canal alfa de la ventana de la película.

(Las teclas siguientes son válidas únicamente cuando se usa el controlador de vídeo sdl.)
'c': Cicla entre los diferentes modos de pantalla completa.
'n': Vuelve al modo original.

(Las teclas siguientes son válidas únicamente cuando se usa un teclado multimedia.)
'PAUSE': Pausa.
'STOP': Detiene la reproducción y sale.
'ANTERIOR' y 'SIGUIENTE': Ir atrás/adelante 1 minuto.

(Las teclas siguientes son válidas únicamente cuando se ha compilado con  soporte  para  la  TV  o  DVB  y  tienen prioridad sobre las anteriores.)
'h' y 'k': Elige el canal anterior/siguiente.
'n': Cambia la norma.
'u': Cambia la lista de canales.

(Las teclas siguientes son válidas únicamente cuando se ha compilado con soporte para dvdnav: se usan para navegar por los menús.)
'Teclado numérico 8': Elige el botón hacia arriba.
'Teclado numérico 2': Elige el botón hacia abajo.
'Teclado numérico 4': Elige el botón hacia la izquierda.
'Teclado numérico 6': Elige el botón hacia la derecha.
'Teclado numérico 5': Vuelve al menú principal.
'Teclado numérico 7': Vuelve al menú más cercano (el orden de elección es: capítulo->título->raíz).
'Teclado numérico INTRO': Confirma la elección.

(Las teclas siguientes se usan para controlar el teletexto de TV. Los datos  pueden  proceder  de  una  fuente  TV analógica o un stream de transporte MPEG.)
'X': Teletexto on/off.
'Q' y 'W': Ir a la página siguiente/anterior del teletexto.
~~~

<br>
<br>
<br>
--- --- ---  

<br>
<br>
<br>
<br>
<br>
<br>

