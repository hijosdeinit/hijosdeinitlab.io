---
layout: post
title: "¿gérmenes de Mendicidad 2.0 en la comunidad Linux? Aguánteme el cubata..."
date: 2021-09-21
author: Termita
category: "internet 4.0"
tags: ["internet 4.0", "mendicidad 2.0", "premium", "porqueyolovalgo", "quehaydelomio", "patreon", "nosinmipatreon", "dameargopayo"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Revisando el *feed* de artículos misceláneos que diariamente suministra [uGeek en su canal de Telegram](https://t.me/uGeek), me llamó la atención el titular de un artículo procedente de la web ['AlgoDeLinux'](https://algodelinux.com).
Al consultarlo en el navegador web no pude reprimir una mueca, rictus mezcolanza de mofa y amargura:

<br>
<a href="/assets/img/2021-09-21-mendicidad2.0_comunidad_linux_aguantame_el_cubata/mendicidad2.0_comunidadlinux.png" target="_blank"><img src="/assets/img/2021-09-21-mendicidad2.0_comunidad_linux_aguantame_el_cubata/mendicidad2.0_comunidadlinux.png" alt="monetizando que es gerundio" width="700"/></a>  
<small>"dame argo, paaayo"</small>  
<br>

**Amargura** porque ¿a esto hemos llegado? Reviso la cabecera de la página; recristo, sí, pone "Linux". (<small>Sobreviene, para qué negarlo, la inquietante pregunta: "¿no iré yo también a convertirme en un ser rastrero-pesetero el día de mañana? Porque la mediocridad no es antídoto contra la avaricia o el ridículo, eso es evidente</small>).  
<br>
Y **mofa** porque ¿vd. se cree que alguien le va a pagar por algo que otros proporcionan -gracias a Dios- de forma desinteresada y gratuíta?  

<br>
Veamos, maeses, según el título, que -junto a las cookies y la telemetría- es lo único que pareciera aquí suministran gratis, ¿de qué trataba el artículo? ¿De limpiar la caché de 'apt'?.  
Pues vamos con eso, una simple consulta en [FuckOffGoogle](https://search.fuckoffgoogle.net/), mi querido metabuscador, que podría hacer cualquier niño de primaria (<small>y, por cierto, de fomentar eso se trata, porque si no mal futuro nos espera</small>) para ver cómo se cotiza ese contenido:

<br>
<a href="/assets/img/2021-09-21-mendicidad2.0_comunidad_linux_aguantame_el_cubata/comunidad_linux_contenidos_desinteresados.png" target="_blank"><img src="/assets/img/2021-09-21-mendicidad2.0_comunidad_linux_aguantame_el_cubata/comunidad_linux_contenidos_desinteresados.png" alt="zzz" width="600"/></a>  
<br>

Gratis y desinteresado, me lo temía.  

<br>
Luego está, claro, el feo asuntillo que se observa en la parte inferior de la web. Sí, la incómoda verdad de las "cookies" y la cuantificación.  

<br>
<a href="/assets/img/2021-09-21-mendicidad2.0_comunidad_linux_aguantame_el_cubata/traga_cookies.png" target="_blank"><img src="/assets/img/2021-09-21-mendicidad2.0_comunidad_linux_aguantame_el_cubata/traga_cookies.png" alt="cookies, todas, sí o sí... porque vds lo valen" width="700"/></a>  
<small>*cookies* sí o sí, porque vds lo valen. Tengan un centimillo, de nada.</small>  
<br>

Miren que conozco sitios agoniosos y avariciosos, y todos ellos permiten **denegar** todas las *cookies* salvo las indispensables. Hasta los casinos y las casas de putas. No es el caso aquí: en AlgoDeLinux.com / QueHayDeLoMío.com las opciones con que se le da la bienvenida al visitante son:
- aceptar TODAS, es decir, tragar con sus manejos, dejar que te cuantifiquen y te analicen.
- pasar de todo, no pulsar en aceptar (ni en nada), lo cual trae implícito el "aceptar" y "tragar".  
<br>
Qué bien.  

<br>
Y, a todo esto, <big>¿cómo se limpia la caché "de paquetes" en Debian y derivados?</big>  
Muy sencillo, la caché de 'apt' se limpia de la siguiente forma:
~~~bash
sudo apt-get clean
~~~
Y lo barato que nos ha salido.  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
