---
layout: post
title: "[HowTo] Obtención de un registro ('log') de errores y de actividad del comando 'tar'"
date: 2021-09-14
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "tar", "comprimir", "empaquetar", "respaldo", "backup", "copia de seguridad", "compresores", "gzip", "rar", "unrar", "7zip", "lzma", "zip", "unzip", "arj", "log", "registro", "registro de errores", "actividad", "entrada", "salida", "input", "output"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
El comando ['tar'](https://manpages.debian.org/bullseye/tar/tar.1.en.html), presente en casi todas las distribuciones GNU Linux, sirve para empaquetar información (directorios, archivos). Si se le añade el parámetro <span style="background-color:#042206"><span style="color:lime">`-z`</span></span> además comprime los datos.  
Es una herramienta muy versátil que, incluso (véase el parámetro <span style="background-color:#042206"><span style="color:lime">`-G`</span></span>), puede servir para crear copias de seguridad incrementales.  

<br>
Sin embargo, *per se*, el comando 'tar', que no es un servicio, no "emite" un registro de errores o de la actividad desempeñada. Mas esto no significa que sea imposible obtener esa información, muchas veces necesaria y/o valiosa; se hace así:  
<br>
<span style="background-color:#042206"><span style="color:lime">`tar -cvzpf /`*output*.tar.gz *input*` > `*registrodeactividad.log*` 2> `*registrodeerrores.log*</span></span>  
<br>
Donde:  
- <span style="background-color:#042206"><span style="color:lime">**`-c`**</span></span> es el parámetro que indica que lo que se está haciendo es crear.
- <span style="background-color:#042206"><span style="color:lime">**`-v`**</span></span> es el parámetro que indica que deseamos ver por pantalla información de todo lo que acontece en el procedimiento (en este caso el parámetro es inútil porque la información de actividad y de errores que pudieran acontecer no la mostrará por pantalla, sino que la guardará en esos 2 archivos '.log' indicados al final de la línea.  
- <span style="background-color:#042206"><span style="color:lime">**`-z`**</span></span> parámetro que indica que se desea compresión
- <span style="background-color:#042206"><span style="color:lime">**`-p`**</span></span> es el parámetro que establece que han de guardarse los derechos de acceso en la extracción
- <span style="background-color:#042206"><span style="color:lime">**`-f`**</span></span> parámetro que indica que lo que se escriba a continuación es un fichero (el nombre del fichero "comprimido" o "empaquetado" que va a crearse)
- <span style="background-color:#042206"><span style="color:lime">***`output.tar.gz`***</span></span> es el fichero resultante, que contendrá -comprimido o no- lo que establezcamos en el *imput* (origen).
- <span style="background-color:#042206"><span style="color:lime">***`input`***</span></span> es el origen, los datos que deseamos "empaquetar" y/o "comprimir"
- <span style="background-color:#042206"><span style="color:lime">`> `***registrodeactividad.log***</span></span> indica que toda la información de la ejecución de 'tar' se almacene en ese archivo
- <span style="background-color:#042206"><span style="color:lime">`2> `***registrodeerrores.log***</span></span> indica que los errores que pudieran acontecer en la ejecución del comando 'tar' se registren / almacenen en ese archivo.  

<br>
Son esos **2 últimos parámetros** los que provocan que el registro de la ejecución -errores y actividad- del comando 'tar' se almacenen en 2 archivos para su posterior revisión.  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Comparación de la suma de verificación md5 de varios directorios mediante 'tar'](https://hijosdeinit.gitlab.io/howto_comparacion_md5sum_varios_directorios_mediante_tar/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.puuteri.org/569153-where-to-view-error-log-QIGFXW">puuteri.org</a>  
<a href="https://www.ionos.es/digitalguide/servidores/herramientas/programa-de-archivo-tar-como-crear-backups-en-linux/">ionos</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
