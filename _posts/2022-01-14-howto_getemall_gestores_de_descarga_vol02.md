---
layout: post
title: "[HowTo] Gestores de descarga, vol.02: 'Get'emAll'"
date: 2022-01-14
author: Termita
category: "descargas"
tags: ["descargas", "internet 4.0", "sistemas operativos", "gnu linux", "linux", "Windows", "macos", "firefox", "chrome", "chromium", "opera", "basilisk", "pale moon", "borealis", "librewolf", "waterfox", "librefox", "web browser", "navegador web", "productividad", "bulk", "masivo", "en masa", "downthemall", "downwemall", "xpi", "extensiones", "plugins", "getemall"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>«Un gestor (o administrador o acelerador) de descargas es un programa diseñado para descargar archivos de Internet, ayudado de distintos medios como algoritmos, para ir pausando y reanudando las descargas de algún servidor FTP o página de Internet. Es muy recomendable su utilización cuando se trata de archivos grandes, tales como imágenes ISO, programas, vídeos, música, por mencionar algunos.  
A diferencia de un navegador (que permite navegar por las páginas de la web, siendo la función de descarga de menos importancia), un gestor de descargas permite automatizar la descarga de varios archivos de acuerdo a parámetros de configuración. En particular, un gestor de descargas se puede utilizar para hacer una réplica de un sitio web completo.»</small>  

<br>
Existen varios gestores de descarga interesantes, como son ['aria2'](https://aria2.github.io/), ['uGet'](https://ugetdm.com/), [JDownloader](https://jdownloader.org/), ['DownThemAll'](https://www.downthemall.org/) o 'Get'EmAll'. Tarde o temprano este blog irá tratando acerca de cada uno de ellos. Se inició la saga con 'DownThemAll', ahora toca hablar de **'Get'emAll'** una modificación de éste.  

<br>
<a href="/assets/img/2022-01-14-howto_getemall_gestores_de_descarga_vol02/getemall.png" target="_blank"><img src="/assets/img/2022-01-14-howto_getemall_gestores_de_descarga_vol02/getemall.png" alt="interfaz de 'Get'EmAll'" width="500"/></a>  
<small>interfaz de 'Get'EmAll'</small>  
<br>

<br>
[Get'EmAll](https://deusrex.neocities.org/xul/getemall/getemall.html) es una modificación de 'DownThemAll' creada para los navegadores web ['Borealis'](https://binaryoutcast.com/projects/borealis/), ['Pale Moon'](https://www.palemoon.org/) y/o sus derivados como ['Basilisk'](https://hijosdeinit.gitlab.io/howto_basilisk_ubuntu_18_bionic/).  

<br>
Es, al igual que 'DownThemAll' «una poderosa y fácil de usar extensión que añade nuevas capacidades de descarga al navegador web. DownThemAll permite descargar todos los contenidos -enlaces, imagenes, etc...- de un sitio web.  
Además pueden aplicarse filtros personalizados para refinar las descargas y descargar así únicamente lo que se desea.  
Permite poner en cola, pausar y reemprender descargas en cualquier momento, y está totalmente integrado en el navegador web».  
<br>
Tiene las mismas características y similar modo de uso que DownThemAll, que es detallado en [esta entrada de este blog](https://hijosdeinit.gitlab.io/howto_DownThemAll_gestores_de_descarga_vol01/).  

<br>
<br>
## ¿Cómo "instalar" 'Get'EmAll'?
Si en nuestro navegador está instalado 'DownThemAll', desinstálese para prevenir conflictos.  
Desde el menú de nuestro navegador ('Borealis', 'Pale Moon' o derivados como 'Basilisk'), en el apartado 'Extensiones' bastará con buscar "getemall" y pulsar en el botón correspondiente a 'instalar'.  

<br>
<br>
## Otros recursos relativos a 'GetEmAll'
- [Repositorio de 'Get'EmAll' en 'GitLab'](https://gitlab.com/xul-extensions/getemall). Ahí se puede encontrar, por ejemplo, el código fuente.
- ['Get'EmAll en el repositorio de *addons* del navegador 'Pale Moon'](https://addons.palemoon.org/addon/getemall/).  


<br>
<br>
Entradas relacionadas:  
- [[HowTo] Gestores de descarga, vol.01: 'DownThemAll'](https://hijosdeinit.gitlab.io/howto_DownThemAll_gestores_de_descarga_vol01/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
