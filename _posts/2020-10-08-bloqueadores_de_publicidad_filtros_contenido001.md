---
layout: post
title: 'Bloqueadores de publicidad, filtros de contenido. [vol. I]: un listado'
date: 2020-10-06
author: Termita
category: "seguridad"
tags: ["sistemas operativos", "PiHole", "AdGuard", "AdAway", "uBlock", "web 4.0", "anuncios", "publicidad", "propaganda", "DNS"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
No soy un experto en redes, sólo un usuario. Y el léxico suele ir acorde con el conocimiento. Siempre fui férreo defensor del "hablar con propiedad" mas lo que aquí y en otros capítulos explico está basado en mi experiencia personal -sencilla, plagada de lagunas, vacíos conceptuales y guiada muchas veces por la intuición más que por la "ciencia"- como usuario de computadoras y redes.  
Una vez dicho esto, procedo a explicar lo que he sacado en claro de este asunto de los bloqueadores de publicidad y los filtros de contenido.  

Un filtro de contenido es un sistema que rechaza los contenidos que el usuario le ha ordenado que rechace (pornografía, publicidad, spam, sitios web concretos, etcétera...).  
Un bloqueador de publicidad es un filtro de contenido que rechaza los contenidos publicitarios y/o propagandísticos.  

<br>
![Esquema de Filtros de contenido](/assets/img/2020-10-08-bloqueadores_de_publicidad/esquema_fitros_contenidos.png)  
<small>Esquema de Filtros de contenido</small>  

<br>
<br>
## bloqueadores / filtros a nivel red (centralizados)
Se instalan en un servidor que puede estar en la propia red local o bien fuera de ella ("en internet")

"internet" --- "router" --- máquina cliente

<br>
### En la red local (LAN)
- PiHole
- AdGuard
- ¿Ad Away? (¿android?  

<br>
### En "internet" (WAN)
- DNS de ???? echar un vistazo a floppy y otras web exquisitas
- PiHole (servidor externo a la red local)  

<br>
<br>
## bloqueadores / filtros en propia máquina
<br>
### En el navegador web
- uBlock origin  

<br>
### En el sistema operativo de la máquina
- archivo hosts
- cortafuegos  

<br>
<br>
<br>
---  
<small>Fuentes:  
[]()</small>  
<br>
<br>
<br>
<br>
<br>
