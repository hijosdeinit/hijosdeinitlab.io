---
layout: post
title: "[HowTo] 'FreeTube': 'YouTube' sin publicidad al más puro estilo 'NewPipe'"
date: 2020-04-23
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "Windows", "ad", "adblock", "freetube", "multimedia", "newpipe", "privacidad", "publicidad", "web 4.0", "youtube", "degoogled", "privacidad", "youtube-dl", "tracking", "audio", "video"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<big>**! Actualización 2021-05-21: FreeTube no reproduce videos de Youtube, al menos en mi sistema. Probablemente no está al día en lo que al software de Youtube respecta.**</big>  

<br>
<br>
[Freetube](https://freetubeapp.io/) es una aplicación para ordenador que permite visualizar contenidos de Youtube sin publicidad y salvaguardando la privacidad.  
Es el equivalente a [NewPipe](https://newpipe.schabi.org/), aplicación para Android que [tiene una entrada dedicada en este blog](https://hijosdeinit.gitlab.io/newpipe-contenidos-de-youtube-como-si/) y permite visualizar youtube sin seguimiento, sin publicidad, con privacidad y permite cosas que sólo están permitidas a los clientes de pago de Google, como son: apagar la pantalla o hacer otras cosas mientras los videos se reproducen, o descargar los videos (youtube-dl).  

[El código y los binarios están en Github](https://github.com/FreeTubeApp/FreeTube/releases), desde donde pueden ser descargados y también revisados, auditados. A día de hoy aún está en fase beta, versión 0.7.2, mas una vez descargado e instalado el programa es funcional, cumple con su cometido.  

<br>
<a href="/assets/img/2020-04-23-howto_freetube_youtube_sin_publicidad/freetube_001.png" target="_blank"><img src="/assets/img/2020-04-23-howto_freetube_youtube_sin_publicidad/freetube_001.png" alt="releases de freetube" width="800"/></a>  
<small>Releases de 'freetube'</small>  
<br>

Existe versión para Windows, Mac, Ubuntu/Debian, Fedora/RedHat y otras distribuciones GNU Linux en sus variantes arm y x86. Se puede, asimismo, descargar el código fuente para compilarlo.  

<br>
<a href="/assets/img/2020-04-23-howto_freetube_youtube_sin_publicidad/freetube_002.png" target="_blank"><img src="/assets/img/2020-04-23-howto_freetube_youtube_sin_publicidad/freetube_002.png" alt="Descarga de freetube" width="800"/></a>  
<small>Descarga de 'freetube'</small>  
<br>

<br>
## Instalación de 'freetube' en Debian y derivados
El procedimiento más sencillo para instalar FreeTube 0.7.2 en Debian y derivados es:  

1. Instalar las dependencias
~~~bash
sudo apt install libappindicator3-1
~~~

2. Descargar el .deb desde el repositorio oficial
~~~bash
wget https://github.com/FreeTubeApp/FreeTube/releases/download/v0.7.2-beta/FreeTube_0.7.2_amd64.deb  
~~~

3. Instalar el .deb haciéndole doble click o bien desde terminal mediante dpkg
~~~bash
sudo dpkg --install FreeTube_0.7.2_amd64.deb
~~~

<br>
Nota: Si queremos una versión más actualizada, substitúyase la url de descarga por la actualizada (así como el nombre del fichero .deb, obviamente).  

<br>
<br>
## Trabajando con 'freetube'
FreeTube permite seleccionar la calidad de video que se desea en cada momento, e incluso el tipo de reproducción. En esto último permite 3 opciones:
- Dash format
- Legacy format
- "youtube player" format  

<br>
Me he percatado de que algunos filtros antipublicidad (regex) que tengo en la red local mediante PiHole  (filtro DNS) interfieren en algunos videos que trato de visualizar con NewPipe (Android) y, probablemente esa sea la explicación al por qué no se pueden visualizar algunos videos en FreeTube.
Aparentemente esto afecta a los vídeos que tienen muchas visitas. Es posible que Google introduzca algún código extra en los vídeos que tienen muchas visitas, que dicho código sea detectado y filtrado por 'PiHole' y que el efecto de esto sea que el video no se puede reproducir de forma convencional en aplicaciones como FreeTube o NewPipe.  
Podría sospechar, por otro lado, que se trata algún tipo de código que marca vídeos desafectos al régimen, puesto por Google a instancia de los estados. Debería investigar eso, mas no tengo tiempo ni conocimientos.  

Lo que sí sé es que una solución a esto es, en el apartado 'video formats' escoger el formato 'youtube player' para ese video que está dando "problemas".  

<br>
<a href="/assets/img/2020-04-23-howto_freetube_youtube_sin_publicidad/freetube_003.png" target="_blank"><img src="/assets/img/2020-04-23-howto_freetube_youtube_sin_publicidad/freetube_003.png" alt="freetube" width="800"/></a>  
<small>'freetube'</small>  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] 'invidious', 'Youtube' desde el navegador sin publicidad ni rastreo](https://hijosdeinit.gitlab.io/howto_invidious_youtube_desde_navegador_sin_publicidad/)
- [NewPipe: contenidos de youtube con privacidad y con privilegios de cliente de pago (sin pagarles un céntimo)](https://hijosdeinit.gitlab.io/newpipe-contenidos-de-youtube-como-si/)
- [[HowTo] Mejor forma de incorporar la última versión de 'youtube-dl' en Debian y derivados. II](https://hijosdeinit.gitlab.io/howto_youtube-dl_debian_netinstall/)  

<br>
<br>
<br>
<br>
<br>
<br>
