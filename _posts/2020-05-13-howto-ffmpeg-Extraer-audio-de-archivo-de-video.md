---
layout: post
title: "[HowTo] Extraer mediante ffmpeg el audio de un archivo de video"
date: 2020-05-13
author: Termita
category: "multimedia"
tags: ["multimedia", "sistemas operativos", "transcodificar", "transcodificacion", "extraccion", "gnu linux", "linux", "cli", "avi", "ogg", "ogm", "webm", "mp3", "aac", "vorbis", "mencoder", "ffmpeg", "audio", "video", "ffprobe"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Hay veces que deseamos extraer la pista de audio de un fichero de video.  

> Esto puede suceder por varias razones. Por ejemplo cuando se corrompe el archivo que contiene una película y no hay forma de obtenerla nuevamente en la red en idioma español:
- Arreglamos el índice de la fichero corrupto que contiene la película
- Extraemos el audio del recién "reparado" fichero corrupto que contiene la película
- Descargamos la versión extranjera de dicha película
- Le añadimos la pista de audio que extrajimos.  

<br>
La extracción del audio de un video se puede hacer con [**ffmpeg**](https://ffmpeg.org/), una herramienta que evidentemente sirve para muchas cosas más que transcodificar video.  

El procedimiento es el siguiente:  

<br>
Averiguamos las características de la pista de audio que queremos extraer (códec, etc...)
~~~bash
ffmpeg -i ArchivoDeVideo
~~~
ô
~~~bash
ffprobe ArchivoDeVideo
~~~

<br>
Extraemos la pista de audio, por ejemplo, en .mp3 (si la pista está en ese formato)
~~~bash
ffmpeg -i ArchivoDeVideo -vn -acodec copy Archivo_Pista_De_Audio.mp3
~~~  

<br>
<br>
## Recodificando / Transcodificando / Convirtiendo
Si deseáramos transformar la codificación (transcodificar) la pista de audio algunos ejemplos:  

transcodificando a mp3:
~~~bash
ffmpeg -i ArchivoDeVideo -vn -acodec mp3 Archivo_Pista_De_Audio.mp3
~~~
transcodificando a aac:
~~~bash
ffmpeg -i ArchivoDeVideo -vn -acodec aac Archivo_Pista_De_Audio.aac
~~~
transcodificando a vorbis .ogg:
~~~bash
ffmpeg -i ArchivoDeVideo -vn -acodec libvorbis Archivo_Pista_De_Audio.ogg
~~~

<br>
... o también, transcodificando a partir de un video **.webm** a .mp3
~~~bash
ffmpeg -i ArchivoDeVideo.webm -vn -ab 128k -ar 44100 -y Archiv_Pista_De_Audio.mp3
~~~

<br>
<br>
## Extraer el audio de un fragmento de video
Si deseara extraerse, no el audio de todo el video sino el audio de un fragmento de video (por ejemplo, del segundo 10 al segundo 40, y transcodificando a .mp3):
~~~bash
ffmpeg -i ArchivoDeVideo -ss 10 -to 40 -vn -acodec mp3 Archivo_Pista_De_Audio.mp3
~~~

<br>
<br>
## Extraer 1 de varias pistas de audio de un video
Cuando un archivo de vídeo tiene varias pistas de audio, el parámetro <big>**'map'**</big> permite seleccionar cuál de ellas extraer.  
El procedimiento es sencillo:  

Averiguamos las características de la pista de audio que queremos extraer (códec, etc...)
~~~bash
ffmpeg -i ArchivoDeVideo
~~~
ô
~~~bash
ffprobe ArchivoDeVideo
~~~

<br>
<a href="/assets/img/2020-05-13-howto-ffmpeg-Extraer-audio-de-archivo-de-video/ffmpeg-i_listado_pistas_audio_de_un_video.png" target="_blank"><img src="/assets/img/2020-05-13-howto-ffmpeg-Extraer-audio-de-archivo-de-video/ffmpeg-i_listado_pistas_audio_de_un_video.png" alt="listado de pistas de audio de un video" width="800"/></a>  
<small>listado de pistas de audio de un video</small>  

<br>
En la fotografía de ejemplo se observan 2 pistas de audio:
- `Stream #0:1(spa): Audio: ac3, 48000 Hz, 5.1(side), fltp, 448 kb/s`
- `Stream #0:2(eng): Audio: ac3, 48000 Hz, 5.1(side), fltp, 640 kb/s`  

<br>
En este ejemplo, para **extraer la pista en español** -`Stream #0:1(spa)`- **convirtiéndola a .mp3** bastaría con ejecutar desde línea de comandos:
~~~bash
ffmpeg -i ArchivoDeVideo -map 0:1 -vn -acodec mp3 pista1_audio_español.mp3
~~~

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Convertir vídeo ‘.ogv’ a otros formatos](https://hijosdeinit.gitlab.io/howto_convertir_ogv_a_otros_formatos/)
- [[HowTo] Incorporar pista de audio a una pista de video](https://hijosdeinit.gitlab.io/howto_incorporar_pista_audio_a_video/)
- [[HowTo] Trocear un video](https://hijosdeinit.gitlab.io/howto_trocear_video/)
- [[HowTo] Cálculo del bitrate de un video para su recompresión en 2 pasadas](https://hijosdeinit.gitlab.io/howto_calculo_bitrate_video_recompresion_2_pasadas_ffmpeg_handbrake/)
- [[HowTo] instalacion de HandBrake en Ubuntu y derivados](https://hijosdeinit.gitlab.io/howto_instalacion-handbrake-ubuntu-apt/)
- [[HowTo] Extracción de imagenes de un video mediante ffmpeg (GNU Linux)](https://hijosdeinit.gitlab.io/howto_ffmpeg_extraer_fotogramas_de_archivo_video/)
- [[HowTo] Aligerar, reducir y/o recomprimir audio mediante 'ffmpeg'](https://hijosdeinit.gitlab.io/howto_aligerar_reducir_recomprimir_audio_mediante_ffmpeg/)  

<br>
<br>
<br>

--- --- ---
<small>
Fuentes:  
[UGeek](https://ugeek.github.io/blog/post/2019-09-30-extraer-audio-de-un-v%C3%ADdeo-con-ffmpeg.html)  
[BugCodeMaster](https://www.bugcodemaster.com/article/extract-audio-video-using-ffmpeg)  
[Artem Vasiliev en StackOverflow](https://stackoverflow.com/a/54380171)  
[llogan en StackOverflow](https://stackoverflow.com/a/32925753)  
[ByteFreaks.net](https://bytefreaks.net/gnulinux/bash/ffmpeg-extract-audio-from-webm-to-mp3)</small>  

<br>
<br>
<br>
