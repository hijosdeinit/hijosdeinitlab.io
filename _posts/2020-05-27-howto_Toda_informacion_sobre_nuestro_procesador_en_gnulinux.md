---
layout: post
title: '[HowTo] Obtener toda la información sobre nuestro procesador desde línea de comandos en GNU Linux'
date: 2020-05-27
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "cpu", "core", "hardware", "procesador", "nproc", "lscpu", "cpuinfo", "CLI"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
![esquema múltiples procesadores, múltiples núcleos]({{site.url}}{{site.baseurl}}{{site.assets_path}}/img/2020-05-27-[HowTo]_Toda_informacion_sobre_nuestro_procesador_en_gnulinux/cpu_multiple_core_multiple.png)

~~~
nproc --all
~~~  

~~~
lscpu | grep 'CPU(s)'
~~~  

Averiguar el número de *cpu*s/*core*s **físicos**:
~~~
lscpu -p | egrep -v '^#' | wc -l
~~~  

Averiguar el número de *cpu*s **lógicas** (*core*s con HT incluídos):
~~~
lscpu -p | egrep -v '^#' | sort -u -t, -k 2,4 | wc -l
~~~  

En '/proc/cpuinfo' hay mucha información sobre el procesador:
~~~
grep processor /proc/cpuinfo | wc -l
~~~  
~~~
cat /proc/cpuinfo | grep "model name"
~~~  
~~~
cat /proc/cpuinfo | grep "cpu cores"
~~~  

~~~
getconf _NPROCESSORS_ONLN
~~~  

~~~
sudo dmidecode -t 4 | egrep 'Socket Designation|Count'
~~~  
<br>
<br>
No basta con que nuestro procesador tenga varios núcleos, hilos, etcétera. Es necesario que el *kernel* de Linux tenga capacidad para manejarlos. Para asegurarnos de que el *kernel* que estamos empleando tiene soporte multiprocesador (SMP, *Symmetric MultiProcessing*):
~~~
uname -a
~~~
> <small> **Linux MÁQUINA01 5.3.0-53-generic #47~18.04.1-Ubuntu <big>SMP</big> Thu May 7 13:10:50 UTC 2020 x86_64 x86_64 x86_64 GNU/Linux**</small>  

<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[Daniel López Azaña](https://www.daniloaz.com/es/como-saber-cuantos-procesadores-y-nucleos-tiene-una-maquina-linux/)  
[Daniel López Azaña](https://www.daniloaz.com/es/diferencias-entre-cpu-fisica-cpu-logica-core-nucleo-thread-hilo-y-socket/)</small>
<br>
<br>
<br>
<br>
