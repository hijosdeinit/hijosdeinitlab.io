---
layout: post
title: "[HowTo] Atajos de teclado del editor 'nano'"
date: 2019-07-21
author: Termita
category: "productividad"
tags: ["sistemas operativos", "gnu linux", "pico", "nano", "editor de texto", "vim", "vi", "emacs", "texto", "programación", "texto plano", "latex", "herramientas", "código", "markdown", "org mode", "pandoc", "keybindings"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Ctrl + W : usar un buscador de texto.  
Ctrl + 6 : seleccionar texto.  
Ctrl + K : cortar el texto.  
Ctrl + U : pegar el texto.  
Ctrl + P : subir el cursor a la línea anterior.  
Ctrl + M : hacer lo mismo que el Enter.  
Ctrl + B : volver atrás letra a letra.  
Ctrl + V : ir al fondo del documento, a la línea final.  
Ctrl + A : ir al principio de la línea o el texto.  
Ctrl + O : salvar el documento.  
Ctrl + C : cancelar la operación de guardado y además, ver el número de línea.  
Ctrl + T : revisar la ortografía.  
Alt + Shift + 3 : Muestra los números de línea a la izquierda  (*) No funciona en Ubuntu 18.04 [Solución](https://hijosdeinit.gitlab.io/nano__mostrar_numeros_linea_cuando_altshift3_nofunciona/).  

<br>
El editor 'nano' está diseñado para emular la funcionalidad y sencillez de uso del editor de texto 'Pico'. El editor cuenta con cuatro secciones principales. La línea superior muestra la versión del programa, el nombre del fichero que se está editando, y si ha sido modificado o no. La ventana principal del editor muestra lo que está siendo editado. La línea de estado es la tercera empezando por abajo y muestra mensajes importantes.  Las dos últimas líneas muestran las combinaciones de teclas usadas más a menudo en el editor.  

<br>
<br>
Las combinaciones de teclas se escriben como sigue: las secuencias con la tecla Control se indican con <span style="background-color:#042206"><span style="color:lime">`^`</span></span> y se pueden introducir tanto pulsando la tecla `Ctrl` como pulsando dos veces `Esc`.  Las secuencias con la tecla 'Meta' se indican con <span style="background-color:#042206"><span style="color:lime">`M-`</span></span> y se pueden introducir con las teclas 'Alt', 'Cmd' o 'Esc', dependiendo de su configuración de teclado.  Además, si pulsa dos veces 'Esc' y escribe después un código decimal de tres dígitos entre 000 y 255, introducirá el carácter de valor correspondiente. Dispone de las siguientes pulsaciones en la ventana principal del editor. Las pulsaciones alternativas se muestran entre paréntesis:  

<br>
^G    (F1)      Mostrar esta ayuda  
^X    (F2)      Cerrar el fichero mostrado / Salir de nano  
^O    (F3)      Escribir el fichero actual a disco  
^R    (F5)      Insertar otro fichero en el actual  

<br>
^W    (F6)      Buscar hacia delante una cadena o expresión regular  
^\    (M-R)     Reemplazar una cadena o expresión regular  
^K    (F9)      Cortar la línea actual y guardarla en el cutbuffer  
^U    (F10)     Pegar el cutbuffer en la línea actual  

<br>
^J    (F4)      Justificar el párrafo actual  
^T    (F12)     Invocar el corrector ortográfico (si está disponible)  
                Invocar el corrector de sintaxis (si está disponible)  
                Invocar el arreglador (si está disponible)  

<br>
^C    (F11)     Mostrar la posición del cursor  
^_    (M-G)     Ir a una línea y columna  

<br>
M-U             Deshacer la última operación  
M-E             Rehacer la última operación deshecha  

<br>
M-A   (^6)      Marcar texto desde la posición actual del cursor  
M-6   (M-^)     Copiar la línea actual y guardarla en el cutbuffer  

<br>
M-]             Ir a la llave correspondiente  

<br>
M-W   (F16)     Repetir la última búsqueda  
M-▲             Seguir buscando hacia atrás  
M-▼             Seguir buscando hacia delante  

<br>
^B    (◀)       Ir hacia atrás un carácter  
^F    (▶)       Ir hacia delante un carácter  
^◀    (M-Space) Ir hacia atrás una palabra  
^▶    (^Space)  Ir hacia delante una palabra  
^A    (Home)    Ir al principio de la línea actual  
^E    (End)     Ir al final de la línea actual  

<br>
^P    (▲)       Ir a la línea anterior  
^N    (▼)       Ir a la siguiente línea  
M--   (M-_)     Desplazar el texto una línea arriba sin mover el cursor  
M-+   (M-=)     Desplazar el texto una línea abajo sin mover el cursor  

<br>
^▲    (M-7)     Ir al bloque de texto anterior  
^▼    (M-8)     Ir al siguiente bloque de texto  
M-(   (M-9)     Ir al principio del párrafo; después, al del párrafo anterior  
M-)   (M-0)     Ir al final del párrafo; después, al del párrafo siguiente  

<br>
^Y    (F7)      Ir una pantalla hacia arriba  
^V    (F8)      Ir una pantalla hacia abajo  
M-\   (^Home)   Ir a la primera línea del fichero  
M-/   (^End)    Ir a la última línea del fichero  

<br>
M-◀   (M-<)     Cambiar al búfer de fichero anterior  
M-▶   (M->)     Cambiar al siguiente búfer de fichero  

<br>
^I    (Tab)     Insertar un carácter de tabulación en la posición del cursor  
^M    (Enter)   Insertar un retorno de carro en la posición del cursor  

<br>
^D    (Del)     Borrar el carácter bajo el cursor  
^H    (Bsp)     Borrar el carácter a la izquierda del cursor  
                Cortar hacia atrás desde cursor a principio de palabra  
                Cortar desde cursor a principio de siguiente palabra  
M-T             Cortar desde el cursor hasta el final de línea  

<br>
M-J             Justificar el fichero completo  
M-D             Contar el número de palabras, líneas y caracteres  
M-V             Insertar la próxima pulsación literalmente  

<br>
^L              Redibujar la pantalla actual  
^Z              Suspender el editor (si la suspensión está activada)  

<br>
M-}   (Tab)     Sangrar la línea actual (o las líneas marcadas)  
M-{   (Sh-Tab)  Quitar sangrado de la línea actual (o las líneas marcadas)  

<br>
M-3             Comentar/descomentar la línea actual (o las líneas marcadas)  
^]              Intenta completar la palabra actual  

<br>
M-:             Iniciar/parar grabación de macro  
M-;             Ejecutar la última macro grabada  

<br>
^Q              Buscar hacia atrás una cadena o expresión regular  

<br>
^S              Guardar fichero sin preguntar  

<br>
M-X             Modo de ayuda activar/desactivar  
M-C             Mostrar constantemente la posición del cursor activar/desactivar  
M-O             Uso de una línea más para editar activar/desactivar  
M-S             Desplazamiento suave activar/desactivar  
M-$             Ajuste suave de líneas largas activar/desactivar  
M-#             Numeración de líneas activar/desactivar  
M-P             Muestra los blancos activar/desactivar  
M-Y             Coloreado de sintaxis activar/desactivar  

<br>
M-H             Tecla de inicio inteligente activar/desactivar  
M-I             Auto-sangrado activar/desactivar  
M-K             Cortado desde el cursor hasta el final de línea activar/desactivar  
M-L             Ajuste estricto de líneas largas activar/desactivar  
M-Q             Conversión de pulsaciones de tabulador a espacios activar/desactivar  

<br>
M-B             Respaldo de ficheros activar/desactivar  
M-F             Leer fichero dejándolo en un buffer diferente activar/desactivar  
M-M             Soporte para ratón activar/desactivar  
M-N             Sin conversión desde formato DOS/Mac activar/desactivar  
M-Z             Suspensión activar/desactivar  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Mostrar el número de línea en el editor 'nano' cuando Alt+Shift+3 no funciona](https://hijosdeinit.gitlab.io/nano__mostrar_numeros_linea_cuando_altshift3_nofunciona/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Añadir resaltado de sintaxis al editor de textos NANO](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [[HowTo] nano: parámetros de arranque útiles](https://hijosdeinit.gitlab.io/howto_parametros_utiles_arranque_nano/)
- [En nano no existe overtyping](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/)
- [Expresiones regulares no aceptadas por el editor de texto 'nano'](https://hijosdeinit.gitlab.io/regexp_no_aceptadas_por_nano/)
- [[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados](https://hijosdeinit.gitlab.io/howto_micro_editor_texto_debian/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [[HowTo] Visualización de 'markdown' en la línea de comandos (CLI): 'MDLESS'](https://hijosdeinit.gitlab.io/howto_mdless_visor_markdown_cli/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://nano-editor.org/">nano-editor.org</a>  
<a href="https://www.nanotutoriales.com/tutorial-del-editor-de-texto-nano">NanoTutoriales</a>  
<a href="https://iesmunoztorrero.educarex.es/web/lenix/nano/nano.htm">iesmunoztorrero</a>  
<a href="https://www.atareao.es/software/programacion/nano-un-editor-de-texto-para-la-terminal/">atareao.es</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

