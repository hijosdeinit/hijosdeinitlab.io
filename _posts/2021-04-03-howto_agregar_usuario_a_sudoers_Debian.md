---
layout: post
title: "[HowTo] 'sudo' en Debian y derivados: agregar usuario a 'sudoers'"
date: 2021-04-03
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "debian 10", "buster", "su", "sudo", "sudoers", "usermod", "groups", "id", "usermod", "root", "visudo"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Debian 10.9.0 "de serie" ya trae consigo el comando 'sudo' -cosa curiosa porque pareciérame que la anterior "entrega" de Debian 10 no lo incorporaba por defecto y había que instalárselo mediante `su` y, a continuación, `apt install sudo`- mas, sin embargo, tal como termina la instalación del sistema, nuestro usuario -el que creamos durante la instalación- no está autorizado para ejecutar 'sudo'.  
Habrá que darle ese privilegio a mano, y de eso tratará esta entrada del blog.  

<br>
Cabe señalar que Debian 10, recién instalado, permite a nuestro usuario ejecutar comandos como superusuario de una forma: mediante el comando 'su'. Ejecutando 'su' el sistema pide la contraseña, y si ésta es correcta cambiará el *prompt* y permitirá lanzar ALGUNAS órdenes como superusuarios, es decir, como root.  

<br>
## 1. Hágase copia de seguridad del archivo '/etc/sudoers'
~~~bash
su
cp /etc/sudoers /etc/sudoers.bak001
~~~

<br>
## 2. Edítese el archivo `/etc/sudoers`
Generalmente esto se hacía ejecutando el comando 'sudo visudo', que abría automáticamente el editor de texto 'nano' con permisos para editar '/etc/sudoers'.  
Mas, evidentemente, como nuestro usuario aún no pertenece al grupo 'sudo' no puede ejecutar 'sudo', valga la redundancia.  
TAMPOCO sirve en este particular caso ejecutar `su` y a continuación `visudo`.  
Por consiguiente, habrá que arriesgarse a emplear el editor de texto 'nano' ejecutado como superusuario de la única manera que, de serie, permite Debian 10.
~~~bash
su -
nano /etc/sudoers
~~~
y añádase, bajo el "apartado" `User privilege specification`:
~~~
miusuario ALL=(ALL:ALL) ALL
~~~
Reiniciese el sistema con el comando `sudo reboot`.  

<big>Con esto el usuario 'miusuario' ya tiene privilegios para ejecutar 'sudo'.</big>  

<br>
## 3. (extra) Añadir nuestro usuario al grupo 'sudo' mediante el comando 'usermod'
Para añadir al usuario 'miusuario' al grupo 'sudo', bastaría con ejecutar:
~~~bash
sudo usermod -aG sudo miusuario
~~~

<br>
Para comprobar que el usuario 'miusuario' forma parte del grupo 'sudo' ejecuto el comando 'id':
~~~bash
id miusuario
~~~
> <small>uid=1000(miusuario) gid=1000(miusuario) grupos=1000(miusuario),24(cdrom),25(floppy),27(sudo),29(audio),30(dip),44(video),46(plugdev),109(netdev),112(bluetooth),116(lpadmin),117(scanner)</small>  

<br>
Me he percatado de que ejecutar el comando `groups' a secas en Debian 10 no mostraría que 'miusuario' pertenezca al grupo 'sudo'.  
Para que el comando 'groups' muestre correctamente todos los grupos a los que pertenece el usuario 'miusuario' hay que ejecutarlo así:
~~~bash
groups miusuario
~~~
> <small>miusuario : miusuario cdrom floppy sudo audio dip video plugdev netdev bluetooth lpadmin scanner</small>  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://linoxide.com/add-user-sudoers-ubuntu/">Linoxide</a>  
<a href="https://www.osradar.com/how-to-enable-sudo-on-debian-10/">OsRadar</a>  
<a href="https://jaweewo.es/como-crear-un-usuario-sudo-en-debian/">Jaweewo.es</a>  
<a href="https://wiki.debian.org/es/sudo">wiki.debian.org</a>  
<a href="https://linuxize.com/post/usermod-command-in-linux/">Linuxize</a>  
<a href="https://www.zeppelinux.es/modificar-usuarios-en-linux-con-el-comando-usermod/">ZeppeLinux</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
