---
layout: post
title: "[HowTo] Corregir error en Jekyll (Gitlab): mis incompatibilidades con la útima versión de ruby disponible"
date: 2024-01-28
author: Termita
category: "programación"
tags: ["ci", "error", "gema", "gem", "csv", "base64", "bigdecimal", "blog", "web 4.0", "Jekyll", "Ruby", "GitLab", "GitHub", "programación", "markdown", "texto plano", "html", "estático"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
LLevaba tiempo sin actualizar este blog que, como muchos sabrán está alojado en *Gitlab*. Mi útlimo *push* fue hace casi 2 meses, aún en 2023.  
GitLab a veces realiza modificaciones en su sistema, aquel del que me sirvo para publicar este blog. Eso conlleva que deba adecuarlo a los nuevos requisitos. Y eso es lo que ha ocurrido.  

<br>
Hoy, al actualizar el blog, he obtenido un ERROR:
>...  
>/usr/local/bundle/gems/jekyll-4.0.1/lib/jekyll.rb:28: warning: csv was loaded from the standard library, but will no longer be part of the default gems since Ruby 3.4.0. Add csv to your Gemfile or gemspec. Also contact author of jekyll-4.0.1 to add csv into its gemspec.  
>/usr/local/bundle/gems/safe_yaml-1.0.5/lib/safe_yaml/transform.rb:1: warning: base64 was loaded from the standard library, but will no longer be part of the default gems since Ruby 3.4.0. Add base64 to your Gemfile or gemspec. Also contact author of safe_yaml-1.0.5 to add base64 into its gemspec.  
>/usr/local/bundle/gems/liquid-4.0.4/lib/liquid/standardfilters.rb:2: warning: bigdecimal was loaded from the standard library, but will no longer be part of the default gems since Ruby 3.4.0. Add bigdecimal to your Gemfile or gemspec. Also contact author of liquid-4.0.4 to add bigdecimal into its gemspec.  
>...  
>ERROR: Job failed: exit code 1  

<br>
En mi archivo **'.gitlab-ci.yml'** tengo configurado <span style="background-color:#042206"><span style="color:lime">'image: ruby:latest'</span></span>, es decir, que emplee la última versión de 'ruby' disponible en el sistema de Gitlab.  
Y a eso se debe este error: la última versión de 'ruby' que pone a disposición GitLab es incompatible con ciertas 'gemas' y elementos de mi blog, que sí funcionan correctamente en versiones anteriores como, por ejemplo, la 3.2.  

<br>
<big>**SOLUCIÓN**:</big>  
Establecer que se emplee, no la útima versión disponible de 'ruby', sino específicamente la versión **'3.2'**. Para ello edito mi archivo **'.gitlab-ci.yml'** sustituyendo <span style="background-color:#042206"><span style="color:lime">'image: ruby:latest'</span></span> por <span style="background-color:#042206"><span style="color:lime">'image: ruby:3.2'</span></span>. Las primeras líneas de este archivo, por consiguiente, quedan así:
~~~
image: ruby:3.2
#image: ruby:latest
~~~
Tras guardar los cambios en el archivo '.gitlab-ci.yml' empujamos todo mediante git y la "compilación" ya no dará error.  

<br>
<br>
Entradas relacionadas:  
- [Corregir error en Jekyl (Gitlab): versión listen vs versión ruby](https://hijosdeinit.gitlab.io/howto_corregir_error_gitlab_jekyll_listen_vs_ruby/)
- [[HowTo] Generación automática de sitemap.xml para blog Jekyll en GitLab](https://hijosdeinit.gitlab.io/howto_generar-autom%C3%A1ticamente-sitemap.xml_jekyll_gitlab/)
- [[HowTo] Creación de un blog Jekyll en GitLab](https://hijosdeinit.gitlab.io/creaci%C3%B3n-blog-jekyll-en-gitlab/)  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://github.com/ruby/ruby/blob/v3_3_0/NEWS.md#stdlib-updates" target="_blank">GitHub - ruby - stdlib-updates</a>
<a href="zzzzzz" target="_blank">zzz</a>
<a href="zzzzzz" target="_blank">zzz</a>
<a href="zzzzzz" target="_blank">zzz</a>  
<br>
<br>
<br>
<br>
