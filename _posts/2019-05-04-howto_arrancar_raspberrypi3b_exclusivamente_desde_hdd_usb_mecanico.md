---
layout: post
title: '[HowTo] Arrancar RaspBerry Pi 3b desde hdd usb mecánico'
date: 2019-05-04
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "RaspBerry Pi", "RaspBian", "blkid", "boot", "rootfs", "disco duro", "hdd", "hard disk", "linux", "microSD", "usb"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
A falta de hardware específico -una controladora SATA- en RaspBerry Pi no queda más remedio que recurrir a los puertos USB para conectarle discos duros.  
Generalmente RaspBerry Pi, tal como viene de serie, sólo puede arrancar y cargar sistema operativo desde el zócalo SD. Pero [modificando sencillamente el software de la máquina RaspBerry Pi se consigue poder arrancar desde dispositivos USB](https://hijosdeinit.gitlab.io/howto_anadir_arranque_usb_raspberry_pi/).  

(*) Los discos duros mecánicos por usb tienen una latencia considerable, tardan unos instantes en ser detectados por la máquina anfitrión. Desconozco cómo se comportan los discos de estado sólido -SSD-, no los he probado en Rpi aún.  

Cuando encendemos RaspBerryPi ésta espera tan sólo unos segundos, transcurridos los cuales, si no detecta el disco duro no arrancará desde él. Por eso la latencia de un hdd mecánico conectado por usb puede hacer que la máquina -RaspBerry Pi- no lo detecte y, por ejemplo, no arranque el sistema operativo.  

Para esquivar este problema se puede recurrir a un <big>**arranque mixto**</big>: SD (o pendrive usb) + hddusb.  
Se trata, por consiguiente, de arrancar a través de microsd o pendrive usb (tienen menor latencia), que contendrá la pequeña partición "BOOT" en la que el fichero "cmdline.txt" debe ser modificado para que haga referencia al PARTUUID de la partición "ROOTFS" del hddusb (que es la que contiene el sistema en sí, con sus carpetas /home /etc y demás), que deberá estar también conectado y, a ser posible, alimentado de manera externa. (Dicen que hay una manera de subirle el amperaje a los puertos usb de Raspberry, mas yo no he constatado tal cosa).  

A grandes rasgos el procedimiento es este:  

1. Arrancamos nuestro RaspBian (supongo que con otros sistemas de RaspberryPi será lo mismo) desde microsd o pendrive usb (yo arranco desde pendrive pues ya no me queda ninguna microsd viva).  

2. Conectamos el disco duro usb (hddusb) formateado en ext4  

3. `sudo blkid`  
Anotamos el partuuid del hddusb, concretamente de la partición donde irá el sistema. Por ejemplo si es la partición nº 1, el partuiid será una serie de números, con un guión y "01".  

4. editamos el fichero '/cmdline.txt' que hay en la partición BOOT de la sd o pendrive
~~~
sudo nano cmdline.txt
~~~
(*) La línea debe quedar más o menos así (el partuuid, que se obtiene del comando 'sudo blkid', me lo acabo de inventar para el ejemplo). Todos esos parámetros [tienen explicación](https://hijosdeinit.gitlab.io/howto_cmdline.txt_KernelCommandLine_RaspBian_rpi/). No me extenderé, mas es fácil averiguar qué significan:  
><small>`dwc_otg.lpm_enable=0 console=serial0,115200 console=tty1 root=PARTUUID=1234567e4-01 rootfstype=ext4 elevator=deadline fsck.repair=yes rootwait rootdelay=5 quiet splash plymouth.ignore-serial-consoles`</small>  

5. `sudo fdisk -l`  
Anotamos '/dev/sdXX' de la partición de hddusb donde meteremos el sistema.  

6. `sudo mount /dev/sdXX /mnt`  
Montamos en /mnt la partición del hddusb donde queremos meter el sistema operativo.  

7. `sudo rsync -axv / /mnt`  
Transferimos el sistema a la partición del hddusb que hemos montado, con todos los permisos y características de los archivos. Este comando probablemente podría mejorarse con los parámetros `-avxlHpEXothi --progress --specials`.  

8. `sudo nano /mnt/etc/fstab`  
Editamos el fichero '/etc/fstab' del hddusb (del sistema que acabamos de transferir) de tal forma que quede así, cambiando los parámetros que puse como ejemplo:  

><small>
    proc            /proc           proc    defaults          0       0  
    PARTUUID=1234567e4-01  /               ext4    defaults,noatime  0       1  
    PARTUUID=1b67890a1-01  /boot           vfat    defaults          0       2  
    # HAY QUE COMENTAR LA LÍNEA DE LA PARTICIÓN ROOTFS DE LA SD O PENDRIVE, YA NO LA NECESITAREMOS:  
    # PARTUUID=1b67890a1-02  /               ext4    defaults,noatime  0       1  
    # 887516e7-01 es el valor partuuid del discoduroextraible ide 60gb  
    # a swapfile is not a swap partition, no line here  
    #   use  dphys-swapfile swap[on|off]  for that  
</small>  

<br>
Y ya está.  

No había elaborado aún la bitácora de este procedimiento, mas al oir a atareao enfrascado en las mismas cuestiones, me he sentido empujado a poner algo de mi parte pues es mucho lo que aprendo y me motiva su podcast.  
Debo señalar que este procedimiento lo he aplicado basándome, como siempre, en otros que me antecedieron.  

<br>
<br>
<br>
<br>
<br>
---  
<small>Fuentes:  
[maketecheasier](https://www.maketecheasier.com/boot-up-raspberry-pi-3-external-hard-disk/)</small>  
<br>
<br>
<br>
<br>
