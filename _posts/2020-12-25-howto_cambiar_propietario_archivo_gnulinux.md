---
layout: post
title: "[HowTo] Cambiar el propietario de un archivo/directorio en GNU Linux"
date: 2020-12-25
author: Termita
category: "sistemas operativos"
tags: ["propietario", "usuario", "grupo", "permisos", "chown", "gnu linux", "sistemas operativos", "sudo"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Para cambiar el propietario de un archivo o un directorio en GNU Linux el procedimiento desde línea de comandos es:  
`sudo chown -R` *usuario*:*grupo* *archivoôdirectorio*  
<br>
Por ejemplo:
~~~bash
sudo chown -R www-data:www-data /var/www/kanboard
~~~
<br>
Donde:  
`-R` = recursivo  
`www-data:www-data` = usuario 'www-data', grupo 'www-data'  
`/var/www/kanboard` = directorio cuyo propietario deseamos cambiar  
<br>
<br>
<br>
<br>
<br>
