---
layout: post
title: "[HowTo] Proxies de acceso a 'ThePirateBay'"
date: 2019-12-09
author: Termita
category: "descargas"
tags: ["descargas", "torrent", "p2p", "tor", "knaben", "proxybay", "proxy", "proxies", "thepiratebay", "internet 4.0", "isp", "busqueda", "buscador", "web browser", "navegador web", "censura", "filtro", "bloqueo"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Hay una guerra contra [ThePirateBay](https://es.wikipedia.org/wiki/The_Pirate_Bay) hasta el punto en que los proveedores de acceso a internet y, sospecho, los propios navegadores web (<small>bajo la escusa de una falsa "amenaza" de seguridad</small>) bloquean la página oficial.  
Existen varias formas de saltarse esa "prohibición", unas más complejas que otras, como por ejemplo acceder mediante TOR.  
Mas es más sencillo hacerlo a través de alguno de los muchos proxies que a lo largo del globo dan acceso al buscador de torrents más famoso y polémico de la red.  

<br>
Listado de proxies para acceder a 'ThePirateBay':  
~~[proxybay.dev](https://proxybay.dev)~~  
[proxy-bay.me](https://proxy-bay.me/)  

<br>
<a href="/assets/img/2019-11-09-howto_proxies_acceso_a_ThePirateBay/thepiratebay_listadeproxies.png" target="_blank"><img src="/assets/img/2019-11-09-howto_proxies_acceso_a_ThePirateBay/thepiratebay_listadeproxies.png" alt="listado de proxies de acceso a 'ThePirateBay'" width="400"/></a>  
<small>listado de proxies de acceso a 'ThePirateBay'</small>  
<br>

<br>
Por ejemplo, hoy he usado este:  
~~[https://www3.knaben.ru/ThePirateBay.php](https://www3.knaben.ru/ThePirateBay.php)~~  
<big>[knaben.ru](https://knaben.ru/thepiratebay.php)</big>  

<br>
<a href="/assets/img/2019-11-09-howto_proxies_acceso_a_ThePirateBay/knaben_proxy_thepiratebay.png" target="_blank"><img src="/assets/img/2019-11-09-howto_proxies_acceso_a_ThePirateBay/knaben_proxy_thepiratebay.png" alt="'Knaben'" width="300"/></a>  
<small>'Knaben'</small>  
<br>

<br>
~~También existe, por supuesto, [KickAssTorrents](https://katcr.co/new/full/). <small>Habrá que ver cuánto dura.</small>~~  
A parte de 'ThePirateBay', también existen, por supuesto, otros buscadores como [KickAssTorrents](https://kickasstorrents.to/) y [LimeTorrents](https://limetorrents.cyou) <small>Habrá que ver cuánto duran.</small>  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
