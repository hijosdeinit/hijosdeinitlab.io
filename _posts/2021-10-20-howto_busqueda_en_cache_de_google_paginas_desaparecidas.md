---
layout: post
title: "[HowTo] búsqueda de páginas web 'desaparecidas', directamente en la caché de Google"
date: 2021-10-20
author: Termita
category: "internet 4.0"
tags: ["internet 4.0", "productividad", "web", "cache", "buscador", "google", "archive.org", "arqueologia", "obsolescencia"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Tenemos cierta tendencia a dar por hecho que los contenidos en internet son eternos, que no "desaparecen" nunca. Y sí, todos sabemos que desaparecen, que las páginas se caen, muchas veces indefinidamente, que las url cambian, que los dominios caducan, que los hostings y las empresas quiebran, se fusionan, cambian de manos, etcétera, etcétera. La información probablemente es eterna -porque quizás la almacenan sistemática y masivamente- para los que hoy se erigen como "amos de la red": las *Big Tech* (Alphabet-Google, Amazon, etc...), mas no para el común de los mortales. ¿Cuántas veces ese atesorado enlace que uno encontró tiempo ha tras horas de procrastinación resulta estar "caído" a la hora de la verdad, cuando uno se dispone a retomarlo en plenitud? ¿Cuántas veces en los propios resultados de los buscadores hay enlaces "a ninguna parte", a páginas que se extinguieron como lágrimas en la lluvia?</small>  

<br>
'WayBack Machine' de [archive.org](https://archive.org/) suele aportar una esperanza de encontrar esos contenidos desaparecidos, dado que almacena copias de más de 600 billones (<small>desconozco si europeos o norteamericanos</small>) de páginas web.  

<br>
No obstante otro método adicional -y ese es el motivo de esta entrada de este blog- es utilizar directamente la ***caché*** del buscador hegemónico: 'Google'.  
Y esto se hace dirigiéndose con el navegador web a la siguiente url:  
<span style="background-color:#042206"><span style="color:lime"><big>`http://webcache.googleusercontent.com/search?q=cache:`*urldelapaginadesaparecida*</big></span></span>  

<br>
Por ejemplo, para visitar esta url, ya desaparecida: 'https://forum.obsidian.md/t/deb-package-for-v0-12-15-has-wrong-symlink-for-the-executable-in-usr-bin/24227' bastaría con dirigir el navegador a: <span style="background-color:#042206"><span style="color:lime">`http://webcache.googleusercontent.com/search?q=cache:https://forum.obsidian.md/t/deb-package-for-v0-12-15-has-wrong-symlink-for-the-executable-in-usr-bin/24227`</span></span>  

<br>
<br>
Entradas relacionadas:  
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
