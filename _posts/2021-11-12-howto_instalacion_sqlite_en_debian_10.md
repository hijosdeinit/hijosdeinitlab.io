---
layout: post
title: "[HowTo] Instalación de 'sqlite' en Debian 10 y derivados"
date: 2021-11-12
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "sqlite", "bbdd", "base de datos", "database", "dbase", "mysql", "mariadb", "mongodb", "sql", "gnu linux", "linux", "debian", "ubuntu"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno: cpu Core2Duo, 6gb RAM, gpu gforce 7600gt, GNU Linux Debian 10 Buster (netinstall) stable, kernel 4.19.0-18-amd64, gnome</small>  

<br>
<br>
<small>«SQLite es un sistema de gestión de bases de datos relacional compatible con ACID, contenida en una relativamente pequeña (~275 kiB) biblioteca escrita en C. SQLite es un proyecto de dominio público creado por D. Richard Hipp.  
A diferencia de los sistema de gestión de bases de datos cliente-servidor, el motor de SQLite no es un proceso independiente con el que el programa principal se comunica. En lugar de eso, la biblioteca SQLite se enlaza con el programa pasando a ser parte integral del mismo. El programa utiliza la funcionalidad de SQLite a través de llamadas simples a subrutinas y funciones. Esto reduce la latencia en el acceso a la base de datos, debido a que las llamadas a funciones son más eficientes que la comunicación entre procesos. El conjunto de la base de datos (definiciones, tablas, índices, y los propios datos), son guardados como un solo fichero estándar en la máquina host. Este diseño simple se logra bloqueando todo el fichero de base de datos al principio de cada transacción.  
En su versión 3, SQLite permite bases de datos de hasta 2 Terabytes de tamaño, y también permite la inclusión de campos tipo BLOB.  
El autor de SQLite ofrece formación, contratos de soporte técnico y características adicionales como compresión y cifrado.»</small>  

<br>
<br>
## Instalación desde los repositorios oficiales de Debian
~~~bash
sudo apt-get update
sudo apt install sqlite
~~~

Ejecutando <span style="background-color:#042206"><span style="color:lime">`sqlite3 --version`</span></span> compruébese la versión instalada:
> 3.27.2 2019-02-25 16:06:06 bd49a8271d650fa89e446b42e513b595a717b9212c91dd384aab871fc1d0alt1  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Instalación de 'SqliteStudio' en Debian 10 y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_sqlite_studio_en_debian_10/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://es.wikipedia.org/wiki/SQLite">Wikipedia - sqlite</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
