---
layout: post
title: "[HowTo] Mostrar el número de línea en el editor 'nano' cuando Alt+Shift+3 no funciona"
date: 2019-07-21
author: Termita
category: "productividad"
tags: ["sistemas operativos", "gnu linux", "pico", "nano", "editor de texto", "vim", "vi", "emacs", "texto", "programación", "texto plano", "latex", "herramientas", "código", "markdown", "org mode", "pandoc", "atajo", "atajo de teclado", "error", "solucion"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
La combinación de teclas **Alt + Shift + 3** : Muestra los números de línea a la izquierda del interfaz, mas -pareciera- no funciona en Ubuntu 18.04, por ejemplo.  
Solución:  

<br>
Cuando queremos que el editor de texto 'nano' muestre el número de línea a la izquierda, **si el atajo de teclado Alt+Shift+3 no funciona**, y estamos empecinados en usar ese atajo y no otro -**ESC #**- no queda más remedio que editar como superusuarios el archivo de configuración de 'nano'.  
Dicho archivo es '/etc/nanorc'.  
~~~bash
sudo cp /etc/nanorc /etc/nanorc.bkp
sudo nano /etc/nanorc
~~~
...y descomentamos la línea que dice <span style="background-color:#042206"><span style="color:lime">`# set linenumbers`</span></span>, es decir, la dejamos así:  
<span style="background-color:#042206"><span style="color:lime">**`set linenumbers`**</span></span>  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Atajos de teclado del editor 'nano'](https://hijosdeinit.gitlab.io/howto_atajos_teclado_nano/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Añadir resaltado de sintaxis al editor de textos NANO](https://hijosdeinit.gitlab.io/howto_A%C3%B1adir-resaltado-de-sintaxis-al-editor-de-textos-NANO/)
- [[HowTo] nano: parámetros de arranque útiles](https://hijosdeinit.gitlab.io/howto_parametros_utiles_arranque_nano/)
- [En nano no existe overtyping](https://hijosdeinit.gitlab.io/no_overtyping_en_nano/)
- [Expresiones regulares no aceptadas por el editor de texto 'nano'](https://hijosdeinit.gitlab.io/regexp_no_aceptadas_por_nano/)
- [[HowTo] 'micro', editor de texto CLI alternativo a 'nano', en Debian y derivados](https://hijosdeinit.gitlab.io/howto_micro_editor_texto_debian/)
- [[HowTo] Apps de NextCloud20 'Text', 'Plain Text Editor' y 'MarkDown Editor'. Funcionamiento independiente vs. funcionamiento en conjunto (suite)](https://hijosdeinit.gitlab.io/NextCloud20_apps_Text_PlainTextEditor_MarkDownEditor_ensolitario_o_ensuite/)
- [[HowTo] Edición MarkDown a 2 columnas (código y previsualización) en NextCloud 20 con 'MarkDown Editor' y 'Plain Text Editor'](https://hijosdeinit.gitlab.io/howto_nextcloud20_Markdown_Editor_a_2_columnas_codigoyprevisualizacion/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)
- [[HowTo] Visualización de 'markdown' en la línea de comandos (CLI): 'MDLESS'](https://hijosdeinit.gitlab.io/howto_mdless_visor_markdown_cli/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="">zzzzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
