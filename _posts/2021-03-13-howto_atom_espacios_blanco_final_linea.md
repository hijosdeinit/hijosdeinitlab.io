---
layout: post
title: "[HowTo] Evitar que 'atom' elimine espacios en blanco al final de línea"
date: 2021-03-13
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "atom", "editor de texto", "ide", "programacion", "bash", "script", "bash scripting", "markdown", "texto", "texto plano", "código", "editor", "gnu linux", "debian", "vi", "vim", "nano", "emacs", "atom", "eclipse", "sublime text", "visual studio code", "vscodium", "codium", "vscode", "netbeans", "espacios en blanco"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Los espacios en blanco al final de una línea pueden ser algo necesario -como en el caso de MarkDown (señalan un salto de línea)- ô, por el contrario, algo molesto y generador incesante de errores.  
En el caso de textos escritos en lenguaje MarkDown interesa que los dos espacios al final de algunas líneas se mantengan, que no se borren automáticamente al guardar el documento.  
Tras instalar 'Atom', que por defecto viene con el paquete *'markdown-preview'* instalado y activado, y añadirle algunos paquetes *de la comunidad* como *'markdown-writer'* y *'language-markdown'* he constatado que, al guardar un documento, los 2 espacios en blanco al final de algunas líneas son extirpados automáticamente. Desconozco si los dos plugins que menciono -que deshabilitaron sin aviso ni permiso el paquete ***'language-gfm'***- tienen algo que ver con el hecho de que el paquete *'whitespace'* -pese a estar configurado para que respete los espacios al final de línea en documentos markdown- extirpe esos espacios.  

<br>
## Configurar Atom para que no elimine automáticamente los espacios en blanco al final de las líneas

### Método 1
Edit → Preferences → Paquetes → 'whitespace' → Remove Trailling Whitespace: **deshabilitado**  
El inconveniente de esto es que mantendrá los espacios al final de línea de cualquier código, esté o no en MarkDown. No es, por consiguiente, una medida muy acertada. Pero sirve para salir del paso.  

<br>
<a href="/assets/img/2021-03-13-howto_atom_espacios_blanco_final_linea/whitespace_atom.png" target="_blank"><img src="/assets/img/2021-03-13-howto_atom_espacios_blanco_final_linea/whitespace_atom.png" alt="Configuración del paquete *'whitespace'* para que nunca elimine automáticamente los espacios al final de línea." width="800"/></a>  
<small>Configuración del paquete *'whitespace'* para que nunca elimine automáticamente los espacios al final de línea.</small>  
<br>

<br>
### Método 2
Es el método más adecuado, **aunque a mí no me ha funcionado** exactamente. Probablemente se deba a que, por algún motivo, 'atom' no se da cuenta correctamente de cuándo está frente a un documento *markdown*.  

<br>
<a href="/assets/img/2021-03-13-howto_atom_espacios_blanco_final_linea/language-gfm.png" target="_blank"><img src="/assets/img/2021-03-13-howto_atom_espacios_blanco_final_linea/language-gfm.png" alt="Hay que activar nuevamente el paquete *'language-gfm'*" width="800"/></a>  
<small>Hay que activar nuevamente el paquete *'language-gfm'*</small>  
<br>

- Volver a habilitar *'language-gfm'*: Edit → Preferences → Paquetes → 'language-gfm': habilitado
- Configurar 'whitespace' para que sí borre los espacios al final de línea: Edit → Preferences → Paquetes → 'whitespace' → Remove Trailling Whitespace: habilitado
- Configurar 'whitespace' para que mantenga los espacios al final de línea **en los documentos markdown**: Edit → Preferences → Paquetes → 'whitespace' → Keep Markdown Line Break Whitespace: habilitado.  

<br>
<br>
<br>
Entradas relacionadas
- [[HowTo] El editor (IDE) atom y su instalacion en Debian y derivados](https://hijosdeinit.gitlab.io/howto_editor_ide_atom_instalacion_en_debian_y_derivados/)
- [[HowTo] Agregar idiomas al editor (IDE) atom. Ponerlo en español](https://hijosdeinit.gitlab.io/howto_poner_atom_en_espa%C3%B1ol/)
- [Listado de editores de código / IDE interesantes](https://hijosdeinit.gitlab.io/listado_IDE_o_editores_de_codigo/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://discuss.atom.io/t/whitespace-package-removing-newline-spaces-in-markdown/25702">discuss.atom.io</a>  
<a href="https://ourcodeworld.co/articulos/leer/514/como-eliminar-los-espacios-en-blanco-al-guardar-en-atom-editor">OurCodeWorld</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
