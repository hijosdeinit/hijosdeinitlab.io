---
layout: post
title: "[HowTo] Obtención de 'ID' (embed) de un video de 'Rumble.com' y descargarlo mediante 'youtube-dl'"
date: 2021-06-15
author: Termita
category: "descargas"
tags: ["descargas", "linux", "gnu linux", "youtube-dl", "youtube", "rumble.com", "hack", "crack", "ofuscacion", "codigo","offline", "privacidad", "anonimato", "tracking", "torrent", "p2p", "subtítulos", "web 4.0", "descargar", "audio", "video", "multimedia"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
EN CONSTRUCCION  

'youtube-dl' no descarga los videos de 'Rumble.com' de manera convencional: 'youtube-dl' no es capaz de extraer el video de la url convencional de rumble.com.  
> <small>'Rumble.com' es una plataforma de video online que compite, junto a otras, con 'Youtube', esa plataforma hegemónica propiedad de 'Google'. 'Rumble.com' ([y otras similares](https://hijosdeinit.gitlab.io/alternativas_a_youtube_menos_toxicas/) como 'Odysee' / 'lbry', 'Bitchute', 'Archive.org' o 'PeerTube') hacen gala de NO practicar esa censura sistemática que -pareciera que para salvaguardar los intereses de la ideología *mainstream* o, quizás, de la [*nomenklatura*](https://es.wikipedia.org/wiki/Nomenklatura) mundial- indiscriminadamente lleva a cabo 'Youtube'.</small>  

<br>
<big>**'youtube-dl' necesita conocer el 'ID' de un video alojado en 'Rumble.com' para poder descargarlo**</big>. Ese 'ID' no está oculto, 'Rumble.com' no lo esconde, mas para conocerlo habría que consultar desde la página web del video el enlace (url) de incrustación (*embed*) de dicho video, cosa sencilla mas harto tediosa. PERO hay soluciones para todo, en este caso para NO haber de visitar ninguna página web y hacerlo todo de manera cuasi automatizada desde la propia terminal, que es el interfaz desde el que funciona 'youtube-dl'.  

<br>
¿Cómo obtener el 'ID' de los videos de 'Rumble.com' y descargarlos con 'youtube-dl' desde línea de comandos?  

<br>
<br>
## 1 . Creando una función para automatizar el proceso
**ℹ** </small>Esa función es temporal. Se perderá al reiniciar el sistema.</small>
~~~bash
rumble(){ youtube-dl --no-check-certificate -f mp4-480p/webm-480p/mp4-360p/mp4-720p/mp4-1080p $(curl -s "$1" | tr -d '\n'|awk -F "embedUrl" '{print $2}'|awk -F '"' '{print $3}'); }
~~~
Entonces bastará con ejecutar <span style="background-color:#042206"><span style="color:lime">`rumble` *urlconvencionaldelvideoderumblecom*</span></span> para que automáticamente se obtenga el 'ID' del video y se descargue.  
Anotación: pareciera que, de las diversas calidades disponibles, se elige automáticametne el formato '.mp4'.

<br>
<br>
## 2. Comandos "a mano"
Desmenuzando <span style="background-color:#042206"><span style="color:lime">`rumble(){ youtube-dl --no-check-certificate -f mp4-480p/webm-480p/mp4-360p/mp4-720p/mp4-1080p $(curl -s "$1" | tr -d '\n'|awk -F "embedUrl" '{print $2}'|awk -F '"' '{print $3}'); }`</span></span>:

<br>
<br>
## 3 . Anexo: Comandos "a mano" que no me han funcionado






~~~bash
youtube-dl -F https://rumble.com/embed/vfxez5/?pub=4
~~~
~~~
[RumbleEmbed] vfxez5: Downloading JSON metadata
[info] Available formats for vfxez5:
format code  extension  resolution note
mp4-240p     mp4        240p        203k 
mp4-360p     mp4        360p        634k 
mp4-480p     mp4        480p        815k 
webm-480p    webm       480p        819k  (best)
~~~

<br>
<br>

Entradas relacionadas:  
- [Alternativas a 'Youtube' menos tóxicas](https://hijosdeinit.gitlab.io/alternativas_a_youtube_menos_toxicas/)  

<br>
<br>
<br>

--- --- ---  
<small>Fuentes:  
<a href="https://www.reddit.com/r/youtubedl/comments/k60crm/anyone_know_how_to_get_rumblecom_videos_supported/gkuk53z/">m10950 en ReddIt</a>  
<a href="https://github.com/ytdl-org/youtube-dl/issues/10785">github - youtube-dl - *issues*</a>  
<a href=""></a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
