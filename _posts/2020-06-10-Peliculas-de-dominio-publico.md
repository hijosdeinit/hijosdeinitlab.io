---
layout: post
title: 'Películas de dominio público'
date: 2020-06-10
author: Termita
category: "multimedia"
tags: ["multimedia", "películas", "dominio público", "descargas", "cine"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Hay películas que hoy están ya libres de derechos de autor, royalties y demás.  
Se pueden encontrar en sitios como estos:
- <a href="Archive.org - Movies" target="_blank">https://archive.org/details/moviesandfilms</a>  
- <a href="Archive.org - Movies" target="_blank">https://archive.org/details/more_animation</a>  
- <a href="Public Domain Torrents" target="_blank">https://www.publicdomaintorrents.info/nshowcat.html?category=ALL</a>  
- <a href="CineLibreOnline" target="_blank">https://www.cinelibreonline.com/2017/01/peliculas-clasicas-de-united-artists-en.html</a>  
- <a href="RetroVision" target="_blank">https://retrovision.tv/</a>  
- <a href="" target="_blank">zzzzzzzz</a>  
- <a href="@@@@@@" target="_blank">zzzzzzzz</a>  
- <a href="@@@@@@" target="_blank">zzzzzzzz</a>  
- <a href="@@@@@@" target="_blank">zzzzzzzz</a>  
- <a href="@@@@@@" target="_blank">zzzzzzzz</a>  
- <a href="@@@@@@" target="_blank">zzzzzzzz</a>  
<br>
<br>
Algunas de las mejores películas de dominio público que se pueden descargar libremente, desde originales que han dado lugar a nuevas versiones posiblemente más conocidas a auténticos clásicos del cine:  
>Viaje a la luna (1902), de Melies
El vagabundo (1916), de Charles Chaplin
Nosferatu (1922), de F. W. Murnau
Metrópolis (1925), de Frizt Lang
El fantasma de la ópera (1925) de Lon Chaney
El acorazado Potemkin (1925), de Sergei M. Eisenstein
El maquinista de la general (1926) de Buster Keaton
Drácula (1931), de Tod Browning
La parada de los monstruos (Freaks, 1932), de Tod Browning
El mago de Oz (1939), de Victor Fleming
Juan Nadie (1941), de Frank Capra
El libro de la selva (1942), de Zoltan Korda
Rashomon (1950), de Akira Kurosawa
Plan 9 del espacio exterior (1959), de Edward Wood
La mansión de los horrores (House on Haunted Hill, 1959) de William Castle
La pequeña tienda de los horrores (1960), de Roger Corman
La última mujer sobre la tierra (1960), de Roger Corman
El carnaval de las almas (1962), de Herk Harvey
Charada (1963), de Alfred Hitchcock
La noche de los muertos vivientes (1968), de George A. Romero
Driller Killer (1979), de Abel Ferrara  

<br>
<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://help.nextcloud.com/t/solved-does-markdown-editor-no-longer-work/75909/2" target="_blank">https://www.cinemaficionados.com/search/label/Pel%C3%ADculas%20de%20dominio%20p%C3%BAblico</a>  
<br>
<br>
<br>
<br>
<br>
<br>
