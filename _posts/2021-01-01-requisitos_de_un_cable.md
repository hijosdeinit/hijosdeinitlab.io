---
layout: post
title: "Qué pedirle a un cable"
date: 2021-01-01
author: Termita
category: "hardware"
tags: ["hardware", "cable", "alambrico", "sata", "usb", "ide", "alimentacion", "electrico", "electricidad", "datos", "conector", "cola de cerdo", "conector angulado", "goma", "silicona"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Cables. Los hay de todas las medidas y grosores, y siguen siendo indispensables, aún en esta posmodernidad que ama "lo inalámbrico".  
En mi bregar con cables he llegado a determinar qué tienen los cables que me gustan, y -válgame Dios- que tampoco cuesta tanto complacerme. Hay una sentencia muy verdadera que señala que "cuesta lo mismo hacer las cosas mal que hacerlas bien".  

<br>
¿Qué pedirle a un cable?  
- Que no sea rígido y/o que su rigidez vaya acorde con el aparato al que va a ir conectado. <small>Hoy día, que los chismes son realmente pequeños y/o ligeros, suele darse la "paradoja" de que la propia rigidez del cable es capaz de MOVER -o incluso, por su propia rigidez y/o peso, tirar al suelo- el dispositivo al que se conecta. Los cables con funda de silicona o de goma no son rígidos, son perfectos y por todo el mundo conocida, ya no sólo su existencia, sino sus ventajas; No es caro hacer la funda de los cables con silicona o goma en lugar de plástico.
- Que no sea frágil. <small>Muchos cables no soportan bien la torsión, sobre todo cuando son rígidos. Un buen fabricante prueba su producto y lo refuerza allá donde ve que puede darse algún problema... o emplea materiales más apropiados (goma, silicona, etc...).</small>
- Que sea acorde con el chisme al que va a ir conectado. <small>Hay -por ejemplo- dispositivos para los que resulta más adecuado que el conector del cable sea angulado y otros a los que no, y así sucesivamente. Pareciera, por otra parte, que en el mundo posmoderno los fabricantes se han olvidado de -por poner un ejemplo- la "técnica de cola de cerdo", tan útil en ciertos dispositivos (guitarras eléctricas, máquinas de afeitar, etc...).</small>
- Que sea fiable. <small>Hay cables que -por su mala calidad, su mal diseño o su escaso control en la fabricación-, siendo incluso caros, son incompatibles o al cabo del tiempo dejan de funcionar correctamente. ¿Cómo puede ser eso posible?</small>
- Que sea seguro. <small>Si llevan grabado que cumplen con unas especificaciones de seguridad, en la práctica deberían -valga la redundancia- cumplir. Mal vamos cuando, por mucha "iso" y certificación, un cable se pela a causa de no soportar una mínima torsión. Y eso, lamentablemente, ocurre con demasiada frecuencia.</small>
- Que su precio no sea un insulto a la inteligencia y al sentido común. <small>Sólo es un cable, no se flipen.</small>  

<br>
<br>
Entradas relacionadas:  
- [Qué pedirle a un 'pendrive' USB](https://hijosdeinit.gitlab.io/requisitos_de_un_pendrive/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href=""></a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
