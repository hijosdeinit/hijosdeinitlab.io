---
layout: post
title: "[HowTo] respaldo mediante rsync a almacenamiento remoto por ssh"
date: 2023-02-27
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "respaldo", "backup", "rsync", "remoto", "ssh", "redes", "acceso remoto", "remote", "secure shell", "gnu linux", "debian", "debian 11", "bullseye", "cortafuegos", "firewall", "server", "ufw", "uncomplicated firewall", "22"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>Entorno: cpu Core2Duo, 6gb RAM, gpu nvidia GeForce GT 220, Live USB - GNU Linux Debian 11 Buster LXDE, kernel 5.10.0-9-amd64</small>  

<br>
Para que 'rsync' "grabe" los datos vía 'ssh' en un directorio remoto **es necesario agregarle el parámetro <span style="background-color:#042206"><span style="color:lime">`"ssh -p número_de_puerto"`</span></span>**. <small>Si no se especifica el número de puerto -`"ssh"` a secas- comunicará por el puerto 22, que es el puerto 'ssh' por defecto</small>.  
Para que 'rsync' pueda sincronizar remotamente vía 'ssh' es necesario que en la máquina remota que va a almacenar el respaldo esté instalado y ejecutándose el servidor 'ssh'](https://hijosdeinit.gitlab.io/howto_instalacion_servidor_ssh_debian_y_derivados/).  

<br>
<span style="background-color:#042206"><span style="color:lime">`rsync OPCIONES "ssh" RUTA_ARCHIVO_O_DIRECTORIO_ORIGEN USUARIO@IP_DEL_HOST:/RUTA_DIRECTORIO_ALMACENAMIENTO/`</span></span>  

<br>
Por ejemplo:
~~~bash
rsync -avxlHpEXothe "ssh" --progress  ~/Downloads/*.deb usuario@192.168.1.156:/home/usuario/Descargas/paquetesdeb/
~~~
Este comando de ejemplo sincronizará por ssh, mostrando el progreso y atendiendo a una serie de parámetros (<small>-avxlHpEXothe</small>) propios de 'rsync' que no vienen al caso, todos los archivos cuyo nombre termine en '.deb' sitos en el subdirectorio '~/Downloads' en el directorio '/home/USUARIO/Descargas/paquetesdeb/' del host remoto '192.168.0.17' al que se conectará utilizando la cuenta 'usuario'.  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Instalación de 'openssh-server' (servidor SSH) en Debian y derivados](https://hijosdeinit.gitlab.io/howto_instalacion_servidor_ssh_debian_y_derivados/)
- [[HowTo] solucion al error 'Permission denied (publickey)' en servidor SSH ('openssh-server') [Debian 11 y derivados]](https://hijosdeinit.gitlab.io/howto_error_ssh_Permission_denied_publickey_debian11bullseye/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://atareao.es/software-linux/sincronizacion-a-fondo-con-rsync/">atareao.es</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
