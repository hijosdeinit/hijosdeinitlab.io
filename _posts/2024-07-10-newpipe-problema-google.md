---
layout: post
title: 'NewPipe: contenidos de youtube con privacidad y con privilegios de cliente de pago (sin pagarles un céntimo)'
date: '2024-07-10T03:34:00.002+02:00'
author: Termita
comments: true
tags: ["youtube", "google", "fdroid", "youtube-dl", "web 4.0", "privacidad", "ad", "newpipe", "clipious", "freetube", "android", "publicidad", "tracking", "rastreo", "multimedia", "audio", "video", "degoogled", "frontend"]
category: Multimedia
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
tipue_search_active: true
exclude_from_search: false
modified_time: '2024-07-10T03:48:11.625+02:00'
thumbnail: https://1.bp.blogspot.com/-_FNJw4zD3j8/XqDxOiwlCaI/AAAAAAAAIes/vyMbuBPuOcwT7_DVfSOc3kKjhDuz4x0WwCLcBGAsYHQ/s72-c/newpipe-21304-1.jpg
blogger_id: tag:blogger.com,1999:blog-1446475121237311525.post-559693693876390617
blogger_orig_url: https://built4rocknroll.blogspot.com/2020/04/newpipe-contenidos-de-youtube-como-si.html
---
<div class="pull-left"><a href="https://hijosdeinit.gitlab.io/assets/img/2020-04-23-newpipe-contenidos-de-youtube-como-si/newpipe-21304-1.jpg"><img src="https://hijosdeinit.gitlab.io/assets/img/2020-04-23-newpipe-contenidos-de-youtube-como-si/newpipe-21304-1.jpg" style="margin-right: 1em;" alt="newpipe" width="400"/></a>
</div>
<div class="pull-right">Google quiere que el usuario le preste toda la atención.
</div><br/>
Youtube <b>para móviles</b>, desde su propia <i>app</i> o desde navegador móvil, DETIENE la reproducción de los vídeos si la ventana se minimiza o la pantalla se apaga, y tampoco permite descargar los videos.<br/><br/>
Esas "prestaciones" en los smartphones son sólo para los clientes premium, es decir, aquellos que pagan.<br/>
Además, dado que el negocio de Google es la publicidad, junto al consumo de contenidos vía su aplicación oficial o vía navegador para móviles viene de regalo el traqueo del visitante y los anuncios publicitarios.<br/><br/>
<a href="https://newpipe.schabi.org/" target="_blank">NewPipe</a>, una aplicación para Android de código abierto, permite esas cosas sin que uno tenga que pagarle un céntimo a Google. Con NewPipe se puede descargar el vídeo, extraer el audio, reproducir en segundo plano, apagar la pantalla, enviar a otro reproductor, etcétera... amén de saltarse el traqueo y la publicidad de Google.<br/>
<br/><br/>
NewPipe se instala a través del repositorio de <a href="https://f-droid.org/es/" target="_blank">F·Droid</a>.<br/>  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] 'invidious', 'Youtube' desde el navegador sin publicidad ni rastreo](https://hijosdeinit.gitlab.io/howto_invidious_youtube_desde_navegador_sin_publicidad/)
- [NewPipe: contenidos de youtube con privacidad y con privilegios de cliente de pago (sin pagarles un céntimo)](https://hijosdeinit.gitlab.io/newpipe-contenidos-de-youtube-como-si/)
- [[HowTo] Mejor forma de incorporar la última versión de 'youtube-dl' en Debian y derivados. II](https://hijosdeinit.gitlab.io/howto_youtube-dl_debian_netinstall/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="@@@@@@@@@@@@@@@@@@@@@@@@@@@">@@@@@@@@@@@@@@@@@@@@@@@@@@@</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
