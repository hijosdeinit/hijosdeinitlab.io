---
layout: post
title: "Ciberpunkfilia y atracción por la sociedad del sometimiento 4.0"
date: 2021-06-17
author: Termita
category: "sociedad 4.0"
tags: ["sociedad 4.0", "ciberpunk", "privacidad", "censura", "libertad", "grandes tecnologicas", "nuevo orden mundial", "ciberpunkfilos", "adiccion", "adictos", "tecnoadictos", "teléfonia movil", "movilidad", "maltrato"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Hay un dicho popular que señala que "todo en exceso es malo". Y es aplicable también a la relación del ser humano con la tecnología.  
A mi entender, la tecnología, que no es más que una herramienta, se convierte en calabozo cuando el ser humano es física y emocionalmente dependiente de ella. Esto no impide que el cautivo pueda ser consciente de ello -cuando está lúcido-, de esa cárcel de oro a la que tiene, por propio pie, querencia.  Mas -como el heroinómano que gusta del arranque de autocompasión cuando dice que se está matando y que este será el último pico- pareciera que obtiene, incluso así, una dosis de placer.  

<br>
Estoy viendo que hay personas adictas a lo que llaman "la vida en movilidad" y, a sabiendas, se dejan maltratar. Porque maltrato es también lo que experimenta aquel que expone su privacidad 'porque si no no puede "disfrutar" de esos servicios tecnológicos' a los que -siendo éstos esencialmente inútiles, pues antes, sin ellos, se vivía perfectamente- es adicto.  
En casos extremos -ciberpunkfilos- se les observa fantasear con una sociedad aún más sometida, sucia, decadente... y ellos se ven en ella como "rebeldes en movilidad". Hay que tener cuidado con lo que uno, a veces sin tan solo ser conscientes de ello, desea... Un inconsciente no es capaz de abstraer correctamente, anticiparse, y sólo cuando acontecen las cosas advierte su idiosincracia, muchas veces tremenda. Sospecho que esa realidad futurista, ese "mundo feliz" que evocan, acabaría disgustándoles bastante una vez hubieran de sufrirla, como peones que son, en toda su crudeza.  
Y esto me recuerda a los fanáticos consumidores compulsivos de series como *The Walking Dead* que fantasean y se imaginan en un mundo similar al que se describe en dicho producto. En sus ensoñaciones se ven como "los buenos", los hombres chulos que luchan contra los zombies... cuando por mera probabilidad lo más seguro es que, de advenir la absurdez de un mundo así, a ellos les tocaría el papel de esos feísimos cadáveres vivientes devoradores de carne humana.  
Pues lo mismo con los delirios ciberpunkfilos.  

<br>
Y yo a veces no puedo evitar un regusto amargo cuando les escucho fantasear en podcast y demás formas de comunicación que hoy tenemos. Porque detecto que, inexplicablemente, les atrae eso.  
Hay personas a las que su libertad individual pareciera que en esencia no les preocupa y que les basta, hoy y mañana, con una celda más cómoda. Y esto, paradójicamente, no les impide -en su inconsciencia lanar- "sentirse" integrantes de una especie de "facción rebelde"... una rimbombante "resistencia" basada en la verborrea. Porque "sentirse algo" aún hoy es posible y, generalmente, gratuíto. Mas es cosa alarmante cuando se prima el sentimiento por encima del razonamiento.    
"Cyberpunk", "facción rebelde". Cosas del primer mundo. Cojan una piedra de molino, átensela al pescuezo y tírense por un acantilado.  

<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
<br>
<br>
<br>
