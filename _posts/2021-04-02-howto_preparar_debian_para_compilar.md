---
layout: post
title: "[HowTo] Preparar Debian y derivados para compilar software"
date: 2021-04-02
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "compilar", "compilacion", "build-essential", "linux-headers", "uname -r", "pkg-config", "module-assistant", "software", "make"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
En muchas ocasiones se deseará compilar software en lugar de instalarlo desde los repositorios oficiales. Esto tiene muchas ventajas, mas también algunos inconvenientes como, por ejemplo, que habremos de actualizar ese software a mano (`sudo apt upgrade` no actualiza el software que se compila "a mano" en el sistema).  
Preparar para compilar software sistemas GNU Linux como Debian y derivados es sencillo: basta con instalar en el sistema los paquetes `build-essential` y `linux-headers` apropiados para nuestro sistema.
~~~bash
sudo apt-get update
sudo apt install build-essential
sudo apt install linux-headers-$(uname -r)
~~~

También, si no están instalados, es conveniente instalar `make`, `automake`, `cmake`, `autoconf`.
~~~bash
sudo apt install make
sudo apt install automake
sudo apt install cmake
sudo apt install autoconf
~~~

<br>
En mi caso, [cuando hube de compilar un programita llamado 'bucklespring'](https://hijosdeinit.gitlab.io/howto_compilar_bucklespring/), hube de instalar también el paquete 'pkg-config':
~~~bash
sudo apt install pkg-config
~~~

<br>
**ℹ** Y para finalizar, he leído que hay [quien instala el paquete 'module-assistant'](https://www.galisteocantero.com/compilar-un-paquete-en-linux/).
~~~bash
sudo apt install module-assistant
~~~

<br>
<br>
En cuanto al procedimiento de **compilación**, una vez puestos en faena, generalmente éste consiste en:
~~~bash
./configure
make
sudo make install
~~~

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://helpdeskgeek.com/linux-tips/how-to-compile-software-packages-on-linux/">HelpDeskGeeek</a>  
<a href="https://tutostecnologicos.com/como-compilar-paquetes-de-software-en-linux/">TutosTecnologicos</a>  
<a href="https://www.galisteocantero.com/compilar-un-paquete-en-linux/">GalisteoCantero - module-assistant</a>  
<a href="https://www.diversidadyunpocodetodo.com/debian-buster-10-guia-configuracion-instalacion-software/">diversidadyunpocodetodo</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
