---
layout: post
title: "[HowTo] Reproducir un sonido de aviso cuando un trabajo termina (línea de comandos (CLI))"
date: 2021-02-15
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "linea de comandos", "cli", "terminal", "productividad", "lotes", "sonido", "alerta", "dialogo", "advertencia"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Mediante el parámetro <span style="background-color:black">**` && \`**</span> se pueden encadenar tareas en la línea de comandos de GNU Linux: <span style="color:lime">`trabajo1 && \trabajo2 && \trabajo3`</span>  

<br>
Mediante el comando <span style="background-color:black">**'mplayer'**</span> se pueden reproducir archivos de audio desde un emulador de terminal.  
<small>Muchos sistemas operativos GNU Linux traen de serie 'mplayer'. Si no, se puede instalar: <span style="color:lime">`sudo apt install mplayer`</span></small>  

<br>
<br>
Por consiguiente, combinando en un emulador de terminal -como es aquel que generalmente se lanza desde el entorno de escritorio- el comando <span style="background-color:black">**'mplayer'**</span> con el parámetro <span style="background-color:black">**` && \`**</span> se puede hacer que **obtengamos un <big>aviso sonoro</big> cuando uno o varios comandos terminen su trabajo**. Así: <span style="color:lime">`trabajo1 && \trabajo2 && \mplayer archivodeaudio.mp3`</span>.  
<br>
Por ejemplo, para que el sistema nos avise de que ha terminado de copiar un archivo:
~~~bash
cp /home/usuario/archivo /home/usuario/directorio/ && \mplayer /home/usuario/archivodeaudio.mp3
~~~

<br>
Si se desea que la reproducción de audio no muestre mensajes en la terminal se puede acompañar al comando 'mplayer' con el parámetro '-msglevel all=-1':
~~~bash
cp /home/usuario/archivo /home/usuario/directorio/ && \mplayer -msglevel all=-1 /home/usuario/archivodeaudio.mp3
~~~

<br>
<br>
En el [artículo de este blog dedicado a la incorporación de sonido a las pulsaciones del teclado](), entre otras cosas, se listaban algunos lugares de Internet donde hay bibliotecas de sonidos:
>[Aquí (FreeSound.org)](https://freesound.org/)  
[Y aquí (Soundjay)](https://www.soundjay.com/)  
[Y aquí (SoundBible)](https://soundbible.com)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.mankier.com/1/mplayer">ManKier - mplayer</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
