---
layout: post
title: "[HowTo] Mejor forma de formatear SD desde Windows ô MacOS"
date: 2021-07-09
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "almacenamiento", "sd", "microsd", "sdhc", "pendrive", "formatear", "formato", "ntfs", "fat", "fat32", "tarjeta", "sd association", "sd memory card formatter", "hp usb format tool", "format", "diskpart", "fdisk", "explorador de archivos", "administrador de discos", "windows", "msdos", "macos"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Existen diversas formas de formatear una tarjeta SD o microSD desde Windows.  
La mejor forma, a mi entender, es mediante la herramienta que proporciona [SD Association](https://www.sdcard.org/): <big>**'SD Memory Card Formatter'**</big>.  
'SD Memory Card Formatter' es a las tarjetas SD o microSD lo que 'HP USB Format Tool' es a los dispositivos de almacenamiento USB (*pendrives*).  

<br>
## 'SD Memory Card Formatter'
'SD Memory Card Formatter' es una aplicación para formatear tarjetas SD (o microSD) que funciona en Windows y en MacOS. Se puede **descargar** -para cualquiera de estas 2 plataformas- [desde la página oficial](https://www.sdcard.org/downloads/formatter/):
- [Página oficial de descarga de 'SD Memory Card Formatter' para Windows](https://www.sdcard.org/downloads/formatter/sd-memory-card-formatter-for-windows-download/). <small>{[mirror](https://hijosdeinit.gitlab.io/assets/docs/2021-07-09-howto_formatear_SD_desde_windows/SDCardFormatterv5_WinEN.zip)}</small>
- [Página oficial de descarga de 'SD Memory Card Formatter' para MacOS](https://www.sdcard.org/downloads/formatter/sd-memory-card-formatter-for-mac-download/SDCardFormatterv5_Mac.zip). <small>{[mirror](https://hijosdeinit.gitlab.io/assets/docs/2021-07-09-howto_formatear_SD_desde_windows/SDCardFormatterv5_Mac.zip)}</small>  

<br>
<a href="/assets/img/2021-07-09-howto_formatear_SD_desde_windows/sd-memory-card-formatter.png" target="_blank"><img src="/assets/img/2021-07-09-howto_formatear_SD_desde_windows/sd-memory-card-formatter.png" alt="interfaz de 'SD Memory Card Formatter'" width="400"/></a>  
<small>interfaz de 'SD Memory Card Formatter'</small>  
<br>

Probablemente 'SD Memory Card Formatter' podría funcionar en GNU Linux mediante [Wine](https://www.winehq.org/). Yo no lo he probado... aún.  

<br>
El [**manual de usuario** puede ser descargado también desde la página oficial](https://www.sdcard.org/pdf/SD_CardFormatterUserManualEN.pdf). <small>{[mirror](https://hijosdeinit.gitlab.io/assets/docs/2021-07-09-howto_formatear_SD_desde_windows/SD_CardFormatterUserManualEN.pdf)}</small>  

<br>
<br>
## Otras formas de formatear tarjetas SD o microSD desde Windows:
- Herramienta -interfaz gráfico- que Windows incorpora por defecto [desde el 'Explorador de Archivos'(<small>click derecho sobre la unidad → formatear)</small>), o desde 'Administrador de discos'].
- Desde línea de comandos MS-DOS: Comando ['format'](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/format) [<span style="background-color:#042206"><span style="color:lime">`format '*letra_de_la_unidad*</span></span>], que puede ser acompañado de los comandos / herramientas [<span style="background-color:#042206"><span style="color:lime">'diskpart'</span></span>](https://docs.microsoft.com/en-us/windows-server/administration/windows-commands/diskpart) y/o [<span style="background-color:#042206"><span style="color:lime">'fdisk'</span></span>](https://www.computerhope.com/fdiskhlp.htm).  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Mejor forma de formatear un dispositivo de almacenamiento USB (*pendrive*) desde Windows](https://hijosdeinit.gitlab.io/howto_formatear_dispositivo_almacenamiento_usb_desde_windows/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="@@@@@@@@@@@@@@@@@@@@@@@@@@@">@@@@@@@@@@@@@@@@@@@@@@@@@@@</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
