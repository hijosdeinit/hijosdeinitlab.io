---
layout: post
title: "[HowTo] Instalación de 'GO' en Debian 10 32bits (y derivados)"
date: 2021-08-13
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "debian", "go", "programacion", "32 bits", "x32"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small>**ℹ** Para arquitecturas de 64 bits **el procedimiento es el mismo**, salvo que el archivo 'tar.gz' que habrá de descargarse es el de 64 bits.</small>  

<br>
Descárguese 'GO' para arquitectura de 32 bits desde su [página oficial](https://golang.org/dl/). Generalmente interesa incorporar la última versión.
~~~bash
wget https://golang.org/dl/go1.16.7.linux-386.tar.gz
~~~

<br>
Compruébese la integridad del *tarball* descargado
~~~bash
sha256sum go1.16.7.linux-386.tar.gz
~~~

<br>
Descomprímase el *tarball* descargado en '/usr/local'
~~~bash
sudo tar -C /usr/local -xzf go1.16.7.linux-386.tar.gz
~~~

<br>
Edítese la variable de entorno '$PATH' para especificarle permanentemente al sistema dónde están ubicados los binarios ejecutables de 'GO'.  
Si se desea configurar dicha variable para todo el sistema (todos los usuarios) hay que editar el archivo '/etc/profile'.  
Si se desea configurar dicha variable para sólo un usuario hay que editar el archivo '/home/usuario/.profile'. <small>Esto fue lo que decidí</small>.  
Añádase al archivo '/home/usuario/.profile' (<small>o '/etc/profile' si se desea configurar la variable para todo el sistema</small>) la siguiente línea:
~~~
export PATH=$PATH:/usr/local/go/bin
~~~
Guárdense los cambios.

<br>
Aplíquese la nueva variable de entorno 'PATH' a la actual sesión shell ejecutando:
~~~bash
source ~/.profile
~~~

<br>
Compruébese la versión de 'GO' y que todo está correcto:
~~~bash
go version
~~~
> go version go1.16.7 linux/386

<br>
<br>
Entradas relaccionadas:  
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://linuxize.com/post/how-to-install-go-on-debian-10/">LinuXize</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
