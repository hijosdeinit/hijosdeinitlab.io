---
layout: post
title: '[HowTo] Búsquedas desde la línea de comandos GNU Linux'
date: 2020-08-31
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "búsquedas", "find", "locate", "grep", "awk", "archivos", "cadenas", "texto"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
# 1. Búsqueda de archivos: FIND
<small>Nota: Una alternativa al comando <span style="background-color:#042206"><span style="color:lime">`find`</span></span> es <span style="background-color:#042206"><span style="color:lime">`locate`</span></span>.</small>  
Esta entrada del blog se centrará en el empleo de los comandos <span style="background-color:#042206"><span style="color:lime">`find`</span></span> y <span style="background-color:#042206"><span style="color:lime">`grep`</span></span> para buscar y filtrar archivos.  

<br>
## 1.1. Búsqueda de un archivo cuyo nombre conocemos con exactitud
<span style="background-color:#042206"><span style="color:lime">`find` *ruta* `-name` *nombredelarchivo*</span></span>  
Por ejemplo, para buscar en raiz el archivo "FooBar.md" (y no "foobar.md")
~~~bash
find / -name FooBar.md
~~~

<br>
<br>
## 1.2. Búsqueda de un archivo cuyo nombre desconocemos con exactitud

### 1.2.1. Irrelevancia de mayúsculas y minúsculas en la búsqueda
<span style="background-color:#042206"><span style="color:lime">`find `*ruta*` -iname` *nombredelarchivo*</span></span>  
Por ejemplo, para buscar en raiz los archivos foobar.md, FooBar.md, etc
~~~bash
find / -iname foobar.md
~~~

<br>
### 1.2.2. Conocimiento de sólo una parte del nombre del archivo
<span style="background-color:#042206"><span style="color:lime">`find `*ruta*` -name `*partedelnombre*</span></span>  
Por ejemplo, para buscar en raiz los archivos en cuyo nombre contengan "oob"
~~~bash
find / -name *oob*
~~~
... O para buscar en raiz los archivos en cuyo nombre contengan "oob", sin importar mayúsculas o minúsculas
~~~bash
find / -iname *oob*
~~~

<br>
<br>
## 1.3. (parámetros extra:) Filtrando por tipo
Añadiendo el parámetro <span style="background-color:#042206"><span style="color:lime">`-type`</span></span> a continuación de la ruta donde se desea buscar, se filtran los resultados por tipo:
> '-type f' para los archivos  
> '-type l' para los enlaces simbólicos  
> '-type d' para los directorios  

Por ejemplo, para buscar en '/home' los directorios cuyos nombres contengan la cadena "foo" sin importar mayúsculas o minúsculas:
~~~bash
find /home -type d -iname *foo*
~~~
ô, para buscar en '/' los archivos cuyo nombre contenga la cadena "foo" sin importar mayúsculas o minúsculas:
~~~bash
find / -type f -iname *foo*
~~~

<br>
<br>
<br>
# 2. Búsqueda de cadenas de texto: GREP
<span style="background-color:#042206"><span style="color:lime">`grep -r -i -l `*palabra* *ruta*</span></span>  
Por ejemplo, para buscar de forma recursiva detro de los contenidos de la carpeta /home/hijosdeinit/ la cadena 'foo' sin importar mayúsculas o minúsculas:
~~~bash
grep -r -i -l -s foo /home/hijosdeinit/
~~~
o varias palabras:
~~~bash
grep -r -i -l -s "Érase una vez" /home/hijosdeinit/
~~~
El parámetro `-s` sirve para que no se muestren por pantalla los errores de acceso, etcétera.  

<br>
<br>
<br>
Entradas relacionadas:  
- [Búsqueda de archivos grandes desde la línea de comandos (CLI) de GNU Linux](https://hijosdeinit.gitlab.io/howto_busqueda_archivos_grandes_cli_gnu_linux/)
- [[HowTo] Cálculo del número de archivos y subdirectorios que contiene un directorio mediante 'find'](https://hijosdeinit.gitlab.io/howto_calcular_numero_archivos_y_o_subdirectorios__find/)  

<br>
<br>
<br>
<br>

---  
<small>Fuentes:  
[StackOverflow](https://es.stackoverflow.com/questions/1479/buscar-todos-los-archivos-que-contienen-una-cadena-de-texto-en-linux)</small>  

<br>
<br>
<br>
<br>
