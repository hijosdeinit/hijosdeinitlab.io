---
layout: post
title: "[HowTo] Capturar la pantalla mediante 'ffmpeg'"
date: 2021-06-13
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "ffmpeg", "screencast", "captura", "pantalla", "multimedia", "audio", "video", "transcodificacion", "compresion"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Existe una gran variedad de software para capturar la pantalla y el sonido:
- ['VLC']()
- ['Simple Screen Recorder'](https://www.maartenbaert.be/simplescreenrecorder/#download)
- ['vokoscreen']()
- ['kazam']()
- ['recordmydesktop']()
- ['shutter']()
- ['Open Broadcaster Software Studio' (OBS)]()
- ['Screen Studio']()
- ['Peek']()
- ['Gifine']()  

Sin embargo todo ese software no está instalado por defecto en la mayoría de distribuciones, y menos en distribuciones que se despliegan "peladas" como es el caso de Debian en su modo de instalación *netinstall*.  
<big>**'ffmpeg'**</big> es un comando que generalmente está presente en casi todas las distribuciones GNU Linux (En Debian 10, aún instalado al modo *netinstall* está, por ejemplo).  

<br>
Bien, mediante **'ffmpeg'** se puede capturar la pantalla de una manera sencilla y cómoda.  
<span style="background-color:#042206"><span style="color:lime">`ffmpeg -f x11grab -r 25 -s` *resolucion-de-pantalla* `-i :0.0 -vcodec` *codec-de-compresión* *nombre-del-archivo-de-video-resultante*</span></span>  

<br>
Donde:  
<span style="background-color:#042206"><span style="color:lime">`-f`</span></span> indica el formato.  
<span style="background-color:#042206"><span style="color:lime">`-s`</span></span> indica la resolución.  
<span style="background-color:#042206"><span style="color:lime">`-r`</span></span> indica fps.  
<span style="background-color:#042206"><span style="color:lime">`-i`</span></span> indica el “archivo de entrada”, en este caso la pantalla.  

<br>
Por ejemplo:  
~~~bash
ffmpeg -f x11grab -r 25 -s 1440x900 -i :0.0 -vcodec huffyuv screencast.avi
~~~
ô, a mi juicio, mejor (menor tamaño de archivo resultante)  
~~~bash
ffmpeg -f x11grab -r 25 -s 1440x900 -i :0.0 -vcodec h264 screencast.avi
~~~

<br>
(**ℹ**) Para **parar la captura** basta con pulsar <span style="background-color:#042206"><span style="color:lime">**'ctrl'+'c'**</span></span>.  

<br>
<br>
Entradas relacionadas:  
- [[HowTo] Extraer mediante ffmpeg el audio de un archivo de video](https://hijosdeinit.gitlab.io/howto-ffmpeg-Extraer-audio-de-archivo-de-video/)
- [[HowTo] Convertir vídeo ‘.ogv’ a otros formatos](https://hijosdeinit.gitlab.io/howto_convertir_ogv_a_otros_formatos/)
- [[HowTo] Incorporar pista de audio a una pista de video](https://hijosdeinit.gitlab.io/howto_incorporar_pista_audio_a_video/)
- [[HowTo] Trocear un video](https://hijosdeinit.gitlab.io/howto_trocear_video/)
- [[HowTo] Cálculo del bitrate de un video para su recompresión en 2 pasadas](https://hijosdeinit.gitlab.io/howto_calculo_bitrate_video_recompresion_2_pasadas_ffmpeg_handbrake/)
- [[HowTo] instalacion de HandBrake en Ubuntu y derivados](https://hijosdeinit.gitlab.io/howto_instalacion-handbrake-ubuntu-apt/)
- [[HowTo] Extracción de imagenes de un video mediante ffmpeg (GNU Linux)](https://hijosdeinit.gitlab.io/howto_ffmpeg_extraer_fotogramas_de_archivo_video/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://blog.desdelinux.net/top-5-para-screencasting-en-linux/">DesdeLinux</a>  
<a href="https://www.solvetic.com/page/recopilaciones/s/programas/programas-gratis-descargar-grabar-escritorio-pantalla-linux">Solvetic</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
