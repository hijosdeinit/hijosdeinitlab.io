---
layout: post
title: "'DuckDuckGo' NO es de fiar"
date: 2022-04-17
author: Termita
category: "internet 4.0"
tags: ["internet 4.0", "buscadores", "web", "duckduckgo", "censura", "privacidad", "maltrato"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Desconfiar  de 'Google' (Alphabet) es razonable.  
Y también lo es desconfiar de 'DuckDuckGo', el buscador web que decía ser respetuoso con la privacidad y la libertad del internauta, tan alabado por los mentideros como alternativa supuestamente digna a 'Google'.  
'DuckDuckGo' no respeta la privacidad, y tampoco la libertad de los que a él acuden a buscar información. Hace tiempo que 'DuckDuckGo' reconoce que censura contenidos y adultera resultados de búsqueda.  Trata al internauta como si de un incapaz se tratara, filtrando contenidos supuestamente por su bien, sin que se la haya pedido.  
Como ocurre con la moda de lo supuestamente "verde" y "sostenible" -y caro, *porqueyolovalgo*- y el afán de las empresas por aparentar ecologismo, lo de 'DuckDuckGo' con la privacidad y la libertad de información es pura fachada.  
'DuckDuckGo' CENSURA contenidos relacionados con descargas 'torrent' (deja, por ejemplo, de proporcionar resultados de 'The Pirate Bay'), y también la información "alternativa" -es decir, no afín al discurso del régimen "occidental"- sobre la guerra de Ucrania. Todo al más puro estilo de 'Google' y las ['Big Tech']().  

<br>
<br>
Existiendo [**buscadores web MEJORES**]() como ['SearX']() -<small><small>[]()</small></small>-, ['FuckOffGoogle']() -<small><small>[]()</small></small>-, ['QWant']() -<small><small>[]()</small></small>-, ['Yandex']() o ['Brave'](https://search.brave.com/), ¿quién necesita soportar los manejos de 'DuckDuckGo'?  

<br>
<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">ivpn.net</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>

