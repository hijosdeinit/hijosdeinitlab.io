---
layout: post
title: "'GAFAM', 'FAANG', 'MAMAA', 'G-Mafia', 'BATX' y otros palabros de esta internet cuatropuntocero"
date: 2022-01-29
author: Termita
category: "internet 4.0"
tags: ["gafam", "faang", "fang", "mamaa", "G-Mafia + BAT", "G-Mafia", "gmafia", "bat", "batx", "internet 4.0", "tecnologicas", "financieras", "big tech", "big four", "big five", "tech giants", "gratis", "servicios", "Ivoox", "Alphabet", "Google", "meta", "Facebook", "twitter", "netflix", "disney", "xiaomi", "huawei", "snap", "uber", "bitedance", "mendicidad 4.0", "patreon", "mercaderes", "mercantil", "suscripción", "incerteza", "dependencia", "web 4.0", "oligopolio", "hegemonia", "dominio", "dominio del mercado", "mercado", "globalizacion", "neutralidad en la red", "totalitarismo", "maltrato"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
***'GAFAM'***. Jamás antes había leído / escuchado tal palabro que a la sazón sonaba a acrónimo de nada bueno.  
Se lo leí a [sl1200](https://nixnet.social/sl1200) en la red 'Mastodon', mas -atareado como yo andaba- mi curiosidad húbose de posponer para momentos mejores, esos que uno suele dedicar a procastinar.  

<br>
<a href="/assets/img/2022-01-29-glosario_palabros_internet_4.0_gafam_faang/sl1200_gafam.png" target="_blank"><img src="/assets/img/2022-01-29-glosario_palabros_internet_4.0_gafam_faang/sl1200_gafam.png" alt="¿Qué demonios significará 'GAFAM'? Nada bueno, probablemente" width="500"/></a>  
<small>¿Qué demonios significará 'GAFAM'? Nada bueno, probablemente</small>  
<br>

Bien, llegado el momento, una simple búsqueda en [FuckOffGoogle](https://search.fuckoffgoogle.net) me ha sacado de la ignorancia. Ahora sé lo que significa, ese y otros términos relacionados.  
<br>
'GAFAM' es el acrónimo formado por los nombres de las empresas:
- **G**oogle (Alphabet)
- **A**pple
- **F**acebook (Meta)
- **A**mazon
- **M**icrosoft  

Así que es, en definitiva, un término que hace referencia a las llamadas *Big Tech* o *Tech Giants*, *Big Four* o *Big Five*, al igual que -décadas atrás- se hablaba de las denominadas *Big Oil*. Como ocurre con el demonio, las corporaciones reciben muchos nombres.  

Esta palabreja me condujo a otras similares, algunas de ellas acuñadas por [Jim Cramer](https://en.wikipedia.org/wiki/Jim_Cramer), de la NBC:
- 'GAFA': (<small>*Big Four*</small>) 'Google', 'Amazon', 'Facebook', 'Apple'.
- 'FAANG' o 'FANG': 'Facebook', 'Apple', 'Amazon', 'Netflix', 'Google'.
- 'MAMAA': 'Microsoft', 'Alphabet' ('Google'), 'Meta', 'Apple', 'Amazon', 
- 'GAMAM': (<small>*Big Five*</small>) 'Google', 'Amazon', 'Meta', 'Apple', 'Microsoft'.
- 'G-MAFIA + BAT': 'Google', 'Microsoft', 'Apple', 'Facebook', 'IBM', 'Amazon', 'Baidu', 'Alibaba', 'Tencent'.
- 'BATX': 'Baidu', 'Alibaba', 'Tencent', 'Xiaomi'.  

Aunque más pequeñas en capitalización de mercado, 'Netflix', 'Twitter', 'Snap' y 'Uber' son en ocasiones llamadas "Big Tech" debido a su influencia.  
Es el caso de 'Twitter', que desempeña un relevante e influyente papel en el debate político y económico y/o en la percepción que de éste se tiene.  

<br>
<br>
Conjugando todo esto con [aquello otro que repasaba esta entrada de este blog](https://hijosdeinit.gitlab.io/lo_gratis/) podríase sospechar sin errar que el asunto de estas entidades NO es el progreso, ni el bien de sus usuarios / usados, sino -en todo caso- la prestación de un servicio o la venta de bienes a cambio de extraer el mayor beneficio económico posible y, también, una mayor cuota de PODER, comprar y vender voluntades.  
Utilizar sus servicios, depender de ellos -aún existiendo otros libres y, en muchos casos, mejores, más potentes y más dignos- no es inofensivo, ni tan siquiera cortoplacista (<small>"lo dejo cuando quiero", "es sólo ocio", etc</small>). Tiene repercusiones muy serias no sólo en la red, sino en el propio devenir de la humanidad.  
Hemos criado y alimentado nuevas hienas; y ahí siguen, junto a aquellas otras que las precedieron... engordando.  

<br>
<br>
Entradas relacionadas:  
- ["Gratis"](https://hijosdeinit.gitlab.io/lo_gratis/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://en.wikipedia.org/wiki/Big_Tech">WikiPedia - Big Tech</a>  
<a href="https://www.cnbc.com/2021/10/29/cramer-new-acronym-to-replace-faang-after-facebook-name-change-to-meta.html">cnbd - Mad Money - Jim Cramer</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
