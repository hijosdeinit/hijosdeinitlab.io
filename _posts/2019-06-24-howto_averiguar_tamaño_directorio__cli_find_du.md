---
layout: post
title: "[HowTo] Averiguar el tamaño de un directorio desde línea de comandos (CLI) mediante 'du'"
date: 2019-06-24
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "gnu linux", "linux", "windows", "backup", "respaldo", "comparacion", "catalogacion", "comparar", "comparar contenidos", "contenido", "almacenamiento", "disco", "directorio", "archivo", "fichero", "diff", "diffmerge", "total commander", "winmerge", "tar", "md5", "md5sum", "suma de verificacion", "sha256", "sha256sum", "transferir", "copiar", "sincronizar", "sort", "find", "calculo", "almacenamiento", "cli", "du"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
~~~bash
du -bsh nombredeldirectorio
~~~

<br>
¿Qué significan cada uno de esos parámetros?

<span style="background-color:#042206"><span style="color:lime">`-b`</span></span>	tamaño en bytes  
<span style="background-color:#042206"><span style="color:lime">`-s`</span></span>	sólo mostrar el tamaño total de cada argumento  
<span style="background-color:#042206"><span style="color:lime">`-h`</span></span>	mostrar los tamaños de forma fácilmente legible (K, M, G, etc...)  

<br>
<a href="/assets/img/2019-06-24-howto_averiguar_tamaño_directorio__cli_find_du/du.png" target="_blank"><img src="/assets/img/2019-06-24-howto_averiguar_tamaño_directorio__cli_find_du/du.png" alt="'du'" width="600"/></a>  
<small>'du'</small>  
<br>
<br>

<br>
Recordatorio:  
<small>(*) Cuando hacemos click derecho sobre una carpeta desde los exploradores de archivos en entorno gráfico -Nautilus, caja, etcétera- y seleccionamos "Propiedades", se nos muestra -a parte del TAMAÑO- el NÚMERO DE ARCHIVOS Y SUBDIRECTORIOS que contiene.</small>  
<small>Si queremos contar la cantidad de elementos -archivos y/o subdirectorios- que contiene un directorio podemos usar el comando find:</small>  
<small>Contar ficheros y subdirectorios: <span style="background-color:#042206"><span style="color:lime">`find . | wc -l`</span></span></small>  
<small>Contar sólo archivos: <span style="background-color:#042206"><span style="color:lime">`find . -type f | wc -l`</span></span></small>  
<small>Contar sólo subdirectorios: <span style="background-color:#042206"><span style="color:lime">`find . -type d | wc -l`</span></span></small>  

<br>
<br>
<br>
Entradas relacionadas:  
- [[HowTo] Cálculo del número de archivos y subdirectorios que contiene un directorio mediante 'find'](https://hijosdeinit.gitlab.io/howto_calcular_numero_archivos_y_o_subdirectorios__find/)
- [[HowTo] Búsquedas desde la línea de comandos GNU Linux](https://hijosdeinit.gitlab.io/howto_busquedas_CLI_gnulinux/)
- [[HowTo] Búsqueda de archivos grandes desde línea de comandos en GNU Linux](https://hijosdeinit.gitlab.io/howto_busqueda_archivos_grandes_cli_gnu_linux/)
- [[HowTo] Comprobación de que un directorio y sus subdirectorios se han transferido correctamente a su destino](https://hijosdeinit.gitlab.io/howto_comprobacion_copiado_correcto_directorios_archivos/)
- [[HowTo] Comparación de la suma de verificación md5 de varios directorios mediante 'tar'](https://hijosdeinit.gitlab.io/howto_comparacion_md5sum_varios_directorios_mediante_tar/)
- [[HowTo] Comparación de discos, directorios y archivos](https://hijosdeinit.gitlab.io/howto_comparacion_discos_directorios_archivos/)
- [[HowTo] 'diff'. Comparar directorios ô archivos](https://hijosdeinit.gitlab.io/howto_comparar_directorios_o_archivos/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://blog.desdelinux.net/con-el-terminal-comandos-de-tamano-y-espacio/">blog DesdeLinux</a>  
<a href="https://ed.team/blog/como-ver-el-tamano-de-una-carpeta-con-la-terminal">ed.team</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
