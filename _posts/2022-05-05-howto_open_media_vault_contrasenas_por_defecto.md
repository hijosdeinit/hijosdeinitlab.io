---
layout: post
title: "Postinstalación de Open Media Vault 6: usuario y contraseña por defecto"
date: 2022-05-05
author: Termita
category: "redes"
tags: ["redes", "sistemas operativos", "gnu linux", "linux", "open media vault", "omv", "omv 6", "open media vault 6", "raspberry pi", "armbian", "arm", "nas", "RaspberryPi", "Raspbian", "RaspBerry OS", "interfaz web", "ssh"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
<small><small>Entorno: RaspBerry Pi 3b, RaspBerry OS x64, Kernel: 5.15.32-v8+, sin entorno gráfico</small></small>  

<br>
<br> 
Recién instalado ['Open Media Vault'](https://www.openmediavault.org/) mediante el script de -valga la redundancia- instalación, es constatable  que los usuarios "convencionales" del sistema ya no pueden acceder vía SSH. De hecho, si la ejecución del script se ha realizado a través de una conexión SSH ésta se interrumpe.  
Esto es obra y milagro del script de instalación de OMV, y está hecho a drede, no es una fatalidad o un error. Sólo el usuario 'root', si éste previamente tenía acceso ssh al sistema, mantendrá ese privilegio.  
Una de las formas de volver a otorgar acceso SSH a los usuarios es mediante el **interfaz web de administración de 'OMV'**. Mas, por ejemplo, ¿cómo nos *logueamos* en dicho interfaz?

<br>
## Usuarios y contraseñas por defecto de OMV 6
### 1. Interfaz Web
url: http://ip_de_la_máquina_que_ejecuta_omv:80  
usuario: admin  
contrasema: openmediavault  

### 2. SSH
usuario: root  
contraseña: la contraseña del usuario 'root' (el script de instalación de OMV no la cambia, como tampoco hace que pierda el acceso SSH).  

<br>
Vía SSH, el comando <span style="background-color:#042206"><span style="color:lime">`omv-firstaid`</span></span> permite, al igual que el interfaz web, dar privilegios de acceso ssh a los usuarios "convencionales", y también -entre otras configuraciones- cambiar la contraseña de acceso a dicho interfaz.  

<br>
<br>
<br>
Entradas relacionadas:  
- []()
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a>  
<a href="zzz">zzz</a></small>  

<br>
<br>
<br>
<br>
<br>
<br>
