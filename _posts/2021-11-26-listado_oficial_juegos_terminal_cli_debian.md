---
layout: post
title: "Listado oficial de juegos para línea de comandos de Debian"
date: 2021-11-26
author: Termita
category: "sistemas operativos"
tags: [sistemas operativos", "gnu linux", "linux", "debian", "juegos", "linea de comandos", "terminal", "cli"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Debian pone a disposición de todos un [listado oficial de juegos para terminal de Debian](https://blends.debian.org/games/tasks/console)  
<br>
No están listados todos los juegos. Por ejemplo [**'pacvim'**](https://github.com/jmoon018/PacVim), un juego basado en el arcaico 'pacman' que -a parte de entretener- sirve para asimilar las teclas de movimiento características de 'vi', las conocidas teclas 'hjkl'. En [esta entrada este blog se habla, entre otros, de 'pacvim'](https://hijosdeinit.gitlab.io/vimadventures-vimtutor_juegos_para_aprender_vi/).  
 
<br>
<br>
Entradas relacionadas:  
- [Listado oficial de juegos para línea de comandos de Debian](https://hijosdeinit.gitlab.io/listado_oficial_juegos_terminal_cli_debian/)
- [[HowTo] 'zangband', juego *roguelike* para la terminal ('CLI')](https://hijosdeinit.gitlab.io/howto_zangband_juego_cli/)  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="zzz">zzz</a></small>  
<br>
<br>
<br>
<br>
<br>
<br>
