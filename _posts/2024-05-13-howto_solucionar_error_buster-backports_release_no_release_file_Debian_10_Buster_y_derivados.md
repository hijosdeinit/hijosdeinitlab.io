---
layout: post
title: "[HowTo] Solucionar error 'buster-backports Release no longer has a Release file' (en Debian 10 y derivados)"
date: 2024-05-13
author: Termita
category: "sistemas operativos"
tags: ["sistemas operativos", "linux", "gnu linux", "debian", "debian 10", "buster", "debian 10 buster", "software", "apt", "backports", "debian-backports", "release", "release file", "archive.debian.org", "deb.debian.org"]
toc: true
toc_level: 4
post_list: "date"
home_btn: true
btn_text: true
footer: true
maximize: true
encrypted_text: true
comments: true
tipue_search_active: true
exclude_from_search: false
---
Si, en Debian 10 *Buster* o derivados, al ejecutar <span style="background-color:#042206"><span style="color:lime">`sudo apt-get update`</span></span> se obtiene un error similar a este:
> E: The repository 'http://deb.debian.org/debian buster-backports Release' no longer has a Release file.  

...habremos de editar el archivo '**/etc/apt/sources.list**' -ejecutando, por ejemplo, <span style="background-color:#042206"><span style="color:lime">`sudo nano /etc/apt/sources.list`</span></span>- y cambiar:  
'<span style="background-color:#042206"><span style="color:lime">deb http://deb.debian.org/debian buster-backports main contrib non-free</span></span>'  
por:  
<big>'<span style="background-color:#042206"><span style="color:lime">deb http://archive.debian.org/debian buster-backports main contrib non-free</span></span>'</big>  

<br>
A continuación bastará con ejecutar <span style="background-color:#042206"><span style="color:lime">`sudo apt-get update`</span></span> y podremos constatar que el error está solucionado.  

<br>
<br>
<br>
El error que acabamos de subsanar puede deberse a una de estas razones:
- repositorio desactualizado
- una entrada incorrecta en el archivo '/etc/apt/sources.list'  

<br>
<br>
<br>
¿Qué son los ***backports***?  
De la [página oficial de debian](https://backports.debian.org/):  
«You are running Debian stable, because you prefer the Debian stable tree. It runs great, there is just one problem: the software is a little bit outdated compared to other distributions. This is where backports come in.  
Backports are packages taken from the next Debian release (called "testing"), adjusted and recompiled for usage on Debian stable. Because the package is also present in the next Debian release, you can easily upgrade your stable+backports system once the next Debian release comes out. (In a few cases, usually for security updates, backports are also created from the Debian unstable distribution.)  
Backports cannot be tested as extensively as Debian stable, and backports are provided on an as-is basis, with risk of incompatibilities with other components in Debian stable. Use with care!  
It is therefore recommended to only select single backported packages that fit your needs, and not use all available backports.»  

<br>
<br>
<br>
<br>
Entradas relaccionadas:  
- [[HowTo] '/etc/apt/sources.list': repositorios en Debian y derivados](https://hijosdeinit.gitlab.io/howto_repositorios_sources.list_debian_y_derivados/)
- [[HowTo] Agregar repositorio 'debian unstable' a Debian 10 y similares](https://hijosdeinit.gitlab.io/howto_agregar_repositorio_debian_unstable_debian_y_derivados/)
- [[HowTo] Instalación de una versión más actualizada de un paquete desde los repositorios oficiales de Debian: 'backports'](https://hijosdeinit.gitlab.io/howto_instalacion_version_mas_actualizada_desde_repositorios_Debian_backports/)
- [[HowTo] Instalar el comando 'add-apt-repository' en Debian](https://hijosdeinit.gitlab.io/howto_add-apt-repository_en_debian_y_derivados/)
- [[HowTo] Eliminar repositorio agregado a mano ('add-apt-repository') en Debian y derivados](https://hijosdeinit.gitlab.io/howto_eliminar_repositorios_agregados_manualmente_addaptrepository_Debian_derivados/)
- [[HowTo] Eliminar Clave 'Gpg' Cuyo 'Id' Desconocemos. (Debian Y Derivados)](https://hijosdeinit.gitlab.io/howto_eliminar_clave_gpg_id_desconocida_agregada_mediante_apt-key_add_/)
- [[HowTo] Solucionar error de GPG cuando la clave pública de un repositorio no está disponible. (Debian y derivados)](https://hijosdeinit.gitlab.io/howto_solucionar_error_gpg_clave_publica_repositorio_Debian_y_derivados/)
- [El repositorio de firmware / drivers para GNU Linux](https://hijosdeinit.gitlab.io/el_repositorio_de_firmware_drivers_para_gnu_linux/)
- ['RaspBian' / 'RaspBerry OS' incorpora furtivamente -sin aviso ni permiso- un repositorio de 'Microsoft'. Así no vamos bien](https://hijosdeinit.gitlab.io/repositorio_microsoft_instalado_furtivamente_en_Raspbian_RaspBerryOs/)
- [[HowTo] Extirpar todo rastro del repositorio de MicroSoft que Raspbian / RaspBerry Os instala furtivamente](https://hijosdeinit.gitlab.io/howto_eliminar_repositorio_microsoft_instalado_sin_permiso_en_Raspbian_RaspBerryOs/)
- [Repositorio con todos los resaltados de sintaxis (.nanorc) para nano](https://hijosdeinit.gitlab.io/Repositorio-resaltados-sintaxis-nano/)
- [[HowTo] Instalación de VSCodium (MS VSCode sin telemetría) en Debian y derivados [desde repositorio]](https://hijosdeinit.gitlab.io/howto_instalacion_vscodium_vscode-sintelemetria_desderepositorio/)
- []()  

<br>
<br>
<br>
--- --- ---  
<small>Fuentes:  
<a href="https://www.cyberciti.biz/faq/the-repository-http-deb-debian-org-debian-buster-backports-release-no-longer-has-a-release-file/">nixCraft</a>  

<br>
<br>
<br>
<br>
<br>
<br>

