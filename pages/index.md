---
layout: default
post_list: "date"
toc: false
home_btn: true
btn_text: true
footer: true
title: ""
author: ""
encrypted_text: true
permalink: /
---

# <big>H i j o s · D e · I n i t</big>   - <small>Bitácora de tecnología libre</small>
<br>
<p align="center"><img src="https://hijosdeinit.gitlab.io/assets/img/hormiga_alfa.png" alt="H i j o s D e I n i t"></p>
<br>
## Propósito
Cuaderno de bitácora acerca de andanzas por los senderos del software libre y la tecnología. El arte de buscarse la vida y pasarlas canutas en la persecución de la autosuficiencia. Lonchafinismo y madmaxismo. Aquí no se tira nada.  
<br>
<big>**♫**</big> <a href="https://odysee.com/@hijosdeinit:4" target="_blank">hijosdeinit - **Canal de video en Odysee/lbry**</a>  
<big>**♪**</big> <a href="@@@@@@@@@@@@@@@@" target="_blank">hijosdeinit - **Podcast** <small>(en construcción)</small></a>  
<br>
<br>
<small>*«La industria informática es la única más controlada por las modas 
que la propia moda para mujeres»*</small>  
<small>Stallman</small>  

<br>
<br>
<small>Este blog no cuantifica ni monetiza al visitante. Sean muchos, sean pocos, no me importan esos detalles, de dónde vienen o a dónde van; lo que aquí se expone está escrito por puro placer. Este blog prefiere sacrificar un sistema de comentarios cómodo en pro de la privacidad del visitante. Incorporar sistemas de comentarios como 'Disqus' u otros suministra telemetría a terceros, es contrario a la privacidad y una **falta de respeto** al lector.</small>  
<span style="color:orange"><big>**✉**</big></span> Siéntanse libres de **comentar**, **corregir** o **comunicar**. Para ello pueden dirigirme un correo electrónico a <span style="background-color:#00FF00"><span style="color:black"><big>**hijosdeinit@protonmail.com**</big></span></span>. Pronto o tarde siempre respondo.  
<br>
<br>

## Búsqueda ↅ
<big><a href="https://hijosdeinit.gitlab.io/search/">**🝯** Página de BÚSQUEDA</a></big>
<br>
<br>
<br>

##  Orden de batalla
token de acceso = <small>init</small>
<p class="encrypted" id="KORrzvC0RTn9+aa+niVEcwp1mPTolPA0uCoUqrQqNg3FIQMCT5UA3eeLfuVsOTxsnCI2GcLIveQy+HtY7r+9kuXJJTR+VDY4qVwa6Z5FJ1jzx1VVzbJFA9cc8lgT0kEK0Lg0rODzGYaDPd/9xzecDKSNf8w0DHL3ZLJDEGQle1jmdwwhZgh69NHNs4Z7BBpnn5JcbKQbd8">κρυπτογραφημένο περιεχόμενο</p>
<p class="encrypted" id="ioHAW08S23hdWkHtivK37gSIDti610iqz6coRpw92OqRK4fV9bQTfLmIty+HaVi/0gqplJMI8+Thk6U8MbdBZNvhKKLfbIHpsETSA1C4gurFPHttgdhE0/KgYm8rK/A9oQaSFPwRxphdVIbg==">κρυπτογραφημένο περιεχόμενο</p>  
<small>[herramienta de cifrado](https://hijosdeinit.gitlab.io/sjcl/)</small>  
<br>

$$
NADH+Q+5\;H_{matrix}^{+}\rightarrow NAD^{+}+QH_{2}+4\;H_{intermembrane}^{+}\!
$$  
$$
2\;CH_{4}+2\;NH_{3}+3\;O_{2}\rightarrow 2\;HCN+6\;H_{2}O
$$  

<br>
<br>
## Recursos ajenos exquisitos 
<div class="pull-left"><small><a href="http://intrusos.info/doku.php/start" target="_blank">ℹ [LCWIKI]</a><br>
<a href="http://logicademhyst.blogspot.com/" target="_blank">ℹ [REALITY CRACKING]</a><br>
<a href="https://ea4bns.blogspot.com/" target="_blank">ℹ [Sobre ceros y unos]</a><br>
<a href="https://ugeek.github.io/blog/" target="_blank">ℹ [uGeek blog]</a><br>
<a href="https://www.flopy.es/" target="_blank">ℹ [Flopy.es]</a><br>
<a href="https://msdos.club/" target="_blank">ℹ [MSDOS club]</a></small></div>
<div class="pull-right"><small><a href="https://naseros.com/" target="_blank">ℹ [NASeros]</a><br>
<a href="https://www.eduardocollado.com/" target="_blank">ℹ [Eduardo Collado]</a><br>
<a href="https://salmorejogeek.com/" target="_blank">ℹ [Salmorejo Geek]</a><br>
<a href="https://elblogdelazaro.org" target="_blank">ℹ [Blog de Lázaro]</a><br>
<a href="https://fuubar.wordpress.com" target="_blank">ℹ [FuuBar]</a><br>
<a href="https://entropiabinaria.blogspot.com" target="_blank">ℹ [Entropía Binaria]</a></small></div>  

<br>
<br>
<br>
<br>
<br>
<br>
<br>
## [Feeds] Podcasts (y Canales de video) exquisitos
<div class="pull-left"><small><a href="http://feeds.feedburner.com/rcracking" target="_blank">♪ [feed podcast Reality Cracking]</a><br>
<a href="https://www.atareao.es/mp3-feed/" target="_blank">♪ [feed podcast Atareao]</a><br>
<a href="https://feedpress.me/naseros" target="_blank">♪ [feed podcast NASeros]</a><br>
<a href="https://msdos.club/podfeed/feed.xml" target="_blank">♪ [feed podcast MSDOS club]</a></small></div>
<div class="pull-right"><small><a href="http://feeds.feedburner.com/ugeek" target="_blank">♪ [feed podcast UGeek]</a><br>
<a href="http://feeds.feedburner.com/mosqueterowebLinux" target="_blank">♪ [feed podcast MosqueteroWeb]</a><br>
<a href="https://www.eduardocollado.com/podcast/feed/" target="_blank">♪ [feed podcast Eduardo Collado]</a><br>
<a href="https://yewtu.be/channel/UCU6NCvO1cnhH0QfnXeNkJqQ" target="_blank">♪ [canal de video Entropía Binaria]</a></small></div>  

<br>
<br>
<br>
<br>
<br>
<hr/>
<br>
<br>
